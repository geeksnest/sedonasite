{{ content() }}

<script type="text/ng-template" id="bookDelete.html">
    <div ng-include="'/be/tpl/bookDelete.html'"></div>
</script>

<script type="text/ng-template" id="replyBooking.html">
    <div ng-include="'/be/tpl/replyBooking.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
    <h1 class="m-n font-thin h3">Booking</h1>
    <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" name="formbooking">
    <fieldset ng-disabled="isSaving">
        <div class="wrapper-md">
            <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

            <div class="row">

                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading font-bold">
                            List
                        </div>

                        <div class="panel-body">

                            <div class="row wrapper">
                                <div class="col-sm-4 m-b-xs" ng-show="keyword || searchdate">
                                    <strong>{[{ bigTotalItems }]}</strong> Results found for: <strong>  <span ng-show="keyword">"{[{ keyword }]}"</span></strong>  <span ng-show="keyword && searchdate">OR</span> <strong><span ng-show="searchdate">"{[{ searchdate }]}"</span></strong> <button class="btn btn-default btn-xs" ng-click="clear()">Clear</button>
                                </div>
                                <div class="col-sm-3 m-b-xs pull-right">
                                    <div class="input-group">
                                        <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                                        <span class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext, searchdateelem)">Go!</button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-sm-3 m-b-xs pull-right">
                                    <div class="input-group">
                                      <span class="input-group-btn">
                                        <input id="date" name="date" class="input-sm form-control" datepicker-popup="yyyy-MM-dd" ng-model="searchdateelem" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
                                        <button type="button" class="btn btn-sm btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                                      </span>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped b-t b-light">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th style="width:15%">Name</th>
                                        <th style="width:15%">Email</th>
                                        <th style="width:15%">Contact</th>
                                        <th style="width:20%">Service</th>
                                        <th style="width:15%">Date</th>
                                        <th style="width:10%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody ng-show="loading">
                                        <tr >
                                            <td colspan="7">Loading News</td>
                                        </tr>
                                    </tbody>
                                    <tbody ng-hide="loading">
                                    <tr ng-show="bigTotalItems==0"> <td colspan="7"> No records found! </td></tr>
                                    <tr ng-repeat="book in data.data">
                                        <td>
                                            <i class="fa fa-envelope" ng-show="book.status=='new'"></i>
                                            <i class="fa fa-envelope-o" ng-show="book.status=='read'"></i>
                                            <i class="fa fa-mail-reply" ng-show="book.status=='replied'"></i>
                                        </td>
                                        <td>
                                            <strong ng-if="book.status == 'new'">{[{ book.fname }]} {[{ book.lname }]}</strong>
                                            <span ng-if="book.status != 'new'">{[{ book.fname }]} {[{ book.lname }]}</span>
                                        </td>
                                        <td>
                                            <strong ng-if="book.status == 'new'">{[{ book.email }]}</strong>
                                            <span ng-if="book.status != 'new'">{[{ book.email }]}</span>
                                        </td>
                                        <td>
                                            <strong ng-if="book.status == 'new'">{[{ book.contactno }]}</strong>
                                            <span ng-if="book.status != 'new'">{[{ book.contactno }]}</span>
                                        </td>
                                        <td>
                                            <strong ng-if="book.status == 'new'">{[{ book.service }]}</strong>
                                            <span ng-if="book.status != 'new'">{[{ book.service }]}</span>
                                        </td>
                                        <td>
                                            <strong ng-if="book.status == 'new'">{[{ book.updated_at }]}</strong>
                                            <span ng-if="book.status != 'new'">{[{ book.updated_at }]}</span>
                                        </td>
                                        <td>
                                            <a href="" ng-click="replyBook(book.id)"><span class="label bg-warning" ><i class="fa fa-mail-reply-all"></i></i></span></a>
                                            <a href="" ng-click="deleteBook(book.id)"> <span class="label bg-danger"><i class="fa fa-times"></i></span></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>



                        </div>


                    </div>
                </div>

            </div>

            <div class="row" ng-hide="bigTotalItems==0 || loading">
                <div class="panel-body">
                    <footer class="panel-footer text-center bg-light lter">
                        <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
                        <pagination ng-hide="maxSize > bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
                    </footer>
                </div>
            </div>

        </div>
    </fieldset>
</form>