<?php

namespace Controllers;

use \Models\Users as Users;
use \Models\Pages as Pages;
use \Models\Pageleftbar as Pageleftbar;
use \Models\Pagerightbar as Pagerightbar;
use \Models\Workshop as Workshop;
use \Models\Worshoptitle as Workshoptitle;
use \Models\Workshopvenue as Workshopvenue;
use \Models\Testimonies as Testimonies;
use \Models\Leftsidebaritem as Leftsidebaritem;
use \Models\Pageimage as Pageimage;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class PagesController extends \Phalcon\Mvc\Controller {

    public function createPagesAction() {

        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $title = $request->getPost('title');
            $metatags = preg_replace('/^([^a-zA-Z0-9])*/', '', $request->getPost('metatags'));
            $metatitle = $request->getPost('metatitle');
            $metadesc = $request->getPost('metadesc');
            $slugs = $request->getPost('slugs');
            $body = $request->getPost('body');
            $layout = $request->getPost('layout');
            $leftbar = $request->getPost('leftbar');
            $rightbar = $request->getPost('rightbar');
            $fbcapc = $request->getPost('fbcapc');

            $guid = new \Utilities\Guid\Guid();
            $id = $guid->GUID();

            $page = new Pages();
            $page->assign(array(
                'pageid' => $id,
                'title' => $title,
                'metatitle' => $metatitle,
                'metatags' => $metatags,
                'metadesc' => $metadesc,
                'pageslugs' => $slugs,
                'body' => $body,
                'status' => 1,
                'pagelayout' => $layout,
                'fbcapc' => $fbcapc,
                'type' => 'Pages',
            ));

            if (!$page->save()) {
                $errors = array();
                foreach ($page->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {

                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Pages",
                    "event" => "Add",
                    "title" => "Add New Page ".$title.""
                ));

                $pageleft = new Pageleftbar();
                $pageleft->assign(array(
                    'pageid' => $id,
                    'item' => $leftbar
                ));

                if (!$pageleft->save()) {
                    $data['error'] = "Something went wrong saving the Left Sidebar, please try again.";
                } else {
                    $data['success'] = "Success";
                }

                $tags = array();
                $tags = $rightbar;
                foreach($tags as $tag){
                    $pageright = new Pagerightbar();
                    $pageright->assign(array(
                        'pageid' => $id,
                        'item' => $tag
                    ));

                    if (!$pageright->save()) {
                        $errors = array();
                        foreach ($pageright->getMessages() as $message) {
                            $errors[] = $message->getMessage();
                        }
                        $data['error'] =  $errors;
                    } else {

                        $data['success'] = "Success";
                    }
                }
            }
        }
        echo json_encode($data);
    }

    public function saveimageAction() {

        $filename = $_POST['imgfilename'];

        $picture = new Pageimage();
        $picture->assign(array(
            'filename' => $filename
        ));

        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }

    }

    public function listimageAction() {

        $getimages = Pageimage::find(array("order" => "id DESC"));
        foreach ($getimages as $getimages)
        {
            $data[] = array(
                'id'=>$getimages->id,
                'filename'=>$getimages->filename
            );
        }
        echo json_encode($data);

    }


    public function managepagesAction($num, $page, $keyword) {

        if ($keyword == 'null' || $keyword == 'undefined') {
            $Pages = Pages::find();
        } else {
            $conditions = "title LIKE '%" . $keyword . "%' OR pageslugs LIKE '%" . $keyword . "%'";
            $Pages = Pages::find(array($conditions));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $Pages,
                "limit" => 10,
                "page" => $currentPage
            )
        );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'pageid' => $m->pageid,
                'title' => $m->title,
                'pageslugs' => $m->pageslugs,
                'status' => $m->status
            );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }


    public function pageUpdatestatusAction($status,$pageid,$keyword) {

        $data = array();
        $page = Pages::findFirst('pageid="' . $pageid . '"');
        $title = $page->title;
        $page->status = $status;
        if (!$page->save()) {
            $data['error'] = "Something went wrong saving page status, please try again.";
        } else {
            $data['success'] = "Success";
            if($status == 1){
                $pagestatus = 'Active';
            }
            else{
                $pagestatus = 'Deactivate';
            }
            $audit = new CB();
            $audit->auditlog(array(
                "module" =>"Pages",
                "event" => "Update",
                "title" => "Update ".$title." status to ".$pagestatus
            ));

        }

        echo json_encode($data);
    }

    public function pagedeleteAction($pageid) {
        $conditions = 'pageid="' . $pageid . '"';
        $pages = Pages::findFirst(array($conditions));
        $title = $pages->title;
        $data = array('error' => 'Not Found');
        if ($pages) {
            if ($pages->delete()) {
                $data = array('success' => 'Page Deleted');
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Pages",
                    "event" => "Delete",
                    "title" => "Delete page ".$title.""
                ));
            }
        }
        echo json_encode($data);
    }

    public function pageeditoAction($pageid) {
        $data = array();
        $leftbaritem = array();
        $rightbaritem = array();

        $pages = Pages::findFirst('pageid="' . $pageid . '"');
        $leftbar = Pageleftbar::findFirst('pageid="' . $pageid . '"');
        if ($leftbar) {
            $leftbaritem = array(
                'leftbar' => $leftbar->item
            );
        }

        $rightbar= Pagerightbar::find('pageid="' . $pageid . '"');
        foreach ($rightbar as $rightbar) { //;
            $rightbaritem[] = array(
                'rightbar'=>$rightbar->item
            );
        }

        if ($pages) {
            $data = array(
                'pageid' => $pages->pageid,
                'title' => $pages->title,
                'metatitle' =>$pages->metatitle,
                'metatags' => $pages->metatags,
                'metadesc' => $pages->metadesc,
                'slugs' => htmlentities($pages->pageslugs),
                'body' => $pages->body,
                'layout' => $pages->pagelayout,
                'leftbaritem' => $leftbaritem,
                'rightbaritem' => $rightbaritem,
                'fbcapc' => $pages->fbcapc
            );
        }
        echo json_encode($data);
    }

    public function pagerightitemAction($pageid) {
        $data = array();


        $rightbar= Pagerightbar::find('pageid="' . $pageid . '"');
        foreach ($rightbar as $rightbar) {;
            $data[] = array(
                'rightbar'=>$rightbar->item
            );
        }

        echo json_encode($data);
    }



    public function saveeditedPagesAction() {

        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $pageid = $request->getPost('pageid');
            $title = $request->getPost('title');
            $metatitle = $request->getPost('metatitle');
            $metatags = preg_replace('/^([^a-zA-Z0-9])*/', '', $request->getPost('metatags'));
            $metadesc = $request->getPost('metadesc');
            $slugs = $request->getPost('slugs');
            $body = $request->getPost('body');
            $layout = $request->getPost('layout');
            $leftbar = $request->getPost('leftbar');
            $rightbar = $request->getPost('rightbar');
            $fbcapc = $request->getPost('fbcapc');

            $pages = Pages::findFirst('pageid="' . $pageid . '"');
            $pages->title = $title;
            $pages->metatitle = $metatitle;
            $pages->metatags = $metatags;
            $pages->metadesc = $metadesc;
            $pages->pageslugs = $slugs;
            $pages->body = $body;
            $pages->pagelayout = $layout;
            $pages->fbcapc = $fbcapc;

            if (!$pages->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {

                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Pages",
                    "event" => "Update",
                    "title" => "Update page ".$title.""
                ));

                $conditions = 'pageid="' . $pageid . '"';
                $rightbaritem = pagerightbar::find(array($conditions));
                $data = array('error' => 'Not Found');
                if ($rightbaritem) {
                    if ($rightbaritem->delete()) {
                        $data = array('success' => 'Page Deleted');
                    }
                }


                $tags = array();
                $tags = $rightbar;
                foreach($tags as $tag){
                    $pageright = new Pagerightbar();
                    $pageright->assign(array(
                        'pageid' => $pageid,
                        'item' => $tag
                    ));

                    if (!$pageright->save()) {
                        $data['error'] = "Something went wrong saving the Right Sidebar, please try again.";
                    } else {

                        $data['success'] = "Success";
                    }
                }


                $conditions = 'pageid="' . $pageid . '"';
                $leftbaritem = Pageleftbar::find(array($conditions));
                $data = array('error' => 'Not Found');
                if ($leftbaritem) {
                    if ($leftbaritem->delete()) {
                        $data = array('success' => 'Page Deleted');
                    }
                }

                $pageleft = new Pageleftbar();
                $pageleft->assign(array(
                    'pageid' => $pageid,
                    'item' => $leftbar
                ));

                if (!$pageleft->save()) {
                    $data['error'] = "Something went wrong saving the Left Sidebar, please try again.";
                } else {
                    $data['success'] = "Success";
                }



            }
            echo json_encode($data);



        }
    }

    public function getPageAction($pageslugs) {

        $conditions = "pageslugs LIKE'" . $pageslugs . "'" ;
        echo json_encode(Pages::findFirst(array($conditions)));

    }

    public function leftsidebaridAction($pageid) {

        $conditions = 'pageid="' . $pageid . '"' ;
        echo json_encode(Pageleftbar::findFirst(array($conditions))->toArray(), JSON_NUMERIC_CHECK);

    }

    public function listleftsidebarAction($offset,$sidebarid) {

        $leftbar = Leftsidebaritem::find(array("sidebarid =" . $sidebarid . ""));
        $leftsidebar = json_encode($leftbar->toArray(), JSON_NUMERIC_CHECK);
        echo $leftsidebar;

    }

    public function listrightsidebarAction($offset,$pageid) {
        $rightbar = Pagerightbar::find(array('pageid="' . $pageid . '"'));
        $rightbar = json_encode($rightbar->toArray(), JSON_NUMERIC_CHECK);
        echo $rightbar;
    }

    public function pageinitialawakeningAction() {
        $datenow = date('Y-m-d');
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM workshop LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN center ON workshop.center = center.centerid WHERE workshoptitle.titleslugs = 'initial-awakening' AND workshop.datestart >= '$datenow' ORDER BY workshop.datestart LIMIT 1 ");
        $stmt->execute();
        $initialawakening = $stmt->fetch(\PDO::FETCH_ASSOC);

        $date_s = "";
        $date_started = date_create($initialawakening['datestart']);
        $new_date_started = date_format($date_started, "M Y");
        $month_date_started = date_format($date_started, "F");
        $year_date_started = date_format($date_started, "Y");

        $date_ended = date_create($initialawakening['dateend']);
        $new_date_ended = date_format($date_ended, "M Y");
        $year_date_ended = date_format($date_ended, "Y");

        if($year_date_started == $year_date_ended) { //IF YEAR IS THE SAME
            if($new_date_started == $new_date_ended) { //IF YEAR AND MONTH IS THE SAME
                $new_date_started = date_format($date_started, "M d-");
                $new_date_ended = date_format($date_ended,"d");
                $date_s = $new_date_started . $new_date_ended . " ".date_format($date_ended,"Y");
            } else { //IF YEAR IS THE SAME BUT MONTH IS !SAME
                $date_s = date_format($date_started, "M d-") . date_format($date_ended, "M d")." ".date_format($date_ended,"Y");
            }
        } else { //IF YEAR IS !SAME
            $date_s = date_format($date_started, "M d, Y - "). " " . date_format($date_ended, "M d, Y");
        }

        if($initialawakening == '') {
            $date_s = '';
        }

        echo json_encode(array("initawakening" => $initialawakening, "date_s" => $date_s));
    }

    public function pagerelatedAction($pageslugs) {
        $metatag = str_replace("-"," ",$pageslugs);

        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT testimonies.id, testimonies.photo, testimonies.author, testimonies.state, testimonies.metadesc, center.centertitle, center.centercity, center.centerstate FROM testimonies LEFT JOIN center ON testimonies.center = center.centerid WHERE metatags LIKE '%".$metatag."%' ORDER BY RAND() LIMIT 2");
        $stmt->execute();
        $stories = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        if($stories == null) {
            $stmt = $db->prepare("SELECT testimonies.id, testimonies.photo, testimonies.author, testimonies.state, testimonies.metadesc, center.centertitle, center.centercity, center.centerstate FROM testimonies LEFT JOIN center ON testimonies.center = center.centerid ORDER BY RAND() LIMIT 2");
            $stmt->execute();
            $stories = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }

        foreach($stories as $key => $value) {
            $stories[$key]['metadesc'] = preg_replace('/[^A-Za-z0-9\-]/', ' ', $stories[$key]['metadesc']);
        }

        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM news WHERE relatedworkshop LIKE '%$pageslugs%' ORDER BY RAND() LIMIT 2");
        $stmt->execute();
        $articles = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        echo json_encode(array("stories" => $stories, "articles" => $articles));
    }

    public function fepageviewAction($pageslugs) {
        $db = \Phalcon\DI::getDefault()->get('db');

        $condition1 = "pageslugs LIKE'" . $pageslugs . "'" ;
        $page = Pages::findFirst(array($condition1));
        if($page) {
            $pageid = $page->pageid;
        }

        $condition2 = 'pageid="' . $pageid . '"' ;
        $pageleftbar = Pageleftbar::findFirst(array($condition2));
        if($pageleftbar) {
            $sidebarid = $pageleftbar->item;
        }

        $leftsidebar = Leftsidebaritem::find(array("sidebarid =" . $sidebarid . ""));

        $rightsidebar = Pagerightbar::find(array('pageid="' . $pageid . '"'));

        $stmt = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE featurednews = 0 and categoryid != 9 and  status = 1 ORDER BY news.views DESC LIMIT 0,5");
        $stmt->execute();
        $popularnews = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        echo json_encode(array(
            "page" => $page,
            "pageleftbar" => $pageleftbar,
            "leftsidebar" => $leftsidebar->toArray(),
            "rightsidebar" => $rightsidebar->toArray(),
            "popularnews" => $popularnews
        ));
    }
}