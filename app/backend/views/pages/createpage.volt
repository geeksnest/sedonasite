{{ content() }}

<script type="text/ng-template" id="mediagallery.html">
  <div ng-include="'/be/tpl/mediagallery.html'"></div>
</script>

<script type="text/ng-template" id="pagecategoryAdd.html">
  <div ng-include="'/be/tpl/pagecategoryAdd.html'"></div>
</script>

<script type="text/ng-template" id="stylepreview">
  <div ng-include="'/be/tpl/stylepreview.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Create Service</h1>
  <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="savePage(page)" name="formpage">
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">
        <div class="col-sm-8">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Service Information  <span style="color:red"> * </span>
            </div>

              <div class="panel-body">

                Menu Service Category
                <select class="form-control" required="required" ng-model="page.category" >
                  <option value="{[{ data.id }]}" ng-repeat="data in catlist"> {[{ data.title }]} </option>
                  <option value="Others"> Others</option>
                </select>
                <br>

                <div class="form-inline">
                    <a href="" class="btn m-b-xs btn-sm btn-primary btn-addon" ng-click="addcategory()"><i class="fa fa-plus"></i>Add category</a>

                </div>

                <div class="line line-dashed b-b line-lg"></div>

                Title
                <input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.title" required="required" ng-keyup="onpagetitle(page.title)">
                <br>
                <div class="line line-dashed b-b line-lg"></div>
                <b class="pull-left">Service Slugs: </b>
                <input type="text" ng-show="editslug" id="pageslugs" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern pull-left" ng-model="page.slugs" ng-keypress="onslugs(page.slugs)">
                <span ng-bind="page.slugs" class="pull-left mg-left"></span>
                <div ng-show="editslug">
                  <a class="btn btn-danger btn-xs pull-right mg-left" ng-click="cancelpageslug(page.title)">cancel</a>
                  <a class="btn btn-primary btn-xs pull-right mg-left" ng-click="setslug(page.slugs)">ok</a>
                </div>
                <a class="btn btn-danger btn-xs pull-right mg-left" ng-hide="editslug" ng-click="clearslug(page.title)">clear</a>
                <a class="btn btn-primary btn-xs pull-right mg-left" ng-hide="editslug" ng-click="editpageslug()">edit slug</a>

                <div class="line line-dashed b-b line-lg"></div>

                Sub-title
                <input type="text" id="subtitle" name="subtitle" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.subtitle1">
                <!-- Sub-title 2
                <input type="text" id="subtitle" name="subtitle" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.subtitle2"> -->


                <div class="line line-dashed b-b line-lg"></div>

                Button Title
                <input type="text" id="buttontitle" name="buttontitle" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.buttontitle" required>

                <div class="line line-dashed b-b line-lg"></div>

                Body Content
                <a class="btn btn-default btn-xs" ng-click="media('content')"><i class="fa  fa-folder-open"></i> Media Library</a>
                <br><br>
                <textarea class="ck-editor" ng-model="page.body" required="required"></textarea>

              </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Meta  <span style="color:red"> * </span>
            </div>
            <div class="panel-body">
               Title
              <input type="text" id="metatitle" name="metatitle" class="form-control" ng-model="page.metatitle" required="required" placeholder="Meta Title">

              <div class="line line-dashed b-b line-lg"></div>
                Description
              <input input="text"  id="metadesc" name="metadesc"  class="form-control" ng-model="page.metadesc" required="required" placeholder="Meta Description">

              <div class="line line-dashed b-b line-lg"></div>
                Keyword
              <input type="text" id="metatags" name="metatags" class="form-control" ng-model="page.metatags" required="required" placeholder="Meta Keyword">
            </div>
          </div>

        </div>

        <div class="col-sm-4">

          <div class="panel panel-default">
            <div class="panel-heading font-bold">
            Service Banner
              <a class="btn btn-default btn-xs pull-right" ng-click="media('featured','banner')"><i class="fa  fa-folder-open"></i> Media Library</a>
            </div>
            <div class="panel-body">
              <img ng-show="page.banner" src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{ page.banner }]}" style="width: 100%">
              <label><em class="text-muted">Click Media Library to add image or select an existing image.</em></label>
            </div>
          </div>

          <!-- <div class="panel panel-default">
            <div class="panel-heading font-bold">
            Special Page
            </div>
            <div class="panel-body">
              <div class="form-group">
                <div class="col-sm-2">
                  <label class="i-switch i-switch-md bg-info m-t-xs m-r">
                    <input type="checkbox" checked="checked" ng-model="page.specialpage">
                    <i></i>
                  </label>
                </div>
                <div class="col-sm-10">
                  <div ng-if="page.specialpage">
                    <select class="form-control" required="required" ng-model="page.action" ng-change="sample(page.action)">
                      <option value="{[{ data.action }]}" ng-repeat="data in actions"> {[{ data.name }]} </option>
                    </select>
                  </div>
                </div>
              </div>

            </div>
          </div> -->

          <div class="panel panel-default">
            <div class="panel-heading font-bold">Left Sidebar  <span style="color:red"> * </span></div>
              <div class="panel-body">
                Service Price
                <input type="text" id="serviceprice" name="serviceprice" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.serviceprice" required="required" >
              </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              <i class="fa fa-dollar"></i> Prices  <span style="color:red"> * </span>
              <input type="hidden" name="inputprices" ng-model="page.prices" required />
              <a ng-click="addPrice=true;showAdd=true" ng-hide="addPrice"><span class="pull-right"><i class="fa fa-plus-square"></i></span></a>
              <button  type="button" class="btn btn-default btn-xs pull-right" ng-show="addPrice" ng-click="priceCancel()"><i class="glyphicon glyphicon-ban-circle"></i> Cancel</button>
              <button  type="button" class="btn btn-default btn-xs pull-right" ng-show="showUpdate" ng-click=" updatePrice(price)" ng-disabled="!price.desc || !price.amount || price.amount <= 0"><i class="glyphicon glyphicon-edit"></i> Update</button>
              <button class="btn btn-default btn-xs pull-right " type="button" ng-show="addPrice && !showUpdate" ng-click="createPrice(price)" ng-disabled="!price.desc || !price.amount || price.amount <= 0"><i class="glyphicon  glyphicon-plus-sign "></i> Add</button>
            </div>
            <ul class="list-group no-radius">
              <li class="list-group-item" ng-show="addpriceerror">
                <div class="row">
                  <div class="col-md-12">The price per hour/minute value is invalid. Use 3d for 3 Days or 30m for 30 minutes</div>
                </div>
              </li>
              <li class="list-group-item animated fadeInDown" ng-show="addPrice || showUpdate">
                <div class="row">
                  <div class="col-md-6"><input class="form-control " name="desc"  ng-model="price.desc" placeholder="Description" type="text"></div>
                  <div class="col-md-3 padding-left0"><input class="form-control " name="amount" ng-model="price.amount" placeholder="Price" type="text" only-digits></div>
                  <div class="col-md-3 padding-left0"><input class="form-control " name="priceper" ng-model="price.priceper" placeholder="h/m" type="text" value-date-time ></div>
                </div>
              </li>
              <li ng-repeat="p in page.prices track by $index "class="list-group-item service-price" ng-hide="currentEditPrice == $index">
                  <span class="pull-right"> ${[{p.amt}]} ({[{p.price}]}) </span>
                  <span class="service-price"> {[{p.d}]}
                    <span class="price-control-action"><a href="" ng-click="price.desc=p.d; price.amount=p.amt;price.priceper=p.price;showUpdate=true; editPrice($index)"><i class="fa fa-pencil-square"></i></a></span>
                    <span class="price-control-action"><a href="" ng-click="delete($index)"><i class="fa fa-times-circle"></i></a></span>
                  </span>
              </li>
            </ul>
          </div>

          <div ng-if="page.category!='Others'" class="panel panel-success bg-gray lter">
            <div class="panel-heading font-bold">
              Service Landing Page Content  <span style="color:red"> * </span>
              <a class="btn btn-default btn-xs pull-right" ng-click="media('featured','thumb')"><i class="fa  fa-folder-open"></i> Media Library</a>
            </div>
            <div class="panel-body">
              <label><em class="text-muted">Click Media Library to add image or select an existing image. Recommended image size: a ratio of 704px in height.</em></label>
              <img ng-show="page.imagethumb" src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{ page.imagethumb }]}" style="width: 100%">
              <div class="line line-dashed b-b line-lg"></div>

               <input type="hidden" ng-model="page.imagethumb" required> 

              Title (maxlength of 60)
              <input type="text" id="imagethumbsubtitle" name="imagethumbsubtitle" class="form-control" ng-model="page.imagethumbsubtitle" maxlength="60" required>
              <div class="line line-dashed b-b line-lg"></div>

              Short Description (maxlength of 300)
              <textarea type="text" id="thumbdesc" name="thumbdesc" class="form-control resize-v" ng-model="page.thumbdesc" maxlength="300" rows="4" required> </textarea>
              <div class="line line-dashed line-lg b-b"></div>

              <div class="form-group" ng-init="page.align = 'center' ">
                <div class="col-sm-3">Text Align</div>
                <div class="col-sm-9 wrapper-sm">
                  <div class="btn-group">
                    <label class="btn btn-primary" ng-model="page.align" btn-radio="'left'">Left</label>
                    <label class="btn btn-primary" ng-model="page.align" btn-radio="'center'">Center</label>
                    <label class="btn btn-primary" ng-model="page.align" btn-radio="'right'">Right</label>
                  </div>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg"></div>

              <div class="form-group">
                <div class="col-sm-3">Background Color</div>
                <div class="col-sm-9 well bg-light wrapper-sm">
                  <input colorpicker ng-model="page.bgcolor" ng-init="page.bgcolor = '#ffffff' " type="text" class="form-control width90px pull-left pointer" required="required" readonly>
                  <div class="colorpicker-colorbox form-control pull-left" style="background-color: {[{ page.bgcolor }]}"></div>
                </div>
              </div>
              <div class="line line-dashed line-lg b-b"></div>

              <div class="form-group">
                <div class="col-sm-3">Text Color</div>
                <div class="col-sm-9 well bg-light wrapper-sm">
                  <input colorpicker ng-model="page.color" type="text" ng-init="page.color = '#000000' " class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern width90px pull-left pointer" required="required" readonly>
                  <div class="colorpicker-colorbox form-control pull-left" style="background-color: {[{ page.color }]}"></div>
                </div>
              </div>

              <div class="line line-dashed b-b line-lg"></div>

              <div class="form-group">
                <div class="col-sm-3">White Box</div>
                <div class="col-sm-9 well bg-light wrapper-sm" ng-init="page.box = false ">
                  <label class="i-switch bg-info m-t-xs m-r">
                    <input type="checkbox" ng-model="page.box">
                    <i></i>
                  </label>
                  <span ng-if="page.box == false" class="label bg-danger Services-checkbox-label">off</span>
                  <span ng-if="page.box == true" class="label bg-success Services-checkbox-label">on</span>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-3">Featured</div>
                <div class="col-sm-9 well bg-light wrapper-sm">
                  <label class="i-switch bg-info m-t-xs m-r">
                    <input type="checkbox" ng-model="page.featured">
                    <i></i>
                  </label>
                  <span ng-if="page.featured == true" class="label bg-success Services-checkbox-label">This will show on Homepage</span>
                </div>
              </div>
              <div class="line line-dashed line-lg b-b"></div>

              <div class="form-group" ng-init="page.banneropt = 'regular' ">
                <div class="col-sm-3">Banner Size</div>
                <div class="col-sm-9 wrapper-sm">
                  <div class="btn-group">
                    <label class="btn btn-default" ng-model="page.banneropt" btn-radio="'regular'">Regular</label>
                    <label class="btn btn-default" ng-model="page.banneropt" btn-radio="'custom'">Customize</label>
                  </div>
                </div>
                <div ng-if="page.banneropt=='custom'">
                  <div class="col-sm-4">
                    <input type="text" class="form-control" value="1920" disabled="">
                  </div>
                  <div class="col-sm-1">
                    x
                  </div>
                  <div class="col-sm-4">
                    <input type="number" class="form-control" ng-model="page.bannersize">
                  </div>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg"></div>

          

              <div class="form-group">
                <div class="col-sm-12">
                  <tabset class="tab-container tab-danger">
                    <tab>
                       <tab-heading class="font-bold">Note</tab-heading>
                       Please click <a class="btn m-b-xs btn-xs btn-success" ng-click="stylepreview(page)">Quick Preview</a>
                       before submitting so you can check whether the title and description you inserted is too long. Thank you.
                    </tab>
                  </tabset>
                </div>
              </div>

            </div>
          </div>

          <!-- <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Sidebar Switch
            </div>

            <div class="panel-body">

              <div class="radio">
                <label class="i-checks">
                  <input type="radio" name="a" ng-model="page.layout" value="1" ng-click="radio('1')" required="required" checked="true">
                  <i></i>
                  Full Page Layout
                </label>
              </div>

              <div class="radio">
                <label class="i-checks">
                  <input type="radio" name="a" ng-model="page.layout" value="2" ng-click="radio('2')" required="required">
                  <i></i>
                  Page with Sidebar Layout
                </label>
              </div>
            </div>
          </div> -->

        <!-- <div class="col-sm-3" ng-show="sidebar">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Left Sidebar
            </div>

              <div class="panel-body">
                Service Price
                <input type="text" id="serviceprice" name="serviceprice" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.serviceprice" required="required" >
              </div>

          </div>
        </div>  -->
        </div>
      </div>

      <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-right bg-light lter">
              <a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
              <button type="submit" class="btn btn-success" ng-disabled="formpage.$invalid || page.prices.length == 0">Submit</button>
            </footer>
        </div>
      </div>
  </div>
</fieldset>
</form>
