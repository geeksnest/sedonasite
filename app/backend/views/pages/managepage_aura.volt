{{ content() }}
<script type="text/ng-template" id="pageDelete.html">
<div ng-include="'/be/tpl/pageDelete.html'"></div>
</script>
<script type="text/ng-template" id="pageEdit.html">
<div ng-include="'/be/tpl/pageEdit.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Service List</h1>
  <a id="top"></a>
</div>
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
  <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

<div class="row wrapper">
  <div class="col-sm-5 m-b-xs" ng-show="keyword">
    <strong>{[{ bigTotalItems }]}</strong> Results found for: <strong> "{[{ keyword }]}"</strong> <button class="btn btn-default btn-xs" ng-click="clear()">Clear</button>
  </div>
  <div class="col-sm-5 m-b-xs pull-right">
    <div class="input-group">
      <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
      <span class="input-group-btn">
        <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
      </span>
    </div>
    <br>
    <div class="input-group" style="width:100%">
      Sort by:
      <div class="pull-right">
        <label class="radio-inline">
          <input type="radio" ng-model="sort" ng-click="sortpage(sort)" value="title"> Title
        </label>
        <label class="radio-inline">
          <input type="radio" ng-model="sort" ng-click="sortpage(sort)" value="pagecreated_at"> Date created
        </label>
        <label class="radio-inline">
          <input type="radio" ng-model="sort" ng-click="sortpage(sort)" value="pageupdated_at"> Date updated
        </label>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <ul class="timeline">
    <li class="tl-header">
      <div class="btn btn-primary">Date Created</div>
    </li>

    <li class="tl-item tl-left" ng-repeat="aura in data.data">
      <div ng-if="aura.status == 1" class="tl-wrap b-primary">
        <span class="tl-date" ng-bind="aura.pagecreated_at"></span>
        <div class="tl-content panel padder b-a block">
          <span class="arrow left pull-up hidden-left"></span>
          <span class="arrow right pull-up visible-left"></span>
          <div class="text-lt m-b-sm font-bold">
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-stack-1x text-white" ng-bind="aura.title | limitTo: 1 | uppercase"></i>
            </span>
            <span ng-bind="aura.title"></span>
            <span class="pull-right">
              <a ng-click="editpage(aura.pageid)"><span class="btn btn-primary btn-xs" >Edit</span></a>
              <a ng-click="deletepage(aura.pageid)"> <span class="btn btn-primary btn-xs">Delete</span></a>
            </span>
          </div>
          <div class="panel-body pull-in b-t b-light">
            <label class="font-bold">Status:</label> <span class="badge bg-primary">Active</span> <br>
            <label class="font-bold">Short Description:</label> <span ng-bind="aura.thumbdesc"></span>
          </div>
        </div>
      </div>

      <div ng-if="aura.status == 0" class="tl-wrap b-default">
        <span class="tl-date" ng-bind="aura.pagecreated_at"></span>
        <div class="tl-content panel padder b-a block">
          <span class="arrow left pull-up hidden-left"></span>
          <span class="arrow right pull-up visible-left"></span>
          <div class="text-lt m-b-sm font-bold">
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-stack-1x text-white" ng-bind="aura.title | limitTo: 1 | uppercase"></i>
            </span>
            <span ng-bind="aura.title"></span>
            <span class="pull-right">
              <a ng-click="editpage(aura.pageid)"><span class="btn btn-default btn-xs" >Edit</span></a>
              <a ng-click="deletepage(aura.pageid)"> <span class="btn btn-default btn-xs">Delete</span></a>
            </span>
          </div>
          <div class="panel-body pull-in b-t b-light">
            <label class="font-bold">Status:</label> <span class="badge bg-gray text-dark">Inactive</span> <br>
            <label class="font-bold">Short Description:</label> <span ng-bind="aura.thumbdesc"></span>
          </div>
        </div>
      </div>
    </li>
  </ul>
</div>
<div class="row" ng-hide="bigTotalItems==0 || loading">
  <div class="panel-body">
    <footer class="panel-footer text-center bg-light lter">
      <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
    <pagination ng-hide="maxSize >= bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
    </footer>
  </div>
</div>
</div>
</fieldset>
