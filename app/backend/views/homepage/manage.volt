{{ content() }}
<style>
.ssortable{
    overflow-y: auto;
    overflow-x: hidden;
}
</style>
<script type="text/ng-template" id="homepageDelete.html">
  <div ng-include="'/be/tpl/homepageDelete.html'"></div>
</script>
<script type="text/ng-template" id="homepageUpdate.html">
  <div ng-include="'/be/tpl/homepageUpdate.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Manage Homepage</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
  <div class="panel panel-default">
    <div class="panel-heading">
      Manage Homepage
    </div>
    <div class="row wrapper">
          <div class="col-sm-8 m-b-xs" ng-show="keywordshow">
              <strong class="ng-binding">{[{ total_items }]}</strong> Results found for: "{[{ keyword }]}" <button class="btn btn-default btn-xs" ng-click="clear()">Clear</button>
          </div>
          <div class="col-sm-4 pull-right">
            <div class="input-group">
              <input class="input-sm form-control" ng-init="searchtext == undefined" placeholder="Search" type="text" name="searchtext" ng-model="searchtext">
              <span class="input-group-btn">
                <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
              </span>
            </div>

            <div class="col-sm-3">
            </div>
          </div>
            <form name="Form">
      <table class="table table-striped b-t b-light ssortable" ui:sortable ng:model="data.data">
        <thead>
          <tr>
            <th>Title</th>
            <th>Button Name</th>
            <th>Button Link</th>
            <th>Text Position</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody class="table table-striped b-t b-light ssortable" ui:sortable ng:model="data">

          <tr ng-repeat="content in data track by $index" data-id="data[$index].id">
            <td>{[{ content.title }]}</td>
            <td>{[{ content.btnname }]}</td>
            <td>{[{ content.btnlink }]}</td>
            <td>{[{ content.textposition }]}</td>
            <td>
              <button ng-click="update(content.id)" class="btn m-b-xs btn-xs btn-info btn-addon">Edit</button>
              <button ng-click="delete(content.id)" class="btn m-b-xs btn-xs btn-danger btn-addon">Delete</button>
            </td>
          </tr>

        </tbody>
      </table>
      
    </form>
    </div>

  
    <div class="col-sm-12 m-b-xs">
     <button class="btn btn-success btn-addon btn-lg" ng-click="shit(data)"><i class="fa fa-plus"></i>Update</button>
    </div>
    <div class="col-sm-12 m-b-xs">
    <footer class="panel-footer">
      <div class="row">
        <div class="row" ng-hide="bigTotalItems==0 || loading">
            <div class="panel-body">
                <footer class="panel-footer text-center bg-light lter">
                    <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
                    <pagination ng-hide="maxSize >= bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
                </footer>
            </div>
        </div>
      </div>
    </footer>
    </div>
  </div>
</div>
