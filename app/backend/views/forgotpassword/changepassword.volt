<!DOCTYPE html>
<html lang="en" data-ng-app="app">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">

  <!-- Title and other stuffs -->
  {{ get_title() }}
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="/img/frontend/favicon.gif" type='image/x-icon'/>
  <!-- Stylesheets -->
  {{ stylesheet_link('be/css/bootstrap.css') }}
  {{ stylesheet_link('be/css/animate.css') }}
  {{ stylesheet_link('be/css/font-awesome.min.css') }}
  {{ stylesheet_link('be/css/simple-line-icons.css') }}
  {{ stylesheet_link('be/css/font.css') }}
  {{ stylesheet_link('be/css/app.css') }}

  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="be/js/html5shim.js"></script>
  <![endif]-->


  <link rel="shortcut icon" href="/img/favicon/favicon.ico">
    <style type="text/css">
  .tokenexpired
  {
    color:#F00;

  }
  .login {
    font-size: 15px;
    font-weight: bold;
  }
  </style>
</head>

<body ng-controller="Changepasswordctrl">
  <div class="container w-xxl w-auto-xs">
    <a href class="navbar-brand block m-t">Sedona Healing Arts</a>
    <div class="m-b-lg">
      <div class="wrapper text-center">
        <img src='/img/frontend/sedona_logo.png' style="width:100%;height:auto;">
      </div>
      <br>
        <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)"><span ng-bind-html="alert.msg"></span></alert>       <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="send(forgot)" name="formNews" id="formNews" ng-hide="hideform" >
        <h4>Please Enter your new password</h4>
          <div class="list-group list-group-sm">
            <input type="hidden" ng-model="forgot.email"  ng-init="forgot.email='<?php echo $email; ?>'">
            <input type="hidden" ng-model="forgot.token" ng-init="forgot.token='<?php echo $token; ?>'">
            <div class="list-group-item">
              <input type="password" id="password" class="form-control no-border" ng-model="forgot.password" name="forgot.password" required="required" placeholder="Password" size="30">
            </div>
            <div class="list-group-item">
              <input type="password" id="repassword" class="form-control no-border" ng-model="forgot.repassword" name="forgot.repassword" required="required" placeholder="Re-Type Password" size="30" ng-change="onpassword(forgot)">
            </div>
          </div>
          <button type="submit" class="btn btn-lg btn-primary btn-block" ng-disabled='form.$invalid'>Reset password</button>
          <div class="line line-dashed"></div>
        </form>
      <p class="text-center"><small>Dont share your password to anyone.</small></p>
    </div>
  </div>
<!-- JS -->
{{ javascript_include('be/js/jquery/jquery.min.js') }}
<!-- angular -->
{{ javascript_include('be/js/angular/angular.min.js') }}
{{ javascript_include('be/js/angular/ui-bootstrap-tpls.min.js') }}
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular-sanitize.js"></script>

</body>
</html>

<script type="text/javascript">
'use strict';

var app = angular.module('app', [
    'ui.bootstrap',
    'ngSanitize'
    ])
  .config(function ($interpolateProvider){

     $interpolateProvider.startSymbol('{[{');
     $interpolateProvider.endSymbol('}]}');

   })
</script>
{{ javascript_include('be/js/scripts/config.js') }}

<!-- APP -->
<script type="text/javascript">
    app.controller('Changepasswordctrl', function ($scope, $http,Config){
        var emailnull = "<?php echo $email ?>";
        var tokennull = "<?php echo $token ?>";
        if(emailnull=="" || tokennull==""){
          window.location.replace("/sedonaadmin");
        }
        $scope.alerts = [];
        $scope.closeAlert = function(index){
          $scope.alerts.splice(index, 1);
        }
        $scope.hideform = true;

        $http({
          url:Config.ApiURL +"/checktoken/check/<?php echo $email; ?>/<?php echo $token; ?>",
          method: "get",
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        }).success(function (data, status, headers, config) {
          $scope.alerts.splice(0, 1);
          console.log(data.valid);
          if(data.valid == true){
            $scope.hideform = false;
          }else {
            $scope.hideform = true;
            $scope.alerts.push({ type : "danger", msg : "Token has expired."});
          }
        }).error(function(data, status, headers, config) {
          $scope.alerts.splice(0,1);
          $scope.alerts.push({ type : "danger", msg : "Error"});
        });

        $scope.onpassword = function(forgot){
          if(forgot.password != forgot.repassword){
            $scope.alerts.splice(0, 1);
            $scope.alerts.push({ type : "danger", msg : "Password did not match"});
          }else {
            $scope.alerts.splice(0, 1);
          }
        }

        $scope.send = function(forgot) {
        $http({
            url:Config.ApiURL +"/forgotpassword/updatepassword",
            method: "POST",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: $.param(forgot)
        }).success(function(data, status, headers, config) {
            $scope.hideform = true;
            console.log(data);
            $scope.alerts.push({ type : data.type, msg : data.msg });
        }).error(function(data, status, headers, config) {
            $scope.alerts.push({ type : "danger", msg : "Error" });
        });
    };
  });
  </script>
