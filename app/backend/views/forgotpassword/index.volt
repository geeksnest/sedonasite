<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  {{ get_title() }}
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <!-- Stylesheets -->
  {{ stylesheet_link('be/css/bootstrap.css') }}
  {{ stylesheet_link('be/css/animate.css') }}
  {{ stylesheet_link('be/css/font-awesome.min.css') }}
  {{ stylesheet_link('be/css/simple-line-icons.css') }}
  {{ stylesheet_link('be/css/font.css') }}
  {{ stylesheet_link('be/css/app.css') }}
  
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="be/js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon 
  <link rel="shortcut icon" href="{{url.getBaseUri()}}public/img/favicon/favicon.png">-->
</head>

<body>
  <div class="container w-xxl w-auto-xs" ng-controller="Forgotpasswordctrl">
  <a href class="navbar-brand block m-t">Sedona Healing Arts</a>
  <div class="m-b-lg">
    <div class="wrapper text-center">
      <img src='/img/frontend/sedona_logo.png' style="width:100%;height:auto;">
    </div>
    <br>
    <form name="form" class="form-validation" method="post" ng-submit="send(data)" >
      <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="list-group-item">
        {{ text_field('email', 'size': "30", 'class': "form-control no-border", 'id': "email", 'required':'required', 'placeholder' : 'Enter your email address', 'ng-model' : 'data.email') }}
      </div>
      <br>
      <button type="submit" class="btn btn-lg btn-primary btn-block" ng-disabled='form.$invalid'>Reset password</button>
      <br>
      <p class="text-center"><small>Dont share your password to anyone.</small></p>
    </form>
  </div>
  <!-- <div class="text-center" ng-include="'tpl/blocks/page_footer.html'"></div> -->
</div>
<!-- JS -->
{{ javascript_include('be/js/jquery/jquery.min.js') }}
<!-- angular -->
{{ javascript_include('be/js/angular/angular.min.js') }}
{{ javascript_include('be/js/angular/angular-cookies.min.js') }}
{{ javascript_include('be/js/angular/angular-animate.min.js') }}
{{ javascript_include('be/js/angular/angular-ui-router.min.js') }}
{{ javascript_include('be/js/angular/angular-translate.js') }}
{{ javascript_include('be/js/angular/ngStorage.min.js') }}
{{ javascript_include('be/js/angular/ui-load.js') }}
{{ javascript_include('be/js/angular/ui-jq.js') }}
{{ javascript_include('be/js/angular/ui-validate.js') }}
{{ javascript_include('be/js/angular/ui-bootstrap-tpls.min.js') }}


<!-- APP -->
 <script type="text/javascript">
'use strict';

var app = angular.module('app', [
    'ui.bootstrap'
    ])
  .config(function ($interpolateProvider){

     $interpolateProvider.startSymbol('{[{');
     $interpolateProvider.endSymbol('}]}');

   })
  .controller('Forgotpasswordctrl', function ($scope,$http, Config){
     $scope.alerts = [];
     $scope.closeAlert = function (index) {
         $scope.alerts.splice(index, 1);
     };

     $scope.send = function(data){
        $http({
          url: Config.ApiURL +"/forgotpassword/reset",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(data)
        }).success(function (data, status, headers, config) {
          $scope.alerts.splice(0,1);
          $scope.alerts.push({'type': data.type, 'msg': data.msg});
        }).error(function(data, status, headers, config) {
          $scope.alerts.splice(0,1);
          $scope.alerts.push({'type': data.type, 'msg': data.msg});
        });
      }   

  });
</script>
{{ javascript_include('be/js/scripts/config.js') }}

</body>
</html>