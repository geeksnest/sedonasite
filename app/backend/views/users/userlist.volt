{{ content() }}
<script type="text/ng-template" id="userDelete.html">
  <div ng-include="'/be/tpl/userDelete.html'"></div>
</script>
<script type="text/ng-template" id="userUpdate.html">
  <div ng-include="'/be/tpl/userUpdate.html'"></div>
</script>
<script type="text/ng-template" id="deleteUsers.html">
  <div ng-include="'/be/tpl/deleteUsers.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">User List</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
  <div class="panel panel-default">
    <div class="panel-heading">
      Manage Users
    </div>
    <div class="row wrapper">
          <div class="col-sm-8 m-b-xs" ng-show="keywordshow">
              <strong class="ng-binding">{[{ total_items }]}</strong> Results found for: "{[{ keyword }]}" <button class="btn btn-default btn-xs" ng-click="clear()">Clear</button>
          </div>
          <div class="col-sm-4 pull-right">
            <div class="input-group">
              <input class="input-sm form-control" ng-init="searchtext == undefined" placeholder="Search" type="text" name="searchtext" ng-model="searchtext">
              <span class="input-group-btn">
                <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
              </span>
            </div>

            <div class="col-sm-3">
            </div>
          </div>
    </div>

    <form name="userlistForm" >
     <!--  <input type="hidden" ng-init='userdata = {{ data }}'> -->
      <table class="table table-striped b-t b-light">
        <thead>
          <tr>
            <th style="width:20px;">
              <label class="i-checks m-b-none">
                <input type="checkbox" id="all" ng-model="checkedAll" ng-click="toggleCheckAll()"><i></i>
              </label>
            </th>
            <th>Name</th>
            <th>Email</th>
            <th>Username</th>
            <th>Status</th>
            <th>User Role</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="user in data.data">
            <td><label class="i-checks m-b-none"><input type="checkbox" value="{{user.id}}" ng-model="user.checked" ng-click="usersArray(user)"><i></i></label></td>
            <td>{[{ user.fname }]} {[{ user.lname }]}</td>
            <td>{[{ user.email }]}</td>
            <td>{[{ user.username }]}</td>
            <td  ng-if="user.status == 1">
              <div class="pagestatuscontent fade-in-out"><span class="label bg-success" >Active</span></div>
              <div class="checkstatuscontent">
                <label class="i-switch bg-info m-t-xs m-r">
                  <input type="checkbox" checked="" ng-click="setstatus(user.status, user.id, searchtext)">
                  <i></i>
                </label>

              </div>
              <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == user.id"><i class="fa fa-check"></i></spand></div>
            </td>
            <td  ng-if="user.status == 0">
              <div class="pagestatuscontent fade-in-out"><span class="label bg-danger">Deactivated</span></div>
              <div class="checkstatuscontent">
                <label class="i-switch bg-info m-t-xs m-r">
                  <input type="checkbox" ng-click="setstatus(user.status, user.id, searchtext)">
                  <i></i>
                </label>

              </div>
               <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == user.id"><i class="fa fa-check"></i></spand></div>
            </td>
            <td><span ng-class="">{[{ user.userrole }]}</span></td>
            <td>
              <button ng-click="update(user.id)" class="btn m-b-xs btn-xs btn-info btn-addon">Edit</button>
              <button ng-click="delete(user.id)" class="btn m-b-xs btn-xs btn-danger btn-addon">Delete</button>
            </td>
          </tr>
          <tr>
            <td>
              <label class="i-checks m-b-none">
                <input type="checkbox" id="all" ng-model="checkedAll" ng-change="toggleCheckAll()"><i></i>
              </label>
            </td>
            <td>
               <button class="btn btn-danger btn-xs" ng-disabled="useridArray==0" ng-click="deleteUsers()">Delete</button>
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        </tbody>
      </table>
      
    </form>

    <footer class="panel-footer">
      <div class="row">
        <!--<div class="col-sm-4 text-left text-center-xs">
          <ul class="pagination">
            <li id="DataTables_Table_0_previous" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == 1  }" class="paginate_button previous">
              <a class="fa fa-chevron-left" href="" ng-click="numpages(data.before)"></a>
            </li>
            <li ng-repeat="page in data.pages" ng-class="{ 'active' : data.index == page.num  }" class="paginate_button">
              <a ng-click="numpages(page.num)"> {[{ page.num }]}</a>
            </li>
            <li id="DataTables_Table_0_next" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == data.last  }" class="paginate_button next">
              <a href="" class="fa fa-chevron-right" ng-click="numpages(data.next)"> </a>
            </li>
          </ul>
        </div>
        <div class="col-sm-4 text-center">
          <small class="text-muted inline m-t-sm m-b-sm">showing {[{ current.page }]} - {[{ current.total }]} of {[{ current.total }]} items</small>
        </div>-->
        <div class="row" ng-hide="bigTotalItems==0 || loading">
            <div class="panel-body">
                <footer class="panel-footer text-center bg-light lter">
                    <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
                    <pagination ng-hide="maxSize >= bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
                </footer>
            </div>
        </div>
      </div>
    </footer>
  </div>
</div>
