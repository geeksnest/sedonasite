{{ content() }}
<script type="text/ng-template" id="homepageDelete.html">
  <div ng-include="'/be/tpl/homepageDelete.html'"></div>
</script>
<script type="text/ng-template" id="homepageUpdate.html">
  <div ng-include="'/be/tpl/homepageUpdate.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Manage Page</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
  <div class="panel panel-default">
    <div class="panel-heading">
      Manage Page
    </div>
    <div class="row wrapper">
          <div class="col-sm-8 m-b-xs" ng-show="keyword">
              <strong class="ng-binding">{[{ total_items }]}</strong> Results found for: "{[{ keyword }]}" <button class="btn btn-default btn-xs" ng-click="clear()">Clear</button>
          </div>
          <div class="col-sm-4 pull-right">
            <div class="input-group">
              <input class="input-sm form-control" ng-init="searchtext == undefined" placeholder="Search" type="text" name="searchtext" ng-model="searchtext">
              <span class="input-group-btn">
                <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
              </span>
            </div>

            <div class="col-sm-3">
            </div>
          </div>
    </div>

    <form name="Form">
      <table class="table table-striped b-t b-light">
        <thead>
          <tr>
            <th>Title</th>
            <th>Page Type</th>
            <th>Page Slugs</th>
            <th>Status</th>
            <th>Date Created</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="content in data">
            <td>{[{ content.title }]}</td>
            <td>{[{ content.pagetype }]}</td>
            <td>{[{ content.slugs }]}</td>
            <td ng-if="content.status == 1">
                    <div class="pagestatuscontent fade-in-out"><span class="label bg-success" >Active</span></div>
                    <div class="checkstatuscontent">
                      <label class="i-switch bg-info m-t-xs m-r">
                        <input type="checkbox" checked="" ng-click="setstatus(content.status,content.pageid,searchtext)">
                        <i></i>
                      </label>
                      
                    </div>
                    <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == content.pageid"><i class="fa fa-check"></i></spand></div>
                  </td>
                  <td ng-if="content.status == 0">
                    <div class="pagestatuscontent fade-in-out"><span class="label bg-danger">Deactivated</span></div>
                    <div class="checkstatuscontent">
                      <label class="i-switch bg-info m-t-xs m-r">
                        <input type="checkbox" ng-click="setstatus(content.status,content.pageid,searchtext)">
                        <i></i>
                      </label>

                    </div>
                    <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == content.pageid"><i class="fa fa-check"></i></spand></div>
                  </td>

             <td>{[{ content.datecreated }]}</td>
            <td>
              <button ng-click="update(content.pageid)" class="btn m-b-xs btn-xs btn-info btn-addon">Edit</button>
              <button ng-click="delete(content.pageid)" class="btn m-b-xs btn-xs btn-danger btn-addon">Delete</button>
            </td>
          </tr>
        </tbody>
      </table>
      
    </form>

    <footer class="panel-footer">
      <div class="row">
        <div class="row" ng-hide="bigTotalItems==0 || loading">
            <div class="panel-body">
                <footer class="panel-footer text-center bg-light lter">
                    <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
                    <pagination ng-hide="maxSize >= bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
                </footer>
            </div>
        </div>
      </div>
    </footer>
  </div>
</div>
