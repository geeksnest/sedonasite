{{ content() }}

<script type="text/ng-template" id="pageimagelist.html">
  <div ng-include="'/be/tpl/pageimagelist.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Offline Order</h1>
  <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="savePage(page)" name="formpage" ng-init="centerid='<?php echo $username['bnb_centerid']; ?>'">
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
    <tabset class="tab-container" ng-init="steps={percent:20, step1:true, step2:false, step3:false, step4: false, step5:false}; view=false">

      <tab heading="Add Product" active="steps.step1" select="steps.percent=10">
        <p class="m-b">Please add atleast one product.</p>
        <progressbar value="steps.percent" class="progress-xs" type="success"></progressbar>

        <div class="row">
          <div class="col-sm-4 m-b-xs">
            <span class="label label-danger" ng-if="error.product">This field is required.</span>
            <div class="input-group">
              <!-- <div ui-module="select2">
                <select ui-select2 id="selectproduct" ng-model="selected" class="form-control" required="required">
                  <option ng-repeat="product in products">{[{product.name}]}</option>
                </select>
              </div> -->
              <select ng-model="selected" class="form-control" required="required">
                  <option ng-repeat="product in products">{[{product.name}]}</option>
              </select>
              <span class="input-group-btn">
                <button class="btn btn-default" type="button" ng-click="addproduct(selected, $event)">Add Product</button>
              </span>
            </div>
          </div>
        </div>

        <div class="table-responsive">
          <table class="table table-striped b-t b-light">
              <thead>
                  <tr>
                    <th>Product</th>
                    <th>Price</th>
                    <th style="width:10%">Quantity</th>
                    <th>Total Price</th>
                    <th>Action</th>
                  </tr>
              </thead>
              <tbody>
                <tr ng-repeat="mem in order">
                  <td>{[{ mem.name }]}</td>
                  <td>{[{ mem.price | currency }]}</td>
                  <td>
                      <input ui-jq="TouchSpin" type="text" name="quantity" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern input-sm" data-min='1' data-verticalbuttons="true" data-verticalupclass="fa fa-caret-up" ng-model="mem.orderquantity" data-verticaldownclass="fa fa-caret-down" required="required" data-min='1' data-max="{[{ mem.maxquantity }]}" ng-change="onqty(mem.productid)" min="1" max="{[{mem.maxquantity}]}" only-digits>
                      <p class="qty">There are only {[{ mem.quantity}]}qty. remaining.</p>
                  </td>
                  <td>{[{ mem.total | currency }]}</td>
                  <td>
                    <a href="" ng-click="remove(mem.productid)">
                      <span class="label bg-danger">Remove</span>
                    </a>
                  </td>
                </tr>
              </tbody>
          </table>
        </div>
        <div class="row">
          <div class="col-sm-offset-10 col-sm-2">
            <button class="btn btn-primary btn-rounded pull-right" type="button" ng-disabled="order.length < 1" ng-click="steps.step2 = true">Save and Continue</button>
          </div>
        </div>
      </tab>

      <tab heading="Account & Billing Information" disabled="order.length < 1" active="steps.step2" select="steps.percent=40">
        <div ng-form name="step2" class="form-horizontal">
          <progressbar value="steps.percent" class="progress-xs" type="success"></progressbar>

          <div class="form-group hiddenoverflow">
            <label class="col-lg-2 control-label text-bold">Account Information:</label>
            <div class="col-lg-6">

            </div>
          </div>

          <div class="form-group hiddenoverflow">
            <label class="col-lg-2 control-label">Email *</label>
            <div class="col-lg-6">
              <span class="label label-danger" ng-if="error.email">This field is required.</span>
              <input type="email" ng-change="onemail(billingaddr.email)" class="form-control" ng-model="billingaddr.email" placeholder="Email" required>
            </div>
          </div>

          <div class="form-group hiddenoverflow" ng-if="disabled">
            <label class="col-lg-2 control-label"></label>
            <div class="col-lg-6">
              <p>Do you want to use this as Billing Information?</p>
              <button class="btn btn-primary btn-rounded" ng-click="yes()" type="button">Yes</button>
              <button class="btn btn-default btn-rounded" ng-click="no()" type="button">No</button>
            </div>
          </div>

          <div class="form-group hiddenoverflow">
            <label class="col-lg-2 control-label text-bold">Billing Information:</label>
            <div class="col-lg-6">

            </div>
          </div>

          <div class="form-group hiddenoverflow">
            <label class="col-lg-2 control-label">First name *</label>
            <div class="col-lg-6">
              <span class="label label-danger" ng-if="error.firstname">This field is required.</span>
              <input type="text" class="form-control" ng-model="billingaddr.firstname" ng-disabled="disabled" ng-change="change(billingaddr.firstname, 'firstname')" required>
            </div>
          </div>

          <div class="form-group hiddenoverflow">
            <label class="col-lg-2 control-label">Last name *</label>
            <div class="col-lg-6">
              <span class="label label-danger" ng-if="error.lastname">This field is required.</span>
              <input type="text" class="form-control" ng-model="billingaddr.lastname" ng-disabled="disabled" ng-change="change(billingaddr.lastname, 'lastname')" required>
            </div>
          </div>

          <div class="form-group hiddenoverflow">
            <label class="col-lg-2 control-label">address *</label>
            <div class="col-lg-6">
              <span class="label label-danger" ng-if="error.address">This field is required.</span>
              <input type="text" class="form-control" ng-model="billingaddr.address" ng-disabled="disabled" ng-change="change(billingaddr.address, 'address')" required>
            </div>
          </div>

          <div class="form-group hiddenoverflow">
            <label class="col-lg-2 control-label">address 2 </label>
            <div class="col-lg-6">
              <input type="text" class="form-control" ng-model="billingaddr.address2" ng-disabled="disabled">
            </div>
          </div>

          <div class="form-group hiddenoverflow">
            <label class="col-lg-2 control-label">City *</label>
            <div class="col-lg-6">
              <span class="label label-danger" ng-if="error.city">This field is required.</span>
              <input type="text" class="form-control" ng-model="billingaddr.city" ng-disabled="disabled" ng-change="change(billingaddr.city, 'city')" required>
            </div>
          </div>

          <div class="form-group hiddenoverflow">
            <label class="col-lg-2 control-label">Country *</label>
            <div class="col-lg-6">
              <span class="label label-danger" ng-if="error.country">This field is required.</span>
              <div ng-model="billingaddr.country" ng-change="change(billingaddr.country, 'country')" country-select></div>
            </div>
          </div>

          <div class="form-group hiddenoverflow">
            <label class="col-lg-2 control-label">State/Province *</label>
            <div class="col-lg-6">
              <span class="label label-danger" ng-if="error.state">This field is required.</span>
              <div country="{[{ billingaddr.country }]}" ng-model="billingaddr.state" ng-change="change(billingaddr.state, 'state')" state-select></div>
            </div>
          </div>

          <div class="form-group hiddenoverflow">
            <label class="col-lg-2 control-label">Zip/Postal Code</label>
            <div class="col-lg-6">
              <input type="text" class="form-control" ng-model="billingaddr.zipcode" ng-disabled="disabled">
            </div>
          </div>

          <div class="form-group hiddenoverflow">
            <label class="col-lg-2 control-label">Phone number</label>
            <div class="col-lg-6">
              <input type="text" class="form-control" ng-model="billingaddr.phoneno" allow-pattern="\d" ng-disabled="disabled">
            </div>
          </div>

          <div class="row">
            <div class="col-sm-offset-10 col-sm-2">
              <button class="btn btn-primary btn-rounded pull-right" type="button" id="shipping" ng-disabled="step2.$invalid" ng-click="click()">Save and Continue</button>
            </div>
          </div>
        </div>
      </tab>

      <tab heading="Shipping Address (Optional)" disabled="step2.$invalid" active="steps.step3" select="steps.percent=60">
        <div ng-form name="step3" class="form-horizontal">
          <progressbar value="steps.percent" class="progress-xs" type="success"></progressbar>
          <p>Shipping address is optional if you are going to deliver the item/s</p>
          <div class="form-group hiddenoverflow">
            <label class="col-lg-2 control-label"></label>
            <div class="col-lg-6">
              <div class="checkbox">
                <label class="i-checks">
                  <input type="checkbox" checked ng-model="use" ng-change="usebillingaddr(use)"><i></i> use billing address
                </label>
              </div>
            </div>
          </div>

          <div class="form-group hiddenoverflow">
            <label class="col-lg-2 control-label">First name <span ng-if="shippingaddr.firstname">*</span></label>
            <div class="col-lg-6">
              <input type="text" class="form-control" ng-model="shippingaddr.firstname" ng-disabled="disabled" ng-required="shippingaddr.firstname">
            </div>
          </div>

          <div class="form-group hiddenoverflow">
            <label class="col-lg-2 control-label">Last name <span ng-if="shippingaddr.firstname">*</span></label>
            <div class="col-lg-6">
              <input type="text" class="form-control" ng-model="shippingaddr.lastname" ng-disabled="disabled" ng-required="shippingaddr.firstname">
            </div>
          </div>

          <div class="form-group hiddenoverflow">
            <label class="col-lg-2 control-label">address <span ng-if="shippingaddr.firstname">*</span></label>
            <div class="col-lg-6">
              <input type="text" class="form-control" ng-model="shippingaddr.address" ng-disabled="disabled" required>
            </div>
          </div>

          <div class="form-group hiddenoverflow">
            <label class="col-lg-2 control-label">address 2</label>
            <div class="col-lg-6">
              <input type="text" class="form-control" ng-model="shippingaddr.address2" ng-disabled="disabled" required>
            </div>
          </div>

          <div class="form-group hiddenoverflow">
            <label class="col-lg-2 control-label">City <span ng-if="shippingaddr.firstname">*</span></label>
            <div class="col-lg-6">
              <input type="text" class="form-control" ng-model="shippingaddr.city" ng-disabled="disabled" ng-required="shippingaddr.firstname">
            </div>
          </div>

          <div class="form-group hiddenoverflow">
            <label class="col-lg-2 control-label">Country <span ng-if="shippingaddr.firstname">*</span></label>
            <div class="col-lg-6">
              <div ng-model="shippingaddr.country" country-select ng-required="shippingaddr.firstname"></div>
            </div>
          </div>

          <div class="form-group hiddenoverflow">
            <label class="col-lg-2 control-label">State/Province <span ng-if="shippingaddr.firstname">*</span></label>
            <div class="col-lg-6">
              <div country="{[{ shippingaddr.country }]}" ng-model="shippingaddr.state" state-select ng-required="shippingaddr.firstname"></div>
            </div>
          </div>

          <div class="form-group hiddenoverflow">
            <label class="col-lg-2 control-label">Zip/Postal Code <span ng-if="shippingaddr.firstname">*</span></label>
            <div class="col-lg-6">
              <input type="text" class="form-control" ng-model="shippingaddr.zipcode" ng-disabled="disabled" ng-required="shippingaddr.firstname">
            </div>
          </div>

          <div class="form-group hiddenoverflow">
            <label class="col-lg-2 control-label">Phone number <span ng-if="shippingaddr.firstname!=undefined || shippingaddr.firstname">*</span></label>
            <div class="col-lg-6">
              <input type="text" class="form-control" ng-model="shippingaddr.phoneno" allow-pattern="\d" ng-disabled="disabled" ng-required="shippingaddr.firstname!=undefined || shippingaddr.firstname">
            </div>
          </div>
          <!-- ng-disabled='step3.$invalid' -->
          <div class="row">
            <div class="col-sm-offset-10 col-sm-2">
              <button class="btn btn-primary btn-rounded pull-right" type="button" ng-click="steps.step4 = true; payment = true">Save and Continue</button>
            </div>
          </div>
        </div>
      </tab>

      <tab heading="Payment Method" disabled="payment==false" active="steps.step4" select="steps.percent=80">
        <progressbar value="steps.percent" class="progress-xs" type="success"></progressbar>
        <div class="btn-group" ng-init="radioModel = 'cash'" style="margin-bottom:17px">
          <label class="btn btn-md btn-info ng-valid ng-dirty" ng-model="radioModel" btn-radio="'cash'" ng-click="paymentt('cash'); radioModel = 'cash'">
              <i class="fa fa-check text-active"> </i>
              <i class="fa fa-money"></i> Cash
          </label>
          <label class="btn btn-md btn-info ng-valid ng-dirty" ng-model="radioModel" btn-radio="'card'" ng-click="paymentt('card'); radioModel = 'card'" >
            <i class="fa fa-check text-active"></i>
            <i class="fa fa-credit-card"></i> Card
          </label>
          <label class="btn btn-md btn-info ng-valid ng-dirty active" ng-model="radioModel" btn-radio="'check'" ng-click="paymentt('check');radioModel = 'check'">
            <i class="fa fa-check text-active"></i>
            <i class="fa fa-edit"></i> E-Check
          </label>
        </div>

        <div class="row">
          <div class="col-sm-6" ng-if="radioModel=='card'">
            <div class="form-group row">
                <div class="col-md-8">
                    <img src="/img/creditcards/amex.png" width="49px" height="29px">
                    <img src="/img/creditcards/discover.png" width="49px" height="29px">
                    <img src="/img/creditcards/mastercard.png" width="49px" height="29px">
                    <img src="/img/creditcards/visa.png" width="49px" height="29px">
                </div>
            </div>
            <div class="form-group row" >
                <div class="col-md-4">
                    Credit Card Number: *
                </div>
                <div class="col-md-8">
                    <span class="label label-danger" ng-if="error.ccn">This field is required</span>
                    <input class="form-control" type="text" ng-model="userccreg.ccn" ng-change="change(userccreg.ccn, 'ccn')" name="" required only-digits />
                </div>
            </div>
            <div class="form-group row" >
                <div class="col-md-4">
                    CVV Number: *
                </div>
                <div class="col-md-8">
                    <span class="label label-danger" ng-if="error.cvvn">This field is required</span>
                    <input type="text" class="form-control " ng-model="userccreg.cvvn" ng-change="change(userccreg.cvvn, 'cvvn')" name="" required only-digits />
                </div>
            </div>
            <div class="form-group row" >
                <div class="col-md-4">
                    Credit Card Expiration: *
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-xs-6">
                            <span>Month</span><br/>
                            <span class="label label-danger" ng-if="error.expiremonth">This field is required</span>
                            <select ng-model="userccreg.expiremonth" class="form-control" ng-change="change(userccreg.expiremonth, 'expiremonth')" ng-options="m.val as m.name for m in months" required></select>
                        </div>
                        <div class="col-xs-6">
                            <span>Year</span><br/>
                            <span class="label label-danger" ng-if="error.expireyear">This field is required</span>
                            <select ng-model="userccreg.expireyear" class="form-control" ng-change="change(userccreg.expireyear, 'expireyear')" ng-options="y.val as y.val for y in years" required></select>
                        </div>
                    </div>
                </div>
            </div>
          </div>


          <div ng-form name="check" class="col-sm-6" ng-if="radioModel=='check'">
            <div class="form-group row" style="padding-left:15px;">
                <div class="col-md-4">
                    Account Holder Name: *
                </div>
                <div class="col-md-8">
                    <span class="label label-danger" ng-if="error.accountname">This field is required</span>
                    <input class="form-control " type="text" ng-model="userecheck.accountname" ng-change="change(userecheck.accountname, 'accountname')" name="accountname" ng-required="true"/>
                </div>
            </div>
            <div class="form-group row" style="padding-left:15px;">
                <div class="col-md-4">
                    Your bank name: *
                </div>
                <div class="col-md-8">
                            <span class="label label-danger" ng-if="error.bankname">This field is required</span>
                    <input class="form-control " type="text" ng-model="userecheck.bankname" ng-change="change(userecheck.bankname, 'bankname')" name="bankname" ng-required="true"/>
                </div>
            </div>
            <div class="form-group row" style="padding-left:15px;">
                <div class="col-md-12">
                    Enter the routing code and account number as they appear on the bottom of your check:
                </div>
                <div class="col-md-12">
                    <img src="/img/checksample.png">
                </div>
            </div>
            <div class="form-group row" style="padding-left:15px;">
                <div class="col-md-4">
                    Bank Routing Number: *
                </div>
                <div class="col-md-8">
                    <span class="label label-danger" ng-if="error.bankrouting">This field is required</span>
                    <input class="form-control " type="text" ng-model="userecheck.bankrouting" ng-change="change(userecheck.bankrouting, 'bankrouting')" name="bankrouting" ng-required="true" only-digits/>
                </div>
            </div>
            <div class="form-group row" style="padding-left:15px;">
                <div class="col-md-4">
                    Bank Account Number: *
                </div>
                <div class="col-md-8">
                    <span class="label label-danger" ng-if="error.bankaccountnumber">This field is required</span>
                    <input class="form-control " type="text" ng-model="userecheck.bankaccountnumber" ng-change="change(userecheck.bankaccountnumber, 'bankaccountnumber')" name="bankaccountnumber" ng-required="true" only-digits />
                </div>
            </div>
            <div class="form-group row" style="padding-left:15px;">
                <div class="col-md-4">
                    Select Account Type:
                </div>
                <div class="col-md-8" ng-init="userecheck.at = 'CHECKING'">
                    <input type="radio" name="at" ng-model="userecheck.at" value="CHECKING">&nbsp;&nbsp;Checking account<br/>
                    <input type="radio" name="at" ng-model="userecheck.at" value="BUSINESSCHECKING">&nbsp;&nbsp;Business checking account<br/>
                    <input type="radio" name="at" ng-model="userecheck.at" value="SAVINGS">&nbsp;&nbsp;Savings account
                </div>
            </div>

            <div class="form-group row" style="padding-left:15px;">
                <div class="col-md-12">
                    <input type="checkbox"  ng-model="userecheck.autorz" name="autorz" value="1" ng-required="true">*
                      <span style="text-align: center;">
                      By entering my account number about and Clicking authorize, I authorize my payment to be processed
                      as an electronic funds transfer or draft drawn from my account. If the payment returned unpaid, I authorize you or your service
                      provider to collect the payment at my state's return item fee by electronic funds transfer(s) or draft(s) drawn from my account.
                      <a href="https://www.achex.com/html/NSF_pop.jsp" target="_blank">Click here to view your state's returned item fee.</a> If this payment is from a corporate account, I make these
                      authorizations as an authorized corporate representative and agree that the entity will be bound by the NACHA operating rules.
                      </span>
                </div>
            </div>
          </div>

          <div class="col-sm-6" ng-if="radioModel!='cash'">
            <div class="panel panel-default">
              <div class="panel-heading">Billing Information</div>
              <div class="panel-body">
                <div class="form-group hiddenoverflow">
                  <label class="col-lg-3 control-label text-right">First name:</label>
                  <div class="col-lg-7">
                    <p>{[{ billingaddr.firstname }]}</p>
                  </div>
                </div>

                <div class="form-group hiddenoverflow">
                  <label class="col-lg-3 control-label text-right">Last name:</label>
                  <div class="col-lg-7">
                    <p>{[{ billingaddr.lastname }]}</p>
                  </div>
                </div>

                <div class="form-group hiddenoverflow">
                  <label class="col-lg-3 control-label text-right">Address:</label>
                  <div class="col-lg-7">
                    <p>{[{ billingaddr.address }]}</p>
                  </div>
                </div>

                <div class="form-group hiddenoverflow">
                  <label class="col-lg-3 control-label text-right">Address 2:</label>
                  <div class="col-lg-7">
                    <p>{[{ billingaddr.address2 }]}</p>
                  </div>
                </div>

                <div class="form-group hiddenoverflow">
                  <label class="col-lg-3 control-label text-right">City:</label>
                  <div class="col-lg-7">
                    <p>{[{ billingaddr.city }]}</p>
                  </div>
                </div>

                <div class="form-group hiddenoverflow">
                  <label class="col-lg-3 control-label text-right">Country:</label>
                  <div class="col-lg-7">
                    <p>{[{ billingaddr.country }]}</p>
                  </div>
                </div>

                <div class="form-group hiddenoverflow">
                  <label class="col-lg-3 control-label text-right">State/Province:</label>
                  <div class="col-lg-7">
                    <p>{[{ billingaddr.state }]}</p>
                  </div>
                </div>

                <div class="form-group hiddenoverflow">
                  <label class="col-lg-3 control-label text-right">Zip/Postal Code:</label>
                  <div class="col-lg-7">
                    <p>{[{ billingaddr.zipcode }]}</p>
                  </div>
                </div>

                <div class="form-group hiddenoverflow">
                  <label class="col-lg-3 control-label text-right">Phone number:</label>
                  <div class="col-lg-7">
                    <p>{[{ billingaddr.phoneno }]}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-offset-10 col-sm-2">
            <button class="btn btn-primary btn-rounded pull-right" type="button" ng-disabled="(radioModel=='card' && card.$invalid) || (radioModel=='check' && check.$invalid)" ng-click="preview(radioModel)">Save and Continue</button>
          </div>
        </div>
      </tab>

      <tab heading="View Order" disabled="order.length < 1 || step2.$invalid || view == false" active="steps.step5" select="steps.percent=100">
        <progressbar value="steps.percent" class="progress-xs" type="success"></progressbar>
        <div class="row">
          <div class="col-sm-4">
            <div class="panel panel-default">
              <div class="panel-heading">Payment Method:
                <span class="label label-info label" style="font-size:15px;">
                  <i class="fa fa-money" ng-if="radioModel=='cash'"></i>
                  <i class="fa fa-credit-card" ng-if="radioModel=='card'"></i>
                  <i class="fa fa-edit" ng-if="radioModel=='check'"></i>
                  {[{ radioModel }]}
                </span>
              </div>
              <div class="panel-body" ng-if="radioModel=='card'">
                <div class="form-group row" >
                  <div class="col-md-5">
                    Credit Card Number:
                  </div>
                  <div class="col-md-5">
                    <p>{[{ userccreg.ccn }]}</p>
                  </div>
                </div>
                <div class="form-group row" >
                  <div class="col-md-5">
                    CVV Number:
                  </div>
                  <div class="col-md-5">
                    <p>{[{ userccreg.cvvn }]}</p>
                  </div>
                </div>
                <div class="form-group row" >
                  <div class="col-md-5">
                      Credit Card Expiration:
                  </div>
                  <div class="col-md-5">
                    <p>{[{ userccreg.expiremonth + "-" + userccreg.expireyear }]}</p>
                  </div>
                </div>
              </div>

              <div class="panel-body" ng-if="radioModel=='check'">
                <div class="form-group row" style="padding-left:15px;">
                    <div class="col-md-5">
                        Account Holder Name:
                    </div>
                    <div class="col-md-5">
                      {[{ userecheck.accountname }]}
                    </div>
                </div>
                <div class="form-group row" style="padding-left:15px;">
                    <div class="col-md-5">
                        Your bank name:
                    </div>
                    <div class="col-md-5">
                      {[{ userecheck.bankname }]}
                    </div>
                </div>
                <div class="form-group row" style="padding-left:15px;">
                    <div class="col-md-5">
                        Bank Routing Number:
                    </div>
                    <div class="col-md-5">
                      {[{ userecheck.bankrouting }]}
                    </div>
                </div>
                <div class="form-group row" style="padding-left:15px;">
                    <div class="col-md-5">
                        Bank Account Number:
                    </div>
                    <div class="col-md-5">
                      {[{ userecheck.bankaccountnumber }]}
                    </div>
                </div>
                <div class="form-group row" style="padding-left:15px;">
                    <div class="col-md-5">
                        Select Account Type:
                    </div>
                    <div class="col-md-5">
                        {[{ userecheck.at=='CHECKING' ? 'Checking account' : '' }]}
                        {[{ userecheck.at=='BUSINESSCHECKING' ? 'Business checking account' : '' }]}
                        {[{ userecheck.at=='SAVINGS' ? 'Savings account' : '' }]}
                    </div>
                </div>
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading">Billing Information</div>
              <div class="panel-body">
                <div class="form-group hiddenoverflow">
                  <label class="col-lg-4 control-label text-right">First name:</label>
                  <div class="col-lg-8">
                    <p>{[{ billingaddr.firstname }]}</p>
                  </div>
                </div>

                <div class="form-group hiddenoverflow">
                  <label class="col-lg-4 control-label text-right">Last name:</label>
                  <div class="col-lg-8">
                    <p>{[{ billingaddr.lastname }]}</p>
                  </div>
                </div>

                <div class="form-group hiddenoverflow">
                  <label class="col-lg-4 control-label text-right">Address:</label>
                  <div class="col-lg-8">
                    <p>{[{ billingaddr.address }]}</p>
                  </div>
                </div>

                <div class="form-group hiddenoverflow">
                  <label class="col-lg-4 control-label text-right">Address 2:</label>
                  <div class="col-lg-8">
                    <p>{[{ billingaddr.address2 }]}</p>
                  </div>
                </div>

                <div class="form-group hiddenoverflow">
                  <label class="col-lg-4 control-label text-right">City:</label>
                  <div class="col-lg-8">
                    <p>{[{ billingaddr.city }]}</p>
                  </div>
                </div>

                <div class="form-group hiddenoverflow">
                  <label class="col-lg-4 control-label text-right">Country:</label>
                  <div class="col-lg-8">
                    <p>{[{ billingaddr.country }]}</p>
                  </div>
                </div>

                <div class="form-group hiddenoverflow">
                  <label class="col-lg-4 control-label text-right">State/Province:</label>
                  <div class="col-lg-8">
                    <p>{[{ billingaddr.state }]}</p>
                  </div>
                </div>

                <div class="form-group hiddenoverflow">
                  <label class="col-lg-4 control-label text-right">Zip/Postal Code:</label>
                  <div class="col-lg-8">
                    <p>{[{ billingaddr.zipcode }]}</p>
                  </div>
                </div>

                <div class="form-group hiddenoverflow">
                  <label class="col-lg-4 control-label text-right">Phone number:</label>
                  <div class="col-lg-8">
                    <p>{[{ billingaddr.phoneno }]}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm-8">
            <div class="panel panel-default">
              <div class="panel-heading">Order</div>
              <div class="panel-body">
                <div class="table-responsive">
                  <table class="table table-striped b-t b-light">
                    <thead>
                        <tr>
                          <th>Product</th>
                          <th>Price</th>
                          <th style="width:5%">Quantity</th>
                          <th>Total Price</th>
                        </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="mem in order">
                        <td>{[{ mem.name }]}</td>
                        <td>{[{ mem.price | currency }]}</td>
                        <td>{[{ mem.orderquantity }]}</td>
                        <td>{[{ mem.total | currency }]}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm-5">
            <div class="panel panel-default" ng-if="shippingaddr.firstname!=''">
              <div class="panel-heading">Shipping Information</div>
              <div class="panel-body">
                <div class="form-group hiddenoverflow">
                  <label class="col-lg-4 control-label text-right">Name: </label>
                  <div class="col-lg-8">
                    <p>{[{ shippingaddr.firstname + " " + shippingaddr.lastname }]}</p>
                  </div>
                </div>
                <div class="form-group hiddenoverflow">
                  <label class="col-lg-4 control-label text-right">Address: </label>
                  <div class="col-lg-8">
                    <p>{[{ shippingaddr.address }]}</p>
                  </div>
                </div>
                <div class="form-group hiddenoverflow">
                  <label class="col-lg-4 control-label text-right">Address 2: </label>
                  <div class="col-lg-8">
                    <p>{[{ shippingaddr.address2 }]}</p>
                  </div>
                </div>
                <div class="form-group hiddenoverflow">
                  <label class="col-lg-4 control-label text-right">Phone no: </label>
                  <div class="col-lg-8">
                    <p>{[{ shippingaddr.phoneno }]}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm-3">
            <div class="panel panel-default">
              <div class="panel-heading">Sub Total : {[{ totalamount | currency}]} </div>
              <div class="panel-body">
                <p ng-if="shippingaddr.firstname != undefined"> Shipping fee: {[{ shippingfee | currency}]}</p>
                <p> Tax: {[{ tax | currency}]}</p>
                <p class="text-bold"> Grand Total: {[{ gtotal | currency}]}</p>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-offset-10 col-sm-2">
            <button class="btn btn-primary btn-rounded pull-right" type="button" ng-click="submit()">Submit Order</button>
          </div>
        </div>
      </tab>

    </tabset>

  </div>
</fieldset>
</form>
