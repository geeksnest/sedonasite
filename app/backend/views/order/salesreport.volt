<!-- order reports -->

<div>
    <!-- header -->


    <div class="wrapper bg-light lter b-b">
        <div class="btn-toolbar">
                <div class="form-group">
                    <div class="col-sm-4">
                        <label>Start Date:</label><br>
                        <div class="input-group w-md">
                            <input type="text" class="form-control" datepicker-popup="{[{format1}]}" 
                            ng-model="startdate" is-open="opened1" datepicker-options="dateOptions" ng-required="true" close-text="Close" placeholder="Start Date" />
                              <span class="input-group-btn">
                            <button type="button" class="btn btn-default" ng-click="open($event,'opened1')"><i class="glyphicon glyphicon-calendar"></i></button>
                              </span>
                               <input type='hidden' ng-model='start2'>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label>End Date:</label><br>
                        <div class="input-group w-md">
                            <input type="text" class="form-control" datepicker-popup="{[{format2}]}" 
                            ng-model="enddate" is-open="opened2" datepicker-options="dateOptions" ng-required="true" close-text="Close" placeholder="End Date" />
                              <span class="input-group-btn">
                             <button type="button" class="btn btn-default" ng-click="open($event,'opened2')"><i class="glyphicon glyphicon-calendar"></i></button>
                              </span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label>Status:</label><br>
                        <div class="input-group w-md">
                            <select chosen allow-single-deselect class="form-control" ng-model="news.status" ng-options="mem for mem in status" required="required">
                            </select>
                            <input type='hidden' ng-model='stat'>
                        </div>
                    </div>
                   
                    <div class="col-sm-4">
                        <br>
                        <label>Payment Types:</label><br>
                        <div class="input-group w-md">
                            <select chosen allow-single-deselect class="form-control" ng-model="news.payment" ng-options="mem for mem in payment"  required="required">
                            </select>
                             <input type='hidden' ng-model='pay'>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <br>
                        <label>Members</label><br>
                        <div class="input-group w-md">
     <select chosen allow-single-deselect class="form-control" ng-model="news.members" required="required" ng-change='member1(members)' ng-options="x.id as x.name for x in members">

        <option value='{[{x.id}]}'> {[{x.name}]} </option>
                         

             </select>
              <input type='hidden' ng-model='member2'>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <br>
                        <label></label><br>
                        <div class="input-group w-md">
        <button class="btn m-b-xs btn-sm btn-primary btn-addon searchbtn" ng-click="searchbtn(startdate,enddate,news.status,news.payment,news.members)" style="margin-right:2%">SEARCH</button>
   
          <button class="btn m-b-xs btn-sm btn-info showbtn" ng-click="show()">SHOW ALL</button>
    
        <a class="btn m-b-xs btn-sm btn-danger btn-addon" class='pdf' ng-click="print()">DOWNLOAD AS PDF</a>
     

                        </div>
                    </div>
                </div>
        </div>

         
    </div>
    <!-- / header -->

     <div class="row">
           <div class="col-sm-12">
            <div class='panel panel-default'>
             <div class="panel-heading font-bold">                  
              TOTAL SUMMARY:  $<span ng-bind='totalsales'> </span> 
          </div>
          <div class="panel-heading font-bold">                  
           EXPECTED GROSS:    $<span ng-bind='expectedsales'></span>
       </div>
            </div>
            </div>
    </div>

                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                              <th>Order no.</th>
                              <th>Purchased On</th>
                              <th>Bill to:</th>
                              <th>Ship to:</th>
                              <th>Payment Type</th>
                              <th>Grand Total(Purchased)</th>
                              <th>Status</th>
                             <!--  <th>Action</th> -->
                            </tr>
                        </thead>
                        <tbody ng-show="loading">
                            <tr colspan="4">
                                <td>Loading Products</td>
                            </tr>
                        </tbody>
                        <tbody ng-hide="loading">
                            <tr colspan="4" ng-show="bigTotalItems==0"> <td> No records found! </td></tr>
                            <tr ng-repeat="mem in orders">
                                <td>{[{ mem.invoiceno }]}</td>
                                <td>{[{ mem.created_at }]}</td>
                                <td>{[{ mem.firstname!=null ? mem.firstname + " " + mem.lastname : '' }]}</td>
                                <td>{[{ mem.shipfirstname!=null ? mem.shipfirstname + " " + mem.shiplastname : '' }]}</td>
                                <td ng-if="mem.paymenttype=='cash'">Cash</td>
                                <td ng-if="mem.paymenttype=='card'">Credit Card</td>
                                <td ng-if="mem.paymenttype=='check'">E-check</td>
                                <td ng-if="mem.paymenttype=='paypal'">Paypal</td>
                                <td>{[{ mem.amount | currency }]}</td>
                                <td>
                                <!-- {[{ mem.status }]} -->
                                <span class="label bg-success" ng-show="mem.status=='completed'">Completed</span>
                                <span class="label bg-primary" ng-show="mem.status=='processing'">Processing</span>
                                <span class="label bg-danger" ng-show="mem.status=='void'">Void</span>
                                <span class="label bg-info" ng-show="mem.status=='pending'">Pending</span> <br>
                               
                                </td>
                                <!-- <td>
                                  <a href="" ng-click="view(mem.id)"><span class="label bg-info" >View</span></a>
                                </td> -->
                            </tr>
                        </tbody>
                    </table>
                </div>



<!-- with search for pdf -->
  <div class="forsearching">
     <img src="/img/sedonalogo.png" style="width:140px;
  visibility: collapse;
  position: absolute;
  top:0;
  margin-left: 240px;">
        <table class="table table-striped b-t b-light" id='tableExport' style='font-size:11px;position:absolute;bottom:200000px'>
            <thead>
                <tr>
                  <th>Order<br>no.</th>
                  <th>Purchased <br> On</th>
                  <th>Bill<br> to:</th>
                  <th>Ship<br> to:</th>
                  <th>Payment <br>Type</th>
                  <th>Grand<br> Total <br>(Purchased)</th>
                  <th>Status</th>
              </tr>
          </thead>
        <tbody>
            <tr ng-repeat="mem in orders3">
                <td>{[{ mem.invoiceno }]}</td>
                <td>{[{ mem.created_at }]}</td>
                <td>{[{ mem.firstname!=null ? mem.firstname + " " + mem.lastname : '' }]}</td>
                <td>{[{ mem.shipfirstname!=null ? mem.shipfirstname + " " + mem.shiplastname : '' }]}</td>
                <td ng-if="mem.paymenttype=='cash'">Cash</td>
                <td ng-if="mem.paymenttype=='card'">Credit Card</td>
                <td ng-if="mem.paymenttype=='check'">E-check</td>
                <td ng-if="mem.paymenttype=='paypal'">Paypal</td>
                <td>{[{ mem.amount | currency }]}</td>
                <td>
                    {[{ mem.status }]}
                </td>
          </tr>
      </tbody>
        </table>
    </div>
<!-- with search for pdf -->



<!-- all entries for pdf -->
 <div class="allentries">
     <img src="/img/sedonalogo.png" style="width:140px;
  visibility: collapse;
  position: absolute;
  top:0;
  margin-left: 240px;">
        <table class="table table-striped b-t b-light" id='tableExport' style='font-size:11px;position:absolute;bottom:2000000px;word-wrap: break-word;'>
            <thead>
                <tr>
                  <th>Order<br>no.</th>
                  <th>Purchased <br> On</th>
                  <th>Bill<br> to:</th>
                  <th>Ship<br> to:</th>
                  <th>Payment <br>Type</th>
                  <th>Grand<br> Total <br>(Purchased)</th>
                  <th>Status</th>
              </tr>
          </thead>
        <tbody>
            <tr ng-repeat="mem in orders2">
                <td>{[{ mem.invoiceno }]}</td>
                <td>{[{ mem.created_at }]}</td>
                <td>{[{ mem.firstname!=null ? mem.firstname + " " + mem.lastname : '' }]}</td>
                <td>{[{ mem.shipfirstname!=null ? mem.shipfirstname + " " + mem.shiplastname : '' }]}</td>
                <td ng-if="mem.paymenttype=='cash'">Cash</td>
                <td ng-if="mem.paymenttype=='card'">Credit Card</td>
                <td ng-if="mem.paymenttype=='check'">E-check</td>
                <td ng-if="mem.paymenttype=='paypal'">Paypal</td>
                <td>{[{ mem.amount | currency }]}</td>
                <td>
                    {[{ mem.status }]}
                </td>
          </tr>
      </tbody>
        </table>
    </div>

<!-- all entries for pdf -->

    <footer class="panel-footer">
        <div class="row">
            <div class="col-sm-4 hidden-xs">
            </div>
            <div class="col-sm-5 text-center" ng-hide="bigTotalItems==0 || loading">
                <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
                <pagination ng-hide="maxSize >= bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </div>
        </div>
    </footer>


<script>
$(document).ready(function(){
    $('.forsearching').removeAttr('id');
    $(".allentries").attr("id","print");
});

$(".searchbtn").click(function(){
    $('.allentries').removeAttr('id');
    $(".forsearching").attr("id","print");
});


$(".showbtn").click(function(){
    $('.forsearching').removeAttr('id');
    $(".allentries").attr("id","print");
});

</script>

</div>
