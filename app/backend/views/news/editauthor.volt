{{ content() }}

<script type="text/ng-template" id="categoryAdd.html">
   <div ng-include="'/be/tpl/categoryAdd1.html'"></div>
</script>

<script type="text/ng-template" id="mediagallery.html">
  <div ng-include="'/be/tpl/mediagallery.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Edit Author</h1>
  <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="updateAuthor(author)" name="formpage">
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">

        <div class="col-sm-8">
          <div class="panel panel-default">

            <div class="panel-heading font-bold">
              Authors Information
            </div>


              <div class="panel-body">
                <input type="hidden" id="authorid" name="authorid" ng-model="author.authorid">
                <label class="col-sm-2 control-label">
                  <label for="title">Name</label>
                </label>
                <div class="col-sm-10">
                  <input type="text" id="name" name="name" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="author.name" required="required">
                  
                  <div class="line line-dashed b-b line-lg"></div>
                </div> 

                <label class="col-sm-2 control-label">
                  <label for="title">Location</label>
                </label>
                <div class="col-sm-10">
                  <input type="text" id="location" name="location" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="author.location" required="required">
                  
                  <div class="line line-dashed b-b line-lg"></div>
                </div> 

                <label class="col-sm-2 control-label">
                  <label for="title">Occupation</label>
                </label>
                <div class="col-sm-10">
                  <input type="text" id="occupation" name="occupation" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="author.occupation" required="required">
                  
                  <div class="line line-dashed b-b line-lg"></div>
                </div> 
            
                <label class="col-sm-2 control-label">
                  <label for="title">About the Author</label>
                </label>
                <div class="col-sm-10">
                  <textarea class="ck-editor" ng-model="author.about" required="required"></textarea>
                </div>
                
              </div>

          </div>
        </div>


        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Photo of the Author
              <a class="btn btn-default btn-xs pull-right" ng-click="media('featured')"><i class="fa  fa-folder-open"></i> Media Library </a>
            </div>
            <div class="panel-body">
              <img ng-show="author.photo" src="<?php echo $this->config->application->amazonlink; ?>/uploads/saveauthorimage/{[{ author.photo }]}" style="width: 100%">
            </div>
          </div>
        </div>



      </div>


      <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-right bg-light lter">
              <a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
              <button type="submit" class="btn btn-success" ng-disabled="formpage.$invalid">Submit</button>
            </footer>
        </div>
      </div>

  </div>
</fieldset>
</form>