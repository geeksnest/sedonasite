{{ content() }}

<script type="text/ng-template" id="newsDelete.html">
  <div ng-include="'/be/tpl/newsDelete.html'"></div>
</script>

<script type="text/ng-template" id="newsCenterDelete.html">
  <div ng-include="'/be/tpl/newsCenterDelete.html'"></div>
</script>

<script type="text/ng-template" id="newsEdit.html">
  <div ng-include="'/be/tpl/newsEdit.html'"></div>
</script>

<script type="text/ng-template" id="newscenterEdit.html">
  <div ng-include="'/be/tpl/newscenterEdit.html'"></div>
</script>


<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">News List</h1>
  <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" name="formmanagenews">
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">

        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              News List
            </div>


              <div class="panel-body">

                <div class="row wrapper">
                    <div class="col-sm-5 m-b-xs" ng-show="keyword">
                        <strong>{[{ bigTotalItems }]}</strong> Results found for: <strong> "{[{ keyword }]}"</strong> <button class="btn btn-default btn-xs" ng-click="clear()">Clear</button>
                    </div>
                    <div class="col-sm-5 m-b-xs pull-right">
                        <div class="input-group">
                            <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                            <span class="input-group-btn">
                            <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                            </span>
                        </div>
                        <br>
                        <div class="input-group" style="width:100%">
                          Sort by:
                          <div class="pull-right">
                            <label class="radio-inline">
                              <input type="radio" ng-model="sort" ng-click="sortpage(sort)" value="title"> Title
                            </label>
                            <label class="radio-inline">
                              <input type="radio" ng-model="sort" ng-click="sortpage(sort)" value="author.name"> Auhtor
                            </label>
                            <label class="radio-inline">
                              <input type="radio" ng-model="sort" ng-click="sortpage(sort)" value="date"> Date published
                            </label>
                            <label class="radio-inline">
                              <input type="radio" ng-model="sort" ng-click="sortpage(sort)" value="datecreated"> Date created
                            </label>
                            <label class="radio-inline">
                              <input type="radio" ng-model="sort" ng-click="sortpage(sort)" value="dateedited"> Date updated
                            </label>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th style="width:40%">Title</th>
                                <th style="width:20%">Author</th>
                                <th style="width:20%">Status</th>
                                <th style="width:15%">Action</th>
                            </tr>
                        </thead>
                        <tbody ng-show="loading">
                            <tr colspan="4">
                                <td>Loading News</td>
                            </tr>
                        </tbody>
                        <tbody ng-hide="loading">
                            <tr colspan="4" ng-show="bigTotalItems==0"> <td> No records found! </td></tr>
                            <tr ng-repeat="mem in data.data">

                                <td><a href="<?php echo $this->config->application->BaseURL.'/blog/'; ?>{[{ mem.newsslugs }]}" target="_blank">{[{ mem.title }]}</a></td>
                                <td>{[{ mem.name }]}</td>
                                <td  ng-if="mem.status == 1">
                                <div class="pagestatuscontent fade-in-out"><span class="label bg-success" >Active</span></div>
                                <div class="checkstatuscontent">
                                  <label class="i-switch bg-info m-t-xs m-r">
                                    <input type="checkbox" checked="" ng-click="setstatus(mem.status,mem.newsid,searchtext,mem.newslocation,mem.newsslugs)">
                                    <i></i>
                                  </label>
                                  
                                </div>
                                <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.newsslugs"><i class="fa fa-check"></i></spand></div>
                                </td>
                                <td  ng-if="mem.status == 0">
                                <div class="pagestatuscontent fade-in-out"><span class="label bg-danger">Deactivated</span></div>
                                <div class="checkstatuscontent">
                                  <label class="i-switch bg-info m-t-xs m-r">
                                    <input type="checkbox" ng-click="setstatus(mem.status,mem.newsid,searchtext,mem.newslocation,mem.newsslugs)">
                                    <i></i>
                                  </label>
                                 
                                </div>
                                 <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow ==mem.newsslugs"><i class="fa fa-check"></i></spand></div>
                                </td>
                                </td>
                                <td>
                                    <a href="" ng-click="editnews(mem.newsid)"><span class="label bg-warning" >Edit</span></a>
                                    <a href="" ng-click="deletenews(mem.newsid)"> <span class="label bg-danger">Delete</span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

              </div>


          </div>
        </div>

      </div>

      <div class="row" ng-hide="bigTotalItems==0 || loading">
        <div class="panel-body">
            <footer class="panel-footer text-center bg-light lter">
                <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
                <pagination ng-hide="maxSize > bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </footer>
        </div>
      </div>

  </div>
</fieldset>
</form>