<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class ProductController extends ControllerBase
{

    public function indexAction()
    {

    }
    public function addproductAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function manageproductAction()
    {
    	$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function editproductAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function viewproductAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function managecategoryAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function managesubcategoryAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function managetypeAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function managetagsAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
}
