<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class ProfileController extends ControllerBase
{

    public function indexAction()
    {        
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

}
