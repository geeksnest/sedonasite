<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class PagesController extends ControllerBase
{

    public function indexAction()
    {

    }
    public function createpageAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function managepageAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function managepagecategoryAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function editpageAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function auraAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function managepage_auraAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function editpage_auraAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

}
