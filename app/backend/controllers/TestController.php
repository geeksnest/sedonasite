<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;
use Modules\Backend\Forms\CreateuserForm;
use Modules\Backend\Models\Users as Users;

class TestController extends ControllerBase
{
    public function indexAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

     public function createtestAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function createresultAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

         public function viewtestsAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

       public function edittestAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

     public function editresultAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
      public function testtakersAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

}

