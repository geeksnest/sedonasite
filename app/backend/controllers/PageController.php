<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class PageController extends ControllerBase
{
    public function pagecreateAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function pagemanageAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
     public function pageeditAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
     public function pagefooterAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

}