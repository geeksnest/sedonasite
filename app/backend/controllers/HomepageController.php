<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class HomepageController extends ControllerBase
{
    public function indexAction()
    {
        echo "BOOM";
    }
    public function createAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function manageAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function editAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }


}

