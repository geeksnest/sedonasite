<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class OrderController extends ControllerBase
{
    public function addorderAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
	
	public function manageAction()
	{
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}  

	public function viewAction()
	{
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	} 
	public function reportsAction()
	{
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	} 
	public function salesreportAction()
	{
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	} 
	public function saleschartAction()
	{
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	} 
}

