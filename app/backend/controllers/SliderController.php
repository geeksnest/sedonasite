<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;
use Modules\Backend\Forms\CreateuserForm;
use Modules\Backend\Models\Users as Users;

class SliderController extends ControllerBase
{
    public function indexAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

     public function create_albumAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function edit_albumAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }


}

