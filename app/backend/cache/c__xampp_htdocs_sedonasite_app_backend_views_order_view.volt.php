<?php echo $this->getContent(); ?>

<script type="text/ng-template" id="addInvoice.html">
   <div ng-include="'/be/tpl/addInvoice.html'"></div>
</script>

<script type="text/ng-template" id="mediagallery.html">
   <div ng-include="'/be/tpl/mediagallery.html'"></div>
</script>

<script type="text/ng-template" id="removeProduct.html">
   <div ng-include="'/be/tpl/removeProduct.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md ng-scope">
  <h1 class="m-n font-thin h3">
    View Order 
  </h1>
  <a id="top"></a>
</div>

<div class="hbox hbox-auto-xs hbox-auto-sm">
    <div class="col">
      <fieldset ng-disabled="isSaving">
        <div class="wrapper-md">
            <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

            <div class="row">
              <div class="col-sm-12">
                <div class="col-sm-6">
                  <h4>Order #: {[{ order.invoiceno }]}</h4>
                </div>
                <div class="col-sm-6">
                  <button ng-click="print()" class="btn btn-info viewpr-mg-bot pull-right pdf"> Print</button>
                  <a ui-sref="manageorder" class="btn btn-default viewpr-mg-bot pull-right"><i class="glyphicon glyphicon-chevron-left"></i> back</a>
                </div>
                <!-- product -->
                <div class="col-sm-6">
                  <div class="panel panel-info">
                    <div class="panel-heading font-bold">
                      Order
                    </div>
                    <div class="list-group bg-white">
                      <div class="list-group-item">
                        Order date : {[{ order.created_at }]}
                      </div>
                      <div class="list-group-item">
                        Order status : {[{ order.status }]}
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="panel panel-info">
                    <div class="panel-heading font-bold">
                      Account Information
                    </div>
                    <div class="list-group bg-white">
                      <div class="list-group-item">
                        Customer name : {[{ acctinfo.firstname + " " + acctinfo.lastname }]}
                      </div>
                      <div class="list-group-item">
                        Email : {[{ acctinfo.email }]}
                      </div>
                      <div class="list-group-item">
                        Phone number : {[{ acctinfo.phoneno }]}
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="panel panel-info">
                    <div class="panel-heading font-bold">
                      Shipping Address
                    </div>
                    <div class="list-group bg-white">
                      <div class="list-group-item">
                        Name : {[{ shippinginfo.firstname + " " + shippinginfo.lastname }]}
                      </div>
                      <div class="list-group-item">
                        Address : {[{ shippinginfo.address }]}
                      </div>
                      <div class="list-group-item">
                        Address 2 : {[{ shippinginfo.address2 }]}
                      </div>
                      <div class="list-group-item">
                        State : {[{ shippinginfo.state }]}
                      </div>
                      <div class="list-group-item">
                        Country : {[{ shippinginfo.country }]}
                      </div>
                      <div class="list-group-item">
                        Zip code : {[{ shippinginfo.zipcode }]}
                      </div>
                      <div class="list-group-item">
                        Phone no : {[{ shippinginfo.phoneno }]}
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="col-sm-6">
                  <div class="panel panel-info">
                    <div class="panel-heading font-bold">
                      Billing Address
                    </div>
                    <div class="list-group bg-white">
                      <div class="list-group-item">
                        Name : {[{ order.firstname + " " + order.lastname }]}
                      </div>
                      <div class="list-group-item">
                        Address : {[{ order.address }]}
                      </div>
                      <div class="list-group-item">
                        Address 2 : {[{ order.address2 }]}
                      </div>
                      <div class="list-group-item">
                        State : {[{ order.state }]}
                      </div>
                      <div class="list-group-item">
                        Country : {[{ order.country }]}
                      </div>
                      <div class="list-group-item">
                        Zip code : {[{ order.zipcode }]}
                      </div>
                      <div class="list-group-item">
                        Phone no : {[{ order.phoneno }]}
                      </div>
                    </div>
                  </div>
                </div>

                <!-- purchased history -->
                <div class="col-sm-12">
                  <div class="panel panel-info no-margin">
                    <div class="panel-heading font-bold">
                      Items Ordered (Clients total ordered product: {[{ itemcount }]})
                    </div>
                    <div class="panel-body">      
                      <div class="table-responsive">
                        <table class="table table-striped b-t b-light">
                            <thead>
                                <tr>
                                  <th>Product</th>
                                  <th>Price</th>
                                  <th>Quantity</th>
                                  <th>Total</th>
                                </tr>
                            </thead>
                            <tbody ng-show="loading">
                                <tr colspan="4">
                                    <td>Loading Products</td>
                                </tr>
                            </tbody>
                            <tbody ng-hide="loading">
                                <tr colspan="4" ng-show="bigTotalItems"> <td> No records found! </td></tr>
                                <tr ng-repeat="product in products">
                                    <td>{[{ product.name }]}</td>
                                    <td>{[{ product.price2 | currency }]}</td>
                                    <td>{[{ product.orderquantity }]}</td>
                                    <td>{[{ product.total | currency }]}</td>
                                </tr>
                            </tbody>
                        </table>
                      </div>

                      <p class="full-width text-right">Sub Total : {[{ subtotal | currency }]}</p>
                      <p class="full-width text-right" ng-if="shippinginfo.firstname!=''">Shipping Fee : {[{ "200" | currency }]}</p>
                      <p class="full-width text-right">Grand Total : {[{ totalamount | currency }]}</p>
                    </div>
                  </div>
                  <div class="row" ng-hide="bigTotalItems==0 || loading">
                    <div class="panel-body">
                      <footer class="panel-footer text-center bg-light lter">
                          <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
                          <pagination ng-hide="maxSize > bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
                      </footer>
                    </div>
                  </div>
                </div>
                
                <div class="col-sm-6">
                  <div class="panel panel-info no-margin">
                    <div class="panel-heading font-bold">
                      Comment History
                    </div>
                    <div class="panel-body">
                      <form class="form-validation form-horizontal  ng-pristine ng-invalid ng-invalid-required" name="formorderc" ng-submit="submitcomment(order)">
                        <div class="form-group hiddenoverflow">
                          <label class="col-lg-offset-1 col-lg-2 control-label">Status:</label>
                          <div class="col-lg-3">
                            <select name="account" class="form-control m-b" disabled='disabled' ng-model="order.status">
                              <option>pending</option>
                              <option>processing</option>
                              <option>completed</option>
                            </select>
                          </div>
                        </div>

                        <div class="form-group hiddenoverflow">
                          <label class="col-lg-offset-1 col-lg-2 control-label">Comments:</label>
                          <div class="col-lg-8">
                            <span class="label label-danger" ng-if="error.comment">Please add your comment.</span>
                            <textarea class="form-control" ng-model="order.comment" ng-change="change(order.comment)"></textarea>
                          </div>
                        </div>
                        
                        <div class="form-group hiddenoverflow">
                          <div class="col-lg-offset-3 col-lg-8">
                            <button class="btn btn-info pull-right" type="submit">Submit comment</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                
                <div class="col-sm-6 ordercomments" ng-if="comments.length > 0">
                  <blockquote ng-repeat="comment in comments">
                    <p>{[{ comment.comment }]}</p>
                    <footer>{[{ comment.created_at }]} | {[{comment.status}]}</footer>
                  </blockquote>
                </div>

                <div class="col-sm-12">
                  <footer class="panel-footer text-center bg-light lter hiddenoverflow">
                    <button class="btn btn-primary pull-right" type="button" ng-click="submit('processing')" ng-if="order.status=='pending'">Submit Shipment</button>
                    <button class="btn btn-default pull-right" type="button" ng-click="submit('void')" ng-if="order.status=='processing'">Void</button>
                    <button class="btn btn-primary pull-right" type="button" ng-click="submit('completed')" ng-if="order.status=='processing'">Submit Invoice</button>
                  </footer>
                </div>
              </div>
            </div>
          </div>
          
        </div>
      </fieldset>

    </div>
</div>

  
<div id="print">
  <img src="/img/sedonalogo.png" style="width:140px;
    visibility: collapse;
    position: absolute;
    top:0;
    left:0; 
    margin-bottom: 5px;
    margin-left: 240px;">
  <div class="hide">
    <div class="row">
      <div class="col-sm-12">
        <h4>Order #: {[{ order.invoiceno }]}</h4>
      </div>
      <div class="col-sm-12">
        <p><i class="text-bold">Order date:</i> {[{ order.created_at }]}</p>
      </div>
      <div class="col-sm-12">
        <p><i class="text-bold">Order status:</i> {[{ order.status }]}</p>
      </div>
      <div class="col-sm-12">
        <p><i class="text-bold">Sub Total:</i>  {[{ subtotal | currency }]}</p>
      </div>
      <div class="col-sm-12">
        <p><i class="text-bold">Shipping Fee:</i>  {[{ "200" | currency }]}</p>
      </div>
      <div class="col-sm-12">
        <p><i class="text-bold">Grand Total:</i> {[{ totalamount | currency }]}</p>
      </div>
      <div class="col-sm-12">
        <p><i class="text-bold">Account Information:</i></p>
      </div>
      <div class="col-sm-12">
        <p><i class="text-bold">Customer name:</i> {[{ acctinfo.firstname + " " + acctinfo.lastname }]}</p>
      </div>
      <div class="col-sm-12">
        <p><i class="text-bold">Email:</i> {[{ acctinfo.email }]}</p>
      </div>
      <div class="col-sm-12">
        <p><i class="text-bold">Phone no:</i> {[{ acctinfo.phoneno }]}</p>
      </div>
    </div>
  </div>
  <br>
  <div class="table-responsive">
    <table class="table table-striped b-t b-light" style='position:absolute;bottom:200000px'>
      <h5 class="hide">Order/s</h5>
        <thead>
            <tr>
              <th>Product</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Total</th>
            </tr>
        </thead>
        <tbody>
            <tr ng-repeat="product in products">
                <td>{[{ product.name }]}</td>
                <td>{[{ product.price2 | currency }]}</td>
                <td>{[{ product.orderquantity }]}</td>
                <td>{[{ product.total | currency }]}</td>
            </tr>
        </tbody>
    </table>
  </div>
  <div class="hide">
    <div class="row">
      <div class="col-sm-12">
        <p class="text-bold">Billing information</i></p>
      </div>
      <div class="col-sm-12">
        <p><i class="text-bold">Name:</i> {[{ order.firstname + " " + order.lastname }]}</p>
      </div>
      <div class="col-sm-12">
        <p><i class="text-bold">Address:</i>  {[{ order.address }]}</p>
      </div>
      <div class="col-sm-12">
        <p><i class="text-bold">Address 2:</i>  {[{ order.address2 }]}</p>
      </div>
      <div class="col-sm-12">
        <p><i class="text-bold">Phone no.:</i>  {[{ order.phoneno }]}</p>
      </div>
      <div ng-if="shippinginfo.firstname">
        <div class="col-sm-12">
          <p class="text-bold">Shipping information</i></p>
        </div>
        <div class="col-sm-12">
          <p><i class="text-bold">Name:</i> {[{ shippinginfo.firstname + " " + shippinginfo.lastname }]}</p>
        </div>
        <div class="col-sm-12">
          <p><i class="text-bold">Address:</i>  {[{ shippinginfo.address }]}</p>
        </div>
        <div class="col-sm-12">
          <p><i class="text-bold">Address 2:</i>  {[{ shippinginfo.address2 }]}</p>
        </div>
        <div class="col-sm-12">
          <p><i class="text-bold">Phone no.:</i>  {[{ shippinginfo.phoneno }]}</p>
        </div>
      </div>
    </div>
  </div>
</div>