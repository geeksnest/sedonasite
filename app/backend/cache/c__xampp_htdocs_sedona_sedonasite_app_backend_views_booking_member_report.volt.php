

<div>
    <!-- header -->


    <div class="wrapper bg-light lter b-b">
        <div class="btn-toolbar">
                <div class="form-group">
                    <div class="col-sm-4">
                        <label>Start Date:</label><br>
                        <div class="input-group w-md">
                            <input type="text" class="form-control" datepicker-popup="{[{format1}]}" 
                            ng-model="startdate" is-open="opened1" datepicker-options="dateOptions" ng-required="true" close-text="Close" placeholder="Start Date" />
                              <span class="input-group-btn">
                            <button type="button" class="btn btn-default" ng-click="open($event,'opened1')"><i class="glyphicon glyphicon-calendar"></i></button>
                              </span>
                               <input type='hidden' ng-model='start2'>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label>End Date:</label><br>
                        <div class="input-group w-md">
                            <input type="text" class="form-control" datepicker-popup="{[{format2}]}" 
                            ng-model="enddate" is-open="opened2" datepicker-options="dateOptions" ng-required="true" close-text="Close" placeholder="End Date" />
                              <span class="input-group-btn">
                             <button type="button" class="btn btn-default" ng-click="open($event,'opened2')"><i class="glyphicon glyphicon-calendar"></i></button>
                              </span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label>Status:</label><br>
                        <div class="input-group w-md">
                            <select chosen allow-single-deselect class="form-control" ng-model="news.author" ng-options="mem for mem in status" required="required">
                            </select>
                            <input type='hidden' ng-model='stat'>
                        </div>
                    </div>
                   
                    <div class="col-sm-4">
                        <br>
                        <label>Payment Types:</label><br>
                        <div class="input-group w-md">
                            <select chosen allow-single-deselect class="form-control" ng-model="news.payment" ng-options="mem for mem in payment"  required="required">
                            </select>
                             <input type='hidden' ng-model='pay'>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <br>
                        <label>Members</label><br>
                        <div class="input-group w-md">
     <select chosen allow-single-deselect class="form-control" ng-model="news.members" required="required" ng-change='member1(members)' ng-options="x.id as x.name for x in members">

        <option value='{[{x.id}]}'> {[{x.name}]} </option>
                         

             </select>
              <input type='hidden' ng-model='member2'>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <br>
                        <label></label><br>
                        <div class="input-group w-md" style='width:300px'>
        <button class="btn m-b-xs btn-sm btn-primary btn-addon searchbtn" ng-click="searchbtn(startdate,enddate,news.author,news.payment,news.members)">SEARCH</button>
           &nbsp;
          <button class="btn m-b-xs btn-sm btn-info showbtn" ng-click="show()">SHOW ALL</button>
            &nbsp;
        <a class="btn m-b-xs btn-sm btn-danger btn-addon" class='pdf' href='javascript:pdfToHTMLMembers()'>DOWNLOAD AS PDF</a>
     

                        </div>
                    </div>
                </div>
        </div>
    </div>
    <!-- / header -->

    <div class="table-responsive">
        <table class="table table-striped b-t b-light">
            <thead>
            <tr>
              <th></th>
              
                <th>Member</th>
                <th>Service</th>
       
                <th>Scheduled Date</th>
                <th style="width:120px;">Status</th>
            </tr>
            </thead>
            <tbody ng-show="loading">
            <tr>
                <td colspan="5">Loading List</td>
            </tr>
            </tbody>

            <tbody ng-hide="loading">

            <tr ng-show="bigTotalItems==0"> <td colspan="7"> No records found! </td></tr>
            <tr ng-repeat="ss in schedlist">
                <td><icon class="fa fa-circle" style="color: {[{ ss.textColor }]}"></icon></td>
              
                <td>{[{ ss.txtname }]} </td>
                <td>{[{ ss.title }]} <br> <span class="text-muted">{[{ ss.info }]} {[{ ss.hourday }]}</span>  </td>
       
                <td>{[{ ss.txtdate }]} {[{ ss.txtstime }]} - {[{ ss.txtetime }]} <br><span class="text-muted">Date applied: {[{ ss.created_at }]}</span></td>
                <td>
                    <span class="label bg-success" ng-show="ss.status=='DONE'">Done</span>
                    <span class="label bg-primary" ng-show="ss.status=='RESERVED' && !ss.unattended">Reserve</span>
                    <span class="label bg-warning" ng-show="ss.unattended">Unattended</span>
                    <span class="label bg-danger" ng-show="ss.status=='VOID'">Void</span>
                    <span class="label bg-info" ng-show="ss.status=='PENDING'">Pending</span> <br>
                    <span class="text-muted pull-right">Last update: {[{ ss.updated_at }]}</span>
                </td>
            </tr>
            </tbody>
        </table>
    </div>



<!-- with search for pdf -->
  <div class="table-responsive forsearching" style='position:absolute;bottom:200000px;font-size:9px'>
     <canvas> <img src='/img/sedonalogo.png'> </canvas>
        <table class="table table-striped b-t b-light" id='tableExport' style='font-size:9px'>
            <thead>
            <tr>
          
                <th>Member</th>
                <th>Service</th>
                <th>Scheduled Date</th>
                <th style="width:120px;">Status</th>
            </tr>
            </thead>
          
            <tbody ng-hide="loading">
            <tr ng-repeat="ss in allsearch">
             
                <td>{[{ ss.txtname }]} </td>
                <td>{[{ ss.title }]} <br> </td>
                <td>{[{ ss.txtdate }]} <br> {[{ ss.txtstime }]}<br>-{[{ ss.txtetime }]}</td>
                <td>
                   {[{ ss.status }]}
                </td>
            </tr>
            </tbody>
        </table>
    </div>
<!-- with search for pdf -->



<!-- all entries for pdf -->
 <div class="table-responsive allentries" style='position:absolute;bottom:200000px;font-size:9px'>
     <canvas> <img src='/img/sedonalogo.png'> </canvas>
        <table class="table table-striped b-t b-light" style='font-size:9px'>
            <thead>
            <tr>
                <th>Member</th>
                <th>Service</th>
                <th>Scheduled Date</th>
                <th style="width:120px;">Status</th>
            </tr>
            </thead>
          
            <tbody ng-hide="loading">
            <tr ng-repeat="ss in allentry">
             
                <td>{[{ ss.txtname }]} </td>
                <td>{[{ ss.title }]} <br> </td>
                <td>{[{ ss.txtdate }]} <br> {[{ ss.txtstime }]}<br>-{[{ ss.txtetime }]}</td>
                <td>
                   {[{ ss.status }]}
                </td>
            </tr>
            </tbody>
        </table>
    </div>

<!-- all entries for pdf -->

    <footer class="panel-footer">
        <div class="row">
            <div class="col-sm-4 hidden-xs">
            </div>
            <div class="col-sm-5 text-center" ng-hide="bigTotalItems==0 || loading">
                <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
                <pagination ng-hide="maxSize >= bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </div>
        </div>
    </footer>



<script>

$(document).ready(function(){
    $('.forsearching').removeAttr('id');
    $(".allentries").attr("id","pdf2htmldivMembers");
});

$(".searchbtn").click(function(){
    $('.allentries').removeAttr('id');
    $(".forsearching").attr("id","pdf2htmldivMembers");
});


$(".showbtn").click(function(){
    $('.forsearching').removeAttr('id');
    $(".allentries").attr("id","pdf2htmldivMembers");
});

</script>

</div>
