
<script type="text/ng-template" id="testiDelete.html">
    <div ng-include="'/be/tpl/testiDelete.html'"></div>
</script>

<script type="text/ng-template" id="testiEdit.html">
    <div ng-include="'/be/tpl/testiEdit.html'"></div>
</script>

<script type="text/ng-template" id="addTestimonial.html">
    <div ng-include="'/be/tpl/addTestimonial.html'"></div>
</script>


<div class="bg-light lter b-b wrapper-md">
    <h1 class="m-n font-thin h3">Testimonials <button class="btn m-b-xs btn-sm btn-primary btn-addon pull-right" ng-click="addtesti()"><i class="fa fa-plus"></i>Add Testimonials</button></h1>

    <a id="top"></a>
</div>

<!--<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="savePage(page)" name="formpage">-->
    <!--<fieldset ng-disabled="isSaving">-->
        <!--<div class="wrapper-md">-->
            <!--<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>-->

            <!--<div class="row">-->

                <!--<div class="col-sm-12">-->
                    <!--<div class="panel panel-default">-->
                        <!--<div class="panel-heading font-bold">-->
                            <!--List-->
                        <!--</div>-->


                        <!--<div class="panel-body">-->



                            <!--<div class="row wrapper">-->
                                <!--<div class="col-sm-5 m-b-xs">-->
                                    <!--<div class="input-group">-->
                                        <!--<input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">-->
                            <!--<span class="input-group-btn">-->
                            <!--<button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>-->
                            <!--</span>-->
                                    <!--</div>-->
                                <!--</div>-->
                            <!--</div>-->
                            <!--<div class="table-responsive">-->
                                <!--<table class="table table-striped b-t b-light">-->
                                    <!--<thead>-->
                                    <!--<tr>-->

                                        <!--<th style="width:30%">Name</th>-->
                                        <!--<th style="width:35%">Message</th>-->
                                        <!--<th style="width:25%">Picture</th>-->
                                        <!--<th style="width:25%">Action</th>-->
                                    <!--</tr>-->
                                    <!--</thead>-->
                                    <!--<tbody>-->
                                    <!--<tr ng-repeat="mem in data.data">-->

                                        <!--<td>{[{ mem.name }]}</td>-->
                                        <!--<td>{[{ mem.message }]}</td>-->
                                        <!--<td  ng-if="mem.status == 1">-->
                                            <!--<div class="pagestatuscontent fade-in-out"><span class="label bg-success" >Active</span></div>-->
                                            <!--<div class="checkstatuscontent">-->
                                                <!--<label class="i-switch bg-info m-t-xs m-r">-->
                                                    <!--<input type="checkbox" checked="" ng-click="setstatus(mem.status,mem.newsid,searchtext,mem.newslocation,mem.newsslugs)">-->
                                                    <!--<i></i>-->
                                                <!--</label>-->

                                            <!--</div>-->
                                            <!--<div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.newsslugs"><i class="fa fa-check"></i></spand></div>-->
                                        <!--</td>-->
                                        <!--<td  ng-if="mem.status == 0">-->
                                            <!--<div class="pagestatuscontent fade-in-out"><span class="label bg-danger">Deactivated</span></div>-->
                                            <!--<div class="checkstatuscontent">-->
                                                <!--<label class="i-switch bg-info m-t-xs m-r">-->
                                                    <!--<input type="checkbox" ng-click="setstatus(mem.status,mem.newsid,searchtext,mem.newslocation,mem.newsslugs)">-->
                                                    <!--<i></i>-->
                                                <!--</label>-->

                                            <!--</div>-->
                                            <!--<div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow ==mem.newsslugs"><i class="fa fa-check"></i></spand></div>-->
                                        <!--</td>-->
                                        <!--</td>-->
                                        <!--<td>-->
                                            <!--<a href="" ng-click="editnews(mem.newsid)"><span class="label bg-warning" >Edit</span></a>-->
                                            <!--<a href="" ng-click="deletenews(mem.newsid)"> <span class="label bg-danger">Delete</span></a>-->
                                        <!--</td>-->
                                    <!--</tr>-->
                                    <!--</tbody>-->
                                <!--</table>-->
                            <!--</div>-->



                        <!--</div>-->


                    <!--</div>-->
                <!--</div>-->

            <!--</div>-->



        <!--</div>-->
    <!--</fieldset>-->
<!--</form>-->

<div class="wrapper-md">

    <div class="row wrapper">
        <div class="col-sm-5 m-b-xs" ng-show="keyword">
            <strong>{[{ bigTotalItems }]}</strong> Results found for: <strong> "{[{ keyword }]}"</strong> <button class="btn btn-default btn-xs" ng-click="clear()">Clear</button>
        </div>
        <div class="col-sm-5 m-b-xs pull-right">
            <div class="input-group">
                <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                <span class="input-group-btn">
                <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <!-- .comment-list -->
            <div class="m-b b-l m-l-md streamline">
                <div ng-show="loading"> Loading list. </div>
                <div ng-show="bigTotalItems==0"> No records found. </div>
                <div ng-hide="loading" ng-repeat="testi in data.data">
                    <a class="pull-left thumb-sm avatar m-l-n-md" ng-show="testi.picture !=''"><div style="background-image : url('{[{ am }]}/uploads/testimonialpic/{[{ testi.picture }]}') ; width: 40px; height :38px; background-size: cover; background-position: center; background-repeat : no-repeat; border-radius:100%;" alt="..."></div></a>
                    <a class="pull-left thumb-sm avatar m-l-n-md" ng-show="testi.picture == ''"><img src="/img/a0.jpg" alt="..."></a>
                    <div class="m-l-lg panel b-a">
                        <div class="panel-heading pos-rlt b-b b-light">
                            <span class="arrow left"></span>
                            <a href>{[{ testi.name }]}</a>
                      <span class="text-muted m-l-sm pull-right">
                        <i class="fa fa-clock-o"></i>
                        {[{ testi.date }]}
                      </span>
                        </div>
                        <div class="panel-body">
                            <blockquote>
                                <p>{[{ testi.message }]}</p>
                                <span class="label label-info">{[{ testi.title }]}</span>
                            </blockquote>
                            <div class="m-t-sm pull-right">
                                <a href class="btn btn-default btn-xs" ng-click="editTesti(testi.id)">
                                    <i class="fa fa-edit"></i>
                                    Edit
                                </a>
                                <a href class="btn btn-default btn-xs" ng-click="deletetesti(testi.id)"><i class="fa fa-times"></i> Delete</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    <div class="row" ng-hide="bigTotalItems==0 || loading">
        <div class="panel-body">
            <footer class="panel-footer text-center bg-light lter">
                <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
                <pagination ng-hide="maxSize > bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </footer>
        </div>
    </div>
</div>