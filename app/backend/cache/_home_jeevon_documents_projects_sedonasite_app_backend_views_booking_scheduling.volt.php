<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="paymentModal.html">
    <div ng-include="'/be/tpl/paymentModal.html'"></div>
</script>
<script type="text/ng-template" id="voidForm.html">
    <div ng-include="'/be/tpl/voidForm.html'"></div>
</script>
<div>
    <div class="wrapper-md bg-light b-b">
        <button type="button" class="btn btn-default btn-addon pull-right m-t-n-xs" ng-click="renderCalender(calendar1); showOfflineReg = !showOfflineReg;closeedit(); table = false;">
            <i class="fa fa-bookmark"></i> Add Reservation
        </button>
        <h1 class="m-n font-thin h3">Reservation Management</h1>
    </div>
    <div class="hbox hbox-auto-xs hbox-auto-sm">
        <div class="wrapper-md" style="padding-bottom: 0px;">
            <div class="clearfix m-b">
                <span class="badge bg-danger badge-sm m-l-xs">{[{ counts.void }]}</span> Void
                <span class="badge bg-success badge-sm m-l-xs">{[{ counts.done }]}</span> Done
                <span class="badge bg-warning badge-sm m-l-xs">{[{ counts.unattended }]}</span> Unattended
                <span class="badge bg-primary badge-sm m-l-xs">{[{ counts.reserved }]}</span> Reserved
                <span class="badge bg-info badge-sm m-l-xs">{[{ counts.pending }]}</span> Pending
            </div>
        </div>
    </div>
    <div class="hbox hbox-auto-xs hbox-auto-sm" ng-show="table">
        <div class="col wrapper-md">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Booking List
                    <div class="pull-right">
                        <a href="" ng-click="table=false;"><icon class="fa fa-calendar"></icon> Back to Calendar</a>
                    </div>
                </div>
                <div class="row wrapper">
                    <div class="col-sm-4 m-b-xs" >
                        <span ng-show="keyword"><strong>{[{ bigTotalItems }]}</strong> Results found for: <strong> "{[{ keyword }]}"</strong> <button class="btn btn-default btn-xs" ng-click="clearsearch()">Clear</button></span>
                        <br><span ng-show="txtdate">Filter by: <strong> "{[{ txtdate }]}"</strong> <button class="btn btn-default btn-xs" ng-click="clearfilter()">Clear</button></span>
                    </div>

                    <div class="col-sm-4 pull-right text-right">
                        <div class="input-group">
                            <input type="text" class="input-sm form-control" ng-model="searchkeyword" placeholder="Search">
                              <span class="input-group-btn">
                                <button class="btn btn-sm btn-default" type="button" ng-click="search(searchkeyword, filterdate);">Go!</button>
                              </span>
                        </div>
                        <br>
                        <div class="btn-group m-b-xs">
                            <label class="btn btn-xs ng-valid ng-dirty active" ng-model="searchStatus" btn-radio="'ALL'"><i class="fa fa-check text-active"></i> All </label>
                            <label class="btn btn-xs btn-primary ng-valid ng-dirty" ng-model="searchStatus" btn-radio="'RESERVED'"><i class="fa fa-check text-active"></i> Reserved</label>
                            <label class="btn btn-xs btn-info ng-valid ng-dirty" ng-model="searchStatus" btn-radio="'PENDING'"><i class="fa fa-check text-active"></i> Pending</label>
                            <label class="btn btn-xs btn-success ng-valid ng-dirty" ng-model="searchStatus" btn-radio="'DONE'"><i class="fa fa-check text-active"></i> Done</label>
                            <label class="btn btn-xs btn-warning ng-valid ng-dirty" ng-model="searchStatus" btn-radio="'UNATTENDED'"><i class="fa fa-check text-active"></i> Unattended</label>
                            <label class="btn btn-xs btn-danger ng-valid ng-dirty" ng-model="searchStatus" btn-radio="'VOID'"><i class="fa fa-check text-active"></i> Void</label>
                        </div>
                    </div>
                    <div class="col-sm-4 pull-right">
                        <div class="col-sm-4">
                            <select ng-model="filterdate.day" class="input-sm form-control" ng-options="d.val as d.val for d in days" required>
                                <option value="">Day</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <select ng-model="filterdate.year" class="input-sm form-control" ng-options="y.val as y.val for y in years" required>
                                <option value="">Year</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <select ng-model="filterdate.month" class="input-sm form-control" ng-options="m.val as m.name for m in months" required>
                                <option value="">Month</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Invoice No</th>
                            <th>Member</th>
                            <th>Service</th>
                            <th>Payment Type</th>
                            <th>Scheduled Date</th>
                            <th style="width:120px;">Status</th>
                            <th style="width:120px;"></th>
                        </tr>
                        </thead>
                        <tbody ng-show="loading">
                        <tr>
                            <td colspan="5">Loading List</td>
                        </tr>
                        </tbody>
                        <tbody ng-hide="loading">
                            <tr  ng-show="bigTotalItems==0"> <td colspan="5"> No records found! </td></tr>
                            <tr ng-repeat="ss in schedlist">
                                <td><icon class="fa fa-circle" style="color: {[{ ss.textColor }]}"></icon></td>
                                <td><a href="" ng-click="openinvoice(ss.invoiceno)">{[{ ss.invoiceno }]}</a></td>
                                <td>{[{ ss.txtname }]} </td>
                                <td>{[{ ss.title }]} <br> <span class="text-muted">{[{ ss.info }]} {[{ ss.hourday }]}</span></td>
                                <td>{[{ ss.paymenttype }]}</td>
                                <td>{[{ ss.txtdate }]} {[{ ss.txtstime }]} - {[{ ss.txtetime }]} <br><span class="text-muted">Date applied: {[{ ss.created_at }]}</span></td>
                                <td>
                                    <span class="label bg-success" ng-show="ss.status=='DONE'">Done</span>
                                    <span class="label bg-primary" ng-show="ss.status=='RESERVED' && !ss.unattended">Reserve</span>
                                    <span class="label bg-warning" ng-show="ss.unattended">Unattended</span>
                                    <span class="label bg-danger" ng-show="ss.status=='VOID'">Void</span>
                                    <span class="label bg-info" ng-show="ss.status=='PENDING'">Pending</span> <br>
                                    <span class="text-muted pull-right">Last update: {[{ ss.updated_at }]}</span>
                                </td>
                                <td>
                                    <a href="" ng-click="gotodate(ss.start, false)"><span class="label bg-info" ><i class="fa fa-mail-reply-all"></i></i></span></a>
                                    <a href="" ng-click="gotodate(ss.start, true, ss)"> <span class="label bg-success"><i class="fa fa-pencil"></i></span></a>
                                    <a href="" ng-click="printinvoice(ss.invoiceno)"> <span class="label bg-success"><i class="fa fa-print"></i></span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-4 hidden-xs">
                        </div>
                        <div class="col-sm-4 text-center" ng-hide="bigTotalItems==0 || loading">
                            <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
                            <pagination ng-hide="maxSize >= bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>
    <div class="hbox hbox-auto-xs hbox-auto-sm" ng-show="!table">
        <div class="col wrapper-md">
            <div class="clearfix m-b">
                <button type="button" class="btn btn-sm btn-primary btn-addon" ng-click="tablelist()">
                    <i class="fa fa-plus"></i>List View
                    <!--<div ng-repeat="e in events" class="bg-white-only r r-2x m-b-xs wrapper-sm {[{e.className[0]}]}">-->
                        <!--<input ng-model="e.title" class="form-control m-t-n-xs no-border no-padder no-bg">-->
                        <!--<a class="pull-right text-xs text-muted" ng-click="remove($index)"><i class="fa fa-trash-o"></i></a>-->
                        <!--<div class="text-xs text-muted">{[{e.start | date:"MMM dd"}]} - {[{e.end | date:"MMM dd"}]}</div>-->
                    <!--</div>-->
                </button>
                <div class="pull-right">
                    <button type="button" class="btn btn-sm btn-default" ng-click="today('calendar1')">today</button>
                    <div class="btn-group m-l-xs">
                        <button class="btn btn-sm btn-default" ng-click="changeView('agendaDay', 'calendar1')">Day</button>
                        <button class="btn btn-sm btn-default" ng-click="changeView('agendaWeek', 'calendar1')">Week</button>
                        <button class="btn btn-sm btn-default" ng-click="changeView('month', 'calendar1')">Month</button>
                    </div>
                </div>
            </div>
            <div class="pos-rlt">
                <div class="fc-overlay">
                    <div class="panel bg-white b-a pos-rlt">
                        <span class="arrow"></span>
                        <div class="h4 font-thin m-b-sm">{[{event.txtname}]}
                            <!--<a href="" class="pull-right" ng-click="editsched(event); showOfflineReg = false;"><i class=" icon-note  text-muted m-r-xs"></i></a>-->
                        </div>
                        <div class="line b-b b-light"></div>
                        <div><i class="icon-calendar text-muted m-r-xs"></i> {[{event.txtdate }]}</div>
                        <div><i class="icon-clock text-muted m-r-xs"></i> {[{event.txtstime }]} to {[{event.txtetime }]} ({[{ event.hourday }]})</div>
                        <div><i class="icon-envelope  text-muted m-r-xs"></i> {[{ event.email }]} <i class="icon-call-out  text-muted m-r-xs"></i> {[{event.phoneno }]} </div>
                        <div><i class="icon-wallet text-muted m-r-xs"></i> <a href="" ng-click="openinvoice(event.invoiceno)">{[{event.invoiceno}]} </a></div>
                        <div class="m-t-sm">{[{event.info}]} </div>
                        <div class="m-t-sm">Date Created: {[{ event.created_at }]}</div>
                    </div>
                </div>
                <div class="calendar" ng-model="eventSources" calendar="calendar1" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div>
            </div>
            
        </div>
        <div class="col w-auto-xs bg-light dk bg-auto b-l" id="bside" style="position: relative" ng-show="editingsched">
            <div class="overlay" ng-show="processing">
                <div class="loader">
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                </div>
            </div>
            <div class="wrapper">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Updating Schedule <a href="" ng-click="closeedit()" class="pull-right"><icon class="fa  fa-times-circle"></icon></a>
                    </div>
                    <div class="row wrapper" style="margin: 0px 5px;">
                        <div class="m-t">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Reservation Information
                                    <span class="label bg-success pull-right" ng-show="editingval.status=='DONE'">Done</span>
                                    <span class="label bg-primary pull-right" ng-show="editingval.status=='RESERVED' && !editingval.unattended">Reserved</span>
                                    <span class="label bg-warning pull-right" ng-show="editingval.unattended">Unattended</span>
                                    <span class="label bg-danger pull-right" ng-show="editingval.status=='VOID'">Void</span>
                                    <span class="label bg-info pull-right" ng-show="editingval.status=='PENDING'">Pending</span>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped b-t b-light">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <div class="h4 font-thin m-b-sm">{[{editingval.txtname}]}
                                                    <span ng-hide="editingval.status=='PENDING'">
                                                        <button class="btn btn-xs btn-success pull-right" ng-click="changeStat(editingval.schedid, 'DONE')" ng-hide="editingval.status=='DONE'"><i class="fa fa-check "></i> Done</button>
                                                        <button class="btn btn-xs btn-warning pull-right" ng-click="changeStat(editingval.schedid, 'RESERVED')" ng-show="editingval.status=='VOID' || editingval.status=='DONE' "><i class="fa fa-check "></i> Reserved</button>
                                                        <button class="btn btn-xs btn-danger pull-right" ng-click="voidForm(editingval.schedid, 'VOID')" ng-hide="editingval.status=='VOID'"><i class="fa fa-ban"></i> Void</button>
                                                    </span>
                                                    <span ng-show="editingval.status=='PENDING'">
                                                        <button class="btn btn-xs btn-info pull-right" ng-click="changeStat(editingval.schedid, 'RESERVED'); editingval.status='RESERVED'"><i class="fa fa-check"></i> Approve Schedule</button>
                                                    </span>
                                                </div>
                                                <div class="line b-b b-light"></div>
                                                <div><i class="icon-calendar text-muted m-r-xs"></i> {[{editingval.txtdate }]}</div>
                                                <div><i class="icon-clock text-muted m-r-xs"></i> {[{editingval.txtstime }]} to {[{editingval.txtetime }]} ({[{ editingval.hourday }]})</div>
                                                <div><i class="icon-envelope  text-muted m-r-xs"></i> {[{ editingval.email }]} <i class="icon-call-out  text-muted m-r-xs"></i> {[{editingval.phoneno }]} </div>
                                                <div><i class="icon-wallet text-muted m-r-xs"></i> <a href="" ng-click="openinvoice(event.invoiceno)">{[{editingval.invoiceno}]} </a></div>
                                                <div class="m-t-sm">{[{editingval.info}]}</div>
                                                <div class="pull-right">Date created: {[{ event.created_at }]} Last update: {[{ event.updated_at }]}</div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="line line-dashed b-b line-lg"></div>
                            <h5>Want to move the schedule?</h5>
                            <p><em class="text-muted">If you want to move the schedule to another date, just select a new date below and click update schedule.</em></p>
                            <div class="col-xs-8 no-padding">
                                Select Date:
                                <datepicker ng-model="sched.reservation" min-date="minDate" show-weeks="true" class="datepicker"></datepicker>
                            </div>
                            <div class="col-xs-4 no-padding">
                                Select Your Time:
                                <timepicker ng-model="sched.mytime" ng-change="changedtime(sched.mytime)" hour-step="hstep" minute-step="mstep" show-meridian="ismeridian"></timepicker>
                                <div>
                                    Computed Time:
                                    <input type="hidden" name="endtime" ng-model="sched.endtime">
                                    <button class="btn m-b-xs btn-sm btn-info"><i class="fa fa-clock-o"></i> {[{ computedtime }]}</button>
                                    <em class="text-muted">The computed time is base on your selected service time with the start time.</em>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="m-t m-b text-right" style="margin: 10px;">
                        <button class="btn btn-success" ng-click="updateschedule(sched)">
                            <span class="text">Update</span>
                        </button>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col w-auto-xs bg-light dk bg-auto b-l" id="aside" style="position: relative" ng-show="showOfflineReg">
            <div class="overlay" ng-show="processing">
                <div class="loader">
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                </div>
            </div>
            <div class="wrapper">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Offline Reservation
                            <a href="" ng-click="showOfflineReg = false;" class="pull-right"><icon class="fa  fa-times-circle"></icon></a>
                        </div>

                        <div class="row wrapper" style="margin: 0px 5px;">
                            <p ng-show="invoice">Your reservation was successfuly saved! Click here if invoice did not open: <a href="" ng-click="openinvoice(invoice)">{[{ invoice }]}</a></p>
                            <tabset class="tab-container">
                                <tab heading="User Information" active="steps.step1" select="steps.percent=3">
                                    <p class="m-b">Please complete the requested information.</p>
                                    <progressbar value="steps.percent" class="progress-xs" type="success"></progressbar>
                                    <form name="firststep1" id="firststep1" novalidate class="form-validation">
                                        <p>Email: * </p>
                                        <input type="email" name="email" class="form-control" ng-model="user.email" required ng-change="firststep1.email.$valid ? (steps.percent=4) : (steps.percent = steps.percent - 3)" ng-blur="checkEmail(user.email)">
                                        <span class="label bg-danger" ng-show="existinguser && emailexist!=3">This email is already been used!</span> <span class="label bg-info" ng-show="existinguser && emailexist!=3">Use as existing member? <a href="" ng-click="useuser();">YES</a></span>
                                        <p class="m-t">Last name: *</p>
                                        <input type="text" name="lastname" class="form-control" ng-model="user.lastname" ng-disabled="emailexist != 2" required ng-change="firststep1.lastname.$valid ? (steps.percent=8) : (steps.percent = steps.percent - 3)">
                                        <p class="m-t">First name: *</p>
                                        <input type="text" name="firstname" class="form-control" ng-model="user.firstname" ng-disabled="emailexist != 2" required ng-change="firststep1.firstname.$valid ? (steps.percent=12) : (steps.percent = steps.percent - 3)">
                                        <p class="m-t">Address 1: *</p>
                                        <input type="text" name="address1" class="form-control" ng-model="user.address1" ng-disabled="emailexist != 2" required ng-change="firststep1.address1.$valid ? (steps.percent=16) : (steps.percent = steps.percent - 3)">
                                        <p class="m-t">Address 2:</p>
                                        <input type="text" name="address2" class="form-control" ng-model="user.address2" ng-disabled="emailexist != 2">
                                        <p class="m-t">Country: *</p>
                                        <select onchange="print_state('state', this.selectedIndex);" ng-disabled="emailexist != 2" ng-change="user.state=''" id="country" name="country" ng-model="user.country" class="form-control" required="required">
                                        </select>
                                        <p class="m-t">State: *</p>
                                        <select name ="state" id ="state" class="form-control" ng-model="user.state" ng-disabled="emailexist != 2" required ng-change="firststep1.state.$valid ? (steps.percent=24) : (steps.percent = steps.percent - 3)">
                                        </select>
                                        <p class="m-t">Zip Code / Postal: *</p>
                                        <input type="text" name="zipcode" class="form-control" ng-model="user.zipcode" ng-disabled="emailexist != 2" required ng-change="firststep1.zipcode.$valid ? (steps.percent=28) : (steps.percent = steps.percent - 3)" only-digits>
                                        <p class="m-t">Phone number: *</p>
                                        <input type="text" name="phoneno" class="form-control" ng-model="user.phoneno" ng-disabled="emailexist != 2" required ng-change="firststep1.phoneno.$valid ? (steps.percent=32) : (steps.percent = steps.percent - 3)" only-digits>

                                        <div class="m-t m-b">
                                            <button type="submit" ng-disabled="firststep1.$invalid" class="btn btn-default btn-rounded" ng-click="steps.step2=true;">Next</button>
                                        </div>
                                    </form>
                                </tab>
                                <tab heading="Reservation Details" disabled="firststep1.$invalid" active="steps.step2">
                                    <form name="step2" class="form-validation">
                                        <p class="m-b">Continue the next step</p>
                                        <progressbar value="steps.percent" class="progress-xs" type="success"></progressbar>
                                        <p class="m-t">Select service:</p>

                                        <select chosen allow-single-deselect class="chosen-choices form-control" options="pagelist" ng-options="pl as pl.title group by pl.cattitle for pl in pagelist"  ng-model="sched.service" ng-change="selectServiceCat(sched.service)" required="required">
                                        </select>

                                        <em class="text-muted">By selecting the service, price options and their correspoding hours will appear below.</em>

                                        <p class="m-t" ng-show="servicepricelist">Select Prices:</p>
                                        <div class="radio" ng-repeat="spl in servicepricelist" ng-show="servicepricelist">
                                            <label class="i-checks">
                                                <input type="radio" name="a" value="{[{ spl }]}" ng-model="sched.servicechoice" required="required" ng-click="priceselected(sched.servicechoice)">
                                                <i></i>
                                                <strong>${[{ spl.price }]} </strong> - <strong> {[{ spl.hourday }]} </strong> {[{ spl.description }]}
                                            </label>
                                        </div>

                                        <div class="m-t" ng-show="servicepricelist">
                                            <div class="col-xs-8 no-padding">
                                                Select Date:
                                                <datepicker ng-model="sched.reservation" min-date="minDate" show-weeks="true" class="datepicker"></datepicker>
                                            </div>
                                            <div class="col-xs-4 no-padding">
                                                Select Your Time:
                                                <timepicker ng-model="sched.mytime" ng-change="changedtime(sched.mytime)" hour-step="hstep" minute-step="mstep" show-meridian="ismeridian"></timepicker>
                                                <div ng-show="sched.servicechoice">
                                                    Computed Time:
                                                    <input type="hidden" name="endtime" ng-model="sched.endtime">
                                                    <button class="btn m-b-xs btn-sm btn-info"><i class="fa fa-clock-o"></i> {[{ computedtime }]}</button>
                                                    <em class="text-muted">The computed time is base on your selected service time with the start time.</em>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="line line-dashed b-b line-lg"></div>
                                        <span class="label bg-danger" ng-show="conflict">The schedule you are trying to add is already reserved to another client.</span>
                                        <div class="panel panel-default" ng-show="user.reservationlist.length > 0">
                                            <div class="panel-heading">
                                                Reservation Lists
                                                <input type="hidden" name="reservationlist" ng-model="user.reservationlist" required="required">
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table table-striped b-t b-light">
                                                    <thead>
                                                    <tr>
                                                        <th>Description</th>
                                                        <th>Amount</th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr ng-repeat="list in user.reservationlist">
                                                        <td>
                                                            {[{ list.service.title }]}
                                                            <p class="text-muted">{[{ list.servicechoice.description }]} <br> <strong>{[{ list.servicechoice.hourday }]} ({[{ list.text }]})</strong></p>
                                                        </td>
                                                        <td>
                                                            ${[{ list.servicechoice.price }]}
                                                        </td>
                                                        <td style="width: 10px;">
                                                            <a href="" ng-click="deleteService($index)" class="active"><i class="fa fa-times text-danger text-active"></i></a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="m-t m-b">
                                            <button type="button" class="btn btn-default btn-rounded" ng-click="steps.step1=true; ">Prev</button>
                                            <button type="button" ng-disabled="user.reservationlist.length <= 0" class="btn btn-default btn-rounded" ng-click="steps.step3=true">Proceed to Payment</button>
                                            <button type="button" ng-disabled="!sched.servicechoice" class="btn btn-default btn-rounded" ng-click="saveSched(sched);">Save</button>
                                        </div>
                                    </form>
                                </tab>
                                <tab heading="Payment Option" disabled="step2.$invalid" active="steps.step3" select="steps.percent=75">
                                    <p class="m-b">Congraduations! You got the last step.</p>
                                    <progressbar value="steps.percent" class="progress-xs" type="success"></progressbar>
                                    <p>We are almost there, please verify the information  you entered.</p>
                                    <h4>{[{ user.firstname }]} {[{ user.lastname }]}</h4>
                                    <p>
                                        {[{ user.email }]} {[{ user.phoneno }]} <br>
                                        {[{  user.address1 }]}, <br>
                                        {[{  user.address2 }]}, <br>
                                        {[{  user.country }]}, {[{  user.state }]} {[{  user.zipcode }]}
                                    </p>
                                    <div class="panel panel-default" ng-show="user.reservationlist.length > 0">
                                        <div class="panel-heading">
                                            Reservation Lists
                                            <input type="hidden" name="reservationlist" ng-model="user.reservationlist" required="required">
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-striped b-t b-light">
                                                <thead>
                                                    <tr>
                                                        <th>Description</th>
                                                        <th>Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <tr ng-repeat="list in user.reservationlist">
                                                    <td>
                                                        {[{ list.service.title }]}
                                                        <p class="text-muted">{[{ list.servicechoice.description }]} <br> <strong>{[{ list.servicechoice.hourday }]} ({[{ list.text }]})</strong></p>
                                                    </td>
                                                    <td>
                                                        ${[{ list.servicechoice.price }]}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right">
                                                        <strong>Total</strong>
                                                    </td>
                                                    <td class="text-left">



                                                        <strong>${[{ getTotal() }]}</strong>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    Payment Types:
                                    <div class="m-b-sm">
                                        <div class="btn-group" ng-init="radioModel = 'cash'">
                                            <label class="btn btn-sm btn-info ng-valid ng-dirty" ng-model="radioModel" btn-radio="'cash'" ng-click="radioModel = 'cash'"><i class="fa fa-check text-active"> </i> <i class="fa fa-money"></i> Cash</label>
                                            <label class="btn btn-sm btn-info ng-valid ng-dirty" ng-model="radioModel" btn-radio="'card'" ng-click="paymentModal('card', user); radioModel = 'card'" > <i class="fa fa-check text-active"></i> <i class="fa fa-credit-card"></i> Card</label>
                                            <label class="btn btn-sm btn-info ng-valid ng-dirty active" ng-model="radioModel" btn-radio="'check'" ng-click="paymentModal('check', user);radioModel = 'check'"> <i class="fa fa-check text-active"></i> <i class="fa fa-edit"></i> E-Check</label>
                                        </div>
                                    </div>
                                    <div class="line line-dashed b-b line-lg"></div>
                                    <div class="panel panel-default" ng-show="user.billinginfo && user.billinginfo.paymenttype == radioModel">
                                        <div class="panel-heading">
                                            Billing Info
                                            <input type="hidden" name="billinginfo" ng-model="user.billinginfo" required="required" ng-change="step3.billinginfo.$valid ? (steps.percent=100) : (steps.percent = steps.percent - 25)">
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-striped b-t b-light">
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <h4>{[{ user.billinginfo.billingfname }]} {[{ user.billinginfo.billinglname }]}</h4>
                                                        <p >{[{ user.billinginfo.al1 }]} <br>
                                                            {[{ user.billinginfo.al2 }]} <br>
                                                            {[{ user.billinginfo.state }]}, {[{ user.billinginfo.country.name }]} {[{ user.billinginfo.zip }]}
                                                        </p>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="m-t m-b text-right">
                                        <button type="button" class="btn btn-default btn-rounded pull-left" ng-click="steps.step2=true">Prev</button>
                                        <!--<button type="button" class="btn btn-default btn-rounded pull-right" ng-click="steps.percent=100">Final</button>-->
                                        <button class="btn btn-success" ng-show="radioModel == 'cash' || (user.billinginfo && user.billinginfo.paymenttype == radioModel)" ng-click="submit(user, firststep1, radioModel)">
                                            <span class="text">Submit</span>
                                        </button>
                                        <div class="clearfix"></div>
                                    </div>
                                </tab>
                            </tabset>
                        </div>
                    </div>
            </div>
        </div>
    </div>

</div>
<div class="hbox hbox-auto-xs hbox-auto-sm">
    <div class="wrapper-md" style="padding-bottom: 0px;">
        <div class="clearfix m-b">
            <span ng-repeat="history in pagescatlegen"> <icon class="fa fa-circle" style="color: {[{ history.colorlegend }]} "></icon> {[{ history.title }]} </span>
        </div>
    </div>
</div>
<br><br><br>
<script>
    $(document).ready(function() {
        if ($('#country').length) {
            print_country("country");
            print_state('state', 239);
        }
    });
</script>