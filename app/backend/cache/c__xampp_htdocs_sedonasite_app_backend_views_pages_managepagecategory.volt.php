<?php echo $this->getContent(); ?>

<script type="text/ng-template" id="pageDelete.html">
  <div ng-include="'/be/tpl/pageDelete.html'"></div>
</script>

<script type="text/ng-template" id="pagecategoryDelete.html">
  <div ng-include="'/be/tpl/pagecategoryDelete.html'"></div>
</script>

<script type="text/ng-template" id="pagecategoryAdd.html">
  <div ng-include="'/be/tpl/pagecategoryAdd.html'"></div>
</script>

<script type="text/ng-template" id="pagecategoryEdit.html">
  <div ng-include="'/be/tpl/pagecategoryEdit.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Service Category <a href="" class="pull-right btn m-b-xs btn-sm btn-primary btn-addon" ng-click="addcategory()"><i class="fa fa-plus"></i>Add new category</a></h1>
  <a id="top"></a>
</div>

<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

    <div class="row">

      <div class="col-sm-12">

        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            Service Category List
          </div>

          <div class="panel-body">
            
            <div class="row wrapper">
              <div class="col-sm-5 m-b-xs" ng-show="keyword">
                <strong>{[{ bigTotalItems }]}</strong> Results found for: <strong> "{[{ keyword }]}"</strong> <button class="btn btn-default btn-xs" ng-click="clear()">Clear</button>
              </div>
              <div class="col-sm-5 m-b-xs pull-right">
                <div class="input-group">
                  <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                            <span class="input-group-btn">
                            <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                            </span>
                </div>
              </div>
            </div>

            <div class="table-responsive">
              <table class="table table-striped b-t b-light">
                <thead>
                  <tr>
                    <th style="width:15%">Category name</th>
                    <th style="width:15%">Category slugs</th>
            <!--         <th style="width:75%">Category Sub Title</th> -->
                    <th style="width:25%">Action</th>
                  </tr>
                </thead>
                <tbody ng-show="loading">
                <tr >
                  <td colspan="3">Loading News</td>
                </tr>
                </tbody>
                <tbody ng-hide="loading">
                <tr  ng-show="bigTotalItems==0"> <td colspan="3"> No records found! </td></tr>
                  <tr ng-repeat="mem in data.data" >
                    <td> {[{ mem.title }]} </span>
                    </td>
                     <td> {[{ mem.slugs }]} </span>
                    </td>
                    <!-- <td> {[{ mem.subtitle }]}</span>
                    </td> -->
                    <td>
                      <a href="" ng-click="editcategory(mem.id, mem.title, mem.subtitle, mem.colorlegend,mem.slugs)" ng-hide="textBtnForm.$visible"><span class="label bg-warning" >Edit</span></a>
                      <a href="" ng-click="deletecategory(mem.id)" ng-hide="textBtnForm.$visible"> <span class="label bg-danger">Delete</span>
                      </a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>


        </div>
      </div>

    </div>

    <div class="row" ng-hide="bigTotalItems==0 || loading">
      <div class="panel-body">
        <footer class="panel-footer text-center bg-light lter">
          <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
          <pagination ng-hide="maxSize >= bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
        </footer>
      </div>
    </div>

  </div>
</fieldset>