<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="success.html">
  <div ng-include="'/be/tpl/success.html'"></div>
</script>
<script type="text/ng-template" id="imagelist.html">
  <div ng-include="'/be/tpl/imagelist.html'"></div>
</script>
<script type="text/ng-template" id="notification.html">
  <div ng-include="'/be/tpl/notification.html'"></div>
</script>
<script type="text/ng-template" id="stylepreview_homepage">
  <div ng-include="'/be/tpl/stylepreview_homepage.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Update Homepage</h1>
  <a id="top"></a>
</div>

<div class="wrapper-md">
  <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="updateData(content)" name="contentform" id="contentform">
    <fieldset ng-disabled="isSaving">
        <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
        <div class="col-md-8">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">Content</div>
            <div class="panel-body">

              <div class="form-group hiddenoverflow">
                <label class="col-sm-2 control-label">Title</label>
                <div class="col-sm-10">
                  <input type="text"  id="" name="" ng-space class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="content.title" required="required">
                </div>
              </div>
              <div class="line line-dashed b-b line-lg"></div>

              <div class="form-group hiddenoverflow">
                <label class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                  <textarea class="form-control resize-v" ng-model="content.body" required="required" rows="4"></textarea>
                </div>
              </div>
               <div class="line line-dashed b-b line-lg"></div>

              <div class="form-group hiddenoverflow">
                <label class="col-sm-2 control-label">Background Image</label>
                <div class="col-sm-10">
                  <center><img ng-if='amazonpath == undefinded'id="backhomepage" src="{[{base_url}]}/img/defaultback.jpg">
                  <input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon" >
                  <img class="customimg" id="backhomepage" ng-if='amazonpath != undefinded' src="<?php echo $this->config->application->amazonlink; ?>/uploads/homepagebackground/{[{amazonpath}]}">
                  <input type="hidden" class="form-control" ng-value="content.backgroundimg = amazonpath " ng-model="content.backgroundimg" placeholder="{[{amazonpath}]}" readonly>
                  <a class="label_backhome_pic control-label btn btn-primary" ng-click="showimageList('lg')" required="required">Change Picture</a>
                </div>
              </div>
            </div>
          </div>
        </div> 

    <div class="col-sm-4">
      <div class="panel panel-default">
        <div class="panel-heading font-bold">Button
          <em>(Optional)</em>
          <a class="pull-right text-info" data-placement="left" data-toggle="popover" title="Help" data-content="Please leave the button name blank if you do not want to appear it in frontend. Thank you!">
            <i class="icon-question text-info" title="Help"></i>
          </a>
        </div>
        <div class="panel-body">
          <div class="form-group hiddenoverflow">
            <label class="col-sm-2 control-label">Button Name</label>
            <div class="col-sm-10">
              <input type="text" name="" ng-space class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="content.btnname">
            </div>
          </div>
          <div class="line line-dashed b-b line-lg"></div>

          <div class="form-group hiddenoverflow">
            <label class="col-sm-2 control-label">Button Link</label>
            <div class="col-sm-10">
              <input type="text" name="" ng-space class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="content.btnlink" placeholder="e.g.: http://www">
            </div>
          </div>

        </div>
      </div>

      <div class="panel panel-default">
        <div class="panel-heading font-bold">Style
          <em>(Optional)</em>
          <a class="btn m-b-xs btn-xs btn-info pull-right" ng-click="stylepreview(content, amazonpath)"><i class="icon icon-eyeglasses"></i> Quick Preview</a>
        </div>
        <div class="panel-body">

          <div class="form-group">
            <label class="col-sm-3 control-label">Background Color</label>
            <div class="col-sm-9 wrapper-sm">
              <input colorpicker ng-model="content.bgcolor" type="text" class="form-control pull-left width90px pointer" title="Click me!" readonly>
              <div class="colorpicker-colorbox form-control pull-left" style='width:50px; background: {[{content.bgcolor}]} '> </div>
            </div>
          </div>
          <div class="line line-dashed b-b line-lg"></div>

          <div class="form-group">
            <label class="col-sm-3 control-label">Title text color</label>
            <div class="col-sm-9 wrapper-sm">
              <input colorpicker ng-model="content.titlecolor" type="text" class="form-control pull-left width90px pointer" title="Click me!" readonly>
              <div class="colorpicker-colorbox form-control pull-left" style='width:50px; background: {[{content.titlecolor}]} '> </div>
            </div>
          </div>
          <div class="line line-dashed b-b line-lg"></div>

          <div class="form-group">
            <label class="col-sm-3 control-label">Title Font-Size</label>
            <div class="col-sm-9 wrapper-sm">
              <ui-select tagging tagging-label="false"
              ng-model="content.titlesize" theme="bootstrap" ng-disabled="disabled" style="" title="Choose font">
              <ui-select-match placeholder="Select font-size...">  {[{ content.titlesize }]}</ui-select-match>
                <ui-select-choices repeat="ta in tag">
                {[{ ta }]}
                </ui-select-choices>
              </ui-select>
             <label><em class="text-muted">For sizes that doesnt exist just type and press enter. (Default: 44px)</em></label>
            </div>
          </div>
          <div class="line line-dashed b-b line-lg"></div>

          <div class="form-group">
            <label class="col-sm-3 control-label">Description text color</label>
            <div class="col-sm-9 wrapper-sm">
              <input colorpicker ng-model="content.descriptioncolor" type="text" class="form-control pull-left width90px pointer" title="Click me!" readonly>
              <div class="colorpicker-colorbox form-control pull-left" style='width:50px; background: {[{content.descriptioncolor}]} '> </div>
            </div>
          </div>
          <div class="line line-dashed b-b line-lg"></div>

          <div class="form-group">
            <label class="col-sm-3 control-label">Description Font-Size</label>
            <div class="col-sm-9 wrapper-sm" ng-init="content.descriptionsize = 20">
              <ui-select tagging tagging-label="false"
              ng-model="content.descriptionsize" theme="bootstrap" ng-disabled="disabled" style="" title="Choose font">
              <ui-select-match placeholder="Select font-size...">  {[{ content.descriptionsize }]}</ui-select-match>
                <ui-select-choices repeat="ta in tag">
                {[{ ta }]}
                </ui-select-choices>
              </ui-select>
             <label><em class="text-muted">For sizes that doesnt exist just type and press enter. (Default: 20px)</em></label>
            </div>
          </div>
          <div class="line line-dashed b-b line-lg"></div>

          <div class="form-group">
            <div class="col-sm-3">Text Align</div>
            <div class="col-sm-9 wrapper-sm">
              <div class="btn-group">
                <label class="btn btn-primary" ng-model="content.position" btn-radio="'Left'">Left</label>
                <label class="btn btn-primary" ng-model="content.position" btn-radio="'Center'">Center</label>
                <label class="btn btn-primary" ng-model="content.position" btn-radio="'Right'">Right</label>
              </div>
            </div>
          </div>
          <div class="line line-dashed b-b line-lg"></div>

          <div class="form-group">
            <div class="col-sm-3">Text Box</div>
            <div class="col-sm-9 wrapper-sm">
              <div class="btn-group">
                <label class="btn btn-default" ng-model="content.box" btn-radio="'on'">On</label>
                <label class="btn btn-default" ng-model="content.box" btn-radio="'off'">Off</label>
              </div>
            </div>
          </div>
          <div class="line line-dashed b-b line-lg"></div>

          <div class="form-group">
            <div class="col-sm-3">Banner Size</div>
            <div class="col-sm-9 wrapper-sm">
              <div class="btn-group">
                <label class="btn btn-default" ng-model="content.banneroption" btn-radio="'regular'">Regular</label>
                <label class="btn btn-default" ng-model="content.banneroption" btn-radio="'custom'">Customize</label>
              </div>
            </div>
            <div ng-if="content.banneroption=='custom'">
            <div class="col-sm-4">
              <input type="text" class="form-control" value="1920" disabled="">
            </div>
             <div class="col-sm-1">
              x
            </div>
            <div class="col-sm-4">
              <input type="number" class="form-control" ng-model="content.bannersize">
            </div>
            </div>
          </div>

          <div class="line line-dashed b-b line-lg"></div>

          <div class="form-group">
            <div class="col-sm-3">Do you want to display text? </div>
            <div class="col-sm-9 wrapper-sm">
              <div class="btn-group">
                <label class="btn btn-default" ng-model="content.display" btn-radio="'yes'">Yes</label>
                <label class="btn btn-default" ng-model="content.display" btn-radio="'no'">No</label>
              </div>
            </div>
          </div>
          <div class="line line-dashed b-b line-lg"></div>

          <div class="form-group">
            <div class="col-sm-12">
              <tabset class="tab-container tab-danger">
                <tab>
          	       <tab-heading class="font-bold">Note</tab-heading>
                   Please click "Quick Preview" before submitting changes in style so you can check whether the title and description you inserted is too long or too large in font-size. Thank you.
                </tab>
              </tabset>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="panel-body" style="overflow: hidden; width : 100%">
      <footer class="panel-footer text-right bg-light lter">
        <a ui-sref="homepagemanage" class="btn btn-default"> Cancel </a>
        <button type="submit" class="btn btn-success" ng-disabled="contentform.$invalid">Submit</button>
      </footer>
    </div>
  </fieldset>
</form>
</div>

<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();
});
</script>
