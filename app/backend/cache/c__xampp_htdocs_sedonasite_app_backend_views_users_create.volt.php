<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="success.html">
  <div ng-include="'/be/tpl/success.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Create User</h1>
  <a id="top"></a>
</div>
<div  ng-controller="AddUserCtrl">
  <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="submitData(user, file)" name="userform" id="userform">
    <fieldset ng-disabled="isSaving">
      <div class="wrapper-md" >
        <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
        <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Account Information
            </div>
            <div class="panel-body">

              <div class="form-group hiddenoverflow">
                <label class="col-sm-3 control-label">Username</label>
                <div class="col-sm-8">
                  <span class="label bg-danger" ng-show="validusrname">Username already taken. <br/></span>

                  <input type="text"  id="usn" name="usn" ng-space class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.username" ng-change="chkusername(user.username)" required="required">
                  <em class="text-muted">(allow 'a-zA-Z0-9', 4-10 length)</em>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group hiddenoverflow">
                <label class="col-sm-3 control-label">Email Address</label>
                <div class="col-sm-8">
                  <span class="label bg-danger" ng-show="validemail">Email address already taken. <br/></span>
                  <input type="email" id="email" name="email" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.email" ng-change="chkemail(user.email)" required="required"
                  placeholder="smith@example.com">
                </div>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group hiddenoverflow">
                <label class="col-sm-3 control-label">Password</label>
                <div class="col-sm-8">
                  <span class="label bg-danger" ng-show="minpass">Password must be 6 - 20 characters. <br/></span>
                  <input type="password" id="password" name="pw" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-change="chkpass(user.password, user.conpass)" ng-model="user.password" required="required" min-length="6" max-length="20">
                  <em class="text-muted">(allow 'a-zA-Z0-9', 6-20 length)</em>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group hiddenoverflow">
                <label class="col-sm-3 control-label">Confirm Password</label>
                <div class="col-sm-8">
                   <span class="label bg-danger" ng-show="pwdconfirm">Password do not match! <br/></span>
                  <input type="password" id="conpass" name="conpass" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.conpass" ng-change="confirmpass(user.conpass)" required="required" >
                  <!-- <span ng-show='form.confirm_password.$error.validator'>Passwords do not match!</span> -->
                </div>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group hiddenoverflow">
                 <label class="col-lg-3 control-label">User Role</label>
                  <div class="col-sm-8">
                   <div class="radio">
                     <label class="i-checks">
                       <input type="checkbox" name="userrole" checklist-value="'Administrator'" checklist-model="user.userrole" >
                       <i></i>
                       [ Administrator ] ~ Can access all the features in CMS including managing all content in the page, and creating user account and other function and features in CMS.
                     </label>
                   </div>
                   <div class="line line-dashed b-b line-lg pull-in"></div>
                   <div class="radio">
                     <label class="i-checks">
                       <input type="checkbox" name="userrole" checklist-value="'Page Manager'" checklist-model="user.userrole">
                       <i></i>
                       [ Pages ] ~ had the access on managing the pages in the CMS.
                     </label>
                   </div>
                   <div class="line line-dashed b-b line-lg pull-in"></div>
                   <div class="radio">
                     <label class="i-checks">
                       <input type="checkbox" name="userrole" checklist-value="'News Editor'" checklist-model="user.userrole">
                       <i></i>
                       [ News ] ~ <!-- Can access only the center page where he/she is working. -->had the access on managing the News/Blog posts in the CMS.
                     </label>
                   </div>
                   <div class="line line-dashed b-b line-lg pull-in"></div>
                   <div class="radio">
                     <label class="i-checks">
                       <input type="checkbox" name="userrole" checklist-value="'Testimonial Editor'" checklist-model="user.userrole">
                       <i></i>
                       [ Testimonials ] ~ had the access on managing the Testimonials in the CMS.<!--  The editor can manage the news/blog content in all the center and in all the front end news/blog pages. -->
                     </label>
                   </div>
                   <div class="line line-dashed b-b line-lg pull-in"></div>
                   <div class="radio">
                     <label class="i-checks">
                       <input type="checkbox" name="userrole" checklist-value="'Booking Manager'" checklist-model="user.userrole">
                       <i></i>
                       [ Booking ] ~ Can accessing only the booking page in the CMS.
                     </label>
                   </div>
                   <div class="col-sm-6">
                   <br>
                    <label ng-show="nrole && (user.userrole==undefined || user.userrole=='')" class="control-label p" style="color:#a94442;" ng-cloak>
                     User Role is required. Choose at least one. </label>

                  </div>
                   <div class="line line-dashed b-b line-lg pull-in"></div>
                 </div>
               </div>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              User Profile
            </div>

            <div class="panel-body">
              <div class="form-group hiddenoverflow">
                <label class="col-sm-3 control-label">Profile Picture</label>
                <div class="col-sm-8">
                  <img ngf-src="file[0]" id="profpic" ngf-default-src="{[{base_url}]}/img/testidefaultimage.png" ngf-accept="'image/*'">
                  <label class="label_profile_pic btn btn-primary" id="change-picture" ngf-change="prepare(file)" ngf-select ng-model="file" ngf-multiple="false" required="required">Change Picture</label>
                </div>
              </div>
              <div class="form-group hiddenoverflow">
                <label class="col-sm-3 control-label">First Name</label>
                <div class="col-sm-8">
                  <input type="text" id="" name="fn" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.fname" required="required" >
                </div>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group hiddenoverflow">
                <label class="col-sm-3 control-label">Last Name</label>
                <div class="col-sm-8">
                  <input type="text" id="" name="ln" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.lname" required="required" >
                </div>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Birthdate</label>
               
                <div class="col-sm-9">
                  <div class="">
                    <div class="row">
                      <div class="col-md-3">
                        <label class="control-label" >Year</label>
                        <select ng-model="user.byear" class="form-control ng-pristine ng-invalid ng-invalid-required " class="required" name="selyear" id="selyear" required="true">
                          <?php for ($year= 1960 ; $year < date('Y'); $year++) { echo "<option value='".$year."'>".$year."</option>";}?>
                        </select>
                      </div>
                      <div class="col-md-9">
                        <div class="col-xs-8" style="padding-left:0px;">
                          <label class="control-label" >Month</label>
                          <select  ng-model="user.bmonth" class="form-control ng-pristine ng-invalid ng-invalid-required required" name="month" required="true">
                            <?php $formonths = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'); ?>
                            <?php foreach ($formonths as $index => $formonth) { echo "<option value='".$index."'>".$formonth."</option>";}?>
                          </select>
                        </div>
                        <div class="col-xs-4" style="padding-right:0px;">
                          <label class="control-label">Day</label>
                          <select ng-model="user.bday" class="form-control ng-pristine ng-invalid ng-invalid-required required" name="day" required="true">
                            <?php for ($day= 1 ; $day < 31; $day++) { 
                              (strlen($day)==1)? $_day = trim("0".$day) : $_day = $day;
                              echo "<option value='".$_day."'>".$_day."</option>";
                            }?>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <br>
                        <label ng-show="nbday && (!user.byear || !user.bmonth || !user.bday)" class="control-label p" style="color:#a94442;" ng-cloak>
                        Birthdate is required. </label>
                      </div>
                       
                    </div>

                  </div>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group hiddenoverflow">
                <label class="col-sm-3 control-label">Gender</label>
                <div class="col-sm-8">
                  <div class="radio">
                    <label class="i-checks">
                      <input type="radio" name="gender" value="Male" ng-model="user.gender" required="required">
                      <i></i>
                      Male
                    </label>
                  </div>
                  <div class="radio">
                    <label class="i-checks">
                      <input type="radio" name="gender" value="Female" ng-model="user.gender" required="required">
                      <i></i>
                      Female
                    </label>
                  </div>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>

              <div class="form-group hiddenoverflow">
                <label class="col-lg-3 control-label">Status</label>
                <div class="col-sm-8">
                  <label class="i-switch i-switch-md bg-info m-t-xs m-r">
                    <input type="checkbox" checked="checked" ng-model="user.status" name="stat" ng-init="user.status=false">
                    <i></i>
                  </label>
                  <label class="btn m-b-xs btn-xs btn-danger" ng-show="user.status==false"> Inactive </label>
                  <label class="btn m-b-xs btn-xs btn-success" ng-show="user.status==true"> Active </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
              </div>
              </div>
            </div>
          </div>
          <!-- <div class="progress" style="width:97%; margin:0 20px;">
            <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {[{ process }]}%;">
              <span class="sr-only">{[{ process }]}% Complete</span>
            </div>
          </div> -->
         <div class="col-sm-12">
           <div class="loadercontainer" ng-show="loader" ng-cloak>
              <div class="spinner">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
                <p>Please Wait</p>
              </div>
            </div>
         </div>
          <div class="panel-body" style="overflow: hidden; width : 100%" >
            <footer class="panel-footer text-right bg-light lter">
              <button type="button" ui-sref="dashboard" ng-disabled="loader" class="btn btn-default"> Cancel </button>
              <button type="submit" class="btn btn-success" >Submit</button>
            </footer>
          </div>

        </fieldset>
      </form>

 </div>
