

<div>
    <!-- header -->


    <div class="wrapper bg-light lter b-b">
        <div class="btn-toolbar">
       
        </div>
 

<div class="row">
<div class="col-sm-12">
  <div class="table-responsive">
      <div class="panel panel-default">
     <div class="panel-heading font-bold">                  
        <h4  style='text-align:center'> TOP 10 MEMBERS </h4>
    </div>
    
        <table class="table table-striped b-t b-light">
            <thead>
            <tr>
                <th>Member</th>
                <th>No. of Services</th>
            </tr>
            </thead>

            <tbody ng-show="loading">
            <tr>
                <td colspan="5">Loading List</td>
            </tr>
            </tbody>

            <tbody ng-hide="loading">

            <tr ng-show="bigTotalItems==0"> <td colspan="7"> No records found! </td></tr>
            <tr ng-repeat="ss in mem">
              
                <td>{[{ss.fn}]} {[{ss.ln}]}  </td>
                 <td> {[{ss.count}]}  </td>
            </tr>
<!-- 
          <tr> <td colspan="2"> 
           <a ng-show='button' ng-click='showmore()' style='color:#0198E1;font-weight:bold'> SEE MORE </a> 
          </td> </tr>

            <tr> <td colspan="2"> 
           <a ng-show='button2' ng-click='showless()' style='color:#0198E1;font-weight:bold'> SEE LESS </a> 
          </td> </tr>
 -->


            </tbody>
        </table>

       
    <footer class="panel-footer">
        <div class="row">
            <div class="col-sm-4 hidden-xs">
            </div>
            <div class="col-sm-5 text-center" ng-hide="bigTotalItems==0 || loading">
                <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
                <pagination ng-hide="maxSize >= bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </div>
        </div>
    </footer>


    </div>

</div>
</div>
</div>

<div class="row" style='margin-top:2%'>

   <div class="col-sm-12">
    <div class="panel panel-default">
    <div class="panel-heading font-bold">                  
              TOTAL SUMMARY:  <span ng-bind='totalsummary'> </span> Users
    </div>

          <div class="panel-heading">                  
              TOTAL OFFLINE:  <span  ng-bind='totaloffline'> </span> Users
          </div>

           <div class="panel-heading">                  
              TOTAL ONLINE:  <span ng-bind='totalonline'> </span> Users
          </div>
 <highchart config="UserStat"></highchart>
 </div>

</div>
</div>



<div class="row" style='margin-top:2%'>

   <div class="col-sm-12">
   <div class="panel panel-default">
      <div class="panel-heading font-bold" style='width:60%;margin:0 auto'>                  
              TOTAL SUMMARY:  <span ng-bind='totalsummary'> </span> Users
    </div>
 <highchart config="OverallUserStat" style='width:60%;margin:0 auto'></highchart>

 </div>

</div>
</div>
</div>
  




</div>