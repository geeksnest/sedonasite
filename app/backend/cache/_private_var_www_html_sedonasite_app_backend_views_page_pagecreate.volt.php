<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="mediagallery.html">
  <div ng-include="'/be/tpl/pagebuilder/mediagallery.html'"></div>
</script>
<script type="text/ng-template" id="column.html">
  <div ng-include="'/be/tpl/pagebuilder/column.html'"></div>
</script>
<script type="text/ng-template" id="module.html">
  <div ng-include="'/be/tpl/pagebuilder/module.html'"></div>
</script>
<script type="text/ng-template" id="moduletext.html">
  <div ng-include="'/be/tpl/pagebuilder/moduletext.html'"></div>
</script>
<script type="text/ng-template" id="moduleimage.html">
  <div ng-include="'/be/tpl/pagebuilder/moduleimage.html'"></div>
</script>
<script type="text/ng-template" id="moduletestimonial.html">
  <div ng-include="'/be/tpl/pagebuilder/moduletestimonial.html'"></div>
</script>
<script type="text/ng-template" id="modulecontact.html">
  <div ng-include="'/be/tpl/pagebuilder/modulecontact.html'"></div>
</script>
<script type="text/ng-template" id="modulenews.html">
  <div ng-include="'/be/tpl/pagebuilder/modulenews.html'"></div>
</script>
<script type="text/ng-template" id="moduledivider.html">
  <div ng-include="'/be/tpl/pagebuilder/moduledivider.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Create Page</h1>
  <a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="savePage(page)" name="formpage">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="row">
        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Page Information
            </div>
            <div class="panel-body">
              Page Type
              <select class="form-control" required="required" ng-model="page.type" style="margin-top:10px;" ng-init="page.type='Normal'">
                <option value="Normal">Normal page</option>
                <option value="Special">Special page</option>
              </select>
              <div class="line line-dashed b-b line-lg"></div>
              Title
              <input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.title" required="required" ng-keyup="onpagetitle(page.title)">
              <br>
              <div class="line line-dashed b-b line-lg"></div>
              <b class="pull-left">Page Slugs: </b>
              <input type="text" ng-show="editslug" id="pageslugs" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern pull-left" ng-model="page.slugs" ng-keypress="onslugs(page.slugs)"><span ng-bind="page.slugs" class="pull-left mg-left"></span>
              <div ng-show="editslug">
                <a class="btn btn-danger btn-xs pull-right mg-left" ng-click="cancelpageslug(page.title)">cancel</a>
                <a class="btn btn-primary btn-xs pull-right mg-left" ng-click="setslug(page.slugs)">ok</a>
              </div>
              <a class="btn btn-danger btn-xs pull-right mg-left" ng-hide="editslug" ng-click="clearslug(page.title)">clear</a>
              <a class="btn btn-primary btn-xs pull-right mg-left" ng-hide="editslug" ng-click="editpageslug()">edit slug</a>
              <div class="line line-dashed b-b line-lg"></div>
              Title
              <input type="text" id="metatitle" name="metatitle" class="form-control" ng-model="page.metatitle" required="required" placeholder="Meta Title">
              <div class="line line-dashed b-b line-lg"></div>
              Description
              <input input="text"  id="metadesc" name="metadesc"  class="form-control" ng-model="page.metadesc" required="required" placeholder="Meta Description">
              <div class="line line-dashed b-b line-lg"></div>
              Keyword
              <input type="text" id="metatags" name="metatags" class="form-control" ng-model="page.metatags" required="required" placeholder="Meta Keyword">
              <div class="line line-dashed b-b line-lg"></div>
              
              Status
                <div ng-init="page.status = true">  </div>
                  <label class="i-switch bg-info m-t-xs m-r">
                   <input type="checkbox" ng-model="page.status" ng-checked="checked" ng-init="checked=true">
                    <i></i>
                  </label>
                  <span ng-if="page.status == false" class="label bg-danger Services-checkbox-label">Deactivate</span>
              <span ng-if="page.status == true" class="label bg-success Services-checkbox-label">Active</span>
            
         

            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Page Content
            </div>
            <div class="panel-body" ng-if="page.type=='Normal'">
              Content
              <a class="btn btn-default btn-xs" ng-click="media('content')"><i class="fa  fa-folder-open"></i> Media Library</a>
              <br><br>
              <textarea class="ck-editor" ng-model="page.body" required="required"></textarea>
            </div>

            <div class="panel-body" ng-if="page.type=='Special'">
              <div class="col-sm-12 bg-light special_page_row" ng-repeat="count in rowcount">
                <div class="clear specialpage_pad">
                  <button type="button" class="col-sm-12 btn m-b-xs btn-lg btn-default btn-addon bg-info  " ng-click="insertcolumn($index)"><i class="fa fa-plus"></i>Select Column</button>
                </div>
                <input type="hidden" ng-model="page.row[$index]" style="color:black">
                <div class="clear specialpage_pad" ng-if="column[$index] == 1">
                  <input type="hidden" ng-model="page.colcount[$index]" style="color:black" required>
                  <input type="hidden" ng-model="page.col1[$index]" style="color:black" required>
                  <button type="button" ng-if="!page.col1[$index]" ng-click="insertmodule('1',$index)" class="col-sm-12 btn m-b-xs btn-lg btn-default btn-addon bg-info "><i class="fa fa-plus"></i>Insert Module</button>

                  <div class="input-group col-sm-12" ng-if="page.col1[$index]">
                    <button type="button" type="button" class="col-sm-10 btn btn-lg btn-default">{[{ page.col1[$index] }]}</button>

                    <!-- TEXT MODULE -->
                    <textarea style="display:none;" ng-if="page.col1[$index] == 'TEXT'" ng-model="page.col1content[$index]" style="color:black" required></textarea>
                    <button type="button" type="button" ng-if="page.col1[$index] == 'TEXT'" ng-click="edittextmodule('1',$index,page.col1content[$index])" title="Edit" class="col-sm-1 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="page.col1[$index] == 'TEXT'" ng-click="page.col1[$index] = '';page.col1content[$index]=''" title="Delete" class="col-sm-1 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="page.col1[$index] == 'IMAGE'" ng-model="page.col1img[$index]" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="page.col1[$index] == 'IMAGE'" ng-model="page.col1link[$index]" style="color:black"></textarea>
                    <button type="button" type="button" ng-if="page.col1[$index] == 'IMAGE'" ng-click="editimagemodule('1',$index,page.col1img[$index],page.col1link[$index])" title="Edit" class="col-sm-1 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="page.col1[$index] == 'IMAGE'" ng-click="page.col1[$index] = '';page.col1img[$index]='';page.col1link[$index]=''" title="Delete" class="col-sm-1 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>



                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-model="page.col1testimonial[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-model="page.col1testimonialcat[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-click="edittestimonialmodule('1',$index,page.col1testimonial[$index],page.col1testimonialcat[$index])" title="Edit" class="col-sm-1 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-click="page.col1[$index] = '';page.col1testimonial[$index]='';page.col1testimonialcat[$index]=''" title="Delete" class="col-sm-1 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- CONTACT FORM MODULE -->
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'CONTACT'" ng-model="page.col1contact[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'CONTACT'" ng-model="page.col1contactemail[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'CONTACT'" ng-click="editcontactmodule('1',$index,page.col1contact[$index],page.col1contactemail[$index])" title="Edit" class="col-sm-1 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'CONTACT'" ng-click="page.col1[$index] = '';page.col1contact[$index]='';page.col1contactemail[$index]=''" title="Delete" class="col-sm-1 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'NEWS'" ng-model="page.col1viewed[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'NEWS'" ng-model="page.col1popular[$index]" style="color:black"></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'NEWS'" ng-model="page.col1newslimit[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'NEWS'" ng-click="editnewsmodule('1',$index,page.col1viewed[$index],page.col1popular[$index],page.col1newslimit[$index])" title="Edit" class="col-sm-1 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'NEWS'" ng-click="page.col1[$index] = '';page.col1viewed[$index]='';page.col1newslimit[$index]='';page.col1popular[$index]=''" title="Delete" class="col-sm-1 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- DIVIDER MODULE -->
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'DIVIDER'" ng-model="page.col1height[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'DIVIDER'" ng-model="page.col1color[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'DIVIDER'" ng-click="editdividermodule('1',$index,page.col1height[$index],page.col1color[$index])" title="Edit" class="col-sm-1 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'DIVIDER'" ng-click="page.col1[$index] = '';page.col1height[$index]='';page.col1color[$index]=''" title="Delete" class="col-sm-1 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                    </div>

                </div>

                <div class="clear specialpage_pad" ng-if="column[$index] == 2">
                  <input type="hidden" ng-model="page.colcount[$index]" style="color:black" required>
                  <input type="hidden" ng-model="page.col1[$index]" style="color:black" required>
                  <input type="hidden" ng-model="page.col2[$index]" style="color:black" required>
                  
                  <button type="button" ng-if="!page.col1[$index]" ng-click="insertmodule('1',$index)" class="col-sm-6 btn m-b-xs btn-lg btn-default btn-addon pull-left"><i class="fa fa-plus"></i>Insert Module</button>
                  <button type="button" ng-if="!page.col2[$index]" ng-click="insertmodule('2',$index)" class="col-sm-6 btn m-b-xs btn-lg btn-default btn-addon pull-right"><i class="fa fa-plus"></i>Insert Module</button>


                  <div class="input-group col-sm-6 pull-left" ng-if="page.col1[$index]">
                    <button type="button" type="button" class="col-sm-8 btn btn-lg btn-default">{[{ page.col1[$index] }]}</button>

                    <!-- TEXT MODULE -->
                    <textarea style="display:none;" ng-if="page.col1[$index] == 'TEXT'" ng-model="page.col1content[$index]" style="color:black" required></textarea>
                    <button type="button" ng-if="page.col1[$index] == 'TEXT'" type="button" ng-click="edittextmodule('1',$index,page.col1content[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" ng-if="page.col1[$index] == 'TEXT'" type="button" ng-click="page.col1[$index] = '';page.col1content[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="page.col1[$index] == 'IMAGE'" ng-model="page.col1img[$index]" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="page.col1[$index] == 'IMAGE'" ng-model="page.col1link[$index]" style="color:black"></textarea>
                    <button type="button" type="button" ng-if="page.col1[$index] == 'IMAGE'" ng-click="editimagemodule('1',$index,page.col1img[$index],page.col1link[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="page.col1[$index] == 'IMAGE'" ng-click="page.col1[$index] = '';page.col1img[$index]='';page.col1link[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-model="page.col1testimonial[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-model="page.col1testimonialcat[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-click="edittestimonialmodule('1',$index,page.col1testimonial[$index],page.col1testimonialcat[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-click="page.col1[$index] = '';page.col1testimonial[$index]='';page.col1testimonialcat[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>


                       <!-- CONTACT FORM MODULE -->
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'CONTACT'" ng-model="page.col1contact[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'CONTACT'" ng-model="page.col1contactemail[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'CONTACT'" ng-click="editcontactmodule('1',$index,page.col1contact[$index],page.col1contactemail[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'CONTACT'" ng-click="page.col1[$index] = '';page.col1contact[$index]='';page.col1contactemail[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'NEWS'" ng-model="page.col1viewed[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'NEWS'" ng-model="page.col1popular[$index]" style="color:black"></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'NEWS'" ng-model="page.col1newslimit[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'NEWS'" ng-click="editnewsmodule('1',$index,page.col1viewed[$index],page.col1popular[$index],page.col1newslimit[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'NEWS'" ng-click="page.col1[$index] = '';page.col1viewed[$index]='';page.col1newslimit[$index]='';page.col1popular[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- DIVIDER MODULE -->
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'DIVIDER'" ng-model="page.col1height[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'DIVIDER'" ng-model="page.col1color[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'DIVIDER'" ng-click="editdividermodule('1',$index,page.col1height[$index],page.col1color[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'DIVIDER'" ng-click="page.col1[$index] = '';page.col1height[$index]='';page.col1color[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                    </div>


                  <div class="input-group col-sm-6 pull-right" ng-if="page.col2[$index]">
                    <button type="button" type="button" class="col-sm-8 btn btn-lg btn-default">{[{ page.col2[$index] }]}</button>

                    <!-- TEXT MODULE -->
                    <textarea style="display:none;" ng-if="page.col2[$index] == 'TEXT'" ng-model="page.col2content[$index]" style="color:black" required></textarea>
                    <button type="button" type="button" ng-if="page.col2[$index] == 'TEXT'" ng-click="edittextmodule('2',$index,page.col2content[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="page.col2[$index] == 'TEXT'" ng-click="page.col2[$index] = '';page.col2content[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="page.col2[$index] == 'IMAGE'" ng-model="page.col2img[$index]" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="page.col2[$index] == 'IMAGE'" ng-model="page.col2link[$index]" style="color:black"></textarea>
                    <button type="button" type="button" ng-if="page.col2[$index] == 'IMAGE'" ng-click="editimagemodule('2',$index,page.col2img[$index],page.col2link[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="page.col2[$index] == 'IMAGE'" ng-click="page.col2[$index] = '';page.col2img[$index]='';page.col2link[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'TESTIMONIAL'" ng-model="page.col2testimonial[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'TESTIMONIAL'" ng-model="page.col2testimonialcat[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'TESTIMONIAL'" ng-click="edittestimonialmodule('2',$index,page.col2testimonial[$index],page.col2testimonialcat[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'TESTIMONIAL'" ng-click="page.col2[$index] = '';page.col2testimonial[$index]='';page.col2testimonialcat[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- CONTACT FORM MODULE -->
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'CONTACT'" ng-model="page.col2contact[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'CONTACT'" ng-model="page.col2contactemail[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'CONTACT'" ng-click="editcontactmodule('2',$index,page.col2contact[$index],page.col2contactemail[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'CONTACT'" ng-click="page.col2[$index] = '';page.col2contact[$index]='';page.col2contactemail[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'NEWS'" ng-model="page.col2viewed[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'NEWS'" ng-model="page.col2popular[$index]" style="color:black"></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'NEWS'" ng-model="page.col2newslimit[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'NEWS'" ng-click="editnewsmodule('2',$index,page.col2viewed[$index],page.col2popular[$index],page.col2newslimit[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'NEWS'" ng-click="page.col2[$index] = '';page.col2viewed[$index]='';page.col2newslimit[$index]='';page.col2popular[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- DIVIDER MODULE -->
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'DIVIDER'" ng-model="page.col2height[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'DIVIDER'" ng-model="page.col2color[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'DIVIDER'" ng-click="editdividermodule('2',$index,page.col2height[$index],page.col2color[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'DIVIDER'" ng-click="page.col2[$index] = '';page.col2height[$index]='';page.col2color[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>


                  </div>
                </div>

                <div class="clear specialpage_pad" ng-if="column[$index] == 3">
                  <input type="hidden" ng-model="page.colcount[$index]" style="color:black" required>
                  <input type="hidden" ng-model="page.col1[$index]" style="color:black" required>
                  <input type="hidden" ng-model="page.col2[$index]" style="color:black" required>
                   <input type="hidden" ng-model="page.col3[$index]" style="color:black" required>
                  
                  <button type="button" ng-if="!page.col1[$index]" ng-click="insertmodule('1',$index)" class="col-sm-4 btn m-b-xs btn-lg btn-default btn-addon pull-left"><i class="fa fa-plus"></i>Insert Module</button>
                  <div class="input-group col-sm-4 pull-left" ng-if="page.col1[$index]">
                    <button type="button" type="button" class="col-sm-8 btn btn-lg btn-default">{[{ page.col1[$index] }]}</button>

                    <!-- TEXT MODULE -->
                    <textarea style="display:none;" ng-if="page.col1[$index] == 'TEXT'" ng-model="page.col1content[$index]" style="color:black" required></textarea>
                    <button type="button" ng-if="page.col1[$index] == 'TEXT'" type="button" ng-click="edittextmodule('1',$index,page.col1content[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" ng-if="page.col1[$index] == 'TEXT'" type="button" ng-click="page.col1[$index] = '';page.col1content[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="page.col1[$index] == 'IMAGE'" ng-model="page.col1img[$index]" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="page.col1[$index] == 'IMAGE'" ng-model="page.col1link[$index]" style="color:black"></textarea>
                    <button type="button" type="button" ng-if="page.col1[$index] == 'IMAGE'" ng-click="editimagemodule('1',$index,page.col1img[$index],page.col1link[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="page.col1[$index] == 'IMAGE'" ng-click="page.col1[$index] = '';page.col1img[$index]='';page.col1link[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-model="page.col1testimonial[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-model="page.col1testimonialcat[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-click="edittestimonialmodule('1',$index,page.col1testimonial[$index],page.col1testimonialcat[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-click="page.col1[$index] = '';page.col1testimonial[$index]='';page.col1testimonialcat[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>


                       <!-- CONTACT FORM MODULE -->
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'CONTACT'" ng-model="page.col1contact[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'CONTACT'" ng-model="page.col1contactemail[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'CONTACT'" ng-click="editcontactmodule('1',$index,page.col1contact[$index],page.col1contactemail[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'CONTACT'" ng-click="page.col1[$index] = '';page.col1contact[$index]='';page.col1contactemail[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'NEWS'" ng-model="page.col1viewed[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'NEWS'" ng-model="page.col1popular[$index]" style="color:black"></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'NEWS'" ng-model="page.col1newslimit[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'NEWS'" ng-click="editnewsmodule('1',$index,page.col1viewed[$index],page.col1popular[$index],page.col1newslimit[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'NEWS'" ng-click="page.col1[$index] = '';page.col1viewed[$index]='';page.col1newslimit[$index]='';page.col1popular[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- DIVIDER MODULE -->
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'DIVIDER'" ng-model="page.col1height[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'DIVIDER'" ng-model="page.col1color[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'DIVIDER'" ng-click="editdividermodule('1',$index,page.col1height[$index],page.col1color[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'DIVIDER'" ng-click="page.col1[$index] = '';page.col1height[$index]='';page.col1color[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                    </div>

                  <button type="button" ng-if="!page.col2[$index]" ng-click="insertmodule('2',$index)" class="col-sm-4 btn m-b-xs btn-lg btn-default btn-addon pull-left"><i class="fa fa-plus"></i>Insert Module</button>
                  <div class="input-group col-sm-4 pull-left" ng-if="page.col2[$index]">
                    <button type="button" type="button" class="col-sm-8 btn btn-lg btn-default">{[{ page.col2[$index] }]}</button>

                    <!-- TEXT MODULE -->
                    <textarea style="display:none;" ng-if="page.col2[$index] == 'TEXT'" ng-model="page.col2content[$index]" style="color:black" required></textarea>
                    <button type="button" type="button" ng-if="page.col2[$index] == 'TEXT'" ng-click="edittextmodule('2',$index,page.col2content[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="page.col2[$index] == 'TEXT'" ng-click="page.col2[$index] = '';page.col2content[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="page.col2[$index] == 'IMAGE'" ng-model="page.col2img[$index]" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="page.col2[$index] == 'IMAGE'" ng-model="page.col2link[$index]" style="color:black"></textarea>
                    <button type="button" type="button" ng-if="page.col2[$index] == 'IMAGE'" ng-click="editimagemodule('2',$index,page.col2img[$index],page.col2link[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="page.col2[$index] == 'IMAGE'" ng-click="page.col2[$index] = '';page.col2img[$index]='';page.col2link[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'TESTIMONIAL'" ng-model="page.col2testimonial[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'TESTIMONIAL'" ng-model="page.col2testimonialcat[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'TESTIMONIAL'" ng-click="edittestimonialmodule('2',$index,page.col2testimonial[$index],page.col2testimonialcat[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'TESTIMONIAL'" ng-click="page.col2[$index] = '';page.col2testimonial[$index]='';page.col2testimonialcat[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- CONTACT FORM MODULE -->
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'CONTACT'" ng-model="page.col2contact[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'CONTACT'" ng-model="page.col2contactemail[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'CONTACT'" ng-click="editcontactmodule('2',$index,page.col2contact[$index],page.col2contactemail[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'CONTACT'" ng-click="page.col2[$index] = '';page.col2contact[$index]='';page.col2contactemail[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'NEWS'" ng-model="page.col2viewed[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'NEWS'" ng-model="page.col2popular[$index]" style="color:black"></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'NEWS'" ng-model="page.col2newslimit[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'NEWS'" ng-click="editnewsmodule('2',$index,page.col2viewed[$index],page.col2popular[$index],page.col2newslimit[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'NEWS'" ng-click="page.col2[$index] = '';page.col2viewed[$index]='';page.col2newslimit[$index]='';page.col2popular[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- DIVIDER MODULE -->
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'DIVIDER'" ng-model="page.col2height[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'DIVIDER'" ng-model="page.col2color[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'DIVIDER'" ng-click="editdividermodule('2',$index,page.col2height[$index],page.col2color[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'DIVIDER'" ng-click="page.col2[$index] = '';page.col2height[$index]='';page.col2color[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>
                  </div>

                  <button type="button" ng-if="!page.col3[$index]" ng-click="insertmodule('3',$index)" class="col-sm-4 btn m-b-xs btn-lg btn-default btn-addon pull-left"><i class="fa fa-plus"></i>Insert Module</button>
                  <div class="input-group col-sm-4 pull-left" ng-if="page.col3[$index]">
                    <button type="button" type="button" class="col-sm-8 btn btn-lg btn-default">{[{ page.col3[$index] }]}</button>

                    <!-- TEXT MODULE -->
                    <textarea style="display:none;" ng-if="page.col3[$index] == 'TEXT'" ng-model="page.col3content[$index]" style="color:black" required></textarea>
                    <button type="button" type="button" ng-if="page.col3[$index] == 'TEXT'" ng-click="edittextmodule('3',$index,page.col3content[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="page.col3[$index] == 'TEXT'" ng-click="page.col3[$index] = '';page.col3content[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="page.col3[$index] == 'IMAGE'" ng-model="page.col3img[$index]" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="page.col3[$index] == 'IMAGE'" ng-model="page.col3link[$index]" style="color:black"></textarea>
                    <button type="button" type="button" ng-if="page.col3[$index] == 'IMAGE'" ng-click="editimagemodule('3',$index,page.col3img[$index],page.col3link[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="page.col3[$index] == 'IMAGE'" ng-click="page.col3[$index] = '';page.col3img[$index]='';page.col3link[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'TESTIMONIAL'" ng-model="page.col3testimonial[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'TESTIMONIAL'" ng-model="page.col3testimonialcat[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'TESTIMONIAL'" ng-click="edittestimonialmodule('3',$index,page.col3testimonial[$index],page.col3testimonialcat[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'TESTIMONIAL'" ng-click="page.col3[$index] = '';page.col3testimonial[$index]='';page.col3testimonialcat[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- CONTACT FORM MODULE -->
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'CONTACT'" ng-model="page.col3contact[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'CONTACT'" ng-model="page.col3contactemail[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'CONTACT'" ng-click="editcontactmodule('3',$index,page.col3contact[$index],page.col3contactemail[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'CONTACT'" ng-click="page.col3[$index] = '';page.col3contact[$index]='';page.col3contactemail[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'NEWS'" ng-model="page.col3viewed[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'NEWS'" ng-model="page.col3popular[$index]" style="color:black"></textarea>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'NEWS'" ng-model="page.col3newslimit[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'NEWS'" ng-click="editnewsmodule('3',$index,page.col3viewed[$index],page.col3popular[$index],page.col3newslimit[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'NEWS'" ng-click="page.col3[$index] = '';page.col3viewed[$index]='';page.col3newslimit[$index]='';page.col3popular[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- DIVIDER MODULE -->
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'DIVIDER'" ng-model="page.col3height[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'DIVIDER'" ng-model="page.col3color[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'DIVIDER'" ng-click="editdividermodule('3',$index,page.col3height[$index],page.col3color[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'DIVIDER'" ng-click="page.col3[$index] = '';page.col3height[$index]='';page.col3color[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>
                  </div>
                </div>

                <div class="clear specialpage_pad" ng-if="column[$index] == 4">
                  <input type="hidden" ng-model="page.colcount[$index]" style="color:black" required>
                  <input type="hidden" ng-model="page.col1[$index]" style="color:black" required>
                  <input type="hidden" ng-model="page.col2[$index]" style="color:black" required>
                  <input type="hidden" ng-model="page.col3[$index]" style="color:black" required>
                  <input type="hidden" ng-model="page.col4[$index]" style="color:black" required>
                  
                  <button type="button" ng-if="!page.col1[$index]" ng-click="insertmodule('1',$index)" class="col-sm-3 btn m-b-xs btn-lg btn-default btn-addon pull-left"><i class="fa fa-plus"></i>Insert Module</button>
                  <div class="input-group col-sm-3 pull-left" ng-if="page.col1[$index]">
                    <button type="button" type="button" class="col-sm-8 btn btn-lg btn-default">{[{ page.col1[$index] }]}</button>

                    <!-- TEXT MODULE -->
                    <textarea style="display:none;" ng-if="page.col1[$index] == 'TEXT'" ng-model="page.col1content[$index]" style="color:black" required></textarea>
                    <button type="button" ng-if="page.col1[$index] == 'TEXT'" type="button" ng-click="edittextmodule('1',$index,page.col1content[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" ng-if="page.col1[$index] == 'TEXT'" type="button" ng-click="page.col1[$index] = '';page.col1content[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="page.col1[$index] == 'IMAGE'" ng-model="page.col1img[$index]" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="page.col1[$index] == 'IMAGE'" ng-model="page.col1link[$index]" style="color:black"></textarea>
                    <button type="button" type="button" ng-if="page.col1[$index] == 'IMAGE'" ng-click="editimagemodule('1',$index,page.col1img[$index],page.col1link[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="page.col1[$index] == 'IMAGE'" ng-click="page.col1[$index] = '';page.col1img[$index]='';page.col1link[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-model="page.col1testimonial[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-model="page.col1testimonialcat[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-click="edittestimonialmodule('1',$index,page.col1testimonial[$index],page.col1testimonialcat[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-click="page.col1[$index] = '';page.col1testimonial[$index]='';page.col1testimonialcat[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>


                       <!-- CONTACT FORM MODULE -->
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'CONTACT'" ng-model="page.col1contact[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'CONTACT'" ng-model="page.col1contactemail[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'CONTACT'" ng-click="editcontactmodule('1',$index,page.col1contact[$index],page.col1contactemail[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'CONTACT'" ng-click="page.col1[$index] = '';page.col1contact[$index]='';page.col1contactemail[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'NEWS'" ng-model="page.col1viewed[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'NEWS'" ng-model="page.col1popular[$index]" style="color:black"></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'NEWS'" ng-model="page.col1newslimit[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'NEWS'" ng-click="editnewsmodule('1',$index,page.col1viewed[$index],page.col1popular[$index],page.col1newslimit[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'NEWS'" ng-click="page.col1[$index] = '';page.col1viewed[$index]='';page.col1newslimit[$index]='';page.col1popular[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- DIVIDER MODULE -->
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'DIVIDER'" ng-model="page.col1height[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'DIVIDER'" ng-model="page.col1color[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'DIVIDER'" ng-click="editdividermodule('1',$index,page.col1height[$index],page.col1color[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'DIVIDER'" ng-click="page.col1[$index] = '';page.col1height[$index]='';page.col1color[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                    </div>

                  <button type="button" ng-if="!page.col2[$index]" ng-click="insertmodule('2',$index)" class="col-sm-3 btn m-b-xs btn-lg btn-default btn-addon pull-left"><i class="fa fa-plus"></i>Insert Module</button>
                  <div class="input-group col-sm-3 pull-left" ng-if="page.col2[$index]">
                    <button type="button" type="button" class="col-sm-8 btn btn-lg btn-default">{[{ page.col2[$index] }]}</button>

                    <!-- TEXT MODULE -->
                    <textarea style="display:none;" ng-if="page.col2[$index] == 'TEXT'" ng-model="page.col2content[$index]" style="color:black" required></textarea>
                    <button type="button" type="button" ng-if="page.col2[$index] == 'TEXT'" ng-click="edittextmodule('2',$index,page.col2content[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="page.col2[$index] == 'TEXT'" ng-click="page.col2[$index] = '';page.col2content[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="page.col2[$index] == 'IMAGE'" ng-model="page.col2img[$index]" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="page.col2[$index] == 'IMAGE'" ng-model="page.col2link[$index]" style="color:black"></textarea>
                    <button type="button" type="button" ng-if="page.col2[$index] == 'IMAGE'" ng-click="editimagemodule('2',$index,page.col2img[$index],page.col2link[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="page.col2[$index] == 'IMAGE'" ng-click="page.col2[$index] = '';page.col2img[$index]='';page.col2link[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'TESTIMONIAL'" ng-model="page.col2testimonial[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'TESTIMONIAL'" ng-model="page.col2testimonialcat[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'TESTIMONIAL'" ng-click="edittestimonialmodule('2',$index,page.col2testimonial[$index],page.col2testimonialcat[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'TESTIMONIAL'" ng-click="page.col2[$index] = '';page.col2testimonial[$index]='';page.col2testimonialcat[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- CONTACT FORM MODULE -->
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'CONTACT'" ng-model="page.col2contact[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'CONTACT'" ng-model="page.col2contactemail[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'CONTACT'" ng-click="editcontactmodule('2',$index,page.col2contact[$index],page.col2contactemail[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'CONTACT'" ng-click="page.col2[$index] = '';page.col2contact[$index]='';page.col2contactemail[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'NEWS'" ng-model="page.col2viewed[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'NEWS'" ng-model="page.col2popular[$index]" style="color:black"></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'NEWS'" ng-model="page.col2newslimit[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'NEWS'" ng-click="editnewsmodule('2',$index,page.col2viewed[$index],page.col2popular[$index],page.col2newslimit[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'NEWS'" ng-click="page.col2[$index] = '';page.col2viewed[$index]='';page.col2newslimit[$index]='';page.col2popular[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- DIVIDER MODULE -->
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'DIVIDER'" ng-model="page.col2height[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'DIVIDER'" ng-model="page.col2color[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'DIVIDER'" ng-click="editdividermodule('2',$index,page.col2height[$index],page.col2color[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'DIVIDER'" ng-click="page.col2[$index] = '';page.col2height[$index]='';page.col2color[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>
                  </div>
                  
                  <button type="button" ng-if="!page.col3[$index]" ng-click="insertmodule('3',$index)" class="col-sm-3 btn m-b-xs btn-lg btn-default btn-addon pull-left"><i class="fa fa-plus"></i>Insert Module</button>
                  <div class="input-group col-sm-3 pull-left" ng-if="page.col3[$index]">
                    <button type="button" type="button" class="col-sm-8 btn btn-lg btn-default">{[{ page.col3[$index] }]}</button>

                    <!-- TEXT MODULE -->
                    <textarea style="display:none;" ng-if="page.col3[$index] == 'TEXT'" ng-model="page.col3content[$index]" style="color:black" required></textarea>
                    <button type="button" type="button" ng-if="page.col3[$index] == 'TEXT'" ng-click="edittextmodule('3',$index,page.col3content[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="page.col3[$index] == 'TEXT'" ng-click="page.col3[$index] = '';page.col3content[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="page.col3[$index] == 'IMAGE'" ng-model="page.col3img[$index]" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="page.col3[$index] == 'IMAGE'" ng-model="page.col3link[$index]" style="color:black"></textarea>
                    <button type="button" type="button" ng-if="page.col3[$index] == 'IMAGE'" ng-click="editimagemodule('3',$index,page.col3img[$index],page.col3link[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="page.col3[$index] == 'IMAGE'" ng-click="page.col3[$index] = '';page.col3img[$index]='';page.col3link[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'TESTIMONIAL'" ng-model="page.col3testimonial[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'TESTIMONIAL'" ng-model="page.col3testimonialcat[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'TESTIMONIAL'" ng-click="edittestimonialmodule('3',$index,page.col3testimonial[$index],page.col3testimonialcat[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'TESTIMONIAL'" ng-click="page.col3[$index] = '';page.col3testimonial[$index]='';page.col3testimonialcat[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- CONTACT FORM MODULE -->
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'CONTACT'" ng-model="page.col3contact[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'CONTACT'" ng-model="page.col3contactemail[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'CONTACT'" ng-click="editcontactmodule('3',$index,page.col3contact[$index],page.col3contactemail[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'CONTACT'" ng-click="page.col3[$index] = '';page.col3contact[$index]='';page.col3contactemail[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'NEWS'" ng-model="page.col3viewed[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'NEWS'" ng-model="page.col3popular[$index]" style="color:black"></textarea>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'NEWS'" ng-model="page.col3newslimit[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'NEWS'" ng-click="editnewsmodule('3',$index,page.col3viewed[$index],page.col3popular[$index],page.col3newslimit[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'NEWS'" ng-click="page.col3[$index] = '';page.col3viewed[$index]='';page.col3newslimit[$index]='';page.col3popular[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- DIVIDER MODULE -->
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'DIVIDER'" ng-model="page.col3height[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'DIVIDER'" ng-model="page.col3color[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'DIVIDER'" ng-click="editdividermodule('3',$index,page.col3height[$index],page.col3color[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'DIVIDER'" ng-click="page.col3[$index] = '';page.col3height[$index]='';page.col3color[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>
                  </div>


                  <button type="button" ng-if="!page.col4[$index]" ng-click="insertmodule('4',$index)" class="col-sm-3 btn m-b-xs btn-lg btn-default btn-addon pull-left"><i class="fa fa-plus"></i>Insert Module</button>
                  <div class="input-group col-sm-3 pull-left" ng-if="page.col4[$index]">
                    <button type="button" type="button" class="col-sm-8 btn btn-lg btn-default">{[{ page.col4[$index] }]}</button>

                    <!-- TEXT MODULE -->
                    <textarea style="display:none;" ng-if="page.col4[$index] == 'TEXT'" ng-model="page.col4content[$index]" style="color:black" required></textarea>
                    <button type="button" type="button" ng-if="page.col4[$index] == 'TEXT'" ng-click="edittextmodule('4',$index,page.col4content[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="page.col4[$index] == 'TEXT'" ng-click="page.col4[$index] = '';page.col4content[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="page.col4[$index] == 'IMAGE'" ng-model="page.col4img[$index]" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="page.col4[$index] == 'IMAGE'" ng-model="page.col4link[$index]" style="color:black"></textarea>
                    <button type="button" type="button" ng-if="page.col4[$index] == 'IMAGE'" ng-click="editimagemodule('4',$index,page.col4img[$index],page.col4link[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="page.col4[$index] == 'IMAGE'" ng-click="page.col4[$index] = '';page.col4img[$index]='';page.col4link[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="page.col4[$index] == 'TESTIMONIAL'" ng-model="page.col4testimonial[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col4[$index] == 'TESTIMONIAL'" ng-model="page.col4testimonialcat[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col4[$index] == 'TESTIMONIAL'" ng-click="edittestimonialmodule('4',$index,page.col4testimonial[$index],page.col4testimonialcat[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col4[$index] == 'TESTIMONIAL'" ng-click="page.col4[$index] = '';page.col4testimonial[$index]='';page.col4testimonialcat[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- CONTACT FORM MODULE -->
                      <textarea style="display:none;" ng-if="page.col4[$index] == 'CONTACT'" ng-model="page.col4contact[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col4[$index] == 'CONTACT'" ng-model="page.col4contactemail[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col4[$index] == 'CONTACT'" ng-click="editcontactmodule('4',$index,page.col4contact[$index],page.col4contactemail[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col4[$index] == 'CONTACT'" ng-click="page.col4[$index] = '';page.col4contact[$index]='';page.col4contactemail[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <textarea style="display:none;" ng-if="page.col4[$index] == 'NEWS'" ng-model="page.col4viewed[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col4[$index] == 'NEWS'" ng-model="page.col4popular[$index]" style="color:black"></textarea>
                      <textarea style="display:none;" ng-if="page.col4[$index] == 'NEWS'" ng-model="page.col4newslimit[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col4[$index] == 'NEWS'" ng-click="editnewsmodule('4',$index,page.col4viewed[$index],page.col4popular[$index],page.col4newslimit[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col4[$index] == 'NEWS'" ng-click="page.col4[$index] = '';page.col4viewed[$index]='';page.col4newslimit[$index]='';page.col4popular[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- DIVIDER MODULE -->
                      <textarea style="display:none;" ng-if="page.col4[$index] == 'DIVIDER'" ng-model="page.col4height[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col4[$index] == 'DIVIDER'" ng-model="page.col4color[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col4[$index] == 'DIVIDER'" ng-click="editdividermodule('4',$index,page.col4height[$index],page.col4color[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col4[$index] == 'DIVIDER'" ng-click="page.col4[$index] = '';page.col4height[$index]='';page.col4color[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>
                  </div>
                </div>
                <br>
                <span ng-if="countrow != 0 && $last" ng-click="rowdelitem($index)" class="pull-left" style="margin-right:10px"><a href="" title="REMOVE"><i class="fa fa-times fa-fw"></i></a></span>
              </div>
              <div class="clear specialpage_pad">
                <button type="button" class="col-sm-12 btn m-b-xs btn-lg btn-default btn-addon bg-info" ng-click="rownewitem($event)"><i class="fa fa-plus"></i>Add Row</button>
              </div>
            </div>


          </div>

        </div>
        <div class="col-sm-12">
         <!--  <div class="panel panel-default">
            <div class="panel-heading font-bold">
            Page Thumbnail
              <a class="btn btn-default btn-xs pull-right" ng-click="media('featured','banner')"><i class="fa  fa-folder-open"></i> Media Library</a>
            </div>
            <div class="panel-body">
              <img ng-show="page.banner" src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{ page.banner }]}" style="width: 100%">
              <label><em class="text-muted">Click Media Library to add image or select an existing image. Recommended image size: a ratio of 704px in height.</em></label>
            </div>
          </div> -->
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Sidebar
            </div>
            <div class="panel-body">
              <div class="form-group">
                <div class="col-sm-4">
                  <div class="checkbox">
                    <label class="i-checks">
                      <input ng-model="page.sidebarleft" value="left" type="checkbox" ng-true-value="true" ng-init="page.sidebarleft='false'" ng-change="sidebarleft = !sidebarleft" ng-click="removeleft()"><i></i>Left
                    </label>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="checkbox">
                    <label class="i-checks">
                      <input ng-model="page.sidebarright" value="right" type="checkbox" ng-true-value="true" ng-init="page.sidebarright='false'" ng-change="sidebarright = !sidebarright"
                      ng-click="removeright()"><i></i>Right
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default" ng-if="sidebarleft">
            <div class="panel-heading font-bold">
              Left Sidebar  Widget
            <div class="panel-body">
              <div class="col-sm-12" ng-repeat="count in leftsidebarelement">
                <div class="col-sm-12">
                  <select class="form-control col-sm-12" required="required" ng-model="page.left[$index]" style="margin-top:10px;width:60%">

                    <option value="" style="display:none">-Select-</option>
                    <option value="Menu">Menu</option>
                    <option value="Calendar">Calendar</option>
                    <option value="Image">Image</option>
                    <option value="Testimonial">Testimonial</option>
                    <option value="Link">Link</option>
                    <option value="News">News</option>
                    <option value="Archive">Archive</option>
                    <option value="Rss">RSS</option>
                   <!--  <option value="Search">Search</option> -->
                  </select>
                  <button ng-if="countleft != 0 && $last" type="button" class="col-sm-4 btn m-b-xs btn-sm btn-danger btn-addon pull-right" style="margin-top:10px;" ng-click="leftdelitem($index)"><i class="fa fa-minus"></i>Remove</button>
                </div>
                <br><br>
                <div style="padding-top:0px !important;" class="wrapper bg-light" ng-if="page.left[$index] =='Image'">  
                  <div class="line line-dashed b-b line-lg"></div>
                  &nbsp;&nbsp;<i class="icon-arrow-right"></i> Image
                  <a class="btn btn-default btn-xs pull-right" ng-click="media('featured','banner',$index,'left')"><i class="fa  fa-folder-open"></i> Media Library</a>
                  <div class="">
                    <img ng-if="page.leftimg[$index]" src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{ page.leftimg[$index] }]}" style="width: 100%">
                    <input type="text" class="form-control" ng-model="page.leftimglink[$index]" placeholder="Image-Link(optional)">
                  </div>
                </div>

                <div style="padding-top:0px !important;" class="wrapper bg-light" ng-if="page.left[$index] =='Testimonial'">  
                  <div class="line line-dashed b-b line-lg"></div>
                  &nbsp;&nbsp;<i class="icon-arrow-right"></i> Testimonial Limit List View
                  <div class="">
                    <div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input ng-model="page.lefttestimoniallimit[$index]" ui-jq="TouchSpin" type="text" value="10" class="form-control" data-min="0" data-max="20" data-verticalbuttons="true" data-verticalupclass="fa fa-caret-up" data-verticaldownclass="fa fa-caret-down" style="display: block; " class="form-control ng-pristine ng-invalid ng-invalid-required" required><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span></div>
                  </div>
                </div>
                 <div style="padding-top:0px !important;" class="wrapper bg-light" ng-if="page.left[$index] == 'Link'">  
                  <div class="line line-dashed b-b line-lg"></div>
                  &nbsp;&nbsp;<i class="icon-arrow-right"></i> Link
                  <div class="">
                    <input type="text" ng-model="page.leftlinklabel[$index]" class="form-control ng-pristine ng-invalid ng-invalid-required" placeholder="Label"  required=""><br>
                    <input type="url" ng-model="page.leftlinkurl[$index]"  class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-url" placeholder="http://"  required="">
                  </div>
                </div>

                 <div style="padding-top:0px !important;" class="wrapper bg-light" ng-if="page.left[$index] == 'Menu'">  
                  <div class="line line-dashed b-b line-lg"></div>
                  &nbsp;&nbsp;<i class="icon-arrow-right"></i> Menu
                  <div class="">
                   <select class="form-control col-sm-8" required="required" ng-model="page.leftmenu[$index]" style="margin-top:10px;width:60%" >
                    <option value="" style="display:none">-Select-</option>
                    <option ng-repeat='x in menu' value='{[{x.menuID}]}'> {[{x.name}]} </option>
                  </select>
                  </div><br><br>
                </div>

                <div style="padding-top:0px !important;" class="wrapper bg-light" ng-if="page.left[$index] =='News'">  
                  <div class="line line-dashed b-b line-lg"></div>
                  &nbsp;&nbsp;<i class="icon-arrow-right"></i> News
                  <div class="">
                   <!--  <br>
                    <label class="i-checks">
                      <input type="checkbox" ng-model="page.leftnewspopular[$index]" required="" class="ng-dirty ng-invalid ng-invalid-required"><i></i> Most Papular 
                    </label>
                     &nbsp;&nbsp;&nbsp;
                     <label class="i-checks">
                      <input type="checkbox" ng-model="page.leftnewsviews[$index]" required="" class="ng-dirty ng-invalid ng-invalid-required"><i></i> Most Viewed 
                    </label> -->
                    <br>
                    <div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input ng-model="page.leftnewslimit[$index]" ui-jq="TouchSpin" type="text" value="10" class="form-control" data-min="0" data-max="20" data-verticalbuttons="true" data-verticalupclass="fa fa-caret-up" data-verticaldownclass="fa fa-caret-down" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span></div>
                  </div>
                </div>
              </div>
              <button style="margin-top:10px;" type="button" class="col-sm-12 btn m-b-xs btn-sm btn-success btn-addon" ng-click="leftnewitem($event)"><i class="fa fa-plus"></i>Add</button>
            </div>
          </div>
          </div>
          <div class="panel panel-default" ng-if="sidebarright">
            <div class="panel-heading font-bold">
              Right Sidebar Widget
            </div>
            <div class="panel-body font-bold">
              <div class="col-sm-12" ng-repeat="count in rightsidebarelement">
                <div class="col-sm-12">
                  <select class="form-control col-sm-8" required="required" ng-model="page.right[$index]" style="margin-top:10px;width:60%" >
                    <option value="" style="display:none">-Select-</option>
                    <option value="Menu">Menu</option>
                    <option value="Calendar">Calendar</option>
                    <option value="Image">Image</option>
                    <option value="Testimonial">Testimonial</option>
                    <option value="Link">Link</option>
                    <option value="News">News</option>
                    <option value="Archive">Archive</option>
                    <option value="Rss">RSS</option>
                   <!--  <option value="Search">Search</option> -->
                  </select>
                  <button ng-if="countright != 0 && $last" type="button" class="col-sm-4 btn m-b-xs btn-sm btn-danger btn-addon pull-right" style="margin-top:10px;" ng-click="rightdelitem($index)"><i class="fa fa-minus"></i>Remove</button>
                </div>
                <br><br>
                <div style="padding-top:0px !important;" class="wrapper bg-light" ng-if="page.right[$index] =='Image'">  
                  <div class="line line-dashed b-b line-lg"></div>
                  &nbsp;&nbsp;<i class="icon-arrow-right"></i> Image
                  <a class="btn btn-default btn-xs pull-right" ng-click="media('featured','banner',$index,'right')"><i class="fa  fa-folder-open"></i> Media Library</a>
                  <div >
                    <img ng-if="page.rightimg[$index]" src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{ page.rightimg[$index] }]}" style="width: 100%">
                    <input type="text" class="form-control" ng-model="page.rightimglink[$index]" placeholder="Image-Link(optional)">
                  </div>
                </div>

                <div style="padding-top:0px !important;" class="wrapper bg-light" ng-if="page.right[$index] =='Testimonial'">  
                  <div class="line line-dashed b-b line-lg"></div>
                  &nbsp;&nbsp;<i class="icon-arrow-right"></i> Testimonial Limit List View
                  <div class="">
                    <div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input ng-model="page.righttestimoniallimit[$index]" ui-jq="TouchSpin" type="text" value="10" class="form-control" data-min="0" data-max="20" data-verticalbuttons="true" data-verticalupclass="fa fa-caret-up" data-verticaldownclass="fa fa-caret-down" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span></div>
                  </div>
                </div>
                 <div style="padding-top:0px !important;" class="wrapper bg-light" ng-if="page.right[$index] == 'Link'">  
                  <div class="line line-dashed b-b line-lg"></div>
                  &nbsp;&nbsp;<i class="icon-arrow-right"></i> Link
                  <div class="">
                    <input type="text" ng-model="page.rightlinklabel[$index]" class="form-control ng-pristine ng-invalid ng-invalid-required" placeholder="Label"  required=""><br>
                    <input type="url" ng-model="page.rightlinkurl[$index]"  class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-url" placeholder="http://" required="">
                  </div>
                </div>

                <div style="padding-top:0px !important;" class="wrapper bg-light" ng-if="page.right[$index] == 'Menu'">  
                  <div class="line line-dashed b-b line-lg"></div>
                  &nbsp;&nbsp;<i class="icon-arrow-right"></i> Menu
                  <div class="">
                    <select class="form-control col-sm-8" required="required" ng-model="page.rightmenu[$index]" style="margin-top:10px;width:60%" >
                    <option value="" style="display:none">-Select-</option>
                    <option ng-repeat='x in menu' value='{[{x.menuID}]}'> {[{x.name}]} </option>
                  </select>
                  </div>
                  <br><br>
                </div>

                <div style="padding-top:0px !important;" class="wrapper bg-light" ng-if="page.right[$index] =='News'">  
                  <div class="line line-dashed b-b line-lg"></div>
                  &nbsp;&nbsp;<i class="icon-arrow-right"></i> News
                  <div class="">
                    <!-- <br>
                    <label class="i-checks">
                      <input type="checkbox" ng-model="page.rightnewspopular[$index]" required="" class="ng-dirty ng-invalid ng-invalid-required"><i></i> Most Papular 
                    </label>
                     &nbsp;&nbsp;&nbsp;
                     <label class="i-checks">
                      <input type="checkbox" ng-model="page.rightnewsviews[$index]" required="" class="ng-dirty ng-invalid ng-invalid-required"><i></i> Most Viewed 
                    </label> -->
                    <br>
                    <div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input ng-model="page.rightnewslimit[$index]" ui-jq="TouchSpin" type="text" value="10" class="form-control" data-min="0" data-max="20" data-verticalbuttons="true" data-verticalupclass="fa fa-caret-up" data-verticaldownclass="fa fa-caret-down" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span></div>
                  </div>
                </div>

              </div>
              <button style="margin-top:10px;" type="button" class="col-sm-12 btn m-b-xs btn-sm btn-success btn-addon" ng-click="rightnewitem($event)"><i class="fa fa-plus"></i>Add</button>
            </div>
          </div>

        </div>


        <div class="row">
          <div class="panel-body">
            <footer class="panel-footer text-right bg-light lter">
              <a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
              <button type="submit" class="btn btn-success" ng-disabled="formpage.$invalid">Submit</button>
            </footer>
          </div>
        </div>
      </div>
    </fieldset>
  </form>
