<style type="text/css">

</style>
<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="mediagallery.html">
  <div ng-include="'/be/tpl/pagebuilder/mediagallery.html'"></div>
</script>
<script type="text/ng-template" id="column.html">
  <div ng-include="'/be/tpl/pagebuilder/column.html'"></div>
</script>
<script type="text/ng-template" id="module.html">
  <div ng-include="'/be/tpl/pagebuilder/module.html'"></div>
</script>
<script type="text/ng-template" id="moduletext.html">
  <div ng-include="'/be/tpl/pagebuilder/moduletext.html'"></div>
</script>
<script type="text/ng-template" id="moduleimage.html">
  <div ng-include="'/be/tpl/pagebuilder/moduleimage.html'"></div>
</script>
<script type="text/ng-template" id="moduletestimonial.html">
  <div ng-include="'/be/tpl/pagebuilder/moduletestimonial.html'"></div>
</script>
<script type="text/ng-template" id="modulecontact.html">
  <div ng-include="'/be/tpl/pagebuilder/modulecontact.html'"></div>
</script>
<script type="text/ng-template" id="modulenews.html">
  <div ng-include="'/be/tpl/pagebuilder/modulenews.html'"></div>
</script>
<script type="text/ng-template" id="moduledivider.html">
  <div ng-include="'/be/tpl/pagebuilder/moduledivider.html'"></div>
</script>
<script type="text/ng-template" id="rowcolor.html">
  <div ng-include="'/be/tpl/pagebuilder/rowcolor.html'"></div>
</script>
<script type="text/ng-template" id="deleterow.html">
  <div ng-include="'/be/tpl/pagebuilder/deleterow.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Footer Appearance</h1>
  <a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="savePage()" name="formpage">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="row">
        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Update Footer
            </div>
            <div class="panel-body">
             <div class="clear specialpage_pad">
                <button type="button" class="pull-right btn m-b-xs btn-sm btn-default btn-addon" ng-click="rownewitem($event)"><i class="fa fa-plus"></i>Add Row</button>
              </div>
                <ul class="list-group no-radius sortablecol" ui:sortable ng:model="row">
                  <li class="" ng-repeat="(indexX,count) in row track by indexX">
          <br>

              <div class="bg-light">
                <div class="clear">

                 <button type="button" class="pull-right btn m-b-xs btn-sm btn-addon btn-danger mg-left" ng-click="rowdelitem($index)"><i class="fa fa-trash-o"></i>Delete Row</button>
                 <!-- <div ng-init="count.column = 0"></div> -->
                 <button type="button" class="pull-right btn m-b-xs btn-sm btn-addon btn-info mg-left" ng-click="insertmodule(count.column,$index)" ng-disabled="count.column==4"><i class="fa fa-plus"></i>Add Column</button>
                 <button type="button" title="Edit" ng-click="rowcolor($index,count.rowcolor,count.rowfcolor)" class="pull-right btn m-b-xs btn-sm btn-primary mg-left"><i class="fa fa-edit"></i></button>
                </div>
             
              <div class="clear specialpage_pad2" style="margin-right:6px;margin-left: 10px;">
               <ul class="list-group no-radius sortablerow" ui:sortable ng:model="count.row">
                <li class="{[{count.column == 1 ? 'col-sm-12' : ''}]} {[{count.column == 2 ? 'col-sm-6 no-pad' : ''}]} {[{count.column == 3 ? 'col-sm-4 no-pad' : ''}]} {[{count.column == 4 ? 'col-sm-3 no-pad' : ''}]}" ng-repeat="(indexY,counter) in count.row track by indexY" ng-repeat="(indexY,counter) in count.row track by indexY">

              <div class="input-group col-sm-12 col-container" ng-if="$index == 0">

               <a class="{[{count.column == 1 ? 'col-sm-11' : ''}]} {[{count.column == 2 ? 'col-sm-10' : ''}]} {[{count.column == 3 ? 'col-sm-9' : ''}]} {[{count.column == 4 ? 'col-sm-8' : ''}]} btn btn-sm btn-default mg-20"> {[{counter.col}]} </a>

                    <!-- TEXT MODULE -->
                   <textarea style="display:none;" ng-if="counter.col == 'TEXT'" ng-model="counter.colcontent" required></textarea>
                   <div ng-if="counter.col == 'TEXT'" class="input-group-btn">
                     <button type="button" ng-click="edittextmodule(count.column,indexX,counter.colcontent,$index)" title="Edit" class="col-sm-6 btn m-b-xs btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" ng-click="counter.col = '';counter.colcontent='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-6 btn m-b-xs btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                   </div>
                   <div class="txtfooter mgb-20" style="background:{[{count.rowcolor}]};color:{[{count.rowfcolor}]}" ng-if="counter.col == 'TEXT'">
                     <p ng-bind-html="trustAsHtml(counter.colcontent)"></p>
                   </div>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimg" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimglink" style="color:black"></textarea>
                    <div ng-if="counter.col == 'IMAGE'" class="input-group-btn">
                    <button type="button" ng-click="editimagemodule(count.column,indexX,counter.colimg,$index,counter.colimglink)" title="Edit" class="col-sm-6 btn m-b-xs btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" ng-click="counter.col = '';counter.colimg='';counter.collink='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-6 btn m-b-xs btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                    </div>
                    <div class="mgb-20" ng-if="counter.col == 'IMAGE'" style="background:{[{count.rowcolor}]};color:{[{count.rowfcolor}]};max-height:350px;overflow-y:auto">
                    <img style="width:100%" src="{[{s3link}]}/uploads/{[{ directory }]}/{[{counter.colimg}]}">
                    <span ng-if="counter.colimglink"> {[{counter.colimglink}]} </span>
                    </div>
                   
                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonial" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonialcat" style="color:black"></textarea>
                      <div ng-if="counter.col == 'TESTIMONIAL'" class="input-group-btn">
                      <button type="button" ng-click="edittestimonialmodule(count.column,indexX,counter.coltestimonial,counter.coltestimonialcat,$index)" title="Edit" class="col-sm-6 btn m-b-xs btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-click="counter.col = '';counter.coltestimonial='';counter.coltestimonialcat='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-6 btn m-b-xs btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                      </div>
                    <div class="mgb-20 col-sm-12 margin-bot20" ng-if="counter.col == 'TESTIMONIAL'" style="background:{[{count.rowcolor}]};color:{[{count.rowfcolor}]};max-height:350px;overflow-y:auto">
                    <div class="size18" style="padding-top:5px;"><h3 class="font1 italic">What People Are Saying</h3></div>
                       <div ng-repeat="x in testi | filter:{pageid:counter.coltestimonialcat} | limitTo:counter.coltestimonial">
                        <div class="specialpage_col">  
                          <div class="size16 border-left italic margin-top special-testi-wrapper">
                            <div class="row">
                              <div class="col-sm-12 text-center page-testi-image">
                                <img src="{[{s3link}]}/uploads/testimonialpic/{[{x.picture}]}" class="testi-img">
                              </div>
                              <div class="col-sm-12">
                                <div></div>
                                <div>
                                  <span class="fa fa-quote-left"></span>
                                  {[{x.message}]}
                                </div>
                                <div class="text-right">-  {[{x.name}]} </div>
                              </div>
                            </div>
                            <hr class="styled-hr">
                          </div>
                        </div>
                      </div>
                    </div>

                       <!-- CONTACT FORM MODULE -->
                      <div ng-if="counter.col == 'CONTACT'" class="input-group-btn">
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="editcontactmodule(count.column,indexX,counter.colcontact,counter.colcontactemail,$index)" title="Edit" class="col-sm-6 btn m-b-xs btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="counter.col = '';counter.colcontact='';counter.colcontactemail='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-6 btn m-b-xs btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                      </div>
                      <div class="mgb-20" ng-if="counter.col == 'CONTACT'" style="padding-right:10px;background:{[{count.rowcolor}]};color:{[{count.rowfcolor}]};">
                        <div class="row">
                         <div class="col-sm-12" style="height: 300px;
                          overflow-y: auto;">
                          <div class="col-sm-12">
                            <h3>Contact Us</h3>
                          </div>
                          <div class="col-sm-6">
                            <div class="form">
                                <!-- Name -->
                                <div class="control-group margin-bot8">
                                  <div class="row">
                                    <label class="control-label size14 col-sm-12 hidden feedback-success" for="name" style="color:#6cb04b;">Thank you for contacting Sedona Healing Arts, your message was sent successfully.
                                      <br>We will review your inquiry and get back to you shortly.</label>
                                    </div>
                                  </div>
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="name">Subject
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                        <input type="text" class="form-control textbox margin-bot8" readonly="" style="background-color:#fff">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="name">Name
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                      <input type="text" class="form-control textbox margin-bot8" placeholder="First Name" readonly="" style="background-color:#fff">
                                      </div>
                                      <div class="col-sm-12">
                                        <input type="text" class="form-control textbox" placeholder="Last Name" readonly="" style="background-color:#fff">
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Email -->
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="email">Email
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                        <span class="red hidden booking-email">Invalid Email Address</span>
                                        <input type="email" class="textbox form-control margin-bot8" readonly="" style="background-color:#fff">
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Comment -->
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                        <textarea class="form-control input-large textarea" readonly="" style="background-color:#fff"></textarea>
                                      </div>
                                    </div>
                                  </div>
                            
                                  <div class="form-actions margin-bot8">
                                    <div class="row">
                                      <div class="col-sm-12">
                                        <button type="submit" class="btn btn-sedona" disabled="" style="background-color:#fff">Submit</button>
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Buttons -->
                                </div>
                                <div class="clearfix"></div>
                              </div>

                             <div class="col-sm-6 storeinfo" style="word-wrap: break-word;padding-left: 0px;">
                                <div class="storeinfo-details">
                                  <h3 class="orange">Store Information</h3>
                                  <div class="title3">
                                    <span class="titleb">Phone:</span>
                                    <span class="thin-font3">(928) 282-3875</span>
                                    <br/>
                                    <span class="titleb">Email:</span>
                                    <span class="thin-font3">contact@sedonahealingarts.com</span>
                                    <br/>
                                    <span class="titleb">Hours:</span>
                                    <span class="thin-font3">Mon - Sun: 10 am - 7 pm</span>
                                    <br/>
                                    <br/>
                                  </div>
                                  <div class="title3">
                                    <span class="titleb text-top">Address:</span>
                                    <br/>
                                    <span class="thin-font3">201 State Route 179</span>
                                    <br/>
                                    <span class="thin-font3">Sedona, AZ 86336</span>
                                  </div>

                                  <div class="wsite-map">
                                    <iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 200px; margin-top: 10px; margin-bottom: 10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3273.673285579057!2d-111.7637428!3d34.8644358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872da1564d73fd15%3A0x3e31d123930491bf!2s201+Hwy+179%2C+Sedona%2C+AZ+86336%2C+USA!5e0!3m2!1sen!2sph!4v1434348998372"></iframe>
                                  </div>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              </div>
                            </div>
                          </div>

                      <!-- NEWS MODULE -->
                      <div ng-if="counter.col == 'NEWS'" class="input-group-btn">
                      <textarea style="display:none;" ng-if="counter.col == 'NEWS'" ng-model="counter.colnewslimit"></textarea>
                      <button type="button"  ng-click="editnewsmodule(count.column,indexX,count.col1viewed,count.col1popular,counter.colnewslimit,$index)" title="Edit" class="col-sm-6 btn m-b-xs btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-click="counter.col = '';counter.colnewslimit='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-6 btn m-b-xs btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                      </div>
                      <div ng-if="counter.col == 'NEWS'" class="col-sm-12 mgb-20" style="max-height:350px;overflow-y:auto">
                        <h4 class="blog-title-list">Latest News </h4>
                        <div class="list-news-wrapper">
                          <div class="row list-title-blog" style="padding-top:5px;" ng-repeat="news in news | limitTo:counter.colnewslimit">
                            <div style="padding-right:0px;background-image:url('')" class="{[{count.column==1 ? 'col-sm-3' : ''}]} {[{count.column==2 ? 'col-sm-4' : 'col-sm-12'}]} news-thumb-container" title="{[{ news.title }]}">
                              <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}" style="height:130px;width:100%">
                              <div class="youtube-play" ng-show="news.videothumb"><img src="img/youtubeplay.png" ng-click="redirectNews(news.newsslugs);"/></div>
                              
                            </div>
                            <div class="{[{count.column==1 || count.column==2 ? 'col-md-8' : 'col-md-12'}]} news-list-desc">
                              <div class="row">
                                <div class="col-sm-12">
                                  <span class="size25 font1 news-title">{[{ news.title }]}</span>
                                </div>
                                <div class="col-sm-12">
                                  <span class="thin-font1 orange">{[{ news.categorylist }]}</span><span ng-show="news.name !=''" class="thin-font1"> / by <span class="orange">{[{ news.name }]}</span></span> / {[{ news.date }]}
                                  <br/><br/>
                                </div>
                                <div class="col-sm-12">
                                  <div class="font1 size14 summary">
                                    {[{ news.summary }]}
                                    <br/><br/>
                                  </div>
                                </div>
                              </div>

                              <div style="clear:both"></div>
                              <br>
                            </div>
                          </div>
                        </div>
                      </div>

                      <!-- DIVIDER MODULE -->
                      <div ng-if="counter.col == 'DIVIDER'" class="input-group-btn">
                        <textarea style="display:none;" ng-model="counter.colheight" required></textarea>
                        <textarea style="display:none;" ng-model="counter.colcolor" style="color:black"></textarea>
                        <button type="button" ng-click="editdividermodule(count.column,indexX,counter.colheight,counter.colcolor,$index)" title="Edit" class="col-sm-6 btn m-b-xs btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                        <button type="button" ng-click="counter.col = '';counter.colheight='';counter.colcolor='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-6 btn m-b-xs btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                      </div>
                      <br ng-if="counter.col == 'DIVIDER'">
                      <div class="col-sm-11" ng-if="counter.col == 'DIVIDER'">
                      <hr style="height:{[{counter.colheight}]}px;background:{[{counter.colcolor}]}">
                      </div>
                    </div>

                    <div class="col-sm-12 col-container" ng-if="$index == 1">
                      <a class="{[{count.column == 2 ? 'col-sm-10' : ''}]} {[{count.column == 3 ? 'col-sm-9' : ''}]} {[{count.column == 4 ? 'col-sm-8' : ''}]} btn btn-sm btn-default"> {[{counter.col}]} </a>

                    <!-- TEXT MODULE -->
                   <textarea style="display:none;" ng-if="counter.col == 'TEXT'" ng-model="counter.colcontent" required></textarea>
                   <div ng-if="counter.col == 'TEXT'" class="input-group-btn">
                     <button type="button" ng-click="edittextmodule(count.column,indexX,counter.colcontent,$index)" title="Edit" class="col-sm-6 btn m-b-xs btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" ng-click="counter.col = '';counter.colcontent='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-6 btn m-b-xs btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                   </div>
                   <div style="background:{[{count.rowcolor}]};color:{[{count.rowfcolor}]}" ng-if="counter.col == 'TEXT'" class="col-sm-12 txtfooter mgb-20">
                     <p ng-bind-html="trustAsHtml(counter.colcontent)"></p>
                   </div>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimg" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimglink" style="color:black"></textarea>
                    <div ng-if="counter.col == 'IMAGE'" class="input-group-btn">
                    <button type="button" ng-click="editimagemodule(count.column,indexX,counter.colimg,$index,counter.colimglink)" title="Edit" class="col-sm-6 btn m-b-xs btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" ng-click="counter.col = '';counter.colimg='';counter.collink='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-6 btn m-b-xs btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                    </div>
                    <div ng-if="counter.col == 'IMAGE'" style="background:{[{count.rowcolor}]};color:{[{count.rowfcolor}]};max-height:350px;overflow-y:auto" class="mgb-20">
                    <img style="width:100%" src="{[{s3link}]}/uploads/{[{ directory }]}/{[{counter.colimg}]}">
                    <span ng-if="counter.colimglink"> {[{counter.colimglink}]} </span>
                    </div>
                   
                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonial" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonialcat" style="color:black"></textarea>
                      <div ng-if="counter.col == 'TESTIMONIAL'" class="input-group-btn">
                      <button type="button" ng-click="edittestimonialmodule(count.column,indexX,counter.coltestimonial,counter.coltestimonialcat,$index)" title="Edit" class="col-sm-6 btn m-b-xs btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-click="counter.col = '';counter.coltestimonial='';counter.coltestimonialcat='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-6 btn m-b-xs btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                      </div>
                    <div ng-if="counter.col == 'TESTIMONIAL'" style="background:{[{count.rowcolor}]};color:{[{count.rowfcolor}]};max-height:350px;overflow-y:auto" class="mgb-20 col-sm-12 margin-bot20">
                    <div class="size18" style="padding-top:5px;"><h3 class="font1 italic">What People Are Saying</h3></div>
                       <div ng-repeat="x in testi | filter:{pageid:counter.coltestimonialcat} | limitTo:counter.coltestimonial">
                        <div class="specialpage_col">  
                          <div class="size16 border-left italic margin-top special-testi-wrapper">
                            <div class="row">
                              <div class="col-sm-12 text-center page-testi-image">
                                <img src="{[{s3link}]}/uploads/testimonialpic/{[{x.picture}]}" class="testi-img">
                              </div>
                              <div class="col-sm-12">
                                <div></div>
                                <div>
                                  <span class="fa fa-quote-left"></span>
                                  {[{x.message}]}
                                </div>
                                <div class="text-right">-  {[{x.name}]} </div>
                              </div>
                            </div>
                            <hr class="styled-hr">
                          </div>
                        </div>
                      </div>
                    </div>

                       <!-- CONTACT FORM MODULE -->
                      <div ng-if="counter.col == 'CONTACT'" class="input-group-btn">
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="editcontactmodule(count.column,indexX,counter.colcontact,counter.colcontactemail,$index)" title="Edit" class="col-sm-6 btn m-b-xs btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="counter.col = '';counter.colcontact='';counter.colcontactemail='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-6 btn m-b-xs btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                      </div>
                      <div ng-if="counter.col == 'CONTACT'" style="padding-right:10px;background:{[{count.rowcolor}]};color:{[{count.rowfcolor}]};" class="mgb-20">
                        <div class="row">
                         <div class="col-sm-12" style="height: 300px;
                          overflow-y: auto;">
                          <div class="col-sm-12">
                            <h3>Contact Us</h3>
                          </div>
                          <div class="col-sm-6">
                            <div class="form">
                                <!-- Name -->
                                <div class="control-group margin-bot8">
                                  <div class="row">
                                    <label class="control-label size14 col-sm-12 hidden feedback-success" for="name" style="color:#6cb04b;">Thank you for contacting Sedona Healing Arts, your message was sent successfully.
                                      <br>We will review your inquiry and get back to you shortly.</label>
                                    </div>
                                  </div>
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="name">Subject
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                        <input type="text" class="form-control textbox margin-bot8" readonly="" style="background-color:#fff">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="name">Name
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                      <input type="text" class="form-control textbox margin-bot8" placeholder="First Name" readonly="" style="background-color:#fff">
                                      </div>
                                      <div class="col-sm-12">
                                        <input type="text" class="form-control textbox" placeholder="Last Name" readonly="" style="background-color:#fff">
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Email -->
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="email">Email
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                        <span class="red hidden booking-email">Invalid Email Address</span>
                                        <input type="email" class="textbox form-control margin-bot8" readonly="" style="background-color:#fff">
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Comment -->
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                        <textarea class="form-control input-large textarea" readonly="" style="background-color:#fff"></textarea>
                                      </div>
                                    </div>
                                  </div>
                            
                                  <div class="form-actions margin-bot8">
                                    <div class="row">
                                      <div class="col-sm-12">
                                        <button type="submit" class="btn btn-sedona" disabled="" style="background-color:#fff">Submit</button>
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Buttons -->
                                </div>
                                <div class="clearfix"></div>
                              </div>

                             <div class="col-sm-6 storeinfo" style="word-wrap: break-word;padding-left: 0px;">
                                <div class="storeinfo-details">
                                  <h3 class="orange">Store Information</h3>
                                  <div class="title3">
                                    <span class="titleb">Phone:</span>
                                    <span class="thin-font3">(928) 282-3875</span>
                                    <br/>
                                    <span class="titleb">Email:</span>
                                    <span class="thin-font3">contact@sedonahealingarts.com</span>
                                    <br/>
                                    <span class="titleb">Hours:</span>
                                    <span class="thin-font3">Mon - Sun: 10 am - 7 pm</span>
                                    <br/>
                                    <br/>
                                  </div>
                                  <div class="title3">
                                    <span class="titleb text-top">Address:</span>
                                    <br/>
                                    <span class="thin-font3">201 State Route 179</span>
                                    <br/>
                                    <span class="thin-font3">Sedona, AZ 86336</span>
                                  </div>

                                  <div class="wsite-map">
                                    <iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 200px; margin-top: 10px; margin-bottom: 10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3273.673285579057!2d-111.7637428!3d34.8644358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872da1564d73fd15%3A0x3e31d123930491bf!2s201+Hwy+179%2C+Sedona%2C+AZ+86336%2C+USA!5e0!3m2!1sen!2sph!4v1434348998372"></iframe>
                                  </div>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              </div>
                            </div>
                          </div>

                      <!-- NEWS MODULE -->
                      <div ng-if="counter.col == 'NEWS'" class="input-group-btn">
                      <textarea style="display:none;" ng-if="counter.col == 'NEWS'" ng-model="counter.colnewslimit"></textarea>
                      <button type="button" ng-click="editnewsmodule(count.column,indexX,count.col1viewed,count.col1popular,counter.colnewslimit,$index)" title="Edit" class="col-sm-6 btn m-b-xs btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-click="counter.col = '';counter.colnewslimit='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-6 btn m-b-xs btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                      </div>
                      <div ng-if="counter.col == 'NEWS'" class="col-sm-12 mgb-20" style="max-height:350px;overflow-y:auto">
                        <h4 class="blog-title-list">Latest News </h4>
                        <div class="list-news-wrapper">
                          <div class="row list-title-blog" style="padding-top:5px;" ng-repeat="news in news | limitTo:counter.colnewslimit">
                            <div style="padding-right:0px;background-image:url('')" class="{[{count.column==1 ? 'col-sm-3' : ''}]} {[{count.column==2 ? 'col-sm-4' : 'col-sm-12'}]} news-thumb-container" title="{[{ news.title }]}">
                              <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}" style="height:130px;width:100%">
                              <div class="youtube-play" ng-show="news.videothumb"><img src="img/youtubeplay.png" ng-click="redirectNews(news.newsslugs);"/></div>
                              
                            </div>
                            <div class="{[{count.column==1 || count.column==2 ? 'col-md-8' : 'col-md-12'}]} news-list-desc">
                              <div class="row">
                                <div class="col-sm-12">
                                  <span class="size25 font1 news-title">{[{ news.title }]}</span>
                                </div>
                                <div class="col-sm-12">
                                  <span class="thin-font1 orange">{[{ news.categorylist }]}</span><span ng-show="news.name !=''" class="thin-font1"> / by <span class="orange">{[{ news.name }]}</span></span> / {[{ news.date }]}
                                  <br/><br/>
                                </div>
                                <div class="col-sm-12">
                                  <div class="font1 size14 summary">
                                    {[{ news.summary }]}
                                    <br/><br/>
                                  </div>
                                </div>
                              </div>

                              <div style="clear:both"></div>
                              <br>
                            </div>
                          </div>
                        </div>
                      </div>

                      <!-- DIVIDER MODULE -->
                      <div ng-if="counter.col == 'DIVIDER'" class="input-group-btn">
                        <textarea style="display:none;" ng-model="counter.colheight" required></textarea>
                        <textarea style="display:none;" ng-model="counter.colcolor" style="color:black"></textarea>
                        <button type="button" ng-click="editdividermodule(count.column,indexX,counter.colheight,counter.colcolor,$index)" title="Edit" class="col-sm-6 btn m-b-xs btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                        <button type="button" ng-click="counter.col = '';counter.colheight='';counter.colcolor='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-6 btn m-b-xs btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                      </div>
                      <br ng-if="counter.col == 'DIVIDER'">
                      <div ng-if="counter.col == 'DIVIDER'" class="col-sm-11">
                      <hr style="height:{[{counter.colheight}]}px;background:{[{counter.colcolor}]}">
                      </div>
                    </div>
                  
                  <div class="col-sm-12 col-container" ng-if="$index == 2">
                      <a class="{[{count.column == 3 ? 'col-sm-9' : ''}]} {[{count.column == 4 ? 'col-sm-8' : ''}]} btn btn-sm btn-default"> {[{counter.col}]} </a>

                    <!-- TEXT MODULE -->
                   <textarea style="display:none;" ng-if="counter.col == 'TEXT'" ng-model="counter.colcontent" required></textarea>
                   <div ng-if="counter.col == 'TEXT'" class="input-group-btn">
                     <button type="button" ng-click="edittextmodule(count.column,indexX,counter.colcontent,$index)" title="Edit" class="col-sm-6 btn m-b-xs btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" ng-click="counter.col = '';counter.colcontent='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-6 btn m-b-xs btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                   </div>
                   <div style="background:{[{count.rowcolor}]};color:{[{count.rowfcolor}]}" ng-if="counter.col == 'TEXT'" class="col-sm-12 txtfooter mgb-20">
                     <p ng-bind-html="trustAsHtml(counter.colcontent)"></p>
                   </div>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimg" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimglink" style="color:black"></textarea>
                    <div ng-if="counter.col == 'IMAGE'" class="input-group-btn">
                    <button type="button" ng-click="editimagemodule(count.column,indexX,counter.colimg,$index,counter.colimglink)" title="Edit" class="col-sm-6 btn m-b-xs btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" ng-click="counter.col = '';counter.colimg='';counter.collink='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-6 btn m-b-xs btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                    </div>
                    <div ng-if="counter.col == 'IMAGE'" style="background:{[{count.rowcolor}]};color:{[{count.rowfcolor}]};max-height:350px;overflow-y:auto" class="mgb-20">
                    <img style="width:100%" src="{[{s3link}]}/uploads/{[{ directory }]}/{[{counter.colimg}]}">
                    <span ng-if="counter.colimglink"> {[{counter.colimglink}]} </span>
                    </div>
                   
                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonial" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonialcat" style="color:black"></textarea>
                      <div ng-if="counter.col == 'TESTIMONIAL'" class="input-group-btn">
                      <button type="button" ng-click="edittestimonialmodule(count.column,indexX,counter.coltestimonial,counter.coltestimonialcat,$index)" title="Edit" class="col-sm-6 btn m-b-xs btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-click="counter.col = '';counter.coltestimonial='';counter.coltestimonialcat='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-6 btn m-b-xs btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                      </div>
                    <div ng-if="counter.col == 'TESTIMONIAL'" style="background:{[{count.rowcolor}]};color:{[{count.rowfcolor}]};max-height:350px;overflow-y:auto" class="mgb-20 col-sm-12 margin-bot20">
                    <div class="size18" style="padding-top:5px;"><h3 class="font1 italic">What People Are Saying</h3></div>
                       <div ng-repeat="x in testi | filter:{pageid:counter.coltestimonialcat} | limitTo:counter.coltestimonial">
                        <div class="specialpage_col">  
                          <div class="size16 border-left italic margin-top special-testi-wrapper">
                            <div class="row">
                              <div class="col-sm-12 text-center page-testi-image">
                                <img src="{[{s3link}]}/uploads/testimonialpic/{[{x.picture}]}" class="testi-img">
                              </div>
                              <div class="col-sm-12">
                                <div></div>
                                <div>
                                  <span class="fa fa-quote-left"></span>
                                  {[{x.message}]}
                                </div>
                                <div class="text-right">-  {[{x.name}]} </div>
                              </div>
                            </div>
                            <hr class="styled-hr">
                          </div>
                        </div>
                      </div>
                    </div>

                       <!-- CONTACT FORM MODULE -->
                      <div ng-if="counter.col == 'CONTACT'" class="input-group-btn">
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="editcontactmodule(count.column,indexX,counter.colcontact,counter.colcontactemail,$index)" title="Edit" class="col-sm-6 btn m-b-xs btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="counter.col = '';counter.colcontact='';counter.colcontactemail='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-6 btn m-b-xs btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                      </div>
                      <div ng-if="counter.col == 'CONTACT'" style="padding-right:10px;background:{[{count.rowcolor}]};color:{[{count.rowfcolor}]};" class="mgb-20">
                        <div class="row">
                         <div class="col-sm-12" style="height: 300px;
                          overflow-y: auto;">
                          <div class="col-sm-12">
                            <h3>Contact Us</h3>
                          </div>
                          <div class="col-sm-6">
                            <div class="form">
                                <!-- Name -->
                                <div class="control-group margin-bot8">
                                  <div class="row">
                                    <label class="control-label size14 col-sm-12 hidden feedback-success" for="name" style="color:#6cb04b;">Thank you for contacting Sedona Healing Arts, your message was sent successfully.
                                      <br>We will review your inquiry and get back to you shortly.</label>
                                    </div>
                                  </div>
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="name">Subject
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                        <input type="text" class="form-control textbox margin-bot8" readonly="" style="background-color:#fff">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="name">Name
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                      <input type="text" class="form-control textbox margin-bot8" placeholder="First Name" readonly="" style="background-color:#fff">
                                      </div>
                                      <div class="col-sm-12">
                                        <input type="text" class="form-control textbox" placeholder="Last Name" readonly="" style="background-color:#fff">
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Email -->
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="email">Email
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                        <span class="red hidden booking-email">Invalid Email Address</span>
                                        <input type="email" class="textbox form-control margin-bot8" readonly="" style="background-color:#fff">
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Comment -->
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                        <textarea class="form-control input-large textarea" readonly="" style="background-color:#fff"></textarea>
                                      </div>
                                    </div>
                                  </div>
                            
                                  <div class="form-actions margin-bot8">
                                    <div class="row">
                                      <div class="col-sm-12">
                                        <button type="submit" class="btn btn-sedona" disabled="" style="background-color:#fff">Submit</button>
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Buttons -->
                                </div>
                                <div class="clearfix"></div>
                              </div>

                             <div class="col-sm-6 storeinfo" style="word-wrap: break-word;padding-left: 0px;">
                                <div class="storeinfo-details">
                                  <h3 class="orange">Store Information</h3>
                                  <div class="title3">
                                    <span class="titleb">Phone:</span>
                                    <span class="thin-font3">(928) 282-3875</span>
                                    <br/>
                                    <span class="titleb">Email:</span>
                                    <span class="thin-font3">contact@sedonahealingarts.com</span>
                                    <br/>
                                    <span class="titleb">Hours:</span>
                                    <span class="thin-font3">Mon - Sun: 10 am - 7 pm</span>
                                    <br/>
                                    <br/>
                                  </div>
                                  <div class="title3">
                                    <span class="titleb text-top">Address:</span>
                                    <br/>
                                    <span class="thin-font3">201 State Route 179</span>
                                    <br/>
                                    <span class="thin-font3">Sedona, AZ 86336</span>
                                  </div>

                                  <div class="wsite-map">
                                    <iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 200px; margin-top: 10px; margin-bottom: 10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3273.673285579057!2d-111.7637428!3d34.8644358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872da1564d73fd15%3A0x3e31d123930491bf!2s201+Hwy+179%2C+Sedona%2C+AZ+86336%2C+USA!5e0!3m2!1sen!2sph!4v1434348998372"></iframe>
                                  </div>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              </div>
                            </div>
                          </div>

                      <!-- NEWS MODULE -->
                      <div ng-if="counter.col == 'NEWS'" class="input-group-btn">
                      <textarea style="display:none;" ng-if="counter.col == 'NEWS'" ng-model="counter.colnewslimit"></textarea>
                      <button type="button"  ng-click="editnewsmodule(count.column,indexX,count.col1viewed,count.col1popular,counter.colnewslimit,$index)" title="Edit" class="col-sm-6 btn m-b-xs btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-click="counter.col = '';counter.colnewslimit='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-6 btn m-b-xs btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                      </div>
                      <div ng-if="counter.col == 'NEWS'" class="col-sm-12 mgb-20" style="max-height:350px;overflow-y:auto">
                        <h4 class="blog-title-list">Latest News </h4>
                        <div class="list-news-wrapper">
                          <div class="row list-title-blog" style="padding-top:5px;" ng-repeat="news in news | limitTo:counter.colnewslimit">
                            <div style="padding-right:0px;background-image:url('')" class="{[{count.column==1 ? 'col-sm-3' : ''}]} {[{count.column==2 ? 'col-sm-4' : 'col-sm-12'}]} news-thumb-container" title="{[{ news.title }]}">
                              <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}" style="height:130px;width:100%">
                              <div class="youtube-play" ng-show="news.videothumb"><img src="img/youtubeplay.png" ng-click="redirectNews(news.newsslugs);"/></div>
                              
                            </div>
                            <div class="{[{count.column==1 || count.column==2 ? 'col-md-8' : 'col-md-12'}]} news-list-desc">
                              <div class="row">
                                <div class="col-sm-12">
                                  <span class="size25 font1 news-title">{[{ news.title }]}</span>
                                </div>
                                <div class="col-sm-12">
                                  <span class="thin-font1 orange">{[{ news.categorylist }]}</span><span ng-show="news.name !=''" class="thin-font1"> / by <span class="orange">{[{ news.name }]}</span></span> / {[{ news.date }]}
                                  <br/><br/>
                                </div>
                                <div class="col-sm-12">
                                  <div class="font1 size14 summary">
                                    {[{ news.summary }]}
                                    <br/><br/>
                                  </div>
                                </div>
                              </div>

                              <div style="clear:both"></div>
                              <br>
                            </div>
                          </div>
                        </div>
                      </div>

                      <!-- DIVIDER MODULE -->
                      <div ng-if="counter.col == 'DIVIDER'" class="input-group-btn">
                        <textarea style="display:none;" ng-model="counter.colheight" required></textarea>
                        <textarea style="display:none;" ng-model="counter.colcolor" style="color:black"></textarea>
                        <button type="button" ng-click="editdividermodule(count.column,indexX,counter.colheight,counter.colcolor,$index)" title="Edit" class="col-sm-6 btn m-b-xs btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                        <button type="button" ng-click="counter.col = '';counter.colheight='';counter.colcolor='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-6 btn m-b-xs btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                      </div>
                      <br ng-if="counter.col == 'DIVIDER'">
                      <div ng-if="counter.col == 'DIVIDER'" class="col-sm-11">
                      <hr style="height:{[{counter.colheight}]}px;background:{[{counter.colcolor}]}">
                      </div>
                    </div>

                   <div class="col-sm-12 col-container" ng-if="$index == 3">
                      <a class="{[{count.column == 2 ? 'col-sm-10' : ''}]} {[{count.column == 3 ? 'col-sm-9' : ''}]} {[{count.column == 4 ? 'col-sm-8' : ''}]} btn btn-sm btn-default"> {[{counter.col}]} </a>

                    <!-- TEXT MODULE -->
                   <textarea style="display:none;" ng-if="counter.col == 'TEXT'" ng-model="counter.colcontent" required></textarea>
                   <div ng-if="counter.col == 'TEXT'" class="input-group-btn">
                     <button type="button" ng-click="edittextmodule(count.column,indexX,counter.colcontent,$index)" title="Edit" class="col-sm-6 btn m-b-xs btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" ng-click="counter.col = '';counter.colcontent='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-6 btn m-b-xs btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                   </div>
                   <div style="background:{[{count.rowcolor}]};color:{[{count.rowfcolor}]}" ng-if="counter.col == 'TEXT'" class="col-sm-12 txtfooter mgb-20">
                     <p ng-bind-html="trustAsHtml(counter.colcontent)"></p>
                   </div>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimg" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimglink" style="color:black"></textarea>
                    <div ng-if="counter.col == 'IMAGE'" class="input-group-btn">
                    <button type="button" ng-click="editimagemodule(count.column,indexX,counter.colimg,$index,counter.colimglink)" title="Edit" class="col-sm-6 btn m-b-xs btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" ng-click="counter.col = '';counter.colimg='';counter.collink='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-6 btn m-b-xs btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                    </div>
                    <div ng-if="counter.col == 'IMAGE'" style="background:{[{count.rowcolor}]};color:{[{count.rowfcolor}]};max-height:350px;overflow-y:auto" class="mgb-20">
                    <img style="width:100%" src="{[{s3link}]}/uploads/{[{ directory }]}/{[{counter.colimg}]}">
                    <span ng-if="counter.colimglink"> {[{counter.colimglink}]} </span>
                    </div>
                   
                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonial" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonialcat" style="color:black"></textarea>
                      <div ng-if="counter.col == 'TESTIMONIAL'" class="input-group-btn">
                      <button type="button" ng-click="edittestimonialmodule(count.column,indexX,counter.coltestimonial,counter.coltestimonialcat,$index)" title="Edit" class="col-sm-6 btn m-b-xs btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-click="counter.col = '';counter.coltestimonial='';counter.coltestimonialcat='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-6 btn m-b-xs btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                      </div>
                    <div ng-if="counter.col == 'TESTIMONIAL'" style="background:{[{count.rowcolor}]};color:{[{count.rowfcolor}]};max-height:350px;overflow-y:auto" class="col-sm-12 margin-bot20 mgb-20">
                    <div class="size18" style="padding-top:5px;"><h3 class="font1 italic">What People Are Saying</h3></div>
                       <div ng-repeat="x in testi | filter:{pageid:counter.coltestimonialcat} | limitTo:counter.coltestimonial">
                        <div class="specialpage_col">  
                          <div class="size16 border-left italic margin-top special-testi-wrapper">
                            <div class="row">
                              <div class="col-sm-12 text-center page-testi-image">
                                <img src="{[{s3link}]}/uploads/testimonialpic/{[{x.picture}]}" class="testi-img">
                              </div>
                              <div class="col-sm-12">
                                <div></div>
                                <div>
                                  <span class="fa fa-quote-left"></span>
                                  {[{x.message}]}
                                </div>
                                <div class="text-right">-  {[{x.name}]} </div>
                              </div>
                            </div>
                            <hr class="styled-hr">
                          </div>
                        </div>
                      </div>
                    </div>

                       <!-- CONTACT FORM MODULE -->
                      <div ng-if="counter.col == 'CONTACT'" class="input-group-btn">
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="editcontactmodule(count.column,indexX,counter.colcontact,counter.colcontactemail,$index)" title="Edit" class="col-sm-6 btn m-b-xs btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="counter.col = '';counter.colcontact='';counter.colcontactemail='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-6 btn m-b-xs btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                      </div>
                      <div ng-if="counter.col == 'CONTACT'" style="padding-right:10px;background:{[{count.rowcolor}]};color:{[{count.rowfcolor}]};" class="mgb-20">
                        <div class="row">
                         <div class="col-sm-12" style="height: 300px;
                          overflow-y: auto;">
                          <div class="col-sm-12">
                            <h3>Contact Us</h3>
                          </div>
                          <div class="col-sm-6">
                            <div class="form">
                                <!-- Name -->
                                <div class="control-group margin-bot8">
                                  <div class="row">
                                    <label class="control-label size14 col-sm-12 hidden feedback-success" for="name" style="color:#6cb04b;">Thank you for contacting Sedona Healing Arts, your message was sent successfully.
                                      <br>We will review your inquiry and get back to you shortly.</label>
                                    </div>
                                  </div>
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="name">Subject
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                        <input type="text" class="form-control textbox margin-bot8" readonly="" style="background-color:#fff">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="name">Name
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                      <input type="text" class="form-control textbox margin-bot8" placeholder="First Name" readonly="" style="background-color:#fff">
                                      </div>
                                      <div class="col-sm-12">
                                        <input type="text" class="form-control textbox" placeholder="Last Name" readonly="" style="background-color:#fff">
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Email -->
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="email">Email
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                        <span class="red hidden booking-email">Invalid Email Address</span>
                                        <input type="email" class="textbox form-control margin-bot8" readonly="" style="background-color:#fff">
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Comment -->
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                        <textarea class="form-control input-large textarea" readonly="" style="background-color:#fff"></textarea>
                                      </div>
                                    </div>
                                  </div>
                            
                                  <div class="form-actions margin-bot8">
                                    <div class="row">
                                      <div class="col-sm-12">
                                        <button type="submit" class="btn btn-sedona" disabled="" style="background-color:#fff">Submit</button>
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Buttons -->
                                </div>
                                <div class="clearfix"></div>
                              </div>

                             <div class="col-sm-6 storeinfo" style="word-wrap: break-word;padding-left: 0px;">
                                <div class="storeinfo-details">
                                  <h3 class="orange">Store Information</h3>
                                  <div class="title3">
                                    <span class="titleb">Phone:</span>
                                    <span class="thin-font3">(928) 282-3875</span>
                                    <br/>
                                    <span class="titleb">Email:</span>
                                    <span class="thin-font3">contact@sedonahealingarts.com</span>
                                    <br/>
                                    <span class="titleb">Hours:</span>
                                    <span class="thin-font3">Mon - Sun: 10 am - 7 pm</span>
                                    <br/>
                                    <br/>
                                  </div>
                                  <div class="title3">
                                    <span class="titleb text-top">Address:</span>
                                    <br/>
                                    <span class="thin-font3">201 State Route 179</span>
                                    <br/>
                                    <span class="thin-font3">Sedona, AZ 86336</span>
                                  </div>

                                  <div class="wsite-map">
                                    <iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 200px; margin-top: 10px; margin-bottom: 10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3273.673285579057!2d-111.7637428!3d34.8644358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872da1564d73fd15%3A0x3e31d123930491bf!2s201+Hwy+179%2C+Sedona%2C+AZ+86336%2C+USA!5e0!3m2!1sen!2sph!4v1434348998372"></iframe>
                                  </div>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              </div>
                            </div>
                          </div>

                      <!-- NEWS MODULE -->
                      <div ng-if="counter.col == 'NEWS'" class="input-group-btn">
                      <textarea style="display:none;" ng-if="counter.col == 'NEWS'" ng-model="counter.colnewslimit"></textarea>
                      <button type="button"  ng-click="editnewsmodule(count.column,indexX,count.col1viewed,count.col1popular,counter.colnewslimit,$index)" title="Edit" class="col-sm-6 btn m-b-xs btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-click="counter.col = '';counter.colnewslimit='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-6 btn m-b-xs btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                      </div>
                      <div ng-if="counter.col == 'NEWS'" class="col-sm-12 mgb-20" style="max-height:350px;overflow-y:auto">
                        <h4 class="blog-title-list">Latest News </h4>
                        <div class="list-news-wrapper">
                          <div class="row list-title-blog" style="padding-top:5px;" ng-repeat="news in news | limitTo:counter.colnewslimit">
                            <div style="padding-right:0px;background-image:url('')" class="{[{count.column==1 ? 'col-sm-3' : ''}]} {[{count.column==2 ? 'col-sm-4' : 'col-sm-12'}]} news-thumb-container" title="{[{ news.title }]}">
                              <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}" style="height:130px;width:100%">
                              <div class="youtube-play" ng-show="news.videothumb"><img src="img/youtubeplay.png" ng-click="redirectNews(news.newsslugs);"/></div>
                              
                            </div>
                            <div class="{[{count.column==1 || count.column==2 ? 'col-md-8' : 'col-md-12'}]} news-list-desc">
                              <div class="row">
                                <div class="col-sm-12">
                                  <span class="size25 font1 news-title">{[{ news.title }]}</span>
                                </div>
                                <div class="col-sm-12">
                                  <span class="thin-font1 orange">{[{ news.categorylist }]}</span><span ng-show="news.name !=''" class="thin-font1"> / by <span class="orange">{[{ news.name }]}</span></span> / {[{ news.date }]}
                                  <br/><br/>
                                </div>
                                <div class="col-sm-12">
                                  <div class="font1 size14 summary">
                                    {[{ news.summary }]}
                                    <br/><br/>
                                  </div>
                                </div>
                              </div>

                              <div style="clear:both"></div>
                              <br>
                            </div>
                          </div>
                        </div>
                      </div>

                      <!-- DIVIDER MODULE -->
                      <div ng-if="counter.col == 'DIVIDER'" class="input-group-btn">
                        <textarea style="display:none;" ng-model="counter.colheight" required></textarea>
                        <textarea style="display:none;" ng-model="counter.colcolor" style="color:black"></textarea>
                        <button type="button" ng-click="editdividermodule(count.column,indexX,counter.colheight,counter.colcolor,$index)" title="Edit" class="col-sm-6 btn m-b-xs btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                        <button type="button" ng-click="counter.col = '';counter.colheight='';counter.colcolor='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-6 btn m-b-xs btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                      </div>
                      <br ng-if="counter.col == 'DIVIDER'">
                      <div ng-if="counter.col == 'DIVIDER'" class="col-sm-11">
                      <hr style="height:{[{counter.colheight}]}px;background:{[{counter.colcolor}]}">
                      </div>
                    </div>

                    </li>
                    </ul>
                    <br><br>
            </div>
 
        </div>
        </li>
        </ul>
            <!-- <div class="clear specialpage_pad">
                <button type="button" class="col-sm-12 btn m-b-xs btn-lg btn-default btn-addon" ng-click="rownewitem($event)"><i class="fa fa-plus"></i>Add Row</button>
              </div> -->

            </div>
          </div>
        </div>
        <div class="row">
          <div class="panel-body">
            <footer class="panel-footer text-right bg-light lter">
              <a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
              <button type="submit" class="btn btn-success" ng-disabled="formpage.$invalid">Submit</button>
            </footer>
          </div>
        </div>
      </div>
    </fieldset>
  </form>
