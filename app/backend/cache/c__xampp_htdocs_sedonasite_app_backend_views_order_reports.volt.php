<?php echo $this->getContent(); ?>


<div class="hbox hbox-auto-xs hbox-auto-sm">
    <div class="col w-md bg-light dk b-r bg-auto bg-auto-left">
        <div class="wrapper b-b bg">
            <h3 class="m-n font-thin">Reports</h3>
        </div>
        <div class="wrapper hidden-sm hidden-xs" id="email-menu">
            <ul class="nav nav-pills nav-stacked nav-sm">
                <li ui-sref-active="active">
                    <a ui-sref="orderreports.ordersales">
                        Order Report
                    </a>
                </li>
                <li ui-sref-active="active">
                    <a ui-sref="orderreports.saleschart">
                        Order Charts
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="col">
        <div ui-view ></div>
    </div>
</div>