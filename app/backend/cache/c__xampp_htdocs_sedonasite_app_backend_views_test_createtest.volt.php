<?php echo $this->getContent(); ?>
<style>
pre
{
  background-color: white;
    border:none;
    margin-top:0px;
    padding:0px;
    font-family: "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    color:#58666e;
    white-space: normal;
}
#arr li{
    cursor: move;
    padding:1%;
}

</style>
<script type="text/ng-template" id="EditChoices.html">
    <div ng-include="'/be/tpl/test/EditChoices.html'"></div>
</script>

<script type="text/ng-template" id="AddQuestion.html">
    <div ng-include="'/be/tpl/test/AddQuestion.html'"></div>
</script>
<script type="text/ng-template" id="EditServices.html">
    <div ng-include="'/be/tpl/test/EditServices.html'"></div>
</script>
<script type="text/ng-template" id="EditProducts.html">
    <div ng-include="'/be/tpl/test/EditProducts.html'"></div>
</script>

<script type="text/ng-template" id="TestResult.html">
    <div ng-include="'/be/tpl/test/TestResult.html'"></div>
</script>
<script type="text/ng-template" id="AddRec.html">
    <div ng-include="'/be/tpl/test/AddRec.html'"></div>
</script>
<script type="text/ng-template" id="EditAllRec.html">
    <div ng-include="'/be/tpl/test/EditAllRec.html'"></div>
</script>
<script type="text/ng-template" id="ConfirmDeleteR.html">
    <div ng-include="'/be/tpl/test/ConfirmDeleteR.html'"></div>
</script>
<script type="text/ng-template" id="ArrangeQuestions.html">
    <div ng-include="'/be/tpl/test/ArrangeQuestions.html'"></div>
</script>


 <div class="bg-light lter b-b wrapper-md">
    <h1 class="m-n font-thin h3">Create Test </h1>
</div>


    <div class="wrapper">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Create Test
                          <!--   <a href="" ng-click="showOfflineReg = false;" class="pull-right"><icon class="fa  fa-times-circle"></icon></a> -->
                        </div>

                        <div class="row wrapper" style="margin: 0px 5px;">
                            <div class="overlay" ng-show="processing">
                                <div class="loader">
                                    <div class="bar"></div>
                                    <div class="bar"></div>
                                    <div class="bar"></div>
                                </div>
                            </div>
                          <input type='hidden' ng-model='testid' disabled>
                            <p ng-show="invoice"> Test was successfuly saved!</p>
                            <tabset class="tab-container">
                                <tab heading="Test Name" active="steps.step1" select="steps.percent=3">
                                    <p class="m-b">Please fill up the field.</p>
                                    <!-- <progressbar value="steps.percent" class="progress-xs" type="success"></progressbar> -->
                                    <form name="firststep1" id="firststep1" novalidate class="form-validation ng-pristine">
                                    <p>Test Name: * </p>
                                    <input type="text" name="testname" class="form-control" ng-model='testname' required>

                                        <div class="m-t m-b">
                                            <button type="submit" ng-disabled="firststep1.$invalid" class="btn btn-default btn-rounded" ng-click="steps.step2=true;">Next</button>
                                        </div>
                                    </form>

                                </tab>


                            <tab heading="Test Questions" disabled="firststep1.$invalid" active="steps.step2" id='tab2'>
                                    <form name="step2" class="form-validation">
                                        <p class="m-b">Continue the next step</p>

                            <button class='btn btn-info' ng-click='add()'> Add Questions </button>
                          <button class='btn btn-primary' ng-disabled='response==undefined' ng-click='sort()'> Arrange Questions </button>
                                        <br>

                                <table class='table table-striped b-t b-light' id='tblq'>
                                     <thead>Questions </thead>
                            <tbody ng-repeat="option in response">
                                    <td>


                                        {[{option.Question}]}
                                        <ol type='A'>
                                            <li>
                                                {[{option.A}]}
                                            </li>

                                            <li>
                                                {[{option.B}]}
                                            </li>

                                            <li>
                                                {[{option.C}]}
                                            </li>

                                        </ol>
                                    </td>
                                    <td>
                                        <a href class="btn btn-default btn-xs" ng-click="edit($index)">
                                            <i class="fa fa-edit"></i>
                                            Edit
                                        </a>

                                        <a href class="btn btn-default btn-xs" ng-click="delete($index)">
                                            <i class="fa fa-trash-o"></i>
                                            Delete
                                        </a>
                                    </td>


                                </tbody>

                            </table>

                                          <div class="line line-dashed b-b line-lg"></div>
                                        <div class="m-t m-b">
                                            <button type="button" class="btn btn-default btn-rounded" ng-click="steps.step1=true;">Prev</button>

                                            <button type="button" ng-disabled="response == undefined" class="btn btn-default btn-rounded" ng-click="steps.step3=true">Next</button>

                                        </div>
                                    </form>
                                </tab>

                             <tab heading="Test Results" disabled="response == undefined" active="steps.step3">
                                    <p class="m-b">Congraduations! You got the last step.</p>
                                    <p>Please Create Your Probabilities and Add their corresponding test result</p>
                                    <div class='row'>
                                  <div class='col-sm-6'>
                                    <div class="panel panel-default">

                                            <div class="panel-heading">
                                                Create Test Results
                                            </div>
                                            <br>
                                            <div class="form-horizontal">
                                              <div class="form-group">

                                            <label class="control-label col-sm-1"></label>
                                                <div class="col-sm-5">
                                 <a href class="btn btn-info btn-sm" ng-click='check()'>
                                                            <i class="fa fa-plus"></i>
                                                            ADD RESULT
                                                        </a>

                                              </div>
                                          </div>


                                            </div>
<!--  -->
                                    </div>

                             </div>
                             </div>
                             <div class='table-responsive'>
                             <div class='row'>
                              <div class='col-sm-12'>
                                    <div class="panel panel-default" width='100%'>

                                            <div class="panel-heading">
                                               List of Result:
                                            </div>

                                            <div class="table-responsive">
                                        <table class='table' width='100%' style='table-layout: fixed;'>

                                <tr class='font-bold'><td> Probability </td> <td> Result </td> <td> Services </td> <td> Links </td> <td> Products </td> <td> Action </td>  </tr>

                                                <tr ng-repeat='result in res' ng-hide='result.result == undefined'>
                                                    <td>
                                                    {[{result.choice}]} </td>

                                                    <td> {[{result.result}]} </td>

                                                    <td>

                                                      <pre ng-repeat='ss in result.services'>
                                                       {[{ss.Services}]}   </pre>
                                                     </td>

                                                    <td>
                                                             <pre ng-repeat='ss in result.services'>
                                                              {[{ss.Link}]}    </pre>


                                                    </td>

                                                    <td>
                                                               <pre ng-repeat='ss in result.products'>
                                                                {[{ss.Products}]}  </pre>


                                                    </td>
                                                    <td>
                                                <button type="button" ng-click='rec($index,result.choice)' class="btn btn-primary btn-sm">
                                                 <i class="fa fa-plus"></i>
                                                Recommendations</button> <br>

                                                 <a href class="btn btn-warning btn-sm" ng-click='editresult($index)'>
                                                    <i class="fa fa-pencil"></i>
                                                    Edit
                                                </a>

                                                <a href class="btn btn-danger btn-sm" ng-click='deleteresult($index)'>
                                                    <i class="fa fa-trash-o"></i>
                                                    DELETE
                                                </a>
                                                    </td>

                                                </tr>
                                            </table>
                                            </div>
                                    </div>

                             </div>

                               

                             </div>
                               <div class="m-t m-b">

                                <div class='pull-right'>
                                 <span style='color:red' ng-show='req'> Please Add Recommendations </span>
                                 <br><br>
                                <button type='button' class='btn btn-success' ng-disabled='(res == undefined)' ng-click="Create(testid,testname,response,res,firststep1); "> SUBMIT </button>
                                </div>
                                </div>
                             </div>




                                </tab>
                            </tabset>
                        </div>
                    </div>

            </div>



<script type="text/javascript">
function lettersOnly(evt) {
       evt = (evt) ? evt : event;
       var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
          ((evt.which) ? evt.which : 0));
       if (charCode > 31 && (charCode < 65 || charCode > 90) &&
          (charCode < 97 || charCode > 122)) {
          return false;
       }
       return true;
     }
</script>

</form>
</div>
