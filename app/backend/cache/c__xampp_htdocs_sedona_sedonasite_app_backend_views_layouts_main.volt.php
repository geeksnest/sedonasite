<!DOCTYPE html>
<html lang="en" data-ng-app="app">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="user-scalable = yes" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <?php echo $this->tag->getTitle(); ?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <meta name="google-site-verification" content="olwczc3nc3-vWAy4G2_0-L87S-Sjw_HvXrYI26uRjKc" />

  <!-- Stylesheets -->
  <?php echo $this->tag->stylesheetLink('vendors/bootstrap/dist/css/bootstrap.min.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/css/animate.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/css/font-awesome.min.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/css/fonts/flaticon.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/css/simple-line-icons.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/css/font.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/css/app.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/css/custom.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/js/jquery/chosen/chosen.css'); ?>
  <?php echo $this->tag->stylesheetLink('be/js/jquery/select2/select2.css'); ?>
  <?php echo $this->tag->stylesheetLink('vendors/angular-ui-select/dist/select.css'); ?>
  <?php echo $this->tag->stylesheetLink('vendors/angular-ui-notification/dist/angular-ui-notification.min.css'); ?>

  <?php echo $this->tag->stylesheetLink('vendors/angular-xeditable/dist/css/xeditable.css'); ?>
  <?php echo $this->tag->stylesheetLink('vendors/angular-bootstrap-colorpicker/css/colorpicker.min.css'); ?>

  <!----------------------- SPECIAL PAGE CSS BE ---------------------------->
  <?php echo $this->tag->stylesheetLink('be/css/specialpageBE.css'); ?>
  <!-- include the css and sprite -->
  <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.min.css">-->
  <!--<link rel="image_src" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen-sprite.png">-->

  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="be/js/html5shim.js"></script>
  <![endif]-->

  <link rel="shortcut icon" href="/img/frontend/favicon.png" type='image/x-icon'/>
  <style>
      .backimg {
          background-size : cover;
          background-position: center;
          background-repeat: no-repeat;
          border-radius:100%;
      }
  </style>
</head>
<body ng-controller="AppCtrl">
<?php echo $this->getContent(); ?>
<div class="app" id="app" ng-class="{'app-header-fixed':app.settings.headerFixed, 'app-aside-fixed':app.settings.asideFixed, 'app-aside-folded':app.settings.asideFolded}">
    <div class="app-header navbar">
      <!-- navbar header -->
      <div class="navbar-header bg-white">
        <button class="pull-right visible-xs dk" ui-toggle-class="show" data-target=".navbar-collapse">
          <i class="glyphicon glyphicon-cog"></i>
        </button>
        <button class="pull-right visible-xs" ui-toggle-class="off-screen" data-target=".app-aside" ui-scroll="app">
          <i class="glyphicon glyphicon-align-justify"></i>
        </button>
        <!-- brand -->
        <a href="#/" class="navbar-brand text-lt">
          <img src="/img/frontend/favicon.png" alt=".">
          <span class="hidden-folded m-l-xs">{[{app.name}]}</span>
        </a>
        <!-- / brand -->
      </div>
      <!-- / navbar header -->

      <!-- navbar collapse -->
      <div class="collapse navbar-collapse box-shadow {[{app.settings.navbarCollapseColor}]}">
        <!-- buttons -->
        <div class="nav navbar-nav m-l-sm hidden-xs">
          <a href class="btn no-shadow navbar-btn" ng-click="app.settings.asideFolded = !app.settings.asideFolded">
            <i class="fa {[{app.settings.asideFolded ? 'fa-indent' : 'fa-dedent'}]} fa-fw"></i>
          </a>
          <a href class="btn no-shadow navbar-btn" ui-toggle-class="show" target="#aside-user">
            <i class="icon-user fa-fw"></i>
          </a>
        </div>
        <!-- / buttons -->

        <!-- link and dropdown -->
        <ul class="nav navbar-nav hidden-sm">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-fw fa-plus visible-xs-inline-block"></i>
              <span translate="header.navbar.new.NEW">New</span> <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <li>
                <a href="#">
                  <span class="badge bg-info pull-right">5</span>
                  <span translate="header.navbar.new.NEWS">Task</span>
                </a>
              </li>
              <li><a href="#" translate="header.navbar.new.BLOG">User</a></li>
              <li>
                <a href="#">
                  <span class="badge bg-danger pull-right">4</span>
                  <span translate="header.navbar.new.PAGE">Email</span>
                </a>
              </li>
              <li>
                <a href="#">
                  <span class="badge bg-danger pull-right">4</span>
                  <span translate="header.navbar.new.USER">Email</span>
                </a>
              </li>
            </ul>
          </li>
        </ul>
        <!-- / link and dropdown -->

        <!-- search form -->
        <form class="navbar-form navbar-form-sm navbar-left shift" ui-shift="prependTo" target=".navbar-collapse" role="search" ng-controller="TypeaheadDemoCtrl">
          <div class="form-group">
            <div class="input-group">
              <input type="text" ng-model="selected" typeahead="state for state in states | filter:$viewValue | limitTo:8" class="form-control input-sm bg-light no-border rounded padder" placeholder="Search projects...">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-sm bg-light rounded"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </div>
        </form>
        <!-- / search form -->

        <!-- nabar right -->
        <ul class="nav navbar-nav navbar-right">
          <li class="hidden-xs">
            <a ui-fullscreen></a>
          </li>
          <li class="dropdown">
            <a href class="dropdown-toggle">
              <i class="icon-bell fa-fw"></i>
              <span class="visible-xs-inline">Notifications</span>
              <span class="badge badge-sm up bg-danger pull-right-xs">2</span>
            </a>
            <!-- dropdown -->
            <div class="dropdown-menu w-xl animated fadeInUp">
              <div class="panel bg-white">
                <div class="panel-heading b-light bg-light">
                  <strong>You have <span>2</span> notifications</strong>
                </div>
                <div class="list-group">
                  <a href class="media list-group-item">
                    <span class="pull-left thumb-sm">
                      <img src="/img/a0.jpg" alt="..." class="img-circle">
                    </span>
                    <span class="media-body block m-b-none">
                      Use awesome animate.css<br>
                      <small class="text-muted">10 minutes ago</small>
                    </span>
                  </a>
                  <a href class="media list-group-item">
                    <span class="media-body block m-b-none">
                      1.0 initial released<br>
                      <small class="text-muted">1 hour ago</small>
                    </span>
                  </a>
                </div>
                <div class="panel-footer text-sm">
                  <a href class="pull-right"><i class="fa fa-cog"></i></a>
                  <a href="#notes" data-toggle="class:show animated fadeInRight">See all the notifications</a>
                </div>
              </div>
            </div>
            <!-- / dropdown -->
          </li>
          <li class="dropdown">
            <a href class="dropdown-toggle clear" data-toggle="dropdown">
              <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
                <!-- <div style="background-image : url('<?php echo $this->config->application->amazonlink."/uploads/userimages/".$userinfo->profile_pic_name; ?>'); width: 40px; height:40px;" class="backimg" ></div> -->
                <img style="height:40px" src="<?php echo $this->config->application->amazonlink."/uploads/userimages/".$userinfo->profile_pic_name; ?>" alt="...">
                <i class="on md b-white bottom"></i>
              </span>
              <span class="hidden-sm hidden-md"><?php echo $username['pi_fullname']; ?></span> <b class="caret"></b>
            </a>
            <!-- dropdown -->
            <ul class="dropdown-menu animated fadeInRight w">
              <li class="wrapper b-b m-b-sm bg-light m-t-n-xs">
                <div>
                  <p>300mb of 500mb used</p>
                </div>
                <progressbar value="60" class="progress-xs m-b-none bg-white"></progressbar>
              </li>
              <?php if($userinfo->task == "Administrator"){ ?>
              <li>
                <a href ui-sref="settings">
                  <span>Settings</span>
                </a>
              </li>
              <?php } ?>
              <li>
                <a ui-sref="profile({ userid: '<?php echo $userinfo->id; ?>' })">Profile</a>
              </li>
              <li>
                <a ui-sref="app.docs">
                  <span class="label bg-info pull-right">new</span>
                  Help
                </a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="/sedonaadmin/index/logout">Logout</a>
              </li>
            </ul>
            <!-- / dropdown -->
          </li>
        </ul>
        <!-- / navbar right -->

      </div>
      <!-- / navbar collapse -->
      </div>

  <!-- menu -->
  <div class="app-aside hidden-xs {[{app.settings.asideColor}]}">
<div class="aside-wrap">
  <div class="navi-wrap">
    <!-- user -->
    <div class="clearfix hidden-xs text-center hide" id="aside-user">
      <div class="dropdown wrapper">
        <a ui-sref="app.page.profile">
          <span class="thumb-lg w-auto-folded avatar m-t-sm">
            <!-- <div class="img-full backimg" style="background-image : url('<?php echo $this->config->application->amazonlink."/uploads/userimages/".$userinfo->profile_pic_name; ?>'); width: 96px; height:96px;"></div> -->
            <img src="<?php echo $this->config->application->amazonlink."/uploads/userimages/".$userinfo->profile_pic_name; ?>" class="img-full" alt="...">
          </span>
        </a>
        <a href class="dropdown-toggle hidden-folded">
          <span class="clear">
            <span class="block m-t-sm">
              <strong class="font-bold text-lt"><?php echo $username['pi_fullname']; ?></strong>
              <b class="caret"></b>
            </span>
            <span class="text-muted text-xs block"><?php echo $userinfo->task; ?></span>
          </span>
        </a>
        <!-- dropdown -->
        <ul class="dropdown-menu animated fadeInRight w hidden-folded">
          <li class="wrapper b-b m-b-sm bg-info m-t-n-xs">
            <span class="arrow top hidden-folded arrow-info"></span>
            <div>
              <p>300mb of 500mb used</p>
            </div>
            <progressbar value="60" type="white" class="progress-xs m-b-none dker"></progressbar>
          </li>
          <li>
            <a href>Settings</a>
          </li>
          <li>
            <a ui-sref="profile({ userid: '<?php echo $username['pi_id']; ?>' })">Profile</a>
          </li>
          <li>
            <a href>
              <span class="badge bg-danger pull-right">3</span>
              Notifications
            </a>
          </li>
          <li class="divider"></li>
          <li>
            <a ui-sref="logout"> Logout</a>
          </li>
        </ul>
        <!-- / dropdown -->
      </div>
      <div class="line dk hidden-folded"></div>
    </div>
    <!-- / user -->

    <!-- nav -->
    <nav ui-nav class="navi">
<!-- first -->
<ul class="nav">
  <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
    <span translate="aside.nav.HEADER">Navigation</span>
  </li>
  <li>
    <a ui-sref="dashboard">
      <i class="glyphicon glyphicon-stats icon text-primary-dker"></i>
      <span class="font-bold" translate="aside.nav.DASHBOARD">Dashboard</span>
    </a>
  </li>

  <?php if($userinfo->task == "Administrator"){ ?>
    <li ng-class="{active:$state.includes('app.users')}">
      <a href class="auto">
        <span class="pull-right text-muted">
          <i class="fa fa-fw fa-angle-right text"></i>
          <i class="fa fa-fw fa-angle-down text-active"></i>
        </span>
        <i class="glyphicon glyphicon-user icon"></i>
        <span class="font-bold" translate="aside.nav.users.USERS">Users</span>
      </a>
      <ul class="nav nav-sub dk">
        <li ui-sref-active="active">
          <a ui-sref="userlist({ userid: '<?php echo $userinfo->id; ?>' })">
            <span translate="aside.nav.users.USER_LIST">User List</span>
          </a>
        </li>
        <li ui-sref-active="active">
          <a ui-sref="userscreate">
            <span translate="aside.nav.users.CREATE_USER">Create User</span>
          </a>
        </li>
      </ul>
    </li>
  <?php } ?>
  <!-- <li ng-class="{active:$state.includes('app.center')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-inbox icon"></i>
      <span class="font-bold" translate="aside.nav.center.CENTER">Center</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="userlist">
          <span translate="aside.nav.center.CREATE_CENTER">Create Center</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="userscreate">
          <span translate="aside.nav.center.MANAGE_CENTER">Manage Center</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="userscreate">
          <span translate="aside.nav.center.MY_CENTER">My Center</span>
        </a>
      </li>
    </ul>
  </li> -->
  <li ng-class="{active:$state.includes('app.pages')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="fa fa-th"></i>
      <span class="font-bold">Homepage</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a href ui-sref="homepagecreate">
          <span>Create</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="homepagemanage">
          <span>Manage</span>
        </a>
      </li>
    </ul>
  </li>
 <!-- -->

  <li ng-class="{active:$state.includes('app.pages')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="fa fa-pencil"></i>
      <span class="font-bold">Pages</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a href ui-sref="pagecreate">
          <span>Create</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="pagemanage">
          <span>Manage</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="pagefooter">
          <span>Footer</span>
        </a>
      </li>
    </ul>
  </li>

  <?php if($userinfo->task == "Page Manager" || $userinfo->task == "Administrator"){ ?>
  <li ng-class="{active:$state.includes('app.pages')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-th-large icon text-success"></i>
      <span class="font-bold" translate="aside.nav.pages.PAGES">Pages</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="createpage">
          <span translate="aside.nav.pages.CREATE_PAGE">Create Page</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="managepage">
          <span translate="aside.nav.pages.MANAGE_PAGE">Manage Page</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="managepagecategory">
          <span translate="aside.nav.pages.MANAGE_PAGE_CATEGORY">Categories</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="auraservice">
          <span translate="aside.nav.pages.NEW_AURA_SERVICE">New Aura Service</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="manage_auraservice">
          <span translate="aside.nav.pages.MANAGE_AURA_SERVICE">Manage Aura Service</span>
        </a>
      </li>
    </ul>
  </li>
  <?php }
  if($userinfo->task == "Administrator" || $userinfo->task == "News/Blog Editor"){
  ?>
  <li ng-class="{active:$state.includes('app.news')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-edit icon text-info-lter"></i>
      <span class="font-bold" translate="aside.nav.news.NEWS">News</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="createnews">
          <span translate="aside.nav.news.CREATE_NEWS">Create News</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="managenews">
          <span translate="aside.nav.news.MANAGE_NEWS">Manage News</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="category">
          <span translate="aside.nav.news.NEWS_CATEGORY">News Category</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="tags">
          <span translate="aside.nav.news.NEWS_TAGS">News Tags</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="createauthor">
          <span translate="aside.nav.news.CREATE_NEWS_AUTHOR">Create Author</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="manageauthor">
          <span translate="aside.nav.news.MANAGE_NEWS_AUTHOR">Manage Author</span>
        </a>
      </li>
    </ul>
  </li>
  <?php }
  if($userinfo->task == "Administrator" || $userinfo->task == "Testimonial Editor"){
  ?>
  <li ng-class="{active:$state.includes('app.testimonials')}">
    <a ui-sref="testimonials">
      <i class="glyphicon glyphicon-comment"></i>
      <span class="font-bold" translate="aside.nav.testi">Testimonials</span>
    </a>
  </li>
  <?php }
  if($userinfo->task == "Administrator" || $userinfo->task == "Booking Manager"){
  ?>
  <li ng-class="{active:$state.includes('app.booking')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-edit icon text-info-lter"></i>
      <span class="font-bold" translate="aside.nav.booking.BOOKING">Booking</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="scheduling">
          <span translate="aside.nav.booking.SCHEDULING">Scheduling</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="schedulereports">
          <span translate="aside.nav.booking.REPORTS">Reports</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="booking">
          <span translate="aside.nav.booking.BOOKING">Booking</span>
        </a>
      </li>
    </ul>
  </li>

  <?php }

  if($userinfo->task == "Administrator"){
  ?>
  <li ng-class="{active:$state.includes('app.contactus')}">
    <a ui-sref="contactus">
      <i class="glyphicon glyphicon-earphone"></i>
      <span class="font-bold" translate="aside.nav.contactus">contact us</span>
    </a>
  </li>
  <?php } ?>


  <?php
  if($userinfo->task == "Administrator"){
  ?>
  <li ng-class="{active:$state.includes('app.test')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-pencil icon text-info-lter"></i>
      <span class="font-bold" translate="aside.nav.test.TEST">TEST</span>
    </a>

    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="createtest">
          <span translate="aside.nav.test.CREATE_TEST">CREATE TEST</span>
        </a>
      </li>
    </ul>

         <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="viewtests">
          <span>Manage Test</span>
        </a>
      </li>
    </ul>

      <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="testtakers">
          <span>Test Takers</span>
        </a>
      </li>
    </ul>
  </li>

  <?php } ?>

    <?php
  if($userinfo->task == "Administrator"){
  ?>
  <li ng-class="{active:$state.includes('app.testimonials')}">
    <a ui-sref="create_album">
      <i class="glyphicon glyphicon-picture icon text-info-lter"></i>
      <span class="font-bold">Slider</span>
    </a>
  </li>
  <li ng-class="{active:$state.includes('app.testimonials')}">
    <a ui-sref="menu_creator">
      <i class="fa fa-reorder"></i>
      <span class="font-bold">Menu Creator</span>
    </a>
  </li>

 <!--  <li ng-class="{active:$state.includes('app.test')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-picture icon text-info-lter"></i>
      <span class="font-bold">Slider</span>
    </a>

    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="create_album">
          <span>Images</span>
        </a>
      </li>
    </ul>
  </li> -->

  <?php } ?>

  <?php
    if($userinfo->task == "Administrator"){
    ?>
    <li class="line dk hidden-folded"></li>
    <li ng-class="{active:$state.includes('app.product')}">
      <a href class="auto">
        <span class="pull-right text-muted">
          <i class="fa fa-fw fa-angle-right text"></i>
          <i class="fa fa-fw fa-angle-down text-active"></i>
        </span>
        <i class="icon-diamond"></i>
        <span class="font-bold" translate="aside.nav.product.PRODUCT">Product</span>
      </a>
      <ul class="nav nav-sub dk">
        <li ui-sref-active="active">
          <a ui-sref="addproduct">
            <span translate="aside.nav.product.ADD">Add Product/Inventory</span>
          </a>
        </li>
        <li ui-sref-active="active">
          <a ui-sref="manageproduct">
            <span translate="aside.nav.product.MANAGE">Manage Product</span>
          </a>
        </li>
        <li ui-sref-active="active">
          <a ui-sref="manageproductcategory">
            <span translate="aside.nav.product.MANAGECATEGORY">Manage Category</span>
          </a>
        </li>
        <li ui-sref-active="active">
          <a ui-sref="manageproductsubcategory">
            <span translate="aside.nav.product.MANAGESUBCATEGORY">Manage Sub Category</span>
          </a>
        </li>
        <li ui-sref-active="active">
          <a ui-sref="manageproducttype">
            <span translate="aside.nav.product.MANAGETYPE">Manage Type</span>
          </a>
        </li>
        <li ui-sref-active="active">
          <a ui-sref="manageproducttags">
            <span translate="aside.nav.product.MANAGETAGS">Manage Tags</span>
          </a>
        </li>
      </ul>
    </li>
  <?php } ?>

   <?php
    if($userinfo->task == "Administrator"){
    ?>
    <li ng-class="{active:$state.includes('app.manageorder')}">
      <a href class="auto">
        <span class="pull-right text-muted">
          <i class="fa fa-fw fa-angle-right text"></i>
          <i class="fa fa-fw fa-angle-down text-active"></i>
        </span>
        <i class="icon-diamond"></i>
        <span class="font-bold" translate="aside.nav.manageorder.ORDER">ORDER</span>
      </a>
      <ul class="nav nav-sub dk">
        <li ui-sref-active="active">
          <a ui-sref="managepage">
            <span translate="aside.nav.manageorder.MANAGE">Manage Product</span>
          </a>
        </li>
      </ul>
    </li>
  <?php } ?>

  <!-- <li ng-class="{active:$state.includes('app.workshop')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon  glyphicon-paperclip icon text-info-lter"></i>
      <span class="font-bold" translate="aside.nav.workshop.WORKSHOP">Workshop</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="userlist">
          <span translate="aside.nav.workshop.CREATE_WORKSHOP">Create Workshop</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="userscreate">
          <span translate="aside.nav.workshop.MANAGE_WORKSHOP">Manage Workshop</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="userscreate">
          <span translate="aside.nav.workshop.WORKSHOP_REG">Workshop Registrants</span>
        </a>
      </li>
    </ul>
  </li> -->
  <!-- <li ng-class="{active:$state.includes('app.contacts')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon  glyphicon-phone-alt icon"></i>
      <span class="font-bold" translate="aside.nav.contacts.CONTACTS">Contacts</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="userscreate">
          <span translate="aside.nav.contacts.MANAGE_CONTACTS">Manage Contacts</span>
        </a>
      </li>
    </ul>
  </li>  -->
 <!--  <li ng-class="{active:$state.includes('app.newsletter')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-envelope icon"></i>
      <span class="font-bold" translate="aside.nav.newsletter.NEWSLETTER">Newsletter</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="userlist">
          <span translate="aside.nav.newsletter.CREATE_NEWSLETTER">Create Newsletter</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="userscreate">
          <span translate="aside.nav.newsletter.MANAGE_NEWSLETTER">Manage Newsletter</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="userscreate">
          <span translate="aside.nav.newsletter.ADD_SUBSCRIBERS">Add Subscribers</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="userscreate">
          <span translate="aside.nav.newsletter.SUBSCRIBERS_LIST">Subscribers List</span>
        </a>
      </li>
    </ul>
  </li> -->
  <!-- <li ng-class="{active:$state.includes('app.story')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-bullhorn icon"></i>
      <span class="font-bold" translate="aside.nav.story.STORY">Success Stories</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="userlist">
          <span translate="aside.nav.story.MANAGE_STORY">Manage Stories</span>
        </a>
      </li>
    </ul>
  </li> -->
  <!-- <li class="line dk"></li>

  <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
    <span translate="aside.nav.components.COMPONENTS">Components</span>
  </li>
  <li ng-class="{active:$state.includes('app.slider')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-briefcase icon"></i>
      <span translate="aside.nav.components.slider.SLIDER">Slider</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a href>
          <span translate="aside.nav.components.slider.CREATE_SLIDER">Create Slider</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="app.ui.buttons">
          <span translate="aside.nav.components.slider.MANAGE_SLIDER">Manage Slider</span>
        </a>
      </li>
    </ul>
  </li>
  <li ng-class="{active:$state.includes('app.income')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span> -->
      <!-- <b class="label bg-primary pull-right">2</b> -->
      <!-- <i class="glyphicon  glyphicon-usd"></i>
      <span translate="aside.nav.components.income.INCOME">Center Income</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a href>
          <span translate="aside.nav.components.income.MANAGE_INCOME">Manage Income</span>
        </a>
      </li>
    </ul>
  </li> -->

  <li class="line dk hidden-folded"></li>

  <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
    <span translate="aside.nav.your_stuff.OTHERS">Your Stuff</span>
  </li>
  <li>
    <a ui-sref="profile({ userid: '<?php echo $username['pi_id']; ?>' })">
      <i class="icon-user icon text-success-lter"></i>
      <!-- <b class="badge bg-success dk pull-right">30%</b> -->
      <span translate="aside.nav.your_stuff.PROFILE">Profile</span>
    </a>
  </li>
  <li>
    <a ui-sref="app.docs">
      <i class="glyphicon glyphicon-tasks icon"></i>
      <span translate="aside.nav.your_stuff.LOGS">System Logs</span>
    </a>
  </li>

  <?php if($userinfo->task == "Administrator"){ ?>
    <li>
      <a ui-sref="settings">
        <i class="glyphicon glyphicon-cog icon"></i>
        <span translate="aside.nav.your_stuff.SETTINGS">Settings</span>
      </a>
    </li>
  <?php } ?>
</ul>
<!-- / third -->

    </nav>
    <!-- nav -->

    <!-- aside footer -->
    <div class="wrapper m-t">
      <div class="text-center-folded">
        <span class="pull-right pull-none-folded">60%</span>
        <span class="hidden-folded" translate="aside.MEMORY">Milestone</span>
      </div>
      <progressbar value="60" class="progress-xxs m-t-sm dk" type="info"></progressbar>
      <div class="text-center-folded">
        <span class="pull-right pull-none-folded">35%</span>
        <span class="hidden-folded" translate="aside.DISK">Release</span>
      </div>
      <progressbar value="35" class="progress-xxs m-t-sm dk" type="primary"></progressbar>
    </div>
    <!-- / aside footer -->
  </div>
</div>
  </div>
  <!-- / menu -->
  <!-- content -->
  <div class="app-content">
    <div ui-butterbar></div>
    <a href class="off-screen-toggle hide" ui-toggle-class="off-screen" data-target=".app-aside" ></a>
    <div class="app-content-body fade-in-up" ui-view></div>
  </div>
  <!-- /content -->
  <!-- aside right -->
  <div class="app-aside-right pos-fix no-padder w-md w-auto-xs bg-white b-l animated fadeInRight hide">
    <div class="vbox">
      <div class="wrapper b-b b-light m-b">
        <a href class="pull-right text-muted text-md" ui-toggle-class="show" target=".app-aside-right"><i class="icon-close"></i></a>
        Chat
      </div>
      <div class="row-row">
        <div class="cell">
          <div class="cell-inner padder">
            <!-- chat list -->
            <div class="m-b">
              <a href class="pull-left thumb-xs avatar"><img src="/img/a2.jpg" alt="..."></a>
              <div class="clear">
                <div class="pos-rlt wrapper-sm b b-light r m-l-sm">
                  <span class="arrow left pull-up"></span>
                  <p class="m-b-none">Hi John, What's up...</p>
                </div>
                <small class="text-muted m-l-sm"><i class="fa fa-ok text-success"></i> 2 minutes ago</small>
              </div>
            </div>
            <div class="m-b">
              <a href class="pull-right thumb-xs avatar"><img src="/img/a3.jpg" class="img-circle" alt="..."></a>
              <div class="clear">
                <div class="pos-rlt wrapper-sm bg-light r m-r-sm">
                  <span class="arrow right pull-up arrow-light"></span>
                  <p class="m-b-none">Lorem ipsum dolor :)</p>
                </div>
                <small class="text-muted">1 minutes ago</small>
              </div>
            </div>
            <div class="m-b">
              <a href class="pull-left thumb-xs avatar"><img src="/img/a2.jpg" alt="..."></a>
              <div class="clear">
                <div class="pos-rlt wrapper-sm b b-light r m-l-sm">
                  <span class="arrow left pull-up"></span>
                  <p class="m-b-none">Great!</p>
                </div>
                <small class="text-muted m-l-sm"><i class="fa fa-ok text-success"></i>Just Now</small>
              </div>
            </div>
            <!-- / chat list -->
          </div>
        </div>
      </div>
      <div class="wrapper m-t b-t b-light">
        <form class="m-b-none">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Say something">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button">SEND</button>
            </span>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- / aside right -->

  <!-- footer -->
  <div class="app-footer wrapper b-t bg-light">
    <span class="pull-right">{[{app.version}]} <a href ui-scroll="app" class="m-l-sm text-muted"><i class="fa fa-long-arrow-up"></i></a></span>
    &copy; 2015 Copyright.
  </div>
  <!-- / footer -->

</div>
<!-- JS -->
<?php echo $this->tag->javascriptInclude('vendors/jquery/dist/jquery.min.js'); ?>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<?php echo $this->tag->javascriptInclude('be/js/jquery/custom-jquery.js'); ?>

<!-- angular -->
<?php echo $this->tag->javascriptInclude('vendors/angular/angular.min.js'); ?>
<?php echo $this->tag->javascriptInclude('vendors/bootstrap/dist/js/bootstrap.min.js'); ?>

<?php echo $this->tag->javascriptInclude('vendors/angular-cookies/angular-cookies.min.js'); ?>
<?php echo $this->tag->javascriptInclude('vendors/angular-animate/angular-animate.min.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/angular-ui-router.min.js'); ?>
<!-- <?php echo $this->tag->javascriptInclude('vendors/angular-ui-router/release/angular-ui-router.min.js'); ?> -->
<?php echo $this->tag->javascriptInclude('be/js/angular/angular-translate.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/ngStorage.min.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/ui-load.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/ui-jq.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/angular/ui-validate.js'); ?>
<!-- <?php echo $this->tag->javascriptInclude('be/js/angular/ui-bootstrap-tpls.min.js'); ?> -->
<?php echo $this->tag->javascriptInclude('vendors/angular-bootstrap/ui-bootstrap-tpls.min.js'); ?>

<?php echo $this->tag->javascriptInclude('be/js/jsPDF-master/jspdf.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/jsPDF-master/pdfFromHTML.js'); ?>

<!-- <?php echo $this->tag->javascriptInclude('/vendors/angular-pdf/dist/angular-pdf.min.js'); ?> -->

<!-- APP -->
<?php echo $this->tag->javascriptInclude('be/js/scripts/app.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/scripts/factory/factory.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/scripts/controllers/controllers.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/scripts/directives/directives.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/scripts/directives/showErrors.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/scripts/config.js'); ?>

<!-- CKeditor -->
<?php echo $this->tag->javascriptInclude('be/js/ckeditor/ckeditor.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/ckeditor/styles.js'); ?>

<!-- Page Controllers -->
<?php echo $this->tag->javascriptInclude('be/js/scripts/controllers/page/createpage.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/scripts/controllers/page/managepage.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/scripts/controllers/page/managepagecategory.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/scripts/controllers/page/editpage.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/scripts/factory/page/managepagecategory.js'); ?>

<!-- News Controllers -->
<?php echo $this->tag->javascriptInclude('be/js/scripts/controllers/news/editnews.js'); ?>
<?php echo $this->tag->javascriptInclude('be/js/scripts/controllers/news/editnewscenter.js'); ?>

<?php echo $this->tag->javascriptInclude('be/js/scripts/filter.js'); ?>

<!-- other plugins -->
<!-- ng upload -->
<?php echo $this->tag->javascriptInclude('vendors/ng-file-upload/ng-file-upload.min.js'); ?>
<?php echo $this->tag->javascriptInclude('vendors/ng-file-upload/ng-file-upload-shim.js'); ?>

<!-- xeditable -->
<?php echo $this->tag->javascriptInclude('vendors/angular-xeditable/dist/js/xeditable.min.js'); ?>

<?php echo $this->tag->javascriptInclude('vendors/angular-moment/angular-moment.min.js'); ?>
<?php echo $this->tag->javascriptInclude('vendors/moment/min/moment.min.js'); ?>
<?php echo $this->tag->javascriptInclude('vendors/moment-range/dist/moment-range.min.js'); ?>

<!-- Calendar -->
<?php echo $this->tag->stylesheetLink('be/js/jquery/fullcalendar/fullcalendar.css'); ?>
<?php echo $this->tag->javascriptInclude('vendors/moment/min/moment.min.js'); ?>
<?php echo $this->tag->javascriptInclude('vendors/angular-ui-calendar/src/calendar.js'); ?>
<?php echo $this->tag->javascriptInclude('vendors/fullcalendar/dist/fullcalendar.min.js'); ?>
<?php echo $this->tag->javascriptInclude('vendors/fullcalendar/dist/gcal.js'); ?>


<!-- Notifications -->
<?php echo $this->tag->javascriptInclude('vendors/angular-ui-notification/dist/angular-ui-notification.min.js'); ?>

<!-- Countries -->
<?php echo $this->tag->javascriptInclude('fe/scripts/countries3.js'); ?>

<!-- colorpicker -->
<?php echo $this->tag->javascriptInclude('vendors/angular-bootstrap-colorpicker/js/bootstrap-colorpicker-module.min.js'); ?>
<?php echo $this->tag->javascriptInclude('vendors/angular-ui-select/dist/select.js'); ?>
<!-- include the js -->

<!-- ui autocomplete -->
<script type="text/javascript" src="/vendors/ui-autocomplete/autocomplete.js"></script>


<!-- <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular-sanitize.js"></script> -->
<script src="/vendors/angular-latest/src/ngSanitize/sanitize.js"></script>
<script type="text/javascript" src="/vendors/angular-ui-sortable/sortable.js"></script>
<!-- <script type="text/javascript" src="/be/js/jquery/nestable/jquery.nestable2.js"></script> -->
<!-- 

<!-- uuid -->
<?php echo $this->tag->javascriptInclude('vendors/angular-uuid-service/angular-uuid-service.min.js'); ?>

<script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui/0.4.0/angular-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.jquery.min.js"></script>

<!-- include angular-chosen -->
<script src="/vendors/angular-chosen/angular-chosen.js"></script>
<!-- ui -->
<script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui/0.4.0/angular-ui.min.js"></script>

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


<script src="https://code.highcharts.com/highcharts.src.js"></script>
<script src="http://code.highcharts.com/adapters/standalone-framework.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<?php echo $this->tag->javascriptInclude('vendors/highcharts-ng/dist/highcharts-ng.min.js'); ?>

</body>
</html>
