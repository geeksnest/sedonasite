<?php echo $this->getContent(); ?>

<script type="text/ng-template" id="deleteProduct.html">
  <div ng-include="'/be/tpl/deleteProduct.html'"></div>
</script>

<script type="text/ng-template" id="editProduct.html">
  <div ng-include="'/be/tpl/editProduct.html'"></div>
</script>

<script type="text/ng-template" id="addProductQuantity.html">
  <div ng-include="'/be/tpl/addProductQuantity.html'"></div>
</script>

<script type="text/ng-template" id="addDiscount.html">
  <div ng-include="'/be/tpl/addDiscount.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Manage Product</h1>
  <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" name="formmanagenews">
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
      <div class="row">
        <div class="col-sm-4">
          <a ui-sref="addproduct" class="btn btn-primary" style="margin-top:43px;">Add Product</a>
          <a ng-click="print()" class="btn btn-default" style="margin-top:43px;">Print</a>
        </div>
        <div class="col-sm-8">
          <div class="row row-sm text-center">
            <div class="col-xs-3 mouse-pointer">
              <a href="#" ng-click="notif('at')" class="block panel padder-v bg-info item">
                <span class="text-white font-thin h1 block">{[{ added_today }]}</span>
                <span class="text-muted text-xs">Added Today</span>
              </a>
            </div>
            <div class="col-xs-3 mouse-pointer">
              <a href="#" ng-click="notif('nos')" class="block panel padder-v bg-warning item">
                <span class="text-white font-thin h1 block">{[{ nearly_out_of_stock }]}</span>
                <span class="text-muted text-xs">Nearly Out of Stock</span>
              </a>
            </div>
            <div class="col-xs-3 mouse-pointer">
              <a href="#" href ng-click="notif('os')" class="block panel padder-v bg-danger item">
                <span class="text-white font-thin h1 block">{[{ out_of_stock }]}</span>
                <span class="text-muted text-xs">Out of Stock</span>
              </a>
            </div>
            <div class="col-xs-3">
              <a href="#" ng-click="notif(null)" class="block panel padder-v bg-primary item">
                <span class="text-white font-thin h1 block">{[{ totalp }]}</span>
                <span class="text-muted text-xs">Total Product</span>
              </a>
            </div>
          </div>
        </div>
        <div class="col-sm-12">

          <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Product List
            </div>

              <div class="panel-body">
                <div class="row wrapper">
                    <div class="col-sm-5 m-b-xs" ng-show="keyword">
                        <strong>{[{ bigTotalItems }]}</strong> Results found for: <strong> "{[{ keyword }]}"</strong> <button class="btn btn-default btn-xs" ng-click="clear()">Clear</button>
                    </div>
                    <div class="col-sm-5 m-b-xs pull-right">
                        <div class="input-group">
                            <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                            <span class="input-group-btn">
                            <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                              <th>Product Code</th>
                              <th style="width:25%;">Name</th>
                              <th>Category</th>
                              <th>Quantity</th>
                              <th style="width:5%;">Price</th>
                              <th style="width:5%;">Availability</th>
                              <th style="width:5%;">Status</th>
                              <th style="width:25%;">Action</th>
                            </tr>
                        </thead>
                        <tbody ng-show="loading">
                            <tr colspan="4">
                                <td>Loading Products</td>
                            </tr>
                        </tbody>
                        <tbody ng-hide="loading">
                            <tr colspan="4" ng-show="bigTotalItems==0"> <td> No records found! </td></tr>
                            <tr ng-repeat="mem in products">
                                <td>{[{ mem.productcode }]}</td>
                                <td>{[{ mem.name }]}</td>
                                <td><span ng-repeat="category in mem.category">{[{ category.category }]} <span ng-if="!$last">, </span></span></td>
                                <td>{[{ mem.quantity }]}</td>
                                <td>{[{ "$ "+mem.price }]}</td>
                                <td>{[{ mem.quantity > 0 ? "In Stock" : "Out of Stock"}]}</td>
                                <td  ng-if="mem.status == 1">
                                  <div class="pagestatuscontent fade-in-out"><span class="label bg-success" >Active</span></div>
                                  <div class="checkstatuscontent">
                                    <label class="i-switch bg-info m-t-xs m-r">
                                      <input type="checkbox" checked="" ng-click="setstatus(mem.status,mem.productid)">
                                      <i></i>
                                    </label>
                                    
                                  </div>
                                  <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.productid"><i class="fa fa-check"></i></spand></div>
                                </td>
                                <td  ng-if="mem.status == 0">
                                  <div class="pagestatuscontent fade-in-out"><span class="label bg-danger">Deactivated</span></div>
                                  <div class="checkstatuscontent">
                                    <label class="i-switch bg-info m-t-xs m-r">
                                      <input type="checkbox" ng-click="setstatus(mem.status,mem.productid)">
                                      <i></i>
                                    </label>
                                   
                                  </div>
                                   <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow ==mem.productid"><i class="fa fa-check"></i></spand></div>
                                </td>
                                <td>
                                    <a href="" ng-click="edit(mem.productid)"><span class="label bg-warning" >Edit</span></a>
                                    <a href="" ng-click="view(mem.productid)"><span class="label bg-info" >View</span></a>
                                    <a href="" ng-click="addquantity(mem.productid, mem.quantity)"><span class="label bg-success" >Add Quantity</span></a>
                                    <a href="" ng-click="adddiscount(mem)"><span class="label bg-primary" >Discount Settings</span></a>
                                    <a href="" ng-click="delete(mem.productid)"> <span class="label bg-danger">Delete</span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

              </div>


          </div>
        </div>

      </div>

      <div class="row" ng-hide="bigTotalItems==0 || loading">
        <div class="panel-body">
            <footer class="panel-footer text-center bg-light lter">
                <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
                <pagination ng-hide="maxSize > bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </footer>
        </div>
      </div>

  </div>
</fieldset>
</form>

<div id="print">  
  <img src="/img/frontend/sedonalogo.png" style="width:140px;
  visibility: collapse;
  position: absolute;
  top:0;
  margin-left: 240px;">
  <div class="table-responsive">
    <table class="table table-striped b-t b-light" style='position:absolute;bottom:200000px'>
      <h5 class="hide">Product list</h5>
        <thead>
          <tr>
            <th style="width: 18%">Product code</th>
            <th style="width: 25%">Item name</th>
            <th style="width: 15%">Category</th>
            <th style="width: 10%">Quantity</th>
            <th style="width: 10%">Price</th>
            <th style="width: 13%">Availability</th>
            <th style="width: 10%">Status</th>
          </tr>
        </thead>
        <tbody ng-hide="loading">
          <tr ng-repeat="mem in product2">
            <td>{[{ mem.productcode }]}</td>
            <td>{[{ mem.name }]}</td>
            <td><span ng-repeat="category in mem.category">{[{ category.category }]} <span ng-if="!$last">, </span></span></td>
            <td>{[{ mem.quantity }]}</td>
            <td>{[{ "$ "+mem.price }]}</td>
            <td>{[{ mem.quantity > 0 ? "In Stock" : "Out of Stock"}]}</td>
            <td>{[{ mem.status == 1 ? "Active" : "Inactive"}]}</td>
          </tr>
        </tbody>
    </table>
  </div>
</div>