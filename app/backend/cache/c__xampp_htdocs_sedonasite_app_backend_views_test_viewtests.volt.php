<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="ViewTest.html">
    <div ng-include="'/be/tpl/test/ViewTest.html'"></div>
</script>

<script type="text/ng-template" id="DeleteTest.html">
    <div ng-include="'/be/tpl/test/DeleteTest.html'"></div>
</script>
<style type="text/css">
	.table > tbody + tbody {
    border-top: 1px solid #ddd;
}
</style>
 <div class="bg-light lter b-b wrapper-md">
    <h1 class="m-n font-thin h3">Manage Test </h1>
</div>

<div>
  
<div class="wrapper-md">
 <form method="POST" ng-submit="Create(testid,testname,response,sample,allB,allC,allAC,allAB,allBC)" class="form-validation ng-invalid ng-invalid-required ng-invalid-validator" name="testform">

	    <div class="row">
	   
	    	<div class="col-sm-12">
	 
	    		<div class="panel panel-default">
	    		<div class='panel-heading font-bold'>
	    			Test List
	    			</div>
	    		<div class='panel-body'>
	    		 <div class="col-sm-5 m-b-xs" ng-show="keyword">
                        <strong>{[{ bigTotalItems }]}</strong> Results found for: <strong> "{[{ keyword }]}"</strong> <button class="btn btn-default btn-xs" ng-click="clear()">Clear</button>
                    </div>
	    		   		  <div class="col-sm-5 m-b-xs pull-right">
	    		    <div class="input-group">
                            <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                            <span class="input-group-btn">
                            <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                            </span>
                        </div>
                        </div><br>
	    		<div class="col-sm-12">
	    		<table class='table'>
	    		<thead>
	    		<th>
	    		TEST NAME
	    		</th>
	    		<th>
	    		STATUS
	    		</th>
	    		<th>
	    		ACTION
	    		</th>
	    		</thead>
	    		<tbody ng-repeat='ss in testlist'>
	    		<td>
	    		{[{ss.testname}]}
	    		</td>
	    		<td ng-if="ss.status == 1">
                                <div class="pagestatuscontent fade-in-out"><span class="label bg-success" >Active</span></div>
                                <div class="checkstatuscontent">
                                  <label class="i-switch bg-info m-t-xs m-r">
                                    <input type="checkbox" checked="" ng-click="setstatus(ss.testid,ss.status)">
                                    <i></i>
                                  </label>
                                  
                                </div>
                                <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == ss.status"><i class="fa fa-check"></i></spand></div>
                                </td>
                                <td ng-if="ss.status == 0">
                                <div class="pagestatuscontent fade-in-out"><span class="label bg-danger">Deactivated</span></div>
                                <div class="checkstatuscontent">
                                  <label class="i-switch bg-info m-t-xs m-r">
                                    <input type="checkbox" ng-click="setstatus(ss.testid,ss.status)">
                                    <i></i>
                                  </label>
                                 
                                </div>
                                <!--  <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow ==mem.newsslugs"><i class="fa fa-check"></i></spand></div> -->
                                </td>

	    		<td>

	    			<a href class="btn btn-warning btn-xs" ng-click="view(ss.testid)">
	    			<!-- <i class="fa fa-edit"></i> -->
	    			Edit
	    			</a>

	    		<a href class="btn btn-danger btn-xs" ng-click="delete(ss.testid)">
                                    <!-- <i class="fa fa-trash-o"></i> -->
                                    Delete
                                </a>

	    		</td>

	    		</tbody>
	    		</table>

	    		</div>
	    		</div>
	    	</div>
	    </div>

	</div>
	  <div class="row" ng-hide="bigTotalItems==0 || loading">
        <div class="panel-body">
            <footer class="panel-footer text-center bg-light lter">
                <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
                <pagination ng-hide="maxSize > bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </footer>
        </div>
      </div>


</form> 
</div>


