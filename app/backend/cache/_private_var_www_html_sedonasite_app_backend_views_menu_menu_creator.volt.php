<?php echo $this->getContent(); ?>


<script type="text/ng-template" id="deletemenu.html">
  <div ng-include="'/be/tpl/menu/deletemenu.html'"></div>
</script>

<script type="text/ng-template" id="editMenu.html">
  <div ng-include="'/be/tpl/menu/editMenu.html'"></div>
</script>
<script type="text/ng-template" id="addMenu.html">
  <div ng-include="'/be/tpl/menu/addMenu.html'"></div>
</script>

<script type="text/ng-template" id="editSubMenu.html">
  <div ng-include="'/be/tpl/menu/editSubMenu.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Menu Creator</h1>
  <a id="top"></a>
</div>
<style>

        * {
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }

        [ng-drag] {
            -moz-user-select: -moz-none;
            -khtml-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        [ng-drag] {
            /*width: 100px;*/
            /*height: 100px;*/
            /*background: rgba(255, 0, 0, 0.5);*/
            /*color: white;*/
            /*text-align: center;*/
            /*padding-top: 40px;*/
            /*display: block;*/
            cursor: move;
        }

        [ng-drag].one {
            /*background: rgba(255, 0, 0, 0.5);*/
        }

        [ng-drag].two {
            /*background: rgba(0, 255, 0, 0.5);*/
        }

        [ng-drag].three {
            /*background: rgba(0, 0, 255, 0.5);*/
        }

        [ng-drag].drag-over {
            /*border: solid 1px red;*/
        }

        [ng-drag].dragging {
            opacity: 0.5;
        }

        [ng-drop] {
            /*background: rgba(0, 0, 0, 0.25);*/
            /*text-align: center;*/
            display: block;
            position: relative;
            padding: 10px;
            /*width: 140px;
            height: 140px;*/
            /*float: left;*/
        }

        [ng-drop].drag-enter {
            background: rgba(0, 0, 0, 0.25);
            /*border: solid 1px red;*/
        }

        [ng-drop] span.title {
            display: block;
            position: absolute;
            /*top: 50%;*/
            /*left: 50%;*/
            /*width: 200px;*/
            /*height: 20px;*/
            /*margin-left: -100px;*/
            /*margin-top: -10px;*/
        }

        [ng-drop] div {
            position: relative;
            z-index: 2;
        }

        .draglist {
            display: inline-block;
            margin: 0 auto;
        }

        ul.ulmenu{
          padding-left: 5px;
        }
        ul.ulmenu li:hover{
          background: rgba(0, 0, 0, 0.25);
        }


    </style>

<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading font-bold">
      <div class="row">
        <div class="col-sm-5">
          <!-- <div class="input-group">
          <input class="input-sm form-control" placeholder="Enter new menu name" type="text" ng-model="data.newmenu" ng-change="ontitle(data.newmenu)">
            <span class="input-group-btn">
              <button class="btn btn-sm btn-info" ng-click="addmenu(data)" ng-disabled="data.newmenu == '' || data.newmenu ==  null"><i class="glyphicon glyphicon-plus"></i> New Menu</button>
            </span>
          </div> -->
             <span class="input-group-btn">
              <button class="btn btn-sm btn-info" ng-click="addnewmenu()"><i class="glyphicon glyphicon-plus"></i> New Menu</button>
            </span>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-sm-8">
    <div class="wrapper-md">
      <div class="panel panel-default">
        <div class="table-responsive">
          <table class="table table-striped b-t b-light">
            <thead>
              <tr>
                <th style="width:50%">Menu Name</th>
                <th style="width:30%">Sort Code</th>
                <th style="width:20%">Action</th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="menu in menus">
                <td><span>{[{menu.name}]}</span></td>
                <td><span ng-bind="menu.shortCode"></span></td>
                <td>
                  
                      <a href class="btn btn-warning btn-xs" ng-click="editmenu(menu.menuID)">Edit</a>
                    
                      <a href class="btn btn-danger btn-xs" ng-click="deletemenu(menu.menuID)">Delete</a>
                    
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- <div class="row">
  <div class="col-sm-6">
    <div class="wrapper-md">
      <div class="panel panel-default">
        <div class="panel-heading font-bold">
          Menu Name
        </div>
        <ul class="ulmenu">
          <li ng-repeat="menu in menus" ng-drop="true" ng-drop-success="onDropMenu($index, $data,$event)">
            <div ng-drag="true" ng-drag-data="menu" ng-class="menu.name">
              <span>{[{menu.name}]}</span>
              <span>{[{menu.name}]}</span>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div> -->