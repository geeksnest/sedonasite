<?php echo $this->getContent(); ?>
<style type="text/css">
  .ui-sortable-placeholder {
    border: 1px dotted black;
    visibility: visible !important;
    background-color: inherit;
}
.ssortable{
    overflow-y: hidden;
    overflow-x: hidden;
    max-height: 1000px;
/*    width: auto;*/
    cursor: move;
}
</style>
<script type="text/ng-template" id="pageDelete.html">
<div ng-include="'/be/tpl/pageDelete.html'"></div>
</script>
<script type="text/ng-template" id="pageEdit.html">
<div ng-include="'/be/tpl/pageEdit.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Service List</h1>
  <a id="top"></a>
</div>
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
  <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading font-bold">
          Service List
        </div>
        <div class="panel-body">

                  <div class="row wrapper">
            <div class="col-sm-5 m-b-xs" ng-show="keyword">
              <strong>{[{ bigTotalItems }]}</strong> Results found for: <strong> "{[{ keyword }]}"</strong> <button class="btn btn-default btn-xs" ng-click="clear()">Clear</button>
            </div>
            <div class="col-sm-5 m-b-xs pull-right">
              <div class="input-group">
                <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                <span class="input-group-btn">
                  <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                </span>
              </div>
              <br>
              <div class="input-group" style="width:100%">
                Sort by:
                <div class="pull-right">
                  <label class="radio-inline">
                    <input type="radio" ng-model="sort" ng-click="sortpage(sort)" value="title"> Title
                  </label>
                  <!-- <label class="radio-inline">
                    <input type="radio" ng-model="sort" ng-click="sortpage(sort)" value="pagecategory.title"> Category
                  </label> -->
                  <label class="radio-inline">
                    <input type="radio" ng-model="sort" ng-click="sortpage(sort)" value="pagecreated_at"> Date created
                  </label>
                  <label class="radio-inline">
                    <input type="radio" ng-model="sort" ng-click="sortpage(sort)" value="pageupdated_at"> Date updated
                  </label>
                </div>
              </div>
            </div>
          </div>

                                <div class="row wrapper" style="margin: 0px 5px;">
                            <div class="overlay" ng-show="processing">
                                <div class="loader">
                                    <div class="bar"></div>
                                    <div class="bar"></div>
                                    <div class="bar"></div>
                                </div>
                            </div>
                            <tabset class="tab-container">
                                <tab ng-repeat="n in datas" heading="{[{n.heading}]}" active="tab.active" ng-init="tab.active = $index === 0">

                                  <div class="table-responsive">
                                  
                                    <table class="ssortable table table-striped b-t b-light">
                                      <thead>
                                        <tr>

                                          <th style="width:40%">Title</th>
                                          <th style="width:15%">Service Slugs</th>
                                          <th style="width:15%">Category</th>
                                          <th style="width:15%">Service Status</th>
                                          <th style="width:15%">Action</th>
                                        </tr>
                                      </thead>
                                      <tbody class="ssortable" ui:sortable ng:model="n.dat">
                                      <tr> </tr>
                                        <tr ng:repeat="mem in n.dat" data-id="n.dat[$index].pageid">
                                        {[{mem.category}]}
                                          <td><a href="<?php echo $base_url;?>/{[{ mem.categorytitle | lowercase }]}/{[{ mem.pageslugs }]}" target="_blank">{[{ mem.title }]} <!-- {[{$index}]} --> </a></td>
                                          <td>{[{ mem.pageslugs }]}</td>
                                          <td ng-if="mem.categorytitle">{[{ mem.categorytitle }]}</td>
                                          <td ng-if="mem.categorytitle==null"> Others </td>
                                          <td ng-if="mem.status == 1">
                                            <div class="pagestatuscontent fade-in-out"><span class="label bg-success" >Active</span></div>
                                            <div class="checkstatuscontent">
                                              <label class="i-switch bg-info m-t-xs m-r">
                                                <input type="checkbox" checked="" ng-click="setstatus(mem.status,mem.pageid,searchtext)">
                                                <i></i>
                                              </label>

                                            </div>
                                            <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.pageid"><i class="fa fa-check"></i></spand></div>
                                          </td>
                                          <td  ng-if="mem.status == 0">
                                            <div class="pagestatuscontent fade-in-out"><span class="label bg-danger">Deactivated</span></div>
                                            <div class="checkstatuscontent">
                                              <label class="i-switch bg-info m-t-xs m-r">
                                                <input type="checkbox" ng-click="setstatus(mem.status,mem.pageid,searchtext)">
                                                <i></i>
                                              </label>

                                            </div>
                                            <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.pageid"><i class="fa fa-check"></i></spand></div>
                                          </td>
                                        </td>
                                        <td>
                                          <a href="" ng-click="editpage(mem.pageid)"><span class="label bg-warning" >Edit</span></a>
                                          <a href="" ng-click="deletepage(mem.pageid)"> <span class="label bg-danger">Delete</span></a>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>

                                  <div class="col-sm-12 m-b-xs">
                                   <button ng-hide="n.count==0" class="btn btn-success btn-addon" ng-click="shit(n.dat)"><i class="fa fa-plus"></i>Update</button>
                                 </div>
    

                                 <!--  <div class="row" ng-hide="{[{n.count}]}==0 || loading">
                                    <div class="panel-body">
                                      <footer class="panel-footer text-center bg-light lter">
                                        <entries max="maxSize" offset="bigCurrentPage" total="{[{n.count}]}"></entries>
                                        <pagination ng-hide="maxSize >= {[{n.count}]}" total-items="{[{n.count}]}" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
                                      </footer>
                                    </div>
                                  </div> -->

                                </tab>

                                <tab heading="Others">
                                  <div class="table-responsive">
                                    <table class="ssortable table table-striped b-t b-light">
                                      <thead>
                                        <tr>

                                          <th style="width:40%">Title</th>
                                          <th style="width:15%">Service Slugs</th>
                                          <th style="width:15%">Category</th>
                                          <th style="width:15%">Service Status</th>
                                          <th style="width:15%">Action</th>
                                        </tr>
                                      </thead>
                                      <tbody class="ssortable" ng-hide="loading">
                                      <tr> </tr>
                                        <tr ng-repeat="mem in others">
                                        {[{mem.category}]}
                                          <td><a href="<?php echo $base_url;?>/{[{ mem.categorytitle | lowercase }]}/{[{ mem.pageslugs }]}" target="_blank">{[{ mem.title }]} <!-- {[{$index}]} --> </a></td>
                                          <td>{[{ mem.pageslugs }]}</td>
                                          <td ng-if="mem.categorytitle">{[{ mem.categorytitle }]}</td>
                                          <td ng-if="mem.categorytitle==null"> Others </td>
                                          <td ng-if="mem.status == 1">
                                            <div class="pagestatuscontent fade-in-out"><span class="label bg-success" >Active</span></div>
                                            <div class="checkstatuscontent">
                                              <label class="i-switch bg-info m-t-xs m-r">
                                                <input type="checkbox" checked="" ng-click="setstatus(mem.status,mem.pageid,searchtext)">
                                                <i></i>
                                              </label>

                                            </div>
                                            <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.pageid"><i class="fa fa-check"></i></spand></div>
                                          </td>
                                          <td  ng-if="mem.status == 0">
                                            <div class="pagestatuscontent fade-in-out"><span class="label bg-danger">Deactivated</span></div>
                                            <div class="checkstatuscontent">
                                              <label class="i-switch bg-info m-t-xs m-r">
                                                <input type="checkbox" ng-click="setstatus(mem.status,mem.pageid,searchtext)">
                                                <i></i>
                                              </label>

                                            </div>
                                            <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.pageid"><i class="fa fa-check"></i></spand></div>
                                          </td>
                                        </td>
                                        <td>
                                          <a href="" ng-click="editpage(mem.pageid)"><span class="label bg-warning" >Edit</span></a>
                                          <a href="" ng-click="deletepage(mem.pageid)"> <span class="label bg-danger">Delete</span></a>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>


                                <div class="row" ng-hide="ocount==0 || loading">
                                  <div class="panel-body">
                                    <footer class="panel-footer text-center bg-light lter">
                                      <entries max="maxSize" offset="bigCurrentPage" total="ocount"></entries>
                                      <pagination ng-hide="maxSize >= ocount" total-items="ocount" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
                                    </footer>
                                  </div>
                                </div>

                                </tab>

                            </tabset>
                        </div>


</div>
</fieldset>
