<?php echo $this->getContent(); ?>
<style type="text/css">
.fc-left h2{
  font-size: 22px!important;
  color:#666;
}
.fc-toolbar{
  background-color: #fff!important;
  font-weight: bolder!important;
}
.fc-toolbar .fc-button {
  color:#666!important;
  background-color:  #f5f5f5!important;
  height: 40px!important;
}
</style>
<script type="text/ng-template" id="mediagallery.html">
  <div ng-include="'/be/tpl/mediagallery.html'"></div>
</script>
<script type="text/ng-template" id="column.html">
  <div ng-include="'/be/tpl/pagebuilder/column.html'"></div>
</script>
<script type="text/ng-template" id="module.html">
  <div ng-include="'/be/tpl/pagebuilder/module.html'"></div>
</script>
<script type="text/ng-template" id="moduletext.html">
  <div ng-include="'/be/tpl/pagebuilder/moduletext.html'"></div>
</script>
<script type="text/ng-template" id="moduleimage.html">
  <div ng-include="'/be/tpl/pagebuilder/moduleimage.html'"></div>
</script>
<script type="text/ng-template" id="moduletestimonial.html">
  <div ng-include="'/be/tpl/pagebuilder/moduletestimonial.html'"></div>
</script>
<script type="text/ng-template" id="modulecontact.html">
  <div ng-include="'/be/tpl/pagebuilder/modulecontact.html'"></div>
</script>
<script type="text/ng-template" id="modulenews.html">
  <div ng-include="'/be/tpl/pagebuilder/modulenews.html'"></div>
</script>
<script type="text/ng-template" id="moduledivider.html">
  <div ng-include="'/be/tpl/pagebuilder/moduledivider.html'"></div>
</script>
<script type="text/ng-template" id="modulebutton.html">
  <div ng-include="'/be/tpl/pagebuilder/modulebutton.html'"></div>
</script>
<script type="text/ng-template" id="stylepreview">
  <div ng-include="'/be/tpl/pagebuilder/bannerpreview.html'"></div>
</script>
<script type="text/ng-template" id="deleterow.html">
  <div ng-include="'/be/tpl/pagebuilder/deleterow.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Edit Page</h1>
  <a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="savePage(page)" name="formpage">
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)"> {[{ alert.msg }]} </alert>
      <div class="row">
        <div class="col-sm-8">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Page Information <span style="color:red"> * </span>
            </div>
            <input type="hidden" ng-model='page.pageid'>
              <div class="panel-body">
                Page Type
                <div class="form-group"><br>
              <div class="btn-group">
                <label class="btn btn-info" ng-model="page.pagetype" btn-radio="'Normal'"><i class="fa fa-check text-active"></i> Normal</label>
                <label class="btn btn-success" ng-model="page.pagetype" btn-radio="'Special'"><i class="fa fa-check text-active"></i> Special</label>
              </div>
              </div>
                <div class="line line-dashed b-b line-lg"></div>
                Title
                <input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.title" required="required" ng-keyup="onpagetitle(page.title);uniqueslugs(page.slugs)">
                <br>
                <div class="line line-dashed b-b line-lg"></div>
                <span ng-show="validslugs" class="pull-left mg-left text-danger">Slugs is already exist, please change it.</span>
                <div ng-show="validslugs" class="line line-dashed b-b line-lg"></div>
                <b class="pull-left">Page Slugs: </b>
                <input type="text" ng-show="editslug" id="pageslugs" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern pull-left" ng-model="page.slugs" ng-keypress="onslugs(page.slugs)" ng-change="uniqueslugs(page.slugs)"><span ng-bind="page.slugs" class="pull-left mg-left"></span>
                <div ng-show="editslug">
                  <a class="btn btn-danger btn-xs pull-right mg-left" ng-click="cancelpageslug(page.title)">cancel</a>
                   <button type="button" class="btn btn-primary btn-xs pull-right mg-left" ng-disabled="validslugs" ng-click="setslug(page.slugs)">ok</button>
                </div>
                <a class="btn btn-danger btn-xs pull-right mg-left" ng-hide="editslug" ng-click="clearslug(page.title)">clear</a>
                <a class="btn btn-primary btn-xs pull-right mg-left" ng-hide="editslug" ng-click="editpageslug()">edit slug</a>
                <div class="line line-dashed b-b line-lg"></div>
              </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Page Content <span style="color:red"> * </span>
            </div>
            <div class="panel-body" ng-if="page.pagetype=='Normal'">
                Content
                <a class="btn btn-default btn-xs" ng-click="media('content')"><i class="fa  fa-folder-open"></i> Media Library</a>
                <br><br>
                <textarea class="ck-editor" ng-model="page.body" required="required" id="normaleditor"></textarea>

            </div>

             <div class="panel-body" ng-if="page.pagetype=='Special'">
 <div class="clear specialpage_pad">
                <button type="button" class="pull-right btn m-b-xs btn-sm btn-default btn-addon" ng-click="rownewitem($event)"><i class="fa fa-plus"></i>Add Row</button>
              </div>
         
            <ul class="list-group no-radius sortablecol" ui:sortable ng:model="row1">
                  <li class="" ng-repeat="(indexX,count) in row1 track by indexX">

          <br>
                   
              <div class="bg-light">
           <!--    {[{$index}]} -->
           
                <div class="clear">
                   <button type="button" class="pull-right btn m-b-xs btn-sm btn-addon btn-danger mg-left" ng-click="rowdelitem($index)"><i class="fa fa-trash-o"></i>Delete Row</button>
                  <!--  <div ng-init="count.colcount = 0"></div> -->
                     <button type="button" class="pull-right btn m-b-xs btn-sm btn-addon btn-info mg-left" ng-click="insertmodule(count.colcount,$index)" ng-disabled="count.colcount==4"><i class="fa fa-plus"></i>Add Column</button>
                </div>
                <input type="hidden" ng-model="count.colcount" style="color:black" required ng-if="count.colcount!=0 || count.colcount">
                 <input type="hidden" ng-model="dummy" style="color:black" required ng-if="count.colcount==0 || !count.colcount">
                <div class="clear specialpage_pad2">
                 <ul class="list-group no-radius sortablerow" ui:sortable ng:model="count.row">
                   <li class="{[{count.colcount == 1 ? 'col-sm-12' : ''}]} {[{count.colcount == 2 ? 'col-sm-6 no-pad' : ''}]} 
                   {[{count.colcount == 3 ? 'col-sm-4 no-pad' : ''}]} {[{count.colcount == 4 ? 'col-sm-3 no-pad' : ''}]}" ng-repeat="(indexY,counter) in count.row track by indexY">

                  <div class="col-sm-12 input-group col-container" ng-if="$index == 0">
                    <a class="{[{count.column == 2 || count.column == 1 ? 'col-sm-10' : ''}]} {[{count.column == 3 ? 'col-sm-8' : ''}]} {[{count.column == 4 ? 'mainbut2' : ''}]} btn btn-sm btn-default"> {[{counter.col}]} </a>

                    <!-- TEXT MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'TEXT'" ng-model="counter.colcontent" required></textarea>
                    <button type="button" type="button" ng-if="counter.col == 'TEXT'" ng-click="edittextmodule(count.colcount,indexX,counter.colcontent,$index)" title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm  btn btn-primary"><i class="fa fa-edit"></i></button>
                    
                    <button type="button" ng-if="counter.col == 'TEXT'" ng-click="counter.col = '';counter.colcontent='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-warning"><i class="fa fa-trash-o"></i></button>
                     <div ng-if="counter.col == 'TEXT'" class="col-sm-12 txtmodule">
                     <p ng-bind-html="trustAsHtml(counter.colcontent)"></p>
                   </div>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimg" required></textarea>
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimglink"></textarea>
                    <button type="button" ng-if="counter.col == 'IMAGE'" ng-click="editimagemodule(count.colcount,indexX,counter.colimg,$index,counter.colimglink)" title="Edit" class=" {[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-primary"><i class="fa fa-edit"></i></button>

                    <button type="button" ng-if="counter.col == 'IMAGE'" ng-click="counter.col = '';counter.colimg='';counter.collink='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-warning"><i class="fa fa-trash-o"></i></button>
                    <img ng-if="counter.col == 'IMAGE'" class="imgmodule2" src="{[{s3link}]}/uploads/{[{ directory }]}/{[{counter.colimg}]}">
                    <span ng-if="counter.colimglink"> {[{counter.colimglink}]} </span>

                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonial" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonialcat" style="color:black"></textarea>
                      <button type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="edittestimonialmodule(count.colcount,indexX,counter.coltestimonial,counter.coltestimonialcat,$index)"title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="counter.col = '';counter.coltestimonial='';counter.coltestimonialcat='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-warning"><i class="fa fa-trash-o"></i></button>
                         <div ng-if="counter.col == 'TESTIMONIAL'" class="col-sm-12 margin-bot20" style="max-height:300px;overflow-y:auto" id="testi1div">
                       <div class="size18" style="padding-top:5px;"><h3 class="font1 italic">What People Are Saying</h3></div>
                       <div ng-repeat="x in testi | filter:{pageid:counter.coltestimonialcat} | limitTo:counter.coltestimonial">
                        <div class="specialpage_col">  
                          <div class="size16 border-left italic margin-top special-testi-wrapper">
                            <div class="row">
                              <div class="col-sm-12 text-center page-testi-image">
                                <img src="{[{s3link}]}/uploads/testimonialpic/{[{x.picture}]}" class="testi-img">
                              </div>
                              <div class="col-sm-12">
                                <div></div>
                                <div>
                                  <span class="fa fa-quote-left"></span>
                                  {[{x.message}]}
                                </div>
                                <div class="text-right">-  {[{x.name}]} </div>
                              </div>
                            </div>
                            <hr class="styled-hr">
                          </div>
                        </div>
                      </div>
                    </div>

                       <!-- CONTACT FORM MODULE -->
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="editcontactmodule(count.colcount,indexX,counter,$index)" title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="counter.col = '';counter.colcontact='';counter.colcontactemail='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                      <div ng-if="counter.col == 'CONTACT'" style="padding-right:10px">
                        <div class="row">
                         <div class="col-sm-12" style="height: 300px;
                          overflow-y: auto;">
                          <div class="col-sm-12">
                            <h3>Contact Us</h3>
                          </div>
                          <div class="col-sm-6">
                            <div class="form">
                                <!-- Name -->
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="name">Subject
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                        <input type="text" class="form-control textbox margin-bot8" readonly="" style="background-color:#fff">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="name">Name
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                      <input type="text" class="form-control textbox margin-bot8" placeholder="First Name" readonly="" style="background-color:#fff">
                                      </div>
                                      <div class="col-sm-12">
                                        <input type="text" class="form-control textbox" placeholder="Last Name" readonly="" style="background-color:#fff">
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Email -->
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="email">Email
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                        <span class="red hidden booking-email">Invalid Email Address</span>
                                        <input type="email" class="textbox form-control margin-bot8" readonly="" style="background-color:#fff">
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Comment -->
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                        <textarea class="form-control input-large textarea" readonly="" style="background-color:#fff"></textarea>
                                      </div>
                                    </div>
                                  </div>
                            
                                  <div class="form-actions margin-bot8">
                                    <div class="row">
                                      <div class="col-sm-12">
                                        <button type="submit" class="btn btn-sedona" disabled="" style="background-color:#fff">Submit</button>
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Buttons -->
                                </div>
                                <div class="clearfix"></div>
                              </div>

                             <div class="col-sm-6 storeinfo" style="word-wrap: break-word;padding-left: 0px;">
                                <div class="storeinfo-details">
                                  <h3 class="orange"> <span ng-bind="counter.coltitle"> </span> </h3>
                                  <div class="title3">
                                    <span class="titleb">Phone:</span>
                                    <span ng-bind="counter.colphone" class="thin-font3"> </span> <br/>  
                                    <span class="titleb">Email:</span> <span ng-bind="counter.colemail" class="thin-font3"> </span> <br/>
                                    <span class="titleb">Hours:</span> <span ng-bind="counter.colhours" class="thin-font3"> </span>  <br/> <br/>
                                  </div>
                                  <div class="title3">
                                    <span class="titleb text-top">Address:</span>
                                    <br/>
                                    <div ng-repeat="x in counter.str">
                                      <span class="thin-font3" ng-bind="x"> </span> <br/>
                                    </div>
                                  </div>

                                  <div class="wsite-map">
                                    <iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 200px; margin-top: 10px; margin-bottom: 10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3273.673285579057!2d-111.7637428!3d34.8644358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872da1564d73fd15%3A0x3e31d123930491bf!2s201+Hwy+179%2C+Sedona%2C+AZ+86336%2C+USA!5e0!3m2!1sen!2sph!4v1434348998372"></iframe>
                                  </div>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              </div>
                            </div>
                          </div>

                      <!-- NEWS MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'NEWS'" ng-model="counter.colnewslimit" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'NEWS'" ng-click="editnewsmodule(count.colcount,indexX,count.col1viewed,count.col1popular,counter.colnewslimit,$index)"title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'NEWS'" ng-click="counter.col = '';counter.colnewslimit='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                      <div ng-if="counter.col == 'NEWS'" class="col-sm-12 margin-bot20" style="max-height:350px;overflow-y:auto">
                        <h4 class="blog-title-list">Latest News </h4>
                        <div class="list-news-wrapper">
                          <div class="row list-title-blog" style="padding-top:5px;" ng-repeat="news in news | limitTo:counter.colnewslimit">
                            <div style="padding-right:0px" class="{[{count.column==1? 'col-sm-4' : ''}]} {[{count.column==2 ? 'col-sm-6' : 'col-sm-12'}]} news-thumb-container" title="{[{ news.title }]}">
                              <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}" style="height:130px;width:100%">
                              <div class="youtube-play" ng-show="news.videothumb"><img src="img/youtubeplay.png" ng-click="redirectNews(news.newsslugs);"/></div>
                              
                            </div>
                            <div class="{[{count.column==1 ? 'col-md-8' : ''}]} {[{count.column==2 ? 'col-sm-6' : 'col-md-12'}]} news-list-desc">
                              <div class="row">
                                <div class="col-sm-12">
                                  <span class="size25 font1 news-title">{[{ news.title }]}</span>
                                </div>
                                <div class="col-sm-12">
                                  <span class="thin-font1 orange">{[{ news.categorylist }]}</span><span ng-show="news.name !=''" class="thin-font1"> / by <span class="orange">{[{ news.name }]}</span></span> / {[{ news.date }]}
                                  <br/><br/>
                                </div>
                                <div class="col-sm-12">
                                  <div class="font1 size14 summary">
                                    {[{ news.summary }]}
                                    <br/><br/>
                                  </div>
                                </div>
                              </div>

                              <div style="clear:both"></div>
                              <br>
                            </div>
                          </div>
                        </div>
                      </div>

                      <!-- DIVIDER MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colheight" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colcolor" ></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'DIVIDER'" ng-click="editdividermodule(count.colcount,indexX,counter.colheight,counter.colcolor,$index)" title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="counter.col == 'DIVIDER'" ng-click="counter.col = '';counter.colheight='';counter.colcolor='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-warning"><i class="fa fa-trash-o"></i></button>
                       <br ng-if="counter.col == 'DIVIDER'">
                      <div style="padding-right:10px">
                        <hr ng-if="counter.col == 'DIVIDER'" style="height:{[{counter.colheight}]}px;background:{[{counter.colcolor}]}">
                      </div>
                      <!-- BUTTON MODULE -->
                      <span ng-if="counter.col == 'BUTTON'"> 
                      <textarea style="display:none;" ng-model="counter.colbtnname" required></textarea>
                      <textarea style="display:none;" ng-model="counter.colbtnlink" required></textarea>
                      <textarea style="display:none;" ng-model="counter.colbgcolor" required></textarea>
                      <textarea style="display:none;" ng-model="counter.colfcolor" required></textarea>
                      <textarea style="display:none;" ng-model="counter.colpadtop" required></textarea>
                      <textarea style="display:none;" ng-model="counter.colpadside" required></textarea>
                      <textarea style="display:none;" ng-model="counter.colfont" required></textarea>
                      <textarea style="display:none;" ng-model="counter.colposition" required></textarea>
                      </span>
                      <button type="button" ng-if="counter.col == 'BUTTON'" ng-click="editbuttonmodule(1,indexX,counter,$index)" title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-primary"><i class="fa fa-edit"></i></button>

                      <button type="button" ng-if="counter.col == 'BUTTON'" ng-click="counter.col = '';counter.colbtnname='';counter.colbtnlink='';counter.colbgcolor='';counter.colfcolor='';counter.colpadtop='';counter.colpadside='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-warning"><i class="fa fa-trash-o"></i></button>
                    <br>
                    <div style="text-align:{[{counter.colposition}]}">
                    <a ng-if="counter.col == 'BUTTON'" href='javascript:void(0);' class="module-link" style="background-color:{[{counter.colbgcolor}]};color:{[{counter.colfcolor}]};padding:{[{counter.colpadtop}]}px {[{counter.colpadside}]}%;font-size:{[{counter.colfont}]}px">
                     {[{counter.colbtnname}]}
                    </a>
                    </div>

                    </div>

                  <div class="col-sm-12 col-container" ng-if="$index == 1">
                   <a class="{[{count.column == 2 ? 'col-sm-10' : ''}]} {[{count.column == 3 ? 'col-sm-8' : ''}]} {[{count.column == 4 ? 'mainbut2' : ''}]} btn btn-sm btn-default"> {[{counter.col}]} </a>

                       <!-- TEXT MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'TEXT'" ng-model="counter.colcontent" style="color:black" required></textarea>
                    <button type="button" type="button" ng-if="counter.col == 'TEXT'" ng-click="edittextmodule(count.colcount,indexX,counter.colcontent,$index)" title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="counter.col == 'TEXT'" ng-click="counter.col = '';counter.colcontent='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                     <div ng-if="counter.col == 'TEXT'" class="col-sm-12 txtmodule">
                     <p ng-bind-html="trustAsHtml(counter.colcontent)"></p>
                   </div>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimg" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimglink" style="color:black"></textarea>
                    <button type="button" type="button" ng-if="counter.col == 'IMAGE'" ng-click="editimagemodule(count.colcount,indexX,counter.colimg,$index,counter.colimglink)" title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="counter.col == 'IMAGE'" ng-click="counter.col = '';counter.colimg='';counter.collink='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-warning"><i class="fa fa-trash-o"></i></button>
                    <img ng-if="counter.col == 'IMAGE'" class="imgmodule2" src="{[{s3link}]}/uploads/{[{ directory }]}/{[{counter.colimg}]}">
                    <span ng-if="counter.colimglink"> {[{counter.colimglink}]} </span>

                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonial" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonialcat" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="edittestimonialmodule(count.colcount,indexX,counter.coltestimonial,counter.coltestimonialcat,$index)"title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="counter.col = '';counter.coltestimonial='';counter.coltestimonialcat='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-warning"><i class="fa fa-trash-o"></i></button>
                         <div ng-if="counter.col == 'TESTIMONIAL'" class="col-sm-12 margin-bot20" style="max-height:300px;overflow-y:auto" id="testi1div">
                       <div class="size18" style="padding-top:5px;"><h3 class="font1 italic">What People Are Saying</h3></div>
                       <div ng-repeat="x in testi | filter:{pageid:counter.coltestimonialcat} | limitTo:counter.coltestimonial">
                        <div class="specialpage_col">  
                          <div class="size16 border-left italic margin-top special-testi-wrapper">
                            <div class="row">
                              <div class="col-sm-12 text-center page-testi-image">
                                <img src="{[{s3link}]}/uploads/testimonialpic/{[{x.picture}]}" class="testi-img">
                              </div>
                              <div class="col-sm-12">
                                <div></div>
                                <div>
                                  <span class="fa fa-quote-left"></span>
                                  {[{x.message}]}
                                </div>
                                <div class="text-right">-  {[{x.name}]} </div>
                              </div>
                            </div>
                            <hr class="styled-hr">
                          </div>
                        </div>
                      </div>
                    </div>

                       <!-- CONTACT FORM MODULE -->
                      <button type="button" ng-if="counter.col == 'CONTACT'" ng-click="editcontactmodule(count.colcount,indexX,counter,$index)" title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="counter.col == 'CONTACT'" ng-click="counter.col = '';counter.colcontact='';counter.colcontactemail='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-warning"><i class="fa fa-trash-o"></i></button>
                      <div ng-if="counter.col == 'CONTACT'" style="padding-right:10px">
                        <div class="row">
                         <div class="col-sm-12" style="height: 300px;
                          overflow-y: auto;">
                          <div class="col-sm-12">
                            <h3>Contact Us</h3>
                          </div>
                          <div class="col-sm-6">
                            <div class="form">
                                <!-- Name -->
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="name">Subject
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                        <input type="text" class="form-control textbox margin-bot8" readonly="" style="background-color:#fff">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="name">Name
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                      <input type="text" class="form-control textbox margin-bot8" placeholder="First Name" readonly="" style="background-color:#fff">
                                      </div>
                                      <div class="col-sm-12">
                                        <input type="text" class="form-control textbox" placeholder="Last Name" readonly="" style="background-color:#fff">
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Email -->
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="email">Email
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                        <span class="red hidden booking-email">Invalid Email Address</span>
                                        <input type="email" class="textbox form-control margin-bot8" readonly="" style="background-color:#fff">
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Comment -->
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                        <textarea class="form-control input-large textarea" readonly="" style="background-color:#fff"></textarea>
                                      </div>
                                    </div>
                                  </div>
                            
                                  <div class="form-actions margin-bot8">
                                    <div class="row">
                                      <div class="col-sm-12">
                                        <button type="submit" class="btn btn-sedona" disabled="" style="background-color:#fff">Submit</button>
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Buttons -->
                                </div>
                                <div class="clearfix"></div>
                              </div>

                             <div class="col-sm-6 storeinfo" style="word-wrap: break-word;padding-left: 0px;">
                                <div class="storeinfo-details">
                                  <h3 class="orange"> <span ng-bind="counter.coltitle"> </span> </h3>
                                  <div class="title3">
                                    <span class="titleb">Phone:</span>
                                    <span ng-bind="counter.colphone" class="thin-font3"> </span> <br/>  
                                    <span class="titleb">Email:</span> <span ng-bind="counter.colemail" class="thin-font3"> </span> <br/>
                                    <span class="titleb">Hours:</span> <span ng-bind="counter.colhours" class="thin-font3"> </span>  <br/> <br/>
                                  </div>
                                  <div class="title3">
                                    <span class="titleb text-top">Address:</span>
                                    <br/>
                                    <div ng-repeat="x in counter.str">
                                      <span class="thin-font3" ng-bind="x"> </span> <br/>
                                    </div>
                                  </div>

                                  <div class="wsite-map">
                                    <iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 200px; margin-top: 10px; margin-bottom: 10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3273.673285579057!2d-111.7637428!3d34.8644358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872da1564d73fd15%3A0x3e31d123930491bf!2s201+Hwy+179%2C+Sedona%2C+AZ+86336%2C+USA!5e0!3m2!1sen!2sph!4v1434348998372"></iframe>
                                  </div>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              </div>
                            </div>
                          </div>

                      <!-- NEWS MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'NEWS'" ng-model="counter.colnewslimit" ></textarea>
                      <button type="button" ng-if="counter.col == 'NEWS'" ng-click="editnewsmodule(count.colcount,indexX,count.col1viewed,count.col1popular,counter.colnewslimit,$index)"title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'NEWS'" ng-click="counter.col = '';counter.colnewslimit='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-warning"><i class="fa fa-trash-o"></i></button>
                       <div ng-if="counter.col == 'NEWS'" class="col-sm-12 margin-bot20" style="max-height:350px;overflow-y:auto">
                        <h4 class="blog-title-list">Latest News </h4>
                        <div class="list-news-wrapper">
                          <div class="row list-title-blog" style="padding-top:5px;" ng-repeat="news in news | limitTo:counter.colnewslimit">
                            <div style="padding-right:0px" class="{[{count.column==2 ? 'col-sm-6' : 'col-sm-12'}]} news-thumb-container" title="{[{ news.title }]}">
                              <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}" style="height:130px;width:100%">
                              <div class="youtube-play" ng-show="news.videothumb"><img src="img/youtubeplay.png" ng-click="redirectNews(news.newsslugs);"/></div>
                              
                            </div>
                            <div class="{[{count.column==2 ? 'col-sm-6' : 'col-md-12'}]} news-list-desc">
                              <div class="row">
                                <div class="col-sm-12">
                                  <span class="size25 font1 news-title">{[{ news.title }]}</span>
                                </div>
                                <div class="col-sm-12">
                                  <span class="thin-font1 orange">{[{ news.categorylist }]}</span><span ng-show="news.name !=''" class="thin-font1"> / by <span class="orange">{[{ news.name }]}</span></span> / {[{ news.date }]}
                                  <br/><br/>
                                </div>
                                <div class="col-sm-12">
                                  <div class="font1 size14 summary">
                                    {[{ news.summary }]}
                                    <br/><br/>
                                  </div>
                                </div>
                              </div>

                              <div style="clear:both"></div>
                              <br>
                            </div>
                          </div>
                        </div>
                      </div>

                      <!-- DIVIDER MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colheight" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colcolor" required></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'DIVIDER'" ng-click="editdividermodule(count.colcount,indexX,counter.colheight,counter.colcolor,$index)" title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="counter.col == 'DIVIDER'" ng-click="counter.col = '';counter.colheight='';counter.colcolor='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-warning"><i class="fa fa-trash-o"></i></button>
                       <br ng-if="counter.col == 'DIVIDER'">
                      <div style="padding-right:10px">
                        <hr ng-if="counter.col == 'DIVIDER'" style="height:{[{counter.colheight}]}px;background:{[{counter.colcolor}]}">
                      </div>

                      <!-- BUTTON MODULE -->
                      <span ng-if="counter.col == 'BUTTON'"> 
                      <textarea style="display:none;"  ng-model="counter.colbtnname" required></textarea>
                      <textarea style="display:none;" ng-model="counter.colbtnlink" required></textarea>
                      <textarea style="display:none;" ng-model="counter.colbgcolor" required></textarea>
                      <textarea style="display:none;" ng-model="counter.colfcolor" required></textarea>
                      <textarea style="display:none;" ng-model="counter.colpadtop" required></textarea>
                      <textarea style="display:none;" ng-model="counter.colpadside" required></textarea>
                      </span>
                      <button type="button" type="button" ng-if="counter.col == 'BUTTON'" ng-click="editbuttonmodule(1,indexX,counter,$index)" title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="counter.col == 'BUTTON'" ng-click="counter.col = '';counter.colbtnname='';counter.colbtnlink='';counter.colbgcolor='';counter.colfcolor='';counter.colpadtop='';counter.colpadside='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-warning"><i class="fa fa-trash-o"></i></button>
                    <br>
                     <div style="text-align:{[{counter.colposition}]}">
                    <a ng-if="counter.col == 'BUTTON'" href='javascript:void(0);' class="module-link" style="background-color:{[{counter.colbgcolor}]};color:{[{counter.colfcolor}]};padding:{[{counter.colpadtop}]}px {[{counter.colpadside}]}%;font-size:{[{counter.colfont}]}px">
                     {[{counter.colbtnname}]}
                    </a>
                    </div>

                  </div>
                  
             

                  <div class="col-sm-12 col-container" ng-if="$index == 2">
                     <a class="{[{count.column == 3 ? 'col-sm-8' : ''}]} {[{count.column == 4 ? 'mainbut2' : ''}]} btn btn-sm btn-default"> {[{counter.col}]} </a>

                        <!-- TEXT MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'TEXT'" ng-model="counter.colcontent" required></textarea>
                    <button type="button" ng-if="counter.col == 'TEXT'" ng-click="edittextmodule(count.colcount,indexX,counter.colcontent,$index)" title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" ng-if="counter.col == 'TEXT'" ng-click="counter.col = '';counter.colcontent='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn btn-sm btn-warning"><i class="fa fa-trash-o"></i></button>
                     <div ng-if="counter.col == 'TEXT'" class="col-sm-12 txtmodule">
                     <p ng-bind-html="trustAsHtml(counter.colcontent)"></p>
                   </div>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimg"  required></textarea>
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimglink"></textarea>
                    <button type="button" ng-if="counter.col == 'IMAGE'" ng-click="editimagemodule(count.colcount,indexX,counter.colimg,$index,counter.colimglink)" title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" ng-if="counter.col == 'IMAGE'" ng-click="counter.col = '';counter.colimg='';counter.collink='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-warning"><i class="fa fa-trash-o"></i></button>
                    <img ng-if="counter.col == 'IMAGE'" class="imgmodule2" src="{[{s3link}]}/uploads/{[{ directory }]}/{[{counter.colimg}]}">
                    <span ng-if="counter.colimglink"> {[{counter.colimglink}]} </span>

                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonial" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonialcat" required></textarea>
                      <button type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="edittestimonialmodule(count.colcount,indexX,counter.coltestimonial,counter.coltestimonialcat,$index)"title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="counter.col = '';counter.coltestimonial='';counter.coltestimonialcat='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-warning"><i class="fa fa-trash-o"></i></button>
                         <div ng-if="counter.col == 'TESTIMONIAL'" class="col-sm-12 margin-bot20" style="max-height:300px;overflow-y:auto" id="testi1div">
                       <div class="size18" style="padding-top:5px;"><h3 class="font1 italic">What People Are Saying</h3></div>
                       <div ng-repeat="x in testi | filter:{pageid:counter.coltestimonialcat} | limitTo:counter.coltestimonial">
                        <div class="specialpage_col">  
                          <div class="size16 border-left italic margin-top special-testi-wrapper">
                            <div class="row">
                              <div class="col-sm-12 text-center page-testi-image">
                                <img src="{[{s3link}]}/uploads/testimonialpic/{[{x.picture}]}" class="testi-img">
                              </div>
                              <div class="col-sm-12">
                                <div></div>
                                <div>
                                  <span class="fa fa-quote-left"></span>
                                  {[{x.message}]}
                                </div>
                                <div class="text-right">-  {[{x.name}]} </div>
                              </div>
                            </div>
                            <hr class="styled-hr">
                          </div>
                        </div>
                      </div>
                    </div>

                       <!-- CONTACT FORM MODULE -->
                      <button type="button" ng-if="counter.col == 'CONTACT'" ng-click="editcontactmodule(count.colcount,indexX,counter,$index)" title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="counter.col == 'CONTACT'" ng-click="counter.col = '';counter.colcontact='';counter.colcontactemail='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-warning"><i class="fa fa-trash-o"></i></button>
                       <div ng-if="counter.col == 'CONTACT'" style="padding-right:10px">
                        <div class="row">
                         <div class="col-sm-12" style="height: 300px;
                          overflow-y: auto;">
                          <div class="col-sm-12">
                            <h3>Contact Us</h3>
                          </div>
                          <div class="col-sm-6">
                            <div class="form">
                                <!-- Name -->
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="name">Subject
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                        <input type="text" class="form-control textbox margin-bot8" readonly="" style="background-color:#fff">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="name">Name
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                      <input type="text" class="form-control textbox margin-bot8" placeholder="First Name" readonly="" style="background-color:#fff">
                                      </div>
                                      <div class="col-sm-12">
                                        <input type="text" class="form-control textbox" placeholder="Last Name" readonly="" style="background-color:#fff">
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Email -->
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="email">Email
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                        <span class="red hidden booking-email">Invalid Email Address</span>
                                        <input type="email" class="textbox form-control margin-bot8" readonly="" style="background-color:#fff">
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Comment -->
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                        <textarea class="form-control input-large textarea" readonly="" style="background-color:#fff"></textarea>
                                      </div>
                                    </div>
                                  </div>
                            
                                  <div class="form-actions margin-bot8">
                                    <div class="row">
                                      <div class="col-sm-12">
                                        <button type="submit" class="btn btn-sedona" disabled="" style="background-color:#fff">Submit</button>
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Buttons -->
                                </div>
                                <div class="clearfix"></div>
                              </div>

                             <div class="col-sm-6 storeinfo" style="word-wrap: break-word;padding-left: 0px;">
                                <div class="storeinfo-details">
                                  <h3 class="orange"> <span ng-bind="counter.coltitle"> </span> </h3>
                                  <div class="title3">
                                    <span class="titleb">Phone:</span>
                                    <span ng-bind="counter.colphone" class="thin-font3"> </span> <br/>  
                                    <span class="titleb">Email:</span> <span ng-bind="counter.colemail" class="thin-font3"> </span> <br/>
                                    <span class="titleb">Hours:</span> <span ng-bind="counter.colhours" class="thin-font3"> </span>  <br/> <br/>
                                  </div>
                                  <div class="title3">
                                    <span class="titleb text-top">Address:</span>
                                    <br/>
                                    <div ng-repeat="x in counter.str">
                                      <span class="thin-font3" ng-bind="x"> </span> <br/>
                                    </div>
                                  </div>

                                  <div class="wsite-map">
                                    <iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 200px; margin-top: 10px; margin-bottom: 10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3273.673285579057!2d-111.7637428!3d34.8644358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872da1564d73fd15%3A0x3e31d123930491bf!2s201+Hwy+179%2C+Sedona%2C+AZ+86336%2C+USA!5e0!3m2!1sen!2sph!4v1434348998372"></iframe>
                                  </div>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              </div>
                            </div>
                          </div>

                      <!-- NEWS MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'NEWS'" ng-model="counter.colnewslimit" style="color:black"></textarea>
                      <button type="button" ng-if="counter.col == 'NEWS'" ng-click="editnewsmodule(count.colcount,indexX,count.col1viewed,count.col1popular,counter.colnewslimit,$index)"title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="counter.col == 'NEWS'" ng-click="counter.col = '';counter.colnewslimit='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-warning"><i class="fa fa-trash-o"></i></button>
                       <div ng-if="counter.col == 'NEWS'" class="col-sm-12 margin-bot20" style="max-height:350px;overflow-y:auto">
                        <h4 class="blog-title-list">Latest News </h4>
                        <div class="list-news-wrapper">
                          <div class="row list-title-blog" style="padding-top:5px;" ng-repeat="news in news | limitTo:counter.colnewslimit">
                            <div style="padding-right:0px" class="col-sm-12 news-thumb-container" title="{[{ news.title }]}">
                              <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}" style="height:130px;width:100%">
                              <div class="youtube-play" ng-show="news.videothumb"><img src="img/youtubeplay.png" ng-click="redirectNews(news.newsslugs);"/></div>
                              
                            </div>
                            <div class="col-md-12 news-list-desc">
                              <div class="row">
                                <div class="col-sm-12">
                                  <span class="size25 font1 news-title">{[{ news.title }]}</span>
                                </div>
                                <div class="col-sm-12">
                                  <span class="thin-font1 orange">{[{ news.categorylist }]}</span><span ng-show="news.name !=''" class="thin-font1"> / by <span class="orange">{[{ news.name }]}</span></span> / {[{ news.date }]}
                                  <br/><br/>
                                </div>
                                <div class="col-sm-12">
                                  <div class="font1 size14 summary">
                                    {[{ news.summary }]}
                                    <br/><br/>
                                  </div>
                                </div>
                              </div>

                              <div style="clear:both"></div>
                              <br>
                            </div>
                          </div>
                        </div>
                      </div>


                      <!-- DIVIDER MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colheight" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colcolor" required></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'DIVIDER'" ng-click="editdividermodule(count.colcount,indexX,counter.colheight,counter.colcolor,$index)" title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="counter.col == 'DIVIDER'" ng-click="counter.col = '';counter.colheight='';counter.colcolor='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-warning"><i class="fa fa-trash-o"></i></button>
                       <br ng-if="counter.col == 'DIVIDER'">
                      <div style="padding-right:10px">
                        <hr ng-if="counter.col == 'DIVIDER'" style="height:{[{counter.colheight}]}px;background:{[{counter.colcolor}]}">
                      </div>

                      <!-- BUTTON MODULE -->
                      <span ng-if="counter.col == 'BUTTON'"> 
                      <textarea style="display:none;"  ng-model="counter.colbtnname" required></textarea>
                      <textarea style="display:none;" ng-model="counter.colbtnlink" required></textarea>
                      <textarea style="display:none;" ng-model="counter.colbgcolor" required></textarea>
                      <textarea style="display:none;" ng-model="counter.colfcolor" required></textarea>
                      <textarea style="display:none;" ng-model="counter.colpadtop" required></textarea>
                      <textarea style="display:none;" ng-model="counter.colpadside" required></textarea>
                      </span>
                      <button type="button" ng-if="counter.col == 'BUTTON'" ng-click="editbuttonmodule(1,indexX,counter,$index)" title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="counter.col == 'BUTTON'" ng-click="counter.col = '';counter.colbtnname='';counter.colbtnlink='';counter.colbgcolor='';counter.colfcolor='';counter.colpadtop='';counter.colpadside='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-warning"><i class="fa fa-trash-o"></i></button>
                    <br>
                     <div style="text-align:{[{counter.colposition}]}">
                    <a ng-if="counter.col == 'BUTTON'" href='javascript:void(0);' class="module-link" style="background-color:{[{counter.colbgcolor}]};color:{[{counter.colfcolor}]};padding:{[{counter.colpadtop}]}px {[{counter.colpadside}]}%;font-size:{[{counter.colfont}]}px">
                     {[{counter.colbtnname}]}
                    </a>
                    </div>

                  </div>
            

                  <div class="col-sm-12 col-container" ng-if="$index == 3">
                  <a type="button" type="button" class="mainbut2 btn-sm btn btn-default">{[{counter.col}]}</a>

                       <!-- TEXT MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'TEXT'" ng-model="counter.colcontent" style="color:black" required></textarea>
                    <button type="button" ng-if="counter.col == 'TEXT'" ng-click="edittextmodule(count.colcount,indexX,counter.colcontent,$index)" title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" ng-if="counter.col == 'TEXT'" ng-click="counter.col = '';counter.colcontent='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-warning"><i class="fa fa-trash-o"></i></button>
                     <div ng-if="counter.col == 'TEXT'" class="col-sm-12" style="max-height:350px;">
                     <p ng-bind-html="trustAsHtml(counter.colcontent)"></p>
                     {[{counter.colcontent}]}
                   </div>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimg"  required></textarea>
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimglink"></textarea>
                    <button type="button" ng-if="counter.col == 'IMAGE'" ng-click="editimagemodule(count.colcount,indexX,counter.colimg,$index,counter.colimglink)" title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="counter.col == 'IMAGE'" ng-click="counter.col = '';counter.colimg='';counter.collink='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-warning"><i class="fa fa-trash-o"></i></button>
                    <img ng-if="counter.col == 'IMAGE'" class="imgmodule2" src="{[{s3link}]}/uploads/{[{ directory }]}/{[{counter.colimg}]}">
                    <span ng-if="counter.colimglink"> {[{counter.colimglink}]} </span>

                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonial" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonialcat"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="edittestimonialmodule(count.colcount,indexX,counter.coltestimonial,counter.coltestimonialcat,$index)"title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="counter.col = '';counter.coltestimonial='';counter.coltestimonialcat='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-warning"><i class="fa fa-trash-o"></i></button>
                         <div ng-if="counter.col == 'TESTIMONIAL'" class="col-sm-12 margin-bot20" style="max-height:300px;overflow-y:auto" id="testi1div">
                       <div class="size18" style="padding-top:5px;"><h3 class="font1 italic">What People Are Saying</h3></div>
                       <div ng-repeat="x in testi | filter:{pageid:counter.coltestimonialcat} | limitTo:counter.coltestimonial">
                        <div class="specialpage_col">  
                          <div class="size16 border-left italic margin-top special-testi-wrapper">
                            <div class="row">
                              <div class="col-sm-12 text-center page-testi-image">
                                <img src="{[{s3link}]}/uploads/testimonialpic/{[{x.picture}]}" class="testi-img">
                              </div>
                              <div class="col-sm-12">
                                <div></div>
                                <div>
                                  <span class="fa fa-quote-left"></span>
                                  {[{x.message}]}
                                </div>
                                <div class="text-right">-  {[{x.name}]} </div>
                              </div>
                            </div>
                            <hr class="styled-hr">
                          </div>
                        </div>
                      </div>
                    </div>

                       <!-- CONTACT FORM MODULE -->
                      <button type="button" ng-if="counter.col == 'CONTACT'" ng-click="editcontactmodule(count.colcount,indexX,counter,$index)" title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn  btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="counter.col == 'CONTACT'" ng-click="counter.col = '';counter.colcontact='';counter.colcontactemail='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-warning"><i class="fa fa-trash-o"></i></button>
                       <div ng-if="counter.col == 'CONTACT'" style="padding-right:10px">
                        <div class="row">
                         <div class="col-sm-12" style="height: 300px;
                          overflow-y: auto;">
                          <div class="col-sm-12">
                            <h3>Contact Us</h3>
                          </div>
                          <div class="col-sm-6">
                            <div class="form">
                                <!-- Name -->
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="name">Subject
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                        <input type="text" class="form-control textbox margin-bot8" readonly="" style="background-color:#fff">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="name">Name
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                      <input type="text" class="form-control textbox margin-bot8" placeholder="First Name" readonly="" style="background-color:#fff">
                                      </div>
                                      <div class="col-sm-12">
                                        <input type="text" class="form-control textbox" placeholder="Last Name" readonly="" style="background-color:#fff">
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Email -->
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="email">Email
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                        <span class="red hidden booking-email">Invalid Email Address</span>
                                        <input type="email" class="textbox form-control margin-bot8" readonly="" style="background-color:#fff">
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Comment -->
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                        <span class="red">*</span>
                                      </label>
                                      <div class="col-sm-12">
                                        <textarea class="form-control input-large textarea" readonly="" style="background-color:#fff"></textarea>
                                      </div>
                                    </div>
                                  </div>
                            
                                  <div class="form-actions margin-bot8">
                                    <div class="row">
                                      <div class="col-sm-12">
                                        <button type="submit" class="btn btn-sedona" disabled="" style="background-color:#fff">Submit</button>
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Buttons -->
                                </div>
                                <div class="clearfix"></div>
                              </div>

                             <div class="col-sm-6 storeinfo" style="word-wrap: break-word;padding-left: 0px;">
                                <div class="storeinfo-details">
                                  <h3 class="orange"> <span ng-bind="counter.coltitle"> </span> </h3>
                                  <div class="title3">
                                    <span class="titleb">Phone:</span>
                                    <span ng-bind="counter.colphone" class="thin-font3"> </span> <br/>  
                                    <span class="titleb">Email:</span> <span ng-bind="counter.colemail" class="thin-font3"> </span> <br/>
                                    <span class="titleb">Hours:</span> <span ng-bind="counter.colhours" class="thin-font3"> </span>  <br/> <br/>
                                  </div>
                                  <div class="title3">
                                    <span class="titleb text-top">Address:</span>
                                    <br/>
                                    <div ng-repeat="x in counter.str">
                                      <span class="thin-font3" ng-bind="x"> </span> <br/>
                                    </div>
                                  </div>

                                  <div class="wsite-map">
                                    <iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 200px; margin-top: 10px; margin-bottom: 10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3273.673285579057!2d-111.7637428!3d34.8644358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872da1564d73fd15%3A0x3e31d123930491bf!2s201+Hwy+179%2C+Sedona%2C+AZ+86336%2C+USA!5e0!3m2!1sen!2sph!4v1434348998372"></iframe>
                                  </div>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              </div>
                            </div>
                          </div>

                      <!-- NEWS MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'NEWS'" ng-model="counter.colnewslimit" style="color:black"></textarea>
                      <button type="button" ng-if="counter.col == 'NEWS'" ng-click="editnewsmodule(count.colcount,indexX,count.col1viewed,count.col1popular,counter.colnewslimit,$index)"title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="counter.col == 'NEWS'" ng-click="counter.col = '';counter.colnewslimit='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-warning"><i class="fa fa-trash-o"></i></button>
                       <div ng-if="counter.col == 'NEWS'" class="col-sm-12 margin-bot20" style="max-height:350px;overflow-y:auto">
                        <h4 class="blog-title-list">Latest News </h4>
                        <div class="list-news-wrapper">
                          <div class="row list-title-blog" style="padding-top:5px;" ng-repeat="news in news | limitTo:counter.colnewslimit">
                            <div style="padding-right:0px" class="col-sm-12 news-thumb-container" title="{[{ news.title }]}">
                              <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}" style="height:130px;width:100%">
                              <div class="youtube-play" ng-show="news.videothumb"><img src="img/youtubeplay.png" ng-click="redirectNews(news.newsslugs);"/></div>
                              
                            </div>
                            <div class="col-md-12 news-list-desc">
                              <div class="row">
                                <div class="col-sm-12">
                                  <span class="size25 font1 news-title">{[{ news.title }]}</span>
                                </div>
                                <div class="col-sm-12">
                                  <span class="thin-font1 orange">{[{ news.categorylist }]}</span><span ng-show="news.name !=''" class="thin-font1"> / by <span class="orange">{[{ news.name }]}</span></span> / {[{ news.date }]}
                                  <br/><br/>
                                </div>
                                <div class="col-sm-12">
                                  <div class="font1 size14 summary">
                                    {[{ news.summary }]}
                                    <br/><br/>
                                  </div>
                                </div>
                              </div>

                              <div style="clear:both"></div>
                              <br>
                            </div>
                          </div>
                        </div>
                      </div>


                      <!-- DIVIDER MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colheight" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colcolor"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'DIVIDER'" ng-click="editdividermodule(count.colcount,indexX,counter.colheight,counter.colcolor,$index)" title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="counter.col == 'DIVIDER'" ng-click="counter.col = '';counter.colheight='';counter.colcolor='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-warning"><i class="fa fa-trash-o"></i></button>
                       <br ng-if="counter.col == 'DIVIDER'">
                      <div style="padding-right:10px">
                        <hr ng-if="counter.col == 'DIVIDER'" style="height:{[{counter.colheight}]}px;background:{[{counter.colcolor}]}">
                      </div>

                      <!-- BUTTON MODULE -->
                      <span ng-if="counter.col == 'BUTTON'"> 
                      <textarea style="display:none;"  ng-model="counter.colbtnname" required></textarea>
                      <textarea style="display:none;" ng-model="counter.colbtnlink" required></textarea>
                      <textarea style="display:none;" ng-model="counter.colbgcolor" required></textarea>
                      <textarea style="display:none;" ng-model="counter.colfcolor" required></textarea>
                      <textarea style="display:none;" ng-model="counter.colpadtop" required></textarea>
                      <textarea style="display:none;" ng-model="counter.colpadside" required></textarea>
                      </span>
                      <button type="button" ng-if="counter.col == 'BUTTON'" ng-click="editbuttonmodule(1,indexX,counter,$index)" title="Edit" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button ng-if="counter.col == 'BUTTON'" ng-click="counter.col = '';counter.colbtnname='';counter.colbtnlink='';counter.colbgcolor='';counter.colfcolor='';counter.colpadtop='';counter.colpadside='';delcolumn(count.colcount,indexX,$index)" title="Delete" class="{[{count.colcount == 1 || count.colcount == 2 ? 'col-sm-1' : 'col-sm-2'}]} btn-sm btn btn-warning"><i class="fa fa-trash-o"></i></button>
                    <br>
                     <div style="text-align:{[{counter.colposition}]}">
                    <a ng-if="counter.col == 'BUTTON'" href='javascript:void(0);' class="module-link" style="background-color:{[{counter.colbgcolor}]};color:{[{counter.colfcolor}]};padding:{[{counter.colpadtop}]}px {[{counter.colpadside}]}%;font-size:{[{counter.colfont}]}px">
                     {[{counter.colbtnname}]}
                    </a>
                    </div>

                  </div>
                  </li>
                  </ul>
                </div>
                </div>
                <br>
              
             </div>
             </li>
             </ul>
  
              </div> <!-- //special -->
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Meta Information <span style="color:red"> * </span>
            </div>
            <div class="panel-body">
              Title
              <input type="text" id="metatitle" name="metatitle" class="form-control" ng-model="page.metatitle" required="required" placeholder="Meta Title">

              <div class="line line-dashed b-b line-lg"></div>
              Description
              <input input="text"  id="metadesc" name="metadesc"  class="form-control" ng-model="page.metadesc" required="required" placeholder="Meta Description">

              <div class="line line-dashed b-b line-lg"></div>
              Keyword
              <input type="text" id="metatags" name="metatags" class="form-control" ng-model="page.metakeyword" required="required" placeholder="Meta Keyword">
              <div class="line line-dashed b-b line-lg"></div>
              Status <br>
              <div ng-if="page.status==1" ng-init="page.status = true">  </div>
              <label class="i-switch bg-info m-t-xs m-r">
                   <input type="checkbox" checked ng-model="page.status">
                    <i></i>
              </label>
              <span ng-if="page.status == false || page.status==0" class="label bg-danger Services-checkbox-label">Deactivate</span>
              <span ng-if="page.status == true || page.status==1" class="label bg-success Services-checkbox-label">Active</span>
            </div>
          </div>
          </div>
          <div class="col-sm-4">
           <div class="panel panel-default">
                <div class="panel-body" style="padding:5px">
                <label class="btn btn-primary pull-right" ng-click="preview(page)"><i class="fa fa-check text-active"></i> Preview </label>
                </div>
           </div>
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Sidebar
            </div>
            <div class="panel-body">
              <div class="form-group">
                <div class="col-sm-4">
                  <div class="checkbox">
                    <label class="i-checks">
                       <input ng-model="page.sidebarleft" value="left" type="checkbox" ng-change="sidebarleft = !sidebarleft" ng-click="removeleft()" checked=""><i></i>Left
                    </label>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="checkbox">
                    <label class="i-checks">
                      <input ng-model="page.sidebarright" value="right" type="checkbox" ng-change="sidebarright = !sidebarright" checked=""><i></i>Right
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel panel-default" ng-if="sidebarleft">
            <div class="panel-heading font-bold">
              Left Sidebar  Widget

              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtn" ng-click="leftCancel()"><i class="glyphicon glyphicon-ban-circle"></i> Cancel</button>


              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtn" ng-if="sidebar.sidebar == 'Menu'" ng-disabled="!sidebar.menuID" ng-click="updateleftsidebar(currentEdit,sidebar)"><i class="glyphicon glyphicon-edit"></i>Update</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtn" ng-if="sidebar.sidebar == 'Calendar' || sidebar.sidebar == 'Archive' || sidebar.sidebar == 'Rss'" ng-click="updateleftsidebar(currentEdit,sidebar)"><i class="glyphicon glyphicon-edit"></i>Update</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtn" ng-if="sidebar.sidebar == 'Image'" ng-disabled="!sidebar.img" ng-click="updateleftsidebar(currentEdit,sidebar)"><i class="glyphicon glyphicon-edit"></i>Update</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtn" ng-if="sidebar.sidebar == 'Testimonial'" ng-disabled="!sidebar.limittest" ng-click="updateleftsidebar(currentEdit,sidebar)"><i class="glyphicon glyphicon-edit"></i>Update</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtn" ng-if="sidebar.sidebar == 'Link'" ng-disabled="!sidebar.label || !sidebar.url" ng-click="updateleftsidebar(currentEdit,sidebar)"><i class="glyphicon glyphicon-edit"></i>Update</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtn" ng-if="sidebar.sidebar == 'News'" ng-disabled="!sidebar.limitnews" ng-click="updateleftsidebar(currentEdit,sidebar)"><i class="glyphicon glyphicon-edit"></i>Update</button>


            <!--   <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtn" ng-disabled="{[{sidebar.sidebar == 'Menu' ? '!sidebar.menuID' : ''}]}" ng-click="addleftsidebar(sidebar)"><i class="glyphicon  glyphicon-plus-sign "></i>Add</button>
 -->
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtn" ng-if="sidebar.sidebar == 'Menu'" ng-disabled="!sidebar.menuID" ng-click="addleftsidebar(sidebar)"><i class="glyphicon  glyphicon-plus-sign "></i>Add</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtn" ng-if="sidebar.sidebar == 'Calendar' || sidebar.sidebar == 'Archive' || sidebar.sidebar == 'Rss'" ng-click="addleftsidebar(sidebar)"><i class="glyphicon  glyphicon-plus-sign "></i>Add</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtn" ng-if="sidebar.sidebar == 'Image'" ng-disabled="!sidebar.img" ng-click="addleftsidebar(sidebar)"></i>Add</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtn" ng-if="sidebar.sidebar == 'Testimonial'" ng-disabled="!sidebar.limittest" ng-click="addleftsidebar(sidebar)"><i class="glyphicon  glyphicon-plus-sign "></i>Add</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtn" ng-if="sidebar.sidebar == 'Link'" ng-disabled="!sidebar.label || !sidebar.url" ng-click="addleftsidebar(sidebar)"><i class="glyphicon  glyphicon-plus-sign "></i>Add</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtn" ng-if="sidebar.sidebar == 'News'" ng-disabled="!sidebar.limitnews" ng-click="addleftsidebar(sidebar)"><i class="glyphicon  glyphicon-plus-sign "></i>Add</button>
            </div>
              <div class="panel-body">
                <div class="col-sm-12">
                  <div class="">
                    <select class="form-control dd3-content col-sm-12" name="leftsidebar" ng-model="sidebar.sidebar" style="margin-top:10px;" id="left">
                      <option value="" >-Select-</option>
                      <option value="Menu">Menu</option>
                      <option value="Calendar">Calendar</option>
                      <option value="Image">Image</option>
                      <option value="Testimonial">Testimonial</option>
                      <option value="Link">Link</option>
                      <option value="News">News</option>
                      <option value="Archive">Archive</option>
                      <option value="Rss">RSS</option>
                    </select>
                  </div>
                  <div style="padding-top:0px !important;" class="bg-light" ng-if="sidebar.sidebar =='Image'">
                    <div class="line line-dashed b-b line-lg"></div>
                    &nbsp;&nbsp;<i class="icon-arrow-right"></i> Image
                    <a class="btn btn-default btn-xs pull-right" ng-click="media('featured','banner','left')"><i class="fa  fa-folder-open"></i> Media Library</a>
                    <div class="">
                      <img ng-if="sidebar.img" src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{ sidebar.img }]}" style="width: 100%">
                      <input type="text" class="form-control" ng-model="sidebar.imglink" placeholder="Image-Link(optional)">
                    </div>
                  </div>

                  <div style="padding-top:0px !important;" class="bg-light" ng-if="sidebar.sidebar =='Testimonial'">
                    <div class="line line-dashed b-b line-lg"></div>
                    &nbsp;&nbsp;<i class="icon-arrow-right"></i> Testimonial Limit List View
                    <div class="">
                      <div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input ng-model="sidebar.limittest" ui-jq="TouchSpin" type="text" value="10" class="form-control" data-min="0" data-max="20" data-verticalbuttons="true" data-verticalupclass="fa fa-caret-up" data-verticaldownclass="fa fa-caret-down" style="display: block; " class="form-control ng-pristine ng-invalid ng-invalid-required" required><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span></div>
                    </div>
                  </div>
                   <div style="padding-top:0px !important;" class="wrapper bg-light" ng-if="sidebar.sidebar == 'Link'">
                    <div class="line line-dashed b-b line-lg"></div>
                    &nbsp;&nbsp;<i class="icon-arrow-right"></i> Link
                    <div class="">
                      <input type="text" ng-model="sidebar.label" class="form-control ng-pristine ng-invalid ng-invalid-required" placeholder="Label"  required=""><br>
                      <input type="text" ng-model="sidebar.url"  class="form-control" placeholder="http://"  required="">
                    </div>
                  </div>

                   <div style="padding-top:0px !important;" class="wrapper bg-light" ng-if="sidebar.sidebar == 'Menu'">
                    <div class="line line-dashed b-b line-lg"></div>
                    &nbsp;&nbsp;<i class="icon-arrow-right"></i> Menu
                    <div class="">
                     <select class="form-control col-sm-8" required="required" ng-model="sidebar.menuID"  style="margin-top:10px;width:60%" >
                      <option value="" style="display:none">-Select-</option>
                      <option ng-selected="{[{ x.menuID ==  sidebar.menuID }]}" ng-repeat='x in menu' value='{[{x.menuID}]}'> {[{x.name}]} </option>
                    </select>
                    </div><br><br>
                  </div>

                  <div style="padding-top:0px !important;" class="wrapper bg-light" ng-if="sidebar.sidebar =='News'">
                    <div class="line line-dashed b-b line-lg"></div>
                    &nbsp;&nbsp;<i class="icon-arrow-right"></i> News
                    <div class="">
                     <!--  <br>
                      <label class="i-checks">
                        <input type="checkbox" ng-model="page.leftnewspopular[$index]" required="" class="ng-dirty ng-invalid ng-invalid-required"><i></i> Most Papular
                      </label>
                       &nbsp;&nbsp;&nbsp;
                       <label class="i-checks">
                        <input type="checkbox" ng-model="page.leftnewsviews[$index]" required="" class="ng-dirty ng-invalid ng-invalid-required"><i></i> Most Viewed
                      </label> -->
                      <br>
                      <div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input ng-model="sidebar.limitnews" ui-jq="TouchSpin" type="text" value="10" class="form-control" data-min="0" data-max="20" data-verticalbuttons="true" data-verticalupclass="fa fa-caret-up" data-verticaldownclass="fa fa-caret-down" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span></div>
                    </div>
                  </div>
                  <div class="line line-dashed b-b line-lg"></div>
                </div>
                <div class="col-sm-12">
               
                    <ul class="list-group no-radius sortable" ui:sortable ng:model="page.sidedataL" id="list">
                      <li style="border:1px solid #ccc;cursor:pointer;margin-bottom:6px;max-height:330px;overflow-y:auto;" class="list-group-item" ng-repeat="leftdata in page.sidedataL track by $index" ng-hide="currentEdit == $index">
                        <!-- actual -->
                        <span>
                          <span style="font-size:16px"> {[{ leftdata.sidebar }]} </span> 
                           <span class="pull-right" style="font-size:14px"><a href="" ng-click="deleteleft($index)"><i class="fa fa-times-circle"></i></a></span>
                           <span class="pull-right" style="font-size:14px"><a href="" ng-click="editleft($index)"><i class="fa fa-pencil-square"></i></a></span>
                              <!-- calendar -->
                           <div ng-if="leftdata.sidebar=='Calendar'"> 
                            <div class="calendar" ng-model="eventSources" calendar="calendar1" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div>
                           </div>
                              <!-- image -->
                           <div ng-if="leftdata.sidebar=='Image'">
                              <img src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{ leftdata.img }]}" style="width: 100%">
                           <input ng-if="leftdata.imglink" type="text" class="form-control" ng-model="leftdata.imglink" disabled="">
                           </div>
                              <!-- testimonial -->
                           <div ng-if="leftdata.sidebar == 'Testimonial'">
                             <div class="size18"><h4 class="font1 italic">What People Are Saying</h4></div>
                             <div ng-repeat="x in testi | limitTo:leftdata.limittest">
                              <div class="specialpage_col">  
                                <div class="size16 border-left italic margin-top special-testi-wrapper">
                                  <div class="row">
                                    <div class="col-sm-12 text-center page-testi-image">
                                      <img src="{[{s3link}]}/uploads/testimonialpic/{[{x.picture}]}" class="testi-img">
                                    </div>
                                    <div class="col-sm-12">
                                      <div></div>
                                      <div>
                                        <span class="fa fa-quote-left"></span>
                                        {[{x.message}]}
                                      </div>
                                      <div class="text-right">-  {[{x.name}]} </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        <!-- news -->
                          <div ng-if="leftdata.sidebar == 'News'" class="col-sm-12">
                        <h4 class="blog-title-list">Latest News </h4>
                        <div class="list-news-wrapper">
                          <div class="row list-title-blog" ng-repeat="news in news | limitTo:leftdata.limitnews">
                            <div class="col-sm-12 news-thumb-container" title="{[{ news.title }]}">
                              <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}" style="height:150px;width:100%">
                              <div class="youtube-play" ng-show="news.videothumb"><img src="img/youtubeplay.png" ng-click="redirectNews(news.newsslugs);"/></div>
                              
                            </div>
                            <div class="col-md-12 news-list-desc">
                              <div class="row">
                                <div class="col-sm-12">
                                  <span class="size25 font1 news-title">{[{ news.title }]}</span>
                                </div>
                                <div class="col-sm-12">
                                  <span class="thin-font1 orange">{[{ news.categorylist }]}</span><span ng-show="news.name !=''" class="thin-font1"> / by <span class="orange">{[{ news.name }]}</span></span> / {[{ news.date }]}
                                  <br/><br/>
                                </div>
                                <div class="col-sm-12" ng-if="news.summary">
                                  <div class="font1 size14 summary">
                                    {[{ news.summary }]}
                                    <br/><br/>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- link -->
                      <div ng-if="leftdata.sidebar=='Link'" class='lnk'>
                        <a href='javascript:void(0);' class="page-link">
                       {[{leftdata.label}]}
                         </a>
                      </div>
                       <!-- Rss -->
                      <div ng-if="leftdata.sidebar=='Rss'" class='lnk'>
                      <p class="size16"> <a href="javascript:void(0);">
                      <img class="rss" src="/img/frontend/rss_feed.gif"> RSS Feed </a></p>
                      </div>
                      <!-- Archive -->
                      <div ng-if="leftdata.sidebar=='Archive'">
                      <h3 class="font1"><span class="fa fa-calendar fa-1x"></span> Archives</h3>
                      <div class="ul-archives">
                       <ul ng-repeat="archive in archive" class="ul-archive">
                         <li>
                        <a style="color:#ff8421" href="javascript:void(0);"><span class="fa fa-chevron-right"></span> {[{archive.month}]} {[{archive.year}]}</a>
                         </li>
                       </ul>
                    </div>
                    </div>

                    <!-- Menu -->
                   <div class="special-menu" ng-if="leftdata.sidebar=='Menu'">
            <div><a class="special-top-menu">Sedona Healing Arts</a></div>
            <nav>
              <ul style="list-style:disc">
                <li ng-repeat="mobmenu in shortcode" ng-if="mobmenu.menuID == leftdata.menuID ">
                  <a class="special-menu-text" href="javascript:void(0);"><span ng-bind="mobmenu.subname"></span></a>

                  <ul class="no-list" ng-repeat="mob in children">
                    <li ng-repeat="mobsub1 in mob.child1" ng-if="mobsub1.parent==mobmenu.submenuID">
                    <span class="glyphicon glyphicon-menu-right" style="color:#58666e;">
                      <a class="special-menu-text" href="javascript:void(0);"><span ng-bind="mobsub1.subname"></span></a>

                      <ul class="no-list">
                        <li ng-repeat="mobsub2 in mob.child2" ng-if="mobsub2.parent==mobsub1.submenuID">
                         <span class="glyphicon glyphicon-menu-right" style="color:#58666e;">
                          <a class="special-menu-text" href="javascript:void(0);"><span ng-bind="mobsub2.subname"></span></a>

                          <ul class="no-list">
                            <li ng-repeat="mobsub3 in mob.child3" ng-if="mobsub3.parent==mobsub2.submenuID">
                            <span class="glyphicon glyphicon-menu-right" style="color:#58666e;">
                              <a class="special-menu-text" href="javascript:void(0);"><span ng-bind="mobsub3.subname"></span></a>
                            
                              <ul class="no-list">
                              <li ng-repeat="mobsub4 in mob.child4" ng-if="mobsub4.parent==mobsub3.submenuID">
                                  <span class="glyphicon glyphicon-menu-right" style="color:#58666e;">
                                  <a class="special-menu-text" href="javascript:void(0);"><span ng-bind="mobsub4.subname"></span></a>
                                </li>
                              </ul>

                            </li>
                          </ul>

                        </li>
                      </ul>

                    </li>
                  </ul>


                </li>

              </ul>
            </nav>
          </div>
                        </span>
                      </li>
                  </ul>
                  <script type="text/javascript">
                  $('#list').sortable({
                    placeholder: "testclass",
                    forcePlaceholderSize: true
                  });
                </script>
                </div>
              </div>
          </div>
          <div class="panel panel-default" ng-if="sidebarright">
            <div class="panel-heading font-bold">
              Right Sidebar  Widget

              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtnright" ng-click="rightCancel()"><i class="glyphicon glyphicon-ban-circle"></i> Cancel</button>


              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtnright" ng-if="sidebarR.sidebar == 'Menu'" ng-disabled="!sidebarR.menuID" ng-click="updaterightsidebar(currentEditright,sidebarR)"><i class="glyphicon glyphicon-edit"></i>Update</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtnright" ng-if="sidebarR.sidebar == 'Calendar' || sidebarR.sidebar == 'Archive' || sidebarR.sidebar == 'Rss'" ng-click="updaterightsidebar(currentEditright,sidebarR)"><i class="glyphicon glyphicon-edit"></i>Update</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtnright" ng-if="sidebarR.sidebar == 'Image'" ng-disabled="!sidebarR.img" ng-click="updaterightsidebar(currentEditright,sidebarR)"><i class="glyphicon glyphicon-edit"></i>Update</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtnright" ng-if="sidebarR.sidebar == 'Testimonial'" ng-disabled="!sidebarR.limittest" ng-click="updaterightsidebar(currentEditright,sidebarR)"><i class="glyphicon glyphicon-edit"></i>Update</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtnright" ng-if="sidebarR.sidebar == 'Link'" ng-disabled="!sidebarR.label || !sidebarR.url" ng-click="updaterightsidebar(currentEditright,sidebarR)"><i class="glyphicon glyphicon-edit"></i>Update</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtnright" ng-if="sidebarR.sidebar == 'News'" ng-disabled="!sidebarR.limitnews" ng-click="updaterightsidebar(currentEditright,sidebarR)"><i class="glyphicon glyphicon-edit"></i>Update</button>



              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtnright" ng-if="sidebarR.sidebar == 'Menu'" ng-disabled="!sidebarR.menuID" ng-click="addrightsidebar(sidebarR)"><i class="glyphicon  glyphicon-plus-sign "></i>Add</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtnright" ng-if="sidebarR.sidebar == 'Calendar' || sidebarR.sidebar == 'Archive' || sidebarR.sidebar == 'Rss'" ng-click="addrightsidebar(sidebarR)"><i class="glyphicon  glyphicon-plus-sign "></i>Add</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtnright" ng-if="sidebarR.sidebar == 'Image'" ng-disabled="!sidebarR.img" ng-click="addrightsidebar(sidebarR)"></i>Add</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtnright" ng-if="sidebarR.sidebar == 'Testimonial'" ng-disabled="!sidebarR.limittest" ng-click="addrightsidebar(sidebarR)"><i class="glyphicon  glyphicon-plus-sign "></i>Add</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtnright" ng-if="sidebarR.sidebar == 'Link'" ng-disabled="!sidebarR.label || !sidebarR.url" ng-click="addrightsidebar(sidebarR)"><i class="glyphicon  glyphicon-plus-sign "></i>Add</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtnright" ng-if="sidebarR.sidebar == 'News'" ng-disabled="!sidebarR.limitnews" ng-click="addrightsidebar(sidebarR)"><i class="glyphicon  glyphicon-plus-sign "></i>Add</button>
            </div>
              <div class="panel-body">
                <div class="col-sm-12">
                  <div class="">
                  
                    <select class="form-control dd3-content col-sm-12" name="rightsidebar" ng-model="sidebarR.sidebar" style="margin-top:10px;" id="right">
                      <option value="" >-Select-</option>
                      <option value="Menu">Menu</option>
                      <option value="Calendar">Calendar</option>
                      <option value="Image">Image</option>
                      <option value="Testimonial">Testimonial</option>
                      <option value="Link">Link</option>
                      <option value="News">News</option>
                      <option value="Archive">Archive</option>
                      <option value="Rss">RSS</option>
                    </select>
                  </div>
                  <div style="padding-top:0px !important;" class="bg-light" ng-if="sidebarR.sidebar =='Image'">
                    <div class="line line-dashed b-b line-lg"></div>
                    &nbsp;&nbsp;<i class="icon-arrow-right"></i> Image
                    <a class="btn btn-default btn-xs pull-right" ng-click="media('featured','banner','right')"><i class="fa  fa-folder-open"></i> Media Library</a>
                    <div class="">
                      <img ng-if="sidebarR.img" src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{sidebarR.img}]}" style="width: 100%">
                      <input type="text" class="form-control" ng-model="sidebarR.imglink" placeholder="Image-Link(optional)">
                    </div>
                  </div>

                  <div style="padding-top:0px !important;" class="bg-light" ng-if="sidebarR.sidebar =='Testimonial'">
                    <div class="line line-dashed b-b line-lg"></div>
                    &nbsp;&nbsp;<i class="icon-arrow-right"></i> Testimonial Limit List View
                    <div class="">
                      <div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input ng-model="sidebarR.limittest" ui-jq="TouchSpin" type="text" value="10" class="form-control" data-min="0" data-max="20" data-verticalbuttons="true" data-verticalupclass="fa fa-caret-up" data-verticaldownclass="fa fa-caret-down" style="display: block; " class="form-control ng-pristine ng-invalid ng-invalid-required" required><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span></div>
                    </div>
                  </div>
                   <div style="padding-top:0px !important;" class="wrapper bg-light" ng-if="sidebarR.sidebar == 'Link'">
                    <div class="line line-dashed b-b line-lg"></div>
                    &nbsp;&nbsp;<i class="icon-arrow-right"></i> Link
                    <div class="">
                      <input type="text" ng-model="sidebarR.label" class="form-control ng-pristine ng-invalid ng-invalid-required" placeholder="Label"  required=""><br>
                      <input type="url" ng-model="sidebarR.url"  class="form-control" placeholder="http://"  required="">
                    </div>
                  </div>

                   <div style="padding-top:0px !important;" class="wrapper bg-light" ng-if="sidebarR.sidebar == 'Menu'">
                    <div class="line line-dashed b-b line-lg"></div>
                    &nbsp;&nbsp;<i class="icon-arrow-right"></i> Menu
                    <div class="">
                     <select class="form-control col-sm-8" required="required" ng-model="sidebarR.menuID"  style="margin-top:10px;width:60%" >
                      <option value="" style="display:none">-Select-</option>
                      <option ng-selected="{[{ x.menuID == sidebarR.menuID }]}" ng-repeat='x in menu' value='{[{x.menuID}]}'> {[{x.name}]} </option>
                    </select>
                    </div><br><br>
                  </div>

                  <div style="padding-top:0px !important;" class="wrapper bg-light" ng-if="sidebarR.sidebar =='News'">
                    <div class="line line-dashed b-b line-lg"></div>
                    &nbsp;&nbsp;<i class="icon-arrow-right"></i> News
                    <div class="">
                     <!--  <br>
                      <label class="i-checks">
                        <input type="checkbox" ng-model="page.leftnewspopular[$index]" required="" class="ng-dirty ng-invalid ng-invalid-required"><i></i> Most Papular
                      </label>
                       &nbsp;&nbsp;&nbsp;
                       <label class="i-checks">
                        <input type="checkbox" ng-model="page.leftnewsviews[$index]" required="" class="ng-dirty ng-invalid ng-invalid-required"><i></i> Most Viewed
                      </label> -->
                      <br>
                      <div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input ng-model="sidebarR.limitnews" ui-jq="TouchSpin" type="text" value="10" class="form-control" data-min="0" data-max="20" data-verticalbuttons="true" data-verticalupclass="fa fa-caret-up" data-verticaldownclass="fa fa-caret-down" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span></div>
                    </div>
                  </div>
                  <div class="line line-dashed b-b line-lg"></div>
                </div>
                <div class="col-sm-12">
                 <ul class="list-group no-radius sortable" ui:sortable ng:model="page.sidedataR" id="list1">
                      <li style="border:1px solid #ccc;cursor:pointer;margin-bottom:6px;max-height:330px;overflow-y:auto;" class="list-group-item" ng-repeat="rightdata in page.sidedataR track by $index" ng-hide="currentEditright == $index">
                            <!-- actual -->
                        <span>
                          <span style="font-size:16px"> {[{ rightdata.sidebar }]} </span> 
                           <span class="pull-right" style="font-size:14px"><a href="" ng-click="deleteright($index)"><i class="fa fa-times-circle"></i></a></span>
                           <span class="pull-right" style="font-size:14px"><a href="" ng-click="editright($index)"><i class="fa fa-pencil-square"></i></a></span>
                              <!-- calendar -->
                           <div ng-if="rightdata.sidebar=='Calendar'"> 
                            <div class="calendar" ng-model="eventSources" calendar="calendar1" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div>
                           </div>
                              <!-- image -->
                           <div ng-if="rightdata.sidebar=='Image'">
                              <img src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{ rightdata.img }]}" style="width: 100%">
                           <input ng-if="rightdata.imglink" type="text" class="form-control" ng-model="rightdata.imglink" disabled="">
                           </div>
                              <!-- testimonial -->
                           <div ng-if="rightdata.sidebar == 'Testimonial'">
                             <div class="size18"><h4 class="font1 italic">What People Are Saying</h4></div>
                             <div ng-repeat="x in testi | limitTo:rightdata.limittest">
                              <div class="specialpage_col">  
                                <div class="size16 border-left italic margin-top special-testi-wrapper">
                                  <div class="row">
                                    <div class="col-sm-12 text-center page-testi-image">
                                      <img src="{[{s3link}]}/uploads/testimonialpic/{[{x.picture}]}" class="testi-img">
                                    </div>
                                    <div class="col-sm-12">
                                      <div></div>
                                      <div>
                                        <span class="fa fa-quote-left"></span>
                                        {[{x.message}]}
                                      </div>
                                      <div class="text-right">-  {[{x.name}]} </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        <!-- news -->
                          <div ng-if="rightdata.sidebar == 'News'" class="col-sm-12">
                        <h4 class="blog-title-list">Latest News </h4>
                        <div class="list-news-wrapper">
                          <div class="row list-title-blog" ng-repeat="news in news | limitTo:rightdata.limitnews">
                            <div class="col-sm-12 news-thumb-container" title="{[{ news.title }]}">
                              <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}" style="height:150px;width:100%">
                              <div class="youtube-play" ng-show="news.videothumb"><img src="img/youtubeplay.png" ng-click="redirectNews(news.newsslugs);"/></div>
                              
                            </div>
                            <div class="col-md-12 news-list-desc">
                              <div class="row">
                                <div class="col-sm-12">
                                  <span class="size25 font1 news-title">{[{ news.title }]}</span>
                                </div>
                                <div class="col-sm-12">
                                  <span class="thin-font1 orange">{[{ news.categorylist }]}</span><span ng-show="news.name !=''" class="thin-font1"> / by <span class="orange">{[{ news.name }]}</span></span> / {[{ news.date }]}
                                  <br/><br/>
                                </div>
                                <div class="col-sm-12" ng-if="news.summary">
                                  <div class="font1 size14 summary">
                                    {[{ news.summary }]}
                                    <br/><br/>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- link -->
                      <div ng-if="rightdata.sidebar=='Link'" class='lnk'>
                        <a href='javascript:void(0);' class="page-link">
                       {[{rightdata.label}]}
                         </a>
                      </div>
                       <!-- Rss -->
                      <div ng-if="rightdata.sidebar=='Rss'" class='lnk'>
                      <p class="size16"> <a href="javascript:void(0);">
                      <img class="rss" src="/img/frontend/rss_feed.gif"> RSS Feed </a></p>
                      </div>
                      <!-- Archive -->
                      <div ng-if="rightdata.sidebar=='Archive'">
                      <h3 class="font1"><span class="fa fa-calendar fa-1x"></span> Archives</h3>
                      <div class="ul-archives">
                       <ul ng-repeat="archive in archive" class="ul-archive">
                         <li>
                        <a style="color:#ff8421" href="javascript:void(0);"><span class="fa fa-chevron-right"></span> {[{archive.month}]} {[{archive.year}]}</a>
                         </li>
                       </ul>
                    </div>
                    </div>

                    <!-- Menu -->
                    <div class="special-menu" ng-if="rightdata.sidebar=='Menu'">
            <div><a class="special-top-menu">Sedona Healing Arts</a></div>
            <nav>
              <ul style="list-style:disc">
                <li ng-repeat="mobmenu in shortcode" ng-if="mobmenu.menuID == rightdata.menuID ">
                  <a class="special-menu-text" href="javascript:void(0);"><span ng-bind="mobmenu.subname"></span></a>

                  <ul class="no-list" ng-repeat="mob in children">
                    <li ng-repeat="mobsub1 in mob.child1" ng-if="mobsub1.parent==mobmenu.submenuID">
                    <span class="glyphicon glyphicon-menu-right" style="color:#58666e;">
                      <a class="special-menu-text" href="javascript:void(0);"><span ng-bind="mobsub1.subname"></span></a>

                      <ul class="no-list">
                        <li ng-repeat="mobsub2 in mob.child2" ng-if="mobsub2.parent==mobsub1.submenuID">
                         <span class="glyphicon glyphicon-menu-right" style="color:#58666e;">
                          <a class="special-menu-text" href="javascript:void(0);"><span ng-bind="mobsub2.subname"></span></a>

                          <ul class="no-list">
                            <li ng-repeat="mobsub3 in mob.child3" ng-if="mobsub3.parent==mobsub2.submenuID">
                            <span class="glyphicon glyphicon-menu-right" style="color:#58666e;">
                              <a class="special-menu-text" href="javascript:void(0);"><span ng-bind="mobsub3.subname"></span></a>
                            
                              <ul class="no-list">
                              <li ng-repeat="mobsub4 in mob.child4" ng-if="mobsub4.parent==mobsub3.submenuID">
                                  <span class="glyphicon glyphicon-menu-right" style="color:#58666e;">
                                  <a class="special-menu-text" href="javascript:void(0);"><span ng-bind="mobsub4.subname"></span></a>
                                </li>
                              </ul>

                            </li>
                          </ul>

                        </li>
                      </ul>

                    </li>
                  </ul>


                </li>

              </ul>
            </nav>
          </div>
                        </span>
                      </li>
                  </ul>
              <script type="text/javascript">
              $('#list1').sortable({
                    placeholder: "testclass",
                    forcePlaceholderSize: true
                  });
                </script>
                </div>
              </div>
          </div>
          <div class="panel panel-success bg-gray lter">
            <div class="panel-heading font-bold">
              Page Banner
              <label class="pull-right i-switch bg-info m-t-xs m-r">
               <input type="checkbox" ng-model="page.banner" checked>
               <i></i>
             </label>
            </div>
            <div class="panel-body" ng-show="page.banner">
              <a class="btn btn-default btn-xs pull-right" ng-click="media('featured','thumb')"><i class="fa  fa-folder-open"></i> Media Library</a>
                    <label><em class="text-muted">Click Media Library to add image or select an existing image. Recommended image size: a ratio of 704px in height.</em></label>
                    <img ng-show="page.imagethumb" src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{ page.imagethumb }]}" style="width: 100%">
                    <div class="line line-dashed b-b line-lg"></div>
                    Title (maxlength of 60)
                    <input type="text" id="imagethumbsubtitle" name="imagethumbsubtitle" class="form-control" ng-model="page.imagethumbsubtitle" maxlength="60">
                    <div class="line line-dashed b-b line-lg"></div>

                    Short Description (maxlength of 300)
                    <textarea type="text" id="thumbdesc" name="thumbdesc" class="form-control resize-v" ng-model="page.thumbdesc" maxlength="300" rows="4"> </textarea>
                    <div class="line line-dashed line-lg b-b"></div>

                    <div class="form-group" ng-init="page.align = 'center' ">
                      <div class="col-sm-3">Text Align</div>
                      <div class="col-sm-9 wrapper-sm">
                        <div class="btn-group">
                          <label class="btn btn-primary" ng-model="page.align" btn-radio="'left'">Left</label>
                          <label class="btn btn-primary" ng-model="page.align" btn-radio="'center'">Center</label>
                          <label class="btn btn-primary" ng-model="page.align" btn-radio="'right'">Right</label>
                        </div>
                      </div>
                    </div>
                    <div class="line line-dashed b-b line-lg"></div>

                    <div class="form-group">
                      <div class="col-sm-3">Background Color</div>
                      <div class="col-sm-9 well bg-light wrapper-sm">
                        <input colorpicker ng-model="page.bgcolor" ng-init="page.bgcolor = '#ffffff' " type="text" class="form-control width90px pull-left pointer" readonly>
                        <div class="colorpicker-colorbox form-control pull-left" style="background-color: {[{ page.bgcolor }]}"></div>
                      </div>
                    </div>
                    <div class="line line-dashed line-lg b-b"></div>

                    <div class="form-group">
                      <div class="col-sm-3">Text Color</div>
                      <div class="col-sm-9 well bg-light wrapper-sm">
                        <input colorpicker ng-model="page.color" type="text" ng-init="page.color = '#000000' " class="form-control ng-pristine ng-invalid ng-valid-pattern width90px pull-left pointer" readonly>
                        <div class="colorpicker-colorbox form-control pull-left" style="background-color: {[{ page.color }]}"></div>
                      </div>
                    </div>

                    <div class="line line-dashed b-b line-lg"></div>

                    <div class="form-group">
                      <div class="col-sm-3">Text Box</div>
                      <div class="col-sm-9 well bg-light wrapper-sm">
                        <label class="i-switch bg-info m-t-xs m-r">
                          <input type="checkbox" ng-model="page.box">
                          <i></i>
                        </label>
                        <span ng-if="page.box == false" class="label bg-danger Services-checkbox-label">off</span>
                        <span ng-if="page.box == true" class="label bg-success Services-checkbox-label">on</span>
                      </div>
                    </div>
                    <div class="line line-dashed line-lg b-b" ng-if="page.box == true"></div>

                     <div class="font-bold" ng-if="page.box == true">TextBox
                      <em>(Optional)</em></div>
                    <div class="line line-dashed line-lg b-b" ng-if="page.box == true"></div>

                    <div class="form-group" ng-if="page.box == true">
                      <div class="col-sm-3"> Transparency </div>
                      <div class="col-sm-9 well bg-light wrapper-sm">
      
                       <input type="range" min="0" max="100" ng-model="page.boxtrans">
                        {[{page.boxtrans}]}<span ng-if="page.trans"> % </span>
                      </div>
                      <div class="col-sm-3"> Color </div>
                      <div class="col-sm-9 well bg-light wrapper-sm">
                            <input colorpicker="" ng-model="page.boxcolor" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern width90px pull-left ng-touched" required="required">
                        <div class="colorpicker-colorbox form-control pull-left" style="background-color: {[{page.boxcolor}]} "></div>
                      </div>
                      <div class="col-sm-3"> Border Edge </div>
                      <div class="col-sm-9 well bg-light wrapper-sm">
                             <div class="btn-group">
                          <label class="btn btn-primary" ng-model="page.boxedge" btn-radio="'oval'">Oval</label>
                          <label class="btn btn-primary" ng-model="page.boxedge" btn-radio="'square'">Square</label>
                        </div>

                      </div>
                  
                    </div>

                    <div class="line line-dashed line-lg b-b"></div>
                    <div class="font-bold">Button
                      <em>(Optional)</em></div>
                    <div class="line line-dashed line-lg b-b"></div>
                    <div class="form-group hiddenoverflow">
                      <label class="col-sm-2 control-label">Button Name</label>
                      <div class="col-sm-10">
                        <input type="text" name="" ng-space class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.btnname">
                      </div>
                    </div>
                    <div class="line line-dashed b-b line-lg"></div>

                    <div class="form-group hiddenoverflow">
                      <label class="col-sm-2 control-label">Button Link</label>
                      <div class="col-sm-10">
                        <input type="text" name="" ng-space class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.btnlink" placeholder="e.g.: http://www">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-12">
                        <tabset class="tab-container tab-danger">
                          <tab>
                             <tab-heading class="font-bold">Note</tab-heading>
                             Please click <a class="btn m-b-xs btn-xs btn-success" ng-click="stylepreview(page)">Quick Preview</a>
                             before submitting so you can check whether the title and description you inserted is too long. Thank you.
                          </tab>
                        </tabset>
                      </div>
                    </div>
            </div>
          </div>

      </div>
      </div>
      </div>
      </div>
      </div>
      <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-right bg-light lter">
              <a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
              <button type="submit" class="btn btn-success">Submit</button>
            </footer>
        </div>
      </div>
  </div>
</fieldset>
</form>
