<style>
.list {
    list-style: none outside none;
    margin: 10px 0 30px;
}

#arr li{
    cursor: move;
    padding:1%;
}
pre
{
  background-color: white;
    border:none;
    margin-top:0px;
    padding:0px;
    font-family: "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    color:#58666e;
 white-space: normal;
}

</style>
<script type="text/ng-template" id="EditQuestion.html">
    <div ng-include="'/be/tpl/test/EditQuestion.html'"></div>
</script>

<script type="text/ng-template" id="EditResult.html">
    <div ng-include="'/be/tpl/test/EditResult.html'"></div>
</script>

<script type="text/ng-template" id="Testname.html">
    <div ng-include="'/be/tpl/test/Testname.html'"></div>
</script>

<script type="text/ng-template" id="DeleteQuestion.html">
    <div ng-include="'/be/tpl/test/DeleteQuestion.html'"></div>
</script>
<script type="text/ng-template" id="DeleteResult.html">
    <div ng-include="'/be/tpl/test/DeleteResult.html'"></div>
</script>

<script type="text/ng-template" id="ManageServices.html">
    <div ng-include="'/be/tpl/test/ManageServices.html'"></div>
</script>
<script type="text/ng-template" id="ManageProducts.html">
    <div ng-include="'/be/tpl/test/ManageProducts.html'"></div>
</script>

<script type="text/ng-template" id="EditResult.html">
    <div ng-include="'/be/tpl/test/EditResult.html'"></div>
</script>
<script type="text/ng-template" id="AddQuestion.html">
    <div ng-include="'/be/tpl/test/AddQuestion.html'"></div>
</script>
<script type="text/ng-template" id="AddEditResult.html">
    <div ng-include="'/be/tpl/test/AddEditResult.html'"></div>
</script>
<script type="text/ng-template" id="AddEditRec.html">
    <div ng-include="'/be/tpl/test/AddEditRec.html'"></div>
</script>
<script type="text/ng-template" id="EditServices.html">
    <div ng-include="'/be/tpl/test/EditServices.html'"></div>
</script>
<script type="text/ng-template" id="EditProducts.html">
    <div ng-include="'/be/tpl/test/EditProducts.html'"></div>
</script>
<script type="text/ng-template" id="ArrangeQuestions.html">
    <div ng-include="'/be/tpl/test/ArrangeQuestions.html'"></div>
</script>
<div class="wrapper-md">

    <div class="panel-body">

<input type='hidden' ng-model='indexko'>

<div class="panel panel-default">
                        <div class="panel-heading">
                            Edit Test
                           
                        </div>

                        <div class="row wrapper" style="margin: 0px 5px;">
                            <tabset class="tab-container">
                                <tab heading="Test Name" active="steps.step1" select="steps.percent=3">

                                    <form name="firststep1" id="firststep1" novalidate class="form-validation">
                                      
                                      <div class='row'>
                                      <div class='col-sm-5'>
                                      <table class='table'>
                                          <thead>
                                              <th> Test Name</th>
                                          </thead>
                                          <tbody ng-repeat='ss in test'>
                                              <td> {[{ss.testname}]} </td>
                                              <td>  
                                            <a href class="btn btn-default btn-xs" ng-click="edittestname(ss.testid)">
                                                <i class="fa fa-edit"></i>
                                                Edit
                                            </a>
                                            </td>
                                          </tbody>
                                      </table>

                                        <div class="m-t m-b">
                                        <br><br>
                                            <button type="submit" class="btn btn-default btn-rounded" ng-click="steps.step2=true;">Next</button>
                                        </div>
                                        </div>
                                        </div>
                                    </form>
                                </tab>

                     <!--        Test Question -->
                            <tab heading="Test Questions" active="steps.step2">
                                    <form name="step2" class="form-validation">
                                      <button class='btn btn-info' ng-click='addq(indexko)'> Add Questions </button>
                                       <button class='btn btn-primary' ng-click='sort(indexko)'> Arrange Questions </button>
                                        <table ui-sortable="sortableOptions" class='table table-striped b-t b-light' id='tblq'>
                                            <thead>
                                                <th> Questions </th>
                                            </thead>

                                            <tbody ng-repeat="rs in testlist">
                                                <td> {[{rs.question}]} 
                                                 <ol type='A'>
                                            <li>
                                                {[{rs.A}]}
                                            </li>

                                            <li>
                                                {[{rs.B}]}
                                            </li>

                                            <li>
                                                {[{rs.C}]}
                                            </li>

                                        </ol>
                                                </td>
                                                
                                                <td>   <a href class="btn btn-default btn-xs" ng-click="editq(rs.q_id)">
                                                    <i class="fa fa-edit"></i>
                                                    Edit
                                                </a> 

                                                <a href class="btn btn-default btn-xs" ng-click="deleteq(rs.q_id)">
                                                    <i class="fa fa-edit"></i>
                                                    Delete
                                                </a>

                                            </td>

                                        </tbody> 

                                    </table>
                                    </form>
                                    <div class="m-t m-b">
                                        <br><br>
                                        <button type="button" class="btn btn-default btn-rounded pull-left" ng-click="steps.step1=true">Prev</button>

                                            <button type="submit" class="btn btn-default btn-rounded" ng-click="steps.step3=true;">Next</button>
                                        </div>
                                </tab>

                                <tab heading="Test Results" active="steps.step3" select="steps.percent=75">
                            <a href class="btn btn-info btn-sm" ng-click='check(indexko)'>
                                        <i class="fa fa-plus"></i>
                                        ADD RESULT
                                    </a>
                     
                                      <div class="panel panel-default" width='100%'>
                                   <div class="panel-heading"> Edit Result</div>
                                  
                                   <table class='table table-striped b-t b-light' style='table-layout: fixed;'>
                                    <thead>
                                        <th> Probabilities </th>
                                        <th> Result </th>
                                       <!--  <th> Description </th> -->
                                        <th> Services </th>
                                        <th> Link </th>
                                        <th> Products </th>
                                        <th> Action </th>
                                    </thead>

                                    <tbody ng-repeat="rs in resultlist">
                                        <td> {[{rs.choice}]} </td>
                                        <td> {[{rs.result}]} </td>
                                       <!--  <td> {[{rs.description}]} </td> -->
                                        <td> 
                                       
                                        <ul ng-repeat="ss in recommendedlist">
                                        <li ng-hide="((ss.choice!=rs.choice)  || (ss.services==null) | (ss.services==''))">
                                          <pre>  {[{ss.services}]}      </pre>
                                        </li>
                                        </ul>


                                        </td>
                                         <td> 
                                        <ul ng-repeat="ss in recommendedlist">
                                        <li ng-hide="((ss.choice!=rs.choice)  || (ss.link==null) | (ss.link==''))">
                                          <pre>  {[{ss.link}]}    </pre>
                                        </li>
                                        </ul>
                                        </td>

                                        <td> 
                                        <ul ng-repeat="ss in recommendedlist">
                                        <li ng-hide="((ss.choice!=rs.choice)  || (ss.products==null) || (ss.products==''))">
                                           <pre>  {[{ss.products}]} </pre>
                                        </li>
                                        </ul>

                                        </td>

                                        <td>  
                                         <button type="button" ng-click='rec(indexko,rs.choice,$index)' class="btn btn-primary btn-sm">
                                       <i class="fa fa-plus"></i>
                                       <span style="word-break: break-all;
    word-wrap: break-word;"> Recommendations </span> </button>

                                     <a href class="btn btn-warning btn-sm" ng-click="editr(rs.id,indexko,rs.choice)">
                                            <i class="fa fa-edit"></i>
                                            Edit
                                        </a> 

                                        <a href class="btn btn-danger btn-sm" ng-click="deleterr(rs.id,rs.choice,indexko)">
                                                    <i class="fa fa-edit"></i>
                                                    Delete
                                                </a>
                                        </td>

                                    </tbody> 
                                </table>
                                </div>

                                <div class="line line-dashed b-b line-lg"></div>

                    
                                    <div class="m-t m-b text-right">
                                        <button type="button" class="btn btn-default btn-rounded pull-left" ng-click="steps.step2=true">Prev</button>
                                        <!--<button type="button" class="btn btn-default btn-rounded pull-right" ng-click="steps.percent=100">Final</button>-->
                                        <button class="btn btn-success" ng-show="radioModel == 'cash' || (user.billinginfo && user.billinginfo.paymenttype == radioModel)" ng-click="submit(user, firststep1, radioModel)">
                                            <span class="text">Submit</span>
                                        </button>
                                        <div class="clearfix"></div>
                                    </div>
                                </tab>
                            </tabset>
                            <div class='pull-left'>
                            <button type='button' ng-click='go()' class='btn btn-primary'> GO BACK </button>
                            </div>
                        </div>

                    </div>

</div>
