<?php echo $this->getContent(); ?>
<style type="text/css">
.box {
    height: 90px;
    width: 90px;
    margin: 5px;
}

#horizontal .box, #horizontal .ui-sortable-placeholder {
    display: inline-block;
}
.col-container {
   padding-right:0;padding-left:0;display:inline-block
}
.connectedSortable{
  padding-right: 25px;
}
.ui-sortable-placeholder {
    border: 1px dotted black;
    visibility: visible !important;
    background-color: inherit;
}
.ui-sortable-helper{
 /* border: 1px solid red;*/
}
.dd3-handle{
      height: 34px;
}

.dd-item {
height:inherit;
}
.dd-item-column{
min-height: 100px!important;
}

.dd-side-item{
  padding:9px;
}
.sortable{
    border: 0px;
    list-style: none;
    padding: 0;
    display: inline-block;
    max-height: 1000px;
    overflow-y: auto;
    overflow-x: hidden;
    width: 210px;
    float: left;
    margin: 5px;
}
.sortablecol{
    border: 0px;
    list-style: none;
    display: inline;
    padding: 0;
    max-height: 1000px;
    overflow-y: auto;
    overflow-x: hidden;
    margin: 5px;
    cursor: pointer;
}
.sortablerow{
    list-style: none;
}
.sortrow{
    list-style: none;
    overflow-y: auto;
     overflow-x: hidden;
     max-width: 790px;
}
.no-pad{
  padding-right: 0px;
  padding-left: 0px;
}
.but{
    float: left;
    width: 35px;
}
.mainbut{
     float: left;
    width: 195px;
}
.mainbut2{
     float: left;
    width: 126px;
}
</style>
<script type="text/ng-template" id="mediagallery.html">
  <div ng-include="'/be/tpl/mediagallery.html'"></div>
</script>
<script type="text/ng-template" id="column.html">
  <div ng-include="'/be/tpl/pagebuilder/column.html'"></div>
</script>
<script type="text/ng-template" id="module.html">
  <div ng-include="'/be/tpl/pagebuilder/module.html'"></div>
</script>
<script type="text/ng-template" id="moduletext.html">
  <div ng-include="'/be/tpl/pagebuilder/moduletext.html'"></div>
</script>
<script type="text/ng-template" id="moduleimage.html">
  <div ng-include="'/be/tpl/pagebuilder/moduleimage.html'"></div>
</script>
<script type="text/ng-template" id="moduletestimonial.html">
  <div ng-include="'/be/tpl/pagebuilder/moduletestimonial.html'"></div>
</script>
<script type="text/ng-template" id="modulecontact.html">
  <div ng-include="'/be/tpl/pagebuilder/modulecontact.html'"></div>
</script>
<script type="text/ng-template" id="modulenews.html">
  <div ng-include="'/be/tpl/pagebuilder/modulenews.html'"></div>
</script>
<script type="text/ng-template" id="moduledivider.html">
  <div ng-include="'/be/tpl/pagebuilder/moduledivider.html'"></div>
</script>
<script type="text/ng-template" id="stylepreview">
  <div ng-include="'/be/tpl/pagebuilder/bannerpreview.html'"></div>
</script>
<script type="text/ng-template" id="deleterow.html">
  <div ng-include="'/be/tpl/pagebuilder/deleterow.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Create Page</h1>
  <a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="savePage(page)" name="formpage">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="row">
        <div class="col-sm-8">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Page Information <span style="color:red"> * </span>
            </div>
            <div class="panel-body">
              Page Type
               <div class="form-group" ng-init="page.type = 'Normal' "><br>
              <div class="btn-group">
                <label class="btn btn-info" ng-model="page.type" btn-radio="'Normal'"><i class="fa fa-check text-active"></i> Normal</label>
                <label class="btn btn-success" ng-model="page.type" btn-radio="'Special'"><i class="fa fa-check text-active"></i> Special</label>
              </div>
              </div>

              <div class="line line-dashed b-b line-lg"></div>
              Title
              <input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.title" required="required" ng-keyup="onpagetitle(page.title);uniqueslugs(page.slugs)">
              <br>
              <div class="line line-dashed b-b line-lg"></div>
              <span ng-show="validslugs" class="pull-left mg-left text-danger">Slugs is already exist, please change it.</span>
              <div ng-show="validslugs" class="line line-dashed b-b line-lg"></div>
              <b class="pull-left">Page Slugs: </b>
              <input type="text" ng-show="editslug" id="pageslugs" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern pull-left" ng-model="page.slugs" ng-keypress="onslugs(page.slugs);" ng-change="uniqueslugs(page.slugs)"><span ng-bind="page.slugs" class="pull-left mg-left"></span>
              <div ng-show="editslug">
                <a class="btn btn-danger btn-xs pull-right mg-left" ng-click="cancelpageslug(page.title)">cancel</a>
                <a class="btn btn-primary btn-xs pull-right mg-left" ng-click="setslug(page.slugs)">ok</a>
              </div>
              <a class="btn btn-danger btn-xs pull-right mg-left" ng-hide="editslug" ng-click="clearslug(page.title)">clear</a>
              <a class="btn btn-primary btn-xs pull-right mg-left" ng-hide="editslug" ng-click="editpageslug()">edit slug</a>
              <div class="line line-dashed b-b line-lg"></div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Page Content <span style="color:red"> * </span>
            </div> 
            <div class="panel-body" ng-if="page.type=='Normal'">
              Content
              <a class="btn btn-default btn-xs" ng-click="media('content')"><i class="fa  fa-folder-open"></i> Media Library</a>
              <br><br>
              <textarea class="ck-editor" ng-model="page.body" required="required"></textarea>
            </div>

<!-- Special STARTS -->
              <div class="panel-body" ng-if="page.type=='Special'">

               <div class="clear specialpage_pad">
                <button type="button" class="pull-right btn m-b-xs btn-sm btn-default btn-addon" ng-click="rownewitem($event)"><i class="fa fa-plus"></i>Add Row</button>
              </div>
         
            <ul class="list-group no-radius sortablecol" ui:sortable ng:model="row">
                  <li class="" ng-repeat="(indexX,count) in row track by indexX">
          <br>
                   
              <div class="bg-light">
           <!--    {[{$index}]} -->
           
                <div class="clear">
                   <button type="button" class="pull-right btn m-b-xs btn-sm btn-addon btn-danger mg-left" ng-click="rowdelitem($index)"><i class="fa fa-trash-o"></i>Delete Row</button>
                   <div ng-init="count.column = 0"></div>
                     <button type="button" class="pull-right btn m-b-xs btn-sm btn-addon btn-info mg-left" ng-click="insertmodule(count.column,$index)" ng-disabled="count.column==4"><i class="fa fa-plus"></i>Add Column</button>
                </div>

                 <input type="hidden" ng-model="count.colcount" style="color:black" required ng-if="count.colcount!=0 || count.colcount">
                 <input type="hidden" ng-model="dummy" style="color:black" required ng-if="count.colcount==0 || !count.colcount">

                <div class="clear specialpage_pad2" ng-if="count.column == 1">
                
                <ul class="list-group no-radius sortablerow" ui:sortable ng:model="count.row">
                    <li class="col-sm-12" ng-repeat="(indexY,counter) in count.row track by indexY"  ng-init="outerIndex = $index">

                  <div class="input-group col-sm-12" ng-if="counter.col">

                  <button type="button" type="button" class="col-sm-10 btn btn-default">{[{counter.col}]}</button>
                    <!-- TEXT MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'TEXT'" ng-model="counter.colcontent" style="color:black" required></textarea>
                    <button type="button" type="button" ng-if="counter.col == 'TEXT'" ng-click="edittextmodule(1,indexX,counter.colcontent,$index)" title="Edit" class="col-sm-1 btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="counter.col == 'TEXT'" ng-click="counter.col = '';counter.colcontent='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-1 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimg" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimglink" style="color:black"></textarea>
                    <button type="button" type="button" ng-if="counter.col == 'IMAGE'" ng-click="editimagemodule(1,indexX,counter.colimg,$index,counter.colimglink)" title="Edit" class="col-sm-1 btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="counter.col == 'IMAGE'" ng-click="counter.col = '';counter.colimg='';counter.collink='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-1 btn btn-warning"><i class="fa fa-trash-o"></i></button>



                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonial" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonialcat" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="edittestimonialmodule(1,indexX,counter.coltestimonial,counter.coltestimonialcat,$index)" title="Edit" class="col-sm-1 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="counter.col = '';counter.coltestimonial='';counter.coltestimonialcat='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-1 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                       <!-- CONTACT FORM MODULE -->
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="editcontactmodule(1,indexX,counter.colcontact,counter.colcontactemail,$index)" title="Edit" class="col-sm-1 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="counter.col = '';counter.colcontact='';counter.colcontactemail='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-1 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'NEWS'" ng-model="counter.colnewslimit" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'NEWS'" ng-click="editnewsmodule(1,indexX,count.col1viewed,count.col1popular,counter.colnewslimit,$index)" title="Edit" class="col-sm-1 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'NEWS'" ng-click="counter.col = '';counter.colnewslimit='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-1 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                      <!-- DIVIDER MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colheight" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colcolor" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'DIVIDER'" ng-click="editdividermodule(1,indexX,counter.colheight,counter.colcolor,$index)" title="Edit" class="col-sm-1 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="counter.col == 'DIVIDER'" ng-click="counter.col = '';counter.colheight='';counter.colcolor='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-1 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                    </div>
                    </li>
                    </ul>
                </div>

  <div class="clear specialpage_pad row" ng-if="count.column == 2">

                  <input type="hidden" ng-model="count.colcount" style="color:black" required>

                    <script type="text/javascript">
                    var $horizontal = $(".horizontal");
                    $horizontal.sortable({
                      connectWith: ".connectedSortable"
                    });
                  </script>
                     
               
  <div id="horizontal" class="connectedSortable col-sm-12 horizontal" >
    <ul class="list-group no-radius sortablerow" ui:sortable ng:model="count.row">
                    <li class="col-sm-6 no-pad" ng-repeat="(indexY,counter) in count.row track by indexY">

                  <span class="col-sm-12 col-container" ng-if="$index == 0">
                  <a type="button" class="col-sm-10 btn btn-default"> {[{counter.col}]} </a>

                   <!-- TEXT MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'TEXT'" ng-model="counter.colcontent" style="color:black" required></textarea>
                    <button type="button" type="button" ng-if="counter.col == 'TEXT'" ng-click="edittextmodule(2,indexX,counter.colcontent,$index)" title="Edit" class="col-sm-1 btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="counter.col == 'TEXT'" ng-click="counter.col = '';counter.colcontent='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-1 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimg" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimglink" style="color:black"></textarea>
                    <button type="button" type="button" ng-if="counter.col == 'IMAGE'" ng-click="editimagemodule(2,indexX,counter.colimg,$index,counter.colimglink)" title="Edit" class="col-sm-1 btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="counter.col == 'IMAGE'" ng-click="counter.col = '';counter.colimg='';counter.collink='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-1 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonial" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonialcat" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="edittestimonialmodule(2,indexX,counter.coltestimonial,counter.coltestimonialcat,$index)" title="Edit" class="col-sm-1 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="counter.col = '';counter.coltestimonial='';counter.coltestimonialcat='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-1 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                       <!-- CONTACT FORM MODULE -->
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="editcontactmodule(2,indexX,counter.colcontact,counter.colcontactemail,$index)" title="Edit" class="col-sm-1 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="counter.col = '';counter.colcontact='';counter.colcontactemail='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-1 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'NEWS'" ng-model="counter.colnewslimit" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'NEWS'" ng-click="editnewsmodule(2,indexX,count.col1viewed,count.col1popular,counter.colnewslimit,$index)" title="Edit" class="col-sm-1 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'NEWS'" ng-click="counter.col = '';counter.colnewslimit='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-1 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                      <!-- DIVIDER MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colheight" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colcolor" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'DIVIDER'" ng-click="editdividermodule(2,indexX,counter.colheight,counter.colcolor,$index)" title="Edit" class="col-sm-1 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="counter.col == 'DIVIDER'" ng-click="counter.col = '';counter.colheight='';counter.colcolor='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-1 btn btn-warning"><i class="fa fa-trash-o"></i></button>
                      </span>

                  <div class="col-sm-12 col-container" ng-if="$index == 1">

                    <a type="button" class="col-sm-10 btn btn-default">{[{ counter.col }]} 
                    </a>

                   <!-- TEXT MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'TEXT'" ng-model="counter.colcontent" style="color:black" required></textarea>
                    <button type="button" type="button" ng-if="counter.col == 'TEXT'" ng-click="edittextmodule(2,indexX,counter.colcontent,$index)" title="Edit" class="col-sm-1 btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="counter.col == 'TEXT'" ng-click="counter.col = '';counter.colcontent='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-1 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimg" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimglink" style="color:black"></textarea>
                    <button type="button" type="button" ng-if="counter.col == 'IMAGE'" ng-click="editimagemodule(2,indexX,counter.colimg,$index,counter.colimglink)" title="Edit" class="col-sm-1 btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="counter.col == 'IMAGE'" ng-click="counter.col = '';counter.colimg='';counter.collink='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-1 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonial" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonialcat" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="edittestimonialmodule(2,indexX,counter.coltestimonial,counter.coltestimonialcat,$index)" title="Edit" class="col-sm-1 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="counter.col = '';counter.coltestimonial='';counter.coltestimonialcat='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-1 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                       <!-- CONTACT FORM MODULE -->
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="editcontactmodule(2,indexX,counter.colcontact,counter.colcontactemail,$index)" title="Edit" class="col-sm-1 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="counter.col = '';counter.colcontact='';counter.colcontactemail='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-1 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'NEWS'" ng-model="counter.colnewslimit" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'NEWS'" ng-click="editnewsmodule(2,indexX,count.col1viewed,count.col1popular,counter.colnewslimit,$index)" title="Edit" class="col-sm-1 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'NEWS'" ng-click="counter.col = '';counter.colnewslimit='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-1 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                      <!-- DIVIDER MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colheight" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colcolor" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'DIVIDER'" ng-click="editdividermodule(2,indexX,counter.colheight,counter.colcolor,$index)" title="Edit" class="col-sm-1 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="counter.col == 'DIVIDER'" ng-click="counter.col = '';counter.colheight='';counter.colcolor='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-1 btn btn-warning"><i class="fa fa-trash-o"></i></button>

              </div>
                  </li>
                </ul>
                </div>
              
              
          </div>

                <div class="clear specialpage_pad" ng-if="count.column == 3">
                  <input type="hidden" ng-model="count.colcount" style="color:black" required>
                       <script type="text/javascript">
                    var $horizontal = $(".horizontal");
                    $horizontal.sortable({
                      connectWith: ".connectedSortable"
                    });
                  </script>
                     
                    
  <div id="horizontal" class="connectedSortable col-sm-12 horizontal" >
    <ul class="list-group no-radius sortablerow" ui:sortable ng:model="count.row">
                    <li class="col-sm-4 no-pad" ng-repeat="(indexY,counter) in count.row track by indexY">

              <div class="col-sm-12 input-group col-container" ng-if="$index == 0">
                 <a type="button" type="button" class="mainbut btn btn btn-default">{[{ counter.col }]}</a>

                   <!-- TEXT MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'TEXT'" ng-model="counter.colcontent" style="color:black" required></textarea>
                    <button type="button" type="button" ng-if="counter.col == 'TEXT'" ng-click="edittextmodule(3,indexX,counter.colcontent,$index)" title="Edit" class="but btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="counter.col == 'TEXT'" ng-click="counter.col = '';counter.colcontent='';delcolumn(count.column,indexX,$index)" title="Delete" class="but btn btn-warning"><i class="fa fa-trash-o"></i></button>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimg" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimglink" style="color:black"></textarea>
                    <button type="button" type="button" ng-if="counter.col == 'IMAGE'" ng-click="editimagemodule(3,indexX,counter.colimg,$index,counter.colimglink)" title="Edit" class="but btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="counter.col == 'IMAGE'" ng-click="counter.col = '';counter.colimg='';counter.collink='';delcolumn(count.column,indexX,$index)" title="Delete" class="but btn btn-warning"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonial" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonialcat" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="edittestimonialmodule(3,indexX,counter.coltestimonial,counter.coltestimonialcat,$index)" title="Edit" class="but btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="counter.col = '';counter.coltestimonial='';counter.coltestimonialcat='';delcolumn(count.column,indexX,$index)" title="Delete" class="but btn btn-warning"><i class="fa fa-trash-o"></i></button>

                       <!-- CONTACT FORM MODULE -->
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="editcontactmodule(3,indexX,counter.colcontact,counter.colcontactemail,$index)" title="Edit" class="but btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="counter.col = '';counter.colcontact='';counter.colcontactemail='';delcolumn(count.column,indexX,$index)" title="Delete" class="but btn btn-warning"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'NEWS'" ng-model="counter.colnewslimit" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'NEWS'" ng-click="editnewsmodule(3,indexX,count.col1viewed,count.col1popular,counter.colnewslimit,$index)" title="Edit" class="but btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'NEWS'" ng-click="counter.col = '';counter.colnewslimit='';delcolumn(count.column,indexX,$index)" title="Delete" class="but btn btn-warning"><i class="fa fa-trash-o"></i></button>

                      <!-- DIVIDER MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colheight" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colcolor" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'DIVIDER'" ng-click="editdividermodule(3,indexX,counter.colheight,counter.colcolor,$index)" title="Edit" class="but btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="counter.col == 'DIVIDER'" ng-click="counter.col = '';counter.colheight='';counter.colcolor='';delcolumn(count.column,indexX,$index)" title="Delete" class="but btn btn-warning"><i class="fa fa-trash-o"></i></button>

                    </div>


                  <div class="col-sm-12 col-container" ng-if="$index == 1">
                    <a type="button" type="button" class="mainbut btn btn btn-default">{[{ counter.col }]}</a>

                     <!-- TEXT MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'TEXT'" ng-model="counter.colcontent" style="color:black" required></textarea>
                    <button type="button" type="button" ng-if="counter.col == 'TEXT'" ng-click="edittextmodule(3,indexX,counter.colcontent,$index)" title="Edit" class="but btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="counter.col == 'TEXT'" ng-click="counter.col = '';counter.colcontent='';delcolumn(count.column,indexX,$index)" title="Delete" class="but btn btn-warning"><i class="fa fa-trash-o"></i></button>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimg" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimglink" style="color:black"></textarea>
                    <button type="button" type="button" ng-if="counter.col == 'IMAGE'" ng-click="editimagemodule(3,indexX,counter.colimg,$index,counter.colimglink)" title="Edit" class="but btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="counter.col == 'IMAGE'" ng-click="counter.col = '';counter.colimg='';counter.collink='';delcolumn(count.column,indexX,$index)" title="Delete" class="but btn btn-warning"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonial" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonialcat" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="edittestimonialmodule(3,indexX,counter.coltestimonial,counter.coltestimonialcat,$index)" title="Edit" class="but btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="counter.col = '';counter.coltestimonial='';counter.coltestimonialcat='';delcolumn(count.column,indexX,$index)" title="Delete" class="but btn btn-warning"><i class="fa fa-trash-o"></i></button>

                       <!-- CONTACT FORM MODULE -->
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="editcontactmodule(3,indexX,counter.colcontact,counter.colcontactemail,$index)" title="Edit" class="but btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="counter.col = '';counter.colcontact='';counter.colcontactemail='';delcolumn(count.column,indexX,$index)" title="Delete" class="but btn btn-warning"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'NEWS'" ng-model="counter.colnewslimit" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'NEWS'" ng-click="editnewsmodule(3,indexX,count.col1viewed,count.col1popular,counter.colnewslimit,$index)" title="Edit" class="but btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'NEWS'" ng-click="counter.col = '';counter.colnewslimit='';delcolumn(count.column,indexX,$index)" title="Delete" class="but btn btn-warning"><i class="fa fa-trash-o"></i></button>

                      <!-- DIVIDER MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colheight" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colcolor" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'DIVIDER'" ng-click="editdividermodule(3,indexX,counter.colheight,counter.colcolor,$index)" title="Edit" class="but btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="counter.col == 'DIVIDER'" ng-click="counter.col = '';counter.colheight='';counter.colcolor='';delcolumn(count.column,indexX,$index)" title="Delete" class="but btn btn-warning"><i class="fa fa-trash-o"></i></button>
                  </div>


                  <div class="col-sm-12 col-container" ng-if="$index == 2">
                  <a type="button" type="button" class="mainbut btn btn btn-default">{[{counter.col}]}</a>

                     <!-- TEXT MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'TEXT'" ng-model="counter.colcontent" style="color:black" required></textarea>
                    <button type="button" type="button" ng-if="counter.col == 'TEXT'" ng-click="edittextmodule(3,indexX,counter.colcontent,$index)" title="Edit" class="but btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="counter.col == 'TEXT'" ng-click="counter.col = '';counter.colcontent='';delcolumn(count.column,indexX,$index)" title="Delete" class="but btn btn-warning"><i class="fa fa-trash-o"></i></button>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimg" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimglink" style="color:black"></textarea>
                    <button type="button" type="button" ng-if="counter.col == 'IMAGE'" ng-click="editimagemodule(3,indexX,counter.colimg,$index,counter.colimglink)" title="Edit" class="but btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="counter.col == 'IMAGE'" ng-click="counter.col = '';counter.colimg='';counter.collink='';delcolumn(count.column,indexX,$index)" title="Delete" class="but btn btn-warning"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonial" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonialcat" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="edittestimonialmodule(3,indexX,counter.coltestimonial,counter.coltestimonialcat,$index)" title="Edit" class="but btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="counter.col = '';counter.coltestimonial='';counter.coltestimonialcat='';delcolumn(count.column,indexX,$index)" title="Delete" class="but btn btn-warning"><i class="fa fa-trash-o"></i></button>

                       <!-- CONTACT FORM MODULE -->
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="editcontactmodule(3,indexX,counter.colcontact,counter.colcontactemail,$index)" title="Edit" class="but btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="counter.col = '';counter.colcontact='';counter.colcontactemail='';delcolumn(count.column,indexX,$index)" title="Delete" class="but btn btn-warning"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'NEWS'" ng-model="counter.colnewslimit" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'NEWS'" ng-click="editnewsmodule(3,indexX,count.col1viewed,count.col1popular,counter.colnewslimit,$index)" title="Edit" class="but btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'NEWS'" ng-click="counter.col = '';counter.colnewslimit='';delcolumn(count.column,indexX,$index)" title="Delete" class="but btn btn-warning"><i class="fa fa-trash-o"></i></button>

                      <!-- DIVIDER MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colheight" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colcolor" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'DIVIDER'" ng-click="editdividermodule(3,indexX,counter.colheight,counter.colcolor,$index)" title="Edit" class="but btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="counter.col == 'DIVIDER'" ng-click="counter.col = '';counter.colheight='';counter.colcolor='';delcolumn(count.column,indexX,$index)" title="Delete" class="but btn btn-warning"><i class="fa fa-trash-o"></i></button>
                  </div>
                  </li>
                  </ul>
                </div>
                </div>

                <div class="clear specialpage_pad" ng-if="count.column == 4">
                  <input type="hidden" ng-model="count.colcount" style="color:black" required>
                  
                  <script type="text/javascript">
                    var $horizontal = $(".horizontal");
                    $horizontal.sortable({
                      connectWith: ".connectedSortable"
                    });
                  </script>

<div id="horizontal" class="connectedSortable col-sm-12 horizontal">
    <ul class="list-group no-radius sortrow" ui:sortable ng:model="count.row">
                    <li class="col-sm-3 no-pad" ng-repeat="(indexY,counter) in count.row track by indexY">

                  <div class="col-sm-12 input-group col-container" ng-if="$index == 0">
                    <a type="button" type="button" class="mainbut2 btn btn btn-default">{[{ counter.col }]}</a>

                    <!-- TEXT MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'TEXT'" ng-model="counter.colcontent" style="color:black" required></textarea>
                    <button type="button" type="button" ng-if="counter.col == 'TEXT'" ng-click="edittextmodule(4,indexX,counter.colcontent,$index)" title="Edit" class="col-sm-2 btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="counter.col == 'TEXT'" ng-click="counter.col = '';counter.colcontent='';delcolumn(count.column,indexX,$index)" title="Delete" class="but col-sm-2 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimg" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimglink" style="color:black"></textarea>
                    <button type="button" type="button" ng-if="counter.col == 'IMAGE'" ng-click="editimagemodule(4,indexX,counter.colimg,$index,counter.colimglink)" title="Edit" class=" col-sm-2 btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="counter.col == 'IMAGE'" ng-click="counter.col = '';counter.colimg='';counter.collink='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-2 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonial" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonialcat" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="edittestimonialmodule(4,indexX,counter.coltestimonial,counter.coltestimonialcat,$index)" title="Edit" class="col-sm-2 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="counter.col = '';counter.coltestimonial='';counter.coltestimonialcat='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-2 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                       <!-- CONTACT FORM MODULE -->
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="editcontactmodule(4,indexX,counter.colcontact,counter.colcontactemail,$index)" title="Edit" class="col-sm-2 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="counter.col = '';counter.colcontact='';counter.colcontactemail='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-2 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'NEWS'" ng-model="counter.colnewslimit" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'NEWS'" ng-click="editnewsmodule(4,indexX,count.col1viewed,count.col1popular,counter.colnewslimit,$index)" title="Edit" class="col-sm-2 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'NEWS'" ng-click="counter.col = '';counter.colnewslimit='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-2 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                      <!-- DIVIDER MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colheight" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colcolor" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'DIVIDER'" ng-click="editdividermodule(4,indexX,counter.colheight,counter.colcolor,$index)" title="Edit" class="col-sm-2 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="counter.col == 'DIVIDER'" ng-click="counter.col = '';counter.colheight='';counter.colcolor='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-2 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                    </div>


                  <div class="col-sm-12 col-container" ng-if="$index == 1">
                    <a type="button" type="button" class="mainbut2 btn btn-default">{[{ counter.col}]}</a>

                       <!-- TEXT MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'TEXT'" ng-model="counter.colcontent" style="color:black" required></textarea>
                    <button type="button" type="button" ng-if="counter.col == 'TEXT'" ng-click="edittextmodule(4,indexX,counter.colcontent,$index)" title="Edit" class="col-sm-2 btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="counter.col == 'TEXT'" ng-click="counter.col = '';counter.colcontent='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-2 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimg" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimglink" style="color:black"></textarea>
                    <button type="button" type="button" ng-if="counter.col == 'IMAGE'" ng-click="editimagemodule(4,indexX,counter.colimg,$index,counter.colimglink)" title="Edit" class="col-sm-2 btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="counter.col == 'IMAGE'" ng-click="counter.col = '';counter.colimg='';counter.collink='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-2 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonial" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonialcat" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="edittestimonialmodule(4,indexX,counter.coltestimonial,counter.coltestimonialcat,$index)" title="Edit" class="col-sm-2 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="counter.col = '';counter.coltestimonial='';counter.coltestimonialcat='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-2 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                       <!-- CONTACT FORM MODULE -->
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="editcontactmodule(4,indexX,counter.colcontact,counter.colcontactemail,$index)" title="Edit" class="col-sm-2 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="counter.col = '';counter.colcontact='';counter.colcontactemail='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-2 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'NEWS'" ng-model="counter.colnewslimit" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'NEWS'" ng-click="editnewsmodule(4,indexX,count.col1viewed,count.col1popular,counter.colnewslimit,$index)" title="Edit" class="col-sm-2 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'NEWS'" ng-click="counter.col = '';counter.colnewslimit='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-2 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                      <!-- DIVIDER MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colheight" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colcolor" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'DIVIDER'" ng-click="editdividermodule(4,indexX,counter.colheight,counter.colcolor,$index)" title="Edit" class="col-sm-2 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="counter.col == 'DIVIDER'" ng-click="counter.col = '';counter.colheight='';counter.colcolor='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-2 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                  </div>
                  
             

                  <div class="col-sm-12 col-container" ng-if="$index == 2">
                    <a type="button" type="button" class="mainbut2 btn btn btn-default">{[{ counter.col }]}</a>

                        <!-- TEXT MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'TEXT'" ng-model="counter.colcontent" style="color:black" required></textarea>
                    <button type="button" type="button" ng-if="counter.col == 'TEXT'" ng-click="edittextmodule(4,indexX,counter.colcontent,$index)" title="Edit" class="col-sm-2 btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="counter.col == 'TEXT'" ng-click="counter.col = '';counter.colcontent='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-2 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimg" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimglink" style="color:black"></textarea>
                    <button type="button" type="button" ng-if="counter.col == 'IMAGE'" ng-click="editimagemodule(4,indexX,counter.colimg,$index,counter.colimglink)" title="Edit" class="col-sm-2 btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="counter.col == 'IMAGE'" ng-click="counter.col = '';counter.colimg='';counter.collink='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-2 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonial" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonialcat" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="edittestimonialmodule(4,indexX,counter.coltestimonial,counter.coltestimonialcat,$index)" title="Edit" class="col-sm-2 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="counter.col = '';counter.coltestimonial='';counter.coltestimonialcat='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-2 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                       <!-- CONTACT FORM MODULE -->
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="editcontactmodule(4,indexX,counter.colcontact,counter.colcontactemail,$index)" title="Edit" class="col-sm-2 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="counter.col = '';counter.colcontact='';counter.colcontactemail='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-2 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'NEWS'" ng-model="counter.colnewslimit" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'NEWS'" ng-click="editnewsmodule(4,indexX,count.col1viewed,count.col1popular,counter.colnewslimit,$index)" title="Edit" class="col-sm-2 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'NEWS'" ng-click="counter.col = '';counter.colnewslimit='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-2 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                      <!-- DIVIDER MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colheight" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colcolor" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'DIVIDER'" ng-click="editdividermodule(4,indexX,counter.colheight,counter.colcolor,$index)" title="Edit" class="col-sm-2 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="counter.col == 'DIVIDER'" ng-click="counter.col = '';counter.colheight='';counter.colcolor='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-2 btn btn-warning"><i class="fa fa-trash-o"></i></button>
                  </div>
            

                  <div class="col-sm-12 col-container" ng-if="$index == 3">
                    <a type="button" type="button" class="mainbut2 btn btn-default">{[{counter.col}]}</a>

                       <!-- TEXT MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'TEXT'" ng-model="counter.colcontent" style="color:black" required></textarea>
                    <button type="button" type="button" ng-if="counter.col == 'TEXT'" ng-click="edittextmodule(4,indexX,counter.colcontent,$index)" title="Edit" class="col-sm-2 btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="counter.col == 'TEXT'" ng-click="counter.col = '';counter.colcontent='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-2 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                    <!-- IMAGE MODULE -->
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimg" style="color:black" required></textarea>
                    <textarea style="display:none;" ng-if="counter.col == 'IMAGE'" ng-model="counter.colimglink" style="color:black"></textarea>
                    <button type="button" type="button" ng-if="counter.col == 'IMAGE'" ng-click="editimagemodule(4,indexX,counter.colimg,$index,counter.colimglink)" title="Edit" class="col-sm-2 btn btn-primary"><i class="fa fa-edit"></i></button>
                    <button type="button" type="button" ng-if="counter.col == 'IMAGE'" ng-click="counter.col = '';counter.colimg='';counter.collink='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-2 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonial" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'TESTIMONIAL'" ng-model="counter.coltestimonialcat" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="edittestimonialmodule(4,indexX,counter.coltestimonial,counter.coltestimonialcat,$index)" title="Edit" class="col-sm-2 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'TESTIMONIAL'" ng-click="counter.col = '';counter.coltestimonial='';counter.coltestimonialcat='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-2 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                       <!-- CONTACT FORM MODULE -->
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="editcontactmodule(4,indexX,counter.colcontact,counter.colcontactemail,$index)" title="Edit" class="col-sm-2 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'CONTACT'" ng-click="counter.col = '';counter.colcontact='';counter.colcontactemail='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-2 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'NEWS'" ng-model="counter.colnewslimit" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'NEWS'" ng-click="editnewsmodule(4,indexX,count.col1viewed,count.col1popular,counter.colnewslimit,$index)" title="Edit" class="col-sm-2 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="counter.col == 'NEWS'" ng-click="counter.col = '';counter.colnewslimit='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-2 btn btn-warning"><i class="fa fa-trash-o"></i></button>

                      <!-- DIVIDER MODULE -->
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colheight" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="counter.col == 'DIVIDER'" ng-model="counter.colcolor" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="counter.col == 'DIVIDER'" ng-click="editdividermodule(4,indexX,counter.colheight,counter.colcolor,$index)" title="Edit" class="col-sm-2 btn btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="counter.col == 'DIVIDER'" ng-click="counter.col = '';counter.colheight='';counter.colcolor='';delcolumn(count.column,indexX,$index)" title="Delete" class="col-sm-2 btn btn-warning"><i class="fa fa-trash-o"></i></button>
                  </div>
                  </li>
                  </ul>
                </div>
                </div>
                <br>
                <!-- <span ng-if="countrow != 0 && $last" ng-click="rowdelitem($index)" class="pull-left" style="margin-right:10px"><a href="" title="REMOVE"><i class="fa fa-times fa-fw"></i></a></span> -->
           <!--    </div> -->
             </div>
             </li>
             </ul>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Meta Information <span style="color:red"> * </span>
            </div>
            <div class="panel-body">
              Title
              <input type="text" id="metatitle" name="metatitle" class="form-control" ng-model="page.metatitle" required="required" placeholder="Meta Title">
              <div class="line line-dashed b-b line-lg"></div>
              Description
              <input input="text"  id="metadesc" name="metadesc"  class="form-control" ng-model="page.metadesc" required="required" placeholder="Meta Description">
              <div class="line line-dashed b-b line-lg"></div>
              Keyword
              <input type="text" id="metatags" name="metatags" class="form-control" ng-model="page.metatags" required="required" placeholder="Meta Keyword">
              <div class="line line-dashed b-b line-lg"></div>

              Status
                <div ng-init="page.status = true">  </div>
                  <label class="i-switch bg-info m-t-xs m-r">
                   <input type="checkbox" ng-model="page.status" ng-checked="checkedz" ng-init="checkedz=true">
                    <i></i>
                  </label>
                  <span ng-if="page.status == false" class="label bg-danger Services-checkbox-label">Deactivate</span>
              <span ng-if="page.status == true" class="label bg-success Services-checkbox-label">Active</span>
              </div>
          </div>
        </div>
                <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Sidebar
            </div>
            <div class="panel-body">
              <div class="form-group">
                <div class="col-sm-4">
                  <div class="checkbox">
                    <label class="i-checks">
                      <input ng-model="page.sidebarleft" value="left" type="checkbox" ng-true-value="true" ng-init="page.sidebarleft='false'" ng-change="sidebarleft = !sidebarleft" ng-click="removeleft()"><i></i>Left
                    </label>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="checkbox">
                    <label class="i-checks">
                      <input ng-model="page.sidebarright" value="right" type="checkbox" ng-true-value="true" ng-init="page.sidebarright='false'" ng-change="sidebarright = !sidebarright"
                      ng-click="removeright()"><i></i>Right
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default" ng-if="sidebarleft">
            <div class="panel-heading font-bold">
              Left Sidebar  Widget

              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtn" ng-click="leftCancel()"><i class="glyphicon glyphicon-ban-circle"></i> Cancel</button>


              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtn" ng-if="sidebar.left == 'Menu'" ng-disabled="!sidebar.leftmenu" ng-click="updateleftsidebar(currentEdit,sidebar)"><i class="glyphicon glyphicon-edit"></i>Update</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtn" ng-if="sidebar.left == 'Calendar' || sidebar.left == 'Archive' || sidebar.left == 'Rss'" ng-click="updateleftsidebar(currentEdit,sidebar)"><i class="glyphicon glyphicon-edit"></i>Update</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtn" ng-if="sidebar.left == 'Image'" ng-disabled="!sidebar.leftimg" ng-click="updateleftsidebar(currentEdit,sidebar)"><i class="glyphicon glyphicon-edit"></i>Update</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtn" ng-if="sidebar.left == 'Testimonial'" ng-disabled="!sidebar.lefttestimoniallimit" ng-click="updateleftsidebar(currentEdit,sidebar)"><i class="glyphicon glyphicon-edit"></i>Update</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtn" ng-if="sidebar.left == 'Link'" ng-disabled="!sidebar.leftlinklabel || !sidebar.leftlinkurl" ng-click="updateleftsidebar(currentEdit,sidebar)"><i class="glyphicon glyphicon-edit"></i>Update</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtn" ng-if="sidebar.left == 'News'" ng-disabled="!sidebar.leftnewslimit" ng-click="updateleftsidebar(currentEdit,sidebar)"><i class="glyphicon glyphicon-edit"></i>Update</button>



              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtn" ng-if="sidebar.left == 'Menu'" ng-disabled="!sidebar.leftmenu" ng-click="addleftsidebar(sidebar)"><i class="glyphicon  glyphicon-plus-sign "></i>Add</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtn" ng-if="sidebar.left == 'Calendar' || sidebar.left == 'Archive' || sidebar.left == 'Rss'" ng-click="addleftsidebar(sidebar)"><i class="glyphicon  glyphicon-plus-sign "></i>Add</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtn" ng-if="sidebar.left == 'Image'" ng-disabled="!sidebar.leftimg" ng-click="addleftsidebar(sidebar)"></i>Add</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtn" ng-if="sidebar.left == 'Testimonial'" ng-disabled="!sidebar.lefttestimoniallimit" ng-click="addleftsidebar(sidebar)"><i class="glyphicon  glyphicon-plus-sign "></i>Add</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtn" ng-if="sidebar.left == 'Link'" ng-disabled="!sidebar.leftlinklabel || !sidebar.leftlinkurl" ng-click="addleftsidebar(sidebar)"><i class="glyphicon  glyphicon-plus-sign "></i>Add</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtn" ng-if="sidebar.left == 'News'" ng-disabled="!sidebar.leftnewslimit" ng-click="addleftsidebar(sidebar)"><i class="glyphicon  glyphicon-plus-sign "></i>Add</button>
            </div>
              <div class="panel-body">
                <div class="col-sm-12">
                  <div class="">
                    <select class="form-control col-sm-12" name="leftsidebar" ng-model="sidebar.left" style="margin-top:10px;">
                      <option value="" >-Select-</option>
                      <option value="Menu">Menu</option>
                      <option value="Calendar">Calendar</option>
                      <option value="Image">Image</option>
                      <option value="Testimonial">Testimonial</option>
                      <option value="Link">Link</option>
                      <option value="News">News</option>
                      <option value="Archive">Archive</option>
                      <option value="Rss">RSS</option>
                    </select>
                  </div>
                  <div style="padding-top:0px !important;" class="bg-light" ng-if="sidebar.left =='Image'">
                    <div class="line line-dashed b-b line-lg"></div>
                    &nbsp;&nbsp;<i class="icon-arrow-right"></i> Image
                    <a class="btn btn-default btn-xs pull-right" ng-click="media('featured','banner','left')"><i class="fa  fa-folder-open"></i> Media Library</a>
                    <div class="">
                      <img ng-if="sidebar.leftimg" src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{ sidebar.leftimg }]}" style="width: 100%">
                      <input type="text" class="form-control" ng-model="sidebar.leftimglink" placeholder="Image-Link(optional)">
                    </div>
                  </div>

                  <div style="padding-top:0px !important;" class="bg-light" ng-if="sidebar.left =='Testimonial'">
                    <div class="line line-dashed b-b line-lg"></div>
                    &nbsp;&nbsp;<i class="icon-arrow-right"></i> Testimonial Limit List View
                    <div class="">
                      <div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input ng-model="sidebar.lefttestimoniallimit" ui-jq="TouchSpin" type="text" value="10" class="form-control" data-min="0" data-max="20" data-verticalbuttons="true" data-verticalupclass="fa fa-caret-up" data-verticaldownclass="fa fa-caret-down" style="display: block; " class="form-control ng-pristine ng-invalid ng-invalid-required" required><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span></div>
                    </div>
                  </div>
                   <div style="padding-top:0px !important;" class="wrapper bg-light" ng-if="sidebar.left == 'Link'">
                    <div class="line line-dashed b-b line-lg"></div>
                    &nbsp;&nbsp;<i class="icon-arrow-right"></i> Link
                    <div class="">
                      <input type="text" ng-model="sidebar.leftlinklabel" class="form-control ng-pristine ng-invalid ng-invalid-required" placeholder="Label"  required=""><br>
                      <input type="text" ng-model="sidebar.leftlinkurl"  class="form-control" placeholder="http://"  required="">
                    </div>
                  </div>

                   <div style="padding-top:0px !important;" class="wrapper bg-light" ng-if="sidebar.left == 'Menu'">
                    <div class="line line-dashed b-b line-lg"></div>
                    &nbsp;&nbsp;<i class="icon-arrow-right"></i> Menu
                    <div class="">
                     <select class="form-control col-sm-8" required="required" ng-model="sidebar.leftmenu"  style="margin-top:10px;width:60%" >
                      <option value="" style="display:none">-Select-</option>
                      <option ng-selected="{[{ x.menuID ==  sidebar.leftmenu }]}" ng-repeat='x in menu' value='{[{x.menuID}]}'> {[{x.name}]} </option>
                    </select>
                    </div><br><br>
                  </div>

                  <div style="padding-top:0px !important;" class="wrapper bg-light" ng-if="sidebar.left =='News'">
                    <div class="line line-dashed b-b line-lg"></div>
                    &nbsp;&nbsp;<i class="icon-arrow-right"></i> News
                    <div class="">
                     <!--  <br>
                      <label class="i-checks">
                        <input type="checkbox" ng-model="page.leftnewspopular[$index]" required="" class="ng-dirty ng-invalid ng-invalid-required"><i></i> Most Papular
                      </label>
                       &nbsp;&nbsp;&nbsp;
                       <label class="i-checks">
                        <input type="checkbox" ng-model="page.leftnewsviews[$index]" required="" class="ng-dirty ng-invalid ng-invalid-required"><i></i> Most Viewed
                      </label> -->
                      <br>
                      <div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input ng-model="sidebar.leftnewslimit" ui-jq="TouchSpin" type="text" value="10" class="form-control" data-min="0" data-max="20" data-verticalbuttons="true" data-verticalupclass="fa fa-caret-up" data-verticaldownclass="fa fa-caret-down" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span></div>
                    </div>
                  </div>

              <div class="line line-dashed b-b line-lg"></div>
               <div class="col-sm-12">
                 <!-- <div ui-jq="nestable" class="dd" id="nestable2"> -->
                    <ul class="list-group no-radius sortable" ui:sortable ng:model="page.sidedataL" >
                      <li style="border:1px solid #ccc;cursor:pointer" class="list-group-item" ng-repeat="leftdata in page.sidedataL track by $index" ng-hide="currentEdit == $index" data-sidebar="{[{page.sidebar[$index]}]}" data-img=" {[{page.sidedataL[$index].img}]}" data-imglink="{[{page.sidedataL[$index].imglink}]}" data-limittest="{[{page.sidedataL[$index].limittest}]}" data-url="{[{page.sidedataL[$index].url}]}" data-label="{[{page.sidedataL[$index].label}]}" data-menuID="{[{page.sidedataL[$index].menu}]}" data-limitnews="{[{page.sidedataL[$index].limitnews}]}">

                  
                        <span style="padding-left:37px">
                           {[{ leftdata.sidebar }]}
                           <span class="pull-right"><a href="" ng-click="deleteleft($index)"><i class="fa fa-times-circle"></i></a></span>
                           <span class="pull-right"><a href="" ng-click="editleft($index)"><i class="fa fa-pencil-square"></i></a></span>
                        </span>
                      </li>
                  </ul>
              <!--     </div> -->
                </div>

            </div>
          </div>
          </div>

          <div class="panel panel-default" ng-if="sidebarright">
            <div class="panel-heading font-bold">
              Right Sidebar  Widget

              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtnright" ng-click="rightCancel()"><i class="glyphicon glyphicon-ban-circle"></i> Cancel</button>


              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtnright" ng-if="sidebarR.right == 'Menu'" ng-disabled="!sidebarR.rightmenu" ng-click="updaterightsidebar(currentEditright,sidebarR)"><i class="glyphicon glyphicon-edit"></i>Update</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtnright" ng-if="sidebarR.right == 'Calendar' || sidebarR.right == 'Archive' || sidebarR.right == 'Rss'" ng-click="updaterightsidebar(currentEditright,sidebarR)"><i class="glyphicon glyphicon-edit"></i>Update</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtnright" ng-if="sidebarR.right == 'Image'" ng-disabled="!sidebarR.rightimg" ng-click="updaterightsidebar(currentEditright,sidebarR)"><i class="glyphicon glyphicon-edit"></i>Update</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtnright" ng-if="sidebarR.right == 'Testimonial'" ng-disabled="!sidebarR.righttestimoniallimit" ng-click="updaterightsidebar(currentEditright,sidebarR)"><i class="glyphicon glyphicon-edit"></i>Update</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtnright" ng-if="sidebarR.right == 'Link'" ng-disabled="!sidebarR.rightlinklabel || !sidebarR.rightlinkurl" ng-click="updaterightsidebar(currentEditright,sidebarR)"><i class="glyphicon glyphicon-edit"></i>Update</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="!addbtnright" ng-if="sidebarR.right == 'News'" ng-disabled="!sidebarR.rightnewslimit" ng-click="updaterightsidebar(currentEditright,sidebarR)"><i class="glyphicon glyphicon-edit"></i>Update</button>



              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtnright" ng-if="sidebarR.right == 'Menu'" ng-disabled="!sidebarR.rightmenu" ng-click="addrightsidebar(sidebarR)"><i class="glyphicon  glyphicon-plus-sign "></i>Add</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtnright" ng-if="sidebarR.right == 'Calendar' || sidebarR.right == 'Archive' || sidebarR.right == 'Rss'" ng-click="addrightsidebar(sidebarR)"><i class="glyphicon  glyphicon-plus-sign "></i>Add</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtnright" ng-if="sidebarR.right == 'Image'" ng-disabled="!sidebarR.rightimg" ng-click="addrightsidebar(sidebarR)"></i>Add</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtnright" ng-if="sidebarR.right == 'Testimonial'" ng-disabled="!sidebarR.righttestimoniallimit" ng-click="addrightsidebar(sidebarR)"><i class="glyphicon  glyphicon-plus-sign "></i>Add</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtnright" ng-if="sidebarR.right == 'Link'" ng-disabled="!sidebarR.rightlinklabel || !sidebarR.rightlinkurl" ng-click="addrightsidebar(sidebarR)"><i class="glyphicon  glyphicon-plus-sign "></i>Add</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-show="addbtnright" ng-if="sidebarR.right == 'News'" ng-disabled="!sidebarR.rightnewslimit" ng-click="addrightsidebar(sidebarR)"><i class="glyphicon  glyphicon-plus-sign "></i>Add</button>
            </div>
              <div class="panel-body">
                <div class="col-sm-12">
                  <div class="">
                    <select class="form-control col-sm-12" name="rightsidebar" ng-model="sidebarR.right" style="margin-top:10px;">
                      <option value="" >-Select-</option>
                      <option value="Menu">Menu</option>
                      <option value="Calendar">Calendar</option>
                      <option value="Image">Image</option>
                      <option value="Testimonial">Testimonial</option>
                      <option value="Link">Link</option>
                      <option value="News">News</option>
                      <option value="Archive">Archive</option>
                      <option value="Rss">RSS</option>
                    </select>
                  </div>
                  <div style="padding-top:0px !important;" class="bg-light" ng-if="sidebarR.right =='Image'">
                    <div class="line line-dashed b-b line-lg"></div>
                    &nbsp;&nbsp;<i class="icon-arrow-right"></i> Image
                    <a class="btn btn-default btn-xs pull-right" ng-click="media('featured','banner','right')"><i class="fa  fa-folder-open"></i> Media Library</a>
                    <div class="">
                      <img ng-if="sidebarR.rightimg" src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{ sidebarR.rightimg }]}" style="width: 100%">
                      <input type="text" class="form-control" ng-model="sidebarR.rightimglink" placeholder="Image-Link(optional)">
                    </div>
                  </div>

                  <div style="padding-top:0px !important;" class="bg-light" ng-if="sidebarR.right =='Testimonial'">
                    <div class="line line-dashed b-b line-lg"></div>
                    &nbsp;&nbsp;<i class="icon-arrow-right"></i> Testimonial Limit List View
                    <div class="">
                      <div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input ng-model="sidebarR.righttestimoniallimit" ui-jq="TouchSpin" type="text" value="10" class="form-control" data-min="0" data-max="20" data-verticalbuttons="true" data-verticalupclass="fa fa-caret-up" data-verticaldownclass="fa fa-caret-down" style="display: block; " class="form-control ng-pristine ng-invalid ng-invalid-required" required><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span></div>
                    </div>
                  </div>
                   <div style="padding-top:0px !important;" class="wrapper bg-light" ng-if="sidebarR.right == 'Link'">
                    <div class="line line-dashed b-b line-lg"></div>
                    &nbsp;&nbsp;<i class="icon-arrow-right"></i> Link
                    <div class="">
                      <input type="text" ng-model="sidebarR.rightlinklabel" class="form-control ng-pristine ng-invalid ng-invalid-required" placeholder="Label"  required=""><br>
                      <input type="text" ng-model="sidebarR.rightlinkurl"  class="form-control" placeholder="http://"  required="">
                    </div>
                  </div>

                   <div style="padding-top:0px !important;" class="wrapper bg-light" ng-if="sidebarR.right == 'Menu'">
                    <div class="line line-dashed b-b line-lg"></div>
                    &nbsp;&nbsp;<i class="icon-arrow-right"></i> Menu
                    <div class="">
                     <select class="form-control col-sm-8" required="required" ng-model="sidebarR.rightmenu"  style="margin-top:10px;width:60%" >
                      <option value="" style="display:none">-Select-</option>
                      <option ng-selected="{[{ x.menuID == sidebarR.rightmenu }]}" ng-repeat='x in menu' value='{[{x.menuID}]}'> {[{x.name}]} </option>
                    </select>
                    </div><br><br>
                  </div>

                  <div style="padding-top:0px !important;" class="wrapper bg-light" ng-if="sidebarR.right =='News'">
                    <div class="line line-dashed b-b line-lg"></div>
                    &nbsp;&nbsp;<i class="icon-arrow-right"></i> News
                    <div class="">
                     <!--  <br>
                      <label class="i-checks">
                        <input type="checkbox" ng-model="page.leftnewspopular[$index]" required="" class="ng-dirty ng-invalid ng-invalid-required"><i></i> Most Papular
                      </label>
                       &nbsp;&nbsp;&nbsp;
                       <label class="i-checks">
                        <input type="checkbox" ng-model="page.leftnewsviews[$index]" required="" class="ng-dirty ng-invalid ng-invalid-required"><i></i> Most Viewed
                      </label> -->
                      <br>
                      <div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input ng-model="sidebarR.rightnewslimit" ui-jq="TouchSpin" type="text" value="10" class="form-control" data-min="0" data-max="20" data-verticalbuttons="true" data-verticalupclass="fa fa-caret-up" data-verticaldownclass="fa fa-caret-down" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span></div>
                    </div>
                  </div>
                  <div class="line line-dashed b-b line-lg"></div>
                </div>
                <div class="col-sm-12">
                 <!-- <div ui-jq="nestable" class="dd" id="nestable2"> -->
                    <ul class="list-group no-radius sortable" ui:sortable ng:model="page.sidedataR" >
                      <li style="border:1px solid #ccc;cursor:pointer" class="list-group-item" ng-repeat="rightdata in page.sidedataR track by $index" ng-hide="currentEditright == $index" data-sidebar="{[{page.sidebar[$index]}]}" data-img=" {[{page.sidedataR[$index].img}]}" data-imglink="{[{page.sidedataR[$index].imglink}]}" data-limittest="{[{page.sidedataR[$index].limittest}]}" data-url="{[{page.sidedataR[$index].url}]}" data-label="{[{page.sidedataR[$index].label}]}" data-menuID="{[{page.sidedataR[$index].menu}]}" data-limitnews="{[{page.sidedataR[$index].limitnews}]}">

                  
                        <span style="padding-left:37px">
                           {[{ rightdata.sidebar }]}
                           <span class="pull-right"><a href="" ng-click="deleteright($index)"><i class="fa fa-times-circle"></i></a></span>
                           <span class="pull-right"><a href="" ng-click="editright($index)"><i class="fa fa-pencil-square"></i></a></span>
                        </span>
                      </li>
                  </ul>
              <!--     </div> -->
                </div>
              </div>
          </div>

          <input type="hidden" class="form-control" ng-model="stat">

           <div class="panel panel-success bg-gray lter">
            <div class="panel-heading font-bold">
              Page Banner
              <label class="pull-right i-switch bg-info m-t-xs m-r" ng-init="page.banner = false">
               <input type="checkbox" ng-model="page.banner" ng-checked="checked" ng-init="checked=false">
               <i></i>
             </label>
            </div>
            <div class="panel-body" ng-show="page.banner">
               <a class="btn btn-default btn-xs pull-right" ng-click="media('featured','thumb')"><i class="fa  fa-folder-open"></i> Media Library</a>
                    <label>Banner Image is required<br><em class="text-muted">Click Media Library to add image or select an existing image. Recommended image size: a ratio of 704px in height.</em></label>
                    <img ng-show="page.imagethumb" src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{ page.imagethumb }]}" style="width: 100%">
                    <div class="line line-dashed b-b line-lg"></div>
                    <input type="hidden" ng-model="page.imagethumb" ng-required="page.banner">
                    Title (maxlength of 60)<em>(Optional)</em>
                    <input type="text" id="imagethumbsubtitle" name="imagethumbsubtitle" class="form-control" ng-model="page.imagethumbsubtitle" maxlength="60">
                    <div class="line line-dashed b-b line-lg"></div>

                    Short Description (maxlength of 300)<em>(Optional)</em>
                    <textarea type="text" id="thumbdesc" name="thumbdesc" class="form-control resize-v" ng-model="page.thumbdesc" maxlength="300" rows="4"> </textarea>
                    <div class="line line-dashed line-lg b-b"></div>

                    <div class="form-group" ng-init="page.align = 'center' ">
                      <div class="col-sm-3">Text Align</div>
                      <div class="col-sm-9 wrapper-sm">
                        <div class="btn-group">
                          <label class="but btn btn-primary" ng-model="page.align" btn-radio="'left'">Left</label>
                          <label class="but btn btn-primary" ng-model="page.align" btn-radio="'center'">Center</label>
                          <label class="but btn btn-primary" ng-model="page.align" btn-radio="'right'">Right</label>
                        </div>
                      </div>
                    </div>
                    <div class="line line-dashed b-b line-lg"></div>

                    <div class="form-group">
                      <div class="col-sm-3">Background Color</div>
                      <div class="col-sm-9 well bg-light wrapper-sm">
                        <input colorpicker ng-model="page.bgcolor" ng-init="page.bgcolor = '#ffffff' " type="text" class="form-control width90px pull-left pointer" readonly>
                        <div class="colorpicker-colorbox form-control pull-left" style="background-color: {[{ page.bgcolor }]}"></div>
                      </div>
                    </div>
                    <div class="line line-dashed line-lg b-b"></div>

                    <div class="form-group">
                      <div class="col-sm-3">Text Color</div>
                      <div class="col-sm-9 well bg-light wrapper-sm">
                        <input colorpicker ng-model="page.color" type="text" ng-init="page.color = '#000000' " class="form-control ng-pristine ng-invalid ng-valid-pattern width90px pull-left pointer" readonly>
                        <div class="colorpicker-colorbox form-control pull-left" style="background-color: {[{ page.color }]}"></div>
                      </div>
                    </div>

                    <div class="line line-dashed b-b line-lg"></div>

                    <div class="form-group">
                      <div class="col-sm-3">White Box</div>
                      <div class="col-sm-9 well bg-light wrapper-sm" ng-init="page.box = false ">
                        <label class="i-switch bg-info m-t-xs m-r">
                          <input type="checkbox" ng-model="page.box">
                          <i></i>
                        </label>
                        <span ng-if="page.box == false" class="label bg-danger Services-checkbox-label">off</span>
                        <span ng-if="page.box == true" class="label bg-success Services-checkbox-label">on</span>
                      </div>
                    </div>
                    <div class="line line-dashed line-lg b-b"></div>
                    <div class="font-bold">Button
                      <em>(Optional)</em></div>
                    <div class="line line-dashed line-lg b-b"></div>
                    <div class="form-group hiddenoverflow">
                      <label class="col-sm-2 control-label">Button Name</label>
                      <div class="col-sm-10">
                        <input type="text" name="" ng-space class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.btnname">
                      </div>
                    </div>
                    <div class="line line-dashed b-b line-lg"></div>

                    <div class="form-group hiddenoverflow">
                      <label class="col-sm-2 control-label">Button Link</label>
                      <div class="col-sm-10">
                        <input type="text" name="" ng-space class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.btnlink" placeholder="e.g.: http://www">
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-sm-12">
                        <tabset class="tab-container tab-danger">
                          <tab>
                             <tab-heading class="font-bold">Note</tab-heading>
                             Please click <a class="btn m-b-xs btn-xs btn-success" ng-click="stylepreview(page)">Quick Preview</a>
                             before submitting so you can check whether the title and description you inserted is too long. Thank you.
                          </tab>
                        </tabset>
                      </div>
                    </div>
            </div>
           </div>


        </div>
        <div class="col-sm-12">
          <div class="panel-body">
            <footer class="panel-footer text-right bg-light lter">
              <a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
              <button type="submit" class="btn btn-success" ng-disabled="formpage.$invalid || validslugs">Submit</button>
            </footer>
          </div>
        </div>
      </div>
    </fieldset>
  </form>
