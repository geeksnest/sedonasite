<div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <!-- main header -->
    <div class="bg-light lter b-b wrapper-md">
      <div class="row">
        <div class="col-sm-6 col-xs-12">
          <h1 class="m-n font-thin h3 text-black">Dashboard</h1>
          <small class="text-muted">Sedona Content Management System</small>
        </div>
      <!--   <div class="col-sm-6 text-right hidden-xs">
          <div class="inline m-r text-left">
            <div class="m-b-xs"><span class="text-muted">Comming soon.</span></div>
            <div ng-init="data1=[ 106,108,110,105,110,109,105,104,107,109,105,100,105,102,101,99,98 ]"
                 ui-jq="sparkline"
                 ui-options="{[{ data1 }]}, {type:'bar', height:20, barWidth:5, barSpacing:1, barColor:'#dce5ec'}"
                 class="sparkline inline">loading...
            </div>
          </div>
          <div class="inline text-left">
            <div class="m-b-xs"> <span class="text-muted">Comming soon.</span></div>
            <div ng-init="data2=[ 105,102,106,107,105,104,101,99,98,109,105,100,108,110,105,110,109 ]"
                 ui-jq="sparkline"
                 ui-options="{[{data2}]}, {type:'bar', height:20, barWidth:5, barSpacing:1, barColor:'#dce5ec'}"
                 class="sparkline inline">loading...
            </div>
          </div>
        </div> -->
      </div>

    </div>

    <div class='row'>
      <div class='col-sm-12'>
        <!-- <div class="panel panel-default"> -->
          <div class='panel panel-body'>
            <div style='font-size:16px' class="font-bold"> TOTAL NEWS: <b> <span ng-bind='totalnews'></span> </b> </div>

          </div>
       
      <!--   </div> -->
      </div>

      </div>

     <div class='row'>
      <div class='col-sm-6'>
        <div class="panel panel-default">
        <div class='panel-heading font-bold'> Total: <span ng-bind='schedule'></span> </div>
          <div class='panel panel-body'>
             <highchart config="DailySchedule"></highchart>
             <div style='text-align:center;color:red'> <span ng-show='norecords'> No Schedule for </span> <span ng-bind='date'> </span> </div>
          </div>
       
        </div>

      </div>


      <div class='col-sm-6'>
        <div class="panel panel-default">
         <div class='panel-heading font-bold'>Total:  <span ng-bind='reservation'></span> </div>
          <div class='panel panel-body'>
             <highchart config="DailyReservation"></highchart>
             <div style='text-align:center;color:red'> <span ng-show='norecords2'> No Reservation for </span> <span ng-bind='date2'> </span> </div>
          </div>
       
        </div>
      </div>

      </div>

      <div class="row">
        <div class='col-sm-12'>
          <div class="panel panel-default">
            <div class='panel panel-body'>
              <div class="panel panel-default">
               <div class="panel-heading font-bold">                  
                TOTAL NUMBER OF RESERVATION:  <span ng-bind='totalReservation'> </span> Reservations
              </div>
              <highchart config="MonthlyReservation"></highchart>
            </div>
          </div>

        </div>
      </div>

    </div>


</div>

</div>