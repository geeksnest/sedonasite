<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="mediagallery.html">
  <div ng-include="'/be/tpl/pagebuilder/mediagallery.html'"></div>
</script>
<script type="text/ng-template" id="column.html">
  <div ng-include="'/be/tpl/pagebuilder/column.html'"></div>
</script>
<script type="text/ng-template" id="module.html">
  <div ng-include="'/be/tpl/pagebuilder/module.html'"></div>
</script>
<script type="text/ng-template" id="moduletext.html">
  <div ng-include="'/be/tpl/pagebuilder/moduletext.html'"></div>
</script>
<script type="text/ng-template" id="moduleimage.html">
  <div ng-include="'/be/tpl/pagebuilder/moduleimage.html'"></div>
</script>
<script type="text/ng-template" id="moduletestimonial.html">
  <div ng-include="'/be/tpl/pagebuilder/moduletestimonial.html'"></div>
</script>
<script type="text/ng-template" id="modulecontact.html">
  <div ng-include="'/be/tpl/pagebuilder/modulecontact.html'"></div>
</script>
<script type="text/ng-template" id="modulenews.html">
  <div ng-include="'/be/tpl/pagebuilder/modulenews.html'"></div>
</script>
<script type="text/ng-template" id="moduledivider.html">
  <div ng-include="'/be/tpl/pagebuilder/moduledivider.html'"></div>
</script>
<script type="text/ng-template" id="rowcolor.html">
  <div ng-include="'/be/tpl/pagebuilder/rowcolor.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Footer Appearance</h1>
  <a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="savePage(page)" name="formpage">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="row">
        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Update Footer
            </div>
            <div class="panel-body">
              <div class="col-sm-12 bg-light special_page_row" ng-repeat="count in rowcount">
                <div class="clear specialpage_pad">
                    <button type="button" class="col-sm-11 btn m-b-xs btn-lg btn-default btn-addon" ng-click="insertcolumn($index)"><i class="fa fa-plus"></i>Select Column</button>
                    <input type="hidden" ng-model="page.rowcolor[$index]" style="color:black">
                    <input type="hidden" ng-model="page.rowfontcolor[$index]" style="color:black">
                    <button type="button" type="button" title="Edit" ng-click="rowcolor($index,page.rowcolor[$index],page.rowfontcolor[$index])" class="col-sm-1 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                </div>
                <div ng-if="!count.rowcolor" ng-init="page.rowcolor[$index]='#ffffff'"> </div>
                <div ng-if="count.rowcolor" ng-init="page.rowcolor[$index]=count.rowcolor"> </div>
                <div ng-if="!count.rowfontcolor" ng-init="page.rowfontcolor[$index]='#ffffff'"> </div>
                <div ng-if="count.rowfontcolor" ng-init="page.rowfontcolor[$index]=count.rowfontcolor"> </div>
                <div ng-if="count.columntype==1" ng-init="column[$index]=count.columntype"> </div>
                <div ng-if="count.columntype==2" ng-init="column[$index]=count.columntype"> </div>
                <div ng-if="count.columntype==3" ng-init="column[$index]=count.columntype"> </div>
                <div ng-if="count.columntype==4" ng-init="column[$index]=count.columntype"> </div>
                <div ng-init="page.col1[$index]=datacol[$index].module"> </div>
                <div ng-init="page.col2[$index]=datacol2[$index].module"> </div>
                <div ng-init="page.col3[$index]=datacol3[$index].module"> </div>
                <div ng-init="page.col4[$index]=datacol4[$index].module"> </div>
                <input type="hidden" ng-model="page.row[$index]" style="color:black">
                <div class="clear specialpage_pad" ng-if="column[$index] == 1">
                <div ng-if="!page.colcount[$index]" ng-init="page.colcount[$index]=count.columntype"></div>
                    <input type="hidden" ng-model="page.colcount[$index]" style="color:black" required>
                    <input type="hidden" ng-model="page.col1[$index]" style="color:black" required>
                    <button type="button" ng-if="!page.col1[$index]" ng-click="insertmodule('1',$index,1)" class="col-sm-12 btn m-b-xs btn-lg btn-default btn-addon bg-info"><i class="fa fa-plus"></i>Insert Module</button>
                   

                    <div class="input-group col-sm-12" ng-if="page.col1[$index]">
                      <button type="button" type="button" class="col-sm-10 btn btn-lg btn-default">{[{ page.col1[$index] }]}</button>

                      <!-- TEXT MODULE -->
                      <div ng-if="datacol[$index].content" ng-init="page.col1content[$index]=datacol[$index].content"> </div>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'TEXT'" ng-model="page.col1content[$index]" style="color:black" required></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'TEXT'" ng-click="edittextmodule('1',$index,page.col1content[$index])" title="Edit" class="col-sm-1 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'TEXT'" ng-click="page.col1[$index] = '';page.col1content[$index]=''" title="Delete" class="col-sm-1 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- IMAGE MODULE -->
                  <div ng-if='datacol[$index].module' ng-init="page.col1img[$index]=datacol[$index].image"></div>
                  <div ng-if='datacol[$index].module' ng-init="page.col1link[$index]=datacol[$index].link"></div>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'IMAGE'" ng-model="page.col1img[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'IMAGE'" ng-model="page.col1link[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'IMAGE'" ng-click="editimagemodule('1',$index,page.col1img[$index],page.col1link[$index])" title="Edit" class="col-sm-1 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'IMAGE'" ng-click="page.col1[$index] = '';page.col1img[$index]='';page.col1link[$index]=''" title="Delete" class="col-sm-1 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>


                       <!-- TESTIMONIAL MODULE -->
                       <div ng-if='datacol[$index].limitcount' ng-init="page.col1testimonial[$index]=datacol[$index].limitcount"> </div>
                       <div ng-if='datacol[$index].category' ng-init="page.col1testimonialcat[$index]=datacol[$index].category"></div>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-model="page.col1testimonial[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-model="page.col1testimonialcat[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-click="edittestimonialmodule('1',$index,page.col1testimonial[$index],page.col1testimonialcat[$index])" title="Edit" class="col-sm-1 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-click="page.col1[$index] = '';page.col1testimonial[$index]='';page.col1testimonialcat[$index]=''" title="Delete" class="col-sm-1 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- CONTACT FORM MODULE -->
                        <div ng-if="datacol[$index].module" ng-init="page.col1contact[$index]=datacol[$index].title"> </div>
                       <div ng-if="datacol[$index].module" ng-init="page.col1contactemail[$index]=datacol[$index].email"> </div>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'CONTACT'" ng-model="page.col1contact[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'CONTACT'" ng-model="page.col1contactemail[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'CONTACT'" ng-click="editcontactmodule('1',$index,page.col1contact[$index],page.col1contactemail[$index])" title="Edit" class="col-sm-1 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'CONTACT'" ng-click="page.col1[$index] = '';page.col1contact[$index]='';page.col1contactemail[$index]=''" title="Delete" class="col-sm-1 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <div ng-if='datacol[$index].module' ng-init="page.col1viewed[$index]=datacol[$index].mostviewed"></div>
                       <div ng-if='datacol[$index].module' ng-init="page.col1popular[$index]=datacol[$index].popular"></div>
                       <div ng-if='datacol[$index].module' ng-init="page.col1newslimit[$index]=datacol[$index].limitcount"></div>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'NEWS'" ng-model="page.col1viewed[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'NEWS'" ng-model="page.col1popular[$index]" style="color:black"></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'NEWS'" ng-model="page.col1newslimit[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'NEWS'" ng-click="editnewsmodule('1',$index,page.col1viewed[$index],page.col1popular[$index],page.col1newslimit[$index])" title="Edit" class="col-sm-1 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'NEWS'" ng-click="page.col1[$index] = '';page.col1viewed[$index]='';page.col1newslimit[$index]='';page.col1popular[$index]=''" title="Delete" class="col-sm-1 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- DIVIDER MODULE -->
                       <div ng-if='datacol[$index].module' ng-init="page.col1height[$index]=datacol[$index].height"></div>
                       <div ng-if='datacol[$index].module' ng-init="page.col1color[$index]=datacol[$index].color"></div>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'DIVIDER'" ng-model="page.col1height[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'DIVIDER'" ng-model="page.col1color[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'DIVIDER'" ng-click="editdividermodule('1',$index,page.col1height[$index],page.col1color[$index])" title="Edit" class="col-sm-1 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'DIVIDER'" ng-click="page.col1[$index] = '';page.col1height[$index]='';page.col1color[$index]=''" title="Delete" class="col-sm-1 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                    </div>

                </div>
<!--database -->
   <div class="clear specialpage_pad" ng-if="count == 1">
      </div>
      <div class="clear specialpage_pad" ng-if="count == 2">
      </div> 
<!-- database end -->

        <!--first-->
                <div class="clear specialpage_pad" ng-if="column[$index] == 2">
                <div ng-if="!page.colcount[$index]" ng-init="page.colcount[$index]=count.columntype"></div>
                    <input type="hidden" ng-model="page.colcount[$index]" style="color:black" required>
                    <button type="button" ng-if="!page.col1[$index]" ng-click="insertmodule('1',$index)" class="col-sm-6 btn m-b-xs btn-lg btn-default btn-addon bg-info pull-left"><i class="fa fa-plus"></i>Insert Module</button>
                    <button type="button" ng-if="!page.col2[$index]" ng-click="insertmodule('2',$index)" class="col-sm-6 btn m-b-xs btn-lg btn-default btn-addon bg-info pull-right"><i class="fa fa-plus"></i>Insert Module</button>
                    <div class="input-group col-sm-6 pull-left" ng-if="page.col1[$index]">
                      <button type="button" type="button" class="col-sm-8 btn btn-lg btn-default">{[{ page.col1[$index] }]} </button>
                      <!-- TEXT MODULE -->
                       <div ng-if="datacol[$index].content" ng-init="page.col1content[$index]=datacol[$index].content"> </div>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'TEXT'" ng-model="page.col1content[$index]" style="color:black" required></textarea>
                      <button type="button" ng-if="page.col1[$index] == 'TEXT'" type="button" ng-click="edittextmodule('1',$index,page.col1content[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="page.col1[$index] == 'TEXT'" type="button" ng-click="page.col1[$index] = '';page.col1content[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- IMAGE MODULE -->
                      <div ng-if="datacol[$index].image" ng-init="page.col1img[$index]=datacol[$index].image"></div>
                      <div ng-if="datacol[$index].link" ng-init="page.col1link[$index]=datacol[$index].link"></div>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'IMAGE'" ng-model="page.col1img[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'IMAGE'" ng-model="page.col1link[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'IMAGE'" ng-click="editimagemodule('1',$index,page.col1img[$index],page.col1link[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'IMAGE'" ng-click="page.col1[$index] = '';page.col1img[$index]='';page.col1link[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                        <div ng-if="datacol[$index].category" ng-init="page.col1testimonialcat[$index]=datacol[$index].category"> </div>
                       <div ng-if="datacol[$index].limitcount" ng-init="page.col1testimonial[$index]=datacol[$index].limitcount"> </div>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-model="page.col1testimonial[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-model="page.col1testimonialcat[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-click="edittestimonialmodule('1',$index,page.col1testimonial[$index],page.col1testimonialcat[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-click="page.col1[$index] = '';page.col1testimonial[$index]='';page.col1testimonialcat[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>


                       <!-- CONTACT FORM MODULE -->
                       <div ng-if="datacol[$index].title" ng-init="page.col1contact[$index]=datacol[$index].title"> </div>
                       <div ng-if="datacol[$index].email" ng-init="page.col1contactemail[$index]=datacol[$index].email"> </div>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'CONTACT'" ng-model="page.col1contact[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'CONTACT'" ng-model="page.col1contactemail[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'CONTACT'" ng-click="editcontactmodule('1',$index,page.col1contact[$index],page.col1contactemail[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'CONTACT'" ng-click="page.col1[$index] = '';page.col1contact[$index]='';page.col1contactemail[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <div ng-if="datacol[$index].mostviewed" ng-init="page.col1viewed[$index]=datacol[$index].mostviewed"></div>
                       <div ng-if="datacol[$index].popular" ng-init="page.col1popular[$index]=datacol[$index].popular"></div>
                       <div ng-if='datacol[$index].limitcount' ng-init="page.col1newslimit[$index]=datacol[$index].limitcount"></div>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'NEWS'" ng-model="page.col1viewed[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'NEWS'" ng-model="page.col1popular[$index]" style="color:black"></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'NEWS'" ng-model="page.col1newslimit[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'NEWS'" ng-click="editnewsmodule('1',$index,page.col1viewed[$index],page.col1popular[$index],page.col1newslimit[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'NEWS'" ng-click="page.col1[$index] = '';page.col1viewed[$index]='';page.col1newslimit[$index]='';page.col1popular[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- DIVIDER MODULE -->
                        <div ng-if="datacol[$index].height" ng-init="page.col1height[$index]=datacol[$index].height"></div>
                       <div ng-if="datacol[$index].color" ng-init="page.col1color[$index]=datacol[$index].color"></div>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'DIVIDER'" ng-model="page.col1height[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'DIVIDER'" ng-model="page.col1color[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'DIVIDER'" ng-click="editdividermodule('1',$index,page.col1height[$index],page.col1color[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'DIVIDER'" ng-click="page.col1[$index] = '';page.col1height[$index]='';page.col1color[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                    </div>

                    <!--second-->
                    <div class="input-group col-sm-6 pull-right" ng-if="page.col2[$index]">
                      <button type="button" type="button" class="col-sm-8 btn btn-lg btn-default">{[{ page.col2[$index] }]}</button>

                      <!-- TEXT MODULE -->
                       <div ng-if="datacol2[$index].content" ng-init="page.col2content[$index]=datacol2[$index].content"> </div>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'TEXT'" ng-model="page.col2content[$index]" style="color:black" required></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'TEXT'" ng-click="edittextmodule('2',$index,page.col2content[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'TEXT'" ng-click="page.col2[$index] = '';page.col2content[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- IMAGE MODULE -->
                      <div ng-if='datacol2[$index].image' ng-init="page.col2img[$index]=datacol2[$index].image"></div>
                      <div ng-if='datacol2[$index].link' ng-init="page.col2link[$index]=datacol2[$index].link"></div>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'IMAGE'" ng-model="page.col2img[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'IMAGE'" ng-model="page.col2link[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'IMAGE'" ng-click="editimagemodule('2',$index,page.col2img[$index],page.col2link[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'IMAGE'" ng-click="page.col2[$index] = '';page.col2img[$index]='';page.col2link[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                        <div ng-if="datacol2[$index].category" ng-init="page.col2testimonialcat[$index]=datacol2[$index].category"> </div>
                       <div ng-if="datacol2[$index].limitcount" ng-init="page.col2testimonial[$index]=datacol2[$index].limitcount"> </div>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'TESTIMONIAL'" ng-model="page.col2testimonial[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'TESTIMONIAL'" ng-model="page.col2testimonialcat[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'TESTIMONIAL'" ng-click="edittestimonialmodule('2',$index,page.col2testimonial[$index],page.col2testimonialcat[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'TESTIMONIAL'" ng-click="page.col2[$index] = '';page.col2testimonial[$index]='';page.col2testimonialcat[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- CONTACT FORM MODULE -->
                       <div ng-if="datacol2[$index].title" ng-init="page.col2contact[$index]=datacol2[$index].title"> </div>
                       <div ng-if="datacol2[$index].email" ng-init="page.col2contactemail[$index]=datacol2[$index].email"> </div>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'CONTACT'" ng-model="page.col2contact[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'CONTACT'" ng-model="page.col2contactemail[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'CONTACT'" ng-click="editcontactmodule('2',$index,page.col2contact[$index],page.col2contactemail[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'CONTACT'" ng-click="page.col2[$index] = '';page.col2contact[$index]='';page.col2contactemail[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <div ng-if='datacol2[$index].mostviewed' ng-init="page.col2viewed[$index]=datacol2[$index].mostviewed"></div>
                      <div ng-if='datacol2[$index].popular' ng-init="page.col2popular[$index]=datacol2[$index].popular"></div>
                      <div ng-if='datacol2[$index].limitcount' ng-init="page.col2newslimit[$index]=datacol2[$index].limitcount"></div>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'NEWS'" ng-model="page.col2viewed[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'NEWS'" ng-model="page.col2popular[$index]" style="color:black"></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'NEWS'" ng-model="page.col2newslimit[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'NEWS'" ng-click="editnewsmodule('2',$index,page.col2viewed[$index],page.col2popular[$index],page.col2newslimit[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'NEWS'" ng-click="page.col2[$index] = '';page.col2viewed[$index]='';page.col2newslimit[$index]='';page.col2popular[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- DIVIDER MODULE -->
                       <div ng-if="datacol2[$index].height" ng-init="page.col2height[$index]=datacol2[$index].height"></div>
                       <div ng-if="datacol2[$index].color" ng-init="page.col2color[$index]=datacol2[$index].color"></div>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'DIVIDER'" ng-model="page.col2height[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'DIVIDER'" ng-model="page.col2color[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'DIVIDER'" ng-click="editdividermodule('2',$index,page.col2height[$index],page.col2color[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'DIVIDER'" ng-click="page.col2[$index] = '';page.col2height[$index]='';page.col2color[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                    </div>
             <!--    </div> -->
                <br>
              </div>


                <!--3 cols-->
                <div class="clear specialpage_pad" ng-if="column[$index] == 3">
                <div ng-if="!page.colcount[$index]" ng-init="page.colcount[$index]=count.columntype"></div>
                    <input type="hidden" ng-model="page.colcount[$index]" style="color:black" required>

                    <button type="button" ng-if="!page.col1[$index]" ng-click="insertmodule('1',$index)" class="col-sm-4 btn m-b-xs btn-lg btn-default btn-addon bg-info pull-left"><i class="fa fa-plus"></i>Insert Module</button>
                    <div class="input-group col-sm-4 pull-left" ng-if="page.col1[$index]">
                      <button type="button" type="button" class="col-sm-8 btn btn-lg btn-default">{[{ page.col1[$index] }]} </button>
                      <!-- TEXT MODULE -->
                       <div ng-if="datacol[$index].content" ng-init="page.col1content[$index]=datacol[$index].content"> </div>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'TEXT'" ng-model="page.col1content[$index]" style="color:black" required></textarea>
                      <button type="button" ng-if="page.col1[$index] == 'TEXT'" type="button" ng-click="edittextmodule('1',$index,page.col1content[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="page.col1[$index] == 'TEXT'" type="button" ng-click="page.col1[$index] = '';page.col1content[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- IMAGE MODULE -->
                      <div ng-if="datacol[$index].image" ng-init="page.col1img[$index]=datacol[$index].image"></div>
                      <div ng-if="datacol[$index].link" ng-init="page.col1link[$index]=datacol[$index].link"></div>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'IMAGE'" ng-model="page.col1img[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'IMAGE'" ng-model="page.col1link[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'IMAGE'" ng-click="editimagemodule('1',$index,page.col1img[$index],page.col1link[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'IMAGE'" ng-click="page.col1[$index] = '';page.col1img[$index]='';page.col1link[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                        <div ng-if="datacol[$index].category" ng-init="page.col1testimonialcat[$index]=datacol[$index].category"> </div>
                       <div ng-if="datacol[$index].limitcount" ng-init="page.col1testimonial[$index]=datacol[$index].limitcount"> </div>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-model="page.col1testimonial[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-model="page.col1testimonialcat[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-click="edittestimonialmodule('1',$index,page.col1testimonial[$index],page.col1testimonialcat[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-click="page.col1[$index] = '';page.col1testimonial[$index]='';page.col1testimonialcat[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>


                       <!-- CONTACT FORM MODULE -->
                       <div ng-if="datacol[$index].title" ng-init="page.col1contact[$index]=datacol[$index].title"> </div>
                       <div ng-if="datacol[$index].email" ng-init="page.col1contactemail[$index]=datacol[$index].email"> </div>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'CONTACT'" ng-model="page.col1contact[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'CONTACT'" ng-model="page.col1contactemail[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'CONTACT'" ng-click="editcontactmodule('1',$index,page.col1contact[$index],page.col1contactemail[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'CONTACT'" ng-click="page.col1[$index] = '';page.col1contact[$index]='';page.col1contactemail[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <div ng-if="datacol[$index].mostviewed" ng-init="page.col1viewed[$index]=datacol[$index].mostviewed"></div>
                       <div ng-if="datacol[$index].popular" ng-init="page.col1popular[$index]=datacol[$index].popular"></div>
                       <div ng-if='datacol[$index].limitcount' ng-init="page.col1newslimit[$index]=datacol[$index].limitcount"></div>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'NEWS'" ng-model="page.col1viewed[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'NEWS'" ng-model="page.col1popular[$index]" style="color:black"></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'NEWS'" ng-model="page.col1newslimit[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'NEWS'" ng-click="editnewsmodule('1',$index,page.col1viewed[$index],page.col1popular[$index],page.col1newslimit[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'NEWS'" ng-click="page.col1[$index] = '';page.col1viewed[$index]='';page.col1newslimit[$index]='';page.col1popular[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- DIVIDER MODULE -->
                        <div ng-if="datacol[$index].height" ng-init="page.col1height[$index]=datacol[$index].height"></div>
                       <div ng-if="datacol[$index].color" ng-init="page.col1color[$index]=datacol[$index].color"></div>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'DIVIDER'" ng-model="page.col1height[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'DIVIDER'" ng-model="page.col1color[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'DIVIDER'" ng-click="editdividermodule('1',$index,page.col1height[$index],page.col1color[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'DIVIDER'" ng-click="page.col1[$index] = '';page.col1height[$index]='';page.col1color[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                    </div>

                    <!--second-->
                    <button type="button" ng-if="!page.col2[$index]" ng-click="insertmodule('2',$index)" class="col-sm-4 btn m-b-xs btn-lg btn-default btn-addon bg-info pull-left"><i class="fa fa-plus"></i>Insert Module</button>
                    <div class="input-group col-sm-4 pull-left" ng-if="page.col2[$index]">
                      <button type="button" type="button" class="col-sm-8 btn btn-lg btn-default">{[{ page.col2[$index] }]}</button>

                      <!-- TEXT MODULE -->
                       <div ng-if="datacol2[$index].content" ng-init="page.col2content[$index]=datacol2[$index].content"> </div>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'TEXT'" ng-model="page.col2content[$index]" style="color:black" required></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'TEXT'" ng-click="edittextmodule('2',$index,page.col2content[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'TEXT'" ng-click="page.col2[$index] = '';page.col2content[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- IMAGE MODULE -->
                      <div ng-if='datacol2[$index].image' ng-init="page.col2img[$index]=datacol2[$index].image"></div>
                      <div ng-if='datacol2[$index].link' ng-init="page.col2link[$index]=datacol2[$index].link"></div>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'IMAGE'" ng-model="page.col2img[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'IMAGE'" ng-model="page.col2link[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'IMAGE'" ng-click="editimagemodule('2',$index,page.col2img[$index],page.col2link[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'IMAGE'" ng-click="page.col2[$index] = '';page.col2img[$index]='';page.col2link[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                        <div ng-if="datacol2[$index].category" ng-init="page.col2testimonialcat[$index]=datacol2[$index].category"> </div>
                       <div ng-if="datacol2[$index].limitcount" ng-init="page.col2testimonial[$index]=datacol2[$index].limitcount"> </div>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'TESTIMONIAL'" ng-model="page.col2testimonial[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'TESTIMONIAL'" ng-model="page.col2testimonialcat[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'TESTIMONIAL'" ng-click="edittestimonialmodule('2',$index,page.col2testimonial[$index],page.col2testimonialcat[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'TESTIMONIAL'" ng-click="page.col2[$index] = '';page.col2testimonial[$index]='';page.col2testimonialcat[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- CONTACT FORM MODULE -->
                       <div ng-if="datacol2[$index].title" ng-init="page.col2contact[$index]=datacol2[$index].title"> </div>
                       <div ng-if="datacol2[$index].email" ng-init="page.col2contactemail[$index]=datacol2[$index].email"> </div>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'CONTACT'" ng-model="page.col2contact[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'CONTACT'" ng-model="page.col2contactemail[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'CONTACT'" ng-click="editcontactmodule('2',$index,page.col2contact[$index],page.col2contactemail[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'CONTACT'" ng-click="page.col2[$index] = '';page.col2contact[$index]='';page.col2contactemail[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <div ng-if='datacol2[$index].mostviewed' ng-init="page.col2viewed[$index]=datacol2[$index].mostviewed"></div>
                      <div ng-if='datacol2[$index].popular' ng-init="page.col2popular[$index]=datacol2[$index].popular"></div>
                      <div ng-if='datacol2[$index].limitcount' ng-init="page.col2newslimit[$index]=datacol2[$index].limitcount"></div>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'NEWS'" ng-model="page.col2viewed[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'NEWS'" ng-model="page.col2popular[$index]" style="color:black"></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'NEWS'" ng-model="page.col2newslimit[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'NEWS'" ng-click="editnewsmodule('2',$index,page.col2viewed[$index],page.col2popular[$index],page.col2newslimit[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'NEWS'" ng-click="page.col2[$index] = '';page.col2viewed[$index]='';page.col2newslimit[$index]='';page.col2popular[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- DIVIDER MODULE -->
                       <div ng-if="datacol2[$index].height" ng-init="page.col2height[$index]=datacol2[$index].height"></div>
                       <div ng-if="datacol2[$index].color" ng-init="page.col2color[$index]=datacol2[$index].color"></div>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'DIVIDER'" ng-model="page.col2height[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'DIVIDER'" ng-model="page.col2color[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'DIVIDER'" ng-click="editdividermodule('2',$index,page.col2height[$index],page.col2color[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'DIVIDER'" ng-click="page.col2[$index] = '';page.col2height[$index]='';page.col2color[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                    </div>

                    <!--thrd-->
                    <button type="button" ng-if="!page.col3[$index]" ng-click="insertmodule('3',$index)" class="col-sm-4 btn m-b-xs btn-lg btn-default btn-addon bg-info pull-left"><i class="fa fa-plus"></i>Insert Module</button>
                    <div class="input-group col-sm-4 pull-left" ng-if="page.col3[$index]">
                      <button type="button" type="button" class="col-sm-8 btn btn-lg btn-default">{[{ page.col3[$index] }]}</button>

                      <!-- TEXT MODULE -->
                       <div ng-if="datacol3[$index].content" ng-init="page.col3content[$index]=datacol3[$index].content"> </div>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'TEXT'" ng-model="page.col3content[$index]" style="color:black" required></textarea>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'TEXT'" ng-click="edittextmodule('3',$index,page.col3content[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'TEXT'" ng-click="page.col3[$index] = '';page.col3content[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- IMAGE MODULE -->
                      <div ng-if='datacol3[$index].image' ng-init="page.col3img[$index]=datacol3[$index].image"></div>
                      <div ng-if='datacol3[$index].link' ng-init="page.col3link[$index]=datacol3[$index].link"></div>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'IMAGE'" ng-model="page.col3img[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'IMAGE'" ng-model="page.col3link[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'IMAGE'" ng-click="editimagemodule('3',$index,page.col3img[$index],page.col3link[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'IMAGE'" ng-click="page.col3[$index] = '';page.col3img[$index]='';page.col3link[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                        <div ng-if="datacol3[$index].category" ng-init="page.col3testimonialcat[$index]=datacol3[$index].category"> </div>
                       <div ng-if="datacol3[$index].limitcount" ng-init="page.col3testimonial[$index]=datacol3[$index].limitcount"> </div>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'TESTIMONIAL'" ng-model="page.col3testimonial[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'TESTIMONIAL'" ng-model="page.col3testimonialcat[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'TESTIMONIAL'" ng-click="edittestimonialmodule('3',$index,page.col3testimonial[$index],page.col3testimonialcat[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'TESTIMONIAL'" ng-click="page.col3[$index] = '';page.col3testimonial[$index]='';page.col3testimonialcat[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- CONTACT FORM MODULE -->
                       <div ng-if="datacol3[$index].title" ng-init="page.col3contact[$index]=datacol3[$index].title"> </div>
                       <div ng-if="datacol3[$index].email" ng-init="page.col3contactemail[$index]=datacol3[$index].email"> </div>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'CONTACT'" ng-model="page.col3contact[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'CONTACT'" ng-model="page.col3contactemail[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'CONTACT'" ng-click="editcontactmodule('3',$index,page.col3contact[$index],page.col3contactemail[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'CONTACT'" ng-click="page.col3[$index] = '';page.col3contact[$index]='';page.col3contactemail[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <div ng-if='datacol3[$index].mostviewed' ng-init="page.col3viewed[$index]=datacol3[$index].mostviewed"></div>
                      <div ng-if='datacol3[$index].popular' ng-init="page.col3popular[$index]=datacol3[$index].popular"></div>
                      <div ng-if='datacol3[$index].limitcount' ng-init="page.col3newslimit[$index]=datacol3[$index].limitcount"></div>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'NEWS'" ng-model="page.col3viewed[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'NEWS'" ng-model="page.col3popular[$index]" style="color:black"></textarea>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'NEWS'" ng-model="page.col3newslimit[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'NEWS'" ng-click="editnewsmodule('3',$index,page.col3viewed[$index],page.col3popular[$index],page.col3newslimit[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'NEWS'" ng-click="page.col3[$index] = '';page.col3viewed[$index]='';page.col3newslimit[$index]='';page.col3popular[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- DIVIDER MODULE -->
                       <div ng-if="datacol3[$index].height" ng-init="page.col3height[$index]=datacol3[$index].height"></div>
                       <div ng-if="datacol3[$index].color" ng-init="page.col3color[$index]=datacol3[$index].color"></div>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'DIVIDER'" ng-model="page.col3height[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'DIVIDER'" ng-model="page.col3color[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'DIVIDER'" ng-click="editdividermodule('3',$index,page.col3height[$index],page.col3color[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'DIVIDER'" ng-click="page.col3[$index] = '';page.col3height[$index]='';page.col3color[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>
                    </div>
                <br>
              </div>



              <!---------------------------------------------------------------------------------------------------------------------------------4 cols-->
                <div class="clear specialpage_pad" ng-if="column[$index] == 4">
                <div ng-if="!page.colcount[$index]" ng-init="page.colcount[$index]=count.columntype"></div>
                    <input type="hidden" ng-model="page.colcount[$index]" style="color:black" required>

                    <button type="button" ng-if="!page.col1[$index]" ng-click="insertmodule('1',$index)" class="col-sm-3 btn m-b-xs btn-lg btn-default btn-addon bg-info pull-left"><i class="fa fa-plus"></i>Insert Module</button>
                    <div class="input-group col-sm-3 pull-left" ng-if="page.col1[$index]">
                      <button type="button" type="button" class="col-sm-8 btn btn-lg btn-default">{[{ page.col1[$index] }]} </button>
                      <!-- TEXT MODULE -->
                       <div ng-if="datacol[$index].content" ng-init="page.col1content[$index]=datacol[$index].content"> </div>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'TEXT'" ng-model="page.col1content[$index]" style="color:black" required></textarea>
                      <button type="button" ng-if="page.col1[$index] == 'TEXT'" type="button" ng-click="edittextmodule('1',$index,page.col1content[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" ng-if="page.col1[$index] == 'TEXT'" type="button" ng-click="page.col1[$index] = '';page.col1content[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- IMAGE MODULE -->
                      <div ng-if="datacol[$index].image" ng-init="page.col1img[$index]=datacol[$index].image"></div>
                      <div ng-if="datacol[$index].link" ng-init="page.col1link[$index]=datacol[$index].link"></div>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'IMAGE'" ng-model="page.col1img[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'IMAGE'" ng-model="page.col1link[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'IMAGE'" ng-click="editimagemodule('1',$index,page.col1img[$index],page.col1link[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'IMAGE'" ng-click="page.col1[$index] = '';page.col1img[$index]='';page.col1link[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                        <div ng-if="datacol[$index].category" ng-init="page.col1testimonialcat[$index]=datacol[$index].category"> </div>
                       <div ng-if="datacol[$index].limitcount" ng-init="page.col1testimonial[$index]=datacol[$index].limitcount"> </div>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-model="page.col1testimonial[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-model="page.col1testimonialcat[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-click="edittestimonialmodule('1',$index,page.col1testimonial[$index],page.col1testimonialcat[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'TESTIMONIAL'" ng-click="page.col1[$index] = '';page.col1testimonial[$index]='';page.col1testimonialcat[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>


                       <!-- CONTACT FORM MODULE -->
                       <div ng-if="datacol[$index].title" ng-init="page.col1contact[$index]=datacol[$index].title"> </div>
                       <div ng-if="datacol[$index].email" ng-init="page.col1contactemail[$index]=datacol[$index].email"> </div>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'CONTACT'" ng-model="page.col1contact[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'CONTACT'" ng-model="page.col1contactemail[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'CONTACT'" ng-click="editcontactmodule('1',$index,page.col1contact[$index],page.col1contactemail[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'CONTACT'" ng-click="page.col1[$index] = '';page.col1contact[$index]='';page.col1contactemail[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <div ng-if="datacol[$index].mostviewed" ng-init="page.col1viewed[$index]=datacol[$index].mostviewed"></div>
                       <div ng-if="datacol[$index].popular" ng-init="page.col1popular[$index]=datacol[$index].popular"></div>
                       <div ng-if='datacol[$index].limitcount' ng-init="page.col1newslimit[$index]=datacol[$index].limitcount"></div>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'NEWS'" ng-model="page.col1viewed[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'NEWS'" ng-model="page.col1popular[$index]" style="color:black"></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'NEWS'" ng-model="page.col1newslimit[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'NEWS'" ng-click="editnewsmodule('1',$index,page.col1viewed[$index],page.col1popular[$index],page.col1newslimit[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'NEWS'" ng-click="page.col1[$index] = '';page.col1viewed[$index]='';page.col1newslimit[$index]='';page.col1popular[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- DIVIDER MODULE -->
                        <div ng-if="datacol[$index].height" ng-init="page.col1height[$index]=datacol[$index].height"></div>
                       <div ng-if="datacol[$index].color" ng-init="page.col1color[$index]=datacol[$index].color"></div>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'DIVIDER'" ng-model="page.col1height[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col1[$index] == 'DIVIDER'" ng-model="page.col1color[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'DIVIDER'" ng-click="editdividermodule('1',$index,page.col1height[$index],page.col1color[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col1[$index] == 'DIVIDER'" ng-click="page.col1[$index] = '';page.col1height[$index]='';page.col1color[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                    </div>

                    <!--second-->
                    <button type="button" ng-if="!page.col2[$index]" ng-click="insertmodule('2',$index)" class="col-sm-3 btn m-b-xs btn-lg btn-default btn-addon bg-info pull-left"><i class="fa fa-plus"></i>Insert Module</button>
                    <div class="input-group col-sm-3 pull-left" ng-if="page.col2[$index]">
                      <button type="button" type="button" class="col-sm-8 btn btn-lg btn-default">{[{ page.col2[$index] }]}</button>

                      <!-- TEXT MODULE -->
                       <div ng-if="datacol2[$index].content" ng-init="page.col2content[$index]=datacol2[$index].content"> </div>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'TEXT'" ng-model="page.col2content[$index]" style="color:black" required></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'TEXT'" ng-click="edittextmodule('2',$index,page.col2content[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'TEXT'" ng-click="page.col2[$index] = '';page.col2content[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- IMAGE MODULE -->
                      <div ng-if='datacol2[$index].image' ng-init="page.col2img[$index]=datacol2[$index].image"></div>
                      <div ng-if='datacol2[$index].link' ng-init="page.col2link[$index]=datacol2[$index].link"></div>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'IMAGE'" ng-model="page.col2img[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'IMAGE'" ng-model="page.col2link[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'IMAGE'" ng-click="editimagemodule('2',$index,page.col2img[$index],page.col2link[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'IMAGE'" ng-click="page.col2[$index] = '';page.col2img[$index]='';page.col2link[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                        <div ng-if="datacol2[$index].category" ng-init="page.col2testimonialcat[$index]=datacol2[$index].category"> </div>
                       <div ng-if="datacol2[$index].limitcount" ng-init="page.col2testimonial[$index]=datacol2[$index].limitcount"> </div>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'TESTIMONIAL'" ng-model="page.col2testimonial[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'TESTIMONIAL'" ng-model="page.col2testimonialcat[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'TESTIMONIAL'" ng-click="edittestimonialmodule('2',$index,page.col2testimonial[$index],page.col2testimonialcat[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'TESTIMONIAL'" ng-click="page.col2[$index] = '';page.col2testimonial[$index]='';page.col2testimonialcat[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- CONTACT FORM MODULE -->
                       <div ng-if="datacol2[$index].title" ng-init="page.col2contact[$index]=datacol2[$index].title"> </div>
                       <div ng-if="datacol2[$index].email" ng-init="page.col2contactemail[$index]=datacol2[$index].email"> </div>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'CONTACT'" ng-model="page.col2contact[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'CONTACT'" ng-model="page.col2contactemail[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'CONTACT'" ng-click="editcontactmodule('2',$index,page.col2contact[$index],page.col2contactemail[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'CONTACT'" ng-click="page.col2[$index] = '';page.col2contact[$index]='';page.col2contactemail[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <div ng-if='datacol2[$index].mostviewed' ng-init="page.col2viewed[$index]=datacol2[$index].mostviewed"></div>
                      <div ng-if='datacol2[$index].popular' ng-init="page.col2popular[$index]=datacol2[$index].popular"></div>
                      <div ng-if='datacol2[$index].limitcount' ng-init="page.col2newslimit[$index]=datacol2[$index].limitcount"></div>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'NEWS'" ng-model="page.col2viewed[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'NEWS'" ng-model="page.col2popular[$index]" style="color:black"></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'NEWS'" ng-model="page.col2newslimit[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'NEWS'" ng-click="editnewsmodule('2',$index,page.col2viewed[$index],page.col2popular[$index],page.col2newslimit[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'NEWS'" ng-click="page.col2[$index] = '';page.col2viewed[$index]='';page.col2newslimit[$index]='';page.col2popular[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- DIVIDER MODULE -->
                       <div ng-if="datacol2[$index].height" ng-init="page.col2height[$index]=datacol2[$index].height"></div>
                       <div ng-if="datacol2[$index].color" ng-init="page.col2color[$index]=datacol2[$index].color"></div>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'DIVIDER'" ng-model="page.col2height[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col2[$index] == 'DIVIDER'" ng-model="page.col2color[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'DIVIDER'" ng-click="editdividermodule('2',$index,page.col2height[$index],page.col2color[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col2[$index] == 'DIVIDER'" ng-click="page.col2[$index] = '';page.col2height[$index]='';page.col2color[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                    </div>

                    <!--thrd-->
                    <button type="button" ng-if="!page.col3[$index]" ng-click="insertmodule('3',$index)" class="col-sm-3 btn m-b-xs btn-lg btn-default btn-addon bg-info pull-left"><i class="fa fa-plus"></i>Insert Module</button>
                    <div class="input-group col-sm-3 pull-left" ng-if="page.col3[$index]">
                      <button type="button" type="button" class="col-sm-8 btn btn-lg btn-default">{[{ page.col3[$index] }]}</button>

                      <!-- TEXT MODULE -->
                       <div ng-if="datacol3[$index].content" ng-init="page.col3content[$index]=datacol3[$index].content"> </div>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'TEXT'" ng-model="page.col3content[$index]" style="color:black" required></textarea>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'TEXT'" ng-click="edittextmodule('3',$index,page.col3content[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'TEXT'" ng-click="page.col3[$index] = '';page.col3content[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- IMAGE MODULE -->
                      <div ng-if='datacol3[$index].image' ng-init="page.col3img[$index]=datacol3[$index].image"></div>
                      <div ng-if='datacol3[$index].link' ng-init="page.col3link[$index]=datacol3[$index].link"></div>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'IMAGE'" ng-model="page.col3img[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'IMAGE'" ng-model="page.col3link[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'IMAGE'" ng-click="editimagemodule('3',$index,page.col3img[$index],page.col3link[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'IMAGE'" ng-click="page.col3[$index] = '';page.col3img[$index]='';page.col3link[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                        <div ng-if="datacol3[$index].category" ng-init="page.col3testimonialcat[$index]=datacol3[$index].category"> </div>
                       <div ng-if="datacol3[$index].limitcount" ng-init="page.col3testimonial[$index]=datacol3[$index].limitcount"> </div>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'TESTIMONIAL'" ng-model="page.col3testimonial[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'TESTIMONIAL'" ng-model="page.col3testimonialcat[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'TESTIMONIAL'" ng-click="edittestimonialmodule('3',$index,page.col3testimonial[$index],page.col3testimonialcat[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'TESTIMONIAL'" ng-click="page.col3[$index] = '';page.col3testimonial[$index]='';page.col3testimonialcat[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- CONTACT FORM MODULE -->
                       <div ng-if="datacol3[$index].title" ng-init="page.col3contact[$index]=datacol3[$index].title"> </div>
                       <div ng-if="datacol3[$index].email" ng-init="page.col3contactemail[$index]=datacol3[$index].email"> </div>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'CONTACT'" ng-model="page.col3contact[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'CONTACT'" ng-model="page.col3contactemail[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'CONTACT'" ng-click="editcontactmodule('3',$index,page.col3contact[$index],page.col3contactemail[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'CONTACT'" ng-click="page.col3[$index] = '';page.col3contact[$index]='';page.col3contactemail[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <div ng-if='datacol3[$index].mostviewed' ng-init="page.col3viewed[$index]=datacol3[$index].mostviewed"></div>
                      <div ng-if='datacol3[$index].popular' ng-init="page.col3popular[$index]=datacol3[$index].popular"></div>
                      <div ng-if='datacol3[$index].limitcount' ng-init="page.col3newslimit[$index]=datacol3[$index].limitcount"></div>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'NEWS'" ng-model="page.col3viewed[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'NEWS'" ng-model="page.col3popular[$index]" style="color:black"></textarea>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'NEWS'" ng-model="page.col3newslimit[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'NEWS'" ng-click="editnewsmodule('3',$index,page.col3viewed[$index],page.col3popular[$index],page.col3newslimit[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'NEWS'" ng-click="page.col3[$index] = '';page.col3viewed[$index]='';page.col3newslimit[$index]='';page.col3popular[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- DIVIDER MODULE -->
                       <div ng-if="datacol3[$index].height" ng-init="page.col3height[$index]=datacol3[$index].height"></div>
                       <div ng-if="datacol3[$index].color" ng-init="page.col3color[$index]=datacol3[$index].color"></div>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'DIVIDER'" ng-model="page.col3height[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col3[$index] == 'DIVIDER'" ng-model="page.col3color[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'DIVIDER'" ng-click="editdividermodule('3',$index,page.col3height[$index],page.col3color[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col3[$index] == 'DIVIDER'" ng-click="page.col3[$index] = '';page.col3height[$index]='';page.col3color[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>
                    </div>

                    <!--4th-->
                    <button type="button" ng-if="!page.col4[$index]" ng-click="insertmodule('4',$index)" class="col-sm-3 btn m-b-xs btn-lg btn-default btn-addon bg-info pull-left"><i class="fa fa-plus"></i>Insert Module</button>
                    <div class="input-group col-sm-3 pull-left" ng-if="page.col4[$index]">
                      <button type="button" type="button" class="col-sm-8 btn btn-lg btn-default">{[{ page.col4[$index] }]}</button>

                      <!-- TEXT MODULE -->
                       <div ng-if="datacol4[$index].content" ng-init="page.col4content[$index]=datacol4[$index].content"> </div>
                      <textarea style="display:none;" ng-if="page.col4[$index] == 'TEXT'" ng-model="page.col4content[$index]" style="color:black" required></textarea>
                      <button type="button" type="button" ng-if="page.col4[$index] == 'TEXT'" ng-click="edittextmodule('4',$index,page.col4content[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col4[$index] == 'TEXT'" ng-click="page.col4[$index] = '';page.col4content[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- IMAGE MODULE -->
                      <div ng-if='datacol4[$index].image' ng-init="page.col4img[$index]=datacol4[$index].image"></div>
                      <div ng-if='datacol4[$index].link' ng-init="page.col4link[$index]=datacol4[$index].link"></div>
                      <textarea style="display:none;" ng-if="page.col4[$index] == 'IMAGE'" ng-model="page.col4img[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col4[$index] == 'IMAGE'" ng-model="page.col4link[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col4[$index] == 'IMAGE'" ng-click="editimagemodule('4',$index,page.col4img[$index],page.col4link[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col4[$index] == 'IMAGE'" ng-click="page.col4[$index] = '';page.col4img[$index]='';page.col4link[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- TESTIMONIAL MODULE -->
                        <div ng-if="datacol4[$index].category" ng-init="page.col4testimonialcat[$index]=datacol4[$index].category"> </div>
                       <div ng-if="datacol4[$index].limitcount" ng-init="page.col4testimonial[$index]=datacol4[$index].limitcount"> </div>
                      <textarea style="display:none;" ng-if="page.col4[$index] == 'TESTIMONIAL'" ng-model="page.col4testimonial[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col4[$index] == 'TESTIMONIAL'" ng-model="page.col4testimonialcat[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col4[$index] == 'TESTIMONIAL'" ng-click="edittestimonialmodule('4',$index,page.col4testimonial[$index],page.col4testimonialcat[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col4[$index] == 'TESTIMONIAL'" ng-click="page.col4[$index] = '';page.col4testimonial[$index]='';page.col4testimonialcat[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- CONTACT FORM MODULE -->
                       <div ng-if="datacol4[$index].title" ng-init="page.col4contact[$index]=datacol4[$index].title"> </div>
                       <div ng-if="datacol4[$index].email" ng-init="page.col4contactemail[$index]=datacol4[$index].email"> </div>
                      <textarea style="display:none;" ng-if="page.col4[$index] == 'CONTACT'" ng-model="page.col4contact[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col4[$index] == 'CONTACT'" ng-model="page.col4contactemail[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col4[$index] == 'CONTACT'" ng-click="editcontactmodule('4',$index,page.col4contact[$index],page.col4contactemail[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col4[$index] == 'CONTACT'" ng-click="page.col4[$index] = '';page.col4contact[$index]='';page.col4contactemail[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                      <!-- NEWS MODULE -->
                      <div ng-if='datacol4[$index].mostviewed' ng-init="page.col4viewed[$index]=datacol4[$index].mostviewed"></div>
                      <div ng-if='datacol4[$index].popular' ng-init="page.col4popular[$index]=datacol4[$index].popular"></div>
                      <div ng-if='datacol4[$index].limitcount' ng-init="page.col4newslimit[$index]=datacol4[$index].limitcount"></div>
                      <textarea style="display:none;" ng-if="page.col4[$index] == 'NEWS'" ng-model="page.col4viewed[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col4[$index] == 'NEWS'" ng-model="page.col4popular[$index]" style="color:black"></textarea>
                      <textarea style="display:none;" ng-if="page.col4[$index] == 'NEWS'" ng-model="page.col4newslimit[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col4[$index] == 'NEWS'" ng-click="editnewsmodule('4',$index,page.col4viewed[$index],page.col4popular[$index],page.col4newslimit[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col4[$index] == 'NEWS'" ng-click="page.col4[$index] = '';page.col4viewed[$index]='';page.col4newslimit[$index]='';page.col4popular[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>

                       <!-- DIVIDER MODULE -->
                       <div ng-if="datacol4[$index].height" ng-init="page.col4height[$index]=datacol4[$index].height"></div>
                       <div ng-if="datacol4[$index].color" ng-init="page.col4color[$index]=datacol4[$index].color"></div>
                      <textarea style="display:none;" ng-if="page.col4[$index] == 'DIVIDER'" ng-model="page.col4height[$index]" style="color:black" required></textarea>
                      <textarea style="display:none;" ng-if="page.col4[$index] == 'DIVIDER'" ng-model="page.col4color[$index]" style="color:black"></textarea>
                      <button type="button" type="button" ng-if="page.col4[$index] == 'DIVIDER'" ng-click="editdividermodule('4',$index,page.col4height[$index],page.col4color[$index])" title="Edit" class="col-sm-2 btn btn-lg btn-primary"><i class="fa fa-edit"></i></button>
                      <button type="button" type="button" ng-if="page.col4[$index] == 'DIVIDER'" ng-click="page.col4[$index] = '';page.col4height[$index]='';page.col4color[$index]=''" title="Delete" class="col-sm-2 btn btn-lg btn-danger"><i class="fa fa-trash-o"></i></button>
                    </div>

                <br>
              </div>
              <span ng-if="countrow != 0 && $last" ng-click="rowdelitem($index)" class="pull-left" style="margin-right:10px"><a href="" title="REMOVE"><i class="fa fa-times fa-fw"></i></a></span>
           
        </div>
            <div class="clear specialpage_pad">
                <button type="button" class="col-sm-12 btn m-b-xs btn-lg btn-default btn-addon" ng-click="rownewitem($event)"><i class="fa fa-plus"></i>Add Row</button>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="panel-body">
            <footer class="panel-footer text-right bg-light lter">
              <a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
              <button type="submit" class="btn btn-success" ng-disabled="formpage.$invalid">Submit</button>
            </footer>
          </div>
        </div>
      </div>
    </fieldset>
  </form>
