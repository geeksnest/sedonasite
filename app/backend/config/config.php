<?php

return new \Phalcon\Config(array(
	'database' => array(
		'adapter'  => 'Mysql',
		'host'     => 'localhost',
		'username' => 'root',
		'password' => 'acer123',
		'name'     => 'dbsedona',
	),
	'application' => array(
		'controllersDir' => __DIR__ . '/../controllers/',
		'modelsDir' => __DIR__ . '/../models/',
		'viewsDir' => __DIR__ . '/../views/',
		'pluginsDir' => __DIR__ . '/../plugins/',
		'cacheDir' => __DIR__ . '/../cache/',
	)
));
