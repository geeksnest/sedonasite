<?php echo $this->getContent()?>
<style type="text/css">
input[type=radio]{
    display:none;
}
input[type=radio] + label:before {
    content: "";  
    display: inline-block;  
    width: 17px;  
    height: 17px;  
    vertical-align:middle;
    margin-right: 8px;  
    background-color: #E3E3E3;  
    box-shadow: inset 0px 2px 2px rgba(0, 0, 0, .3);
    border-radius: 8px;  
}

input[type=radio]:checked + label:before {
   /* content: "\2022";*/ /* Bullet */
    color:white;
    background-color: #FF9900; 
    font-size:1.8em;
    text-align:center;
    line-height:14px;
    text-shadow:0px 0px 3px #eee;
}

</style>
<script type="text/ng-template" id="Usertestinfo.html">
    <div ng-include="'/fe/tpl/Usertestinfo.html'"></div>
</script>

<style type="text/css">
 
</style>
    <!-- Banner starts -->
    <div class="container" ng-controller='TestCtrl'> 
    <div class='col-sm-12' style='height:100%;margin-top:2%'>
 

    <div class='col-sm-8 col-md-offset-2'> 
    <div class='panel panel-default'>
    <div class='panel-heading font-bold' style='font-size:20px'>
    Sedona Healing Arts Test Taker
    </div>

    <div class='panel panel-body'>

   <table ng-repeat="ss in testitems | startFrom: pagination.page * pagination.perPage | limitTo: pagination.perPage" class='table'>
<tr>
<td>
 Question {[{ss.index}]} out of {[{length}]} </td>
</tr>
<div class='hidden'> {[{ss.lock}]} </div>
<tr><td>
{[{ss.q}]}  
</td></tr>

<tr><td>
<div class="radio">
<input type="radio" id="rb1" name="a" value="A" ng-click='check()' ng-model='answers[ss.lock]' checked="">
<label for="rb1"> {[{ss.a}]} </label><br>
</div>

<div class='radio'> 
<input type="radio" id="rb2" name="a" value='B' ng-model='answers[ss.lock]' checked="">
<label for="rb2"> {[{ss.b}]}  </label><br>
</div> 


<div class="radio">
<input type="radio" id="rb3" name="a" value='C' ng-model='answers[ss.lock]'>
<label for="rb3">   {[{ss.c}]}  </label>

</div> 
</td></tr>

<tr> <td> 
<div class='pull-left'>
<button class='btn btn-default' ng-disabled="pagination.page == 0" ng-click="pagination.prevPage(); prev() ">Previous</button>
<button class='btn btn-default' ng-disabled="pagination.page + 1 > pagination.numPages"
 ng-click="pagination.nextPage(); ans(ss.lock,ss.index,answers)">Next</button>
</div>
</td></tr>

<tr ng-show="submit" ><td>
<div class='pull-right'>
 <button class='btn btn-success' ng-click="sub(answers)">Get Result</button> </td> </tr>
 </div>
<tr><td> 
<div class='pull-right'>
<span ng-show='thanks' style='color:green'> Thank you for answering! You can now get the result </span> </td></tr>
</div>
   </table>




<!-- <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="/fe/js/angularUtils-pagination/dirPagination.tpl.html"></dir-pagination-controls>
 -->


    </div>

    </div>
    </div>



    </div>
    </div>

    <!-- Below Banner ends -->
