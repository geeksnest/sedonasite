<div class="container-fluid bg-light" ng-controller="mycartCtrl">
  <div class="row">
    <div class="col-sm-10 col-sm-offset-1 bg-light ec-nopad">

    <div class="col-sm-12 wrapper-md">

      <div ng-include="'/fe/tpl/ecommerce/header.html'"></div>
      <div ng-include="'/fe/tpl/ecommerce/menu.html'"></div>

      <div class="row">
        <div class="col-sm-12">
          <div class="col-sm-9">
            <a href="<?php echo $shop; ?>" class="ec-font-s">< Continue Shopping</a>
            <a class="btn btn-default pull-right" ng-click="emptycart()"
                data-toggle="tooltip" title="Your items will be cleared">Empty Cart</a>
            <br><br> 
            <div class="table-responsive">
              <table class="table table-striped b-t b-light ec-table-mycart">
                <thead>
                  <tr>
                    <th style="min-width:120px"></th>
                    <th style="width:40%">ITEM</th>
                    <th style="width:6%">PRICE</th>
                    <th style="min-width:150px">QUANTITY</th>
                    <th style="width:10%">SUBTOTAL</th>
                    <th style="width:10%"></th>
                  </tr>
                </thead>
                <tbody>
                  <tr ng-repeat="product in mycart | groupBy: 'productid' ">
                    <td>
                      <div class="ec-prod-mycartthumb" style="background:url('{[{amazonimage}]}/{[{product[0].productid}]}/{[{product[0].filename}]}');">
                      </div>
                    </td>
                    <td class="font-bold ec-font-s pointer" ng-bind="product[0].name" ng-click="gotoproduct(product[0].productcode)"></td>
                    <td>$<span ng-bind="product[0].price|number:2"></span></td>
                    <td>
                      <div class="input-group">
                        
                        <span class="input-group-btn">
                          <button type="button" class="btn btn-sm btn-default"
                            ng-click="minusquantity(product[0].productid)"
                            ng-disabled="product.length <= product[0].minquantity">-</button>
                        </span>

                        
                        <select class="form-control ec-select" ng-model="product.length" ng-change="selectquantity(product[0].productid, product.length)" style="padding:0;">
                          <option ng-repeat="n in optionQuantity(product[0].minquantity, product[0].maxquantity)">{[{n}]}</option>
                        </select>

                        
                        <span class="input-group-btn">
                          <button type="button" class="btn btn-sm btn-default"
                            ng-click="plusquantity(product[0].productid)"
                            ng-disabled="product.length >= product[0].maxquantity">+</button>
                        </span>
                      </div>
                    </td>
                    <td>$<span ng-bind="product[0].price * product.length | number:2 "></span></td>
                    <td><a class="btn btn-sm btn-default" title="Remove" ng-click="removeitem(product[0].productid, product)"><i class=" icon-close "></i></a></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="panel panel-default">
              <div class="panel-heading font-bold">ORDER SUMMARY</div>
              <div class="panel-body">
                <div class="text-c">
                  <div class="ec-font-lg">$<span ng-bind="total | number"></span></div>
                  TOTAL
                </div>
                <br>
                <a href="<?php echo $shop; ?>/checkout" type="button" class="btn btn-danger btn-block" ng-disabled="total < 1">CHECKOUT</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    </div>
  </div>
</div>

<script type="text/ng-template" id="confirm">
  <div ng-include="'/fe/tpl/ecommerce/confirm.html'"></div>
</script>
