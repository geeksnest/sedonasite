
    <!-- Banner starts -->
    <div class="banner-container about-sedona-bg" title="<?php echo $title; ?>">

        <img src="img/frontend/banner_about.png" class="pinterest-img" alt="<?php echo $title; ?>">

    </div>

    <!-- Banner ends -->


    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="phar no-padding">
                    <span class="size20"><h2>OUR STORE</h2></span>
                    <p class="size18">
                        Sedona Healing Arts is a spiritual healing center that offers a wide array of healing services for the body, mind and spirit. We also provide other services such as spiritual readings, acupuncture, retreats, and massage. Sedona Healing Arts also functions as a metaphysical products and souvenirs store. The shop is operated by the most wonderful spiritual guide, healer, and licensed acupuncturist, Banya Lim.<br/><br/>
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="about-video" style="">
                    <iframe style="width:100%; height: 100%;" src="https://www.youtube.com/embed/4XIjgXDx7UE" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <div class="phar no-padding">
                    <p class="size18"><br/>
                        Sedona is a blessed and holy land, and through the transformational services we provide combined with the powerfully healing energies of Sedona’s vortexes, we will help you deepen the love you hold for yourself. Wherever you are standing in your life’s journey, we can help you. You will gain profound insights about making a deep connection with your heart and nature to discover the best of who you truly are.
                    </p>
                </div>

                <div class="phar no-padding">
                    <p class="size18">
                        Whatever path brought you here, there is a reason why you came. You may not realize it now, but open your ears and listen. Listen to the message that your heart and Sedona have for you.
                    </p>
                </div>

                <div class="phar no-padding">
                    <p class="size18">
                        Sedona Healing Arts focuses on the spiritual growth of each individual person who visits. Everyone needs different guidance in his or her spiritual growth. At Sedona Healing Arts, we utilize different tools catered to your specific needs to guide you to become the best version of yourself.
                    </p>
                </div>

                <div class="phar no-padding">
                    <p class="size18">
                        Come to Sedona to find or rediscover a dream you’ve lost.
                    </p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="phar no-padding">
                    <span class="size20"><h2>ABOUT BANYA</h2></span>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="phar no-padding">
                    <img class="col" src="img/frontend/7540108_orig.jpg">
                </div>
            </div>

            <div class="col-sm-9">
                <div class="phar no-padding">
                    <p class="size18">
                        Banya Lim is the Director and Spiritual Intuitive Guide and Healer at Sedona Healing Arts. She is a nationally licensed acupuncturist, healer, and educator of Oriental medicine. Banya is also the Director of IBE (Institute of Brain Education) and an advisor to IBREA (International Brain Education Association).
                    </p>
                </div>
                <div class="phar no-padding">
                    <p class="size18">
                        As a 4th generation intuitive reader and healer of Oriental medicine, Banya can get to the root of your problems beyond the immediate symptoms. Her healing powers go into cellular levels to guide exactly what you need to release in order to have a balanced, happy, and healthy future.
                    </p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="phar no-padding">
                    <p class="size18">
                        Banya’s understanding transcends time and space to bring wisdom into your life. Banya has developed her abilities to understand and heal not only bodies but also lives and relationships in the past, present, and future. She has practiced the Tao tradition for the last 20 years as a Tao Master, who is someone who practices and promotes spiritual awakening, self-realization, and natural healing of body, mind and spirit. At Sedona Healing Arts, Banya envisions a place where she utilizes all of her skills to help as many people as she can to heal themselves.
                    </p>
                </div>
                <div class="phar no-padding">
                    <p class="size18">
                        The messages you will receive from Banya during the readings come from the most sacred source energy of Sedona. There is no limit to what types of questions you can ask her. You may ask any questions from relationships, work, family...the possibilities are endless! Spiritual readings and healings with Banya will equip you with new clarity to see your life in a much more positive and hopeful direction.
                    </p>
                </div>
            </div>
        </div>
    </div>


    <!-- Below Banner ends -->

  <div class="container storeinfo">
    <hr class="styled-hr">
    <div class="row margin-right-979">
      <div class="col-sm-12 title2">
        <span class="titlel">Store Information</span>
      </div>
    </div>
    <div class="row margin-right-979">
    <div class="col-sm-7 center">
        <div class="wsite-map"><iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 250px; margin-top: 10px; margin-bottom: 10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3273.673285579057!2d-111.7637428!3d34.8644358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872da1564d73fd15%3A0x3e31d123930491bf!2s201+Hwy+179%2C+Sedona%2C+AZ+86336%2C+USA!5e0!3m2!1sen!2sph!4v1434348998372"></iframe></div>
    </div>

    <div class="col-sm-5 text-left storeinfo-details">
        <div class="title3">
          <span class="text-top"><span class="titleb">Phone:</span> (928) 282-3875</span> <br/>
          <span class="text-top"><span class="titleb">Email:</span> contact@sedonahealingarts.com</span> <br/>
          <span class="text-top"><span class="titleb">Hours:</span> Mon - Sun: 10 am - 7 pm</span>  <br/> <br/>
        </div>
        <div class="title3">
          <span class="titleb text-top">Address:</span> <br/>
          <span class="text-top">201 State Route 179</span> <br/>
          <span class="text-top">Sedona, AZ 86336</span>
        </div>
      </div>
    </div>
    <hr class="styled-hr">
  </div>
