<?php echo $this->getContent()?>
    <!-- Banner starts -->
<style type="text/css">
    h4.blog-title-list {
     border-top: 0px solid #D0D0D0; 
    /* margin-top: 15px; */
    }
</style>
<!-- Banner starts -->
<?php if($banner==true){ ?>
<div class="banner-container" style="background:<?php echo $bgcolor; ?> url(<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo @$imagethumb; ?>);
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center center;
    width: 100%;
    height: 400px;
    overflow: hidden;">
    <?php if(@$imagethumbsubtitle!='' || @$thumbdesc!=''){ ?>
  <div class="<?php if(@$box == 'true'){ echo "white-box"; }else{ echo "no-box"; } ?>" style="margin: <?php
    if(@$align == 'center'){
      echo 'auto;';
    }else if(@$align == 'left'){
      echo '70px;';
    }else{
      echo '70px 560px;';
    }
   ?>margin-top: 2.5%;">
    <span class="banner-title" style="color:<?php echo @$color; ?>"><?php echo @$imagethumbsubtitle; ?></span>
    <br/>
    <span class="banner-sub-title1" style="color:<?php echo @$color; ?>"><?php echo @$thumbdesc; ?></span> <br/> <br/>
    <?php if(@$btnname != ''){ ?>
       <div class="button"><a href="<?php echo @$btnlink; ?>"><?php echo @$btnname; ?></a></div>
    <?php }} ?>
  </div>

</div>
<?php } ?>

<!-- Banner ends -->
<div class="container-fluid">
    
<div>
    <div class="specialpage-content">
<!-- RESPONSIVE -->
<div class="col-sm-12 mobile-special-maincontent" id="second-div">

</div>
<!-- RESPONSIVE -->


<!-- RESPONSIVE -->
<?php if($data->pagetype == "Normal"){ ?>
<div class="col-sm-12 mobile-normal-maincontent">
           <!-- <div class="normalpage-title margin-bot10"><span><?php
            echo $data->title;
            ?> </span></div> -->

           <div class="normalpage-body size14">
           <?php
            echo $content;
            ?> 
            </div>
</div>
<?php } ?>
<!-- RESPONSIVE -->


<!--     LEFT SIDE BAR -->
    <?php 
     if($data->leftsidebar == 'true')
    { ?>
     <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
<div class="col-sm-2 specialpage-sidebar">
<?php }else{ ?>
    <div class="col-sm-2">
    <?php } ?>
    <div class="left-sidebar">    
    <?php
    foreach ($sidebarleft as $key => $value) {
     if($data->leftsidebar=='true')
     {
        ?>
             <?php
                if($value->sidebar=='Image')
                {
                  if($value->imglink == NULL){
                    ?>
                    <hr class="styled-hr">
                    <img src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $value->img?>" width="100%">
                    <?php }else{ ?>
                    <hr class="styled-hr">
                    <a target="_blank" href="<?php echo $value->imglink; ?>"><img src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $value->img?>" width="100%"></a>
                    <?php 
                    }
                }
                elseif($value->sidebar=='Calendar')
                {
                    ?>
                     <hr class="styled-hr">
                   <div class="calendar" ng-model="eventSources" calendar="calendar1" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div>
                    <?php
                }
                elseif($value->sidebar=='Menu')
                {
                    ?>
        <hr class="styled-hr">
        <div class="special-menu">
          <div class="">
            <div class=""><a class="special-top-menu">Sedona Healing Arts</a>
            </div>
            <nav>
              <?php $mobmenu = $value->shortcode;
              $children = $value->children;
              foreach ($mobmenu as $k => $short) {
                ?>
                <ul class="">
                  <li> 
                    <?php
                    if ($mobmenu[$k]->newtab=='true'){
                      ?>
                      <a class="special-menu-text" href="<?php echo $mobmenu[$k]->sublink; ?>" target="_blank"><span>
                        <?php echo $mobmenu[$k]->subname; ?> 
                      </span></a>
                      <?php
                }//if newtab true
                elseif ($mobmenu[$k]->newtab=='false') {

                  ?>
                  <a class="special-menu-text" href="<?php echo $mobmenu[$k]->sublink; ?>"><span>
                    <?php echo $mobmenu[$k]->subname; ?>
                  </span></a>
                  <?php
                }//if newtab false

                if (sizeof($children)!=0) {
                  ?>
                  <ul style="padding-left: 20px; list-style:none">
                    <?php
                    foreach ($children as $kk => $chi) {
                      $child1 = $children[$kk]->child1;
                      $child2 = $children[$kk]->child2;
                      $child3 = $children[$kk]->child3;
                      $child4 = $children[$kk]->child4;
                      foreach ($child1 as $child1) {
                        if ($child1->parent == $mobmenu[$k]->submenuID) {
                          ?>
                          <li> <span class="glyphicon glyphicon-menu-right" style="color:#58666e;"></span>
                            <?php
                            if ($child1->newtab=="true") {
                              ?>
                              <a class="special-menu-text caps" href="<?php echo $child1->sublink; ?>" target="_blank"><span> <?php echo $child1->subname; ?> </span></a>
                              <?php
                              } //if child1 new tab true
                              elseif ($child1->newtab=="false") {
                                ?>
                                <a class="special-menu-text caps" href="<?php echo $child1->sublink; ?>"><span>
                                  <?php echo $child1->subname; ?>
                                </span></a>
                                <?php
                                  } //if child1 new tab false
                                  ?>
                                  <ul style="list-style:none">
                                   <?php

                                   foreach ($child2 as $child2) {
                                    if ($child2->subname != null) {
                                      if ($child2->parent == $child1->submenuID) {  
                                        ?>
                                        <li> <span class="glyphicon glyphicon-menu-right" style="color:#58666e;"></span>
                                          <?php
                                          if ($child2->newtab=="true") {
                                            ?>
                                            <a class="special-menu-text caps" href="<?php echo $child2->sublink; ?>" target="_blank"><span> <?php echo $child2->subname; ?> </span></a>
                                            <?php
                                          }
                                          elseif($child2->newtab=="false")
                                          {
                                            ?>
                                            <a class="special-menu-text caps" href="<?php echo $child2->sublink; ?>"><span>
                                              <?php echo $child2->subname;?>
                                            </span></a>
                                            <?php
                              } //newtab false
                              ?>

                              <ul style="list-style:none">
                                <?php
                                foreach ($child3 as $children3) {
                                  if ($children3->parent == $child2->submenuID) {

                                    ?> 
                                    <li> <span class="glyphicon glyphicon-menu-right" style="color:#58666e;"></span>
                                      <?php
                                      if ($children3->newtab=="true") {
                                        ?>
                                        <a class="special-menu-text caps" href="<?php echo $children3->sublink; ?>" target="_blank"><span> <?php echo $children3->subname; ?> </span></a>
                                        <?php
                                      }
                                      elseif($children3->newtab=="false")
                                      {
                                        ?>
                                        <a class="special-menu-text caps" href="<?php echo $children3->sublink; ?>"><span>
                                          <?php echo $children3->subname;?>
                                        </span></a>
                                        <?php
                              } //newtab false
                              ?>
                              <ul style="list-style:none">
                                <?php
                                foreach ($child4 as $children4) {
                                  if ($children4->parent == $children3->submenuID) {
                                    ?>
                                    <li> <span class="glyphicon glyphicon-menu-right" style="color:#58666e;"></span>
                                      <?php
                                      if ($children4->newtab=="true") {
                                        ?>
                                        <a class="special-menu-text caps" href="<?php echo $children4->sublink; ?>" target="_blank"><span> <?php echo $children4->subname; ?> </span></a>
                                        <?php
                                      }
                                      elseif($children4->newtab=="false")
                                      {
                                        ?>
                                        <a class="special-menu-text caps" href="<?php echo $children4->sublink; ?>"><span>
                                          <?php echo $children4->subname;?>
                                        </span></a>
                                        <?php
                                                  } //newtab false
                                                  ?>
                                                </li>
                                                <?php
                                              }
                                            }
                                            ?>
                                          </ul>
                                        </li>
                                        <?php
                                    }//if parent==menuid
                                  } //foreach child3
                                  ?>
                                </ul>

                              </li>
                              <?php

                                    } //if parent == menuid
                                  } // if child2 not null

                                } //foreach child2
                                ?>
                              </ul>
                            </li>
                            <?php
                              } //if child1      
                            } //foreach child1

                      } //foreach children
                      ?>
                    </ul>
                    <?php 
                  } //$children not 0

                  ?>
                </li>
              </ul> <!-- main menu -->
              <?php

              }//foreach
              ?>

            </nav>
          </div>

          </div>
                  <!-- Mobile menu ends -->
                    <?php
                }
                elseif($value->sidebar=='Testimonial')
                {
                    ?>
                      <hr class="styled-hr">
                     <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                                <?php $gettesti = $value->testimonial; ?>
                                <div class="specialpage_col">
                                    <?php
                                    $x = 0;
                                    foreach($gettesti as $k => $value) { ?>
                                <div class="size16 border-left italic margin-top special-testi-wrapper">
                                    <div class="row">
                                        <div class="col-sm-3 text-center page-testi-image specialpage-testi-image">
                                            <img src="<?php echo $imageLink.'/uploads/testimonialpic/'.$gettesti[$k]->picture; ?>" class="testi-img">
                                        </div>
                                        <div class="col-sm-9 specialpage-testi-text">
                                            <div></div>
                                            <div>
                                                <span class="fa fa-quote-left"></span>
                                                <?php echo $gettesti[$k]->message; ?>
                                             </div>
                                                <div class="text-right">- <?php echo $gettesti[$k]->name ?></div>
                                            </div>
                                        </div>
                                        <hr class="styled-hr">
                                </div>
                                 <?php $x = 1; } ?>
                                </div>
                    <?php
                }
                elseif($value->sidebar=='Link')
                {
                      if($value->url != '' && $value->label != ''){ ?>
                     <hr class="styled-hr">
                      <a target="_blank" href="<?php echo $value->url; ?>" class="page-link"><?php echo $value->label; ?></a>
                    <?php }?>
                    <?php
                }
                elseif($value->sidebar=='News')
                {
                    ?>
                     <hr class="styled-hr">
                  <div class="specialpage_col">
                                <div class="" ng-controller="NewsCtrl" ng-init="showmorenews()">
                                  <div class="row">
                                      <h4 class="blog-title-list">Latest News</h4>
                                      <div class="list-news-wrapper special-news-wrapper">
                                       <!--  <div ng-hide="newslist"> Loading News...</div> -->

                                        <?php
                                        foreach ($value->newslist as $news) {
                                           if ($news->videothumb) {
                                           $x = preg_match('/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/i',$news->videothumb,$match);
                                           $img = 'http://img.youtube.com/vi/'. $match[1] .'/hqdefault.jpg';
                                           $url = $match[1];
                                           }
                                           else
                                           {
                                            $img = $this->view->path = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                           }
                                           $imagethumb = $this->view->path = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                        ?>
                                        <div class="row list-title-blog specialpage-title-blog">
                                          <div class="col-sm-3 news-thumb-container specialpage-news-thumb" style="background-image: url('<?php echo $img; ?>')" ng-click="redirectNews('<?php echo $news->newsslugs; ?>');" title="{[{ news.title }]}">
                                            <img src="<?php echo $imagethumb;?>" class="pinterest-img" alt="<?php echo $news->title;?>">
                                           <?php if ($news->videothumb) {
                                            ?>
                                            <div class="youtube-play"><a href="/blog/<?php echo $news->newsslugs?>"><img src="img/youtubeplay.png"/></a></div>
                                         <?php
                                           } ?>

                                            <a href="/blog/<?php echo $news->newsslugs; ?>">
                                            </a>
                                        </div>


                                        <div class="news-list-desc specialpage-news-desc">
                                            <div class="row">

                                                <div class="">
                                                  <span class="size25 font1 news-title" ng-click="redirectNews('<?php echo $news->newsslugs; ?>');"><?php echo $news->title; ?></span>
                                              </div>
                                              <div class="">
                                                  <strong><span class="thin-font1 orange">
                                                  <?php 
                                                  foreach ($news->category as $k => $categorylist) {
                                                    if (count($news->category)-1==$k) {
                                                        echo $categorylist->categoryname;
                                                    }
                                                    else
                                                    {
                                                         echo $categorylist->categoryname.", ";
                                                    }
                                                   
                                                  }
                                                  ?>
                                                   </span></strong><span ng-show="news.name !=''" class="thin-font1"> / by <a href="/blog/author/<?php echo $news->name; ?>"><strong><span class="orange"><?php echo $news->name; ?></span></strong></a></span> / <?php echo $news->date; ?>
                                                  <br/><br/>
                                              </div>
                                              <div class="col-sm-12">
                                                  <div class="font1 size14 summary">
                                                    <?php echo $news->summary; ?>
                                                    <br/><br/>
                                                </div>
                                            </div>
                                            </div>
                                            <div style="clear:both"></div>
                                                    <br>
                                              </div>
                                          </div>

                                        <?php
                                        }
                                        ?>
                                            </div>
                                            </div>
                                            </div>
                                            </div>
                    <?php
                }
                elseif($value->sidebar=='Archive')
                {
                    ?>
                    <hr class="styled-hr">
                    <?php $getarchives = $value->archive; ?>
                    <h2 class="font1"><span class="fa fa-calendar fa-1x"></span> Archives</h2>
                    <div class="ul-archives">
                        <?php

                        foreach ($getarchives as $key => $value) { ?>
                        <a href="<?php echo $base_url; ?>/blog/archive/<?php echo $getarchives[$key]->month . "/" . $getarchives[$key]->year; ?>"><span class="fa fa-chevron-right"></span> <?php echo $getarchives[$key]->month . " " . $getarchives[$key]->year; ?></a>
                        <?php
                    } ?>
                </div>

                    <?php
                }
                elseif($value->sidebar=='Rss')
                {
                    ?>
                    <hr class="styled-hr"> <br/>
                    <p class="size16"> <a href="<?php echo $base_url;?>/feed" target="_blank"><img class="rss" src="img/frontend/rss_feed.gif"> RSS Feed</a></p> <br/> <br/>
                    <?php
                }
                
        } //FOREACH

      } //leftsidebar
    ?>
    </div>
        </div>
        <?php }
        if($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
        <div class="col-sm-12 special-maincontent">
        <?php } ?>
        <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'false' || $data->leftsidebar == 'false' && $data->rightsidebar == 'true' 
        || $data->leftsidebar == 'true' && $data->rightsidebar == NULL || $data->leftsidebar == NULL && $data->rightsidebar == 'true'){ ?>
        <div class="col-sm-10 special-maincontent">
        <?php } ?>  
        <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
        <div class="col-sm-8 special-maincontent">
        <?php } ?>   
         

          <!-- Main@@! -->
          <?php if($data->pagetype == "Normal"){ ?>

           <!-- <div class="normalpage-title margin-bot10"><span><?php
            echo $data->title;
            ?> </span></div> -->

           <div class="normalpage-body size14">
           <?php
            echo $content;
            ?> 
            </div>
          <?php }else{ ?>
        <!-- MAIN CONTENT-->

<!-- Modules@@! -->

           <div class="normalpage-body size14"  id="main-div">
           <?php

                foreach ($datarow as $key => $value) {
                      if($value == '1')
                      {
                                ?><div class="row"><?php
                                if($col1[$key]->module == 'TEXT'){
                                    ?> 
                                    <div class="col-sm-12 specialpage_col">
                                        <?php echo $col1[$key]->content; ?>
                                    </div>
                                    <?php
                                }

                                elseif($col1[$key]->module == 'IMAGE'){
                                    ?> 
                                    <div class="col-sm-12 specialpage_col">
                                        <?php if($col1[$key]->link==null){ ?>
                                        <div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $col1[$key]->image; ?>');
                                        background-size:cover;
                                        background-repeat:no-repeat;
                                        background-position:center center;
                                        min-height:400px;
                                        margin-bottom:4px"></div>
                                        <?php }else{ ?>
                                        <a href="<?php echo $col1[$key]->link ?>" target="_blank"><div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $col1[$key]->image; ?>');
                                        background-size:cover;
                                        background-repeat:no-repeat;
                                        background-position:center center;
                                        min-height:400px;
                                        margin-bottom:4px"></div></a>
                                        <?php  } ?>
                                    </div>
                                    <?php
                                }

                                elseif($col1[$key]->module == 'DIVIDER'){
                                    ?> 
                                    <div class="col-sm-12 specialpage_col">
                                        <hr style="height:<?php echo $col1[$key]->height; ?>px;background-color:<?php echo $col1[$key]->color; ?>">
                                    </div>
                                    <?php
                                }

                                elseif($col1[$key]->module == 'TESTIMONIAL'){
                                ?>
                                  <hr class="styled-hr">
                                 <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                                    <?php $gettesti = $col1[$key]->testimonial; ?>
                                    <div class="specialpage_col">
                                        <?php
                                        $x = 0;
                                        foreach($gettesti as $k => $value) { ?>
                                    <div class="size16 border-left italic margin-top special-testi-wrapper">
                                        <div class="row">
                                            <div class="col-sm-3 text-center page-testi-image">
                                                <img src="<?php echo $imageLink.'/uploads/testimonialpic/'.$gettesti[$k]->picture; ?>" class="testi-img">
                                            </div>
                                            <div class="col-sm-9">
                                                <div></div>
                                                <div>
                                                    <span class="fa fa-quote-left"></span>
                                                    <?php echo $gettesti[$k]->message; ?>
                                                 </div>
                                                    <div class="text-right">- <?php echo $gettesti[$k]->name ?></div>
                                                </div>
                                            </div>
                                            <hr class="styled-hr">
                                    </div>
                                     <?php $x = 1; } ?>
                                    </div>
                                <?php
                                }


                            elseif($col1[$key]->module == 'NEWS'){
                              ?> 
                                <div class="special-news-container">
                                <div class="col-sm-12 specialpage_colnews">
                                    <div class="margin-top40 margin-bot20" ng-controller="NewsCtrl">
                                      <div class="row">
                                           <h4 class="blog-title-list">Latest News</h4>
                                          <div class="list-news-wrapper special-news-wrapper">
                                            <?php
                                            foreach ($col1[$key]->news as $news) {
                                               if ($news->videothumb) {
                                               $x = preg_match('/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/i',$news->videothumb,$match);
                                               $img = 'http://img.youtube.com/vi/'. $match[1] .'/hqdefault.jpg';
                                               $url = $match[1];
                                               }
                                               else
                                               {
                                                $img = $this->view->path = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                               }
                                               $imagethumb = $this->view->path = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                            ?>
                                            <div class="row list-title-blog specialpage-title-blog">
                                              <div class="col-sm-3 news-thumb-container module1-news-thumb" style="background-image: url('<?php echo $img; ?>')" ng-click="redirectNews('<?php echo $news->newsslugs; ?>');" title="{[{ news.title }]}">
                                                <img src="<?php echo $imagethumb;?>" class="pinterest-img" alt="<?php echo $news->title;?>">
                                               <?php if ($news->videothumb) {
                                                ?>
                                                <div class="youtube-play"><a href="/blog/<?php echo $news->newsslugs?>"><img src="img/youtubeplay.png"/></a></div>
                                             <?php
                                               } ?>

                                                <a href="/blog/<?php echo $news->newsslugs; ?>">
                                                </a>
                                            </div>


                                            <div class="col-xs-8 col-md-7 news-list-desc">
                                                <div class="row">

                                                    <div class="">
                                                      <span class="size25 font1 news-title" ng-click="redirectNews('<?php echo $news->newsslugs; ?>');"><?php echo $news->title; ?></span>
                                                  </div>
                                                  <div class="">
                                                      <strong><span class="thin-font1 orange">
                                                      <?php 
                                                      foreach ($news->category as $k => $categorylist) {
                                                        if (count($news->category)-1==$k) {
                                                            echo $categorylist->categoryname;
                                                        }
                                                        else
                                                        {
                                                             echo $categorylist->categoryname.", ";
                                                        }
                                                       
                                                      }
                                                      ?>
                                                       </span></strong><span ng-show="news.name !=''" class="thin-font1"> / by <a href="/blog/author/<?php echo $news->name; ?>"><strong><span class="orange"><?php echo $news->name; ?></span></strong></a></span> / <?php echo $news->date; ?>
                                                      <br/><br/>
                                                  </div>
                                                  <div class="col-sm-12">
                                                      <div class="font1 size14 summary">
                                                        <?php echo $news->summary; ?>
                                                        <br/><br/>
                                                    </div>
                                                </div>
                                                </div>
                                                <div style="clear:both"></div>
                                                        <br>
                                                  </div>
                                              </div>

                                                <?php
                                    }
                                                  ?>
                                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <?php
                                              }
                                              elseif($col1[$key]->module == 'CONTACT'){
                                                  ?> 
                                                  <div class="col-sm-12 specialpage_col">
                                                     <div class="container" ng-controller="ContactusCtrl" id="">
                                                        <div class="row">
                                                          <br/>
                                                          <br/>
                                                          <br/>
                                                          <div class="col-sm-12">
                                                            <h1>Contact Us</h1>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form">
                                                              <!-- Contact form -->
                                                              <form name="formContactUs" class="form-validation booking ng-pristine ng-invalid ng-invalid-required" ng-submit="submit(feedback)">
                                                                <!-- Name -->
                                                                <div class="control-group margin-bot8">
                                                                  <div class="row">
                                                                    <label class="control-label size14 col-sm-12 hidden feedback-success" for="name" style="color:#6cb04b;">Thank you for contacting Sedona Healing Arts, your message was sent successfully.
                                                                      <br>We will review your inquiry and get back to you shortly.</label>
                                                                  </div>
                                                              </div>
                                                              <div class="control-group margin-bot8">
                                                                  <div class="row">
                                                                    <label class="control-label size14 col-sm-12" for="name">Subject
                                                                      <span class="red">*</span>
                                                                  </label>
                                                                  <div class="col-sm-12">
                                                                      <input type="text" class="form-control textbox margin-bot8" id="" required="required" ng-model="feedback.subject">
                                                                  </div>
                                                              </div>
                                                          </div>
                                                          <div class="control-group margin-bot8">
                                                              <div class="row">
                                                                <label class="control-label size14 col-sm-12" for="name">Name
                                                                  <span class="red">*</span>
                                                              </label>
                                                              <div class="col-sm-6">
                                                                  <input type="text" class="form-control textbox margin-bot8" id="fname" placeholder="First Name" required="required" ng-model="feedback.fname">
                                                              </div>
                                                              <div class="col-sm-6">
                                                                  <input type="text" class="form-control textbox" id="lname" placeholder="Last Name" required="required" ng-model="feedback.lname">
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <!-- Email -->
                                                      <div class="control-group margin-bot8">
                                                          <div class="row">
                                                            <label class="control-label size14 col-sm-12" for="email">Email
                                                              <span class="red">*</span>
                                                          </label>
                                                          <div class="col-sm-12">
                                                              <span class="red hidden booking-email">Invalid Email Address</span>
                                                              <input type="email" class="textbox form-control margin-bot8" id="email" required="required" ng-model="feedback.email" ng-change="onemail(feedback.email)">
                                                          </div>
                                                      </div>
                                                      </div>
                                                      <!-- Comment -->
                                                      <div class="control-group margin-bot8">
                                                          <div class="row">
                                                            <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                                              <span class="red">*</span>
                                                          </label>
                                                          <div class="col-sm-12">
                                                              <textarea class="form-control input-large textarea" id="feedback" rows="5" required="required" ng-model="feedback.message"></textarea>
                                                          </div>
                                                      </div>
                                                                  </div>
                                                                  <!-- Google recaptcha -->
                                                                <!--   <div class="control-group margin-bot8">
                                                                      <div class="row">
                                                                        <div class="col-sm-12">
                                                                          <div vc-recaptcha key="'6LfiYQsTAAAAAMZIe1BR0fLlSfJAdLxFMr4nzOlm'" on-create="setWidgetId(widgetId)" on-success="setResponse(response)" on-expire="cbExpiration()"></div>
                                                                      </div>
                                                                  </div>
                                                              </div> -->
                                                              <!-- Buttons -->
                                                              <div class="form-actions margin-bot8">
                                                                  <div class="row">
                                                                    <div class="col-sm-12">
                                                                      <button type="submit" class="btn btn-sedona" ng-disabled="formContactUs.$invalid||res==false">Submit</button>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                                  <!-- Buttons -->
                                                              </form>
                                                          </div>
                                                          <div class="clearfix"></div>
                                                      </div>

                                                              <div class="col-sm-6 storeinfo">
                                                                <div class="storeinfo-details">
                                                                  <h2 class="orange">Store Information</h2>
                                                                  <div class="title3">
                                                                    <span class="titleb">Phone:</span>
                                                                    <span class="thin-font3">(928) 282-3875</span>
                                                                    <br/>
                                                                    <span class="titleb">Email:</span>
                                                                    <span class="thin-font3">contact@sedonahealingarts.com</span>
                                                                    <br/>
                                                                    <span class="titleb">Hours:</span>
                                                                    <span class="thin-font3">Mon - Sun: 10 am - 7 pm</span>
                                                                    <br/>
                                                                    <br/>
                                                                </div>
                                                                <div class="title3">
                                                                    <span class="titleb text-top">Address:</span>
                                                                    <br/>
                                                                    <span class="thin-font3">201 State Route 179</span>
                                                                    <br/>
                                                                    <span class="thin-font3">Sedona, AZ 86336</span>
                                                                </div>

                                                                <div class="wsite-map">
                                                                    <iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 250px; margin-top: 10px; margin-bottom: 10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3273.673285579057!2d-111.7637428!3d34.8644358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872da1564d73fd15%3A0x3e31d123930491bf!2s201+Hwy+179%2C+Sedona%2C+AZ+86336%2C+USA!5e0!3m2!1sen!2sph!4v1434348998372"></iframe>
                                                                </div>






                                                                <div class="clearfix"></div>
                                                            </div>
                                                          </div>

                                                          </div>
                                                          </div>
                                                      </div>
                                                    <?php
                                              }

                                  ?></div><?php
                      }
                        //h@zel 2

                        elseif($value == '2'){ 
                            ?><div class="row"><?php
                            if($col1[$key]->module == 'TEXT'){
                            ?>
                                <div class="col-sm-6 specialpage_col">
                                    <?php echo $col1[$key]->content; ?>
                                </div>
                            <?php }

                            elseif($col1[$key]->module == 'IMAGE'){
                            ?> 
                                <div class="col-sm-6 specialpage_col">
                                    <?php if($col1[$key]->link==null){ ?>
                                    <div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $col1[$key]->image; ?>');
                                        background-size:cover;
                                        background-repeat:no-repeat;
                                        background-position:center center;
                                        min-height:350px;
                                        margin-bottom:4px"></div>
                                    <?php }else{ ?>
                                    <a href="<?php echo $col1[$key]->link ?>" target="_blank"><div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $col1[$key]->image; ?>');
                                        background-size:cover;
                                        background-repeat:no-repeat;
                                        background-position:center center;
                                        min-height:350px;
                                        margin-bottom:4px"></div></a>
                                    <?php  } ?>
                                </div>
                            <?php
                            }

                            elseif($col1[$key]->module == 'DIVIDER'){
                            ?> 
                                <div class="col-sm-6 specialpage_col">
                                    <hr style="height:<?php echo $col1[$key]->height; ?>px;background-color:<?php echo $col1[$key]->color; ?>">
                                </div>
                            <?php
                            }


                             elseif($col1[$key]->module == 'TESTIMONIAL'){
                                ?> 
                                <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                                <?php $gettesti = $col1[$key]->testimonial; ?>
                                <div class="col-sm-6 specialpage_col">
                                    <?php
                                    $x = 0;
                                    foreach($gettesti as $k => $value) { ?>
                                <div class="size16 border-left italic margin-left margin-top">
                                    <div class="row">
                                        <div class="col-sm-2 text-center">
                                            <img src="<?php echo $imageLink.'/uploads/testimonialpic/'.$gettesti[$k]->picture; ?>" class="testi-img">
                                        </div>
                                        <div class="col-sm-9">

                                            <div></div>
                                            <div>
                                                <span class="fa fa-quote-left"></span>
                                                <?php echo $gettesti[$k]->message; ?>
                                             </div>
                                                <div class="text-right">- <?php echo $gettesti[$k]->name ?></div>
                                            </div>
                                        </div>
                                        <hr class="styled-hr">
                                </div>
                                 <?php $x = 1; } ?>
                                </div>
                                <?php
                            }


                            elseif($col1[$key]->module == 'NEWS'){
                            ?> 
                                <div class="col-sm-6 specialpage_col">
                                    <div ng-controller="NewsCtrl">
                                             <h4 class="blog-title-list">Latest News</h4>
                                      <div class="list-news-wrapper special-news-wrapper">
                                        <?php
                                        foreach ($col1[$key]->news as $news) {
                                           if ($news->videothumb) {
                                           $x = preg_match('/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/i',$news->videothumb,$match);
                                           $img = 'http://img.youtube.com/vi/'. $match[1] .'/hqdefault.jpg';
                                           $url = $match[1];
                                           }
                                           else
                                           {
                                            $img = $this->view->path = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                           }
                                           $imagethumb = $this->view->path = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                        ?>
                                        <div class="row list-title-blog specialpage-title-blog">
                                          <div class="col-sm-3 news-thumb-container module2-news-thumb" style="background-image: url('<?php echo $img; ?>')" ng-click="redirectNews('<?php echo $news->newsslugs; ?>');" title="{[{ news.title }]}">
                                            <img src="<?php echo $imagethumb;?>" class="pinterest-img" alt="<?php echo $news->title;?>">
                                           <?php if ($news->videothumb) {
                                            ?>
                                            <div class="youtube-play"><a href="/blog/<?php echo $news->newsslugs?>"><img src="img/youtubeplay.png"/></a></div>
                                         <?php
                                           } ?>

                                            <a href="/blog/<?php echo $news->newsslugs; ?>">
                                            </a>
                                        </div>


                                        <div class="col-xs-8 col-md-7 news-list-desc module2-news-desc">
                                            <div class="row">

                                                <div class="">
                                                  <span class="size25 font1 news-title" ng-click="redirectNews('<?php echo $news->newsslugs; ?>');"><?php echo $news->title; ?></span>
                                              </div>
                                              <div class="">
                                                  <strong><span class="thin-font1 orange">
                                                  <?php 
                                                  foreach ($news->category as $k => $categorylist) {
                                                    if (count($news->category)-1==$k) {
                                                        echo $categorylist->categoryname;
                                                    }
                                                    else
                                                    {
                                                         echo $categorylist->categoryname.", ";
                                                    }
                                                   
                                                  }
                                                  ?>
                                                   </span></strong><span ng-show="news.name !=''" class="thin-font1"> / by <a href="/blog/author/<?php echo $news->name; ?>"><strong><span class="orange"><?php echo $news->name; ?></span></strong></a></span> / <?php echo $news->date; ?>
                                                  <br/><br/>
                                              </div>
                                              <div class="col-sm-12">
                                                  <div class="font1 size14 summary">
                                                    <?php echo $news->summary; ?>
                                                    <br/><br/>
                                                </div>
                                            </div>
                                            </div>
                                            <div style="clear:both"></div>
                                                    <br>
                                              </div>
                                          </div>

                                        <?php
                                        }
                                        ?>
                                            </div>
                                                </div>
                                    </div>
                            <?php
                            }



                            elseif($col1[$key]->module == 'CONTACT'){
                                ?> 
                                <div class="col-sm-6 specialpage_col">
                                   <div class="" ng-controller="ContactusCtrl" id="">
                                        <br/>
                                        <div class="col-sm-12 special-contact-us">
                                          <h1>Contact Us</h1>
                                        </div>
                                      <div class="">
                                          <div class="form">
                                            <!-- Contact form -->
                                            <form name="formContactUs" class="form-validation booking ng-pristine ng-invalid ng-invalid-required" ng-submit="submit(feedback)">
                                              <!-- Name -->
                                              <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12 hidden feedback-success" for="name" style="color:#6cb04b;">Thank you for contacting Sedona Healing Arts, your message was sent successfully.
                                                    <br>We will review your inquiry and get back to you shortly.</label>
                                                </div>
                                            </div>
                                            <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12" for="name">Subject
                                                    <span class="red">*</span>
                                                </label>
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control textbox margin-bot8" id="" required="required" ng-model="feedback.subject">
                                                </div>
                                            </div>
                                            </div>
                                            <div class="control-group margin-bot8">
                                            <div class="row">
                                              <label class="control-label size14 col-sm-12" for="name">Name
                                                <span class="red">*</span>
                                            </label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control textbox margin-bot8" id="fname" placeholder="First Name" required="required" ng-model="feedback.fname">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control textbox" id="lname" placeholder="Last Name" required="required" ng-model="feedback.lname">
                                            </div>
                                            </div>
                                            </div>
                                    <!-- Email -->
                                            <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12" for="email">Email
                                                    <span class="red">*</span>
                                                </label>
                                                <div class="col-sm-12">
                                                    <span class="red hidden booking-email">Invalid Email Address</span>
                                                    <input type="email" class="textbox form-control margin-bot8" id="email" required="required" ng-model="feedback.email" ng-change="onemail(feedback.email)">
                                                </div>
                                                </div>
                                            </div>
                                            <!-- Comment -->
                                        <div class="control-group margin-bot8">
                                            <div class="row">
                                              <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                                <span class="red">*</span>
                                              </label>
                                                <div class="col-sm-12">
                                                    <textarea class="form-control input-large textarea" id="feedback" rows="5" required="required" ng-model="feedback.message"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Google recaptcha -->
                                      <!--   <div class="control-group margin-bot8">
                                            <div class="row">
                                              <div class="col-sm-12">
                                                <div vc-recaptcha key="'6LfiYQsTAAAAAMZIe1BR0fLlSfJAdLxFMr4nzOlm'" on-create="setWidgetId(widgetId)" on-success="setResponse(response)" on-expire="cbExpiration()"></div>
                                            </div>
                                        </div>
                                        </div> -->
                                        <!-- Buttons -->
                                        <div class="form-actions margin-bot8">
                                            <div class="row">
                                              <div class="col-sm-12">
                                                <button type="submit" class="btn btn-sedona" ng-disabled="formContactUs.$invalid||res==false">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Buttons -->
                                    </form>
                                    </div>
                                <div class="clearfix"></div>
                                </div>
                                </div>
                                </div>
                            <?php
                            }

                            /////////////////////////////////////////////////////////COLUMN 2 ////////////////////////////
                            //h@zel 2-2
                            if($col2[$key]->module == 'TEXT'){
                            ?>
                                <div class="col-sm-6 specialpage_col">
                                    <?php echo $col2[$key]->content; ?>
                                </div>
                            <?php }

                            elseif($col2[$key]->module == 'IMAGE'){
                            ?> 
                                <div class="col-sm-6 specialpage_col">
                                    <?php if($col2[$key]->link==null){ ?>
                                    <div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $col2[$key]->image; ?>');
                                        background-size:cover;
                                        background-repeat:no-repeat;
                                        background-position:center center;
                                        min-height:350px;
                                        margin-bottom:4px"></div>
                                    <?php }else{ ?>
                                    <a href="<?php echo $col2[$key]->link ?>" target="_blank"><div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $col2[$key]->image; ?>');
                                        background-size:cover;
                                        background-repeat:no-repeat;
                                        background-position:center center;
                                        min-height:350px;
                                        margin-bottom:4px"></div></a>
                                    <?php  } ?>
                                </div>
                            <?php
                            }

                            elseif($col2[$key]->module == 'DIVIDER'){
                            ?> 
                                <div class="col-sm-6 specialpage_col">
                                    <hr style="height:<?php echo $col2[$key]->height; ?>px;background-color:<?php echo $col2[$key]->color; ?>">
                                </div>
                            <?php
                            }

                            elseif($col2[$key]->module == 'TESTIMONIAL'){
                                ?> 
                                <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                                <?php $gettesti1 = $col2[$key]->testimonial; ?>
                                <div class="col-sm-6 specialpage_col">
                                    <?php
                                    $x = 0;
                                    foreach($gettesti1 as $k => $value) { ?>
                                <div class="size16 border-left italic margin-left margin-top">
                                    <div class="row">
                                        <div class="col-sm-2 text-center">
                                            <img src="<?php echo $imageLink.'/uploads/testimonialpic/'.$gettesti1[$k]->picture; ?>" class="testi-img">
                                        </div>
                                        <div class="col-sm-9">
                                            <div></div>
                                            <div>
                                                <span class="fa fa-quote-left"></span>
                                                <?php echo $gettesti1[$k]->message; ?>
                                             </div>
                                                <div class="text-right">- <?php echo $gettesti1[$k]->name ?></div>
                                            </div>
                                        </div>
                                        <hr class="styled-hr">
                                </div>
                                 <?php $x = 1; } ?>
                                </div>
                                <?php
                            }


                            elseif($col2[$key]->module == 'NEWS'){
                            ?> 
                             
                                <div class="col-sm-6 specialpage_col">
                                    <div ng-controller="NewsCtrl" ng-init="showmorenews()">
                                          <h4 class="blog-title-list">Latest News</h4>
                                      <div class="list-news-wrapper special-news-wrapper">
                                        <?php
                                        foreach ($col2[$key]->news as $news) {
                                           if ($news->videothumb) {
                                           $x = preg_match('/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/i',$news->videothumb,$match);
                                           $img = 'http://img.youtube.com/vi/'. $match[1] .'/hqdefault.jpg';
                                           $url = $match[1];
                                           }
                                           else
                                           {
                                            $img = $this->view->path = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                           }
                                           $imagethumb = $this->view->path = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                        ?>
                                        <div class="row list-title-blog specialpage-title-blog">
                                          <div class="col-sm-3 news-thumb-container module2-news-thumb" style="background-image: url('<?php echo $img; ?>')" ng-click="redirectNews('<?php echo $news->newsslugs; ?>');" title="{[{ news.title }]}">
                                            <img src="<?php echo $imagethumb;?>" class="pinterest-img" alt="<?php echo $news->title;?>">
                                           <?php if ($news->videothumb) {
                                            ?>
                                            <div class="youtube-play"><a href="/blog/<?php echo $news->newsslugs?>"><img src="img/youtubeplay.png"/></a></div>
                                         <?php
                                           } ?>

                                            <a href="/blog/<?php echo $news->newsslugs; ?>">
                                            </a>
                                        </div>


                                        <div class="col-xs-8 col-md-7 news-list-desc module2-news-desc">
                                            <div class="row">

                                                <div class="">
                                                  <span class="size25 font1 news-title" ng-click="redirectNews('<?php echo $news->newsslugs; ?>');"><?php echo $news->title; ?></span>
                                              </div>
                                              <div class="">
                                                  <strong><span class="thin-font1 orange">
                                                  <?php 
                                                  foreach ($news->category as $k => $categorylist) {
                                                    if (count($news->category)-1==$k) {
                                                        echo $categorylist->categoryname;
                                                    }
                                                    else
                                                    {
                                                         echo $categorylist->categoryname.", ";
                                                    }
                                                   
                                                  }
                                                  ?>
                                                   </span></strong><span ng-show="news.name !=''" class="thin-font1"> / by <a href="/blog/author/<?php echo $news->name; ?>"><strong><span class="orange"><?php echo $news->name; ?></span></strong></a></span> / <?php echo $news->date; ?>
                                                  <br/><br/>
                                              </div>
                                              <div class="col-sm-12">
                                                  <div class="font1 size14 summary">
                                                    <?php echo $news->summary; ?>
                                                    <br/><br/>
                                                </div>
                                            </div>
                                            </div>
                                            <div style="clear:both"></div>
                                                    <br>
                                              </div>
                                          </div>

                                        <?php
                                        }
                                        ?>
                                            </div>
                                                </div>
                                    </div>
                            <?php
                            } 


                            elseif($col2[$key]->module == 'CONTACT'){
                                ?> 
                                <div class="col-sm-6 specialpage_col">
                                   <div class="" ng-controller="ContactusCtrl" id="">
                                        <br/>
                                        <div class="col-sm-12">
                                          <h1>Contact Us</h1>
                                        </div>
                                      <div class="">
                                          <div class="form">
                                            <!-- Contact form -->
                                            <form name="formContactUs" class="form-validation booking ng-pristine ng-invalid ng-invalid-required" ng-submit="submit(feedback)">
                                              <!-- Name -->
                                              <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12 hidden feedback-success" for="name" style="color:#6cb04b;">Thank you for contacting Sedona Healing Arts, your message was sent successfully.
                                                    <br>We will review your inquiry and get back to you shortly.</label>
                                                </div>
                                            </div>
                                            <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12" for="name">Subject
                                                    <span class="red">*</span>
                                                </label>
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control textbox margin-bot8" id="" required="required" ng-model="feedback.subject">
                                                </div>
                                            </div>
                                            </div>
                                            <div class="control-group margin-bot8">
                                            <div class="row">
                                              <label class="control-label size14 col-sm-12" for="name">Name
                                                <span class="red">*</span>
                                            </label>
                                            <div class="col-sm-6 specialpage-contact-fn">
                                                <input type="text" class="form-control textbox margin-bot8" id="fname" placeholder="First Name" required="required" ng-model="feedback.fname">
                                            </div>
                                            <div class="col-sm-6 specialpage-contact-fn">
                                                <input type="text" class="form-control textbox" id="lname" placeholder="Last Name" required="required" ng-model="feedback.lname">
                                            </div>
                                            </div>
                                            </div>
                                    <!-- Email -->
                                            <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12" for="email">Email
                                                    <span class="red">*</span>
                                                </label>
                                                <div class="col-sm-12">
                                                    <span class="red hidden booking-email">Invalid Email Address</span>
                                                    <input type="email" class="textbox form-control margin-bot8" id="email" required="required" ng-model="feedback.email" ng-change="onemail(feedback.email)">
                                                </div>
                                                </div>
                                            </div>
                                            <!-- Comment -->
                                        <div class="control-group margin-bot8">
                                            <div class="row">
                                              <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                                <span class="red">*</span>
                                              </label>
                                                <div class="col-sm-12">
                                                    <textarea class="form-control input-large textarea" id="feedback" rows="5" required="required" ng-model="feedback.message"></textarea>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- Google recaptcha -->
                                       <!--  <div class="control-group margin-bot8">
                                            <div class="row">
                                              <div class="col-sm-12">
                                                <div vc-recaptcha key="'6LfiYQsTAAAAAMZIe1BR0fLlSfJAdLxFMr4nzOlm'" on-create="setWidgetId(widgetId)" on-success="setResponse(response)" on-expire="cbExpiration()"></div>
                                            </div>
                                        </div>
                                        </div> -->
                                        <!-- Buttons -->
                                        <div class="form-actions margin-bot8">
                                            <div class="row">
                                              <div class="col-sm-12">
                                                <button type="submit" class="btn btn-sedona" ng-disabled="formContactUs.$invalid||res==false">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Buttons -->
                                    </form>
                                    </div>
                                <div class="clearfix"></div>
                                </div>
                                </div>
                                </div>
                            <?php
                            }
                            ?></div><?php
                        }
                        //h@zel 3
                        elseif($value == '3'){
                            ?><div class="row"><?php
                            if($col1[$key]->module == 'TEXT'){
                            ?>
                            <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-4 specialpage_col special-col3">
                            <?php } else{ ?>
                             <div class="col-sm-4 specialpage_col">
                            <?php }?>
                                    <?php echo $col1[$key]->content; ?>
                                </div>
                            <?php }

                            elseif($col1[$key]->module == 'IMAGE'){
                            ?> 
                            <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-4 specialpage_col special-col3">
                              <?php } else{ ?>
                              <div class="col-sm-4 specialpage_col">
                                <?php }?>
                                    <?php if($col1[$key]->link==null){ ?>
                                    <div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $col1[$key]->image; ?>');
                                        background-size:cover;
                                        background-repeat:no-repeat;
                                        background-position:center center;
                                        min-height:300px;
                                        margin-bottom:4px"></div>
                                    <?php }else{ ?>
                                    <a href="<?php echo $col1[$key]->link ?>" target="_blank"><div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $col1[$key]->image; ?>');
                                        background-size:cover;
                                        background-repeat:no-repeat;
                                        background-position:center center;
                                        min-height:300px;
                                        margin-bottom:4px"></div></a>
                                    <?php  } ?>
                                </div>
                            <?php
                            }

                            elseif($col1[$key]->module == 'DIVIDER'){
                            ?> 
                                 <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-4 specialpage_col special-col3">
                            <?php } else{ ?>
                             <div class="col-sm-4 specialpage_col">
                            <?php }?>

                                    <hr style="height:<?php echo $col1[$key]->height; ?>px;background-color:<?php echo $col1[$key]->color; ?>">
                                </div>
                            <?php
                            }


                            elseif($col1[$key]->module == 'TESTIMONIAL'){
                                ?> 
                                
                                <?php $gettesti1 = $col1[$key]->testimonial; ?>
                                 <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-4 specialpage_col special-col3">
                            <?php } else{ ?>
                             <div class="col-sm-4 specialpage_col">
                            <?php }?>
                                    <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                                    <?php
                                    $x = 0;
                                    foreach($gettesti1 as $k => $value) { ?>
                                <div class="size16 border-left italic margin-top special-testi-wrapper">
                                    <div class="row">
                                        <div class="col-sm-3 text-center specialpage-testi-image">
                                            <img src="<?php echo $imageLink.'/uploads/testimonialpic/'.$gettesti1[$k]->picture; ?>" class="testi-img">
                                        </div>
                                        <div class="col-sm-9 specialpage-testi-text">
                                            <div></div>
                                            <div>
                                                <span class="fa fa-quote-left"></span>
                                                <?php echo $gettesti1[$k]->message; ?>
                                             </div>
                                                <div class="text-right">- <?php echo $gettesti1[$k]->name ?></div>
                                            </div>
                                        </div>
                                        <hr class="styled-hr">
                                </div>
                                 <?php $x = 1; } ?>
                                </div>
                                <?php
                            }


                            elseif($col1[$key]->module == 'NEWS'){
                            ?> 
                            <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-4 specialpage_col special-col3">
                            <?php } else{ ?>
                             <div class="col-sm-4 specialpage_col">
                            <?php }?>
                             <!--  <div class="col-sm-4 specialpage_col special-col3"> -->
                                    <div ng-controller="NewsCtrl">
                                           <h4 class="blog-title-list">Latest News</h4>
                                      <div class="list-news-wrapper special-news-wrapper">
                                        <?php
                                        foreach ($col1[$key]->news as $news) {
                                           if ($news->videothumb) {
                                           $x = preg_match('/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/i',$news->videothumb,$match);
                                           $img = 'http://img.youtube.com/vi/'. $match[1] .'/hqdefault.jpg';
                                           $url = $match[1];
                                           }
                                           else
                                           {
                                            $img = $this->view->path = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                           }
                                           $imagethumb = $this->view->path = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                        ?>
                                        <div class="row list-title-blog specialpage-title-blog">
                                          <div class="col-sm-3 news-thumb-container module2-news-thumb" style="background-image: url('<?php echo $img; ?>')" ng-click="redirectNews('<?php echo $news->newsslugs; ?>');" title="{[{ news.title }]}">
                                            <img src="<?php echo $imagethumb;?>" class="pinterest-img" alt="<?php echo $news->title;?>">
                                           <?php if ($news->videothumb) {
                                            ?>
                                            <div class="youtube-play"><a href="/blog/<?php echo $news->newsslugs?>"><img src="img/youtubeplay.png"/></a></div>
                                         <?php
                                           } ?>

                                            <a href="/blog/<?php echo $news->newsslugs; ?>">
                                            </a>
                                        </div>


                                        <div class="col-xs-8 col-md-7 news-list-desc module2-news-desc">
                                            <div class="row">

                                                <div class="">
                                                  <span class="size25 font1 news-title" ng-click="redirectNews('<?php echo $news->newsslugs; ?>');"><?php echo $news->title; ?></span>
                                              </div>
                                              <div class="">
                                                  <strong><span class="thin-font1 orange">
                                                  <?php 
                                                  foreach ($news->category as $k => $categorylist) {
                                                    if (count($news->category)-1==$k) {
                                                        echo $categorylist->categoryname;
                                                    }
                                                    else
                                                    {
                                                         echo $categorylist->categoryname.", ";
                                                    }
                                                   
                                                  }
                                                  ?>
                                                   </span></strong><span ng-show="news.name !=''" class="thin-font1"> / by <a href="/blog/author/<?php echo $news->name; ?>"><strong><span class="orange"><?php echo $news->name; ?></span></strong></a></span> / <?php echo $news->date; ?>
                                                  <br/><br/>
                                              </div>
                                              <div class="col-sm-12">
                                                  <div class="font1 size14 summary">
                                                    <?php echo $news->summary; ?>
                                                    <br/><br/>
                                                </div>
                                            </div>
                                            </div>
                                            <div style="clear:both"></div>
                                                    <br>
                                              </div>
                                          </div>

                                        <?php
                                        }
                                        ?>
                                            </div>
                                                </div>
                                    </div>
                            <?php
                            }



                            elseif($col1[$key]->module == 'CONTACT'){
                                ?> 
                                <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                                <div class="col-sm-4 specialpage_col special-col3">
                                  <?php } else{ ?>
                                  <div class="col-sm-4 specialpage_col">
                                    <?php }?>
                                   <div class="" ng-controller="ContactusCtrl" id="">
                                        <br/>
                                        <div class="col-sm-12">
                                          <h1>Contact Us</h1>
                                        </div>
                                      <div class="">
                                          <div class="form">
                                            <!-- Contact form -->
                                            <form name="formContactUs" class="form-validation booking ng-pristine ng-invalid ng-invalid-required" ng-submit="submit(feedback)">
                                              <!-- Name -->
                                              <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12 hidden feedback-success" for="name" style="color:#6cb04b;">Thank you for contacting Sedona Healing Arts, your message was sent successfully.
                                                    <br>We will review your inquiry and get back to you shortly.</label>
                                                </div>
                                            </div>
                                            <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12" for="name">Subject
                                                    <span class="red">*</span>
                                                </label>
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control textbox margin-bot8" id="" required="required" ng-model="feedback.subject">
                                                </div>
                                            </div>
                                            </div>
                                            <div class="control-group margin-bot8">
                                            <div class="row">
                                              <label class="control-label size14 col-sm-12" for="name">Name
                                                <span class="red">*</span>
                                            </label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control textbox margin-bot8" id="fname" placeholder="First Name" required="required" ng-model="feedback.fname">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control textbox" id="lname" placeholder="Last Name" required="required" ng-model="feedback.lname">
                                            </div>
                                            </div>
                                            </div>
                                    <!-- Email -->
                                            <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12" for="email">Email
                                                    <span class="red">*</span>
                                                </label>
                                                <div class="col-sm-12">
                                                    <span class="red hidden booking-email">Invalid Email Address</span>
                                                    <input type="email" class="textbox form-control margin-bot8" id="email" required="required" ng-model="feedback.email" ng-change="onemail(feedback.email)">
                                                </div>
                                                </div>
                                            </div>
                                            <!-- Comment -->
                                        <div class="control-group margin-bot8">
                                            <div class="row">
                                              <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                                <span class="red">*</span>
                                              </label>
                                                <div class="col-sm-12">
                                                    <textarea class="form-control input-large textarea" id="feedback" rows="5" required="required" ng-model="feedback.message"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Google recaptcha -->
                                       <!--  <div class="control-group margin-bot8">
                                            <div class="row">
                                              <div class="col-sm-12">
                                                <div vc-recaptcha key="'6LfiYQsTAAAAAMZIe1BR0fLlSfJAdLxFMr4nzOlm'" on-create="setWidgetId(widgetId)" on-success="setResponse(response)" on-expire="cbExpiration()"></div>
                                            </div>
                                        </div>
                                        </div> -->
                                        <!-- Buttons -->
                                        <div class="form-actions margin-bot8">
                                            <div class="row">
                                              <div class="col-sm-12">
                                                <button type="submit" class="btn btn-sedona" ng-disabled="formContactUs.$invalid||res==false">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Buttons -->
                                    </form>
                                    </div>
                                <div class="clearfix"></div>
                                </div>
                                </div>
                                </div>
                            <?php
                            }

                            /////////////////////////////////////////////////////////COLUMN 2 ////////////////////////////
                            if($col2[$key]->module == 'TEXT'){
                            ?>
                            <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-4 specialpage_col special-col3">
                              <?php } else{ ?>
                              <div class="col-sm-4 specialpage_col">
                                <?php }?>
                                    <?php echo $col2[$key]->content; ?>
                                </div>
                            <?php }

                            elseif($col2[$key]->module == 'IMAGE'){
                            ?> 
                               <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-4 specialpage_col special-col3">
                              <?php } else{ ?>
                              <div class="col-sm-4 specialpage_col">
                                <?php }?>
                                    <?php if($col2[$key]->link==null){ ?>
                                    <div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $col2[$key]->image; ?>');
                                        background-size:cover;
                                        background-repeat:no-repeat;
                                        background-position:center center;
                                        min-height:300px;
                                        margin-bottom:4px"></div>
                                    <?php }else{ ?>
                                    <a href="<?php echo $col2[$key]->link ?>" target="_blank"><div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $col2[$key]->image; ?>');
                                        background-size:cover;
                                        background-repeat:no-repeat;
                                        background-position:center center;
                                        min-height:300px;
                                        margin-bottom:4px"></div></a>
                                    <?php  } ?>
                                </div>
                            <?php
                            }

                            elseif($col2[$key]->module == 'DIVIDER'){
                            ?> 
                                <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-4 specialpage_col special-col3">
                              <?php } else{ ?>
                              <div class="col-sm-4 specialpage_col">
                                <?php }?>
                                    <hr style="height:<?php echo $col2[$key]->height; ?>px;background-color:<?php echo $col2[$key]->color; ?>">
                                </div>
                            <?php
                            }

                            if($col2[$key]->module == 'TESTIMONIAL'){
                                ?> 
                               
                                <?php $gettesti1 = $col2[$key]->testimonial; ?>

                                <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-4 specialpage_col special-col3">
                              <?php } else{ ?>
                              <div class="col-sm-4 specialpage_col">
                                <?php }?>
                                <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                                    <?php
                                    $x = 0;
                                    foreach($gettesti1 as $k => $value) { ?>
                                <div class="size16 border-left italic margin-top special-testi-wrapper">
                                    <div class="row">
                                        <div class="col-sm-3 text-center specialpage-testi-image">
                                            <img src="<?php echo $imageLink.'/uploads/testimonialpic/'.$gettesti1[$k]->picture; ?>" class="testi-img">
                                        </div>
                                        <div class="col-sm-9 specialpage-testi-text">
                                            <div></div>
                                            <div>
                                                <span class="fa fa-quote-left"></span>
                                                <?php echo $gettesti1[$k]->message; ?>
                                             </div>
                                                <div class="text-right">- <?php echo $gettesti1[$k]->name ?></div>
                                            </div>
                                        </div>
                                        <hr class="styled-hr">
                                </div>
                                 <?php $x = 1; } ?>
                                </div>
                                <?php
                            }


                            elseif($col2[$key]->module == 'NEWS'){
                            ?> 
                               
                                <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-4 specialpage_col special-col3">
                              <?php } else{ ?>
                              <div class="col-sm-4 specialpage_col">
                                <?php }?>
                                    <div ng-controller="NewsCtrl">
                                           <h4 class="blog-title-list">Latest News</h4>
                                      <div class="list-news-wrapper special-news-wrapper">
                                        <?php
                                        foreach ($col2[$key]->news as $news) {
                                           if ($news->videothumb) {
                                           $x = preg_match('/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/i',$news->videothumb,$match);
                                           $img = 'http://img.youtube.com/vi/'. $match[1] .'/hqdefault.jpg';
                                           $url = $match[1];
                                           }
                                           else
                                           {
                                            $img = $this->view->path = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                           }
                                           $imagethumb = $this->view->path = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                        ?>
                                        <div class="row list-title-blog specialpage-title-blog">
                                          <div class="col-sm-3 news-thumb-container module2-news-thumb" style="background-image: url('<?php echo $img; ?>')" ng-click="redirectNews('<?php echo $news->newsslugs; ?>');" title="{[{ news.title }]}">
                                            <img src="<?php echo $imagethumb;?>" class="pinterest-img" alt="<?php echo $news->title;?>">
                                           <?php if ($news->videothumb) {
                                            ?>
                                            <div class="youtube-play"><a href="/blog/<?php echo $news->newsslugs?>"><img src="img/youtubeplay.png"/></a></div>
                                         <?php
                                           } ?>

                                            <a href="/blog/<?php echo $news->newsslugs; ?>">
                                            </a>
                                        </div>


                                        <div class="col-xs-8 col-md-7 news-list-desc module2-news-desc">
                                            <div class="row">

                                                <div class="">
                                                  <span class="size25 font1 news-title" ng-click="redirectNews('<?php echo $news->newsslugs; ?>');"><?php echo $news->title; ?></span>
                                              </div>
                                              <div class="">
                                                  <strong><span class="thin-font1 orange">
                                                  <?php 
                                                  foreach ($news->category as $k => $categorylist) {
                                                    if (count($news->category)-1==$k) {
                                                        echo $categorylist->categoryname;
                                                    }
                                                    else
                                                    {
                                                         echo $categorylist->categoryname.", ";
                                                    }
                                                   
                                                  }
                                                  ?>
                                                   </span></strong><span ng-show="news.name !=''" class="thin-font1"> / by <a href="/blog/author/<?php echo $news->name; ?>"><strong><span class="orange"><?php echo $news->name; ?></span></strong></a></span> / <?php echo $news->date; ?>
                                                  <br/><br/>
                                              </div>
                                              <div class="col-sm-12">
                                                  <div class="font1 size14 summary">
                                                    <?php echo $news->summary; ?>
                                                    <br/><br/>
                                                </div>
                                            </div>
                                            </div>
                                            <div style="clear:both"></div>
                                                    <br>
                                              </div>
                                          </div>

                                        <?php
                                        }
                                        ?>
                                            </div>
                                                </div>
                                    </div>
                            <?php
                            } 


                            elseif($col2[$key]->module == 'CONTACT'){
                                ?> 
                                <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-4 specialpage_col special-col3">
                              <?php } else{ ?>
                              <div class="col-sm-4 specialpage_col">
                                <?php }?>
                                   <div class="" ng-controller="ContactusCtrl" id="">
                                        <br/>
                                        <div class="col-sm-12">
                                          <h1>Contact Us</h1>
                                        </div>
                                      <div class="">
                                          <div class="form">
                                            <!-- Contact form -->
                                            <form name="formContactUs" class="form-validation booking ng-pristine ng-invalid ng-invalid-required" ng-submit="submit(feedback)">
                                              <!-- Name -->
                                              <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12 hidden feedback-success" for="name" style="color:#6cb04b;">Thank you for contacting Sedona Healing Arts, your message was sent successfully.
                                                    <br>We will review your inquiry and get back to you shortly.</label>
                                                </div>
                                            </div>
                                            <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12" for="name">Subject
                                                    <span class="red">*</span>
                                                </label>
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control textbox margin-bot8" id="" required="required" ng-model="feedback.subject">
                                                </div>
                                            </div>
                                            </div>
                                            <div class="control-group margin-bot8">
                                            <div class="row">
                                              <label class="control-label size14 col-sm-12" for="name">Name
                                                <span class="red">*</span>
                                            </label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control textbox margin-bot8" id="fname" placeholder="First Name" required="required" ng-model="feedback.fname">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control textbox" id="lname" placeholder="Last Name" required="required" ng-model="feedback.lname">
                                            </div>
                                            </div>
                                            </div>
                                    <!-- Email -->
                                            <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12" for="email">Email
                                                    <span class="red">*</span>
                                                </label>
                                                <div class="col-sm-12">
                                                    <span class="red hidden booking-email">Invalid Email Address</span>
                                                    <input type="email" class="textbox form-control margin-bot8" id="email" required="required" ng-model="feedback.email" ng-change="onemail(feedback.email)">
                                                </div>
                                                </div>
                                            </div>
                                            <!-- Comment -->
                                        <div class="control-group margin-bot8">
                                            <div class="row">
                                              <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                                <span class="red">*</span>
                                              </label>
                                                <div class="col-sm-12">
                                                    <textarea class="form-control input-large textarea" id="feedback" rows="5" required="required" ng-model="feedback.message"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Google recaptcha -->
                                        <!-- <div class="control-group margin-bot8">
                                            <div class="row">
                                              <div class="col-sm-12">
                                                <div vc-recaptcha key="'6LfiYQsTAAAAAMZIe1BR0fLlSfJAdLxFMr4nzOlm'" on-create="setWidgetId(widgetId)" on-success="setResponse(response)" on-expire="cbExpiration()"></div>
                                            </div>
                                        </div>
                                        </div> -->
                                        <!-- Buttons -->
                                        <div class="form-actions margin-bot8">
                                            <div class="row">
                                              <div class="col-sm-12">
                                                <button type="submit" class="btn btn-sedona" ng-disabled="formContactUs.$invalid||res==false">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Buttons -->
                                    </form>
                                    </div>
                                <div class="clearfix"></div>
                                </div>
                                </div>
                                </div>
                            <?php
                            }


                            /////////////////////////////////////////////////////////COLUMN 3 ////////////////////////////
                            //h@zel 33
                            if($col3[$key]->module == 'TEXT'){
                            ?>
                                <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-4 specialpage_col special-col33">
                              <?php } else{ ?>
                              <div class="col-sm-4 specialpage_col">
                                <?php }?>
                                    <?php echo $col3[$key]->content; ?>
                                </div>
                            <?php }

                            elseif($col3[$key]->module == 'IMAGE'){
                            ?> 
                                <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-4 specialpage_col special-col33">
                              <?php } else{ ?>
                              <div class="col-sm-4 specialpage_col">
                                <?php }?>
                                    <?php if($col3[$key]->link==null){ ?>
                                    <div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $col3[$key]->image; ?>');
                                        background-size:cover;
                                        background-repeat:no-repeat;
                                        background-position:center center;
                                        min-height:300px;
                                        margin-bottom:4px"></div>
                                    <?php }else{ ?>
                                    <a href="<?php echo $col3[$key]->link ?>" target="_blank"><div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $col3[$key]->image; ?>');
                                        background-size:cover;
                                        background-repeat:no-repeat;
                                        background-position:center center;
                                        min-height:300px;
                                        margin-bottom:4px"></div></a>
                                    <?php  } ?>
                                </div>
                            <?php
                            }

                            elseif($col3[$key]->module == 'DIVIDER'){
                            ?> 
                                <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-4 specialpage_col special-col33">
                              <?php } else{ ?>
                              <div class="col-sm-4 specialpage_col">
                                <?php }?>
                                    <hr style="height:<?php echo $col3[$key]->height; ?>px;background-color:<?php echo $col3[$key]->color; ?>">
                                </div>
                            <?php
                            }

                            elseif($col3[$key]->module == 'TESTIMONIAL'){
                                ?> 
                               
                                <?php $gettesti1 = $col3[$key]->testimonial; ?>
                               <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-4 specialpage_col special-col33">
                              <?php } else{ ?>
                              <div class="col-sm-4 specialpage_col">
                                <?php }?>
                                    <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                                    <?php
                                    $x = 0;
                                    foreach($gettesti1 as $k => $value) { ?>
                                <div class="size16 border-left italic margin-top special-testi-wrapper">
                                    <div class="row">
                                        <div class="col-sm-3 text-center specialpage-testi-image">
                                            <img src="<?php echo $imageLink.'/uploads/testimonialpic/'.$gettesti1[$k]->picture; ?>" class="testi-img">
                                        </div>
                                        <div class="col-sm-9 specialpage-testi-text">
                                            <div></div>
                                            <div>
                                                <span class="fa fa-quote-left"></span>
                                                <?php echo $gettesti1[$k]->message; ?>
                                             </div>
                                                <div class="text-right">- <?php echo $gettesti1[$k]->name ?></div>
                                            </div>
                                        </div>
                                        <hr class="styled-hr">
                                </div>
                                 <?php $x = 1; } ?>
                                </div>
                                <?php
                            }


                            elseif($col3[$key]->module == 'NEWS'){
                            ?> 
                               
                                <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-4 specialpage_col special-col33">
                              <?php } else{ ?>
                              <div class="col-sm-4 specialpage_col">
                                <?php }?>
                                    <div ng-controller="NewsCtrl" ng-init="showmorenews()">
                                          <h4 class="blog-title-list">Latest News</h4>
                                      <div class="list-news-wrapper special-news-wrapper">
                                        <?php
                                        foreach ($col3[$key]->news as $news) {
                                           if ($news->videothumb) {
                                           $x = preg_match('/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/i',$news->videothumb,$match);
                                           $img = 'http://img.youtube.com/vi/'. $match[1] .'/hqdefault.jpg';
                                           $url = $match[1];
                                           }
                                           else
                                           {
                                            $img = $this->view->path = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                           }
                                           $imagethumb = $this->view->path = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                        ?>
                                        <div class="row list-title-blog specialpage-title-blog">
                                          <div class="col-sm-3 news-thumb-container module2-news-thumb" style="background-image: url('<?php echo $img; ?>')" ng-click="redirectNews('<?php echo $news->newsslugs; ?>');" title="{[{ news.title }]}">
                                            <img src="<?php echo $imagethumb;?>" class="pinterest-img" alt="<?php echo $news->title;?>">
                                           <?php if ($news->videothumb) {
                                            ?>
                                            <div class="youtube-play"><a href="/blog/<?php echo $news->newsslugs?>"><img src="img/youtubeplay.png"/></a></div>
                                         <?php
                                           } ?>

                                            <a href="/blog/<?php echo $news->newsslugs; ?>">
                                            </a>
                                        </div>


                                        <div class="col-xs-8 col-md-7 news-list-desc module2-news-desc">
                                            <div class="row">

                                                <div class="">
                                                  <span class="size25 font1 news-title" ng-click="redirectNews('<?php echo $news->newsslugs; ?>');"><?php echo $news->title; ?></span>
                                              </div>
                                              <div class="">
                                                  <strong><span class="thin-font1 orange">
                                                  <?php 
                                                  foreach ($news->category as $k => $categorylist) {
                                                    if (count($news->category)-1==$k) {
                                                        echo $categorylist->categoryname;
                                                    }
                                                    else
                                                    {
                                                         echo $categorylist->categoryname.", ";
                                                    }
                                                   
                                                  }
                                                  ?>
                                                   </span></strong><span ng-show="news.name !=''" class="thin-font1"> / by <a href="/blog/author/<?php echo $news->name; ?>"><strong><span class="orange"><?php echo $news->name; ?></span></strong></a></span> / <?php echo $news->date; ?>
                                                  <br/><br/>
                                              </div>
                                              <div class="col-sm-12">
                                                  <div class="font1 size14 summary">
                                                    <?php echo $news->summary; ?>
                                                    <br/><br/>
                                                </div>
                                            </div>
                                            </div>
                                            <div style="clear:both"></div>
                                                    <br>
                                              </div>
                                          </div>

                                        <?php
                                        }
                                        ?>
                                            </div>
                                                </div>
                                    </div>
                            <?php
                            } 


                            elseif($col3[$key]->module == 'CONTACT'){
                                ?> 
                                <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-4 specialpage_col special-col33">
                              <?php } else{ ?>
                              <div class="col-sm-4 specialpage_col">
                                <?php }?>
                                   <div class="" ng-controller="ContactusCtrl" id="">
                                        <br/>
                                        <div class="col-sm-12">
                                          <h1>Contact Us</h1>
                                        </div>
                                      <div class="">
                                          <div class="form">
                                            <!-- Contact form -->
                                            <form name="formContactUs" class="form-validation booking ng-pristine ng-invalid ng-invalid-required" ng-submit="submit(feedback)">
                                              <!-- Name -->
                                              <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12 hidden feedback-success" for="name" style="color:#6cb04b;">Thank you for contacting Sedona Healing Arts, your message was sent successfully.
                                                    <br>We will review your inquiry and get back to you shortly.</label>
                                                </div>
                                            </div>
                                            <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12" for="name">Subject
                                                    <span class="red">*</span>
                                                </label>
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control textbox margin-bot8" id="" required="required" ng-model="feedback.subject">
                                                </div>
                                            </div>
                                            </div>
                                            <div class="control-group margin-bot8">
                                            <div class="row">
                                              <label class="control-label size14 col-sm-12" for="name">Name
                                                <span class="red">*</span>
                                            </label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control textbox margin-bot8" id="fname" placeholder="First Name" required="required" ng-model="feedback.fname">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control textbox" id="lname" placeholder="Last Name" required="required" ng-model="feedback.lname">
                                            </div>
                                            </div>
                                            </div>
                                    <!-- Email -->
                                            <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12" for="email">Email
                                                    <span class="red">*</span>
                                                </label>
                                                <div class="col-sm-12">
                                                    <span class="red hidden booking-email">Invalid Email Address</span>
                                                    <input type="email" class="textbox form-control margin-bot8" id="email" required="required" ng-model="feedback.email" ng-change="onemail(feedback.email)">
                                                </div>
                                                </div>
                                            </div>
                                            <!-- Comment -->
                                        <div class="control-group margin-bot8">
                                            <div class="row">
                                              <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                                <span class="red">*</span>
                                              </label>
                                                <div class="col-sm-12">
                                                    <textarea class="form-control input-large textarea" id="feedback" rows="5" required="required" ng-model="feedback.message"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Google recaptcha -->
                                     <!--    <div class="control-group margin-bot8">
                                            <div class="row">
                                              <div class="col-sm-12">
                                                <div vc-recaptcha key="'6LfiYQsTAAAAAMZIe1BR0fLlSfJAdLxFMr4nzOlm'" on-create="setWidgetId(widgetId)" on-success="setResponse(response)" on-expire="cbExpiration()"></div>
                                            </div>
                                        </div>
                                        </div> -->
                                        <!-- Buttons -->
                                        <div class="form-actions margin-bot8">
                                            <div class="row">
                                              <div class="col-sm-12">
                                                <button type="submit" class="btn btn-sedona" ng-disabled="formContactUs.$invalid||res==false">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Buttons -->
                                    </form>
                                    </div>
                                <div class="clearfix"></div>
                                </div>
                                </div>
                                </div>
                            <?php
                            }
                            //////
                            ?></div><?php
                        }
                        //h@zel 4
                        elseif($value == '4'){
                            ?><div class="row"><?php
                            if($col1[$key]->module == 'TEXT'){
                            ?>
                                 <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-3 specialpage_col special-col4">
                              <?php } else if ($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
                              <div class="col-sm-3 specialpage_col">
                                <?php } else{?>
                                <div class="col-sm-3 specialpage_col  special-col4-no-one">
                                <?php } ?>

                                    <?php echo $col1[$key]->content; ?>
                                </div>
                            <?php }

                            elseif($col1[$key]->module == 'IMAGE'){
                            ?> 
                                 <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-3 specialpage_col special-col4">
                              <?php } else if ($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
                              <div class="col-sm-3 specialpage_col ">
                                <?php } else{?>
                                <div class="col-sm-3 specialpage_col special-col4-no-one">
                                <?php } ?>

                                    <?php if($col1[$key]->link==null){ ?>
                                    <div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $col1[$key]->image; ?>');
                                        background-size:cover;
                                        background-repeat:no-repeat;
                                        background-position:center center;
                                        min-height:200px;
                                        margin-bottom:4px"></div>
                                    <?php }else{ ?>
                                    <a href="<?php echo $col1[$key]->link ?>" target="_blank"><div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $col1[$key]->image; ?>');
                                        background-size:cover;
                                        background-repeat:no-repeat;
                                        background-position:center center;
                                        min-height:200px;
                                        margin-bottom:4px"></div></a>
                                    <?php  } ?>
                                </div>
                            <?php
                            }

                            elseif($col1[$key]->module == 'DIVIDER'){
                            ?> 
                                 <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-3 specialpage_col special-col4">
                              <?php } else if ($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
                              <div class="col-sm-3 specialpage_col">
                                <?php } else{?>
                                <div class="col-sm-3 specialpage_col  special-col4-no-one">
                                <?php } ?>
                                    <hr style="height:<?php echo $col1[$key]->height; ?>px;background-color:<?php echo $col1[$key]->color; ?>">
                                </div>
                            <?php
                            }


                            elseif($col1[$key]->module == 'TESTIMONIAL'){
                                ?> 
                                
                                <?php $gettesti1 = $col1[$key]->testimonial; ?>
                                <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-3 specialpage_col special-col4">
                              <?php } else if ($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
                              <div class="col-sm-3 specialpage_col">
                                <?php } else{?>
                                <div class="col-sm-3 specialpage_col  special-col4-no-one">
                                <?php } ?>
                                   <div class="size16"><h2 class="font1 italic">What People Are Saying</h2></div>
                                    <?php
                                    $x = 0;
                                    foreach($gettesti1 as $k => $value) { ?>
                                <div class="size16 border-left italic margin-top special-testi-wrapper">
                                    <div class="row">
                                    <div class="col-sm-2 text-center page-testi-image specialpage-testi-image">
                                            <img src="<?php echo $imageLink.'/uploads/testimonialpic/'.$gettesti1[$k]->picture; ?>" class="testi-img">
                                        </div>
                                        <div class="col-sm-9 specialpage-testi-text">
                                            <div></div>
                                            <div>
                                                <span class="fa fa-quote-left"></span>
                                                <?php echo $gettesti1[$k]->message; ?>
                                             </div>
                                                <div class="text-right">- <?php echo $gettesti1[$k]->name ?></div>
                                            </div>
                                        </div>
                                        <hr class="styled-hr">
                                </div>
                                 <?php $x = 1; } ?>
                                </div>
                                <?php
                            }


                            elseif($col1[$key]->module == 'NEWS'){
                            ?> 
                            <!--  <hr class="styled-hr"> -->
                                
                                 <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-3 specialpage_col special-col4">
                              <?php } else if ($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
                              <div class="col-sm-3 specialpage_col">
                                <?php } else{?>
                                <div class="col-sm-3 specialpage_col  special-col4-no-one">
                                <?php } ?>
                                    <div ng-controller="NewsCtrl">
                                          <h4 class="blog-title-list">Latest News</h4>
                                      <div class="list-news-wrapper special-news-wrapper">
                                        <?php
                                        foreach ($col1[$key]->news as $news) {
                                           if ($news->videothumb) {
                                           $x = preg_match('/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/i',$news->videothumb,$match);
                                           $img = 'http://img.youtube.com/vi/'. $match[1] .'/hqdefault.jpg';
                                           $url = $match[1];
                                           }
                                           else
                                           {
                                            $img = $this->view->path = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                           }
                                           $imagethumb = $this->view->path = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                        ?>
                                        <div class="row list-title-blog specialpage-title-blog">
                                          <div class="col-sm-3 news-thumb-container specialpage-news-thumb" style="background-image: url('<?php echo $img; ?>')" ng-click="redirectNews('<?php echo $news->newsslugs; ?>');" title="{[{ news.title }]}">
                                            <img src="<?php echo $imagethumb;?>" class="pinterest-img" alt="<?php echo $news->title;?>">
                                           <?php if ($news->videothumb) {
                                            ?>
                                            <div class="youtube-play"><a href="/blog/<?php echo $news->newsslugs?>"><img src="img/youtubeplay.png"/></a></div>
                                         <?php
                                           } ?>

                                            <a href="/blog/<?php echo $news->newsslugs; ?>">
                                            </a>
                                        </div>


                                        <div class="col-xs-8 col-md-7 news-list-desc specialpage-news-desc">
                                            <div class="row">

                                                <div class="">
                                                  <span class="size25 font1 news-title" ng-click="redirectNews('<?php echo $news->newsslugs; ?>');"><?php echo $news->title; ?></span>
                                              </div>
                                              <div class="">
                                                  <strong><span class="thin-font1 orange">
                                                  <?php 
                                                  foreach ($news->category as $k => $categorylist) {
                                                    if (count($news->category)-1==$k) {
                                                        echo $categorylist->categoryname;
                                                    }
                                                    else
                                                    {
                                                         echo $categorylist->categoryname.", ";
                                                    }
                                                   
                                                  }
                                                  ?>
                                                   </span></strong><span ng-show="news.name !=''" class="thin-font1"> / by <a href="/blog/author/<?php echo $news->name; ?>"><strong><span class="orange"><?php echo $news->name; ?></span></strong></a></span> / <?php echo $news->date; ?>
                                                  <br/><br/>
                                              </div>
                                              <div class="col-sm-12">
                                                  <div class="font1 size14 summary">
                                                    <?php echo $news->summary; ?>
                                                    <br/><br/>
                                                </div>
                                            </div>
                                            </div>
                                            <div style="clear:both"></div>
                                                    <br>
                                              </div>
                                          </div>

                                        <?php
                                        }
                                        ?>
                                            </div>
                                                </div>
                                    </div>
                            <?php
                            }



                            elseif($col1[$key]->module == 'CONTACT'){
                                ?> 
                                  <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-3 specialpage_col special-col4">
                              <?php } else if ($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
                              <div class="col-sm-3 specialpage_col">
                                <?php } else{?>
                                <div class="col-sm-3 specialpage_col  special-col4-no-one">
                                <?php } ?>
                                   <div class="" ng-controller="ContactusCtrl" id="">
                                        <br/>
                                        <div class="col-sm-12">
                                          <h1>Contact Us</h1>
                                        </div>
                                      <div class="">
                                          <div class="form">
                                            <!-- Contact form -->
                                            <form name="formContactUs" class="form-validation booking ng-pristine ng-invalid ng-invalid-required" ng-submit="submit(feedback)">
                                              <!-- Name -->
                                              <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12 hidden feedback-success" for="name" style="color:#6cb04b;">Thank you for contacting Sedona Healing Arts, your message was sent successfully.
                                                    <br>We will review your inquiry and get back to you shortly.</label>
                                                </div>
                                            </div>
                                            <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12" for="name">Subject
                                                    <span class="red">*</span>
                                                </label>
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control textbox margin-bot8" id="" required="required" ng-model="feedback.subject">
                                                </div>
                                            </div>
                                            </div>
                                            <div class="control-group margin-bot8">
                                            <div class="row">
                                              <label class="control-label size14 col-sm-12" for="name">Name
                                                <span class="red">*</span>
                                            </label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control textbox margin-bot8" id="fname" placeholder="First Name" required="required" ng-model="feedback.fname">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control textbox" id="lname" placeholder="Last Name" required="required" ng-model="feedback.lname">
                                            </div>
                                            </div>
                                            </div>
                                    <!-- Email -->
                                            <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12" for="email">Email
                                                    <span class="red">*</span>
                                                </label>
                                                <div class="col-sm-12">
                                                    <span class="red hidden booking-email">Invalid Email Address</span>
                                                    <input type="email" class="textbox form-control margin-bot8" id="email" required="required" ng-model="feedback.email" ng-change="onemail(feedback.email)">
                                                </div>
                                                </div>
                                            </div>
                                            <!-- Comment -->
                                        <div class="control-group margin-bot8">
                                            <div class="row">
                                              <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                                <span class="red">*</span>
                                              </label>
                                                <div class="col-sm-12">
                                                    <textarea class="form-control input-large textarea" id="feedback" rows="5" required="required" ng-model="feedback.message"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Google recaptcha -->
                                      <!--   <div class="control-group margin-bot8">
                                            <div class="row">
                                              <div class="col-sm-12">
                                                <div vc-recaptcha key="'6LfiYQsTAAAAAMZIe1BR0fLlSfJAdLxFMr4nzOlm'" on-create="setWidgetId(widgetId)" on-success="setResponse(response)" on-expire="cbExpiration()"></div>
                                            </div>
                                        </div>
                                        </div> -->
                                        <!-- Buttons -->
                                        <div class="form-actions margin-bot8">
                                            <div class="row">
                                              <div class="col-sm-12">
                                                <button type="submit" class="btn btn-sedona" ng-disabled="formContactUs.$invalid||res==false">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Buttons -->
                                    </form>
                                    </div>
                                <div class="clearfix"></div>
                                </div>
                                </div>
                                </div>
                            <?php
                            }

                            /////////////////////////////////////////////////////////COLUMN 2 ////////////////////////////

                            if($col2[$key]->module == 'TEXT'){
                            ?>
                                 <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-3 specialpage_col special-col4">
                              <?php } else if ($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
                              <div class="col-sm-3 specialpage_col">
                                <?php } else{?>
                                <div class="col-sm-3 specialpage_col special-col4-no-one">
                                <?php } ?>

                                    <?php echo $col2[$key]->content; ?>
                                </div>
                            <?php }

                            elseif($col2[$key]->module == 'IMAGE'){
                            ?> 
                                  <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-3 specialpage_col special-col4">
                              <?php } else if ($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
                              <div class="col-sm-3 specialpage_col ">
                                <?php } else{?>
                                <div class="col-sm-3 specialpage_col special-col4-no-one">
                                <?php } ?>

                                    <?php if($col2[$key]->link==null){ ?>
                                    <div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $col2[$key]->image; ?>');
                                        background-size:cover;
                                        background-repeat:no-repeat;
                                        background-position:center center;
                                        min-height:200px;
                                        margin-bottom:4px"></div>
                                    <?php }else{ ?>
                                    <a href="<?php echo $col2[$key]->link ?>" target="_blank"><div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $col2[$key]->image; ?>');
                                        background-size:cover;
                                        background-repeat:no-repeat;
                                        background-position:center center;
                                        min-height:200px;
                                        margin-bottom:4px"></div></a>
                                    <?php  } ?>
                                </div>
                            <?php
                            }

                            elseif($col2[$key]->module == 'DIVIDER'){
                            ?> 
                                <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-3 specialpage_col special-col4">
                              <?php } else if ($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
                              <div class="col-sm-3 specialpage_col ">
                                <?php } else{?>
                                <div class="col-sm-3 specialpage_col special-col4-no-one">
                                <?php } ?>

                                    <hr style="height:<?php echo $col2[$key]->height; ?>px;background-color:<?php echo $col2[$key]->color; ?>">
                                </div>
                            <?php
                            }

                            elseif($col2[$key]->module == 'TESTIMONIAL'){
                                ?> 
                                
                                <?php $gettesti1 = $col2[$key]->testimonial; ?>
                                 <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-3 specialpage_col special-col4">
                              <?php } else if ($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
                              <div class="col-sm-3 specialpage_col ">
                                <?php } else{?>
                                <div class="col-sm-3 specialpage_col special-col4-no-one">
                                <?php } ?>
                                <div class="size16"><h2 class="font1 italic">What People Are Saying</h2></div>
                                    <?php
                                    $x = 0;
                                    foreach($gettesti1 as $k => $value) { ?>
                                <div class="size16 border-left italic margin-top special-testi-wrapper">
                                    <div class="row">
                                    <div class="col-sm-2 text-center page-testi-image specialpage-testi-image">
                                            <img src="<?php echo $imageLink.'/uploads/testimonialpic/'.$gettesti1[$k]->picture; ?>" class="testi-img">
                                        </div>
                                        <div class="col-sm-9 specialpage-testi-text">
                                            <div></div>
                                            <div>
                                                <span class="fa fa-quote-left"></span>
                                                <?php echo $gettesti1[$k]->message; ?>
                                             </div>
                                                <div class="text-right">- <?php echo $gettesti1[$k]->name ?></div>
                                            </div>
                                        </div>
                                        <hr class="styled-hr">
                                </div>
                                 <?php $x = 1; } ?>
                                </div>
                                <?php
                            }


                            elseif($col2[$key]->module == 'NEWS'){
                            ?> 
                                 <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-3 specialpage_col special-col4">
                              <?php } else if ($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
                              <div class="col-sm-3 specialpage_col">
                                <?php } else{?>
                                <div class="col-sm-3 specialpage_col  special-col4-no-one">
                                <?php } ?>
                                    <div ng-controller="NewsCtrl">
                                          <h4 class="blog-title-list">Latest News</h4>
                                      <div class="list-news-wrapper special-news-wrapper">
                                        <?php
                                        foreach ($col2[$key]->news as $news) {
                                           if ($news->videothumb) {
                                           $x = preg_match('/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/i',$news->videothumb,$match);
                                           $img = 'http://img.youtube.com/vi/'. $match[1] .'/hqdefault.jpg';
                                           $url = $match[1];
                                           }
                                           else
                                           {
                                            $img = $this->view->path = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                           }
                                           $imagethumb = $this->view->path = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                        ?>
                                        <div class="row list-title-blog specialpage-title-blog">
                                          <div class="col-sm-3 news-thumb-container module2-news-thumb" style="background-image: url('<?php echo $img; ?>')" ng-click="redirectNews('<?php echo $news->newsslugs; ?>');" title="{[{ news.title }]}">
                                            <img src="<?php echo $imagethumb;?>" class="pinterest-img" alt="<?php echo $news->title;?>">
                                           <?php if ($news->videothumb) {
                                            ?>
                                            <div class="youtube-play"><a href="/blog/<?php echo $news->newsslugs?>"><img src="img/youtubeplay.png"/></a></div>
                                         <?php
                                           } ?>

                                            <a href="/blog/<?php echo $news->newsslugs; ?>">
                                            </a>
                                        </div>


                                        <div class="col-xs-8 col-md-7 news-list-desc module2-news-desc">
                                            <div class="row">

                                                <div class="">
                                                  <span class="size25 font1 news-title" ng-click="redirectNews('<?php echo $news->newsslugs; ?>');"><?php echo $news->title; ?></span>
                                              </div>
                                              <div class="">
                                                  <strong><span class="thin-font1 orange">
                                                  <?php 
                                                  foreach ($news->category as $k => $categorylist) {
                                                    if (count($news->category)-1==$k) {
                                                        echo $categorylist->categoryname;
                                                    }
                                                    else
                                                    {
                                                         echo $categorylist->categoryname.", ";
                                                    }
                                                   
                                                  }
                                                  ?>
                                                   </span></strong><span ng-show="news.name !=''" class="thin-font1"> / by <a href="/blog/author/<?php echo $news->name; ?>"><strong><span class="orange"><?php echo $news->name; ?></span></strong></a></span> / <?php echo $news->date; ?>
                                                  <br/><br/>
                                              </div>
                                              <div class="col-sm-12">
                                                  <div class="font1 size14 summary">
                                                    <?php echo $news->summary; ?>
                                                    <br/><br/>
                                                </div>
                                            </div>
                                            </div>
                                            <div style="clear:both"></div>
                                                    <br>
                                              </div>
                                          </div>

                                        <?php
                                        }
                                        ?>
                                            </div>
                                                </div>
                                    </div>
                            <?php
                            } 


                            elseif($col2[$key]->module == 'CONTACT'){
                                ?> 
                                  <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-3 specialpage_col special-col4">
                              <?php } else if ($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
                              <div class="col-sm-3 specialpage_col">
                                <?php } else{?>
                                <div class="col-sm-3 specialpage_col  special-col4-no-one">
                                <?php } ?>
                                   <div class="" ng-controller="ContactusCtrl" id="">
                                        <br/>
                                        <div class="col-sm-12">
                                          <h1>Contact Us</h1>
                                        </div>
                                      <div class="">
                                          <div class="form">
                                            <!-- Contact form -->
                                            <form name="formContactUs" class="form-validation booking ng-pristine ng-invalid ng-invalid-required" ng-submit="submit(feedback)">
                                              <!-- Name -->
                                              <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12 hidden feedback-success" for="name" style="color:#6cb04b;">Thank you for contacting Sedona Healing Arts, your message was sent successfully.
                                                    <br>We will review your inquiry and get back to you shortly.</label>
                                                </div>
                                            </div>
                                            <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12" for="name">Subject
                                                    <span class="red">*</span>
                                                </label>
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control textbox margin-bot8" id="" required="required" ng-model="feedback.subject">
                                                </div>
                                            </div>
                                            </div>
                                            <div class="control-group margin-bot8">
                                            <div class="row">
                                              <label class="control-label size14 col-sm-12" for="name">Name
                                                <span class="red">*</span>
                                            </label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control textbox margin-bot8" id="fname" placeholder="First Name" required="required" ng-model="feedback.fname">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control textbox" id="lname" placeholder="Last Name" required="required" ng-model="feedback.lname">
                                            </div>
                                            </div>
                                            </div>
                                    <!-- Email -->
                                            <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12" for="email">Email
                                                    <span class="red">*</span>
                                                </label>
                                                <div class="col-sm-12">
                                                    <span class="red hidden booking-email">Invalid Email Address</span>
                                                    <input type="email" class="textbox form-control margin-bot8" id="email" required="required" ng-model="feedback.email" ng-change="onemail(feedback.email)">
                                                </div>
                                                </div>
                                            </div>
                                            <!-- Comment -->
                                        <div class="control-group margin-bot8">
                                            <div class="row">
                                              <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                                <span class="red">*</span>
                                              </label>
                                                <div class="col-sm-12">
                                                    <textarea class="form-control input-large textarea" id="feedback" rows="5" required="required" ng-model="feedback.message"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Google recaptcha -->
                                       <!--  <div class="control-group margin-bot8">
                                            <div class="row">
                                              <div class="col-sm-12">
                                                <div vc-recaptcha key="'6LfiYQsTAAAAAMZIe1BR0fLlSfJAdLxFMr4nzOlm'" on-create="setWidgetId(widgetId)" on-success="setResponse(response)" on-expire="cbExpiration()"></div>
                                            </div>
                                        </div>
                                        </div> -->
                                        <!-- Buttons -->
                                        <div class="form-actions margin-bot8">
                                            <div class="row">
                                              <div class="col-sm-12">
                                                <button type="submit" class="btn btn-sedona" ng-disabled="formContactUs.$invalid||res==false">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Buttons -->
                                    </form>
                                    </div>
                                <div class="clearfix"></div>
                                </div>
                                </div>
                                </div>
                            <?php
                            }


                            /////////////////////////////////////////////////////////COLUMN 3 ////////////////////////////
                            if($col3[$key]->module == 'TEXT'){
                            ?>
                                <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-3 specialpage_col special-col4-3">
                              <?php } else if ($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
                              <div class="col-sm-3 specialpage_col ">
                                <?php } else{?>
                                <div class="col-sm-3 specialpage_col special-col4-no-3">
                                <?php } ?>
                                    <?php echo $col3[$key]->content; ?>
                                </div>
                            <?php }

                            elseif($col3[$key]->module == 'IMAGE'){
                            ?> 
                                <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-3 specialpage_col special-col4-3">
                              <?php } else if ($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
                              <div class="col-sm-3 specialpage_col">
                                <?php } else{?>
                                <div class="col-sm-3 specialpage_col special-col4-no-3">
                                <?php } ?>
                                    <?php if($col3[$key]->link==null){ ?>
                                    <div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $col3[$key]->image; ?>');
                                        background-size:cover;
                                        background-repeat:no-repeat;
                                        background-position:center center;
                                        min-height:200px;
                                        margin-bottom:4px"></div>
                                    <?php }else{ ?>
                                    <a href="<?php echo $col3[$key]->link ?>" target="_blank"><div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $col3[$key]->image; ?>');
                                        background-size:cover;
                                        background-repeat:no-repeat;
                                        background-position:center center;
                                        min-height:200px;
                                        margin-bottom:4px"></div></a>
                                    <?php  } ?>
                                </div>
                            <?php
                            }

                            elseif($col3[$key]->module == 'DIVIDER'){
                            ?> 
                                 <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-3 specialpage_col special-col4-3">
                              <?php } else if ($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
                              <div class="col-sm-3 specialpage_col">
                                <?php } else{?>
                                <div class="col-sm-3 specialpage_col  special-col4-no-3">
                                <?php } ?>
                                    <hr style="height:<?php echo $col3[$key]->height; ?>px;background-color:<?php echo $col3[$key]->color; ?>">
                                </div>
                            <?php
                            }

                            elseif($col3[$key]->module == 'TESTIMONIAL'){
                                ?> 
                                
                                <?php $gettesti1 = $col3[$key]->testimonial; ?>
                                <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-3 specialpage_col special-col4-3">
                              <?php } else if ($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
                              <div class="col-sm-3 specialpage_col">
                                <?php } else{?>
                                <div class="col-sm-3 specialpage_col  special-col4-no-3">
                                <?php } ?>
                                   <div class="size16"><h2 class="font1 italic">What People Are Saying</h2></div>
                                    <?php
                                    $x = 0;
                                    foreach($gettesti1 as $k => $value) { ?>
                                <div class="size16 border-left italic margin-top special-testi-wrapper">
                                    <div class="row">
                                    <div class="col-sm-2 text-center page-testi-image specialpage-testi-image">
                                            <img src="<?php echo $imageLink.'/uploads/testimonialpic/'.$gettesti1[$k]->picture; ?>" class="testi-img">
                                        </div>
                                        <div class="col-sm-9 specialpage-testi-text">
                                            <div></div>
                                            <div>
                                                <span class="fa fa-quote-left"></span>
                                                <?php echo $gettesti1[$k]->message; ?>
                                             </div>
                                                <div class="text-right">- <?php echo $gettesti1[$k]->name ?></div>
                                            </div>
                                        </div>
                                        <hr class="styled-hr">
                                </div>
                                 <?php $x = 1; } ?>
                                </div>
                                <?php
                            }


                            elseif($col3[$key]->module == 'NEWS'){
                            ?> 
                               
                                 <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-3 specialpage_col special-col4-3">
                              <?php } else if ($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
                              <div class="col-sm-3 specialpage_col">
                                <?php } else{?>
                                <div class="col-sm-3 specialpage_col  special-col4-no-3">
                                <?php } ?>
                                    <div ng-controller="NewsCtrl">
                                           <h4 class="blog-title-list">Latest News</h4>
                                      <div class="list-news-wrapper special-news-wrapper">
                                        <?php
                                        foreach ($col3[$key]->news as $news) {
                                           if ($news->videothumb) {
                                           $x = preg_match('/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/i',$news->videothumb,$match);
                                           $img = 'http://img.youtube.com/vi/'. $match[1] .'/hqdefault.jpg';
                                           $url = $match[1];
                                           }
                                           else
                                           {
                                            $img = $this->view->path = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                           }
                                           $imagethumb = $this->view->path = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                        ?>
                                        <div class="row list-title-blog specialpage-title-blog">
                                          <div class="col-sm-3 news-thumb-container module2-news-thumb" style="background-image: url('<?php echo $img; ?>')" ng-click="redirectNews('<?php echo $news->newsslugs; ?>');" title="{[{ news.title }]}">
                                            <img src="<?php echo $imagethumb;?>" class="pinterest-img" alt="<?php echo $news->title;?>">
                                           <?php if ($news->videothumb) {
                                            ?>
                                            <div class="youtube-play"><a href="/blog/<?php echo $news->newsslugs?>"><img src="img/youtubeplay.png"/></a></div>
                                         <?php
                                           } ?>

                                            <a href="/blog/<?php echo $news->newsslugs; ?>">
                                            </a>
                                        </div>


                                        <div class="col-xs-8 col-md-7 news-list-desc module2-news-desc">
                                            <div class="row">

                                                <div class="">
                                                  <span class="size25 font1 news-title" ng-click="redirectNews('<?php echo $news->newsslugs; ?>');"><?php echo $news->title; ?></span>
                                              </div>
                                              <div class="">
                                                  <strong><span class="thin-font1 orange">
                                                  <?php 
                                                  foreach ($news->category as $k => $categorylist) {
                                                    if (count($news->category)-1==$k) {
                                                        echo $categorylist->categoryname;
                                                    }
                                                    else
                                                    {
                                                         echo $categorylist->categoryname.", ";
                                                    }
                                                   
                                                  }
                                                  ?>
                                                   </span></strong><span ng-show="news.name !=''" class="thin-font1"> / by <a href="/blog/author/<?php echo $news->name; ?>"><strong><span class="orange"><?php echo $news->name; ?></span></strong></a></span> / <?php echo $news->date; ?>
                                                  <br/><br/>
                                              </div>
                                              <div class="col-sm-12">
                                                  <div class="font1 size14 summary">
                                                    <?php echo $news->summary; ?>
                                                    <br/><br/>
                                                </div>
                                            </div>
                                            </div>
                                            <div style="clear:both"></div>
                                                    <br>
                                              </div>
                                          </div>

                                        <?php
                                        }
                                        ?>
                                            </div>
                                                </div>
                                    </div>
                            <?php
                            } 


                            elseif($col3[$key]->module == 'CONTACT'){
                                ?> 
                                 <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-3 specialpage_col special-col4-3">
                              <?php } else if ($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
                              <div class="col-sm-3 specialpage_col">
                                <?php } else{?>
                                <div class="col-sm-3 specialpage_col  special-col4-no-3">
                                <?php } ?>
                                   <div class="" ng-controller="ContactusCtrl" id="">
                                        <br/>
                                        <div class="col-sm-12">
                                          <h1>Contact Us</h1>
                                        </div>
                                      <div class="">
                                          <div class="form">
                                            <!-- Contact form -->
                                            <form name="formContactUs" class="form-validation booking ng-pristine ng-invalid ng-invalid-required" ng-submit="submit(feedback)">
                                              <!-- Name -->
                                              <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12 hidden feedback-success" for="name" style="color:#6cb04b;">Thank you for contacting Sedona Healing Arts, your message was sent successfully.
                                                    <br>We will review your inquiry and get back to you shortly.</label>
                                                </div>
                                            </div>
                                            <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12" for="name">Subject
                                                    <span class="red">*</span>
                                                </label>
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control textbox margin-bot8" id="" required="required" ng-model="feedback.subject">
                                                </div>
                                            </div>
                                            </div>
                                            <div class="control-group margin-bot8">
                                            <div class="row">
                                              <label class="control-label size14 col-sm-12" for="name">Name
                                                <span class="red">*</span>
                                            </label>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control textbox margin-bot8" id="fname" placeholder="First Name" required="required" ng-model="feedback.fname">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control textbox" id="lname" placeholder="Last Name" required="required" ng-model="feedback.lname">
                                            </div>
                                            </div>
                                            </div>
                                    <!-- Email -->
                                            <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12" for="email">Email
                                                    <span class="red">*</span>
                                                </label>
                                                <div class="col-sm-12">
                                                    <span class="red hidden booking-email">Invalid Email Address</span>
                                                    <input type="email" class="textbox form-control margin-bot8" id="email" required="required" ng-model="feedback.email" ng-change="onemail(feedback.email)">
                                                </div>
                                                </div>
                                            </div>
                                            <!-- Comment -->
                                        <div class="control-group margin-bot8">
                                            <div class="row">
                                              <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                                <span class="red">*</span>
                                              </label>
                                                <div class="col-sm-12">
                                                    <textarea class="form-control input-large textarea" id="feedback" rows="5" required="required" ng-model="feedback.message"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Google recaptcha -->
                                        <!-- <div class="control-group margin-bot8">
                                            <div class="row">
                                              <div class="col-sm-12">
                                                <div vc-recaptcha key="'6LfiYQsTAAAAAMZIe1BR0fLlSfJAdLxFMr4nzOlm'" on-create="setWidgetId(widgetId)" on-success="setResponse(response)" on-expire="cbExpiration()"></div>
                                            </div>
                                        </div>
                                        </div> -->
                                        <!-- Buttons -->
                                        <div class="form-actions margin-bot8">
                                            <div class="row">
                                              <div class="col-sm-12">
                                                <button type="submit" class="btn btn-sedona" ng-disabled="formContactUs.$invalid||res==false">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Buttons -->
                                    </form>
                                    </div>
                                <div class="clearfix"></div>
                                </div>
                                </div>
                                </div>
                            <?php
                            }

                            /////////////////////////////////////////////////////////COLUMN 4 ////////////////////////////
                            if($col4[$key]->module == 'TEXT'){
                            ?>
                                 <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-3 specialpage_col special-col4-4">
                              <?php } else if ($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
                              <div class="col-sm-3 specialpage_col ">
                                <?php } else{?>
                                <div class="col-sm-3 specialpage_col special-col4-no-4">
                                <?php } ?>
                                    <?php echo $col4[$key]->content; ?>
                                </div>
                            <?php }

                            elseif($col4[$key]->module == 'IMAGE'){
                            ?> 
                                 <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-3 specialpage_col special-col4-4">
                              <?php } else if ($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
                              <div class="col-sm-3 specialpage_col">
                                <?php } else{?>
                                <div class="col-sm-3 specialpage_col  special-col4-no-4">
                                <?php } ?>
                                    <?php if($col4[$key]->link==null){ ?>
                                   <div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $col4[$key]->image; ?>');
                                        background-size:cover;
                                        background-repeat:no-repeat;
                                        background-position:center center;
                                        min-height:200px;
                                        margin-bottom:4px"></div>
                                    <?php }else{ ?>
                                    <a href="<?php echo $col4[$key]->link ?>" target="_blank"><div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $col4[$key]->image; ?>');
                                        background-size:cover;
                                        background-repeat:no-repeat;
                                        background-position:center center;
                                        min-height:200px;
                                        margin-bottom:4px"></div></a>
                                    <?php  } ?>
                                </div>
                            <?php
                            }

                            elseif($col4[$key]->module == 'DIVIDER'){
                            ?> 
                                 <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-3 specialpage_col special-col4-4">
                              <?php } else if ($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
                              <div class="col-sm-3 specialpage_col ">
                                <?php } else{?>
                                <div class="col-sm-3 specialpage_col special-col4-no-4">
                                <?php } ?>
                                    <hr style="height:<?php echo $col4[$key]->height; ?>px;background-color:<?php echo $col4[$key]->color; ?>">
                                </div>
                            <?php
                            }

                            elseif($col4[$key]->module == 'TESTIMONIAL'){
                                ?> 
                               
                                <?php $gettesti1 = $col4[$key]->testimonial; ?>
                                 <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-3 specialpage_col special-col4-4">
                              <?php } else if ($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
                              <div class="col-sm-3 specialpage_col">
                                <?php } else{?>
                                <div class="col-sm-3 specialpage_col  special-col4-no-4">
                                <?php } ?>
                                   <div class="size16"><h2 class="font1 italic">What People Are Saying</h2></div>
                                    <?php
                                    $x = 0;
                                    foreach($gettesti1 as $k => $value) { ?>
                                <div class="size16 border-left italic margin-top special-testi-wrapper">
                                    <div class="row">
                                    <div class="col-sm-2 text-center page-testi-image specialpage-testi-image">
                                            <img src="<?php echo $imageLink.'/uploads/testimonialpic/'.$gettesti1[$k]->picture; ?>" class="testi-img">
                                        </div>
                                        <div class="col-sm-9 specialpage-testi-text">
                                            <div></div>
                                            <div>
                                                <span class="fa fa-quote-left"></span>
                                                <?php echo $gettesti1[$k]->message; ?>
                                             </div>
                                                <div class="text-right">- <?php echo $gettesti1[$k]->name ?></div>
                                            </div>
                                        </div>
                                        <hr class="styled-hr">
                                </div>
                                 <?php $x = 1; } ?>
                                </div>
                                <?php
                            }


                            elseif($col4[$key]->module == 'NEWS'){
                            ?> 
                                
                                 <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-3 specialpage_col special-col4-4">
                              <?php } else if ($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
                              <div class="col-sm-3 specialpage_col">
                                <?php } else{?>
                                <div class="col-sm-3 specialpage_col  special-col4-no-4">
                                <?php } ?>
                                    <div ng-controller="NewsCtrl">
                                          <h4 class="blog-title-list">Latest News</h4>
                                      <div class="list-news-wrapper special-news-wrapper">
                                        <?php
                                        foreach ($col4[$key]->news as $news) {
                                           if ($news->videothumb) {
                                           $x = preg_match('/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/i',$news->videothumb,$match);
                                           $img = 'http://img.youtube.com/vi/'. $match[1] .'/hqdefault.jpg';
                                           $url = $match[1];
                                           }
                                           else
                                           {
                                            $img = $this->view->path = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                           }
                                           $imagethumb = $this->view->path = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                        ?>
                                        <div class="row list-title-blog specialpage-title-blog">
                                          <div class="col-sm-3 news-thumb-container module2-news-thumb" style="background-image: url('<?php echo $img; ?>')" ng-click="redirectNews('<?php echo $news->newsslugs; ?>');" title="{[{ news.title }]}">
                                            <img src="<?php echo $imagethumb;?>" class="pinterest-img" alt="<?php echo $news->title;?>">
                                           <?php if ($news->videothumb) {
                                            ?>
                                            <div class="youtube-play"><a href="/blog/<?php echo $news->newsslugs?>"><img src="img/youtubeplay.png"/></a></div>
                                         <?php
                                           } ?>

                                            <a href="/blog/<?php echo $news->newsslugs; ?>">
                                            </a>
                                        </div>


                                        <div class="col-xs-8 col-md-7 news-list-desc module2-news-desc">
                                            <div class="row">

                                                <div class="">
                                                  <span class="size25 font1 news-title" ng-click="redirectNews('<?php echo $news->newsslugs; ?>');"><?php echo $news->title; ?></span>
                                              </div>
                                              <div class="">
                                                  <strong><span class="thin-font1 orange">
                                                  <?php 
                                                  foreach ($news->category as $k => $categorylist) {
                                                    if (count($news->category)-1==$k) {
                                                        echo $categorylist->categoryname;
                                                    }
                                                    else
                                                    {
                                                         echo $categorylist->categoryname.", ";
                                                    }
                                                   
                                                  }
                                                  ?>
                                                   </span></strong><span ng-show="news.name !=''" class="thin-font1"> / by <a href="/blog/author/<?php echo $news->name; ?>"><strong><span class="orange"><?php echo $news->name; ?></span></strong></a></span> / <?php echo $news->date; ?>
                                                  <br/><br/>
                                              </div>
                                              <div class="col-sm-12">
                                                  <div class="font1 size14 summary">
                                                    <?php echo $news->summary; ?>
                                                    <br/><br/>
                                                </div>
                                            </div>
                                            </div>
                                            <div style="clear:both"></div>
                                                    <br>
                                              </div>
                                          </div>

                                        <?php
                                        }
                                        ?>
                                            </div>
                                                </div>
                                    </div>
                            <?php
                            } 


                            elseif($col4[$key]->module == 'CONTACT'){
                                ?> 
                                <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
                            <div class="col-sm-3 specialpage_col special-col4-4">
                              <?php } else if ($data->leftsidebar == 'false' && $data->rightsidebar == 'false' || $data->leftsidebar == NULL && $data->rightsidebar == NULL){ ?>
                              <div class="col-sm-3 specialpage_col">
                                <?php } else{?>
                                <div class="col-sm-3 specialpage_col special-col4-no-4">
                                <?php } ?>
                                   <div class="" ng-controller="ContactusCtrl" id="">
                                        <br/>
                                        <div class="col-sm-12">
                                          <h2>Contact Us</h2>
                                        </div>
                                      <div class="">
                                          <div class="form">
                                            <!-- Contact form -->
                                            <form name="formContactUs" class="form-validation booking ng-pristine ng-invalid ng-invalid-required" ng-submit="submit(feedback)">
                                              <!-- Name -->
                                              <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12 hidden feedback-success" for="name" style="color:#6cb04b;">Thank you for contacting Sedona Healing Arts, your message was sent successfully.
                                                    <br>We will review your inquiry and get back to you shortly.</label>
                                                </div>
                                            </div>
                                            <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12" for="name">Subject
                                                    <span class="red">*</span>
                                                </label>
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control textbox margin-bot8" id="" required="required" ng-model="feedback.subject">
                                                </div>
                                            </div>
                                            </div>
                                            <div class="control-group margin-bot8">
                                            <div class="row">
                                              <label class="control-label size14 col-sm-12" for="name">Name
                                                <span class="red">*</span>
                                            </label>
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control textbox margin-bot8" id="fname" placeholder="First Name" required="required" ng-model="feedback.fname">
                                            </div>
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control textbox" id="lname" placeholder="Last Name" required="required" ng-model="feedback.lname">
                                            </div>
                                            </div>
                                            </div>
                                    <!-- Email -->
                                            <div class="control-group margin-bot8">
                                                <div class="row">
                                                  <label class="control-label size14 col-sm-12" for="email">Email
                                                    <span class="red">*</span>
                                                </label>
                                                <div class="col-sm-12">
                                                    <span class="red hidden booking-email">Invalid Email Address</span>
                                                    <input type="email" class="textbox form-control margin-bot8" id="email" required="required" ng-model="feedback.email" ng-change="onemail(feedback.email)">
                                                </div>
                                                </div>
                                            </div>
                                            <!-- Comment -->
                                        <div class="control-group margin-bot8">
                                            <div class="row">
                                              <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                                <span class="red">*</span>
                                              </label>
                                                <div class="col-sm-12">
                                                    <textarea class="form-control input-large textarea" id="feedback" rows="5" required="required" ng-model="feedback.message"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Google recaptcha -->
                                       <!--  <div class="control-group margin-bot8">
                                     
                                            <div class="row">
                                              <div class="col-sm-12">
                                                <div class="iframe4" vc-recaptcha key="'6LfiYQsTAAAAAMZIe1BR0fLlSfJAdLxFMr4nzOlm'" on-create="setWidgetId(widgetId)" on-success="setResponse(response)" on-expire="cbExpiration()"></div>
                                            </div>
                                        </div>
                                        </div> -->
                                        <!-- Buttons -->
                                        <div class="form-actions margin-bot8">
                                            <div class="row">
                                              <div class="col-sm-12">
                                                <button type="submit" class="btn btn-sedona" ng-disabled="formContactUs.$invalid||res==false">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Buttons -->
                                    </form>
                                    </div>
                                <div class="clearfix"></div>
                                </div>
                                </div>
                                </div>
                            <?php
                            }

                            //////
                            ?></div><?php
                        }
                }       
            }

            ?> 
            </div>

        </div>

 <!-- RIGHT SIDE     --> 
    <?php 
if($data->rightsidebar == 'true')
    { ?>

  <?php if($data->leftsidebar == 'true' && $data->rightsidebar == 'true'){ ?>
  <div class="col-sm-2 specialpage-sidebar">
    <?php }else{ ?>
    <div class="col-sm-2">
      <?php } ?>
         <div class="right-sidebar">
                  <?php 
        foreach ($sidebarright as $key => $value) {
        ?>

        <?php
               if ($value->sidebar=='Image')
                {
           
                    if($value->imglink == NULL){
                    ?>
                    <hr class="styled-hr">
                    <img src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $value->img?>" width="100%">
                    <?php }else{ ?>
                    <hr class="styled-hr">
                    <a target="_blank" href="<?php echo $value->imglink; ?>"><img src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $value->img?>" width="100%"></a>
                    <?php 
                    }
                }
                 elseif($value->sidebar=='Calendar')
                {
                    ?>
                      <hr class="styled-hr">
                   <div class="calendar" ng-model="eventSources" calendar="calendar1" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div>
                    <?php
                }
                elseif($value->sidebar=='Menu')
                {
                    ?>
                     <hr class="styled-hr">
    <div class="special-menu">
          <div class="">
            <div class=""><a class="special-top-menu">Sedona Healing Arts</a>
            </div>
            <nav>
              <?php $mobmenu = $value->shortcode;
              $children = $value->children;
              foreach ($mobmenu as $k => $short) {
                ?>
                <ul class="">
                  <li> 
                    <?php
                    if ($mobmenu[$k]->newtab=='true'){
                      ?>
                      <a class="special-menu-text" href="<?php echo $mobmenu[$k]->sublink; ?>" target="_blank"><span>
                        <?php echo $mobmenu[$k]->subname; ?> 
                      </span></a>
                      <?php
                }//if newtab true
                elseif ($mobmenu[$k]->newtab=='false') {

                  ?>
                  <a class="special-menu-text" href="<?php echo $mobmenu[$k]->sublink; ?>"><span>
                    <?php echo $mobmenu[$k]->subname; ?>
                  </span></a>
                  <?php
                }//if newtab false

                if (sizeof($children)!=0) {
                  ?>
                  <ul style="padding-left: 20px; list-style:none">
                    <?php
                    foreach ($children as $kk => $chi) {
                      $child1 = $children[$kk]->child1;
                      $child2 = $children[$kk]->child2;
                      $child3 = $children[$kk]->child3;
                      $child4 = $children[$kk]->child4;
                      foreach ($child1 as $child1) {
                        if ($child1->parent == $mobmenu[$k]->submenuID) {
                          ?>
                          <li> <span class="glyphicon glyphicon-menu-right" style="color:#58666e;"></span>
                            <?php
                            if ($child1->newtab=="true") {
                              ?>
                              <a class="special-menu-text caps" href="<?php echo $child1->sublink; ?>" target="_blank"><span> <?php echo $child1->subname; ?> </span></a>
                              <?php
                              } //if child1 new tab true
                              elseif ($child1->newtab=="false") {
                                ?>
                                <a class="special-menu-text caps" href="<?php echo $child1->sublink; ?>"><span>
                                  <?php echo $child1->subname; ?>
                                </span></a>
                                <?php
                                  } //if child1 new tab false
                                  ?>
                                  <ul style="list-style:none">
                                   <?php

                                   foreach ($child2 as $child2) {
                                    if ($child2->subname != null) {
                                      if ($child2->parent == $child1->submenuID) {  
                                        ?>
                                        <li> <span class="glyphicon glyphicon-menu-right" style="color:#58666e;"></span>
                                          <?php
                                          if ($child2->newtab=="true") {
                                            ?>
                                            <a class="special-menu-text caps" href="<?php echo $child2->sublink; ?>" target="_blank"><span> <?php echo $child2->subname; ?> </span></a>
                                            <?php
                                          }
                                          elseif($child2->newtab=="false")
                                          {
                                            ?>
                                            <a class="special-menu-text caps" href="<?php echo $child2->sublink; ?>"><span>
                                              <?php echo $child2->subname;?>
                                            </span></a>
                                            <?php
                              } //newtab false
                              ?>

                              <ul style="list-style:none">
                                <?php
                                foreach ($child3 as $children3) {
                                  if ($children3->parent == $child2->submenuID) {

                                    ?> 
                                    <li> <span class="glyphicon glyphicon-menu-right" style="color:#58666e;"></span>
                                      <?php
                                      if ($children3->newtab=="true") {
                                        ?>
                                        <a class="special-menu-text caps" href="<?php echo $children3->sublink; ?>" target="_blank"><span> <?php echo $children3->subname; ?> </span></a>
                                        <?php
                                      }
                                      elseif($children3->newtab=="false")
                                      {
                                        ?>
                                        <a class="special-menu-text caps" href="<?php echo $children3->sublink; ?>"><span>
                                          <?php echo $children3->subname;?>
                                        </span></a>
                                        <?php
                              } //newtab false
                              ?>
                              <ul style="list-style:none">
                                <?php
                                foreach ($child4 as $children4) {
                                  if ($children4->parent == $children3->submenuID) {
                                    ?>
                                    <li> <span class="glyphicon glyphicon-menu-right" style="color:#58666e;"></span>
                                      <?php
                                      if ($children4->newtab=="true") {
                                        ?>
                                        <a class="special-menu-text caps" href="<?php echo $children4->sublink; ?>" target="_blank"><span> <?php echo $children4->subname; ?> </span></a>
                                        <?php
                                      }
                                      elseif($children4->newtab=="false")
                                      {
                                        ?>
                                        <a class="special-menu-text caps" href="<?php echo $children4->sublink; ?>"><span>
                                          <?php echo $children4->subname;?>
                                        </span></a>
                                        <?php
                                                  } //newtab false
                                                  ?>
                                                </li>
                                                <?php
                                              }
                                            }
                                            ?>
                                          </ul>
                                        </li>
                                        <?php
                                    }//if parent==menuid
                                  } //foreach child3
                                  ?>
                                </ul>

                              </li>
                              <?php

                                    } //if parent == menuid
                                  } // if child2 not null

                                } //foreach child2
                                ?>
                              </ul>
                            </li>
                            <?php
                              } //if child1      
                            } //foreach child1

                      } //foreach children
                      ?>
                    </ul>
                    <?php 
                  } //$children not 0

                  ?>
                </li>
              </ul> <!-- main menu -->
              <?php

              }//foreach
              ?>

            </nav>
          </div>

          </div>
                  <!-- Mobile menu ends -->
                    <?php
                }
                elseif($value->sidebar=='Testimonial')
                {
                    ?>
                    <hr class="styled-hr">
                     <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                                <?php $gettesti = $value->testimonial; ?>
                                <div class="specialpage_col">
                                    <?php
                                    $x = 0;
                                    foreach($gettesti as $k => $value) { ?>
                                <div class="size16 border-left italic margin-top special-testi-wrapper">
                                    <div class="row">
                                        <div class="col-sm-3 text-center page-testi-image specialpage-testi-image">
                                            <img src="<?php echo $imageLink.'/uploads/testimonialpic/'.$gettesti[$k]->picture; ?>" class="testi-img">
                                        </div>
                                        <div class="col-sm-9 specialpage-testi-text">
                                            <div></div>
                                            <div>
                                                <span class="fa fa-quote-left"></span>
                                                <?php echo $gettesti[$k]->message; ?>
                                             </div>
                                                <div class="text-right">- <?php echo $gettesti[$k]->name ?></div>
                                            </div>
                                        </div>
                                        <hr class="styled-hr">
                                </div>
                                 <?php $x = 1; } ?>
                                </div>
                    <?php
                }
                elseif($value->sidebar=='Link')
                {
                      if($value->url != '' && $value->label != ''){ ?>
                     <hr class="styled-hr">
                      <a target="_blank" href="<?php echo $value->url; ?>" class="page-link"><?php echo $value->label; ?></a>
                    <?php }?>

                    <?php
                }
                elseif($value->sidebar=='News')
                {
                    ?>
                  <hr class="styled-hr">
                  <div class="specialpage_col">
                                <div class="" ng-controller="NewsCtrl" ng-init="showmorenews()">
                                  <div class="row">
                                      <h4 class="blog-title-list">Latest News</h4>
                                      <div class="list-news-wrapper special-news-wrapper">
                                       <!--  <div ng-hide="newslist"> Loading News...</div> -->

                                        <?php
                                        foreach ($value->newslist as $news) {
                                           if ($news->videothumb) {
                                           $x = preg_match('/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/i',$news->videothumb,$match);
                                           $img = 'http://img.youtube.com/vi/'. $match[1] .'/hqdefault.jpg';
                                           $url = $match[1];
                                           }
                                           else
                                           {
                                            $img = $this->view->path = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                           }
                                           $imagethumb = $this->view->path = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                        ?>
                                        <div class="row list-title-blog specialpage-title-blog">
                                          <div class="col-sm-3 news-thumb-container specialpage-news-thumb" style="background-image: url('<?php echo $img; ?>')" ng-click="redirectNews('<?php echo $news->newsslugs; ?>');" title="{[{ news.title }]}">
                                            <img src="<?php echo $imagethumb;?>" class="pinterest-img" alt="<?php echo $news->title;?>">
                                           <?php if ($news->videothumb) {
                                            ?>
                                            <div class="youtube-play"><a href="/blog/<?php echo $news->newsslugs?>"><img src="img/youtubeplay.png"/></a></div>
                                         <?php
                                           } ?>

                                            <a href="/blog/<?php echo $news->newsslugs; ?>">
                                            </a>
                                        </div>


                                        <div class="news-list-desc specialpage-news-desc">
                                            <div class="row">

                                                <div class="">
                                                  <span class="size25 font1 news-title" ng-click="redirectNews('<?php echo $news->newsslugs; ?>');"><?php echo $news->title; ?></span>
                                              </div>
                                              <div class="">
                                                  <strong><span class="thin-font1 orange">
                                                  <?php 
                                                  foreach ($news->category as $k => $categorylist) {
                                                    if (count($news->category)-1==$k) {
                                                        echo $categorylist->categoryname;
                                                    }
                                                    else
                                                    {
                                                         echo $categorylist->categoryname.", ";
                                                    }
                                                   
                                                  }
                                                  ?>
                                                   </span></strong><span ng-show="news.name !=''" class="thin-font1"> / by <a href="/blog/author/<?php echo $news->name; ?>"><strong><span class="orange"><?php echo $news->name; ?></span></strong></a></span> / <?php echo $news->date; ?>
                                                  <br/><br/>
                                              </div>
                                              <div class="col-sm-12">
                                                  <div class="font1 size14 summary">
                                                    <?php echo $news->summary; ?>
                                                    <br/><br/>
                                                </div>
                                            </div>
                                            </div>
                                            <div style="clear:both"></div>
                                                    <br>
                                              </div>
                                          </div>

                                        <?php
                                        }
                                        ?>
                                            </div>
                                            </div>
                                            </div>
                                            </div>
                    <?php
                }
                elseif($value->sidebar=='Archive')
                {
                    ?>
                      <hr class="styled-hr">
                     <?php $getarchives = $value->archive; ?>
                    <h2 class="font1"><span class="fa fa-calendar fa-1x"></span> Archives</h2>
                    <div class="ul-archives">
                        <?php
                       
                        foreach ($getarchives as $key => $value) { ?>
                        <a href="<?php echo $base_url; ?>/blog/archive/<?php echo $getarchives[$key]->month . "/" . $getarchives[$key]->year; ?>"><span class="fa fa-chevron-right"></span> <?php echo $getarchives[$key]->month . " " . $getarchives[$key]->year; ?></a>
                        <?php
                    } ?>
                    </div>

                    <?php
                }
                elseif($value->sidebar=='Rss')
                {
                    ?>
                     <hr class="styled-hr">
                    <p class="size16"> <a href="<?php echo $base_url;?>/feed" target="_blank"><img class="rss" src="img/frontend/rss_feed.gif"> RSS Feed</a></p> <br/> <br/>
                    <?php
                }
    } //rightsidebar
    ?>

         </div>
        </div><?php } ?>


    </div>

</div>
</div>
<script type="text/javascript">
   var MyDiv1 = document.getElementById('main-div');
   var MyDiv2 = document.getElementById('second-div');
   MyDiv2.innerHTML = MyDiv1.innerHTML;
</script>
<!-- 

    <!-- Below Banner ends -->
