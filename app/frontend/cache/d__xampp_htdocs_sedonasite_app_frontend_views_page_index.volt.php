<?php echo $this->getContent()?>

    <!-- Banner starts -->
    <style type="text/css">
        .banner{
            background:url(https://bodynbrain.s3.amazonaws.com/uploads/pageimage/<?php echo $banner;?>);    
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center center;
            width: 100%;
            height: 400px;
            overflow: hidden;
        }
    </style>
    <div class="banner-container banner">

        <div class="black-box">
            <span class="banner-title"><?php echo $title;?></span>
            <br/>
            <span class="banner-sub-title1"><?php echo $subtitle1;?></span><br/>
            <span class="banner-sub-title1"><?php echo $subtitle2;?></span> <br/> <br/>
            <div class="button"><a href="<?php echo $base_url;?>/booking" class="learn-sedona"><?php echo $buttontitle;?></a></div>
        </div>

    </div>

    <!-- Banner ends -->

    <div class="container">
        <div class="row">
            <div class="col-sm-8 no-margin content-text">
                <div class="phar no-padding">   
                    <!-- <span class="size20"><h2><?php echo $title;?></h2></span>   -->
                    <span class="size18">
                        <?php echo $body;?>      
                    </span>  
                </div>

                <div class="phar no-padding">   
                    <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>  
                    <div class="size16 border-left italic margin-left margin-top">
                        "<?php echo $peoplesaying;?>" <br/> <br/>  
                        <div class="text-right">- Anonymous</div>        
                    </div>
                </div>

            </div>

            <div class="col-sm-4">
                <!-- ===== SIDE BAR 1 ===== -->
                <div class="phar center no-padding sidebar1">
                    <hr class="styled-hr">
                    <h2><?php echo $title;?></h2>
                    <h3 class="grayfont">Price: <?php echo $serviceprice;?></h3>
                    <div class="button">
                        <a href="<?php echo $base_url;?>/booking" class="learn-sedona">BOOK ONLINE NOW</a>
                    </div><br/>
                    <div class="center">
                        <span class="size20">
                        - OR -<br/>
                        </span>
                        <span class="size20 bold">
                        Call 928-282-3875
                        </span>
                        <span class="size20"><br/>
                        to make an appointment<br/> <br/>
                        </span>
                    </div>
                    <hr class="styled-hr">
                </div>
                <!-- ===== END SIDE BAR 1 ===== -->
                

                <!-- ===== SIDE BAR 2 ===== -->
                <?php echo $sidebar;?>
                <!-- ===== END SIDE BAR 2 ===== -->
            </div>
        </div>
    </div>



    <div class="container margin-bot100 margin-top80">
        <hr class="styled-hr">
        <div class="row padding-topbot20">
            <div class="col-sm-12 center">
                <span class="size25 font2">Call 928-282-3875 or <a href="">CLICK HERE</a> to make an appointment.</span>
            </div>
        </div>
        <hr class="styled-hr">
    </div>

    <!-- Below Banner ends -->

