<?php echo $this->getContent()?>

<!-- Banner starts -->
<style type="text/css">

	img.testi-img{
		width: 68px;
		height: 68px;
		border-radius: 100%;
		border: solid 1px #A9A8A8;
		-webkit-filter: grayscale(100%);
		filter: grayscale(100%);
		-win-filter: grayscale(100%);
	}

</style>


<div class="banner-container banner" style="background-image:url('<?php echo $imageLink;?>/uploads/pageimage/<?php echo $banner;?>');" title="<?php echo $title; ?>">
	<img src="<?php echo $imageLink;?>/uploads/pageimage/<?php echo $banner;?>" class="pinterest-img" alt="<?php echo $title; ?>" />
	<div class="black-box">
		<span class="banner-title"><?php echo $title;?></span>
		<br/>
		<?php if($subtitle1 !=''){echo "<span class='banner-sub-title1'>".$subtitle1."</span><br/>";}?>
		<?php if($subtitle2 !=''){echo "<span class='banner-sub-title1'>".$subtitle2."</span><br/>";}?>
		 <br/>
		<div class="button"><a href="<?php echo $base_url;?>/booking" class="learn-sedona"><?php echo $buttontitle;?></a></div>
	</div>

</div>

<!-- Banner ends -->

<div class="container">
	<div class="row">
		<div class="col-sm-8 no-margin content-text">
			<div class="phar no-padding">
				<!-- <span class="size20"><h2><?php echo $title;?></h2></span>   -->
				<span class="size18">
					<?php echo $body;?>
				</span>
			</div>

			<div class="phar no-padding">
				<div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>

				<?php
				$getginfo = $testimonials;
				foreach ($getginfo as $key => $value) {
					?>
					<div class="size16 border-left italic margin-left margin-top">
						<div class="row">
							<div class="col-sm-2 text-center">
								<?php
									if(!empty($getginfo[$key]->picture)){

								?>
								<img src="<?php echo $this->config->application->amazonlink?>/uploads/testimonialpic/<?php echo $getginfo[$key]->picture;?>" class="testi-img">
								<?php
								}else{
								?>
								<img src="img/testidefaultimage.png" class="testi-img">
								<?php
								}
								?>
							</div>
							<div class="col-sm-9">

							<div></div>
								<div>
								<span class="fa fa-quote-left"></span>
								<?php echo $getginfo[$key]->message;?>
								</div>
								<div class="text-right">- <?php echo $getginfo[$key]->name;?></div>
							</div>
						</div>
						<hr class="styled-hr">
					</div>
					<?php } ?> <br/>

				</div>

			</div>

			<div class="col-sm-4">
				<!-- ===== SIDE BAR 1 ===== -->
				<div class="phar center no-padding sidebar1">
					<hr class="styled-hr">
					<h2><?php echo $title;?></h2>
					<?php if($subtitle1 !=''){echo "<h3>".$subtitle1."</h3>";}?>
					<h3 class="grayfont">Price: <?php echo $serviceprice;?></h3>
					<div class="button">
						<a href="<?php echo $base_url;?>/booking" class="learn-sedona">BOOK ONLINE NOW</a>
					</div><br/>
					<div class="center">
						<span class="size20">
							- OR -<br/>
						</span>
						<span class="size20 bold">
							Call 928-282-3875
						</span>
						<span class="size20"><br/>
							to make an appointment<br/> <br/>
						</span>
					</div>
					<hr class="styled-hr">
				</div>
				<!-- ===== END SIDE BAR 1 ===== -->


				<!-- ===== SIDE BAR 2 ===== -->
				<?php echo $sidebar;?>
				<!-- ===== END SIDE BAR 2 ===== -->
			</div>
		</div>
	</div>

	<div class="container margin-bot100 margin-top80">
		<hr class="styled-hr">
		<div class="row padding-topbot20">
			<div class="col-sm-12 center">
				<span class="size25 font2">Call 928-282-3875 or <a href="<?php echo $base_url;?>/booking">CLICK HERE</a> to make an appointment.</span>
			</div>
		</div>
		<hr class="styled-hr">
	</div>

	<!-- Below Banner ends -->
