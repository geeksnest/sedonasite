<div class="container-fluid bg-light" ng-controller="checkoutCtrl">
  <div class="row">
    <div class="col-sm-10 col-sm-offset-1 bg-light ec-nopad">

    <div class="col-sm-12 wrapper-md">

      <div ng-include="'/fe/tpl/ecommerce/header.html'"></div>
      <div ng-include="'/fe/tpl/ecommerce/menu.html'"></div>

      <div class="row">
        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading">CHECKOUT</div>
            <div class="panel-body">

              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default" ng-hide="token">
                  <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                      <a role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="collapseOne" ng-click="collapse(0)">
                        CHECKOUT METHOD
                      </a>
                    </h4>
                  </div>
                  <div id="checkoutMethod" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body" ng-controller="UserloginCtrl">
                      <div class="col-sm-8" style="vertical-align:bottom;">
                        <em>Register with us for future convenience:</em>
                        <br><br>
                        <a class="btn btn-info" ng-click="collapse(1)">Register & Checkout</a>
                      </div>
                      <div class="col-sm-4">
                        <h3>SIGN IN & CHECKOUT</h3>
                        <em>Sign in to speed up your checkout process</em>
                        <br><br>
                        <form name="registration" class="form-reservation">
                            <fieldset ng-disabled="saving">
		                        <div class="form-group">
		                            <label>Email or Username</label>
		                            <input type="eusernameail" ng-model="userlogin.username" class="form-control text-box" id="username" placeholder="Enter Email or Username." required="required">
		                        </div>
		                        <div class="form-group">
		                            <label>Password</label>
		                            <input type="password" ng-model="userlogin.password" name="password" class="form-control text-box" id="password" placeholder="Enter your Password."required="required">
		                        </div>
		                        <div class="form-group">
		                            <input type='button' class='btn btn-info btn-block btn-sm' name='signin' value='Login' ng-click="dologin_checkout(userlogin)" />
		                        </div>
                            <div class="form-group text-c" ng-show="errormsg">
                              <em class="text-danger" ng-bind="errormsg"></em> <br>
                              <em>For newly registered account please check your email and follow the verification process. Thank you.</em>
                              <br>
                            </div>
		                        <div class="form-group" style="text-align:center;font-weight: bold;">
		                           	Or sign in using
		                        </div>
		                        <div class="form-group" style="text-align:center;font-weight: bold;">
		                        	<a class="btn btn-social btn-facebook" ng-click="facebooklogin()" style="width:100%; margin:1px;">
		                        		<span class="fa fa-facebook"></span> Sign in with Facebook
		                        	</a>
		                        	<a class="btn btn-social btn-twitter" style="width:100%; margin:1px;">
		                        		<span class="fa fa-twitter"></span> Sign in with Twitter
		                        	</a>
		                        	<a class="btn btn-social btn-google" style="width:100%; margin:1px;">
		                        		<span class="fa fa-google-plus"></span> Sign in with Google
		                        	</a>
		                        </div>
                            </fieldset>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default" ng-hide="token">
                  <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                      <a class="collapsed class1" role="button" data-toggle="collapse" data-parent="#accordion" href="" aria-expanded="true" aria-controls="collapseTwo">
                        BILLING INFORMATION
                      </a>
                    </h4>
                  </div>
                  <div id="billingInformation" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body" ng-controller="UserregistrationCtrl">
                      <div class="col-sm-4">
                        <form name="registration" class="form-reservation">
                          <fieldset ng-disabled="saving">
                            <div class="form-group">
                                <label>Email: </label>
                                <input type="email" ng-model="register.email" class="form-control text-box" id="email" placeholder="Enter your Email." required="required" ng-blur="checkEmail(register.email)">
                                <label ng-show="emailexist==1" class="errortext"> This email already belongs to someone. Do you want to use this and login? <a href="" ng-click="login=true; ll.loginemailusername = register.email; register.email = ''; emailexist=0; "> Yes </a> </label>
                                <span ng-show="sendingcompletionlinksuccess">Please check your email for instructions.</span>
                                <span ng-show="sendingcompletionlink">Sending Completion Link...</span>
                                <label  ng-show="emailexist==3 && !sendingcompletionlink"> We have detected that this email have been registered offline, click yes to receive the link to complete your profile? <a href="" ng-click="sendUserCompletionLink(register.email); emailexist=0;"> Yes </a> </label>
                            </div>
                            <div class="form-group">
                                <label>Username:</label>
                                <input type="text" ng-model="register.username" name="username" class="form-control text-box" id="username" placeholder="Enter your Username." required="required" ng-pattern="/^[a-zA-Z0-9]*$/">
                                <span class="help-block" ng-show="registration.username.$error.pattern">Alphanumeric characters only.</span>
                            </div>
                            <div class="form-group">
                                <label>Password:</label>
                                <input type="password" ng-model="register.password" name="password" class="form-control text-box" id="password" placeholder="Enter your Password." pass-strength required="required" ng-minlength="6" ng-change="passwordmatch(register.password, register.repassword)">
                                <span class="help-block" ng-if="registration.password.$error.minlength">Minimum of 6 characters.</span>
                            </div>
                            <div class="form-group">
                                <label>Retype Password:</label>
                                <input type="password" ng-model="register.repassword" class="form-control text-box" id="repassword" name="repassword" placeholder="Retype your Password." required="required" ng-change="passwordmatch(register.password, register.repassword)">
                                  <span class="msg-error" ng-show="registration.repassword.$error.pwmatch">
                                    Passwords don't match.
                                  </span>
                            </div>
                            <div class="form-group">
                                <label>Lastname:</label>
                                <input type="text" ng-model="register.lastname" class="form-control text-box" id="lastname" placeholder="Enter your Lastname." required="required">
                            </div>
                            <div class="form-group">
                                <label>Firstname:</label>
                                <input type="text" ng-model="register.firstname" class="form-control text-box" id="firstname" placeholder="Enter your Firstname." required="required">
                            </div>
                            <div class="form-group">
                                <label>Address 1:</label>
                                <input type="text" ng-model="register.address1" class="form-control text-box" id="address1" placeholder="Enter your Address." required="required">
                            </div>
                            <div class="form-group">
                                <label>Address 2:</label>
                                <input type="text" ng-model="register.address2" class="form-control text-box" id="address2" placeholder="Enter your Address.">
                            </div>
                            <div class="form-group">
                                <label>Country:</label>
                                <select onchange="print_state('regstate', this.selectedIndex);" ng-change="register.state=''" id="regcountry" name="regcountry" ng-model="register.country" class="form-control" required="required">
                                </select>
                            </div>
                            <div class="form-group">
                                <label>State:</label>
                                <select name="regstate" id ="regstate" class="form-control" ng-model="register.state" required="required">
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Zip Code:</label>
                                <input type="text" ng-model="register.zipcode" class="form-control text-box" id="zipcode" placeholder="Enter your Zipcode." required="required" only-digits>
                            </div>
                            <div class="form-group">
                                <label>Phone No:</label>
                                <input type="text" ng-model="register.phoneno" class="form-control text-box" id="phoneno" placeholder="Enter your phoneno." required="required" only-digits>
                            </div>
                            <div class="form-group">
                                <input type='button' class='btn btn-info btn-block btn-sm' name='next' value='Continue' ng-disabled="registration.$invalid" ng-click="registerUser_checkout(register, registration)" />
                            </div>
                          </fieldset>
                        </form>
                      </div> 

                      <script type="text/ng-template" id="successregistration">
                        <div ng-include="'/fe/tpl/ecommerce/successregistration.html'"></div>
                      </script>

                    </div>
                  </div>
                </div>

                <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                      <a class="collapsed class2" role="button" data-toggle="collapse" href="" data-parent="#accordion" aria-expanded="false" aria-controls="collapseThree">
                        SHIPPING METHOD
                      </a>
                    </h4>
                  </div>
                  <div id="shippingMethod" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                      <h3>Flat Rate</h3>
                      <p class="ec-font-s wrapper-md">
                        <strong>Fixed $5.00</strong>
                        <br><br>
                        <a class="btn btn-info w-m" ng-click="collapse(3)">Continue</a>
                      </p>
                    </div>
                  </div>
                </div>

                <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingFour">
                    <h4 class="panel-title">
                      <a class="collapsed class3" role="button" data-toggle="collapse" href="" data-parent="#accordion" aria-expanded="false" aria-controls="collapseFour">
                        PAYMENT INFORMATION
                      </a>
                    </h4>
                  </div>
                  <div id="paymentInformation" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                    <div class="panel-body">
                      <div class="row ec-font-s">
                        <div class="col-sm-12 form-group">
                          <a ng-click="collapse(2)">Shipping Method</a>
                        </div>

                        <form name="CCform" ng-submit="paymentmethod(payment)">
                          <div class="col-sm-12 form-group">
                            <div class="col-sm-3 font-bold">PAYMENT METHOD</div>
                            <div class="col-sm-4">
                              <select ng-model="payment.method" ng-init="payment.method = 'CC' " ng-change="test(payment.method)" class="form-control">
                                <option value="CC">Credit Card (CC)</option>
                                <option disabled value="P">Paypal (P)</option>
                                <option disabled value="EC">E-Check (EC)</option>
                              </select>
                            </div>
                          </div>

                          <hr>

                          <fieldset class="col-sm-12" ng-if="payment.method == 'CC' ">
                            <div class="col-sm-12 form-group">
                              <div class="col-sm-offset-3 col-sm-4">
                                <img src="/img/creditcards/amex.png" class="ec-CC-s"/>
                                <img src="/img/creditcards/visa.png" class="ec-CC-s"/>
                                <img src="/img/creditcards/discover.png" class="ec-CC-s"/>
                                <img src="/img/creditcards/mastercard.png" class="ec-CC-s"/>
                              </div>
                            </div>

                            <div class="col-sm-12 form-group">
                              <div class="col-sm-3 font-bold">Credit Card Number</div>
                              <div class="col-sm-4">
                                <input type="text" class="form-control" ng-model="payment.creditcardnumber" required/>
                              </div>
                            </div>

                            <div class="col-sm-12 form-group">
                              <div class="col-sm-3 font-bold">CCV Number <br> <em>(Card Code Verification)</em></div>
                              <div class="col-sm-4">
                                <input type="text" class="form-control" ng-model="payment.ccvnumber" required/>
                              </div>
                            </div>

                            <div class="col-sm-12 form-group">
                              <div class="col-sm-3 font-bold">Credit Card Expiration</div>
                              <div class="col-sm-2">
                                <label>Month</label>
                                <select class="form-control" ng-model="payment.cc_expimonth"
                                        ng-options="m.val as m.name for m in expimonth"required>
                                </select>
                              </div>
                              <div class="col-sm-2">
                                <label>Year</label>
                                <select class="form-control" ng-model="payment.cc_expiyear"
                                    ng-options="y for y in expiyear"
                                    required>
                                </select>
                              </div>
                            </div>

                            <div class="col-sm-12 form-group">
                              <div class="col-sm-offset-3 col-sm-4">
                                <button type="submit" class="btn btn-info">Continue</button>
                              </div>
                            </div>
                          </fieldset>

                          <fieldset class="col-sm-12" ng-if="payment.method == 'P' ">
                            <div class="col-sm-12 form-group">
                              <div class="col-sm-offset-3 col-sm-4">
                                <button type="submit" class="btn btn-info">Continue</button>
                              </div>
                            </div>
                          </fieldset>
                        </form>
                      </div>  
                    </div> 
                  </div>
                </div>

                <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingFive">
                    <h4 class="panel-title">
                      <a class="collapsed class4" role="button" data-toggle="collapse" href="" data-parent="#accordion" aria-expanded="true" aria-controls="collapseFive">
                        ORDER REVIEW
                      </a>
                    </h4>
                  </div>
                  <div id="orderReview" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                    <div class="panel-body">
                      <a ng-click="collapse(2)">Shipping Method</a> / <a ng-click="collapse(3)">Payment Information</a>
                      <hr>
                      <div class="table-responsive">
                        <table class="table table-striped b-t b-light ec-table-mycart">
                          <thead>
                            <tr>
                              <th style="min-width:120px"></th>
                              <th style="width:40%">ITEM</th>
                              <th style="width:15%">PRICE</th>
                              <th style="width:15%">QUANTITY</th>
                              <th style="width:15%">SUBTOTAL</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr ng-repeat="product in mycart | groupBy: 'productid' ">
                              <td>
                                <div class="ec-prod-mycartthumb" style="background:url('{[{amazonimage}]}/{[{product[0].productid}]}/{[{product[0].filename}]}');">
                                </div>
                              </td>
                              <td class="font-bold ec-font-s pointer" ng-bind="product[0].name" ng-click="gotoproduct(product[0].productcode)"></td>
                              <td>$<span ng-bind="product[0].price|number:2"></span></td>
                              <td ng-bind="product.length"></td>
                              <td>$<span ng-bind="product[0].price * product.length | number:2 "></span></td>
                            </tr>
                          </tbody>
                          <tfoot>
                            <tr style="border-top:1px solid #444; background:white">
                              <td colspan="4" style="text-align:right; font-weight:bold">
                                TOTAL <br><br>
                                Shipping & Handling (Flat Rate - Fixed) <br><br>
                                <span class="ec-font-s font-bold">GRAND TOTAL</span>
                              </td>
                              <td>
                                $<span ng-bind="total | number:2"></span> <br><br>
                                $5.00 <br><br>
                                <span class="ec-font-s font-bold">$<span ng-bind="granddtotal(5) | number:2"></span></span>
                              </td>
                            </tr>
                          </tfoot>
                        </table>
                      </div> 

                      <div class="text-c">
                        <a href="<?php echo $shop; ?>/mycart" class="ec-font-s">Click here to edit your cart</a> <br><br>
                        Or <br><br>
                        <a class="btn btn-lg btn-info" ng-click="PLACEORDER()">PLACE YOUR ORDER</a>
                      </div>

                    </div>
                  </div>
                </div>

              </div> 

            </div> 
          </div>
        </div>
      </div>
    </div>

    </div>
  </div>
</div>
