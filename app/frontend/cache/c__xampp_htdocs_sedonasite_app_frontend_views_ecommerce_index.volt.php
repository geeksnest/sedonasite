<div class="container-fluid" ng-controller="productsCtrl">
  <div class="row">
    <div class="col-sm-10 col-sm-offset-1 ec-nopad">

      <div class="col-md-12">

        <div ng-include="'/fe/tpl/ecommerce/header.html'"></div>
        <div ng-include="'/fe/tpl/ecommerce/menu.html'"></div>

        <div class="row">
          <div class="panel panel-default">
            <div class="panel-heading">
              <div class="row">
                <div class="col-sm-12">
                  <ul class="breadcrumb bg-white b-a" style="margin:0; border:none; border-bottom:1px solid #ccc;">
                    <li><a href=""><i class="fa fa-home"></i> Breadcrumb</a></li>
                    <li><a href="">Elements</a></li>
                    <li class="active">Components</li>
                  </ul>
                </div>
                <div class="col-sm-6 wrapper-sm">
                  <div class="input-group">
                      <span class="input-group-addon bg-gray dker">SORT BY:</span>
                      <select class="form-control ec-select">
                        <option>Popularity</option>
                        <option>Product Name</option>
                        <option>Price: Low to High</option>
                        <option>Price: High to Low</option>
                      </select>
                  </div>
                </div>
                <div class="col-sm-6 wrapper-sm">
                  <div class="input-group pull-right">
                      <span class="input-group-addon bg-gray dker">SHOW</span>
                      <select class="form-control ec-select">
                        <option>12</option>
                        <option>24</option>
                        <option>36</option>
                      </select>
                  </div>
                </div>
              </div>
            </div>

            <div class="panel-body">

              <div class="col-sm-3 wrapper-md ec-prod-tile" ng-repeat="product in products" style="vertical-align:top;">
                <div class="pointer" ng-click="gotoproduct(product.productcode)">
                  <div class="ec-prod-mainthumb" style="background:#fff url('{[{amazonimage}]}/{[{product.productid}]}/{[{product.filename}]}');"></div>
                  <div>
                      <span class="ec-prod-landingtitle" ng-bind="product.name"></span>
                      <table style="width:100%;">
                        <tr ng-show="product.discount != undefined && product.discount > 0">
                          <td colspan="2" class="ec-prod-originalprice">
                            before <strike>$<span ng-bind="product.price"></span></strike>
                          </td>
                        </tr>
                        <tr>
                          <td class="ec-prod-landingprice text-danger">
                            $<span ng-bind="productPrice(product.price, product.discount)|number:2"></span>

                            <span class="ec-discount bg-danger wrapper-xs" ng-show="product.discount != undefined && product.discount > 0">
                              <span ng-bind="product.discount"></span>% <br> off
                            </span>
                          </td>
                        </tr>
                      </table>
                    </div> <!-- END OF unclassed div -->
                  </div> <!-- END OF pointer -->
                    <button type="button" class="btn bg-orange btn-block" style="vertical-align:bottom;"
                            ng-click="addtocart(product.productid, product.minquantity)"
                            ng-disabled="itemQuantityOnCart(product.productid) + convertToInt(product.minquantity) > product.maxquantity">ADD TO CART</button>
              </div>

            </div>
          </div> <!-- END OF PANEL -->
        </div>
      </div>
    </div>
  </div> <!-- END OF OUTER ROW -->

  <script type="text/ng-template" id="addtocart">
    <div ng-include="'/fe/tpl/ecommerce/addtocart.html'"></div>
  </script>
</div> <!-- END OF CONTAINER-FLUID -->
