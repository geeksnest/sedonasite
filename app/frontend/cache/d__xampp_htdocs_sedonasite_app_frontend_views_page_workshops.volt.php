
    <!-- Banner starts -->
  <!--   <div class="banner-container workshops-bg" title="<?php echo $title; ?>">
        <img src="img/frontend/workshops_banner.jpg" class="pinterest-img" alt="<?php echo $title; ?>">
        <div class="black-box">
            <span class="banner-title"><?php echo $workshops[0]->cattitle;?></span>
            <br/>
            <?php if($workshops[0]->catsubtitle !=''){echo "<span class='banner-sub-title1'>".$workshops[0]->catsubtitle."</span><br/>";}?>
        </div>
    </div> -->

    <!-- Banner ends -->
<!-- Below SlidEr starts -->
  <div class="container-fluid">
        <div id="myCarousel" class="carousel slide" data-ride='carousel'>

<!-- Indicators -->
            <ol class="carousel-indicators" style="margin-bottom:-10px">
            <?php
            $nslides = json_decode($slides);
            $dst = 0;
            foreach ($nslides as $key => $value) {
            ?>
                <li data-target="#myCarousel" data-slide-to="<?php echo $dst; ?>"
                 class="<?php if($nslides[$key]->sort == 1){ echo 'active'; } ?>"></li>
            <?php $dst++; } ?>
            </ol>

<!-- Wrapper for Slides -->
            <div class="carousel-inner">
            <?php
            foreach ($nslides as $key => $value) {
                ?>
                <div class="item <?php if($nslides[$key]->sort == 1){ echo 'active'; } ?>">
                    <div class="sidebarimage" style="background-image: url(<?php echo $this->config->application->amazonlink; ?>/uploads/slider/<?php echo $nslides[$key]->foldername.'/'.$nslides[$key]->img; ?>);">
                    </div>
                        <div class="banner-container banner-content-wrapper-green">
                           <!--  <div>
                                <font class="slider-title"><?php echo $nslides[$key]->title; ?></font><br>
                                <div class='col-sm-8 col-md-offset-1'>
                               <div class='slider-desc text-left'> <p class="desc"><?php echo $nslides[$key]->description; ?></p> </div>
                               </div>
                            </div> -->
                            <?php if($nslides[$key]->position=='right') { ?>
                            <div class="col-sm-6 col-sm-offset-6" style='padding:7.25vw'> <!-- RIGHT ALIGN-->

                            <?php if($nslides[$key]->box=='on') { ?>
                              <div class="Slider-content" > <!-- BOX ON-->
                                <span class="slider-title" style="color:<?php echo $nslides[$key]->titlecolor; ?>; font-size:<?php $size=$nslides[$key]->titlesize/16; echo $size; ?>vw "><?php echo $nslides[$key]->title; ?></span>
                                <br>
                                <span class="slider-desc" style="color:<?php echo $nslides[$key]->desccolor; ?>;font-size:<?php $size=$nslides[$key]->descsize/16; echo $size; ?>vw">
                                  <?php echo strip_tags($nslides[$key]->description); ?>
                              </span>
                             </div>
                            <?php } else {?>
                            <div class="Services-outcontent"> <!-- BOX OFF-->
                            <!-- TITLE -->
                                <span class="slider-title" style="color:<?php echo $nslides[$key]->titlecolor; ?>; font-size:<?php $size=$nslides[$key]->titlesize/16; echo $size; ?>vw "><?php echo $nslides[$key]->title; ?></span>
                                <br>
                                <span class="slider-desc" style="color:<?php echo $nslides[$key]->desccolor; ?>;font-size:<?php $size=$nslides[$key]->descsize/16; echo $size; ?>vw">
                                  <?php echo strip_tags($nslides[$key]->description); ?>
                              </span>
                             </div>
                             <?php } ?>


                      </div>

                      <?php } if($nslides[$key]->position=='center') { ?>
                            <div class="col-sm-6 col-sm-offset-2 slider-center" style='padding:7.25vw'> <!-- Center ALIGN-->

                            <?php if($nslides[$key]->box=='on') { ?>
                              <div class="Slider-content" > <!-- BOX ON-->
                                <span class="slider-title" style="color:<?php echo $nslides[$key]->titlecolor; ?>; font-size:<?php $size=$nslides[$key]->titlesize/16; echo $size; ?>vw "><?php echo $nslides[$key]->title; ?></span>
                                <br>
                                <span class="slider-desc" style="color:<?php echo $nslides[$key]->desccolor; ?>;font-size:<?php $size=$nslides[$key]->descsize/16; echo $size; ?>vw">
                                  <?php echo strip_tags($nslides[$key]->description); ?>
                              </span>
                             </div>
                            <?php } else {?>
                            <div class="Services-outcontent"> <!-- BOX OFF-->
                            <!-- TITLE -->
                                <span class="slider-title" style="color:<?php echo $nslides[$key]->titlecolor; ?>; font-size:<?php $size=$nslides[$key]->titlesize/16; echo $size; ?>vw "><?php echo $nslides[$key]->title; ?></span>
                                <br>
                                <span class="slider-desc" style="color:<?php echo $nslides[$key]->desccolor; ?>;font-size:<?php $size=$nslides[$key]->descsize/16; echo $size; ?>vw">
                                  <?php echo strip_tags($nslides[$key]->description); ?>
                              </span>
                             </div>
                             <?php } ?>


                      </div>
                      <?php } else if ($nslides[$key]->position=='left') { ?>
                      <div class="col-sm-6" style='padding:7.25vw'> <!-- LEFT ALIGN-->
                         <?php if($nslides[$key]->box=='on') { ?>
                              <div class="Slider-content"> <!-- BOX ON-->
                                <span class="slider-title" style="color:<?php echo $nslides[$key]->titlecolor; ?>;font-size:<?php $size=$nslides[$key]->titlesize/16; echo $size; ?>vw "><?php echo $nslides[$key]->title; ?></span>
                                <br>
                                <span class="slider-desc" style="color:<?php echo $nslides[$key]->desccolor; ?>;font-size:<?php $size=$nslides[$key]->descsize/16; echo $size; ?>vw">
                                  <?php echo strip_tags($nslides[$key]->description); ?>
                              </span>
                             </div>
                            <?php } else {?>
                            <div class="Services-outcontent"> <!-- BOX OFF-->
                                <span class="slider-title" style="color:<?php echo $nslides[$key]->titlecolor; ?>;font-size:<?php $size=$nslides[$key]->titlesize/16; echo $size; ?>vw "><?php echo $nslides[$key]->title; ?></span>
                                <br>
                                <span class="slider-desc" style="color:<?php echo $nslides[$key]->desccolor; ?>;font-size:<?php $size=$nslides[$key]->descsize/16; echo $size; ?>vw">
                                  <?php echo strip_tags($nslides[$key]->description); ?>
                              </span>
                             </div>
                             <?php } ?>
                  </div>
                  <?php } ?>


                </div>
                </div>
            <?php } ?>
            </div>

<!-- Controls -->

            <a class="left carousel-control" href="#myCarousel" data-slide="prev" style='height:41vw'>
                <span class="icon-prev"></span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next" style='height:41vw'>
                <span class="icon-next"></span>
            </a>
        </div>

    
    <!-- Below Banner ends -->

<?php
      $getginfo = $workshops;
      foreach ($getginfo as $key => $value) {
?>

<div class="container-fluid Services">
    <div class="Services-bg" style="background:linear-gradient( rgba(0, 0, 0, 0), rgba(0, 0, 0, .9) ), url('<?php echo $imageLink.'/uploads/pageimage/'. $getginfo[$key]->banner  ?>')">
      
    </div>

    <div class="Services-body <?php echo $getginfo[$key]->classname; ?>">
        <?php if($getginfo[$key]->textalign == 'right') { ?>  
                  <div class="col-sm-6 col-sm-offset-6 Services-content" style="color:<?php echo $getginfo[$key]->color; ?>">

                    <?php  if($getginfo[$key]->box == 'true') { ?>
                      <div class="Services-box">
                    <?php } else { ?>
                      <div>
                    <?php }

              } elseif ($getginfo[$key]->textalign == 'left') { ?> 
                  <div class="col-sm-6 Services-content" style="color:<?php echo $getginfo[$key]->color; ?>">

                    <?php  if($getginfo[$key]->box == 'true') { ?>
                      <div class="Services-box">
                    <?php } else { ?>
                      <div>
                    <?php }

              } else { ?> 
                  <div class="col-sm-6 col-sm-offset-3 Services-content" style="color:<?php echo $getginfo[$key]->color; ?>">

                    <?php  if($getginfo[$key]->box == 'true') { ?>
                      <div class="Services-box">
                    <?php } else { ?>
                      <div>
                    <?php }
              }  ?>

              
              <span class="Services-title"><?php echo $getginfo[$key]->title; ?></span>
              <span class="Services-description">
                <?php echo strip_tags($getginfo[$key]->thumbdesc); ?>
                <?php echo $color[$key]; ?>
                <a href="<?php echo $base_url;?>/readings/<?php echo $getginfo[$key]->pageslugs;?>" class="Services-learnmore">Learn more.</a>
              </span>
            </div> 
          </div> 
    </div> 
</div> 
        <style>
          @media screen and (max-width:767px) {
            .<?php echo $getginfo[$key]->classname; ?> {
              background:<?php echo $getginfo[$key]->bgcolor; ?>;
            }
          }
        </style>
<?php } ?> 
