<!DOCTYPE html>
<html lang="en" data-ng-app="app">
<head>
	<title>Sedona Healing Arts | Confirmation</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="google-site-verification" content="olwczc3nc3-vWAy4G2_0-L87S-Sjw_HvXrYI26uRjKc" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="user-scalable = yes" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <link rel="shortcut icon" href="/img/frontend/favicon.png">

    <!-- Bootstrap -->
    <link href="/vendors/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <link href="/vendors/bootstrap/dist/css/bootstrap-theme.css" rel="stylesheet">

    <!-- custom style -->
    <link href="/fe/style/registration.css" rel="stylesheet">
    <!-- Font awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Custom stylesheet -->
    <link href="/fe/style/custom-style.css" rel="stylesheet">

    <style type="text/css">
    	html,
    	body {
    		margin: 0;
    		padding: 0;
    		min-height: 100%; /* needed for container min-height */
    		background-position: center bottom 0px;
    	}
    	.container-fluid {
    		position: relative; /* needed for footer positioning*/
    		height: auto !important; /* real browsers */
    		min-height: 100%; /* real browsers */
    	}
    	.centercontent {
    		position: relative; /* needed for footer positioning*/
    		height: auto !important; /* real browsers */
    		min-height: 100%; /* real browsers */
    	}
    	div#header {
    		padding: 1em;
    		background: #efe;
    	}
    	div#content {
    		/* padding:1em 1em 5em; *//* bottom padding for footer */
    	}
    	div#footer {
    		position: absolute;
    		width: 100%;
    		bottom: 0; /* stick to bottom */
    		background: #ddd;
    	}
    	
    </style>

</head>
<body>

	<div class="container-fluid">
		<div class="row">

			<?php

			if($responsetype == 'success'){

			?>

			

			<!-- Success -->
			<div class="existcentercontent">
				<div class="">
					<div class="col-lg-12">
						<div class="reglogo">
							<a href="/"><img src="/img/frontend/reglogo.png" style="width:100%;height:auto;"></a>
						</div>
					</div>
					<div class="col-lg-12 existcontainer">
						<p class="existtext">Thank you for completing your membership. You can now proceed to <a href="/sd/login">login</a></p>
					</div>
					<div class="col-lg-12">
						<a href="/"><h4>Back Home Page</h4></a>
						<br>
						<br>
						<br>
					</div>
				</div>
			</div>
			<!-- end of Success -->

			<?php

			}
			else {

			?>


			<!-- Error -->
			<div class="existcentercontent">
				<div class="">
					<div class="col-lg-12">
						<div class="reglogo">
							<a href="/"><img src="/img/frontend/reglogo.png" style="width:100%;height:auto;"></a>
						</div>
					</div>
					<div class="col-lg-12 existcontainer">
						<p class="existtext">The Verification code is either invalid or expired.</p>
					</div>
					<div class="col-lg-12">
						<a href="/"><h4>Back Home Page</h4></a>
						<br>
						<br>
						<br>
					</div>
				</div>
			</div>
			<!-- end of Error -->


			<?php

			}

			?>

			<div class='col-sm-12'>
              <div class="paragraph" style="text-align:center;">
                 <p class="copy">Copyright © 2015 · All Rights Reserved · Sedona Healing Arts · 201 State Route 179, Sedona, AZ 86336
                 </p>
              </div>
            </div>
		</div>
	</div>



	
</body>
</html>
<?php echo $this->getContent()?>