<!DOCTYPE html>
<html lang="en" data-ng-app="app">
<head>
	<title>Sedona Healing Arts | Registration</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="google-site-verification" content="olwczc3nc3-vWAy4G2_0-L87S-Sjw_HvXrYI26uRjKc" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="user-scalable = yes" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <link rel="shortcut icon" href="/img/frontend/favicon.png">

    <!-- Bootstrap -->
    <link href="/vendors/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <link href="/vendors/bootstrap/dist/css/bootstrap-theme.css" rel="stylesheet">

    <!-- custom style -->
    <link href="/fe/style/registration.css" rel="stylesheet">
    <!-- Font awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Custom stylesheet -->
    <link href="/fe/style/custom-style.css" rel="stylesheet">
    <!-- Angularjs notify -->
    <link href="/vendors/angular-ui-notification/dist/angular-ui-notification.min.css" rel="stylesheet">

    <style type="text/css">
    	html,
    	body {
    		margin: 0;
    		padding: 0;
    		min-height: 100%; /* needed for container min-height */
    		background-position: center bottom 0px;
    	}
    	.container-fluid {
    		position: relative; /* needed for footer positioning*/
    		height: auto !important; /* real browsers */
    		min-height: 100%; /* real browsers */
    	}
    	.centercontent {
    		position: relative; /* needed for footer positioning*/
    		height: auto !important; /* real browsers */
    		min-height: 100%; /* real browsers */
    	}
    	div#header {
    		padding: 1em;
    		background: #efe;
    	}
    	div#content {
    		/* padding:1em 1em 5em; *//* bottom padding for footer */
    	}
    	div#footer {
    		position: absolute;
    		width: 100%;
    		bottom: 0; /* stick to bottom */
    		background: #ddd;
    	}

    </style>

</head>
<body ng-controller="UserregistrationCtrl" ng-cloak>

	<div class="container-fluid">
		<div class="row">

			<!-- registration form -->
			<div class="centercontent" ng-show="regform">
				<div class="">
					<div class="col-lg-12">
						<a href="/"><img src="/img/frontend/reglogo.png" style="width:100%;height:auto;"></a>
					</div>
					<div class="col-lg-12 errorcontainer" ng-show="errorpanel">
						<p class="errortext">{[{errormsg}]}</p>
						<p>{[{errormsg1}]}</p>
					</div>
					<div class="col-lg-12 fieldscontainer">
						<div class="a-spacing-small">
							Create account
						</div>
						<form name="registration" class="form-reservation">
                            <fieldset ng-disabled="saving">
                        <div class="form-group">
                            <label>Email: </label>
                            <input type="email" ng-model="register.email" class="form-control text-box" id="email" placeholder="Enter your Email." required="required" ng-blur="checkEmail(register.email)">
                            <label ng-show="emailexist==1" class="errortext"> This email already belongs to someone. Do you want to use this and login? <a href="" ng-click="login=true; ll.loginemailusername = register.email; register.email = ''; emailexist=0; "> Yes </a> </label>
                            <span ng-show="sendingcompletionlinksuccess">Please check your email for instructions.</span>
                            <span ng-show="sendingcompletionlink">Sending Completion Link...</span>
                            <label  ng-show="emailexist==3 && !sendingcompletionlink"> We have detected that this email have been registered offline, click yes to receive the link to complete your profile? <a href="" ng-click="sendUserCompletionLink(register.email); emailexist=0;"> Yes </a> </label>
                        </div>
                        <div class="form-group">
                            <label>Username:</label>
                            <input type="text" ng-model="register.username" name="username" class="form-control text-box" id="username" placeholder="Enter your Username." required="required" ng-pattern="/^[a-zA-Z0-9]*$/">
                            <span class="help-block" ng-show="registration.username.$error.pattern">Alphanumeric characters only.</span>
                        </div>
                        <div class="form-group">
                            <label>Password:</label>
                            <input type="password" ng-model="register.password" name="password" class="form-control text-box" id="password" placeholder="Enter your Password." pass-strength required="required" ng-minlength="6" >
                            <span class="help-block" ng-if="registration.password.$error.minlength">Minimum of 6 characters.</span>
                        </div>
                        <div class="form-group">
                            <label>Retype Password:</label>
                            <input type="password" ng-model="register.repassword" class="form-control text-box" id="repassword" name="repassword" placeholder="Retype your Password." required="required" pw-check="password">
                              <span class="msg-error" ng-show="registration.repassword.$error.pwmatch">
                                Passwords don't match.
                              </span>
                        </div>
                        <div class="form-group">
                            <label>Lastname:</label>
                            <input type="text" ng-model="register.lastname" class="form-control text-box" id="lastname" placeholder="Enter your Lastname." required="required">
                        </div>
                        <div class="form-group">
                            <label>Firstname:</label>
                            <input type="text" ng-model="register.firstname" class="form-control text-box" id="firstname" placeholder="Enter your Firstname." required="required">
                        </div>
                        <div class="form-group">
                            <label>Address 1:</label>
                            <input type="text" ng-model="register.address1" class="form-control text-box" id="address1" placeholder="Enter your Address." required="required">
                        </div>
                        <div class="form-group">
                            <label>Address 2:</label>
                            <input type="text" ng-model="register.address2" class="form-control text-box" id="address2" placeholder="Enter your Address.">
                        </div>
                        <div class="form-group">
                            <label>Country:</label>
                            <select onchange="print_state('regstate', this.selectedIndex);" ng-change="register.state=''" id="regcountry" name="regcountry" ng-model="register.country" class="form-control" required="required">
                            </select>
                        </div>
                        <div class="form-group">
                            <label>State:</label>
                            <select name="regstate" id ="regstate" class="form-control" ng-model="register.state" required="required">
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Zip Code:</label>
                            <input type="text" ng-model="register.zipcode" class="form-control text-box" id="zipcode" placeholder="Enter your Zipcode." required="required" only-digits>
                        </div>
                        <div class="form-group">
                            <label>Phone No:</label>
                            <input type="text" ng-model="register.phoneno" class="form-control text-box" id="phoneno" placeholder="Enter your phoneno." required="required" only-digits>
                        </div>
                        <div class="form-group">
                            <input type='button' class='btn btn-fill btn-info btn-wd btn-sm form-control' name='next' value='Register' ng-disabled="registration.$invalid" ng-click="registerUser(register, registration)" />
                        </div>
                        <div class="form-group" style="text-align:center;font-weight: bold;">
                        	Already have an account?
                        </div>
                        <div class="form-group">
                            <a class='btn btn-fill btn-warning btn-wd btn-sm form-control' name='signin' href="/sd/login" />Sign in</a>
                        </div>
                                </fieldset>
                        </form>

						<div class="overlay" ng-show="saveloading">
							<i class="fa fa-refresh fa-spin" style="font-size:50px;"></i>
						</div>
					</div>
				</div>
			</div>
			<!-- end of registration form -->

			<!-- Email exist -->
			<!-- <div class="existcentercontent" ng-show="emailexist">
				<div class="">
					<div class="col-lg-12">
						<div class="reglogo">
							<img src="/img/frontend/reglogo.png" style="width:100%;height:auto;">
						</div>
					</div>
					<div class="col-lg-12 existcontainer">
						<p class="existtext">Email address already in use</p>
						<p>You indicated you are a new customer, but the email address <strong>{[{email}]}</strong> is already exists.</p>
					</div>
					<div class="col-lg-12">
						<h4>Are you a returning customer?</h4>
						<a href="/">Sign In</a>
						<br>
						<a href="">Forgot your password?</a>
						<br>
						<br>
						<h4>New to Sedonahealingarts.com?</h4>
						<p>Create a new account with a <a href="" ng-click="backtoreg()">different e-mail address</a></p>
						<br>
						<br>
						<br>
					</div>
				</div>
			</div> -->
			<!-- end of Email exist -->

			<!-- Success -->
			<div class="existcentercontent" ng-show="regsuccess">
				<div class="">
					<div class="col-lg-12">
						<div class="reglogo">
							<a href="/"><img src="/img/frontend/reglogo.png" style="width:100%;height:auto;"></a>
						</div>
					</div>
					<div class="col-lg-12 existcontainer">
						<p class="existtext">You are now registerd to Sedonahealingarts.com</p>
						<p>An email has been sent to this email address <strong>{[{email}]}</strong> with a link to verify your account. Make sure to check your email to and follow the link to complete your account sign up.</p>
					</div>
					<!-- <div class="col-lg-12">
						<a href="/"><h4>Back Home Page</h4></a>
						<br>
						<br>
						<br>
					</div> -->
				</div>
			</div>
			<!-- end of Success -->

			<div class='col-sm-12'>
              <div class="paragraph" style="text-align:center;">
                 <p class="copy">Copyright © 2015 · All Rights Reserved · Sedona Healing Arts · 201 State Route 179, Sedona, AZ 86336
                 </p>
              </div>
            </div>
		</div>
	</div>


	<!-- jQuery -->
	<script src="/vendors/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- Agularjs -->
	<script src="/vendors/angular/angular.min.js"></script>
	<!-- passstrength -->
	<script src="/vendors/pass-strength/src/strength.min.js"></script>
	<!-- Angularjs Storage -->
	<script src="/be/js/angular/ngStorage.min.js"></script>
    <!-- angular bootstrap -->
     <script src="/vendors/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
	<!-- Angularjs Storage -->
	<script src="/vendors/a0-angular-storage/dist/angular-storage.min.js"></script>
	<!-- JWT -->
	<script src="/vendors/angular-jwt/dist/angular-jwt.js"></script>
	<!-- Notifications -->
    <script type="text/javascript" src="/vendors/angular-ui-notification/dist/angular-ui-notification.min.js"></script>

	<!-- Notifications -->
    <script type="text/javascript" src="/vendors/angular-ui-notification/dist/angular-ui-notification.min.js"></script>
    <!-- facebook -->
    <script src="/vendors/angular-facebook/lib/angular-facebook.js"></script>
    <!-- angular bootstrap -->
    <script src="/vendors/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>

	<script type="text/javascript">

	</script>

	<script src="/fe/scripts/app2.js"></script>

	<!-- Agularjs -->
	<script src="/fe/scripts/controllers/userregistration.js"></script>
	<script src="/fe/scripts/factory/userregistration.js"></script>
	<script src="/fe/scripts/countries3.js"></script>
	<script src="/fe/scripts/custom.js"></script>
	<script src="/fe/scripts/directives/showErrors.js"></script>
	<script src="/fe/scripts/directives/validations.js"></script>
	<script src="/fe/scripts/factory/reservation.js"></script>

</body>
</html>
