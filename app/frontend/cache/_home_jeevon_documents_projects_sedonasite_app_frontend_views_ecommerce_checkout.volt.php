<style>
  .msg-error {
    color: red;
  }
</style>

<div class="container-fluid bg-light" ng-controller="checkoutCtrl">
  <toaster-container toaster-options="{'position-class': 'toast-bottom-left'}"></toaster-container>
  <div class="row">
    <div class="col-sm-10 col-sm-offset-1 bg-light ec-nopad">

    <div class="col-sm-12 wrapper-md">

      <div ng-include="'/fe/tpl/ecommerce/header.html'"></div>
      <div ng-include="'/fe/tpl/ecommerce/menu.html'"></div>

      <div class="row">
        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading">CHECKOUT</div>
            <div class="panel-body">
              <div class="existcentercontent" ng-if="emailverify">
                <div class="">
                  <div class="col-lg-12 existcontainer">
                    <p class="existtext">You are now registerd to Sedonahealingarts.com</p>
                    <p>An email has been sent to this email address <strong>{[{email}]}</strong> with a link to verify your account. Make sure to check your email to and follow the link to complete your account sign up.</p>
                  </div>
                  <!-- <div class="col-lg-12">
                    <a href="/"><h4>Back Home Page</h4></a>
                    <br>
                    <br>
                    <br>
                  </div> -->
                </div>
              </div>

              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" ng-if="!emailverify">
                <div class="panel panel-default" ng-hide="token">
                  <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                      <a role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="collapseOne" ng-click="collapse(0)">
                        CHECKOUT METHOD
                      </a>
                    </h4>
                  </div>
                  <div id="checkoutMethod" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body" ng-controller="UserloginCtrl">
                      <div class="col-sm-8" style="vertical-align:bottom;">
                        <em>Register with us for future convenience:</em>
                        <br><br>
                        <a class="btn btn-info" ng-click="collapse(1)">Register & Checkout</a>
                      </div>
                      <div class="col-sm-4">
                        <h3>SIGN IN & CHECKOUT</h3>
                        <em>Sign in to speed up your checkout process</em>
                        <br><br>
                        <form name="registration" class="form-reservation">
                            <fieldset ng-disabled="saving">
		                        <div class="form-group">
		                            <label>Email or Username</label>
		                            <input type="eusernameail" ng-model="userlogin.username" class="form-control text-box" id="username" placeholder="Enter Email or Username." required="required">
		                        </div>
		                        <div class="form-group">
		                            <label>Password</label>
		                            <input type="password" ng-model="userlogin.password" name="password" class="form-control text-box" id="password" placeholder="Enter your Password."required="required">
		                        </div>
		                        <div class="form-group">
		                            <input type='button' class='btn btn-info btn-block btn-sm' name='signin' value='Login' ng-click="dologin_checkout(userlogin)" />
		                        </div>
                            <div class="form-group text-c" ng-show="errormsg">
                              <em class="text-danger" ng-bind="errormsg"></em> <br>
                              <em>For newly registered account please check your email and follow the verification process. Thank you.</em>
                              <br>
                            </div>
		                        <div class="form-group" style="text-align:center;font-weight: bold;">
		                           	Or sign in using
		                        </div>
		                        <div class="form-group" style="text-align:center;font-weight: bold;">
		                        	<a class="btn btn-social btn-facebook" ng-click="facebooklogin()" style="width:100%; margin:1px;">
		                        		<span class="fa fa-facebook"></span> Sign in with Facebook
		                        	</a>
		                        	<a class="btn btn-social btn-twitter" style="width:100%; margin:1px;">
		                        		<span class="fa fa-twitter"></span> Sign in with Twitter
		                        	</a>
		                        	<a class="btn btn-social btn-google" style="width:100%; margin:1px;">
		                        		<span class="fa fa-google-plus"></span> Sign in with Google
		                        	</a>
		                        </div>
                            </fieldset>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default" ng-hide="token">
                  <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                      <a class="collapsed class1" role="button" data-toggle="collapse" data-parent="#accordion" href="" aria-expanded="true" aria-controls="collapseTwo">
                        BILLING INFORMATION
                      </a>
                    </h4>
                  </div>
                  <div id="billingInformation" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                      <div class="col-sm-4">
                        <form name="registration" class="form-reservation">
                          <fieldset ng-disabled="saving">
                            <div class="form-group">
                                <label>Email: </label>
                                <input type="email" ng-model="register.email" class="form-control text-box" id="email" placeholder="Enter your Email." required="required" ng-change="checkEmail(register.email)">
                                <label ng-show="emailexist==1" class="errortext"> This email already belongs to someone. Do you want to use this and login? <a href="" ng-click="collapse(0); userlogin.username = register.email; register.email = ''; emailexist=0; "> Yes </a> </label>
                                <span ng-show="sendingcompletionlinksuccess">Please check your email for instructions.</span>
                                <span ng-show="sendingcompletionlink">Sending Completion Link...</span>
                                <label  ng-show="emailexist==3 && !sendingcompletionlink"> We have detected that this email have been registered offline, click yes to receive the link to complete your profile? <a href="" ng-click="sendUserCompletionLink(register.email); emailexist=0;"> Yes </a> </label>
                            </div>
                            <div class="form-group">
                                <label>Username:</label>
                                <input type="text" ng-model="register.username" name="username" class="form-control text-box" id="username" placeholder="Enter your Username." required="required" ng-pattern="/^[a-zA-Z0-9]*$/">
                                <span class="help-block" ng-show="registration.username.$error.pattern">Alphanumeric characters only.</span>
                            </div>
                            <div class="form-group">
                                <label>Password:</label>
                                <input type="password" ng-model="register.password" name="password" class="form-control text-box" id="password" placeholder="Enter your Password." pass-strength required="required" ng-minlength="6" ng-change="passwordmatch(register.password, register.repassword)">
                                <span class="help-block msg-error" ng-if="registration.password.$error.minlength">Minimum of 6 characters.</span>
                            </div>
                            <div class="form-group">
                                <label>Retype Password:</label>
                                <input type="password" ng-model="register.repassword" class="form-control text-box" id="repassword" name="repassword" placeholder="Retype your Password." required="required" ng-change="passwordmatch(register.password, register.repassword)">
                                  <span class="msg-error" ng-if="repasswordconf">
                                    Passwords don't match.
                                  </span>
                            </div>
                            <div class="form-group">
                                <label>Lastname:</label>
                                <input type="text" ng-model="register.lastname" class="form-control text-box" id="lastname" placeholder="Enter your Lastname." required="required">
                            </div>
                            <div class="form-group">
                                <label>Firstname:</label>
                                <input type="text" ng-model="register.firstname" class="form-control text-box" id="firstname" placeholder="Enter your Firstname." required="required">
                            </div>
                            <div class="form-group">
                                <label>Address 1:</label>
                                <input type="text" ng-model="register.address1" class="form-control text-box" id="address1" placeholder="Enter your Address." required="required">
                            </div>
                            <div class="form-group">
                                <label>Address 2:</label>
                                <input type="text" ng-model="register.address2" class="form-control text-box" id="address2" placeholder="Enter your Address.">
                            </div>
                            <div class="form-group">
                                <label>City:</label>
                                <input type="text" ng-model="register.city" class="form-control text-box" id="city" placeholder="Enter your City.">
                            </div>
                            <div class="form-group">
                                <label>Country:</label>
                                <select onchange="print_state('regstate', this.selectedIndex);" ng-change="register.state=''" id="regcountry" name="regcountry" ng-model="register.country" class="form-control" required="required">
                                </select>
                            </div>
                            <div class="form-group">
                                <label>State:</label>
                                <select name="regstate" id ="regstate" class="form-control" ng-model="register.state" required="required">
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Zip Code:</label>
                                <input type="text" ng-model="register.zipcode" class="form-control text-box" id="zipcode" placeholder="Enter your Zipcode." required="required" only-digits>
                            </div>
                            <div class="form-group">
                                <label>Phone No:</label>
                                <input type="text" ng-model="register.phoneno" class="form-control text-box" id="phoneno" placeholder="Enter your phoneno." required="required" only-digits>
                            </div>
                            <div class="form-group">
                                <input type='button' class='btn btn-info btn-block btn-sm' name='next' value='Continue' ng-disabled="registration.$invalid" ng-click="registerUser_checkout(register, registration)" />
                            </div>
                          </fieldset>
                        </form>
                      </div> 

                      <!-- <script type="text/ng-template" id="successregistration">
                      //   <div ng-include="'/fe/tpl/ecommerce/successregistration.html'"></div>
                      </script> -->

                    </div>
                  </div>
                </div>

                <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                      <a class="collapsed class2" role="button" data-toggle="collapse" href="" data-parent="#accordion" aria-expanded="false" aria-controls="collapseThree">
                        SHIPPING METHOD
                      </a>
                    </h4>
                  </div>
                  <div id="shippingMethod" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                      <h3>Flat Rate {[{ shippingfee | currency }]}</h3>
                      <form name="shippingform" class="hiddenoverflow" style="margin-top: 20px;">
                        <fieldset class="col-sm-12">
                          <div class="col-sm-12 form-group">
                            <div class="col-sm-3 font-bold">First Name</div>
                            <div class="col-sm-4">
                              <input type="text" class="form-control" ng-model="shippinginfo.firstname" required/>
                            </div>
                          </div>

                          <div class="col-sm-12 form-group">
                            <div class="col-sm-3 font-bold">Last Name</div>
                            <div class="col-sm-4">
                              <input type="text" class="form-control" ng-model="shippinginfo.lastname" required/>
                            </div>
                          </div>

                          <div class="col-sm-12 form-group">
                            <div class="col-sm-3 font-bold">Address</div>
                            <div class="col-sm-4">
                              <input type="text" class="form-control" ng-model="shippinginfo.address" required/>
                            </div>
                          </div>

                          <div class="col-sm-12 form-group">
                            <div class="col-sm-3 font-bold">Address 2 (Optional)</div>
                            <div class="col-sm-4">
                              <input type="text" class="form-control" ng-model="shippinginfo.address2"/>
                            </div>
                          </div>

                          <div class="col-sm-12 form-group">
                            <div class="col-sm-3 font-bold">City</div>
                            <div class="col-sm-4">
                              <input type="text" class="form-control" ng-model="shippinginfo.city" required/>
                            </div>
                          </div>

                          <div class="col-sm-12 form-group">
                            <div class="col-sm-3 font-bold">Country</div>
                            <div class="col-sm-4">
                              <div ng-model="shippinginfo.country" country-select></div>
                            </div>
                          </div>

                          <div class="col-sm-12 form-group">
                            <div class="col-sm-3 font-bold">State/Province</div>
                            <div class="col-sm-4">
                              <div country="{[{ shippinginfo.country }]}" ng-model="shippinginfo.state" state-select></div>
                            </div>
                          </div>

                          <div class="col-sm-12 form-group">
                            <div class="col-sm-3 font-bold">Zip/Postal Code</div>
                            <div class="col-sm-4">
                              <input type="text" class="form-control" allow-pattern="\d" ng-model="shippinginfo.zipcode" required/>
                            </div>
                          </div>

                          <div class="col-sm-12 form-group">
                            <div class="col-sm-3 font-bold">Phone Number</div>
                            <div class="col-sm-4">
                              <input type="text" class="form-control" allow-pattern="\d" ng-model="shippinginfo.phoneno" required/>
                            </div>
                          </div>

                          <div class="col-sm-12 form-group">
                            <div class="col-sm-offset-3 col-sm-4">
                              <button type="button" ng-click="collapse(3)" class="btn btn-info" ng-disabled="shippingform.$invalid">Continue</button>
                            </div>
                          </div>
                        </fieldset>
                      </form>
                    </div>
                  </div>
                </div>

                <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingFour">
                    <h4 class="panel-title">
                      <a class="collapsed class3" role="button" data-toggle="collapse" href="" data-parent="#accordion" aria-expanded="false" aria-controls="collapseFour">
                        PAYMENT INFORMATION
                      </a>
                    </h4>
                  </div>
                  <div id="paymentInformation" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                    <div class="panel-body">
                      <div class="row ec-font-s">
                        <div class="col-sm-12 form-group">
                          <a ng-click="collapse(2)">Shipping Method</a>
                        </div>

                        <form name="CCform" ng-submit="paymentmethod(payment)">
                          <div class="col-sm-12 form-group">
                            <div class="col-sm-3 font-bold">PAYMENT METHOD</div>
                            <div class="col-sm-4">
                              <select ng-model="paymenttype" ng-change="test(payment.method)" class="form-control">
                                <option value="card">Credit Card (CC)</option>
                                <option value="paypal">Paypal (P)</option>
                                <option value="echeck">E-Check (EC)</option>
                              </select>
                            </div>
                          </div>

                          <hr>

                          <fieldset class="col-sm-12" ng-if="paymenttype == 'card' ">
                            <div class="col-sm-12 form-group">
                              <div class="col-sm-offset-3 col-sm-4">
                                <img src="/img/creditcards/amex.png" class="ec-CC-s"/>
                                <img src="/img/creditcards/visa.png" class="ec-CC-s"/>
                                <img src="/img/creditcards/discover.png" class="ec-CC-s"/>
                                <img src="/img/creditcards/mastercard.png" class="ec-CC-s"/>
                              </div>
                            </div>

                            <div class="col-sm-12 form-group">
                              <div class="col-sm-3 font-bold">Credit Card Number</div>
                              <div class="col-sm-4">
                                <input type="text" class="form-control" ng-model="payment.ccn" required/>
                              </div>
                            </div>

                            <div class="col-sm-12 form-group">
                              <div class="col-sm-3 font-bold">CCV Number <br> <em>(Card Code Verification)</em></div>
                              <div class="col-sm-4">
                                <input type="text" class="form-control" ng-model="payment.ccvnumber" required/>
                              </div>
                            </div>

                            <div class="col-sm-12 form-group">
                              <div class="col-sm-3 font-bold">Credit Card Expiration</div>
                              <div class="col-sm-2">
                                <label>Month</label>
                                <select class="form-control" ng-model="payment.expiremonth"
                                        ng-options="m.val as m.name for m in expimonth"required>
                                </select>
                              </div>
                              <div class="col-sm-2">
                                <label>Year</label>
                                <select class="form-control" ng-model="payment.expireyear"
                                    ng-options="y for y in expiyear"
                                    required>
                                </select>
                              </div>
                            </div>

                            <div class="col-sm-12 form-group">
                              <div class="col-sm-offset-3 col-sm-4">
                                <button type="submit" class="btn btn-info" ng-disabled="CCform.$invalid">Continue</button>
                              </div>
                            </div>
                          </fieldset>

                          <fieldset class="col-sm-12" ng-if="paymenttype == 'echeck' ">
                            <div class="col-sm-12 form-group">
                              <div class="col-sm-3">Account Holder Name</div>
                              <div class="col-sm-4">
                                <input type="text" class="form-control" ng-model="payment.ccn" required/>
                              </div>
                            </div>

                            <div class="col-sm-12 form-group">
                              <div class="col-sm-3">Your Bank Name</em></div>
                              <div class="col-sm-4">
                                <input type="text" class="form-control" ng-model="payment.ccvnumber" required/>
                              </div>
                            </div>

                            <div class="form-group row" style="padding-left:15px; margin-bottom:15px;">
                                <div class="col-md-12">
                                    Enter the routing code and account number as they appear on the bottom of your check:
                                </div>
                                <div class="col-md-12">
                                    <img src="/img/checksample.png">
                                </div>
                            </div>

                            <div class="form-group col-sm-12" style="padding-left:15px;">
                                <div class="col-md-3">
                                    Bank Routing Number: *
                                </div>
                                <div class="col-md-4">
                                    <span class="label label-danger" ng-if="error.bankrouting">This field is required</span>
                                    <input class="form-control " type="text" ng-model="userecheck.bankrouting" ng-change="change(userecheck.bankrouting, 'bankrouting')" name="bankrouting" ng-required="true" only-digits/>
                                </div>
                            </div>
                            <div class="form-group col-sm-12" style="padding-left:15px;">
                                <div class="col-md-3">
                                    Bank Account Number: *
                                </div>
                                <div class="col-md-4">
                                    <span class="label label-danger" ng-if="error.bankaccountnumber">This field is required</span>
                                    <input class="form-control " type="text" ng-model="userecheck.bankaccountnumber" ng-change="change(userecheck.bankaccountnumber, 'bankaccountnumber')" name="bankaccountnumber" ng-required="true" only-digits />
                                </div>
                            </div>
                            <div class="form-group col-sm-12" style="padding-left:15px;">
                                <div class="col-md-3">
                                    Select Account Type:
                                </div>
                                <div class="col-md-4" ng-init="userecheck.at = 'CHECKING'">
                                    <input type="radio" name="at" ng-model="userecheck.at" value="CHECKING" required>&nbsp;&nbsp;Checking account<br/>
                                    <input type="radio" name="at" ng-model="userecheck.at" value="BUSINESSCHECKING" required>&nbsp;&nbsp;Business checking account<br/>
                                    <input type="radio" name="at" ng-model="userecheck.at" value="SAVINGS" required>&nbsp;&nbsp;Savings account
                                </div>
                            </div>

                            <div class="form-group row" style="padding-left:15px;">
                                <div class="col-md-12">
                                    <input type="checkbox"  ng-model="userecheck.autorz" name="autorz" value="1" ng-required="true">*
                                      <span style="text-align: center;">
                                      By entering my account number about and Clicking authorize, I authorize my payment to be processed
                                      as an electronic funds transfer or draft drawn from my account. If the payment returned unpaid, I authorize you or your service
                                      provider to collect the payment at my state's return item fee by electronic funds transfer(s) or draft(s) drawn from my account.
                                      <a href="https://www.achex.com/html/NSF_pop.jsp" target="_blank">Click here to view your state's returned item fee.</a> If this payment is from a corporate account, I make these
                                      authorizations as an authorized corporate representative and agree that the entity will be bound by the NACHA operating rules.
                                      </span>
                                </div>
                            </div>

                            <div class="col-sm-12 form-group">
                              <div class="col-sm-offset-3 col-sm-4">
                                <button type="submit" class="btn btn-info" ng-disabled="CCform.$invalid">Continue</button>
                              </div>
                            </div>
                          </fieldset>

                        </form>

                        <fieldset class="col-sm-12" ng-if="paymenttype == 'paypal' ">
                          <form class="paypalform" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
                            <input type="hidden" name="business" value="jeevon.ang-facilitator@geeksnest.com">
                            <input type="hidden" name="cmd" value="_cart">
                            <input type="hidden" name="upload" value="1">
                            <input type="hidden" name="lc" value="US">
                            <input type="hidden" name="custom" value="shop|{[{invoiceno}]}">
                            <input type="hidden" name="invoice" ng-model="invoiceno">
                            <input type="hidden" name="return" value="{[{BaseURL}]}/shop/payment/success">
                            <input type="hidden" name="cancel_return" value="{[{BaseURL}]}/shop/cancel">
                            <div ng-repeat="product in cart">
                                <input type="hidden" name="item_name_{[{ $index + 1 }]}" value="{[{ product.name }]}">
                                <input type="hidden" name="amount_{[{ $index + 1 }]}" value="{[{ product.finalprice }]}">
                                <input type="hidden" name="quantity_{[{ $index + 1 }]}" value="{[{ product.cartquantity }]}">
                            </div>
                            <input type="hidden" name="currency_code" value="USD">

                          </form>

                          <input type="image" name="processpaypal" border="0" src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/buy-logo-large.png" alt="PayPal - The safer, easier way to pay online" ng-click="processpaypal()" style="padding: 0px;">
                        </fieldset>
                      </div>  
                    </div> 
                  </div>
                </div>

                <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingFive">
                    <h4 class="panel-title">
                      <a class="collapsed class4" role="button" data-toggle="collapse" href="" data-parent="#accordion" aria-expanded="true" aria-controls="collapseFive">
                        ORDER REVIEW
                      </a>
                    </h4>
                  </div>
                  <div id="orderReview" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive" ng-if="!loader">
                    <div class="panel-body">
                      <a ng-click="collapse(2)">Shipping Method</a> / <a ng-click="collapse(3)">Payment Information</a>
                      <hr>
                      <div class="table-responsive">
                        <table class="table table-striped b-t b-light ec-table-mycart">
                          <thead>
                            <tr>
                              <th style="min-width:120px"></th>
                              <th style="width:40%">ITEM</th>
                              <th style="width:15%">PRICE</th>
                              <th style="width:15%">QUANTITY</th>
                              <th style="width:15%">SUBTOTAL</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr ng-repeat="product in cart">
                              <td>
                                <div class="ec-prod-mycartthumb" style="background:url('{[{amazonimage}]}/{[{product.productid}]}/{[{product.filename}]}');">
                                </div>
                              </td>
                              <td class="font-bold ec-font-s pointer" ng-bind="product.name" ng-click="gotoproduct(product.slugs)"></td>
                              <td><span ng-bind="product.price|currency"></span></td>
                              <td ng-bind="product.cartquantity"></td>
                              <td><span ng-bind="product.total|currency"></span></td>
                            </tr>
                          </tbody>
                          <tfoot>
                            <tr style="border-top:1px solid #444; background:white">
                              <td colspan="4" style="text-align:right; font-weight:bold">
                                TOTAL <br><br>
                                Shipping & Handling (Flat Rate - Fixed) <br><br>
                                Tax <br><br>
                                <span class="ec-font-s font-bold">GRAND TOTAL</span>
                              </td>
                              <td>
                                <span ng-bind="subtotal | currency"></span> <br><br>
                                {[{ shippingfee | currency }]} <br><br>
                                {[{ tax | currency }]} <br><br>
                                <span class="ec-font-s font-bold">{[{ total | currency }]}</span></span>
                              </td>
                            </tr>
                          </tfoot>
                        </table>
                      </div> 

                      <div class="text-c">
                        <a href="<?php echo $shop; ?>/mycart" class="ec-font-s">Click here to edit your cart</a> <br><br>
                        Or <br><br>
                        <a class="btn btn-lg btn-info" ng-click="PLACEORDER()">PLACE YOUR ORDER</a>
                      </div>

                    </div>
                  </div>
                  <div class="loader" ng-show="loader" style="margin-bottom:30px;">
            				<div class="loadercontainer">

            					<div class="spinner">
            						<div class="rect1"></div>
            						<div class="rect2"></div>
            						<div class="rect3"></div>
            						<div class="rect4"></div>
            						<div class="rect5"></div>
            					</div>
            					Please wait...
            				</div>

            			</div>
                </div>

              </div> 

            </div> 
          </div>
        </div>
      </div>
    </div>

    </div>
  </div>
</div>
