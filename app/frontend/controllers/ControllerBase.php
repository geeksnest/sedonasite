<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class ControllerBase extends \Phalcon\Mvc\Controller
{
    public function onConstruct(){

        $this->view->banner = '';
        $this->view->thumb = '';
        $this->view->iconplay = '';
        $this->view->name = '';
        $this->view->facebookpixels = '';

        $this->angularLoader(array(
            'menucontroller' => 'fe/scripts/controllers/menu.js',
            'menufactory' => 'fe/scripts/factory/menu.js',
            'footerCtrl' => 'fe/scripts/controllers/footer.js',
            'Special' => 'fe/scripts/factory/specialpreview.js',
        ));

        /*maintenance*/
        $curl = curl_init($this->config->application->ApiURL.'/settings/load');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);

        if($curl_response === false){
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $maintenance = $decoded->maintenance;
        if($maintenance->status == 1){
            if($maintenance->enddate < strtotime(date('Y-m-d H:i:s'))){
                $curl = curl_init($this->config->application->ApiURL.'/settings/off/maintenance');
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $curl_response = curl_exec($curl);

                if($curl_response === false){
                    $info = curl_getinfo($curl);
                    curl_close($curl);
                    die('error occured during curl exec. Additional info: ' . var_export($info));
                }

                curl_close($curl);
            }else {
                $this->response->redirect($this->config->application->BaseURL.'/maintenance');
            }
        }

        $service_url_menu_healing = $this->config->application->ApiURL. '/menu/healing';

        $curl = curl_init($service_url_menu_healing);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $healing = $decoded;
        $this->view->healing = $decoded;



        $service_url_menu_readings = $this->config->application->ApiURL. '/menu/readings';

        $curl = curl_init($service_url_menu_readings);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->readings = $decoded;
        $readings = $decoded;


        $service_url_menu_acupuncture = $this->config->application->ApiURL. '/menu/acupuncture';

        $curl = curl_init($service_url_menu_acupuncture);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->acupuncture = $decoded;
        $acupuncture = $decoded;


        $service_url_menu_retreats = $this->config->application->ApiURL. '/menu/retreats';

        $curl = curl_init($service_url_menu_retreats);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->retreats = $decoded;
        $retreats = $decoded;


        $service_url_menu_workshops = $this->config->application->ApiURL. '/menu/workshops';

        $curl = curl_init($service_url_menu_workshops);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->workshops = $decoded;
        $workshops = $decoded;

        $this->view->imageLink = $this->config->application->amazonlink;


        $sidebar = "<div class='phar no-padding'>    <br/>
                                    <hr class='styled-hr'>
                                    <h3>ALL SERVICES</h3>  <br/>
                                    <span class='size16 font2 side-bar-color'>
                                    <a href='healing'>HEALING</a><br/>
                                    </span>
                                    <ul>";

        foreach($healing as $h){
            $sidebar .= "<li><a href='healing/".$h->pageslugs."'>".$h->title."</a></li>";
        }

        $sidebar .=                 "</ul>
                                    <span class='size16 font2 side-bar-color'>
                                        <a href='readings'>READINGS</a><br/>
                                    </span>
                                    <ul>";
        foreach($readings as $r){
            $sidebar .= "<li><a href='readings/".$r->pageslugs."'>".$r->title."</a></li>";
        }
        $sidebar .=                 "</ul>
                                    <span class='size16 font2 side-bar-color'>
                                        <a href='acupuncture'>ACUPUNCTURE</a><br/>
                                    </span>
                                    <ul>";
        foreach($acupuncture as $a){
            $sidebar .= "<li><a href='acupuncture/".$a->pageslugs."'>".$a->title."</a></li>";
        }
        $sidebar .=                 "</ul>
                                    <span class='size16 font2 side-bar-color'>
                                        <a href='retreats'>RETREATS</a><br/>
                                    </span>
                                    <ul>";
        foreach($retreats as $rt){
            $sidebar .= "<li><a href='retreats/".$rt->pageslugs."'>".$rt->title."</a></li>";
        }
        $sidebar .=                 "</ul>
                                    <span class='size16 font2 side-bar-color'>
                                        <a href='workshops'>WORKSHOPS</a><br/>
                                    </span>
                                    <ul>";
        foreach($workshops as $w){
            $sidebar .= "<li><a href='workshops/".$w->pageslugs."'>".$w->title."</a></li>";
        }
        $sidebar .=                 "</ul>
                                    <hr class='styled-hr'>
                                </div>";

        $this->view->sidebar = $sidebar;


        $this->view->metatitle = 'Sedona Healing Arts';
        $this->view->metadesc = '';
        $this->view->metakeyword = '';
        $this->view->author = '';


         // GOOGLE ANALYTICS
        $service_url_news = $this->config->application->ApiURL. '/settings/load';

        $curl = curl_init($service_url_news);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);

        $this->view->gscript = $decoded->gscript->script;

        $service_url_news = $this->config->application->ApiURL. '/pages/managepage/100/0/null';

        $curl = curl_init($service_url_news);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);

        $this->view->metathumbnails = $decoded;

    }

    protected function initialize()
    {
        $this->view->base_url = $this->config->application->BaseURL;
        $this->view->api_url = $this->config->application->ApiURL;
        $this->view->amazonlink = $this->config->application->amazonlink;
        $this->view->aura_slugs = $this->curl("/menu/aura");
        $this->view->shop = "/shop";


        $this->view->aura_slugs = $this->curl("/menu/aura");

        $this->view->footerdata = $this->curl("/special/footer/frontend");
        $this->view->datarowf = $this->view->footerdata->datarow;
        $this->view->rowcolor = $this->view->footerdata->rowcolor;
        $this->view->rowfontcolor = $this->view->footerdata->rowfontcolor;

        $col1txt = $this->view->footerdata->col1;
        $col2txt = $this->view->footerdata->col2;
        $col3txt = $this->view->footerdata->col3;
        $col4txt = $this->view->footerdata->col4;

        $coltxt1 = array();
        $coltxt1 = $col1txt;
        $this->view->col1f = $col1txt;

        $coltxt2 = array();
        $coltxt2 = $col2txt;
        $this->view->col2f = $col2txt;

        $coltxt3 = array();
        $coltxt3 = $col3txt;
        $this->view->col3f = $col3txt;

        $coltxt4 = array();
        $coltxt4 = $col4txt;
        $this->view->col4f = $col4txt;

        foreach ($coltxt1 as $key => $value) {
            if($value->module == 'TEXT'){
                $coltxt1[$key]->ccontent = $this->shortcode($value->content);
            }
        }

        foreach ($coltxt2 as $key => $value) {
            if($value->module == 'TEXT'){
                $coltxt2[$key]->ccontent = $this->shortcode($value->content);
            }
        }

        foreach ($coltxt3 as $key => $value) {
            if($value->module == 'TEXT'){
                $coltxt3[$key]->ccontent = $this->shortcode($value->content);
            }
        }

        foreach ($coltxt4 as $key => $value) {
            if($value->module == 'TEXT'){
                $coltxt4[$key]->ccontent = $this->shortcode($value->content);
            }
        }
    }
    function shortcode($contentcol){
                preg_match_all('~\[(.+?)\]~',$contentcol, $matches);
                $newstring = $contentcol;
                $matchesarray = array();
                $matchesarray = $matches[1];
                foreach ($matchesarray as $key => $value) {
                    parse_str(html_entity_decode($value));            
                    if($module == 'testimonial'){
                                    if(!isset($column)){$col = 'undefined';}
                                    elseif(strpos($value,'category') === false){$col = 'undefined';}
                                    else{$col = $column;}

                                    if(!isset($keyword)){$keywordval = 'undefined';}
                                    else{$keywordval = $keyword;}

                                    if(!isset($limit)){$limitcount = 'undefined';}
                                    elseif(strpos($value,'limit') === false){$limitcount = 'undefined';}
                                    else{$limitcount = $limit;}

                                    //var_dump($col .' '. $keywordval .' '.$limitcount);

                                    $testimonialsdata = $this->curl('/shortcode/testimonials/' . $col . '/' . $keywordval . '/' .  $limitcount);
                                    $gettesti = $testimonialsdata;
                                    $loopdata = '';
                                    foreach($gettesti as $k => $value) { 

                                        $loopdata.='<div class="size16 border-left italic margin-top">
                                        <div class="row">
                                            <div class="col-sm-3 text-center">
                                                <img src="'.$this->config->application->amazonlink.'/uploads/testimonialpic/'.$gettesti[$k]->picture.'" class="testi-img" width="100%">
                                            </div>
                                            <div class="col-sm-9">
                                                <div></div>
                                                <div>
                                                    <span class="fa fa-quote-left"></span>
                                                    '.$gettesti[$k]->message.'
                                                </div>
                                                <div class="text-right">- '.$gettesti[$k]->name.'</div>
                                            </div>
                                        </div>
                                        <hr class="styled-hr">
                                    </div>';}
                                    $testimonial = '<div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                                    <div class="specialpage_col">'.$loopdata.'</div>'; 

                                    if($col != 'undefined' && $keywordval != 'undefined' && $limitcount == 'undefined'){   
                                        $newstring = str_replace("[module=".$module."&amp;column=".$column."&amp;keyword=".$keyword."]", $testimonial, $newstring);
                                    }
                                    elseif($col != 'undefined' && $keywordval != 'undefined' && $limitcount != 'undefined'){
                                        $newstring = str_replace("[module=".$module."&amp;column=".$column."&amp;keyword=".$keyword."&amp;limit=".$limit."]", $testimonial, $newstring);
                                    }
                                    elseif($col == 'undefined' && $limitcount != 'undefined'){
                                        $newstring = str_replace("[module=".$module."&amp;limit=".$limit."]", $testimonial, $newstring);
                                    }
                                    elseif($col == 'undefined' && $limitcount == 'undefined'){
                                        $newstring = str_replace("[module=".$module."]", $testimonial, $newstring);
                                    }

                    }

                    elseif($module == 'news'){
                                    if(!isset($column)){$col = 'undefined';}
                                    elseif(strpos($value,'category') === false && strpos($value,'title') === false){$col = 'undefined';}
                                    else{$col = $column;}

                                    if(!isset($keyword)){$keywordval = 'undefined';}
                                    else{$keywordval = $keyword;}

                                    if(!isset($limit)){$limitcount = 'undefined';}
                                    elseif(strpos($value,'limit') === false){$limitcount = 'undefined';}
                                    else{$limitcount = $limit;}

                                    $newsdata = $this->curl('/shortcode/news/' . $col . '/' . $keywordval . '/' .  $limitcount);
                                    $tempnews ='';
                                    foreach ($newsdata as $news) {
                                       $catecate ='';
                                       if ($news->videothumb) {
                                           $x = preg_match('/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/i',$news->videothumb,$match);
                                           $img = 'http://img.youtube.com/vi/'. $match[1] .'/hqdefault.jpg';
                                           $url = $match[1]; 
                                           $ifutube = '<div class="youtube-play"><a href="/blog/'.$news->newsslugs.'"><img src="img/youtubeplay.png"/></a></div>';
                                       }
                                       else
                                       {
                                        $ifutube ='';
                                        $img = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                    }

                                    foreach ($news->category as $k => $categorylist) {
                                        if (count($news->category)-1==$k) {
                                            $catecate.= $categorylist->categoryname;
                                        }
                                        else
                                        {
                                         $catecate.=$categorylist->categoryname.", ";
                                     }  
                                    }

                                         $tempnews .= '<div class="row list-title-blog specialpage-title-blog">
                                         <div class="col-sm-3 news-thumb-container module2-news-thumb" style="background-image: url(\''.$img.'\')" ng-click="redirectNews(\''.$news->newsslugs.'\');">
                                            <img src="'.$img.'" class="pinterest-img" alt="'.$news->title.'">
                                            '.$ifutube.'
                                            <a href="/blog/'.$news->newsslugs.'">
                                            </a>
                                        </div>
                                        <div class="col-xs-8 col-md-7 news-list-desc module2-news-desc">
                                            <div class="row">
                                                <div class="">
                                                  <span class="size25 font1 news-title" ng-click="redirectNews(\''.$news->newsslugs.'\');">'.$news->title.'</span>
                                              </div>
                                              <div class="">
                                                  <strong><span class="thin-font1 orange">
                                                    '.$catecate.'
                                                </span></strong><span class="thin-font1"> / by <a href="/blog/author/'.$news->name.'"><strong><span class="orange">'.$news->name.'</span></strong></a></span> / '.$news->date.'
                                                <br/><br/>
                                            </div>
                                            <div class="col-sm-12">
                                              <div class="font1 size14 summary">
                                                '.$news->summary.'
                                                <br/><br/>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="clear:both"></div>
                                    <br>
                                    </div>
                                    </div>';


                                    }

                                    $newscontent = '<div class="col-sm-12 specialpage_col">
                                    <div ng-controller="NewsCtrl" ng-init="showmorenews()">
                                        <h4 class="blog-title-list">News</h4>
                                        <div class="list-news-wrapper special-news-wrapper">
                                            '.$tempnews.'
                                        </div>
                                    </div>
                                    </div>';

                                    if($col != 'undefined' && $keywordval != 'undefined' && $limitcount == 'undefined'){   
                                        $newstring = str_replace("[module=".$module."&amp;column=".$column."&amp;keyword=".$keyword."]", $newscontent, $newstring);
                                    }
                                    elseif($col != 'undefined' && $keywordval != 'undefined' && $limitcount != 'undefined'){
                                        $newstring = str_replace("[module=".$module."&amp;column=".$column."&amp;keyword=".$keyword."&amp;limit=".$limit."]", $newscontent, $newstring);
                                    }
                                    elseif($col == 'undefined' && $limitcount != 'undefined'){
                                        $newstring = str_replace("[module=".$module."&amp;limit=".$limit."]", $newscontent, $newstring);
                                    }
                                    elseif($col == 'undefined' && $limitcount == 'undefined'){
                                        $newstring = str_replace("[module=".$module."]", $newscontent, $newstring);
                                    }
                    }

                    elseif($module == 'contact')
                    {
                        if(isset($keyword) && $keyword == 'full')
                        {
                                $contact = '<div class="container" ng-controller="ContactusCtrl" id="">
                        <br/><br/><br/>
                            <div class="col-sm-12"><h1>Contact Us</h1> </div>
                            <div class="col-sm-6">
                              <div class="form">
                                <!-- Contact form -->
                                <form name="formContactUs" class="form-validation booking ng-pristine ng-invalid ng-invalid-required" ng-submit="submit(feedback)">
                                  <!-- Name -->
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12 hidden feedback-success" for="name" style="color:#6cb04b;">Thank you for contacting Sedona Healing Arts, your message was sent successfully.
                                        <br>We will review your inquiry and get back to you shortly.</label>
                                    </div>
                                </div>
                                <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="name">Subject
                                        <span class="red">*</span>
                                    </label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control textbox margin-bot8" id="" required="required" ng-model="feedback.subject">
                                    </div>
                                </div>
                            </div>
                            <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="name">Name
                                    <span class="red">*</span>
                                </label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control textbox margin-bot8" id="fname" placeholder="First Name" required="required" ng-model="feedback.fname">
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control textbox" id="lname" placeholder="Last Name" required="required" ng-model="feedback.lname">
                                </div>
                            </div>
                        </div>
                        <!-- Email -->
                        <div class="control-group margin-bot8">
                            <div class="row">
                              <label class="control-label size14 col-sm-12" for="email">Email
                                <span class="red">*</span>
                            </label>
                            <div class="col-sm-12">
                                <span class="red hidden booking-email">Invalid Email Address</span>
                                <input type="email" class="textbox form-control margin-bot8" id="email" required="required" ng-model="feedback.email" ng-change="onemail(feedback.email)">
                            </div>
                        </div>
                        </div>
                        <!-- Comment -->
                        <div class="control-group margin-bot8">
                            <div class="row">
                              <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                <span class="red">*</span>
                            </label>
                            <div class="col-sm-12">
                                <textarea class="form-control input-large textarea" id="feedback" rows="5" required="required" ng-model="feedback.message"></textarea>
                            </div>
                        </div>
                        </div>
                        <!-- Google recaptcha -->
                        <div class="control-group margin-bot8">
                            <div class="row">
                              <div class="col-sm-12">
                                <div vc-recaptcha key="\'6LfiYQsTAAAAAMZIe1BR0fLlSfJAdLxFMr4nzOlm\'" on-create="setWidgetId(widgetId)" on-success="setResponse(response)" on-expire="cbExpiration()"></div>
                            </div>
                        </div>
                        </div>
                        <!-- Buttons -->
                        <div class="form-actions margin-bot8">
                            <div class="row">
                              <div class="col-sm-12">
                                <button type="submit" class="btn btn-sedona" ng-disabled="formContactUs.$invalid||res==false">Submit</button>
                            </div>
                        </div>
                        </div>
                        <!-- Buttons -->
                        </form>
                        </div>
                        <div class="clearfix"></div>
                        </div>

                        <div class="col-sm-6 storeinfo">
                          <div class="storeinfo-details">
                            <h2 class="orange">Store Information</h2>
                            <div class="title3">
                              <span class="titleb">Phone:</span>
                              <span class="thin-font3">(928) 282-3875</span>
                              <br/>
                              <span class="titleb">Email:</span>
                              <span class="thin-font3">contact@sedonahealingarts.com</span>
                              <br/>
                              <span class="titleb">Hours:</span>
                              <span class="thin-font3">Mon - Sun: 10 am - 7 pm</span>
                              <br/>
                              <br/>
                          </div>
                          <div class="title3">
                              <span class="titleb text-top">Address:</span>
                              <br/>
                              <span class="thin-font3">201 State Route 179</span>
                              <br/>
                              <span class="thin-font3">Sedona, AZ 86336</span>
                          </div>

                          <div class="wsite-map">
                              <iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 250px; margin-top: 10px; margin-bottom: 10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3273.673285579057!2d-111.7637428!3d34.8644358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872da1564d73fd15%3A0x3e31d123930491bf!2s201+Hwy+179%2C+Sedona%2C+AZ+86336%2C+USA!5e0!3m2!1sen!2sph!4v1434348998372"></iframe>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        </div></div>';
                        $newstring = str_replace("[module=".$module."&amp;keyword=full]", $contact, $newstring);
                        }
                        else{
                        $contact = '<div class="container" ng-controller="ContactusCtrl" id="">
                        <br/><br/><br/>
                            <div class="col-sm-12"><h1>Contact Us</h1> </div>
                            <div class="col-sm-6">
                              <div class="form">
                                <!-- Contact form -->
                                <form name="formContactUs" class="form-validation booking ng-pristine ng-invalid ng-invalid-required" ng-submit="submit(feedback)">
                                  <!-- Name -->
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12 hidden feedback-success" for="name" style="color:#6cb04b;">Thank you for contacting Sedona Healing Arts, your message was sent successfully.
                                        <br>We will review your inquiry and get back to you shortly.</label>
                                    </div>
                                </div>
                                <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="name">Subject
                                        <span class="red">*</span>
                                    </label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control textbox margin-bot8" id="" required="required" ng-model="feedback.subject">
                                    </div>
                                </div>
                            </div>
                            <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="name">Name
                                    <span class="red">*</span>
                                </label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control textbox margin-bot8" id="fname" placeholder="First Name" required="required" ng-model="feedback.fname">
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control textbox" id="lname" placeholder="Last Name" required="required" ng-model="feedback.lname">
                                </div>
                            </div>
                        </div>
                        <!-- Email -->
                        <div class="control-group margin-bot8">
                            <div class="row">
                              <label class="control-label size14 col-sm-12" for="email">Email
                                <span class="red">*</span>
                            </label>
                            <div class="col-sm-12">
                                <span class="red hidden booking-email">Invalid Email Address</span>
                                <input type="email" class="textbox form-control margin-bot8" id="email" required="required" ng-model="feedback.email" ng-change="onemail(feedback.email)">
                            </div>
                        </div>
                        </div>
                        <!-- Comment -->
                        <div class="control-group margin-bot8">
                            <div class="row">
                              <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                <span class="red">*</span>
                            </label>
                            <div class="col-sm-12">
                                <textarea class="form-control input-large textarea" id="feedback" rows="5" required="required" ng-model="feedback.message"></textarea>
                            </div>
                        </div>
                        </div>
                        <!-- Google recaptcha -->
                        <div class="control-group margin-bot8">
                            <div class="row">
                              <div class="col-sm-12">
                                <div vc-recaptcha key="\'6LfiYQsTAAAAAMZIe1BR0fLlSfJAdLxFMr4nzOlm\'" on-create="setWidgetId(widgetId)" on-success="setResponse(response)" on-expire="cbExpiration()"></div>
                            </div>
                        </div>
                        </div>
                        <!-- Buttons -->
                        <div class="form-actions margin-bot8">
                            <div class="row">
                              <div class="col-sm-12">
                                <button type="submit" class="btn btn-sedona" ng-disabled="formContactUs.$invalid||res==false">Submit</button>
                            </div>
                        </div>
                        </div>
                        <!-- Buttons -->
                        </form>
                        </div>
                        <div class="clearfix"></div>
                        </div>
                        </div>';
                        $newstring = str_replace("[module=".$module."]", $contact, $newstring);
                        }
                        
                    }
                } 
                return $newstring;  
        } 

    public function angularLoader($ang){
        $modules = array();
        $scripts = '';
        foreach($ang as $key => $val){
            $scripts .= $this->tag->javascriptInclude($val);
            $modules[] = $key;
        }
        $this->view->modules = (!empty($modules) ? $modules : array());
        $this->view->otherjvascript = $scripts;
    }
    public function httpPost($url,$params)
    {
        $postData = '';
        //create name value pairs seperated by &
        foreach($params as $k => $v)
        {
            $postData .= $k . '='.$v.'&';
        }
        rtrim($postData, '&');

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

        $output=curl_exec($ch);

        curl_close($ch);
        return $output;
    }

    public function curl($url){
        $service_url = $this->config->application->ApiURL.'/'.$url;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        curl_close($curl);
        return $decoded = json_decode($curl_response);
    }



}
