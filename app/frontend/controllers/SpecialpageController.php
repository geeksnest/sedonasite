<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;


class SpecialpageController extends ControllerBase
{
        public function previewAction()
    {
      $this->angularLoader(array(
            'SpecialpreviewCtrl' => '/fe/scripts/controllers/specialpreview.js',
            'Special' => '/fe/scripts/factory/specialpreview.js'
        ));

    }


    public function specialpageAction($pageslugs)
    {
        $this->view->alldata = $this->curl('/utility/specialpage/' . $pageslugs);
        $this->view->pageslugs = $pageslugs;

        $this->angularLoader(array(
            'SpecialCtrl' => '/fe/scripts/controllers/special.js',
            'Special' => '/fe/scripts/factory/specialpreview.js'
        ));

        $this->view->facebookpixels= "    <script>(function() {
            var _fbq = window._fbq || (window._fbq = []);
            if (!_fbq.loaded) {
            var fbds = document.createElement('script');
            fbds.async = true;
            fbds.src = '//connect.facebook.net/en_US/fbds.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(fbds, s);
            _fbq.loaded = true;
            }
            _fbq.push(['addPixelId', '1617141355217381']);
            })();
            window._fbq = window._fbq || [];
            window._fbq.push(['track', 'PixelInitialized', {}]);
            </script>";
        if($this->view->alldata->data->status == 0){
            
             $this->view->catdata = $this->curl('/utility/catcontent/' . $pageslugs);
             if ($this->view->catdata->data) {
                $this->view->slugs = $this->view->catdata->data->slugs;
                $this->view->slides = $this->view->catdata->slides;
                $this->view->panel=$this->view->catdata->panel;
             }
             elseif ($this->view->catdata->datapages) {
               $this->view->datapages = $this->view->catdata->datapages;
               $this->view->pageid = $this->view->catdata->datapages->pageid;
               $this->view->title = $this->view->catdata->datapages->title;
               $this->view->subtitle1 = $this->view->catdata->datapages->subtitle1;
               $this->view->subtitle2 = $this->view->catdata->datapages->imagethumbsubtitle;
               $this->view->buttontitle = $this->view->catdata->datapages->buttontitle;
               $this->view->pageslugs = $this->view->catdata->datapages->pageslugs;
               $this->view->body = $this->view->catdata->datapages->body;
               $this->view->peoplesaying = $this->view->catdata->datapages->peoplesaying;
               $this->view->serviceprice = $this->view->catdata->datapages->serviceprice;
               $this->view->banner = $this->view->catdata->datapages->banner;
               $this->view->pagelayout = $this->view->catdata->datapages->pagelayout;
               $this->view->imgthumb =  $this->config->application->amazonlink."/uploads/pageimage/".$this->view->catdata->datapages->banner;
               $this->view->metatitle = $this->view->catdata->datapages->metatitle;
               $this->view->metadesc = $this->view->catdata->datapages->metadesc;
               $this->view->metakeyword = $this->view->catdata->datapages->metatags;
                // var_dump($this->view->pageid);
             }
             else{
                 $this->response->redirect($this->config->application->BaseURL.'/page404');
             }
        }

        // if($this->view->alldata->data->banner == 'true'){
        //     $this->view->banner = true;
        //     $this->view->imagethumb = $this->view->alldata->data->imagethumb;
        //     $this->view->imagethumbsubtitle = $this->view->alldata->data->imagethumbsubtitle;
        //     $this->view->thumbdesc = $this->view->alldata->data->thumbdesc;
        //     $this->view->align = $this->view->alldata->data->align;
        //     $this->view->bgcolor = $this->view->alldata->data->bgcolor;
        //     $this->view->color = $this->view->alldata->data->color;
        //     $this->view->box = $this->view->alldata->data->box;
        //     $this->view->btnname = $this->view->alldata->data->btnname;
        //     $this->view->btnlink = $this->view->alldata->data->btnlink;
        // }else{
        //     $this->view->banner = false;
        // }

        // switch($this->view->alldata->data->pagetype){
        //     case "Normal":
        //         $this->view->content = $this->shortcode($this->view->data->body);
        //         break;
        //     case "Special":
        //         $this->view->datarow = $this->view->alldata->datarow; 
        
        //         $col1txt = $this->view->alldata->col1;
        //         $col2txt = $this->view->alldata->col2;
        //         $col3txt = $this->view->alldata->col3;
        //         $col4txt = $this->view->alldata->col4;
         
        //         $coltxt1 = array();
        //         $coltxt1 = $col1txt;
        //         $this->view->col1 = $col1txt;

        //         $coltxt2 = array();
        //         $coltxt2 = $col2txt;
        //         $this->view->col2 = $col2txt;

        //         $coltxt3 = array();
        //         $coltxt3 = $col3txt;
        //         $this->view->col3 = $col3txt;

        //         $coltxt4 = array();
        //         $coltxt4 = $col4txt;
        //         $this->view->col4 = $col4txt;

        //         foreach ($coltxt1 as $key => $value) {
        //             if($value->module == 'TEXT'){
        //                 $coltxt1[$key]->content = $this->shortcode($value->ccontent);
        //             }
        //         }

        //         foreach ($coltxt2 as $key => $value) {
        //             if($value->module == 'TEXT'){
        //                 $coltxt2[$key]->content = $this->shortcode($value->ccontent);
        //             }
        //         }

        //         foreach ($coltxt3 as $key => $value) {
        //             if($value->module == 'TEXT'){
        //                 $coltxt3[$key]->content = $this->shortcode($value->ccontent);
        //             }
        //         }

        //         foreach ($coltxt4 as $key => $value) {
        //             if($value->module == 'TEXT'){
        //                 $coltxt4[$key]->content = $this->shortcode($value->ccontent);
        //             }
        //         }
        //         break;
        // }
        
    }
  
    function shortcode($contentcol){
                preg_match_all('~\[(.+?)\]~',$contentcol, $matches);
                $newstring = $contentcol;
                $matchesarray = array();
                $matchesarray = $matches[1];
                foreach ($matchesarray as $key => $value) {
                    parse_str(html_entity_decode($value));            
                    if($module == 'testimonial'){
                                    if(!isset($column)){$col = 'undefined';}
                                    elseif(strpos($value,'category') === false){$col = 'undefined';}
                                    else{$col = $column;}

                                    if(!isset($keyword)){$keywordval = 'undefined';}
                                    else{$keywordval = $keyword;}

                                    if(!isset($limit)){$limitcount = 'undefined';}
                                    elseif(strpos($value,'limit') === false){$limitcount = 'undefined';}
                                    else{$limitcount = $limit;}

                                    $testimonialsdata = $this->curl('/shortcode/testimonials/' . $col . '/' . $keywordval . '/' .  $limitcount);
                                    $gettesti = $testimonialsdata;
                                    $loopdata = '';
                                    foreach($gettesti as $k => $value) { 

                                        $loopdata.='<div class="size16 border-left italic margin-top">
                                        <div class="row">
                                            <div class="col-sm-3 text-center">
                                                <img src="'.$this->config->application->amazonlink.'/uploads/testimonialpic/'.$gettesti[$k]->picture.'" class="testi-img" width="100%">
                                            </div>
                                            <div class="col-sm-9">
                                                <div></div>
                                                <div>
                                                    <span class="fa fa-quote-left"></span>
                                                    '.$gettesti[$k]->message.'
                                                </div>
                                                <div class="text-right">- '.$gettesti[$k]->name.'</div>
                                            </div>
                                        </div>
                                        <hr class="styled-hr">
                                    </div>';}
                                    $testimonial = '<div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                                    <div class="specialpage_col">'.$loopdata.'</div>'; 

                                    if($col != 'undefined' && $keywordval != 'undefined' && $limitcount == 'undefined'){   
                                        $newstring = str_replace("[module=".$module."&amp;column=".$column."&amp;keyword=".$keyword."]", $testimonial, $newstring);
                                    }
                                    elseif($col != 'undefined' && $keywordval != 'undefined' && $limitcount != 'undefined'){
                                        $newstring = str_replace("[module=".$module."&amp;column=".$column."&amp;keyword=".$keyword."&amp;limit=".$limit."]", $testimonial, $newstring);
                                    }
                                    elseif($col == 'undefined' && $limitcount != 'undefined'){
                                        $newstring = str_replace("[module=".$module."&amp;limit=".$limit."]", $testimonial, $newstring);
                                    }
                                    elseif($col == 'undefined' && $limitcount == 'undefined'){
                                        $newstring = str_replace("[module=".$module."]", $testimonial, $newstring);
                                    }

                    }

                    elseif($module == 'news'){
                                    if(!isset($column)){$col = 'undefined';}
                                    elseif(strpos($value,'category') === false && strpos($value,'title') === false){$col = 'undefined';}
                                    else{$col = $column;}

                                    if(!isset($keyword)){$keywordval = 'undefined';}
                                    else{$keywordval = $keyword;}

                                    if(!isset($limit)){$limitcount = 'undefined';}
                                    elseif(strpos($value,'limit') === false){$limitcount = 'undefined';}
                                    else{$limitcount = $limit;}

                                    $newsdata = $this->curl('/shortcode/news/' . $col . '/' . $keywordval . '/' .  $limitcount);
                                    $tempnews ='';
                                    foreach ($newsdata as $news) {
                                       $catecate ='';
                                       if ($news->videothumb) {
                                           $x = preg_match('/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/i',$news->videothumb,$match);
                                           $img = 'http://img.youtube.com/vi/'. $match[1] .'/hqdefault.jpg';
                                           $url = $match[1]; 
                                           $ifutube = '<div class="youtube-play"><a href="/blog/'.$news->newsslugs.'"><img src="img/youtubeplay.png"/></a></div>';
                                       }
                                       else
                                       {
                                        $ifutube ='';
                                        $img = $this->config->application->amazonlink."/uploads/newsimage/".$news->imagethumb;
                                    }

                                    foreach ($news->category as $k => $categorylist) {
                                        if (count($news->category)-1==$k) {
                                            $catecate.= $categorylist->categoryname;
                                        }
                                        else
                                        {
                                         $catecate.=$categorylist->categoryname.", ";
                                     }  
                                    }

                                         $tempnews .= '<div class="row list-title-blog specialpage-title-blog">
                                         <div class="col-sm-3 news-thumb-container module2-news-thumb" style="background-image: url(\''.$img.'\')" ng-click="redirectNews(\''.$news->newsslugs.'\');">
                                            <img src="'.$img.'" class="pinterest-img" alt="'.$news->title.'">
                                            '.$ifutube.'
                                            <a href="/blog/'.$news->newsslugs.'">
                                            </a>
                                        </div>
                                        <div class="col-xs-8 col-md-7 news-list-desc module2-news-desc">
                                            <div class="row">
                                                <div class="">
                                                  <span class="size25 font1 news-title" ng-click="redirectNews(\''.$news->newsslugs.'\');">'.$news->title.'</span>
                                              </div>
                                              <div class="">
                                                  <strong><span class="thin-font1 orange">
                                                    '.$catecate.'
                                                </span></strong><span class="thin-font1"> / by <a href="/blog/author/'.$news->name.'"><strong><span class="orange">'.$news->name.'</span></strong></a></span> / '.$news->date.'
                                                <br/><br/>
                                            </div>
                                            <div class="col-sm-12">
                                              <div class="font1 size14 summary">
                                                '.$news->summary.'
                                                <br/><br/>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="clear:both"></div>
                                    <br>
                                    </div>
                                    </div>';


                                    }

                                    $newscontent = '<div class="col-sm-12 specialpage_col">
                                    <div ng-controller="NewsCtrl" ng-init="showmorenews()">
                                        <h4 class="blog-title-list">News</h4>
                                        <div class="list-news-wrapper special-news-wrapper">
                                            '.$tempnews.'
                                        </div>
                                    </div>
                                    </div>';

                                    if($col != 'undefined' && $keywordval != 'undefined' && $limitcount == 'undefined'){   
                                        $newstring = str_replace("[module=".$module."&amp;column=".$column."&amp;keyword=".$keyword."]", $newscontent, $newstring);
                                    }
                                    elseif($col != 'undefined' && $keywordval != 'undefined' && $limitcount != 'undefined'){
                                        $newstring = str_replace("[module=".$module."&amp;column=".$column."&amp;keyword=".$keyword."&amp;limit=".$limit."]", $newscontent, $newstring);
                                    }
                                    elseif($col == 'undefined' && $limitcount != 'undefined'){
                                        $newstring = str_replace("[module=".$module."&amp;limit=".$limit."]", $newscontent, $newstring);
                                    }
                                    elseif($col == 'undefined' && $limitcount == 'undefined'){
                                        $newstring = str_replace("[module=".$module."]", $newscontent, $newstring);
                                    }
                    }

                    elseif($module == 'contact')
                    {
                        if(isset($keyword) && $keyword == 'full')
                        {
                                $contact = '<div class="" ng-controller="ContactusCtrl" id="">
                        <br/><br/><br/>
                            <div class="col-sm-12"><h1>Contact Us</h1> </div>
                            <div class="col-sm-6">
                              <div class="form">
                                <!-- Contact form -->
                                <form name="formContactUs" class="form-validation booking ng-pristine ng-invalid ng-invalid-required" ng-submit="submit(feedback)">
                                  <!-- Name -->
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12 hidden feedback-success" for="name" style="color:#6cb04b;">Thank you for contacting Sedona Healing Arts, your message was sent successfully.
                                        <br>We will review your inquiry and get back to you shortly.</label>
                                    </div>
                                </div>
                                <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="name">Subject
                                        <span class="red">*</span>
                                    </label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control textbox margin-bot8" id="" required="required" ng-model="feedback.subject">
                                    </div>
                                </div>
                            </div>
                            <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="name">Name
                                    <span class="red">*</span>
                                </label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control textbox margin-bot8" id="fname" placeholder="First Name" required="required" ng-model="feedback.fname">
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control textbox" id="lname" placeholder="Last Name" required="required" ng-model="feedback.lname">
                                </div>
                            </div>
                        </div>
                        <!-- Email -->
                        <div class="control-group margin-bot8">
                            <div class="row">
                              <label class="control-label size14 col-sm-12" for="email">Email
                                <span class="red">*</span>
                            </label>
                            <div class="col-sm-12">
                                <span class="red hidden booking-email">Invalid Email Address</span>
                                <input type="email" class="textbox form-control margin-bot8" id="email" required="required" ng-model="feedback.email" ng-change="onemail(feedback.email)">
                            </div>
                        </div>
                        </div>
                        <!-- Comment -->
                        <div class="control-group margin-bot8">
                            <div class="row">
                              <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                <span class="red">*</span>
                            </label>
                            <div class="col-sm-12">
                                <textarea class="form-control input-large textarea" id="feedback" rows="5" required="required" ng-model="feedback.message"></textarea>
                            </div>
                        </div>
                        </div>
                        <!-- Google recaptcha -->
                        <div class="control-group margin-bot8">
                            <div class="row">
                              <div class="col-sm-12">
                                <div vc-recaptcha key="\'6LfiYQsTAAAAAMZIe1BR0fLlSfJAdLxFMr4nzOlm\'" on-create="setWidgetId(widgetId)" on-success="setResponse(response)" on-expire="cbExpiration()"></div>
                            </div>
                        </div>
                        </div>
                        <!-- Buttons -->
                        <div class="form-actions margin-bot8">
                            <div class="row">
                              <div class="col-sm-12">
                                <button type="submit" class="btn btn-sedona" ng-disabled="formContactUs.$invalid||res==false">Submit</button>
                            </div>
                        </div>
                        </div>
                        <!-- Buttons -->
                        </form>
                        </div>
                        <div class="clearfix"></div>
                        </div>

                        <div class="col-sm-6 storeinfo">
                          <div class="storeinfo-details">
                            <h2 class="orange">Store Information</h2>
                            <div class="title3">
                              <span class="titleb">Phone:</span>
                              <span class="thin-font3">(928) 282-3875</span>
                              <br/>
                              <span class="titleb">Email:</span>
                              <span class="thin-font3">contact@sedonahealingarts.com</span>
                              <br/>
                              <span class="titleb">Hours:</span>
                              <span class="thin-font3">Mon - Sun: 10 am - 7 pm</span>
                              <br/>
                              <br/>
                          </div>
                          <div class="title3">
                              <span class="titleb text-top">Address:</span>
                              <br/>
                              <span class="thin-font3">201 State Route 179</span>
                              <br/>
                              <span class="thin-font3">Sedona, AZ 86336</span>
                          </div>

                          <div class="wsite-map">
                              <iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 250px; margin-top: 10px; margin-bottom: 10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3273.673285579057!2d-111.7637428!3d34.8644358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872da1564d73fd15%3A0x3e31d123930491bf!2s201+Hwy+179%2C+Sedona%2C+AZ+86336%2C+USA!5e0!3m2!1sen!2sph!4v1434348998372"></iframe>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        </div></div>';
                        $newstring = str_replace("[module=".$module."&amp;keyword=full]", $contact, $newstring);
                        }
                        else{
                        $contact = '<div class="" ng-controller="ContactusCtrl" id="">
                        <br/><br/><br/>
                            <div class="col-sm-12"><h1>Contact Us</h1> </div>
                            <div class="col-sm-12">
                              <div class="form">
                                <!-- Contact form -->
                                <form name="formContactUs" class="form-validation booking ng-pristine ng-invalid ng-invalid-required" ng-submit="submit(feedback)">
                                  <!-- Name -->
                                  <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12 hidden feedback-success" for="name" style="color:#6cb04b;">Thank you for contacting Sedona Healing Arts, your message was sent successfully.
                                        <br>We will review your inquiry and get back to you shortly.</label>
                                    </div>
                                </div>
                                <div class="control-group margin-bot8">
                                    <div class="row">
                                      <label class="control-label size14 col-sm-12" for="name">Subject
                                        <span class="red">*</span>
                                    </label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control textbox margin-bot8" id="" required="required" ng-model="feedback.subject">
                                    </div>
                                </div>
                            </div>
                            <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="name">Name
                                    <span class="red">*</span>
                                </label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control textbox margin-bot8" id="fname" placeholder="First Name" required="required" ng-model="feedback.fname">
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control textbox" id="lname" placeholder="Last Name" required="required" ng-model="feedback.lname">
                                </div>
                            </div>
                        </div>
                        <!-- Email -->
                        <div class="control-group margin-bot8">
                            <div class="row">
                              <label class="control-label size14 col-sm-12" for="email">Email
                                <span class="red">*</span>
                            </label>
                            <div class="col-sm-12">
                                <span class="red hidden booking-email">Invalid Email Address</span>
                                <input type="email" class="textbox form-control margin-bot8" id="email" required="required" ng-model="feedback.email" ng-change="onemail(feedback.email)">
                            </div>
                        </div>
                        </div>
                        <!-- Comment -->
                        <div class="control-group margin-bot8">
                            <div class="row">
                              <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                <span class="red">*</span>
                            </label>
                            <div class="col-sm-12">
                                <textarea class="form-control input-large textarea" id="feedback" rows="5" required="required" ng-model="feedback.message"></textarea>
                            </div>
                        </div>
                        </div>
                        <!-- Google recaptcha -->
                        <div class="control-group margin-bot8">
                            <div class="row">
                              <div class="col-sm-12">
                                <div vc-recaptcha key="\'6LfiYQsTAAAAAMZIe1BR0fLlSfJAdLxFMr4nzOlm\'" on-create="setWidgetId(widgetId)" on-success="setResponse(response)" on-expire="cbExpiration()"></div>
                            </div>
                        </div>
                        </div>
                        <!-- Buttons -->
                        <div class="form-actions margin-bot8">
                            <div class="row">
                              <div class="col-sm-12">
                                <button type="submit" class="btn btn-sedona" ng-disabled="formContactUs.$invalid||res==false">Submit</button>
                            </div>
                        </div>
                        </div>
                        <!-- Buttons -->
                        </form>
                        </div>
                        <div class="clearfix"></div>
                        </div>
                        </div>';
                        $newstring = str_replace("[module=".$module."]", $contact, $newstring);
                        }
                        
                    }
                } 
                return $newstring;  
        } 

}

