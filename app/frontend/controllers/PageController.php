<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class PageController extends ControllerBase
{
    public function indexAction($pageslugs)
    {
        if($pageslugs) {
            $this->response->redirect($this->config->application->BaseURL.'/page404');
        }
        // $pageidglobal = 0;
        // var_dump($pageslugs);
        // die();
        // //view page content
        // $service_url = $this->config->application->ApiURL.'/page/getpage/' . $pageslugs;
        // $curl = curl_init($service_url);
        // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // $curl_response = curl_exec($curl);
        // if ($curl_response === false) {
        //     $info = curl_getinfo($curl);
        //     curl_close($curl);
        //     die('error occured during curl exec. Additioanl info: ' . var_export($info));
        // }
        // curl_close($curl);
        // $decoded = json_decode($curl_response);
        // $this->view->pageid = $decoded->pageid;
        // $this->view->title = $decoded->title;
        // $this->view->subtitle1 = $decoded->subtitle1;
        // $this->view->subtitle2 = $decoded->subtitle2;
        // $this->view->buttontitle = $decoded->buttontitle;
        // $this->view->pageslugs = $decoded->pageslugs;
        // $this->view->body = $decoded->body;
        // $this->view->peoplesaying = $decoded->peoplesaying;
        // $this->view->serviceprice = $decoded->serviceprice;
        // $this->view->banner = $decoded->banner;
        // $this->view->pagelayout = $decoded->pagelayout;

        // $pageidglobal = $decoded->pageid;

        //  // GOOGLE ANALYTICS
        // $service_url_news = $this->config->application->ApiURL. '/settings/load/googlescript';

        // $curl = curl_init($service_url_news);

        // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // $curl_response = curl_exec($curl);

        // if ($curl_response === false)
        // {
        //     $info = curl_getinfo($curl);
        //     curl_close($curl);
        //     die('error occured during curl exec. Additional info: ' . var_export($info));
        // }

        // curl_close($curl);
        // $decoded = json_decode($curl_response);

        // $this->view->gscript = $decoded[0]->script;
    }
    public function viewAction($catslugs,$pageslugs)
    {
        // $pageidglobal = 0;
        // //view page content
        $service_url = $this->config->application->ApiURL.'/page/getpage/' . $catslugs .'/'.$pageslugs;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        // var_dump($decoded);
        $this->view->pageid = $decoded->pageid;
        $this->view->title = $decoded->title;
        $this->view->subtitle1 = $decoded->subtitle1;
        $this->view->subtitle2 = $decoded->imagethumbsubtitle;
        $this->view->buttontitle = $decoded->buttontitle;
        $this->view->pageslugs = $decoded->pageslugs;
        $this->view->body = $decoded->body;
        $this->view->peoplesaying = $decoded->peoplesaying;
        $this->view->serviceprice = $decoded->serviceprice;
        $this->view->banner = $decoded->banner;
        $this->view->pagelayout = $decoded->pagelayout;
        $this->view->imgthumb =  $this->config->application->amazonlink."/uploads/pageimage/".$decoded->banner;
        $this->view->metatitle = $decoded->metatitle;
        $this->view->metadesc = $decoded->metadesc;
        $this->view->metakeyword = $decoded->metatags;

        if($decoded->status == 0){
            $this->response->redirect($this->config->application->BaseURL.'/page404');
        }
        $pageidglobal = $decoded->pageid;

        // $service_url = $this->config->application->ApiURL.'/page/getpagecategory/' . $pageslugs;
        // $curl = curl_init($service_url);
        // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // $curl_response = curl_exec($curl);
        // if ($curl_response === false) {
        //     $info = curl_getinfo($curl);
        //     curl_close($curl);
        //     die('error occured during curl exec. Additioanl info: ' . var_export($info));
        // }
        // curl_close($curl);
        // $decoded = json_decode($curl_response);
        // $this->view->pagecategory = $decoded->categorytitle;
        //list testemonils
        $service_url_testi = $this->config->application->ApiURL. '/page/gettestimonial/' . $catslugs .'/'.$pageslugs;


        $curl = curl_init($service_url_testi);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);

        $this->view->testimonials = $decoded;

    }


     public function healingAction()
    {
        $this->view->title = "Healing";
        $this->view->subtitle2 = "The Deep Tissue Massage realigns the inner layers of the muscles and connective tissues, relieving the pain and stiffness associated";
        $this->view->banner = "healing.jpg";
        $this->view->pagecategory = "";
        $this->view->pageslugs = "healing";
        $this->view->metatitle = 'Healing';
        $service_url2 = $this->config->application->ApiURL.'/utility/healingslides';

        $curl = curl_init($service_url2);

        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            echo curl_errno($curl);
            curl_close($curl);
            die();
        }
        $decoded = json_decode($curl_response);
        $this->view->slides = $decoded->slides;
        //Slides!
    }

     public function chakrabalancingandcrystalhealingAction()
    {

    }

     public function reflexologyAction()
    {

    }

     public function relaxationAction()
    {

    }

     public function hotstonemassageAction()
    {

    }

     public function deeptissuemassageAction()
    {

    }

     public function readingsAction()
    {
        $this->view->title = "Readings";
        $this->view->subtitle2 = "";
        $this->view->banner = "readings.jpg";
        $this->view->pagecategory = "";
        $this->view->pageslugs = "readings";
        $this->view->metatitle = 'Readings';
         $service_url2 = $this->config->application->ApiURL.'/utility/readingslides';

        $curl = curl_init($service_url2);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            echo curl_errno($curl);
            curl_close($curl);
            die();
        }
        $decoded = json_decode($curl_response);
        $this->view->slides = $decoded->slides;
    }

     public function spiritualguidanceAction()
    {

    }

     public function intuitivereadingAction()
    {

    }

     public function pastlifereadingAction()
    {

    }

     public function couplesreadingAction()
    {

    }

     public function acupunctureAction()
    {
        $this->view->title = "Acupuncture";
        $this->view->subtitle2 = "";
        $this->view->banner = "acupuncture.jpg";
        $this->view->pagecategory = "";
        $this->view->pageslugs = "acupuncture";
        $this->view->metatitle = 'Acupuncture';
        $service_url2 = $this->config->application->ApiURL.'/utility/acupunctureslides';

        $curl = curl_init($service_url2);

        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            echo curl_errno($curl);
            curl_close($curl);
            die();
        }
        $decoded = json_decode($curl_response);
        $this->view->slides = $decoded->slides;
    }

     public function spiritualacupunctureAction()
    {

    }

     public function elementsacupunctureAction()
    {

    }

     public function retreatsAction()
    {
        $this->view->title = "Retreats";
        $this->view->subtitle2 = "";
        $this->view->banner = "sedona_baner_retreats.jpg";
        $this->view->pagecategory = "";
        $this->view->pageslugs = "retreats";
        $this->view->metatitle = 'Retreats';
        $service_url2 = $this->config->application->ApiURL.'/utility/retreatslides';

        $curl = curl_init($service_url2);

        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            echo curl_errno($curl);
            curl_close($curl);
            die();
        }
        $decoded = json_decode($curl_response);
        $this->view->slides = $decoded->slides;
    }

     public function healyourbodysoul2daysAction()
    {

    }

     public function findyourpurpose3or4daysAction()
    {

    }

     public function manifestyourdream5or6daysAction()
    {

    }

     public function workshopsAction()
    {
        $this->view->title = "Workshops";
        $this->view->subtitle2 = "";
        $this->view->banner = "workshops_banner.jpg";
        $this->view->pagecategory = "";
        $this->view->pageslugs = "workshops";
        $this->view->metatitle = 'Workshops';
        $service_url2 = $this->config->application->ApiURL.'/utility/workshopslides';

        $curl = curl_init($service_url2);

        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            echo curl_errno($curl);
            curl_close($curl);
            die();
        }
        $decoded = json_decode($curl_response);
        $this->view->slides = $decoded->slides;
    }

     public function guidedmeditationAction()
    {

    }

     public function energyopeningtrainingAction()
    {

    }

     public function thirdeyeopeningleveloneAction()
    {

    }

     public function vortexhikemeditationAction()
    {

    }

     public function crystalpalacevisitguidedmeditationAction()
    {

    }

     public function bodymindspiritintegrationAction()
    {

    }

     public function bookingAction()
    {
        $this->angularLoader(array(
            'booking' => 'fe/scripts/controllers/booking.js',
            'birthdayfactory' => 'fe/scripts/factory/birthday.js',
            'userfactory' => 'fe/scripts/factory/user.js',
            'validation' => 'fe/scripts/directives/validations.js',
            'passstrength' => 'vendors/pass-strength/src/strength.min.js'
        ));

        $this->view->metatitle = 'Booking';
        $this->view->title = "Booking";
        $this->view->subtitle2 = "";
        unset($this->view->imgthumb);
        $this->view->pagecategory = "";
        $this->view->pageslugs = "booking";
        $this->view->facebookpixels= "    <script>(function() {
    var _fbq = window._fbq || (window._fbq = []);
    if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
    }
    _fbq.push(['addPixelId', '1617141355217381']);
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', 'PixelInitialized', {}]);
    </script>";
    }

    public function reservationAction($value){
        $this->view->metatitle = 'Reservation';
        $this->view->title = "Reservation";
        $this->view->subtitle2 = "";
        $this->view->notification = '';
        unset($this->view->imgthumb);
        $this->view->pagecategory = "";
        $this->view->pageslugs = "reservation";

        if(!empty($value)){
            $service_url = $this->config->application->ApiURL.'/booking/confirmation/' . $value;
            $curl = curl_init($service_url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $curl_response = curl_exec($curl);
            if ($curl_response === false) {
                $info = curl_getinfo($curl);
                curl_close($curl);
                die('error occured during curl exec. Additioanl info: ' . var_export($info));
            }
            curl_close($curl);
            $decoded = json_decode($curl_response);
            if(isset($decoded->error)){
                $this->response->redirect("/reservation");
            }else{
                $this->view->notification = '<div class="alert alert-success text-center">'.$decoded->success.' <a ng-click="nextClick()">Login Here</a> </div>';
            }
        }

        $this->angularLoader(array(
            'reservation' => 'fe/scripts/controllers/reservation.js',
            'reservationfactory' => 'fe/scripts/factory/reservation.js',
            'newsfactory' => 'fe/scripts/factory/news.js',
            'validation' => 'fe/scripts/directives/validations.js',
            'passstrength' => 'vendors/pass-strength/src/strength.min.js',
            'wizardboostrap' => 'fe/js/jquery.bootstrap.wizard.js',
            'wizard' => 'fe/js/wizard.js'
        ));
    }

    public function aboutsedonahealingartsAction()
    {
        $this->view->metatitle = 'About Sedona Healing Arts';
        $this->view->title = "About Sedona Healing Arts";
        $this->view->subtitle2 = "Sedona Healing Arts is a spiritual healing center that offers a wide array of healing services for the body, mind and spirit. We also provide other services such as spiritual readings, acupuncture, retreats, and massage. Sedona Healing Arts also functions as a metaphysical products and souvenirs store. The shop is operated by the most wonderful spiritual guide, healer, and licensed acupuncturist, Banya Lim.";
        $this->view->banner = "banner_about.png";
        $this->view->pagecategory = "";
        $this->view->pageslugs = "aboutsedonahealingarts";
    }

    public function contactusAction()
    {

        $this->angularLoader(array(
            'contactus' => 'fe/scripts/controllers/contactus.js'
        ));

        $this->view->metatitle = 'Contact us';
        $this->view->title = "Contact us";
        $this->view->subtitle2 = "";
        unset($this->view->imgthumb);
        $this->view->pagecategory = "";
        $this->view->pageslugs = "contactus";

        $this->view->facebookpixels= "    <script>(function() {
            var _fbq = window._fbq || (window._fbq = []);
            if (!_fbq.loaded) {
            var fbds = document.createElement('script');
            fbds.async = true;
            fbds.src = '//connect.facebook.net/en_US/fbds.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(fbds, s);
            _fbq.loaded = true;
            }
            _fbq.push(['addPixelId', '1617141355217381']);
            })();
            window._fbq = window._fbq || [];
            window._fbq.push(['track', 'PixelInitialized', {}]);
            </script>";
    }

    public function feedAction()
    {
      $service_url = $this->config->application->ApiURL.'/news/frontend/listnews/0/10';
      $curl = curl_init($service_url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      $curl_response = curl_exec($curl);
      if ($curl_response === false) {
          $info = curl_getinfo($curl);
          curl_close($curl);
          die('error occured during curl exec. Additioanl info: ' . var_export($info));
      }
      curl_close($curl);
      $decoded = json_decode($curl_response);

      $this->view->items = $decoded;

      $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function page404Action(){
      $this->view->metatitle = 'Page not found';
      $this->view->title = "Page not found";
      $this->view->subtitle2 = "";
      $this->view->banner = "404.jpg";
      $this->view->pagecategory = "";
      $this->view->pageslugs = "";
    }

    public function servicesAction() {
      // $this->view->service = $this->curl("/service/");
    }
}
