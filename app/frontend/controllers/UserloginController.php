<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;


class UserloginController extends ControllerBase
{
   
	public function userregistrationAction(){


		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

	}

	public function userconfirmationAction($value){


        if(!empty($value)){
            $service_url = $this->config->application->ApiURL.'/booking/confirmation/' . $value;
           
            $curl = curl_init($service_url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $curl_response = curl_exec($curl);
            if ($curl_response === false) {
                $info = curl_getinfo($curl);
                curl_close($curl);
                die('error occured during curl exec. Additioanl info: ' . var_export($info));
            }
            curl_close($curl);
            $decoded = json_decode($curl_response);
            
            if(isset($decoded->error)){
                $this->view->responsetype = 'error';
            }else{
                 $this->view->responsetype = 'success';
            }
          }


		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

	}

	public function userloginAction(){


		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

	}


   
}

