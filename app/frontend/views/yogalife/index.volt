<?php echo $this->getContent()?>
<div id="crumbs_cont">
	<ul id="crumbs">
		<li class="crumbsli"><a class="crumbs" href="#">Home</a></li>
		<li class="crumbsli"><a class="crumbs" href="#">Yoga Life</a></li>
		<?php 
			$getginfo = $newscategory;
			foreach ($getginfo as $key => $value) {
		?>
		<li>
		<a class="crumbscategory" href=""><?php echo $getginfo[$key]->categoryname . " &nbsp; &#8226;";  ?></a>
		</li>
		<?php } ?>  
	</ul>
</div>

<div class="divseparetor"></div>


<div class="row">
    <div class="col s12">
      	<div class="newsindexcontent">

	        <div class="newsindexnewsdata">
	          
	        	<div class="featurednewscontainer">

	        		<div class="featurednews">
	        			<?php 
	        			$getginfo = $featurednews;
	        			foreach ($getginfo as $key => $value) {
	        			?>
	        			<h5>Featured</h5>
	        			<div class="thumbnailimg" style="background-image: url('<?php echo  $this->config->application->amazonlink."/uploads/newsimage/".$getginfo[$key]->banner; ?>');">
                      	</div>

	        			<div class="thumbnailtitlecontainier">
	        				<div class="thumbseparator"></div>
		        			<a href="#"><span class="thumbnailtitle"><?php echo $getginfo[$key]->title; ?></span></a>
		        			<div class="cmt">
		        				<a href="#" class="comment">Comments (1)</a><span> | </span><?php echo $getginfo[$key]->date; ?>
		        			</div>
	        			</div>
	        			<?php } ?>
	        		</div>

	        		<div class="founderswisdom">
	        			<?php 
	        			$getginfo = $founderswisdom;
	        			foreach ($getginfo as $key => $value) {
	        			?>
	        			<h5><a href="">Founder's Wisdom</a></h5>
	        			<div class="thumbnailimg" style="background-image: url('<?php echo  $this->config->application->amazonlink."/uploads/newsimage/".$getginfo[$key]->banner; ?>');">
                      	</div>

	        			<div class="thumbnailtitlecontainier">
	        				<div class="thumbseparator"></div>
		        			<a href="#"><span class="thumbnailtitle"><?php echo $getginfo[$key]->title; ?></span></a>
		        			<div class="cmt">
		        				<a href="#" class="comment">Comments (1)</a><span> | </span><?php echo $getginfo[$key]->date; ?>
		        			</div>
	        			</div>
	        			<?php } ?>
	        		</div>
	           	

	           </div>

	           <div class="latestnewscontainer">
	           		<h5>Latest</h5>
	           		<?php 
	           			$getginfo = $latestnews;
	           			$viewcount = 0;
	           			foreach ($getginfo as $key => $value) {

	           				if($viewcount == 0)
	           				{

	           		?>
	           		<div class="latestnewsleft">
	        			<div class="thumbnailimg" style="background-image: url('<?php echo  $this->config->application->amazonlink."/uploads/newsimage/".$getginfo[$key]->banner; ?>');">
                      	</div>

	        			<div class="thumbnailtitlecontainier">
	        				<div class="thumbseparator"></div>
		        			<a href="#"><span class="thumbnailtitle"><?php echo $getginfo[$key]->title; ?></span></a>
		        			<div class="cmt">
		        				<a href="#" class="comment">Comments (1)</a><span> | </span><?php echo $getginfo[$key]->date; ?>
		        			</div>
	        			</div>
	        		</div>
	        		<?php
		        			$viewcount = 1;
		        			}
		           			else
		           			{

	           		?>


	           			<div class="latestnewsright">
	        			<div class="thumbnailimg" style="background-image: url('<?php echo  $this->config->application->amazonlink."/uploads/newsimage/".$getginfo[$key]->banner; ?>');">
                      	</div>

	        			<div class="thumbnailtitlecontainier">
	        				<div class="thumbseparator"></div>
		        			<a href="#"><span class="thumbnailtitle"><?php echo $getginfo[$key]->title; ?></span></a>
		        			<div class="cmt">
		        				<a href="#" class="comment">Comments (1)</a><span> | </span><?php echo $getginfo[$key]->date; ?>
		        			</div>
	        			</div>
	        		</div>


	           		<?php
	           				$viewcount = 0;
	           				}
	           			} 
	           		?>
	        		


	           </div>

	        </div>

	        <div class="newsindexsidebar">
	          <a href=""><img src="/img/frontend/banner_getstarted_small.jpg" style="width:100%;height:auto;"></a>
	          <!-- <a class="twitter-timeline" href="https://twitter.com/rainierllarenas" data-widget-id="606030918982115330">Tweets by @rainierllarenas</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
 -->	        </div>

      	</div>
    </div>
</div>