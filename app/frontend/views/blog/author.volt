<?php echo $this->getContent()?>
<div class="container margin-top40">


    <div class="col-sm-12" ng-controller="NewsCtrl" ng-init="getauthor()">
      <div class="row">
        <div class="col-sm-12  margin-bot20">
          <div class="row">

            <div class="col-sm-12 margin-bot10 center">
              <div class="img-author-page">
                <div style="background-image : url( '<?php echo $this->config->application->amazonlink; ?>/uploads/saveauthorimage/{[{ authorImage }]} ');" class="author-img author-img-pg"></div>
                <img src="<?php echo $this->config->application->amazonlink; ?>/uploads/saveauthorimage/{[{ authorImage }]} " class="pinterest-img">
              </div>

              <h1 class="margin-bot10 author-name">{[{ name }]}</h1>
              <p class="margin-bot10"><strong class="orange">Location:</strong> {[{ location }]} </p>
              <p class="margin-bot10"><strong class="orange">Occupation:</strong>  {[{ occupation }]} </p>
            </div>
            <div class="col-sm-12 margin-top20 a-little-about-me">
              <h3>A little about me</h3>
            </div>
            <div class="col-sm-12 margin-top20" ng-bind-html="about">
            </div>
          </div>
          <div class="row margin-top20">
            <h5 class="blog-title-list orange ">The News</h5>
              <div class="list-news-wrapper">
                <div class="row list-title-blog padding-left15" ng-repeat="news in newslist" ng-click="redirectNews(news.newsslugs);">
                  <div class="col-sm-1 timeline-news">
                      <div class="radius-bg">{[{ $index + 1 }]}</div>
                  </div>
                  <!--<div class="col-sm-2 img-news-author">-->
                    <!--<div class="youtube-play" ng-show="news.videothumb"><img src="/img/youtubeplay.png"/></div>-->
                    <!--<a href="/blog/view/{[{ news.newsslugs; }]}">-->
                      <!--<img ng-show="news.videothumb != ''" src="{[{ news.videothumb | returnYoutubeThumb }]}" class="news-tumb">-->
                      <!--<img ng-show="news.imagethumb != ''" src="{[{ news.imagethumb | returnImageThumb  }]}" class="news-tumb">-->
                    <!--</a>-->
                  <!--</div>-->

                  <div class="col-sm-3 news-thumb-container-index" style="background-image: url('{[{ news.videothumb | returnYoutubeThumb }]} {[{ news.imagethumb | returnImageThumb }]}')">
                    <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img">
                    <img src="{[{ news.videothumb | returnYoutubeThumb }]} {[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img">
                    <div class="youtube-play" ng-show="news.videothumb"><img src="/img/youtubeplay.png"/></div>
                    <a href="/blog/{[{ news.newsslugs; }]}">
                    </a>
                  </div>


                  <div class="col-sm-7 news-list-desc">
                    <div class="row">

                        <div class="col-sm-12">
                          <span class="size25 font1 news-title">{[{ news.title }]}</span>
                        </div>
                      <div class="col-sm-12">
                        <strong><span class="thin-font1 orange">{[{ news.categorylist }]}</span></strong> <span ng-show="news.name !=''" class="thin-font1"> / by <strong><span class="orange">{[{ news.name }]}</span></strong></span> / {[{ news.date }]}
                        <br/><br/>
                      </div>
                        <div class="col-sm-12">
                          <div class="font1 size14 summary">
                            {[{ news.summary }]}
                            <br/><br/>
                          </div>
                        </div>

                        <!-- <div class="col-sm-12 font1 size15">
                        <br/>
                          <?php
                          $removedimg = strip_tags($getginfo[$key]->body, '');
                          $limitcontent = substr($removedimg,0,275);
                          echo $limitcontent;
                          ?>
                        </div> -->
                      </div>
                    <div style="clear:both"></div>
                    <br>
                  </div>
                </div>
                <button ng-hide="hideloadmore" class="news-list-show-more" ng-click="getauthor()" ng-disabled="loading"><span ng-show="loading">Loading</span><span ng-hide="loading">Show More</span></button>
              </div>
          </div>
        </div>
      </div>
    </div>

</div>
