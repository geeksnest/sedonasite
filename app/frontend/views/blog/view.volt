<?php echo $this->getContent()?>
<div class="banner-container blog-bg" title="<?php echo $title; ?>">
  <img src="/img/frontend/blog.jpg" class="pinterest-img" alt="<?php echo $title; ?>">
</div>
<div class="container margin-top40" ng-controller="NewsCtrl">
  <div class="row">

    <div class="col-sm-9 margin-bot20">
      <div class="row">

        <div class="col-sm-12">
          <p class="size50 thin-font1 orange news-title"><?php echo $title; ?></p>
        </div>
        <?php if($summary !=''){?>
        <div class="col-sm-12">
          <div class="font1 size14 summary">
            <br/>
            <?php echo $summary; ?>
            <br/><br/>
          </div>
        </div>
        <?php } ?>
        <div class="col-sm-12">
          <span class="thin-font1">Date: <span class="orange"><?php echo $date; ?></span></span> | <span class="thin-font1">Author: <span class="orange"><?php echo $name; ?></span></span>
          <br>
          <hr class="styled-hr">
          <br>
        </div>
        <div class="col-sm-12 center" >
          {{ featured }}
          <img src="<?php echo $imgthumb;?>" class="pinterest-img" alt="<?php echo $title; ?>"/>
          <br/>
          <br/>
        </div>
        <div class="col-sm-12">
          <div class="font1 size15">
            <?php echo $body; ?>
          </div>
        </div>
        <div class="col-sm-12">
          <br>
          <br>
          Category :

          <?php
            $x = 1;
            foreach($categorylist as $c=>$val){
          ?>
          <a href="/blog/category/<?php echo $val->categoryslugs; ?>"><?php echo $val->categoryname; ?></a><?php echo $x == count($categorylist) ? "" : ", ";
            $x++;
          }
          ?>
          <br>
          Tags :
          <?php
            $getginfo = $newstags;
            $x = 1;
            foreach ($getginfo as $key => $value) {
          ?>
            <span><a href="/blog/tags/<?php echo $getginfo[$key]->slugs;?>"><?php echo $getginfo[$key]->tags;?></a><?php echo $x == count($getginfo) ?  "" : ", ";?> </span>
          <?php
            $x++;
          } ?>
          <br>
        </div>
        <!-- <div class="col-sm-12">
          <hr class="styled-hr">
          <div class="row">

          </div>
        </div>  -->
        <div class="col-sm-12">
          <hr class="styled-hr">
          <div class="row about-author">
            <div class="col-sm-12 margin-bot10">
              <span class="size18 font1"><strong>About the Author</strong></span>
            </div>
            <div class="col-sm-2">
              <a href="/blog/author/<?php echo $name; ?>" class="size15">
            <div style="background-image: url('<?php echo $this->config->application->amazonlink; ?>/uploads/saveauthorimage/<?php echo $image; ?>'); " class="author-img authorimg-width"></div>
                <img src="<?php echo $this->config->application->amazonlink; ?>/uploads/saveauthorimage/<?php echo $image; ?>" class="pinterest-img">
                </a>
            </div>
            <div class="col-sm-10">
            <p class="margin-bot10"><strong><a href="/blog/author/<?php echo $name; ?>" class="size15"><?php echo $name; ?></a></strong></p>
            <p><?php echo $about; ?></p>
            </div>
          </div>
          <hr class="styled-hr">
        </div>
        <div class="col-sm-12">
          <div class="blog-social">
            <div class="blog-social-item blog-fb-like float-left">
              <!-- <div class="fb-like" data-href="<?php echo $this->config->application->baseURL; ?>/blog/view/<?php echo $newsslugs ?>" data-layout="standard" data-action="like" data-show-faces="true" data-share="false"></div> -->
              <div class="fb-like" data-href="<?php echo $this->config->application->baseURL; ?>/blog/<?php echo $newsslugs ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
              <div id="fb-root"></div>
              <div id="fb-root"></div>
              <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=217644384961866";
                fjs.parentNode.insertBefore(js, fjs);
              }(document, 'script', 'facebook-jssdk'));</script>


            </div>
            <div class="blog-social-item float-left margin-left">
              <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
              <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
              </script>
            </div>
            <div class="blog-social-item float-left margin-left">
              <a href="//www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark"  data-pin-color="red"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_red_20.png" /></a>
              <!-- Please call pinit.js only once per page -->
              <script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
            </div>
            <div style="clear:both"></div>

            <div id="disqus_thread"></div>
            <script type="text/javascript">
              /* * * CONFIGURATION VARIABLES * * */
              var disqus_shortname = 'sedonahealingarts';

              /* * * DON'T EDIT BELOW THIS LINE * * */
              (function() {
                var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
              })();
            </script>
            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
          </div>
        </div>

      </div>
    </div>

    <div class="col-sm-3">
      <div class="no-padding">
          <hr class="styled-hr">
          <span class="size25 font1">About Banya</span>
          <span class="size16 font1"><br/><br/>
          Banya Lim is the spiritual intuitive guide and healer at Sedona Story. She is a nationally licensed acupuncturist and healer of Oriental Medicine. Banya will equip you with new clarity to see your life in a much more positive and hopeful direction.</span>  <br/><br/>
          <!-- <span class="size25 font1">Archives</span> <br/> <br/>
          <span class="size16"><a href="">March 2015 </a></span> <br/> <br/>  -->
          <hr class="styled-hr">
          <span class="size25 font1">Categories</span> <br/> <br/>
          <div class="categorylist-wrapper">
            <span class="size16"><a href="<?php echo $base_url;?>/blog/all">All</a></span>
            <?php
              $getginfo = $newscategory;
              foreach ($getginfo as $key => $value) {
            ?>
              <span class="size16"><a href="<?php echo $base_url;?>/blog/category/<?php echo $getginfo[$key]->categoryslugs; ?>"><?php echo $getginfo[$key]->categoryname; ?></a></span>
            <?php } ?>
          </div>

          <!-- archives -->
          <hr class="styled-hr">
          <h2 class="font1"><span class="fa fa-calendar fa-1x"></span> Archives</h2>
          <div class="ul-archives">
            <?php
              $getarchives = $archivelist;
              foreach ($getarchives as $key => $value) { ?>
                <a href="<?php echo $base_url; ?>/blog/archive/<?php echo $getarchives[$key]->month . "/" . $getarchives[$key]->year; ?>"><span class="fa fa-chevron-right"></span> <?php echo $getarchives[$key]->month . " " . $getarchives[$key]->year; ?></a>
            <?php
            } ?>
          </div>

          <!-- tags -->
          <hr class="styled-hr">
          <h2 class="font1"><span class="fa fa-tag fa-1x"></span> Tags</h2>
          <?php
            $gettags = $newstagslist;
            $count = 1;
            foreach ($gettags as $key => $value) { ?>
              <a href="<?php echo $base_url; ?>/blog/tags/<?php echo $gettags[$key]->slugs; ?>" class="tagfont<?php echo $count; ?> tagfont"><?php echo $gettags[$key]->tags; ?></a>
          <?php
            $count > 5 ? $count =1 : $count++;
          } ?>

          <hr class="styled-hr"> <br/>
          <p class="size16"> <a href="<?php echo $base_url;?>/feed" target="_blank"><img class="rss" src="/img/frontend/rss_feed.gif"> RSS Feed</a></p> <br/> <br/>
      </div>
    </div>

  </div>
  <hr class="styled-hr">
</div>

<!--
<div class="container">
    <div class="row">
      <div class="col-sm-12 title2">
        <span class="titlel">Store Information</span>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-7 center">
        <div class="wsite-map"><iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 250px; margin-top: 10px; margin-bottom: 10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3273.673285579057!2d-111.7637428!3d34.8644358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872da1564d73fd15%3A0x3e31d123930491bf!2s201+Hwy+179%2C+Sedona%2C+AZ+86336%2C+USA!5e0!3m2!1sen!2sph!4v1434348998372"></iframe></div>
      </div>
      <div class="col-sm-5 text-left">
        <div class="title3">
          <span class="titleb">Phone:</span> <span class="thin-font3">(928) 282-3875</span> <br/>
          <span class="titleb">Email:</span> <span class="thin-font3">contact@sedonahealingarts.com</span> <br/>
          <span class="titleb">Hours:</span> <span class="thin-font3">Mon - Sun: 10 am - 7 pm</span>  <br/> <br/>
        </div>
        <div class="title3">
          <span class="titleb text-top">Address:</span> <br/>
          <span class="thin-font3">201 State Route 179</span> <br/>
          <span class="thin-font3">Sedona, AZ 86336</span>
        </div>
      </div>
    </div>
    <hr class="styled-hr">
  </div>   -->
