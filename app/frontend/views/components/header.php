<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="user-scalable = yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!-- Title and other stuffs -->
    <title>Sedona Healing Arts</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>

    <!-- Stylesheets -->
    <link href="style/bootstrap.css" rel="stylesheet">

    <!-- Main stylesheet -->
    <link href="style/style.css" rel="stylesheet">
    <!-- Stylesheet for Color -->
    <link href="style/orange.css" rel="stylesheet">
    <!-- Responsive style (from Bootstrap) -->
    <link href="style/bootstrap-responsive.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <!-- Dropdown stylesheet -->
    <link href="style/dropdown-menu-styles.css" rel="stylesheet">
    <!-- Custom stylesheet -->
    <link href="style/custom-style.css" rel="stylesheet">

    <!-- HTML5 Support for IE -->
    <!--[if lt IE 9]>
    <script src="js/html5shim.js"></script>
    <![endif]-->

    <!-- Favicon -->
    <link rel="shortcut icon" href="img/sedona_images/favicon.png">
</head>

<body>

<!-- Header starts -->
<header>

    <div class="top-bg">
        <div class="contact-us-sun">
            <div class="contact-us-sun-inside">
                <h3>Make an appointment!</h3>
                <h1>Call 928-282-3875</h1>
                <h3>- OR -</h3>
                <div class="book-now">BOOK ONLINE NOW</div>
            </div>
        </div>
        <div class="upper-header">
                <img src="/img/sedona_images/fb-social.png"/>
                <img src="/img/sedona_images/twitter-social.png"/>
                <img src="/img/sedona_images/youtube-social.png"/>
                <img src="/img/sedona_images/mail-social.png"/>
        </div>
        <div class="container">
            <div class="row top-header">
                <div class="col-sm-12">
                    <!-- Logo. Use class "color" to add color to the text. -->
                    <div class="sedona-logo">
                        <a href="index.php"><span class="title-color"><img src="/img/sedona_images/sedonalogo.png"></span></a>
                    </div>

                </div>
                <!-- ===== SOCIALS ===== -->
                <!-- <div class="span4">

                  <div class="social-top">
                    <a href="#"><i class="fa fa-facebook-square fa-2x"></i></a>
                    <a href="#"><i class="fa fa-twitter-square fa-2x"></i></a>
                    <a href="#"><i class="fa fa-linkedin-square fa-2x"></i></a>
                    <a href="#"><i class="fa fa-youtube-square fa-2x"></i></a>
                    <a href="#"><i class="fa fa-google-plus-square fa-2x"></i></a>
                  </div>

                </div> -->
                <!-- ===== END SOCIALS ===== -->
                <div class="contact-us-mobile span12 center">
                    <h3>Make an appointment!</h3>
                    <h1>Call 928-282-3875</h1>
                    <h3>- OR -</h3>
                    <div class="book-now">BOOK ONLINE NOW</div>
                </div>
            </div>
        </div>
    </div>

    <!-- Web menu starts -->
    <div class="menu-bg web-menu">
        <div class="container menu-margin">
            <div class="row center">

                <div class="col-sm-12">
                    <!-- Navigation -->
                    <div id='cssmenu'>
                        <ul>
                            <li class='uppercase has-sub'><a class="menu-text" href='healing.php'><span>HEALING</span></a>
                                <ul>
                                    <li><a class='caps' href='chakra-balancing-and-crystal-healing.php'><span>Chakra Balancing & Crystal Healing</span></a></li>
                                    <li><a class='caps' href='reflexology.php'><span>reflexology</span></a></li>
                                    <li><a class='caps' href='relaxation.php'><span>relaxation massage</span></a></li>
                                    <li><a class='caps' href='hot-stone-massage.php'><span>hot stone massage</span></a></li>
                                    <li><a class='caps' href='deep-tissue-massage.php'><span>deep tissue massage</span></a></li>
                                </ul>
                            </li>
                            <li class='uppercase has-sub'><a class="menu-text" href='readings.php'><span>READINGS</span></a>
                                <ul>
                                    <li><a class='caps' href='spirit-guidance.php'><span>spiritual guidance</span></a></li>
                                    <li><a class='caps' href='intuitive-reading.php'><span>intuitive reading</span></a></li>
                                    <li><a class='caps' href='past-life-reading.php'><span>past life reading</span></a></li>
                                    <li><a class='caps' href='couples-reading.php'><span>couple's reading</span></a></li>
                                </ul>
                            </li>
                        </li>
                        <li class='uppercase has-sub'><a class="menu-text" href='acupuncture.php'><span>ACUPUNCTURE</span></a>
                            <ul>
                                <li><a class='caps' href='spiritual-acupuncture.php'><span>spiritual acupuncture</span></a></li>
                                <li><a class='caps' href='5-elements-acupuncture.php'><span>5 elements acupuncture</span></a></li>
                            </ul>
                        </li>
                        <li class='uppercase has-sub'><a class="menu-text" href='retreats.php'><span>RETREATS</span></a>
                            <ul>
                                <li><a class='caps' href='heal-your-body--soul---2-days.php'><span>Heal Your Body & Soul - (2 DAYS)</span></a></li>
                                <li><a class='caps' href='find-your-purpose-3-or-4-days.php'><span>Find Your Purpose (3 or 4 DAYS)</span></a></li>
                                <li><a class='caps' href='manifest-your-dream-5-or-6-days.php'><span>Manifest Your Dream (5 or 6 DAYS)</span></a></li>
                            </ul>
                        </li>
                        <li class='uppercase has-sub last'><a class="menu-text" href='workshops.php'><span>WORKSHOPS</span></a>
                            <ul>
                                <li><a class='caps' href='guided-meditation.php'><span>Guided Meditation</span></a></li>
                                <li><a class='caps' href='energy-opening-training.php'><span>Energy Opening Training</span></a></li>
                                <li><a class='caps' href='3rd-eye-opening-level-1.php'><span>3rd Eye Opening (Level 1)</span></a></li>
                                <li><a class='caps' href='vortex-hike--meditation.php'><span>Vortex Hike & Meditation</span></a></li>
                                <li><a class='caps' href='crystal-palace---visit--guided-meditation.php'><span>Crystal Palace - Visit & Guided Meditation</span></a></li>
                                <li><a class='caps' href='body-mind--spirit-integration.php'><span>Body, Mind & Spirit Integration</span></a></li>
                            </ul>
                        </li>
                    </ul>
                    </div>

                </div>

            </div>
        </div>

    </div>
    <!-- Web menu ends -->

    <!-- Mobile menu starts -->
    <div class="mobile-menu">
        <div class="no-padding">
            <div class="row no-padding ">

                <div class="col-sm-12 no-padding">
                    <!-- Navigation -->
                    <div class="navbar">
                        <div class="navbar-inner">
                            <div class="">
                                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </a>
                                <div class="nav-collapse collapse">
                                    <ul class="nav menu-left">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle mobile-menu-bg" data-toggle="dropdown">HEALING SERVICES<b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="intuitive-reading/">Intuitive Reading</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#" class="mobile-menu-bg">RETREATS & WORKSHOPS</a></li>
                                        <li><a href="#" class="mobile-menu-bg">MASSAGE</a></li>
                                        <li><a href="#" class="mobile-menu-bg">BLOG</a></li>
                                        <li><a href="#" class="mobile-menu-bg">CONTACT</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>
    <!-- Mobile menu ends -->

</header>

<!-- Seperator -->



<!-- Header ends -->
<!-- Below Banner starts -->
