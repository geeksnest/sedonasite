<style>
#productslider {
  position:relative;
  height:119px;
  width:100%;
  border-top:1px solid #aaa;
  border-bottom:1px solid #aaa;
  margin:10px 0;
}
#productslider ul {
  padding:0;
}
#productslider ul li {
  list-style-type:none;
  cursor:pointer;
  border:1px solid gray;
  height:111px;
  min-width:79px;
  margin:3px;
}
#productslider ul li:hover {
  border-color:orange;
}
</style>

<div class="container-fluid bg-light" ng-controller="productCtrl">
  <div class="row">
    <div class="col-sm-10 col-sm-offset-1 bg-light ec-nopad">

    <div class="col-sm-12 wrapper-md">

      <div ng-include="'/fe/tpl/ecommerce/header.html'"></div>
      <div ng-include="'/fe/tpl/ecommerce/menu.html'"></div>

      <div class="panel panel-default">
        <div class="panel-heading">
          <ul class="breadcrumb bg-white b-a" style="margin:0; border:none; border-bottom:1px solid #ccc;">
            <li><a href=""><i class="fa fa-home"></i> Breadcrumb</a></li>
            <li><a href="">Elements</a></li>
            <li class="active">Components</li>
          </ul>
          <br>
          <span class="font-bold ec-font-md">{{product.name}}</span>
        </div>
        <div class="panel-body">

          <div class="row" ng-init="activeimage = '<?php echo $productimages[0]->filename; ?>' ">
            <div class="col-sm-12">
              <div class="col-sm-6">
                <div class="ec-bg-centered" style="height:34.375vw; border:1px solid #ccc; transition:all ease .9s;
                     background:url('{{amazonlink}}/uploads/productimage/{{product.productid}}/{[{ activeimage }]}')">
                </div>

                <div id="productslider">
                  <div class="thumbelina-but horiz left">&#706;</div>
                  <ul>
                    {% for img in productimages %}
                      <li class="ec-bg-center"
                           ng-click="activeimage = '<?php echo $img->filename;?>' "
                           style="background:url('{{amazonlink}}/uploads/productimage/{{product.productid}}/{{img.filename}}')">
                      </li>
                    {% endfor %}
                  </ul>
                  <div class="thumbelina-but horiz right">&#707;</div>
                </div>
              </div>

              <div class="col-sm-6">
                <div style="margin:0; overflow:auto; padding:0 10px;">
                  {% if product.discount != undefined AND product.discount > 0 %}
                     <span class="ec-prod-price text-danger">
                       ${{ product.price - (product.price * (product.discount / 100)) }}
                     </span>
                     You save {{ product.discount }}%
                  {% else %}
                     <span class="ec-prod-price text-danger">${{ product.price }}</span>
                  {% endif %}

                  <button type="button" class="btn m-b-xs btn-md bg-orange btn-addon pull-right"
                           data-toggle="tooltip"
                           title="{[{ {true: 'You reached the maximum quantity per order of this product.', false: 'Buy me :)'} [limit] }]}"
                           ng-click="addtocart('{{product.productid}}', {{product.minquantity}}, {{product.maxquantity}})"
                           ng-disabled="itemQuantityOnCart( '{{product.productid}}', {{product.maxquantity}} )">
                    <i class="fa fa-shopping-cart"></i>ADD TO CART
                  </button>

                  <table class="table table-responsive ec-prod-info">
                    <tr>
                      <td>Product Code</td>
                      <td>{{product.productcode}}</td>
                    </tr>
                    <tr>
                      <td>Type</td>
                      <td>{{product.type}}</td>
                    </tr>
                    <tr>
                      <td class="font-bold">Stock</td>
                      <td>{{product.quantity}}</td>
                    </tr>
                    <tr>
                      <td class="font-bold">Minimum purchase per order</td>
                      <td>{{product.minquantity}}</td>
                    </tr>
                    <tr>
                      <td class="font-bold">Maximum Purchase per order</td>
                      <td>{{product.maxquantity}}</td>
                    </tr>
                    <tr>
                      <td class="font-bold">Short Description</td>
                      <td>{{product.shortdesc}}</td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>

            <div class="col-sm-12 b-b">
               {#<div  id="productslider">
                 <div class="thumbelina-but vert top">&#708;</div>
                 <ul>
                   {% for img in productimages %}
                     <li class="ec-bg-centered"
                          ng-click="activeimage = '<?php echo $img->filename;?>' "
                          style="background:url('{{amazonlink}}/uploads/productimage/{{product.productid}}/{{img.filename}}')">
                     </li>
                   {% endfor %}
                 </ul>
                 <div class="thumbelina-but vert bottom">&#709;</div>
               </div>

               <div class="ec-bg-centered" style="width:300px; height:300px; float:left; margin:19px 0 0 10px;
                    background:url('{{amazonlink}}/uploads/productimage/{{product.productid}}/{[{ activeimage }]}')">
               </div>#}

            </div>
          </div> {# end of row #}

          <div class="row">
            <div class="col-sm-9 wrapper-sm b-b">
              {{ product.description }}
            </div> {# end of col-sm-9 #}
          </div> {# end of row #}

        </div> {# end of panel-body #}
      </div> {# end of panel-default #}
    </div> {# end of col-sm-9 col-md-10 wrapper-md #}

    </div> {# end of col-sm-12 bg-light ec-nopad #}
  </div> {# end of row #}
</div> {# end of container-fluid bg-light #}

<script type="text/ng-template" id="addtocart">
  <div ng-include="'/fe/tpl/ecommerce/addtocart.html'"></div>
</script>
