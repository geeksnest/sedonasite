<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $invoice[0]->invoiceno.' - '.$invoice[0]->firstname." ".$invoice[0]->lastname ?></title>
    <link rel="stylesheet" href="/be/css/bootstrap.css">
    <style>
        @import url(http://fonts.googleapis.com/css?family=Bree+Serif);
        body, h1, h2, h3, h4, h5, h6{
            font-family: 'Bree Serif', serif;
        }
    </style>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-xs-6">
            <h1>
                <img src="/img/frontend/sedonalogo.png">
            </h1>
        </div>
        <div class="col-xs-6 text-right">
            <h3> <small>Date: <?php echo $invoice[0]->created_at?></small></h3>
            <h1>INVOICE</h1>
            <h1><small>Invoice #<?php echo $invoice[0]->invoiceno?></small></h1>

        </div>
    </div>
    <div class="row">
        <div class="col-xs-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>From: <a href="#">Sedona Healing Arts</a></h4>
                </div>
                <div class="panel-body">
                    <p>
                        201 State Route 179,<br>
                        Sedona, AZ 86336 <br>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-xs-5 col-xs-offset-2 text-right">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>To : <a href="#"><?php echo $invoice[0]->firstname." ".$invoice[0]->lastname ?></a></h4>
                </div>
                <div class="panel-body">
                    <p>
                        <?php echo $invoice[0]->address1;?><br>
                        <?php echo $invoice[0]->state ?>, <?php echo $invoice[0]->country ?>, <?php echo $invoice[0]->zipcode ?><br>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- / end client details section -->
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>
                <h4>Service</h4>
            </th>
            <th>
                <h4>Description</h4>
            </th>
            <th>
                <h4>Hrs/Day</h4>
            </th>
            <th>
                <h4>Rate/Price</h4>
            </th>
            <th>
                <h4>Sub Total</h4>
            </th>
        </tr>
        </thead>
        <tbody>
        <?php
            foreach($invoice as $key=>$val){
                echo '  <tr>
                            <td>'.$val->title.'</td>
                            <td>'.$val->description.' <br> '.$val->startdatetime.' to '.$val->enddatetime.' </td>
                            <td class="text-right">'.$val->hourday.'</td>
                            <td class="text-right">'.$val->price.'</td>
                            <td class="text-right">'.$val->price.'</td>
                        </tr>';
            }
        ?>
        </tbody>
    </table>
    <div class="row text-right">
        <div class="col-xs-2 col-xs-offset-8">
            <p>
                <strong>
                    Sub Total : <br>
                    TAX : <br>
                    Total : <br>
                </strong>
            </p>
        </div>
        <div class="col-xs-2">
            <strong>
                $<?php echo $invoice[0]->amount;?> <br>
                N/A <br>
                $<?php echo $invoice[0]->amount;?> <br>
            </strong>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-5">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4>Billing Info</h4>
                </div>
                <div class="panel-body">
                    <p>Name: <?php echo $invoice[0]->billingfname;?> <?php echo $invoice[0]->billinglname;?></p>
                    <p>Address: <?php echo $invoice[0]->billingadd1;?></p>
                    <p><?php echo $invoice[0]->billingstate;?>, <?php echo $invoice[0]->billingstate;?>, <?php echo $invoice[0]->billingzipcode;?></p>
                    <p>Payment Type: <?php echo $invoice[0]->paymenttype;?></p>
                </div>
            </div>
        </div>
        <div class="col-xs-7">
            <div class="span7">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4>Contact Details</h4>
                    </div>
                    <div class="panel-body">
                        <p>
                            Email : contact@sedonahealingarts.com <br><br>
                            Mobile : (928) 282-3875 <br> <br>
                            Hours: Mon - Sun: 10 am - 7 pm
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>