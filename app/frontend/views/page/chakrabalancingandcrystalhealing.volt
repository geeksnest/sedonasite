

    <!-- Banner starts -->
    <div class="banner-container chakra-balancing-and-crystal-healing-bg">

        <div class="black-box">
            <span class="banner-title">Chakra Balancing & Crystal Healing</span>
            <br/>
            <span class="banner-sub-title1">Release Energetic Tension</span> <br/> <br/>
            <div class="button"><a href="<?php echo $base_url;?>/booking" class="learn-sedona">MAKE AN APPOINTMENT</a></div>
        </div>

    </div>

    <!-- Banner ends -->

    <div class="container">
        <div class="row">
            <div class="col-sm-8 no-margin content-text">
                <div class="phar no-padding">   
                    <span class="size20"><h2>CHAKRA BALANCING & CRYSTAL HEALING</h2></span>  
                    <span class="size18">
                        A Couple’s Relationship Reading will take your relationship to a new level of connection and understanding.  
                        Whether you are in a budding relationship or one that has withstood the test of time, a couple’s reading will help 
                        you clearly understand what each person needs in order to feel valued and understood within the parameters of the 
                        relationship, offering insight into the ways in which the foundational dynamics and energy of the relationship can 
                        be utilized to bring lasting and unconditional love.  By opening the channels of communication through intuitive 
                        guidance you will be able to see your partner in a new light… allowing your relationship to rise to heightened levels 
                        of spiritual intimacy.       
                    </span>  
                </div>

                <div class="phar no-padding">   
                    <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>  
                    <div class="size16 border-left italic margin-left margin-top">
                        "This was a unique experience!" <br/> <br/>  
                        <div class="text-right">- Anonymous</div>        
                    </div>
                </div>

            </div>

            <div class="col-sm-4">
                <!-- ===== SIDE BAR 1 ===== -->
                <div class="phar center no-padding sidebar1">
                    <hr class="styled-hr">
                    <h2>CHAKRA BALANCING AND CRYSTAL HEALING</h2>
                    <h3 class="grayfont">Price: $150 (60 min)</h3>
                    <div class="button">
                        <a href="<?php echo $base_url;?>/booking" class="learn-sedona">BOOK ONLINE NOW</a>
                    </div><br/>
                    <div class="center">
                        <span class="size20">
                        - OR -<br/>
                        </span>
                        <span class="size20 bold">
                        Call 928-282-3875
                        </span>
                        <span class="size20"><br/>
                        to make an appointment<br/> <br/>
                        </span>
                    </div>
                    <hr class="styled-hr">
                </div>
                <!-- ===== END SIDE BAR 1 ===== -->
                

                <!-- ===== SIDE BAR 2 ===== -->
                <?php echo $sidebar;?>
                <!-- ===== END SIDE BAR 2 ===== -->
            </div>
        </div>
    </div>



    <div class="container margin-bot100 margin-top80">
        <hr class="styled-hr">
        <div class="row padding-topbot20">
            <div class="col-sm-12 center">
                <span class="size25 font2">Call 928-282-3875 or <a href="">CLICK HERE</a> to make an appointment.</span>
            </div>
        </div>
        <hr class="styled-hr">
    </div>

    <!-- Below Banner ends -->

