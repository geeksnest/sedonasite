
    <!-- Banner starts -->
    <div class="banner-container retreats-bg workshops-crystal-palace">
        <div class="black-box">
            <span class="banner-title">Crystal Palace Workshop</span>
            <br/>
            <span class="banner-sub-title1">The True Spirit of Sedona</span>
            <br/><br/>
            <div class="button"><a href="<?php echo $base_url;?>/booking" class="learn-sedona">SCHEDULE YOUR WORKSHOP</a></div>
        </div>
    </div>

    <!-- Banner ends -->


    <div class="container">
        <div class="row">

            <div class="col-sm-8 no-margin content-text">

                <div class="phar no-padding">
                    <span class="size20"><h2>CRYSTAL PALACE - VISIT AND & GUIDED MEDITATION</h2></span>
                      <p class="size18">
                          The Crystal Palace Workshop will allow you to experience the most mystical, awe-inspiring, and sacred energy in all of Sedona.  Crystals are the purest form of the Earth’s original essence. Taking hundreds, even thousands of years to develop, they carry the deepest and most profound energy, wisdom, and secrets of life. When you connect with crystals, forged directly from the earth, you are also connecting with the essential and core energy of the Earth.
                      </p>
                    <br/>
                    <p class="size18">
                        The sacred crystals chosen to be a part of the Crystal Palace experience are not only rare and precious but they also carry an indescribable, mysterious, and powerful energy unique to each crystal. The Crystal Palace represents the place where the soul of Mother Earth resides, and therefore connects you with her abundant nurturing heart. Many of the crystals in the Crystal Palace--including one of the world’s rarest known Aquamarine crystals --is something you won’t witness anywhere else. This Aquamarine crystal has grown within the Earth’s crust for thousands of years and is known as the Soul of Mago, Mother Earth.  This is a once in a life time opportunity that promises to be a truly unforgettable experience that will awaken the truest essence of who you are at the deepest and most sacred level of your being.
                    </p>
                    <span class="size20"><h2>Through the Crystal Palace Worckshop You will Experience:</h2></span>
                    <p>
                    <ul style="">
                        <li><span style="background-color: initial;">Opening of the energy channels within your body.</span></li>
                        <li><span style="background-color: initial;">Transportation to the energetic and spiritual realm beyond the 5 senses.</span></li>
                        <li><span style="background-color: initial;">An expansion of consciousness that will enable you to become one with the Universe.</span></li>
                        <li><span style="background-color: initial;">Connection and guidance to Mother Earth so that you’ll be open to receive her message for you.</span></li>
                        <li><span style="background-color: initial;">The clarity to see your life’s path and mission.</span></li>
                        <li><span style="background-color: initial;">Manifestation of your optimal state of well-being.</span></li></ul>
                    </p>
                </div>

                <div class="phar no-padding">
                    <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                    <div class="size16 border-left italic margin-left margin-top">
                        "This was a unique experience!" <br/> <br/>
                        <div class="text-right">- Anonymous</div>
                    </div>

                </div>

            </div>

            <div class="col-sm-4 no-padding">
                <!-- ===== SIDE BAR 1 ===== -->
                <div class="phar center no-padding sidebar1">
                    <hr class="styled-hr">
                    <h2>CRYSTAL PALACE - VISIT AND & GUIDED MEDITATION</h2>
                    <h3 class="grayfont">Cost: $300 (120 min)</h3>
                                <div class="button"><a href="<?php echo $base_url;?>/booking" class="learn-sedona">BOOK ONLINE NOW</a></div><br/>
                                <div class="center">
                        <span class="size20">
                          - OR -<br/>
                        </span>
                        <span class="size20 bold">
                          Call 928-282-3875
                        </span>
                        <span class="size20"><br/>
                          to make an appointment<br/> <br/>
                        </span>
                    </div>
                    <hr class="styled-hr">
                </div>
                <!-- ===== END SIDE BAR 1 ===== -->

                <!-- ===== SIDE BAR 2 ===== -->
                <?php echo $sidebar;?>
                <!-- ===== END SIDE BAR 2 ===== -->

            </div>
        </div>
    </div>



    <div class="container margin-bot100 margin-top80">
        <hr class="styled-hr">
        <div class="row padding-topbot20">
            <div class="col-sm-12 center">
                <span class="size25 font2">Call 928-282-3875 or <a href="">CLICK HERE</a> to make an appointment.</span>
            </div>
        </div>
        <hr class="styled-hr">
    </div>

    <!-- Below Banner ends -->