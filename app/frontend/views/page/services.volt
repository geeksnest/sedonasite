<!-- Below SlidEr starts -->
     <div class="container-fluid1">
        <div id="myCarousel" class="carousel slide mycarousel" data-ride='carousel'>

<!-- Indicators -->
            <ol class="carousel-indicators" style="margin-bottom:-10px">
            <?php
            $nslides = json_decode($slides);
            $dst = 0;
            foreach ($nslides as $key => $value) {
            ?>
                <li data-target="#myCarousel" data-slide-to="<?php echo $dst; ?>"
                 class="<?php if($nslides[$key]->sort == 1){ echo 'active'; } ?>"></li>
            <?php $dst++; } ?>
            </ol>

<!-- Wrapper for Slides -->
            <div class="carousel-inner carousel-wrap">

            <?php
            foreach ($nslides as $key => $value) {
                ?>
                <div class="item <?php if($nslides[$key]->sort == 1){ echo 'active'; } ?>">
                    <div class="sidebarimage" style="background-image: url(<?php echo $this->config->application->amazonlink; ?>/uploads/slider/<?php echo $nslides[$key]->foldername.'/'.$nslides[$key]->img; ?>);">
                    </div>

                        <div class="banner-container banner-content-wrapper-green">
                        <div class='containerlang'>
                          <?php if($nslides[$key]->display=='yes') { ?>
                            <?php if($nslides[$key]->position=='right') { ?>
                            <div class="col-sm-7 col-sm-offset-5 slider-right slider slider<?php echo $key; ?>" style='padding:6vw'> <!-- RIGHT ALIGN-->
                            <?php if($nslides[$key]->box=='on') { ?>
                              <div class="Slider-content" > <!-- BOX ON-->

                                  <!--ARROW!-->
                               <div class='row'>
                            <div class='pull-left click1'>
                                <span class="slider-title" style="color:<?php echo $nslides[$key]->titlecolor; ?>;font-size:<?php $size=$nslides[$key]->titlesize/16; echo $size; ?>vw;line-height:<?php if ($nslides[$key]->titlesize>=90) {echo '6';}?>vw "><?php echo $nslides[$key]->title; ?></span>
                                </div>

                                <div class='pull-right' style='color:black;text-align:center;width:50px'> <a class='btn-lg clickme clickme<?php echo $key; ?>' id='clickme' onclick='slide(<?php echo $key; ?>)'>  <i class='fa fa-arrow-circle-down'> </i> </a>
                                 <a class='clickshow clickshow<?php echo $key; ?>'id='clickshow' onclick='hide(<?php echo $key; ?>)'>
                           <i class='fa fa-arrow-circle-up'> </i> </a>
                            </div>
                            <!-- Overflow Auto-->
                            </div>

                                <br>
                                <span class="slider-desc slider-desc<?php echo $key; ?>" style="color:<?php echo $nslides[$key]->desccolor; ?>;font-size:<?php $size=$nslides[$key]->descsize/16; echo $size; ?>vw;">
                                  <?php echo strip_tags($nslides[$key]->description); ?>
                              </span>

                             </div>
                            <?php } else {?>
                            <div class="Services-outcontent"> <!-- BOX OFF-->
                            <!-- TITLE -->
                                 <!--ARROW!-->
                               <div class='row'>
                            <div class='pull-left click1'>
                                <span class="slider-title" style="color:<?php echo $nslides[$key]->titlecolor; ?>;font-size:<?php $size=$nslides[$key]->titlesize/16; echo $size; ?>vw;line-height:<?php if ($nslides[$key]->titlesize>=90) {echo '6';}?>vw "><?php echo $nslides[$key]->title; ?></span>
                                </div>
                                <div class='pull-right' style='color:black;text-align:center;width:50px'> <a class='btn-lg clickme clickme<?php echo $key; ?>' id='clickme' onclick='slide(<?php echo $key; ?>)'>  <i class='fa fa-arrow-circle-down'> </i> </a>
                                 <a class='clickshow clickshow<?php echo $key; ?>'id='clickshow' onclick='hide(<?php echo $key; ?>)'>
                           <i class='fa fa-arrow-circle-up'> </i> </a>
                            </div>
                            <!-- Overflow Auto-->
                            </div>


                                <br>
                                <span class="slider-desc slider-desc<?php echo $key; ?>" style="color:<?php echo $nslides[$key]->desccolor; ?>;font-size:<?php $size=$nslides[$key]->descsize/16; echo $size; ?>vw">
                                  <?php echo strip_tags($nslides[$key]->description); ?>
                              </span>
                             </div>
                             <?php } ?>


                      </div>

                      <?php } if($nslides[$key]->position=='center') { ?>
                            <div class="slider-center slider slider<?php echo $key; ?>" style='padding:6vw'> <!-- Center ALIGN-->

                            <?php if($nslides[$key]->box=='on') { ?>
                              <div class="Slider-content" > <!-- BOX ON-->

                                <!--ARROW!-->
                               <div class='row'>
                            <div class='pull-left  click1'>
                                <span class="slider-title" style="color:<?php echo $nslides[$key]->titlecolor; ?>;font-size:<?php $size=$nslides[$key]->titlesize/16; echo $size; ?>vw;line-height:<?php if ($nslides[$key]->titlesize>=90) {echo '6';}?>vw "><?php echo $nslides[$key]->title; ?></span>
                                </div>
                                <div class='pull-right' style='color:black;text-align:center;width:50px'> <a class='btn-lg clickme clickme<?php echo $key; ?>' id='clickme' onclick='slide(<?php echo $key; ?>)'>  <i class='fa fa-arrow-circle-down'> </i> </a>
                                 <a class='clickshow clickshowcenter clickshow<?php echo $key; ?>'id='clickshow' onclick='hide(<?php echo $key; ?>)'>
                           <i class='fa fa-arrow-circle-up'> </i> </a>
                            </div>
                            <!-- Overflow Auto-->
                            </div>

                                <br>
                                <span class="slider-desc slider-desc<?php echo $key; ?>" style="color:<?php echo $nslides[$key]->desccolor; ?>;font-size:<?php $size=$nslides[$key]->descsize/16; echo $size; ?>vw">
                                  <?php echo strip_tags($nslides[$key]->description); ?>
                              </span>
                             </div>
                            <?php } else {?>
                            <div class="Services-outcontent"> <!-- BOX OFF-->
                            <!-- TITLE -->
                                   <!--ARROW!-->
                                 <div class='row'>
                            <div class='pull-left  click1'>
                                <span class="slider-title" style="color:<?php echo $nslides[$key]->titlecolor; ?>;font-size:<?php $size=$nslides[$key]->titlesize/16; echo $size; ?>vw;line-height:<?php if ($nslides[$key]->titlesize>=90) {echo '6';}?>vw "><?php echo $nslides[$key]->title; ?></span>
                                </div>
                                <div class='pull-right' style='color:black;text-align:center;width:50px'> <a class='btn-lg clickme clickme<?php echo $key; ?>' id='clickme' onclick='slide(<?php echo $key; ?>)'>  <i class='fa fa-arrow-circle-down'> </i> </a>
                                 <a class='clickshow clickshowcenter clickshow<?php echo $key; ?>'id='clickshow' onclick='hide(<?php echo $key; ?>)'>
                           <i class='fa fa-arrow-circle-up'> </i> </a>
                            </div>
                            <!-- Overflow Auto-->
                            </div>

                                <br>
                                <span class="slider-desc slider-desc<?php echo $key; ?>" style="color:<?php echo $nslides[$key]->desccolor; ?>;font-size:<?php $size=$nslides[$key]->descsize/16; echo $size; ?>vw">
                                  <?php echo strip_tags($nslides[$key]->description); ?>
                              </span>
                             </div>
                             <?php } ?>


                      </div>
                      <?php } else if ($nslides[$key]->position=='left') { ?>
                      <div class="col-sm-7 slider-left slider slider<?php echo $key; ?>" style='padding:6vw' id='slider-left'> <!-- LEFT ALIGN-->
                         <?php if($nslides[$key]->box=='on') { ?>
                              <div class="Slider-content"> <!-- BOX ON-->

                            <!--ARROW!-->
                               <div class='row'>
                            <div class='pull-left  click1'>
                                <span class="slider-title" style="color:<?php echo $nslides[$key]->titlecolor; ?>;font-size:<?php $size=$nslides[$key]->titlesize/16; echo $size; ?>vw;line-height:<?php if ($nslides[$key]->titlesize>=90) {echo '6';}?>vw "><?php echo $nslides[$key]->title; ?></span>
                                </div>
                                <div class='pull-right' style='color:black;text-align:center;width:50px'> <a class='btn-lg clickme clickme<?php echo $key; ?>' id='clickme' onclick='slide(<?php echo $key; ?>)'>  <i class='fa fa-arrow-circle-down'> </i> </a>
                                 <a class='clickshow clickshow<?php echo $key; ?>'id='clickshow' onclick='hide(<?php echo $key; ?>)'>
                           <i class='fa fa-arrow-circle-up'> </i> </a>
                            </div>
                            <!-- Overflow Auto-->
                            </div>

                                <br>
                                <span class="slider-desc slider-desc<?php echo $key; ?>" style="color:<?php echo $nslides[$key]->desccolor; ?>;font-size:<?php $size=$nslides[$key]->descsize/16; echo $size; ?>vw">
                                  <?php echo strip_tags($nslides[$key]->description); ?>
                              </span>

                             </div>
                            <?php } else {?>
                            <div class="Services-outcontent"> <!-- BOX OFF-->

                            <div class='row'>
                            <div class='pull-left click1' stye='border:1px solid black'>
                                <span class="slider-title" style="color:<?php echo $nslides[$key]->titlecolor; ?>;font-size:<?php $size=$nslides[$key]->titlesize/16; echo $size; ?>vw;line-height:<?php if ($nslides[$key]->titlesize>=90) {echo '6';}?>vw "><?php echo $nslides[$key]->title; ?></span>
                                </div>
                                <div class='pull-right click' style='width:5vw'> <a class='clickme clickme<?php echo $key; ?>'
                                 id='clickme' onclick='slide(<?php echo $key; ?>)'>  <i class='fa fa-arrow-circle-down'> </i> </a>
                                <a class='clickshow clickshow<?php echo $key; ?>' id='clickshow' onclick='hide(<?php echo $key; ?>)'>
                           <i class='fa fa-arrow-circle-up'> </i> </a>

                            </div>
                            </div>

                                <br>
                                <span class="slider-desc slider-desc<?php echo $key; ?>" style="color:<?php echo $nslides[$key]->desccolor; ?>;font-size:<?php $size=$nslides[$key]->descsize/16; echo $size; ?>vw">
                                  <?php echo strip_tags($nslides[$key]->description); ?>
                              </span>


                             </div>
                             <?php } ?>
                  </div>
                  <?php } }?>

                  </div>

                </div>

                </div>

            <?php } ?>
            </div>

<!-- Controls -->

            <a class="left-arrow left carousel-control" href="#myCarousel" data-slide="prev" style='height:41vw' >
                <span class="icon-prev"></span>
            </a>
            <a class="right-arrow right carousel-control" href="#myCarousel" data-slide="next" style='height:41vw'>
                <span class="icon-next"></span>
            </a>
        </div>
<!-- slider ends -->


    </div>

<?php
      $getginfo = $acupuncture;
      foreach ($getginfo as $key => $value) {
?>
<div class="container-fluid Services">
    <div class="Services-bg" style="background:linear-gradient( rgba(0, 0, 0, 0), rgba(0, 0, 0, .8) ), url('<?php echo $imageLink.'/uploads/pageimage/'. $getginfo[$key]->imagethumb  ?>')">
      {#this is the banner#}
    </div>

    <div class="Services-body <?php echo $getginfo[$key]->classname; ?>">
        <?php if($getginfo[$key]->textalign == 'right') { ?>  {#RIGHT ALIGN#}
                  <div class="col-sm-6 col-sm-offset-6 Services-content" style="color:<?php echo $getginfo[$key]->color; ?>">

                    <?php  if($getginfo[$key]->box == 'true') { ?>
                      <div class="Services-box">
                    <?php } else { ?>
                      <div>
                    <?php }

              } elseif ($getginfo[$key]->textalign == 'left') { ?> {#LEFT ALIGN#}
                  <div class="col-sm-6 Services-content" style="color:<?php echo $getginfo[$key]->color; ?>">

                    <?php  if($getginfo[$key]->box == 'true') { ?>
                      <div class="Services-box">
                    <?php } else { ?>
                      <div>
                    <?php }

              } else { ?> {#CENTER ALIGN#}
                  <div class="col-sm-6 col-sm-offset-3 Services-content" style="color:<?php echo $getginfo[$key]->color; ?>">

                    <?php  if($getginfo[$key]->box == 'true') { ?>
                      <div class="Services-box">
                    <?php } else { ?>
                      <div>
                    <?php }
              }  ?>

              {#continuation of the Services-content div#}
              <span class="Services-title"><?php echo $getginfo[$key]->imagethumbsubtitle; ?></span>
              <span class="Services-description">
                <?php echo strip_tags($getginfo[$key]->thumbdesc); ?>
                <?php echo $color[$key]; ?>
                <a href="<?php echo $base_url;?>/acupunture/<?php echo $getginfo[$key]->pageslugs;?>" class="Services-learnmore">Learn More</a>
              </span>
            </div> {#end of Services-box#}
          </div> {#end of Services-content#}
    </div> {# end of body #}
</div> {# END OF CONTAINER-FLUID #}
        <style>
          @media screen and (max-width:767px) {
            .<?php echo $getginfo[$key]->classname; ?> {
              background:<?php echo $getginfo[$key]->bgcolor; ?>;
            }
          }
        </style>
<?php } ?> {#end of foreach#}
