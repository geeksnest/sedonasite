

    <!-- Banner starts -->
   <!--  <div class="banner-container healing-bg">

    </div> -->

    <!-- Banner ends -->


    <div class="container" ng-controller="ReservationCTRL" id="booking" >


        <script type="text/ng-template" id="addscheduleModal.html">
            <div ng-include="'/fe/tpl/addscheduleModal.html'"></div>
        </script>

        <script type="text/ng-template" id="userprofileModal.html">
            <div ng-include="'/fe/tpl/userprofileModal.html'"></div>
        </script>

        <div class="row">
            <!--      Wizard container        -->

            <div class="wizard-container">
                <?php
                                                echo $notification;
                                            ?>
                    <div class="card wizard-card ct-wizard-azzure" id="wizard">

                        <!--        You can switch "ct-wizard-azzure"  with one of the next bright colors: "ct-wizard-blue", "ct-wizard-green", "ct-wizard-orange", "ct-wizard-red"             -->
                        <div class="wizard-header">
                            <h3>
                                Online Sedona Healing Arts Services Reservation <br>
                                <small ng-show="loginname !=''">You are login as <a href="" ng-click="userprofile()">  <icon class="fa fa-user"> </icon>  {[{ loginname  }]} </a>  <a href="" ng-click="logoutmember()"> <icon class="fa fa-sign-out"> </icon>  Logout </a> </small>
                            </h3>
                        </div>
                        <ul>
                            <li><a href="#captain" data-toggle="tab">Schedules and Services</a></li>
                            <li><a href="#details" data-toggle="tab">Account Registration / Login</a></li>
                            <li><a href="#description" data-toggle="tab">Payment Mode</a></li>
                        </ul>

                        <div class="clearfix m-b text-center text-middle" style='margin-top:2%'>
                         <span style='font-weight:bold;font-size:18px'>
                             <icon class="fa fa-circle" style="color: #3a87ad;"></icon>
                              blue listed times are already reserved </span>
                         </div>

                        <div class="tab-content">
                            <div class="tab-pane" id="captain">
                                <div class="clearfix m-b">
                                    <div class="pull-right">
                                        <button type="button" class="btn btn-sm btn-default" ng-click="today('calendar1')">today</button>
                                        <div class="btn-group m-l-xs">
                                            <button class="btn btn-sm btn-default" ng-click="changeView('agendaDay', 'calendar1')">Day</button>
                                            <button class="btn btn-sm btn-default" ng-click="changeView('agendaWeek', 'calendar1')">Week</button>
                                            <button class="btn btn-sm btn-default" ng-click="changeView('month', 'calendar1')">Month</button>
                                        </div>
                                    </div>
                                </div>

                            <div class="pos-rlt">

                        <div class="calendar" ng-model="eventSources" calendar="calendar1" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div>

                            </div>

                                <br><br>
                                <div class="panel panel-default" ng-show="member.reservationlist.length > 0">
                                    <div class="panel-heading">
                                        Reservation Lists
                                        <input type="hidden" name="reservationlist" ng-model="member.reservationlist" required="required">
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-striped b-t b-light">
                                            <thead>
                                            <tr>
                                                <th>Description</th>
                                                <th>Amount</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr ng-repeat="list in member.reservationlist">
                                                <td>
                                                    {[{ list.service.title }]}
                                                    <p class="text-muted">{[{ list.servicechoice.description }]} <br> <strong>{[{ list.servicechoice.hourday }]} ({[{ list.text }]})</strong></p>
                                                </td>
                                                <td ng-init="sum = sum + list.servicechoice.price">
                                                    ${[{ list.servicechoice.price }]}
                                                </td>
                                                <td style="width: 10px;">
                                                    <a href="" ng-click="deleteService($index, list.startdatetime)" class="active"><i class="fa fa-times text-danger text-active" alt="delete">Delete</i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right"> Total </td>
                                                <td colspan="2" class="text-lefth">${[{ getTotal() }]}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="details">
                                <div class="row">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-6" ng-show="!login">

                                        <h3 class="info-text"> Register <br> <small>New user? You need to register first. <br> <a href="" ng-click="login=true; forgotpassword=true">Forgot Password? </a> or <a href="" ng-click="login=true; resendconfirmation=true"> Resend Email Confirmation? </a></small></h3>
                                        <!--<div  class="col-sm-4 social-login-icons text-right">-->
                                            <!--<a href="" ng-click="facebooklogin()"><img src="/img/frontend/socialmedia/fb.png" ></a>-->
                                        <!--</div>-->
                                        <!--<div class="col-sm-4 social-login-icons text-center">-->
                                            <!--<img src="/img/frontend/socialmedia/twitter.png">-->
                                        <!--</div>-->
                                        <!--<div  class="col-sm-4 social-login-icons text-left">-->
                                            <!--<img src="/img/frontend/socialmedia/google.png">-->
                                        <!--</div>-->
                                        <!--<div class="col-sm-4"><hr></div>-->
                                        <!--<div class="col-sm-4 text-center"> OR </div>-->
                                        <!--<div class="col-sm-4"><hr></div>-->
                                        <div class="col-sm-12">
                                            <form name="registration" class="form-reservation">
                                                <fieldset ng-disabled="saving">
                                            <div class="form-group">
                                                <label>Email: </label>
                                                <input type="email" ng-model="register.email" class="form-control text-box" id="email" placeholder="Enter your Email." required="required" ng-blur="checkEmail(register.email)">
                                                <label ng-show="emailexist==1"> This email already belongs to someone. Do you want to use this and login? <a href="" ng-click="login=true; ll.loginemailusername = register.email; register.email = ''; emailexist=0; "> Yes </a> </label>
                                                <span ng-show="sendingcompletionlinksuccess">Please check your email for instructions.</span>
                                                <span ng-show="sendingcompletionlink">Sending Completion Link...</span>
                                                <label  ng-show="emailexist==3 && !sendingcompletionlink"> We have detected that this email have been registered offline, click yes to receive the link to complete your profile? <a href="" ng-click="sendUserCompletionLink(register.email); emailexist=0;"> Yes </a> </label>
                                            </div>
                                            <div class="form-group">
                                                <label>Username:</label>
                                                <input type="text" ng-model="register.username" name="username" class="form-control text-box" id="username" placeholder="Enter your Username." required="required" ng-pattern="/^[a-zA-Z0-9]*$/">
                                                <span class="help-block" ng-show="registration.username.$error.pattern">Alphanumeric characters only.</span>
                                            </div>
                                            <div class="form-group">
                                                <label>Password:</label>
                                                <input type="password" ng-model="register.password" name="password" class="form-control text-box" id="password" placeholder="Enter your Password." pass-strength required="required" ng-minlength="6" >
                                                <span class="help-block" ng-if="registration.password.$error.minlength">Minimum of 6 characters.</span>
                                            </div>
                                            <div class="form-group">
                                                <label>Retype Password:</label>
                                                <input type="password" ng-model="register.repassword" class="form-control text-box" id="repassword" name="repassword" placeholder="Retype your Password." required="required" pw-check="password">
                                                  <span class="msg-error" ng-show="registration.repassword.$error.pwmatch">
                                                    Passwords don't match.
                                                  </span>
                                            </div>
                                            <div class="form-group">
                                                <label>Lastname:</label>
                                                <input type="text" ng-model="register.lastname" class="form-control text-box" id="lastname" placeholder="Enter your Lastname." required="required">
                                            </div>
                                            <div class="form-group">
                                                <label>Firstname:</label>
                                                <input type="text" ng-model="register.firstname" class="form-control text-box" id="firstname" placeholder="Enter your Firstname." required="required">
                                            </div>
                                            <div class="form-group">
                                                <label>Address 1:</label>
                                                <input type="text" ng-model="register.address1" class="form-control text-box" id="address1" placeholder="Enter your Address." required="required">
                                            </div>
                                            <div class="form-group">
                                                <label>Address 2:</label>
                                                <input type="text" ng-model="register.address2" class="form-control text-box" id="address2" placeholder="Enter your Address.">
                                            </div>
                                            <div class="form-group">
                                                <label>Country:</label>
                                                <select onchange="print_state('regstate', this.selectedIndex);" ng-change="register.state=''" id="regcountry" name="regcountry" ng-model="register.country" class="form-control" required="required">
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>State:</label>
                                                <select name="regstate" id ="regstate" class="form-control" ng-model="register.state" required="required">
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Zip Code:</label>
                                                <input type="text" ng-model="register.zipcode" class="form-control text-box" id="zipcode" placeholder="Enter your Zipcode." required="required" only-digits>
                                            </div>
                                            <div class="form-group">
                                                <label>Phone No:</label>
                                                <input type="text" ng-model="register.phoneno" class="form-control text-box" id="phoneno" placeholder="Enter your phoneno." required="required" only-digits>
                                            </div>
                                            <div class="form-group">
                                                <input type='button' class='btn btn-fill btn-info btn-wd btn-sm form-control' name='next' value='Register' ng-disabled="registration.$invalid" ng-click="registerUser(register, registration)" />
                                            </div>
                                            <div class="form-group">
                                                <input type='button' class='btn btn-fill btn-warning btn-wd btn-sm form-control' name='signin' value='Login' ng-click="login=true" />
                                            </div>
                                                    </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-sm-6" ng-show="login">
                                        <div ng-show="resendconfirmation">
                                            <h3 class="info-text"> Resend Confirmation Email. <br> <small>Please enter your email below. <br> </small></h3>
                                            <div class="col-sm-12">
                                                <form name="resendemailform" class="form-login">
                                                    <fieldset ng-disabled="requestingresend">
                                                        <div class="form-group">
                                                            <label>Email:</label>
                                                            <input type="email" ng-model="resend.email" class="form-control text-box" id="resendemail" placeholder="Enter your Email." required="required">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type='button' class='btn btn-fill btn-info btn-wd btn-sm form-control' name='forgot' ng-disabled="resendemailform.$invalid" ng-click="resendemail(resend)" value='Submit' />
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <input type='button' class='btn btn-fill btn-warning btn-wd btn-sm form-control' name='backtologin' value='Back to Login' ng-click="login=true; resendconfirmation=false" />
                                                </div>
                                            </div>
                                        </div>
                                        <div ng-show="forgotpassword">
                                            <h3 class="info-text"> Forgot Password <br> <small>Please enter your email below. <br> </small></h3>
                                            <div class="col-sm-12">
                                                <form name="forgotpasswordform" class="form-login">
                                                    <fieldset ng-disabled="requestingreset">
                                                    <div class="form-group">
                                                        <label>Email:</label>
                                                        <input type="email" ng-model="forgot.email" class="form-control text-box" id="forgotemail" placeholder="Enter your Email." required="required">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type='button' class='btn btn-fill btn-info btn-wd btn-sm form-control' name='forgot' ng-disabled="forgotpasswordform.$invalid" ng-click="requestpassword(forgot)" value='Submit' />
                                                    </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <input type='button' class='btn btn-fill btn-warning btn-wd btn-sm form-control' name='backtologin' value='Back to Login' ng-click="login=true; forgotpassword=false" />
                                                </div>
                                            </div>
                                        </div>
                                        <div ng-show="!forgotpassword && !resendconfirmation">
                                            <h3 class="info-text"> Login <br> <small>Already a member? Login your credentials. <br> <a href="" ng-click="forgotpassword=true"> Forgot Password? </a> or <a href="" ng-click="resendconfirmation=true"> Resend Email Confirmation? </a></small></h3>
                                            <!--<div  class="col-sm-4 social-login-icons text-right">-->
                                                <!--<a href="" ng-click="facebooklogin()"><img src="/img/frontend/socialmedia/fb.png" ></a>-->
                                            <!--</div>-->
                                            <!--<div class="col-sm-4 social-login-icons text-center">-->
                                                <!--<img src="/img/frontend/socialmedia/twitter.png">-->
                                            <!--</div>-->
                                            <!--<div  class="col-sm-4 social-login-icons text-left">-->
                                                <!--<img src="/img/frontend/socialmedia/google.png">-->
                                            <!--</div>-->
                                            <!--<div class="col-sm-4"><hr></div>-->
                                            <!--<div class="col-sm-4 text-center"> OR </div>-->
                                            <!--<div class="col-sm-4"><hr></div>-->
                                            <div class="col-sm-12">
                                                <form name="loginform" class="form-login">
                                                    <div class="form-group">
                                                        <label>Email:</label>
                                                        <input type="email" ng-model="ll.loginemailusername" class="form-control text-box" id="loginemailusername" placeholder="Enter your Email." required="required" ng-blur="checkEmail(ll.loginemailusername)">
                                                        <span ng-show="sendingcompletionlinksuccess">Please check your email for instructions.</span>
                                                        <span ng-show="sendingcompletionlink">Sending Completion Link...</span>
                                                        <label  ng-show="emailexist==3 && !sendingcompletionlink"> We have detected that this email have been registered offline, click yes to receive the link to complete your profile? <a href="" ng-click="sendUserCompletionLink(ll.loginemailusername); emailexist=0;"> Yes </a> </label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Password:</label>
                                                        <input type="password" ng-model="ll.loginpassword" class="form-control text-box" id="loginpassword" placeholder="Enter your Password." required="required">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type='button' class='btn btn-fill btn-info btn-wd btn-sm form-control' name='login' ng-disabled="loginform.$invalid" ng-click="loginbutton(ll)" value='Sign In' />
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="col-sm-12">
                                                <h4 class="text-center">
                                                    <small>If you are not yet registered, click the signup button bellow.</small>
                                                </h4>
                                                <div class="form-group">
                                                    <input type='button' class='btn btn-fill btn-warning btn-wd btn-sm form-control' name='signup' value='Sign Up' ng-click="login=false" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                            </div>
                            <div class="tab-pane" id="description">
                                <div class="row" ng-if="paymentsuccess" >
                                    <h4 class="info-text">Thank you <label class="bg-info">{[{ loginname }]}</label> for subscribing to our services! <br> An email has been sent to you with your receipt and reference number, if you paid using Paypal you may print the paypal invoice page.
                                    </h4>
                                    <div class="col-sm-3">

                                    </div>
                                    <div class="col-sm-6">
                                        <a href="" ng-click="bookanother()" class='btn btn-fill btn-info btn-wd btn-sm form-control' > Do you want to book another? </a>
                                    </div>
                                    <div class="col-sm-3">

                                    </div>
                                </div>
                                <div class="row" ng-if="!paymentsuccess">

                                    <h4 class="info-text"> Payment Gateway <br><small>Bellow is the list of your reservation and the total.</small></h4>
                                    <div class="table-responsive">
                                        <table class="table table-striped b-t b-light">
                                            <thead>
                                            <tr>
                                                <th>Description</th>
                                                <th>Amount</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr ng-repeat="list in member.reservationlist">
                                                <td>
                                                    {[{ list.service.title }]}
                                                    <p class="text-muted">{[{ list.servicechoice.description }]} <br> <strong>{[{ list.servicechoice.hourday }]} ({[{ list.text }]})</strong></p>
                                                </td>
                                                <td ng-init="sum = sum + list.servicechoice.price">
                                                    ${[{ list.servicechoice.price }]}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right"> Total </td>
                                                <td class="text-lefth">${[{ getTotal() }]}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <h4 class="info-text"><small>Select your payment method.</small></h4>
                                    <div class="col-sm-12">
                                        <div class="col-sm-1 payment-gateway text-center"></div>
                                        <div  class="col-sm-2 payment-gateway text-center">
                                        </div>
                                        <div  class="col-sm-2 payment-gateway text-center" ng-class=" { 'payment-gateway-selected' : paymenttype=='paypal'}">
                                            <a href="" ng-click="paymenttype='paypal'">
                                                <img src="/img/paymentgateway/paypal.png" >
                                                <h4>Paypal</h4>
                                            </a>
                                        </div>
                                        <div class="col-sm-2 payment-gateway text-center" ng-class=" { 'payment-gateway-selected' : paymenttype=='card'}">
                                            <a href="" ng-click="paymenttype='card'">
                                                <img src="/img/paymentgateway/creditcard.png">
                                                <h4>Credit Card</h4>
                                            </a>
                                        </div>
                                        <div  class="col-sm-2 payment-gateway text-center" ng-class=" { 'payment-gateway-selected' : paymenttype=='check'}">
                                            <a href="" ng-click="paymenttype='check'">
                                                <img src="/img/paymentgateway/cheque.png">
                                                <h4>E-Check</h4>
                                            </a>
                                        </div>
                                        <div  class="col-sm-2 payment-gateway text-center">
                                        </div>
                                        <div class="col-sm-1 payment-gateway text-center"></div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-3"></div>

                                            <!-- CREDIT CARD PAYMENT -->
                                            <div class="col-sm-6" ng-if="paymenttype=='card'">
                                                <form class="bs-example form-horizontal" name="formcard" novalidate>
                                                    <fieldset ng-disabled="paymentprocessing">
                                                <div>
                                                    <br/>
                                                    <div class="form-group">
                                                            <img src="/img/creditcards/amex.png" width="49px" height="29px">
                                                            <img src="/img/creditcards/discover.png" width="49px" height="29px">
                                                            <img src="/img/creditcards/mastercard.png" width="49px" height="29px">
                                                            <img src="/img/creditcards/visa.png" width="49px" height="29px">
                                                    </div>
                                                    <div class="form-group" >
                                                        <label>
                                                            Credit Card Number: *
                                                        </label>
                                                            <input class="form-control" type="text" ng-model="userccreg.ccn" name="" required only-digits />
                                                    </div>
                                                    <div class="form-group row" >
                                                        <label>
                                                            CVV Number: *
                                                        </label>
                                                            <input type="text" class="form-control " ng-model="userccreg.cvvn" name="" required only-digits />
                                                    </div>
                                                    <div class="form-group" >
                                                        <label>
                                                            Credit Card Expiration: *
                                                        </label>
                                                    </div>
                                                    <div class="form-group" >
                                                                <div class="col-xs-6 no-padding">
                                                                    <span>Month</span><br/>
                                                                    <select ng-model="userccreg.expiremonth" class="form-control" ng-options="m.val as m.name for m in months" required></select>
                                                                </div>
                                                                <div class="col-xs-5 ">
                                                                    <span>Year</span><br/>
                                                                    <select ng-model="userccreg.expireyear" class="form-control" ng-options="y.val as y.val for y in years" required></select>
                                                                </div>

                                                    </div>
                                                    <div class="form-group" >
                                                        <span style="font-weight:bold;">Billing Information</span>
                                                        <input type="checkbox"  ng-model="useyouraddress" name="useyouraddress" value="1" ng-required="true" ng-click="getcurrentaddress(useyouraddress, paymenttype)"> Use your current address.
                                                    </div>
                                                    <div>
                                                        <div class="form-group " >
                                                            <label>
                                                                First Name: *
                                                            </label>
                                                                <input class="form-control " type="text" ng-model="userccreg.billingfname" name="billingfname" value="{[{ user.firstname }]} " required/>
                                                        </div>
                                                        <div class="form-group " >
                                                            <label>
                                                                Last Name: *
                                                            </label>
                                                                <input class="form-control " type="text" ng-model="userccreg.billinglname" name="billinglname" value="{[{ user.lastname }]}" required/>
                                                        </div>
                                                        <div class="form-group ">
                                                            <label>
                                                                Address Line 1: *
                                                            </label>
                                                                <input class="form-control " type="text" ng-model="userccreg.al1" name="al1" required value="{[{ user.address1 }]}" />
                                                        </div>
                                                        <div class="form-group ">
                                                            <label>
                                                                Address Line 2:
                                                            </label>
                                                                <input class="form-control " type="text" ng-model="userccreg.al2" name="al2" value="{[{ user.address2 }]}" />
                                                        </div>
                                                        <div class="form-group ">
                                                            <label>
                                                                City: *
                                                            </label>
                                                                <input class="form-control " type="text" ng-model="userccreg.city" name="city" required />
                                                        </div>
                                                        <div class="form-group ">
                                                            <label>
                                                                State:
                                                            </label>
                                                                <input class="form-control " type="text" ng-model="userccreg.state" name="city" required value="{[{ user.state }]}"/>

                                                        </div>
                                                        <div class="form-group ">
                                                            <label>
                                                                ZIP/Postal Code: *
                                                            </label>
                                                                <input class="form-control " type="text" ng-model="userccreg.zip" name="zip" required value="{[{ user.zipcode }]}" only-digits />
                                                        </div>
                                                        <div class="form-group ">
                                                            <label>
                                                                Country: *
                                                            </label>
                                                                <select id="userccregcountry" name="userccregcountry" ng-model="userccreg.country" class="form-control" required="required">
                                                                </select>

                                                        </div>
                                                        <div class="form-group">
                                                            <input type='button' class='btn btn-fill btn-info btn-wd btn-sm form-control' name='login' ng-disabled="formcard.$invalid" ng-click="process(userccreg, paymenttype)" value='Submit' />
                                                        </div>
                                                    </div>
                                                </div>
                                                        </fieldset>
                                                </form>
                                            </div>
                                            <!-- END CARD PAYMENT -->
                                            <!-- ================================================ -->
                                            <!-- START CHECK -->
                                            <div class="col-sm-6" ng-if="paymenttype=='check'">
                                                <form class="bs-example form-horizontal" name="formcheque" novalidate>
                                                    <fieldset ng-disabled="paymentprocessing">
                                                <div class="form-group" >
                                                    <label>
                                                        Account Holder Name: *
                                                    </label>
                                                        <input class="form-control " type="text" ng-model="userecheck.accountname" name="accountname" ng-required="true"/>
                                                </div>
                                                <div class="form-group" >
                                                    <label>
                                                        Your bank name: *
                                                    </label>
                                                        <input class="form-control " type="text" ng-model="userecheck.bankname" name="bankname" ng-required="true"/>
                                                </div>
                                                <div class="form-group" >
                                                    <label>
                                                        Enter the routing code and account number as they appear on the bottom of your check:
                                                    </label>
                                                        <img src="/img/checksample.png">
                                                </div>
                                                <div class="form-group" >
                                                    <label>
                                                        Bank Routing Number: *
                                                    </label>

                                                        <input class="form-control " type="text" ng-model="userecheck.bankrouting" name="bankrouting" ng-required="true" only-digits/>

                                                </div>
                                                <div class="form-group" >
                                                    <label>
                                                        Bank Account Number: *
                                                    </label>

                                                        <input class="form-control " type="text" ng-model="userecheck.bankaccountnumber" name="bankaccountnumber" ng-required="true" only-digits />

                                                </div>
                                                <div class="form-group" >
                                                    <label>
                                                        Select Account Type:
                                                    </label>
                                                    <div ng-init="userecheck.at = 'CHECKING'">
                                                        <input type="radio" name="at" ng-model="userecheck.at" value="CHECKING">&nbsp;&nbsp;Checking account<br/>
                                                        <input type="radio" name="at" ng-model="userecheck.at" value="BUSINESSCHECKING">&nbsp;&nbsp;Business checking account<br/>
                                                        <input type="radio" name="at" ng-model="userecheck.at" value="SAVINGS">&nbsp;&nbsp;Savings account
                                                    </div>
                                                </div>

                                                <div class="form-group" >
                                                    <div class="col-md-12">
                                                        <input type="checkbox"  ng-model="userecheck.autorz" name="autorz" value="1" ng-required="true">*
                                                        <span style="text-align: center;">
                                                        By entering my account number about and Clicking authorize, I authorize my payment to be processed
                                                        as an electronic funds transfer or draft drawn from my account. If the payment returned unpaid, I authorize you or your service
                                                        provider to collect the payment at my state's return item fee by electronic funds transfer(s) or draft(s) drawn from my account.
                                                        <a href="https://www.achex.com/html/NSF_pop.jsp" target="_blank">Click here to view your state's returned item fee.</a> If this payment is from a corporate account, I make these
                                                        authorizations as an authorized corporate representative and agree that the entity will be bound by the NACHA operating rules.
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="form-group" >
                                                        <span style="font-weight:bold;">Billing Information</span>
                                                        <input type="checkbox"  ng-model="useyouraddress1" name="useyouraddress1" value="1" ng-required="true" ng-click="getcurrentaddress(useyouraddress1, paymenttype)"> Use your current address.
                                                </div>
                                                <div class="form-group" >
                                                    <label>
                                                        First Name: *
                                                    </label>
                                                        <input class="form-control " type="text" ng-model="userecheck.billingfname" name="billingfname" ng-required="true" />
                                                </div>
                                                <div class="form-group" >
                                                    <label>
                                                        Last Name: *
                                                    </label>
                                                        <input class="form-control " type="text" ng-model="userecheck.billinglname" name="billinglname" ng-required="true" />
                                                </div>
                                                <div class="form-group" >
                                                    <label>
                                                        Address Line 1: *
                                                    </label>
                                                        <input class="form-control " type="text" ng-model="userecheck.al1" name="al1" ng-required="true" />
                                                </div>
                                                <div class="form-group" >
                                                    <label>
                                                        Address Line 2:
                                                    </label>
                                                        <input class="form-control " type="text" ng-model="userecheck.al2" name="al2" />
                                                </div>
                                                <div class="form-group" >
                                                    <label>
                                                        City: *
                                                    </label>
                                                        <input class="form-control " type="text" ng-model="userecheck.city" name="city" ng-required="true" />
                                                </div>
                                                <div class="form-group" >
                                                    <label>
                                                        State:
                                                    </label>
                                                        <input class="form-control " type="text" ng-model="userecheck.state" name="city" />
                                                </div>
                                                <div class="form-group">
                                                    <label>
                                                        ZIP/Postal Code: *
                                                    </label>
                                                        <input class="form-control " type="text" ng-model="userecheck.zip" name="zip" required />
                                                </div>
                                                <div class="form-group" >
                                                    <label>
                                                        Country: *
                                                    </label>
                                                    <select id="userecheckcountry" name="userecheckcountry" ng-model="userecheck.country" class="form-control" required="required">
                                                    </select>
                                                </div>
                                                        <div class="form-group">
                                                            <input type='button' class='btn btn-fill btn-info btn-wd btn-sm form-control' name='login' ng-disabled="formcheque.$invalid" ng-click="process(userecheck, paymenttype)" value='Submit' />
                                                        </div>
                                                        </fieldset>
                                                </form>
                                            </div>
                                            <!-- END CHECK -->
                                            <!-- ============================ -->
                                            <!-- START PAYPAL -->
                                            <div class="col-sm-6 text-center" ng-if="paymenttype=='paypal'">
                                                <h4 class="info-text"> <small>Just click the paypal button to proceed to paypal payment.</small></h4>
                                                <form class="paypalform" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                                    <input type="hidden" name="cmd" value="_cart">
                                                    <input type="hidden" name="upload" value="1">
                                                    <input type="hidden" name="business" value="efrenbautistajr@gmail.com">
                                                    <input type="hidden" name="lc" value="US">
                                                    <input type="hidden" name="custom" id="invoiceno"  ng-model="invoiceno">
                                                    <input type="hidden" name="invoice" ng-model="invoiceno">
                                                    <div ng-repeat="list in member.reservationlist">
                                                        <input type="hidden" name="item_name_{[{ $index + 1 }]}" value="{[{ list.service.title }]}, {[{ list.servicechoice.description }]} ({[{ list.text }]})">
                                                        <input type="hidden" name="amount_{[{ $index + 1 }]}" value="{[{ list.servicechoice.price }]}">
                                                    </div>

                                                    <input type="hidden" name="currency_code" value="USD">

                                                </form>
                                                <!-- Display the payment button. -->
                                                <input type="image" name="submit" border="0"
                                                       src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/buy-logo-large.png"
                                                       alt="PayPal - The safer, easier way to pay online" ng-click="process('',paymenttype)">
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="wizard-footer">
                            <div class="pull-right">
                                <input type='button' ng-show="wizardStep==1 && member.reservationlist.length != 0; " ng-click="buttonclicked = 'next'; wizardStep = wizardStep + 1" class='btn btn-next btn-fill btn-info btn-wd btn-sm' name='next' value='Next' />
                            </div>
                            <div class="pull-left">
                                <input type='button' ng-show="wizardStep!=1 && !paymentsuccess" ng-click="buttonclicked = 'prev'; wizardStep = wizardStep - 1" class='btn btn-previous btn-fill btn-default btn-wd btn-sm' name='previous' value='Previous' />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
            </div> <!-- wizard container -->
        </div>



    </div>
    <!-- Below Banner ends -->
