


    <!-- Banner starts -->
   <!--  <div class="banner-container healing-bg">

    </div> -->

    <!-- Banner ends -->


    <div class="container" ng-controller="BookingCtrl" id="booking">
        <div class="row"><br/><br/><br/>
        <div class="col-sm-12"><h1>Online Reservations</h1></div>
            <div class="col-sm-6">                
                <h2 class="orange">Contact Information</h2>
                <div class="form">
            <!-- Contact form -->
                    <form name="formBooking" class="form-validation booking ng-pristine ng-invalid ng-invalid-required" ng-submit="submit(book)" >
                <!-- Name -->
                        <div class="control-group margin-bot15"> 
                                <div class="row">
                                    <label class="control-label size14 col-sm-12 hidden booking-success" for="name" style="color:#6cb04b;">Your message has been successfully sent.</label>                                    
                                </div>
                            
                        </div>
                        <div class="control-group margin-bot15"> 
                                <div class="row">
                                    <label class="control-label size14 col-sm-12" for="name">Name <span class="red">*</span> </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control textbox margin-bot15" id="fname" placeholder="First Name" required="required" ng-model="book.fname">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control textbox" id="lname" placeholder="Last Name" required="required" ng-model="book.lname">
                                    </div>
                                </div>
                            
                        </div>
                <!-- Email -->
                        <div class="control-group margin-bot15">
                            <div class="row">
                                <label class="control-label size14 col-sm-12" for="email">Email <span class="red">*</span> </label>
                                <div class="col-sm-12">
                                    <span class="red hidden booking-email">Invalid Email Address</span>
                                    <input type="email" class="textbox form-control" id="email" required="required" ng-model="book.email" ng-change="onemail(book.email)">
                                </div>
                            </div>
                        </div>
                <!-- Phone Number -->
                        <div class="control-group margin-bot15">
                            <div class="row">
                                <label class="control-label size14 col-sm-12" for="email">Phone Number <span class="red">*</span> </label>
                                <div class="col-sm-12">
                                    <span class="red hidden booking-phonenum">The phone number can only contain the digits, spaces, -, (, ), + and .</span>
                                    <input type="text" class="form-control textbox" id="phonenumber" ng-change="onphonenum(book.contactno)" required="required" ng-model="book.contactno">
                                </div>
                            </div>
                        </div>
                <!-- Message Details -->
                        
                        <h2>Reservation Details</h2>

                <!-- Subject -->
                        <div class="control-group margin-bot15">
                            <div class="row">
                                <label class="control-label size14 col-sm-12" for="email">Choose A Service <span class="red">*</span></label>

                                <div class="col-sm-12">
                                    <select name="_u932755291949318754" class="form-control form-select textbox" ng-model="book.service" required="required">
                                        <option value="Healing Service - Intuitive Reading">Healing Service - Intuitive Reading</option>
                                        <option value="Healing Service - Past Life Reading">Healing Service - Past Life Reading</option>
                                        <option value="Healing Service - Couple's Relationship Reading">Healing Service - Couple's Relationship Reading</option>
                                        <option value="Healing Service - Chakra Balancing and Crystal Healing">Healing Service - Chakra Balancing and Crystal Healing</option>
                                        <option value="Healing Service - Spiritual Acupuncture">Healing Service - Spiritual Acupuncture</option>
                                        <option value="Retreat/Workshop - Heal Your Body &amp; Soul">Retreat/Workshop - Heal Your Body &amp; Soul</option>
                                        <option value="Retreat/Workshop - Find Your Purpose">Retreat/Workshop - Find Your Purpose</option>
                                        <option value="Retreat/Workshop - Manifest Your Dream">Retreat/Workshop - Manifest Your Dream</option>
                                        <option value="Retreat/Workshop - Body, Mind, &amp; Spirit Integration">Retreat/Workshop - Body, Mind, &amp; Spirit Integration</option>
                                        <option value="Retreat/Workshop - Crystal Palace Workshop">Retreat/Workshop - Crystal Palace Workshop</option>
                                        <option value="Massage - Relaxation Massage">Massage - Relaxation Massage</option>
                                        <option value="Massage - Meridian Massage">Massage - Meridian Massage</option>
                                        <option value="Massage - Deep Tissue Massage">Massage - Deep Tissue Massage</option>
                                        <option value="Massage - Hot Stone Massage">Massage - Hot Stone Massage</option>
                                        <option value="Massage - Reflexology">Massage - Reflexology</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                <!-- Comment -->
                        <div class="control-group margin-bot15">
                            <div class="row">
                                <label class="control-label size14 col-sm-12" for="comment">Message<span class="red">*</span></label>
                                <div class="col-sm-12">
                                    <textarea class="form-control input-large textarea" id="comment" rows="5" required="required" ng-model="book.message"></textarea>
                                </div>
                            </div>
                        </div>
                <!-- Buttons -->
                        <div class="form-actions margin-bot15">
                            <div class="row">   
                                <div class="col-sm-12">             
                                    <button type="submit" class="btn btn-sedona" ng-disabled="formBooking.$invalid||!validphonenumber||!validemail">Submit</button>
                                </div>
                            </div>
                        </div>
                <!-- Buttons -->
                    </form>
                </div>  
                <div class="clearfix"></div>
            </div>

            <div class="col-sm-6 storeinfo">   
                <div class="storeinfo-details">             
                    <h2 class="orange"> <span ng-bind="contact.title"> </span> </h2>
                    <div class="title3">
                        <span class="titleb">Phone:</span>
                        <span ng-bind="contact.phone" class="thin-font3"> </span> <br/>  
                        <span class="titleb">Email:</span> <span ng-bind="contact.email" class="thin-font3"> </span> <br/>
                        <span class="titleb">Hours:</span> <span ng-bind="contact.hours" class="thin-font3"> </span>  <br/> <br/>
                    </div> 
                    <div class="title3">
                        <span class="titleb text-top">Address:</span> <br/>
                        <div ng-repeat="x in address">
                            <span class="thin-font3" ng-bind="x"> </span> <br/>
                        </div>
                        
                        <!-- <span class="thin-font3">Sedona, AZ 86336</span> -->
                    </div>
                    <div class="wsite-map"><iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 250px; margin-top: 10px; margin-bottom: 10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3273.673285579057!2d-111.7637428!3d34.8644358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872da1564d73fd15%3A0x3e31d123930491bf!2s201+Hwy+179%2C+Sedona%2C+AZ+86336%2C+USA!5e0!3m2!1sen!2sph!4v1434348998372"></iframe></div>
                    


                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
    </div>

    <!-- Below Banner ends -->




