
    <!-- Banner starts -->
<!--     <div class="banner-container retreats-bg" title="<?php echo $title; ?>">
        <img src="img/frontend/sedona_baner_retreats.jpg" class="pinterest-img" alt="<?php echo $title; ?>">
        <div class="black-box">
            <span class="banner-title"><?php echo $retreats[0]->cattitle;?></span>
            <br/>
            <?php if($retreats[0]->catsubtitle !=''){echo "<span class='banner-sub-title1'>".$retreats[0]->catsubtitle."</span><br/>";}?>
        </div>
    </div> -->

    <!-- Banner ends -->

<!-- Below SlidEr starts -->
     <div class="container-fluid1">
        <div id="myCarousel" class="carousel slide mycarousel" data-ride='carousel'>

<!-- Indicators -->
            <ol class="carousel-indicators" style="margin-bottom:-10px">
            <?php
            $nslides = json_decode($slides);
            $dst = 0;
            foreach ($nslides as $key => $value) {
            ?>
                <li data-target="#myCarousel" data-slide-to="<?php echo $dst; ?>"
                 class="<?php if($nslides[$key]->sort == 1){ echo 'active'; } ?>"></li>
            <?php $dst++; } ?>
            </ol>

<!-- Wrapper for Slides -->
            <div class="carousel-inner carousel-wrap">

            <?php
            foreach ($nslides as $key => $value) {
                ?>
                <div class="item <?php if($nslides[$key]->sort == 1){ echo 'active'; } ?>">
                    <div class="sidebarimage" style="background-image: url(<?php echo $this->config->application->amazonlink; ?>/uploads/slider/<?php echo $nslides[$key]->foldername.'/'.$nslides[$key]->img; ?>);">
                    </div>

                        <div class="banner-container banner-content-wrapper-green">
                        <div class='containerlang'>
                          <?php if($nslides[$key]->display=='yes') { ?>
                            <?php if($nslides[$key]->position=='right') { ?>
                            <div class="col-sm-7 col-sm-offset-5 slider-right slider slider<?php echo $key; ?>" style='padding:6vw'> <!-- RIGHT ALIGN-->
                            <?php if($nslides[$key]->box=='on') { ?>
                              <div class="Slider-content" > <!-- BOX ON-->

                                  <!--ARROW!-->
                               <div class='row'>
                            <div class='pull-left click1'>
                                <span class="slider-title" style="color:<?php echo $nslides[$key]->titlecolor; ?>;font-size:<?php $size=$nslides[$key]->titlesize/16; echo $size; ?>vw;line-height:<?php if ($nslides[$key]->titlesize>=90) {echo '6';}?>vw "><?php echo $nslides[$key]->title; ?></span>
                                </div>

                                <div class='pull-right' style='text-align:center;width:50px'> <a class='btn-lg clickme clickme<?php echo $key; ?>' id='clickme' onclick='slide(<?php echo $key; ?>)'>  <i class='fa fa-arrow-circle-down'> </i> </a>
                                 <a class='clickshow clickshow<?php echo $key; ?>'id='clickshow' onclick='hide(<?php echo $key; ?>)'>
                           <i class='fa fa-arrow-circle-up'> </i> </a>
                            </div>
                            <!-- Overflow Auto-->
                            </div>

                                <br>
                                <span class="slider-desc slider-desc<?php echo $key; ?>" style="color:<?php echo $nslides[$key]->desccolor; ?>;font-size:<?php $size=$nslides[$key]->descsize/16; echo $size; ?>vw;">
                                  <?php echo strip_tags($nslides[$key]->description); ?>
                                     <?php if($nslides[$key]->btnlink != ''){ ?>
                              <a href="<?php echo $nslides[$key]->btnlink; ?>" class="Services-learnmore"><?php echo $nslides[$key]->btnname; ?></a>
                              <?php }?>
                              </span>


                             </div>
                            <?php } else {?>
                            <div class="Services-outcontent"> <!-- BOX OFF-->
                            <!-- TITLE -->
                                 <!--ARROW!-->
                               <div class='row'>
                            <div class='pull-left click1'>
                                <span class="slider-title" style="color:<?php echo $nslides[$key]->titlecolor; ?>;font-size:<?php $size=$nslides[$key]->titlesize/16; echo $size; ?>vw;line-height:<?php if ($nslides[$key]->titlesize>=90) {echo '6';}?>vw "><?php echo $nslides[$key]->title; ?></span>
                                </div>
                                <div class='pull-right' style='color:black;text-align:center;width:50px'> <a class='btn-lg clickme clickme<?php echo $key; ?>' id='clickme' onclick='slide(<?php echo $key; ?>)'>  <i class='fa fa-arrow-circle-down'> </i> </a>
                                 <a class='clickshow clickshow<?php echo $key; ?>'id='clickshow' onclick='hide(<?php echo $key; ?>)'>
                           <i class='fa fa-arrow-circle-up'> </i> </a>
                            </div>
                            <!-- Overflow Auto-->
                            </div>
                                <br>
                                <span class="slider-desc slider-desc<?php echo $key; ?>" style="color:<?php echo $nslides[$key]->desccolor; ?>;font-size:<?php $size=$nslides[$key]->descsize/16; echo $size; ?>vw">
                                  <?php echo strip_tags($nslides[$key]->description); ?>

                               <?php if($nslides[$key]->btnlink != ''){ ?>
                              <a href="<?php echo $nslides[$key]->btnlink; ?>" class="Services-learnmore"><?php echo $nslides[$key]->btnname; ?></a>
                              <?php }?>
                              </span>


                             </div>
                             <?php } ?>


                      </div>

                      <?php } if($nslides[$key]->position=='center') { ?>
                            <div class="slider-center slider slider<?php echo $key; ?>" style='padding:6vw'> <!-- Center ALIGN-->

                            <?php if($nslides[$key]->box=='on') { ?>
                              <div class="Slider-content" > <!-- BOX ON-->

                                <!--ARROW!-->
                               <div class='row'>
                            <div class='pull-left  click1'>
                                <span class="slider-title" style="color:<?php echo $nslides[$key]->titlecolor; ?>;font-size:<?php $size=$nslides[$key]->titlesize/16; echo $size; ?>vw;line-height:<?php if ($nslides[$key]->titlesize>=90) {echo '6';}?>vw "><?php echo $nslides[$key]->title; ?></span>
                                </div>
                                <div class='pull-right' style='color:black;text-align:center;width:50px'> <a class='btn-lg clickme clickme<?php echo $key; ?>' id='clickme' onclick='slide(<?php echo $key; ?>)'>  <i class='fa fa-arrow-circle-down'> </i> </a>
                                 <a class='clickshow clickshowcenter clickshow<?php echo $key; ?>'id='clickshow' onclick='hide(<?php echo $key; ?>)'>
                           <i class='fa fa-arrow-circle-up'> </i> </a>
                            </div>
                            <!-- Overflow Auto-->
                            </div>

                                <br>
                                <span class="slider-desc slider-desc<?php echo $key; ?>" style="color:<?php echo $nslides[$key]->desccolor; ?>;font-size:<?php $size=$nslides[$key]->descsize/16; echo $size; ?>vw">
                                  <?php echo strip_tags($nslides[$key]->description); ?>

                               <?php if($nslides[$key]->btnlink != ''){ ?>
                              <a href="<?php echo $nslides[$key]->btnlink; ?>" class="Services-learnmore"><?php echo $nslides[$key]->btnname; ?></a>
                              <?php }?>
                              </span>

                             </div>
                            <?php } else {?>
                            <div class="Services-outcontent"> <!-- BOX OFF-->
                            <!-- TITLE -->
                                   <!--ARROW!-->
                                 <div class='row'>
                            <div class='pull-left  click1'>
                                <span class="slider-title" style="color:<?php echo $nslides[$key]->titlecolor; ?>;font-size:<?php $size=$nslides[$key]->titlesize/16; echo $size; ?>vw;line-height:<?php if ($nslides[$key]->titlesize>=90) {echo '6';}?>vw "><?php echo $nslides[$key]->title; ?></span>
                                </div>
                                <div class='pull-right' style='color:black;text-align:center;width:50px'> <a class='btn-lg clickme clickme<?php echo $key; ?>' id='clickme' onclick='slide(<?php echo $key; ?>)'>  <i class='fa fa-arrow-circle-down'> </i> </a>
                                 <a class='clickshow clickshowcenter clickshow<?php echo $key; ?>'id='clickshow' onclick='hide(<?php echo $key; ?>)'>
                           <i class='fa fa-arrow-circle-up'> </i> </a>
                            </div>
                            <!-- Overflow Auto-->
                            </div>

                                <br>
                                <span class="slider-desc slider-desc<?php echo $key; ?>" style="color:<?php echo $nslides[$key]->desccolor; ?>;font-size:<?php $size=$nslides[$key]->descsize/16; echo $size; ?>vw">
                                  <?php echo strip_tags($nslides[$key]->description); ?>

                                   <?php if($nslides[$key]->btnlink != ''){ ?>
                              <a href="<?php echo $nslides[$key]->btnlink; ?>" class="Services-learnmore"><?php echo $nslides[$key]->btnname; ?></a>
                              <?php }?>
                              </span>

                             </div>
                             <?php } ?>


                      </div>
                      <?php } else if ($nslides[$key]->position=='left') { ?>
                      <div class="col-sm-7 slider-left slider slider<?php echo $key; ?>" style='padding:6vw' id='slider-left'> <!-- LEFT ALIGN-->
                         <?php if($nslides[$key]->box=='on') { ?>
                              <div class="Slider-content"> <!-- BOX ON-->

                            <!--ARROW!-->
                               <div class='row'>
                            <div class='pull-left  click1'>
                                <span class="slider-title" style="color:<?php echo $nslides[$key]->titlecolor; ?>;font-size:<?php $size=$nslides[$key]->titlesize/16; echo $size; ?>vw;line-height:<?php if ($nslides[$key]->titlesize>=90) {echo '6';}?>vw "><?php echo $nslides[$key]->title; ?></span>
                                </div>
                                <div class='pull-right' style='color:black;text-align:center;width:50px'> <a class='btn-lg clickme clickme<?php echo $key; ?>' id='clickme' onclick='slide(<?php echo $key; ?>)'>  <i class='fa fa-arrow-circle-down'> </i> </a>
                                 <a class='clickshow clickshow<?php echo $key; ?>'id='clickshow' onclick='hide(<?php echo $key; ?>)'>
                           <i class='fa fa-arrow-circle-up'> </i> </a>
                            </div>
                            <!-- Overflow Auto-->
                            </div>

                                <br>
                                <span class="slider-desc slider-desc<?php echo $key; ?>" style="color:<?php echo $nslides[$key]->desccolor; ?>;font-size:<?php $size=$nslides[$key]->descsize/16; echo $size; ?>vw">
                                  <?php echo strip_tags($nslides[$key]->description); ?>


                               <?php if($nslides[$key]->btnlink != ''){ ?>
                              <a href="<?php echo $nslides[$key]->btnlink; ?>" class="Services-learnmore"><?php echo $nslides[$key]->btnname; ?></a>
                              <?php }?>
                              </span>


                             </div>
                            <?php } else {?>
                            <div class="Services-outcontent"> <!-- BOX OFF-->

                            <div class='row'>
                            <div class='pull-left click1' stye='border:1px solid black'>
                                <span class="slider-title" style="color:<?php echo $nslides[$key]->titlecolor; ?>;font-size:<?php $size=$nslides[$key]->titlesize/16; echo $size; ?>vw;line-height:<?php if ($nslides[$key]->titlesize>=90) {echo '6';}?>vw "><?php echo $nslides[$key]->title; ?></span>
                                </div>
                                <div class='pull-right click' style='text-align:center;width:50px'> <a class='clickme clickme<?php echo $key; ?>'
                                 id='clickme' onclick='slide(<?php echo $key; ?>)'>  <i class='fa fa-arrow-circle-down'> </i> </a>
                                <a class='clickshow clickshow<?php echo $key; ?>' id='clickshow' onclick='hide(<?php echo $key; ?>)'>
                           <i class='fa fa-arrow-circle-up'> </i> </a>

                            </div>
                            </div>

                                <br>
                                <span class="slider-desc slider-desc<?php echo $key; ?>" style="color:<?php echo $nslides[$key]->desccolor; ?>;font-size:<?php $size=$nslides[$key]->descsize/16; echo $size; ?>vw">
                                  <?php echo strip_tags($nslides[$key]->description); ?>

                                  <?php if($nslides[$key]->btnlink != ''){ ?>
                              <a href="<?php echo $nslides[$key]->btnlink; ?>" class="Services-learnmore"><?php echo $nslides[$key]->btnname; ?></a>
                              <?php }?>
                              </span>

                             </div>
                             <?php } ?>
                  </div>
                  <?php } }?>

                  </div>

                </div>

                </div>

            <?php } ?>
            </div>

<!-- Controls -->

            <a class="left-arrow left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="icon-prev"></span>
            </a>
            <a class="right-arrow right carousel-control" href="#myCarousel" data-slide="next">
                <span class="icon-next"></span>
            </a>
        </div>
<!-- slider ends -->


    </div>


    {#<div class="container">
        <div class="row">

            <div class="span12 center retreats-section">
                <?php
                          $getginfo = $retreats;
                          foreach ($getginfo as $key => $value) {
                ?>
                <div class="span3 marg-bot center landing-page-cols-first hbs">
                    <a href="<?php echo $base_url;?>/retreats/<?php echo $getginfo[$key]->pageslugs;?>">
                        <div class="title2 center landing-page-cols-thumb no-padding no-margin" style="background-image:  url('<?php echo $imageLink.'/uploads/pageimage/'. $getginfo[$key]->imagethumb  ?>'); " title="<?php echo $getginfo[$key]->imagethumbsubtitle ?>">
                            <img src="<?php echo $imageLink.'/uploads/pageimage/'. $getginfo[$key]->imagethumb  ?>" class="pinterest-img" alt="<?php echo $getginfo[$key]->imagethumbsubtitle ?>"/>
                            <div class="imagethumbtitle"><?php echo $getginfo[$key]->title ?></div>
                        </div>
                    </a>
                    <div class="text-phar text-left">
                        <div class="subtitle"><?php echo $getginfo[$key]->imagethumbsubtitle ?></div>
                    <span>
                    <?php
                        echo strip_tags($getginfo[$key]->thumbdesc);
                    ?>
                    <a href="<?php echo $base_url;?>/retreats/<?php echo $getginfo[$key]->pageslugs;?>">Learn more.</a>
                    </span>
                </div>
                <div class="button2 center"><a href="<?php echo $base_url;?>/booking" class="learn-sedona">Make an Appointment</a></div>
                <br/>
            </div>
            <?php } ?>
            <div class="clearfix"></div>

        </div>
    </div>#}

    <!-- Below Banner ends -->

    <?php
          $getginfo = $retreats;
          foreach ($getginfo as $key => $value) {
    ?>
    <div class="container-fluid Services">
        <div class="Services-bg" style="background:linear-gradient( rgba(0, 0, 0, 0), rgba(250, 250, 250, 0) ), url('<?php echo $imageLink.'/uploads/pageimage/'. $getginfo[$key]->imagethumb  ?>');
                                        background-position:
                                          <?php
                                            if($getginfo[$key]->textalign=='left') { echo 	'center right!important'; }
                                            elseif($getginfo[$key]->textalign=='right') { echo 	'center left!important'; }
                                            else { echo 	'center center!important'; }
                                          ?>">
          {#this is the banner#}
        </div>

        <div class="Services-body <?php echo $getginfo[$key]->classname; ?>">
            <?php if($getginfo[$key]->textalign == 'right') { ?>  {#RIGHT ALIGN#}
                      <div class="col-sm-6 col-sm-offset-6 Services-content" style="color:<?php echo $getginfo[$key]->color; ?>">

                        <?php  if($getginfo[$key]->box == 'true') { ?>
                          <div class="Services-box">
                        <?php } else { ?>
                          <div>
                        <?php }

                  } elseif ($getginfo[$key]->textalign == 'left') { ?> {#LEFT ALIGN#}
                      <div class="col-sm-6 Services-content" style="color:<?php echo $getginfo[$key]->color; ?>">

                        <?php  if($getginfo[$key]->box == 'true') { ?>
                          <div class="Services-box">
                        <?php } else { ?>
                          <div>
                        <?php }

                  } else { ?> {#CENTER ALIGN#}
                      <div class="col-sm-6 col-sm-offset-3 Services-content" style="color:<?php echo $getginfo[$key]->color; ?>">

                        <?php  if($getginfo[$key]->box == 'true') { ?>
                          <div class="Services-box">
                        <?php } else { ?>
                          <div>
                        <?php }
                  }  ?>

                  {#continuation of the Services-content div#}
                  <span class="Services-title"><?php echo $getginfo[$key]->imagethumbsubtitle; ?></span>
                  <span class="Services-description">
                    <?php echo strip_tags($getginfo[$key]->thumbdesc); ?>
                    <?php echo $color[$key]; ?>
                    <a href="<?php echo $base_url;?>/retreats/<?php echo $getginfo[$key]->pageslugs;?>" class="Services-learnmore">Learn More</a>
                  </span>
                </div> {#end of Services-box#}
              </div> {#end of Services-content#}
        </div> {# end of body #}
    </div> {# END OF CONTAINER-FLUID #}
            <style>
              @media screen and (max-width:767px) {
                .<?php echo $getginfo[$key]->classname; ?> {
                  background:<?php echo $getginfo[$key]->bgcolor; ?>;
                }
              }
            </style>
    <?php } ?> {#end of foreach#}
