
    <!-- Banner starts -->
    <div class="banner-container retreats-bg workshops-bg">
        <div class="black-box">
            <span class="banner-title">3rd Eye Opening</span>
            <br/>
            <span class="banner-sub-title1">- Level 1 - </span>
            <br/>
            <span class="banner-sub-title1">The True Spirit of Sedona</span>
            <br/>
            <br/>
            <div class="button"><a href="<?php echo $base_url;?>/booking" class="learn-sedona">SCHEDULE YOUR WORKSHOP</a></div>
        </div>
    </div>

    <!-- Banner ends -->


    <div class="container">
        <div class="row">

            <div class="col-sm-8 no-margin content-text">

                <div class="phar no-padding">
                    <span class="size20"><h2>3RD EYE OPENING (LEVEL 1)</h2></span>
                      <p class="size18">
                          This workshop will enable you to experience deeper levels of consciousness. With guided instruction and meditation, you will improve your physical, emotional and mental wellness. You can experience oneness with the Universe through light, sound and vibration, which will open your 3rd eye's MindScreen. Afterward you will have a clearer picture of your life and its direction.
                      </p>
                </div>

                <div class="phar no-padding">
                    <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                    <div class="size16 border-left italic margin-left margin-top">
                        "This was a unique experience!" <br/> <br/>
                        <div class="text-right">- Anonymous</div>
                    </div>

                </div>

            </div>

            <div class="col-sm-4 no-padding">
                <!-- ===== SIDE BAR 1 ===== -->
                <div class="phar center no-padding sidebar1">
                    <hr class="styled-hr">
                    <h2>3RD EYE OPENING (LEVEL 1)</h2>
                    <h3 class="grayfont">Cost: $500 (180 min)</h3>
                                <div class="button"><a href="<?php echo $base_url;?>/booking" class="learn-sedona">BOOK ONLINE NOW</a></div><br/>
                                <div class="center">
                        <span class="size20">
                          - OR -<br/>
                        </span>
                        <span class="size20 bold">
                          Call 928-282-3875
                        </span>
                        <span class="size20"><br/>
                          to make an appointment<br/> <br/>
                        </span>
                    </div>
                    <hr class="styled-hr">
                </div>
                <!-- ===== END SIDE BAR 1 ===== -->

                <!-- ===== SIDE BAR 2 ===== -->
                <?php echo $sidebar;?>
                <!-- ===== END SIDE BAR 2 ===== -->

            </div>
        </div>
    </div>



    <div class="container margin-bot100 margin-top80">
        <hr class="styled-hr">
        <div class="row padding-topbot20">
            <div class="col-sm-12 center">
                <span class="size25 font2">Call 928-282-3875 or <a href="">CLICK HERE</a> to make an appointment.</span>
            </div>
        </div>
        <hr class="styled-hr">
    </div>

    <!-- Below Banner ends -->