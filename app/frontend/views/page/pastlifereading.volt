
    <!-- Banner starts -->
    <div class="banner-container past-life-reading-bg">

        <div class="black-box">
            <span class="banner-title">Past Life Reading</span>
            <br/>
            <span class="banner-sub-title1">Understand the Past, Change the Future</span> <br/> <br/>
            <div class="button"><a href="<?php echo $base_url;?>/booking" class="learn-sedona">MAKE AN APPOINTMENT</a></div>
        </div>

    </div>

    <!-- Banner ends -->

    <div class="container">
        <div class="row">
            <div class="col-sm-8 no-margin content-text">
                <div class="phar no-padding">
                    <span class="size20"><h2>PAST LIFE READING</h2></span>
                    <p class="size18">
                        A Past Life Reading provides both fascinating and practical insight into the challenges and ultimate purpose of your life.  
                        The story of your soul extends past the boundaries of this body; it’s likely that you have lived life as other identities. 
                        Unresolved trauma experienced by your soul in other lifetimes may be causing you hesitation, fear, anxiety, or unexplained 
                        responses to ordinary circumstances that prevent you from living this life to its fullest.  Simply having an awareness of 
                        these formative events will give you a deeper understanding of your history, liberating you from the unconscious behaviors 
                        that may be thwarting your best laid plans.  Just like a personality test can help you understand the varying aspects of 
                        your character, a Past Life Reading can help you understand what your soul really wants to accomplish during your time on 
                        this earth and provide you with the tools to overcome the barriers between you and your soul’s ultimate goal. 
                    </p>
                </div>

                <div class="phar no-padding">
                    <div class="size20"><h2 class="font1 italic">What People Are Saying</h2></div>
                    <div class="size18 border-left italic margin-left margin-top">
                        "This was a unique experience!" <br/> <br/>
                        <div class="text-right">- Anonymous</div>
                    </div>

                </div>
            </div>

            <div class="col-sm-4 no-padding">
                <!-- ===== SIDE BAR 1 ===== -->
                <div class="phar center no-padding sidebar1">
                    <hr class="styled-hr">
                    <h2>PAST LIFE READING</h2>
                    <h3 class="grayfont">Price: $200 (60 min)</h3>
                    <div class="button">
                    <a href="<?php echo $base_url;?>/booking" class="learn-sedona">BOOK ONLINE NOW</a>
                    </div><br/>
                    <div class="center">
                        <span class="size20">
                          - OR -<br/>
                        </span>
                        <span class="size20 bold">
                          Call 928-282-3875
                        </span>
                        <span class="size20"><br/>
                          to make an appointment<br/> <br/>
                        </span>
                    </div>
                    <hr class="styled-hr">
                </div>
                <!-- ===== END SIDE BAR 1 ===== -->

                <!-- ===== SIDE BAR 2 ===== -->
                <?php echo $sidebar;?>
                <!-- ===== END SIDE BAR 2 ===== -->

            </div>
        </div>
    </div>



    <div class="container margin-bot100 margin-top80">
        <hr class="styled-hr">
        <div class="row padding-topbot20">
            <div class="col-sm-12 center">
                <span class="size25 font2">Call 928-282-3875 or <a href="">CLICK HERE</a> to make an appointment.</span>
            </div>
        </div>
        <hr class="styled-hr">
    </div>

    <!-- Below Banner ends -->