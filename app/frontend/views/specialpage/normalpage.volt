<?php echo $this->getContent()?>
<style type="text/css">
    h4.blog-title-list {
     border-top: 0px solid #D0D0D0; 
    /* margin-top: 15px; */
}
</style>
    <!-- Banner starts -->
<div class="container-fluid">

<div class='normal-center-wrap'>
    <div class="row normalpage-content">
<!-- RESPONSIVE -->

<div class="col-sm-6 mobile-normal-maincontent">


           <div class="normalpage-title margin-bot10"><span><?php
            echo $data->title;
            ?> </span></div>

           <div class="normalpage-body size14">
           <?php
            echo $content;
            ?> 
            </div>
        </div>
<!-- RESPONSIVE -->

<!--     LEFT SIDE BAR -->
  <?php 

    if($data->leftsidebar=='true')
     {
    ?>
    <div class="col-sm-3 special-left">
    <div class="left-sidebar">    
   <?php
        foreach ($sidebarleft as $key => $value)
        {
         ?>
<!--        <div style='padding-top:3%'></div> -->
             <?php
                if ($value->sidebar=='Image')
                {
                    ?>
                          <hr class="styled-hr">
                    <img src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $value->img?>" width="100%">

                    <?php
                }
                 if ($value->sidebar=='Calendar')
                {
                    ?>
                    <hr class="styled-hr">
                   <div class="calendar" ng-model="eventSources" calendar="calendar1" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div>
                    <?php
                }
            if ($value->sidebar=='Menu')
            {
                ?>
                 <hr class="styled-hr">
        <div class="special-menu" style="">
          <div class="">
            <div class=""><a class="special-top-menu">Sedona Healing Arts</a>
            </div>
            
            <nav>
             
                  <?php $mobmenu = $value->shortcode;
                  foreach ($mobmenu as $k => $short) {
                    ?>
                     <ul class="">
                    <li> 
                        <?php
                        if ($mobmenu[$k]->newtab=='true'){
                            ?>
                            <a class="special-menu-text" href="<?php echo $mobmenu[$k]->sublink; ?>" target="_blank"><span>
                                <?php echo $mobmenu[$k]->subname; ?>
                            </span></a>
                            <?php
                }//if newtab true
                elseif ($mobmenu[$k]->newtab=='false') {
                    ?>
                    <a class="special-menu-text" href="<?php echo $mobmenu[$k]->sublink; ?>"><span>
                        <?php echo $mobmenu[$k]->subname; ?>
                    </span></a>
                    <?php
                }//if newtab false

                if (sizeof($mobmenu[$k]->children)!=0) {
                ?>
                <ul style="padding-left: 10px; list-style:none">
    
                  <?php 
                    $newdata = array();
                    $newdata = $mobmenu[$k]->children;

                    foreach ($newdata as $mobsub1) {
                    ?>
                    
                    <li> <span class="glyphicon glyphicon-menu-right" style="color:#58666e;"></span>
                   <?php
                    if ($mobsub1->newtab=='true') {
                    ?>
                    <a class="special-menu-text caps" href="<?php echo $mobsub1->sublink; ?>" target="_blank"><span> <?php echo $mobsub1->subname; ?> </span></a>
                    <?php
                    } //if mobsub1 true
                    elseif ($mobsub1->newtab=='false') {
                    ?>
                    <a class="special-menu-text caps" href="<?php echo $mobsub1->sublink; ?>"><span>
                        <?php echo $mobsub1->subname; ?>
                    </span></a>
                    <?php
                    } //if mobsub1 false
                    ?>

                    <?php
                    if (sizeof($mobsub1->children)!=0) {
                    ?>    
                     <ul style="padding-left: 30px;">
                     <?php
                        foreach ($mobsub1->children as $mobsub2)
                        { ?>
                        <li> <span class="glyphicon glyphicon-menu-right" style="color:#58666e;"></span>
                            <?php
                        if ($mobsub2->newtab=='true') {
                        ?>
                        <a class="special-menu-text caps" href="<?php echo $mobsub2->sublink; ?>" target="_blank"><span> <?php echo $mobsub2->subname; ?> </span></a>
                        <?php
                        } //if mobsub2 true
                        elseif ($mobsub2->newtab=='false') {
                        ?>
                        <a class="special-menu-text caps" href="<?php echo $mobsub2->sublink; ?>"><span>
                            <?php echo $mobsub2->subname; ?>
                        </span></a>
                        <?php
                        } //if mobsub2 false
                        ?>
                        <?php
                        if (sizeof($mobsub2->children)!=0) {
                            ?>  
                                <ul style="padding-left: 40px;">
                                  <?php
                            foreach ($mobsub2->children as $mobsub3)
                            { ?>
                            <li> <span class="glyphicon glyphicon-menu-right" style="color:#58666e;"></span>
                                <?php
                            if ($mobsub3->newtab=='true') {
                            ?>
                            <a class="special-menu-text caps" href="<?php echo $mobsub3->sublink; ?>" target="_blank"><span> <?php echo $mobsub3->subname; ?> </span></a>
                            <?php
                            } //if mobsub3 true
                            elseif ($mobsub3->newtab=='false') {
                            ?>
                            <a class="special-menu-text caps" href="<?php echo $mobsub3->sublink; ?>"><span>
                                <?php echo $mobsub3->subname; ?>
                            </span></a>
                            <?php
                            } //if mobsub3 false
                            ?>
                            <?php
                            if (sizeof($mobsub3->children)!=0) {
                                ?>
                                 <ul style="padding-left: 40px;">
                                     <?php
                            foreach ($mobsub3->children as $mobsub4)
                            { ?>
                            <li> <span class="glyphicon glyphicon-menu-right" style="color:#58666e;"></span>
                                <?php
                            if ($mobsub4->newtab=='true') {
                            ?>
                            <a class="special-menu-text caps" href="<?php echo $mobsub4->sublink; ?>" target="_blank"><span> <?php echo $mobsub4->subname; ?> </span></a>
                            <?php
                            } //if mobsub4 true
                            elseif ($mobsub4->newtab=='false') {
                            ?>
                            <a class="special-menu-text caps" href="<?php echo $mobsub4->sublink; ?>"><span>
                                <?php echo $mobsub4->subname; ?>
                            </span></a>
                            <?php
                            } //if mobsub4 false
                            ?>
                            </li> <!-- mobsub4 -->
                            <?php
                            } //foreach mobsub3
                            ?>
                            </ul> <!-- mobsub4 -->
                            <?php
                            } //if mobsub3 children not 0
                            ?>

                            </li> <!-- mobsub3 -->
                            <?php
                            } //foreach mobsub3
                            ?>
                            </ul> <!-- mobsub3 -->
                        <?php
                        } //if mobsub2->children not 0
                        ?>
                        </li> <!-- mobsub2 -->
                    <?php
                        } //foreach mobsub2
                     ?>
                     </ul> <!-- mobsub2 -->
                    <?php
                    } //if mobsub1 children not 0
                    ?>
                    
                    </li> <!-- mobsub1 -->
                <?php
                    } //mobsub1 foreach
                ?>
                <?php    
                } //if mobchild not 0
                ?>
                </ul> <!-- mobchild -->

            </li>
                </ul>
            <?php

              }//foreach
              ?>
          
            </nav>
          </div>

          </div>
                  <!-- Mobile menu ends -->
            <?php
                }
                if ($value->sidebar=='Testimonial')
                {
                    ?>
                   <hr class="styled-hr">
                     <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                                <?php $gettesti = $value->testimonial; ?>
                                <div class="specialpage_col">
                                    <?php
                                    $x = 0;
                                    foreach($gettesti as $k => $value) { ?>
                                <div class="size16 border-left italic margin-top">
                                    <div class="row">
                                        <div class="col-sm-3 text-center">
                                            <img src="<?php echo $imageLink.'/uploads/testimonialpic/'.$gettesti[$k]->picture; ?>" class="testi-img" width="100%">
                                        </div>
                                        <div class="col-sm-9">

                                            <div></div>
                                            <div>
                                                <span class="fa fa-quote-left"></span>
                                                <?php echo $gettesti[$k]->message; ?>
                                             </div>
                                                <div class="text-right">- <?php echo $gettesti[$k]->name ?></div>
                                            </div>
                                        </div>
                                        <hr class="styled-hr">
                                </div>
                                 <?php $x = 1; } ?>
                                </div>
                    <?php
                }
                if ($value->sidebar=='Link')
                {
                    if($value->url != '' && $value->label != ''){ ?>
                     <hr class="styled-hr">
                      <a href="<?php echo $value->url; ?>" class="page-link"><?php echo $value->label; ?></a>
                    <?php }?>

                    <?php
                }
                 if ($value->sidebar=='News')
                {
                    ?>
                    <hr class="styled-hr">
                     <script type="text/javascript">
                                limitcount = <?php echo $value->limit; ?>;
                                </script>
                      <div class="specialpage_col">
                                    <div class="" ng-controller="NewsCtrl" ng-init="showmorenews()">
                                      <div class="row">
                                          <h4 class="blog-title-list">Latest News</h4>
                                          <div class="list-news-wrapper">
                                            <div ng-hide="newslist"> Loading News...</div>
                                            <div class="row list-title-blog" ng-repeat="news in newslist">
                                              <div class="col-sm-3 news-thumb-container" style="background-image: url('{[{ news.videothumb | returnYoutubeThumb }]} {[{ news.imagethumb | returnImageThumb }]}')" ng-click="redirectNews(news.newsslugs);" title="{[{ news.title }]}">
                                                <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}">
                                                <img src="{[{ news.videothumb | returnYoutubeThumb }]} {[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}">
                                                <div class="youtube-play" ng-show="news.videothumb"><img src="img/youtubeplay.png" ng-click="redirectNews(news.newsslugs);"/></div>
                                                <a href="/blog/{[{ news.newsslugs; }]}">
                                                </a>
                                            </div>


                                            <div class="col-xs-8 col-md-7 news-list-desc">
                                                <div class="row">

                                                    <div class="col-sm-12">
                                                      <span class="size25 font1 news-title" ng-click="redirectNews(news.newsslugs);">{[{ news.title }]}</span>
                                                  </div>
                                                  <div class="col-sm-12">
                                                      <strong><span class="thin-font1 orange">{[{ news.categorylist }]}</span></strong><span ng-show="news.name !=''" class="thin-font1"> / by <a href="/blog/author/{[{ news.name }]}"><strong><span class="orange">{[{ news.name }]}</span></strong></a></span> / {[{ news.date }]}
                                                      <br/><br/>
                                                  </div>
                                                  <div class="col-sm-12">
                                                      <div class="font1 size14 summary">
                                                        {[{ news.summary }]}
                                                        <br/><br/>
                                                    </div>
                                                </div>
                                                </div>
                                                <div style="clear:both"></div>
                                                        <br>
                                                  </div>
                                              </div>
                                                </div>
                                                </div>
                                                </div>
                                                </div>
                    <?php
                }
                if ($value->sidebar=='Archive')
                {
                    ?>
                     <hr class="styled-hr">
                    <?php $getarchives = $value->archive; ?>
                    <h2 class="font1"><span class="fa fa-calendar fa-1x"></span> Archives</h2>
                    <div class="ul-archives">
                        <?php
                       
                        foreach ($getarchives as $key => $value) { ?>
                        <a href="<?php echo $base_url; ?>/blog/archive/<?php echo $getarchives[$key]->month . "/" . $getarchives[$key]->year; ?>"><span class="fa fa-chevron-right"></span> <?php echo $getarchives[$key]->month . " " . $getarchives[$key]->year; ?></a>
                        <?php
                    } ?>
                </div>

                    <?php
                }
                if ($value->sidebar=='Rss')
                {
                    ?>
                     <hr class="styled-hr">
                    <p class="size16"> <a href="<?php echo $base_url;?>/feed" target="_blank"><img class="rss" src="img/frontend/rss_feed.gif"> RSS Feed</a></p>
                    <?php
                }
                if ($value->sidebar=='Search')
                {
                    ?>
                    <hr class="styled-hr">
                      <div class="col-sm-12">
                      <div class="col-sm-10">
                      <input class="input-xs form-control" placeholder="Search" type="text" name="searchtext">

                      </div>
                      <div class="col-sm-2 align-left">
                      <span class="input-group-btn">
                        <button class="page-search" type="button">Go!</button>
                    </span>
                    </div>
                        </div>

                    <?php
                }

   

        } //FOREACH
        ?></div>
        </div><?php
    } //leftsidebar
    ?>
<!-- MAIN CONTENT-->
    <?php
    if($data->rightsidebar=='true' && $data->leftsidebar=='true')
    {
    ?>
        <div class="col-sm-6 normal-maincontent">
    <?php }
    elseif($data->leftsidebar=='true' && $data->rightsidebar=='false' || $data->leftsidebar=='false' && $data->rightsidebar=='true')
    {
    ?>
        <div class="col-sm-9 normal-maincontent">
    <?php }
    else
    {
    ?>
        <div class="col-sm-12 normal-maincontent">
    <?php } ?>


         <!-- Main@@! -->
           <div class="normalpage-title margin-bot10"><span><?php
            echo $data->title;
            ?> </span></div>

           <div class="normalpage-body size14">
           <?php
            echo $content;
            ?> 
            </div>
        </div>
<!-- MAIN CONTENT-->

 <!-- RIGHT SIDE     --> 
    <?php
    if($data->rightsidebar=='true')
    {
    ?>
         <div class="col-sm-3">
         <div class="right-sidebar">
    <?php 

     foreach ($sidebarright as $key => $value)
     {
         ?>
        <?php
               if ($value->sidebar=='Image')
                {
                    ?>
                    <hr class="styled-hr">
                    <img src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/<?php echo $value->img?>">

                    <?php
                }
                 if ($value->sidebar=='Calendar')
                {
                    ?>
                    <hr class="styled-hr">
                   <div class="calendar" ng-model="eventSources" calendar="calendar1" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div>
                    <?php
                }
                if ($value->sidebar=='Menu')
                {
                    ?>
             <hr class="styled-hr">
                 <div class="special-menu">
          <div class="">
            <div class=""><a class="special-top-menu">Sedona Healing Arts</a>
            </div>
            
            <nav>
                  <?php $mobmenu = $value->shortcode;
                  foreach ($mobmenu as $k => $short) {
                    ?>
                <ul class="">
                    <li> 
                        <?php
                        if ($mobmenu[$k]->newtab=='true'){
                            ?>
                            <a class="special-menu-text" href="<?php echo $mobmenu[$k]->sublink; ?>" target="_blank"><span>
                                <?php echo $mobmenu[$k]->subname; ?>
                            </span></a>
                            <?php
                }//if newtab true
                elseif ($mobmenu[$k]->newtab=='false') {
                    ?>
                    <a class="special-menu-text" href="<?php echo $mobmenu[$k]->sublink; ?>"><span>
                        <?php echo $mobmenu[$k]->subname; ?>
                    </span></a>
                    <?php
                }//if newtab false

                if (sizeof($mobmenu[$k]->children)!=0) {
                ?>
                <ul style="padding-left: 10px; list-style:none">
                  <?php 
                    $newdata = array();
                    $newdata = $mobmenu[$k]->children;

                    foreach ($newdata as $mobsub1) {
                    ?>
                    <li> <span class="glyphicon glyphicon-menu-right" style="color:#58666e;"></span>
                   <?php
                    if ($mobsub1->newtab=='true') {
                    ?>
                    <a class="special-menu-text caps" href="<?php echo $mobsub1->sublink; ?>" target="_blank"><span> <?php echo $mobsub1->subname; ?> </span></a>
                    <?php
                    } //if mobsub1 true
                    elseif ($mobsub1->newtab=='false') {
                    ?>
                    <a class="special-menu-text caps" href="<?php echo $mobsub1->sublink; ?>"><span>
                        <?php echo $mobsub1->subname; ?>
                    </span></a>
                    <?php
                    } //if mobsub1 false
                    ?>

                    <?php
                    if (sizeof($mobsub1->children)!=0) {
                    ?>    
                     <ul style="padding-left: 30px;">
                     <?php
                        foreach ($mobsub1->children as $mobsub2)
                        { ?>
                        <li> <span class="glyphicon glyphicon-menu-right" style="color:#58666e;"></span>
                            <?php
                        if ($mobsub2->newtab=='true') {
                        ?>
                        <a class="special-menu-text caps" href="<?php echo $mobsub2->sublink; ?>" target="_blank"><span> <?php echo $mobsub2->subname; ?> </span></a>
                        <?php
                        } //if mobsub2 true
                        elseif ($mobsub2->newtab=='false') {
                        ?>
                        <a class="special-menu-text caps" href="<?php echo $mobsub2->sublink; ?>"><span>
                            <?php echo $mobsub2->subname; ?>
                        </span></a>
                        <?php
                        } //if mobsub2 false
                        ?>
                        <?php
                        if (sizeof($mobsub2->children)!=0) {
                            ?>  
                                <ul style="padding-left: 40px;">
                                  <?php
                            foreach ($mobsub2->children as $mobsub3)
                            { ?>
                            <li> <span class="glyphicon glyphicon-menu-right" style="color:#58666e;"></span>
                                <?php
                            if ($mobsub3->newtab=='true') {
                            ?>
                            <a class="special-menu-text caps" href="<?php echo $mobsub3->sublink; ?>" target="_blank"><span> <?php echo $mobsub3->subname; ?> </span></a>
                            <?php
                            } //if mobsub3 true
                            elseif ($mobsub3->newtab=='false') {
                            ?>
                            <a class="special-menu-text caps" href="<?php echo $mobsub3->sublink; ?>"><span>
                                <?php echo $mobsub3->subname; ?>
                            </span></a>
                            <?php
                            } //if mobsub3 false
                            ?>
                            <?php
                            if (sizeof($mobsub3->children)!=0) {
                                ?>
                                 <ul style="padding-left: 50px;">
                                     <?php
                            foreach ($mobsub3->children as $mobsub4)
                            { ?>
                            <li> <span class="glyphicon glyphicon-menu-right" style="color:#58666e;"></span>
                                <?php
                            if ($mobsub4->newtab=='true') {
                            ?>
                            <a class="special-menu-text caps" href="<?php echo $mobsub4->sublink; ?>" target="_blank"><span> <?php echo $mobsub4->subname; ?> </span></a>
                            <?php
                            } //if mobsub4 true
                            elseif ($mobsub4->newtab=='false') {
                            ?>
                            <a class="special-menu-text caps" href="<?php echo $mobsub4->sublink; ?>"><span>
                                <?php echo $mobsub4->subname; ?>
                            </span></a>
                            <?php
                            } //if mobsub4 false
                            ?>
                            </li> <!-- mobsub4 -->
                            <?php
                            } //foreach mobsub3
                            ?>
                            </ul> <!-- mobsub4 -->
                            <?php
                            } //if mobsub3 children not 0
                            ?>

                            </li> <!-- mobsub3 -->
                            <?php
                            } //foreach mobsub3
                            ?>
                            </ul> <!-- mobsub3 -->
                        <?php
                        } //if mobsub2->children not 0
                        ?>
                        </li> <!-- mobsub2 -->
                    <?php
                        } //foreach mobsub2
                     ?>
                     </ul> <!-- mobsub2 -->
                    <?php
                    } //if mobsub1 children not 0
                    ?>
                    
                    </li> <!-- mobsub1 -->
                <?php
                    } //mobsub1 foreach
                ?>
                <?php    
                } //if mobchild not 0
                ?>
                </ul> <!-- mobchild -->

            </li>
              </ul> <!-- main menu -->
            <?php

              }//foreach
              ?>
            
            </nav>
          </div>

          </div>
                  <!-- Mobile menu ends -->
                    <?php
                }
                if ($value->sidebar=='Testimonial')
                {
                    ?>
                     <hr class="styled-hr">
                     <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                                <?php $gettesti = $value->testimonial; ?>
                                <div class="specialpage_col">
                                    <?php
                                    $x = 0;
                                    foreach($gettesti as $k => $value) { ?>
                                <div class="size16 border-left italic margin-top">
                                    <div class="row">
                                        <div class="col-sm-3 text-center page-testi-image">
                                            <img src="<?php echo $imageLink.'/uploads/testimonialpic/'.$gettesti[$k]->picture; ?>" class="testi-img">
                                        </div>
                                        <div class="col-sm-9">

                                            <div></div>
                                            <div>
                                                <span class="fa fa-quote-left"></span>
                                                <?php echo $gettesti[$k]->message; ?>
                                             </div>
                                                <div class="text-right">- <?php echo $gettesti[$k]->name ?></div>
                                            </div>
                                        </div>
                                        <hr class="styled-hr">
                                </div>
                                 <?php $x = 1; } ?>
                                </div>

                    <?php
                }
                if ($value->sidebar=='Link')
                {
                     if($value->url != '' && $value->label != ''){ ?>
                     <hr class="styled-hr">
                      <a href="<?php echo $value->url; ?>" class="page-link"><?php echo $value->label; ?></a>
                    <?php }?>

                    <?php
                }
                 if ($value->sidebar=='News')
                {
                    ?>
                    <hr class="styled-hr">
                    <script type="text/javascript">
                                limitcount = <?php echo $value->limit; ?>;
                                </script>
                      <div class="specialpage_col">
                                    <div class="" ng-controller="NewsCtrl" ng-init="showmorenews()">
                                      <div class="row">
                                          <h4 class="blog-title-list">Latest News</h4>
                                          <div class="list-news-wrapper">
                                            <div ng-hide="newslist"> Loading News...</div>
                                            <div class="row list-title-blog" ng-repeat="news in newslist">
                                              <div class="col-sm-3 news-thumb-container" style="background-image: url('{[{ news.videothumb | returnYoutubeThumb }]} {[{ news.imagethumb | returnImageThumb }]}')" ng-click="redirectNews(news.newsslugs);" title="{[{ news.title }]}">
                                                <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}">
                                                <img src="{[{ news.videothumb | returnYoutubeThumb }]} {[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}">
                                                <div class="youtube-play" ng-show="news.videothumb"><img src="img/youtubeplay.png" ng-click="redirectNews(news.newsslugs);"/></div>
                                                <a href="/blog/{[{ news.newsslugs; }]}">
                                                </a>
                                            </div>


                                            <div class="col-xs-8 col-md-7 news-list-desc">
                                                <div class="row">

                                                    <div class="col-sm-12">
                                                      <span class="size25 font1 news-title" ng-click="redirectNews(news.newsslugs);">{[{ news.title }]}</span>
                                                  </div>
                                                  <div class="col-sm-12">
                                                      <strong><span class="thin-font1 orange">{[{ news.categorylist }]}</span></strong><span ng-show="news.name !=''" class="thin-font1"> / by <a href="/blog/author/{[{ news.name }]}"><strong><span class="orange">{[{ news.name }]}</span></strong></a></span> / {[{ news.date }]}
                                                      <br/><br/>
                                                  </div>
                                                  <div class="col-sm-12">
                                                      <div class="font1 size14 summary">
                                                        {[{ news.summary }]}
                                                        <br/><br/>
                                                    </div>
                                                </div>
                                                </div>
                                                <div style="clear:both"></div>
                                                        <br>
                                                  </div>
                                              </div>
                                                </div>
                                                </div>
                                                </div>
                                                </div>
                    <?php
                }
                if ($value->sidebar=='Archive')
                {
                    ?>
                     <hr class="styled-hr">
                     <?php $getarchives = $value->archive; ?>
                    <h2 class="font1"><span class="fa fa-calendar fa-1x"></span> Archives</h2>
                    <div class="ul-archives">
                        <?php
                       
                        foreach ($getarchives as $key => $value) { ?>
                        <a href="<?php echo $base_url; ?>/blog/archive/<?php echo $getarchives[$key]->month . "/" . $getarchives[$key]->year; ?>"><span class="fa fa-chevron-right"></span> <?php echo $getarchives[$key]->month . " " . $getarchives[$key]->year; ?></a>
                        <?php
                    } ?>
                    </div>

                    <?php
                }
                if ($value->sidebar=='Rss')
                {
                    ?>
                    <hr class="styled-hr">
                    <p class="size16"> <a href="<?php echo $base_url;?>/feed" target="_blank"><img class="rss" src="img/frontend/rss_feed.gif"> RSS Feed</a></p>
                    <?php
                }
                if ($value->sidebar=='Search')
                {
                    ?>
                   <hr class="styled-hr">
                      <div class="col-sm-12">
                      <div class="col-sm-10">
                      <input class="input-xs form-control" placeholder="Search" type="text" name="searchtext">

                      </div>
                      <div class="col-sm-2 align-left">
                      <span class="input-group-btn">
                        <button class="page-search" type="button">Go!</button>
                    </span>
                    </div>
                        </div>

                    <?php
                }

        }  //FOREACH

        ?></div>
        </div><?php
    } //rightsidebar
    ?>
    </div>

</div>
</div>

<!-- 

    <!-- Below Banner ends -->
