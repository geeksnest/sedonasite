<?php echo $this->getContent()?>
   
<style type="text/css">
    h4.blog-title-list {
     border-top: 0px solid #D0D0D0; 
    }
     img.testi-img{
    width: 68px;
    height: 68px;
    border-radius: 100%;
    border: solid 1px #A9A8A8;
    -webkit-filter: grayscale(100%);
    filter: grayscale(100%);
    -win-filter: grayscale(100%);
  }
</style>

<!-- Banner starts -->

<div ng-controller="SpecialpreviewCtrl">

<div ng-if="banner==true" class="specialbanner">
<div class="ban-container" style="background:{[{bgcolor}]} url(<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{imagethumb}]});
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center center;">
  </div>
 
  <div ng-if="imagethumbsubtitle || thumbdesc" class="banner-body">
  <div class="content-ban {[{align == 'center' ? 'col-sm-6 col-sm-offset-3' : ''}]}
  {[{align == 'left' ? 'col-sm-6' : ''}]} {[{align == 'right' ? 'col-sm-6 col-sm-offset-6' : ''}]}">
   <div class="{[{box == true ? 'white-box' : 'no-box'}]}">
    <span class="banner-title" style="color:{[{color}]}"> <span ng-bind="imagethumbsubtitle"></span> </span>
 
    <span class="banner-sub-title1" style="color:{[{color}]}"> <span ng-bind="thumbdesc"></span></span> <br/> <br/>
       <div ng-if="btnname" class="button"><a href="{[{btnlink}]}"> <span ng-bind="btnname"></span></a></div>
     </div>
    </div>
  </div> 

</div>


<!-- Banner ends -->
<div class="container-fluid">

    <div class="specialpage-content">
<!-- RESPONSIVE -->
<div class="col-sm-12 mobile-special-maincontent" id="second-div">

</div>
<!-- RESPONSIVE -->


<!-- RESPONSIVE -->

<div ng-if="pagetype=='Normal'" class="col-sm-12 mobile-normal-maincontent">
           <div class="normalpage-body size14">
              <div ng-bind-html="body1"></div>
            </div>
</div>

<!-- RESPONSIVE -->


<!--     LEFT SIDE BAR -->
<div ng-if="leftsidebar == true">
<div class="{[{leftsidebar == true && rightsidebar == true ? 'col-sm-2 specialpage-sidebar' : 'col-sm-2'}]} ">

    <div class="left-sidebar">
    <div ng-repeat="x in sidebarleft">

    <div ng-if="x.sidebar=='Image'">
      <hr class="styled-hr">
      <img ng-if="x.imglink==NULL" src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{x.img}]}" width="100%">

      <a ng-if="x.imglink!=NULL" target="_blank" href="{[{x.imglink}]}"><img src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{x.img}]}" width="100%"></a>
    </div>

     <div ng-if="x.sidebar=='Calendar'">
      <hr class="styled-hr">
      <div class="pos-rlt">

        <div class="calendar" ng-model="eventSources" calendar="calendar1" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div>

      </div>
     </div>

      <div ng-if="x.sidebar=='Menu'">
         <div class="special-menu">
            <div><a class="special-top-menu">Sedona Healing Arts</a></div>
            <nav>
              <ul style="list-style:disc">
                <li ng-repeat="mobmenu in shortcode" ng-if="mobmenu.menuID == x.menuID ">
                  <a class="special-menu-text" href="javascript:void(0);"><span ng-bind="mobmenu.subname"></span></a>

                  <ul class="no-list" ng-repeat="mob in children">
                    <li ng-repeat="mobsub1 in mob.child1" ng-if="mobsub1.parent==mobmenu.submenuID">
                    <span class="glyphicon glyphicon-menu-right" style="color:#58666e;">
                      <a class="special-menu-text" href="javascript:void(0);"><span ng-bind="mobsub1.subname"></span></a>

                      <ul class="no-list">
                        <li ng-repeat="mobsub2 in mob.child2" ng-if="mobsub2.parent==mobsub1.submenuID">
                         <span class="glyphicon glyphicon-menu-right" style="color:#58666e;">
                          <a class="special-menu-text" href="javascript:void(0);"><span ng-bind="mobsub2.subname"></span></a>

                          <ul class="no-list">
                            <li ng-repeat="mobsub3 in mob.child3" ng-if="mobsub3.parent==mobsub2.submenuID">
                            <span class="glyphicon glyphicon-menu-right" style="color:#58666e;">
                              <a class="special-menu-text" href="javascript:void(0);"><span ng-bind="mobsub3.subname"></span></a>
                            
                              <ul class="no-list">
                              <li ng-repeat="mobsub4 in mob.child4" ng-if="mobsub4.parent==mobsub3.submenuID">
                                  <span class="glyphicon glyphicon-menu-right" style="color:#58666e;">
                                  <a class="special-menu-text" href="javascript:void(0);"><span ng-bind="mobsub4.subname"></span></a>
                                </li>
                              </ul>

                            </li>
                          </ul>

                        </li>
                      </ul>

                    </li>
                  </ul>


                </li>

              </ul>
            </nav>
          </div>
      </div>

      <div ng-if="x.sidebar=='Testimonial'">
        <div class="size18"><h4 class="font1 italic">What People Are Saying</h4></div>
                             <div ng-repeat="x in testi | limitTo:x.limittest">
                              <div class="specialpage_col">  
                                <div class="size16 border-left italic margin-top special-testi-wrapper">
                                  <div class="row">
                                    <div class="col-sm-12 text-center page-testi-image">
                                      <img src="<?php echo $imageLink; ?>/uploads/testimonialpic/{[{x.picture}]}" class="testi-img">
                                    </div>
                                    <div class="col-sm-12">
                                      <div></div>
                                      <div>
                                        <span class="fa fa-quote-left"></span>
                                        {[{x.message}]}
                                      </div>
                                      <div class="text-right">-  {[{x.name}]} </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
      </div>

      <div ng-if="x.sidebar=='Link'">
      <div ng-if="x.url != '' && x.label != ''">
        <hr class="styled-hr">
        <a target="_blank" href="{[{x.url}]}" class="page-link"> {[{x.label}]} </a>
      </div>
      </div>

       <div ng-if="x.sidebar=='News'">
         <h4 class="blog-title-list">Latest News </h4>
                        <div class="list-news-wrapper">
                          <div class="row list-title-blog specialpage-title-blog" ng-repeat="news in news | limitTo:x.limitnews">
                            <!-- <div class="col-sm-12 news-thumb-container" title="{[{ news.title }]}"> -->
                            <div class="col-sm-3 news-thumb-container specialpage-news-thumb" style="background-image: url('{[{ news.videothumb | returnYoutubeThumb }]} {[{ news.imagethumb | returnImageThumb }]}')" ng-click="redirectNews(news.newsslugs);" title="{[{ news.title }]}">
                              <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}">
                              <?php if ($news->videothumb) {
                                ?>
                                <div ng-if="news.videothumb" class="youtube-play"><a href="/blog/{[{news.newsslugs}]}"><img src="img/youtubeplay.png"/></a></div>
                                <?php
                              } ?>

                              <a href="/blog/{[{news.newsslugs}]}">
                              </a>
                            </div>
                      
                            <div class="news-list-desc specialpage-news-desc">
                              <div class="row">
                                <div class="">
                                  <span class="size25 font1 news-title">{[{ news.title }]}</span>
                                </div>
                                <div class="">
                                  <span class="thin-font1 orange">{[{ news.categorylist }]}</span><span ng-show="news.name !=''" class="thin-font1"> / by <span class="orange">{[{ news.name }]}</span></span> / {[{ news.date }]}
                                  <br/><br/>
                                </div>
                                <div class="col-sm-12" ng-if="news.summary">
                                  <div class="font1 size14 summary">
                                    {[{ news.summary }]}
                                    <br/><br/>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
      </div>
      <div ng-if="x.sidebar=='Archive'">
        <h3 class="font1"><span class="fa fa-calendar fa-1x"></span> Archives</h3>
                      <div class="ul-archives">
                       <ul ng-repeat="archive in archive" class="ul-archive">
                         <li>
                        <a style="color:#ff8421" href="javascript:void(0);"><span class="fa fa-chevron-right"></span> {[{archive.month}]} {[{archive.year}]}</a>
                         </li>
                       </ul>
                    </div>
      </div>
      <div ng-if="x.sidebar=='RSS'">
        <hr class="styled-hr"> <br/>
        <p class="size16"> <a href="<?php echo $base_url;?>/feed" target="_blank"><img class="rss" src="img/frontend/rss_feed.gif"> RSS Feed</a></p> <br/> <br/>
      </div>

    </div>  <!-- ng-repeat -->
    
    </div>
        </div>
</div> <!-- if leftsidebar -->
        <div class=" {[{leftsidebar == false && rightsidebar == false ? 'col-sm-12' : ''}]} {[{leftsidebar == true && rightsidebar == false ? 'col-sm-10' : ''}]} {[{leftsidebar == false && rightsidebar == true ? 'col-sm-10' : ''}]} {[{leftsidebar == true && rightsidebar == true ? 'col-sm-8' : ''}]} special-maincontent">

          <!-- Main@@! -->
           <div ng-if="pagetype=='Normal' || page=='Normal'" class="normalpage-body size14">
            <div ng-bind-html="body1"></div>

            <div ng-if="!body1" style='color: #bdbdbd;text-align:center '> Place content here </div>
           
            </div>
      
        <!-- MAIN CONTENT-->

<!-- Modules@@! -->

           <div ng-if="pagetype=='Special' || page=='Special'" class="normalpage-body size14"  id="main-div">
           <div ng-repeat="(indexX,y) in column track by indexX"> 
                <div class="row">
                    <div ng-repeat="x in y.row">
                    <div ng-if="$index == 0">
                      <div class="specialpage_col {[{y.column == 1 ? 'col-sm-12' : ''}]} {[{y.column == 2 ? 'col-sm-6' : ''}]} {[{(y.column == 3) && (leftsidebar==true && rightsidebar == true) ? 'special-col3' : ''}]} 
                      {[{y.column == 3 ? 'col-sm-4' : ''}]} {[{ y.column == 4 ? 'col-sm-3' : ''}]}
                      {[{(y.column == 4) && (leftsidebar==true && rightsidebar == true) ? 'special-col4' : ''}]} {[{(y.column == 4) && (leftsidebar==true && rightsidebar == false) ? 'special-col4-no-one' : ''}]}
                      {[{(y.column == 4) && (leftsidebar==false && rightsidebar == true) ? 'special-col4-no-one' : ''}]}">

                       <div ng-if="x.col=='TEXT'">
                          <p ng-bind-html="trustAsHtml(x.colcontent)"></p>
                       </div>
                      <div ng-if="x.col=='IMAGE'">
                        <div ng-if="x.colimglink==null" class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{x.colimg}]}'); background-size:cover;background-repeat:no-repeat;background-position:center center;min-height:400px;margin-bottom:4px"></div>
                        <a ng-if="x.colimglink!=null" href="{[{x.colimglink}]}" target="_blank"><div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{x.colimg}]}');  background-size:cover; background-repeat:no-repeat;background-position:center center;min-height:400px;margin-bottom:4px"></div></a>
                      </div>
                      <div ng-if="x.col=='DIVIDER'">
                      <hr style="height:{[{x.colheight}]}px;background-color:{[{x.colcolor}]}">
                      </div>


                      <div ng-if="x.col=='BUTTON'">
                    <div ng-if="x.colbtnname!='' && x.colbtnlink!=''" style="text-align:{[{x.colposition}]}">
                         <a href="{[{x.colbtnlink.indexOf('http://') > -1 ? '' : 'http://'}]}{[{x.colbtnlink}]}" target="_blank" class="module-link" style="background-color:{[{x.colbgcolor}]};color:{[{x.colfcolor}]};padding:{[{x.colpadtop}]}px {[{x.colpadside}]}%;font-size:{[{x.colfont}]}px">{[{x.colbtnname}]}</a>
                         </div>
                      </div>

                      <div ng-if="x.col=='TESTIMONIAL'">
                           <hr class="styled-hr">

                               <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                                 <div ng-repeat="t in testi | filter:x.coltestimonialcat | limitTo:x.coltestimonial">

                                    <div class="size16 border-left italic margin-top special-testi-wrapper">
                                        <div class="row">
                                            <div class="col-sm-3 text-center page-testi-image">
                                                <img src="<?php echo $imageLink; ?>/uploads/testimonialpic/{[{t.picture}]}" class="testi-img">
                                            </div>
                                            <div class="col-sm-9">
                                                <div></div>
                                                <div>
                                                    <span class="fa fa-quote-left"></span>
                                                   {[{t.message}]}
                                                 </div>
                                                    <div class="text-right">-  {[{t.name}]} </div>
                                                </div>
                                            </div>
                                            <hr class="styled-hr">
                                    </div>
                                    </div>
                                   
                     </div>
                     <div ng-if="x.col=='NEWS'">
                      <h4 class="blog-title-list">Latest News</h4>
                      <div class="list-news-wrapper special-news-wrapper">
                       <div class="row list-title-blog specialpage-title-blog" ng-repeat="news in news | limitTo:x.colnewslimit">
                        <div class="col-sm-3 news-thumb-container {[{y.column == 3 || y.column == 4 ? 'specialpage-news-thumb' : ''}]}" style="background-image: url('{[{ news.videothumb | returnYoutubeThumb }]} {[{ news.imagethumb | returnImageThumb }]}')" ng-click="redirectNews(news.newsslugs);" title="{[{ news.title }]}">
                          <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}">
                          <a href="/blog/{[{news.newsslugs}]}">
                          </a>
                        </div>
                        <div class="col-xs-8 col-md-7 news-list-desc {[{y.column == 3 || y.column == 4 ? 'specialpage-news-desc' : ''}]}">
                          <div class="row">

                            <div class="">
                              <span class="size25 font1 news-title" ng-click="redirectNews(news.newsslugs)">{[{news.title}]}</span>
                            </div>
                            <div class="">
                              <strong><span class="thin-font1 orange">
                               {[{ news.categorylist }]}
                             </span></strong><span ng-show="news.name !=''" class="thin-font1"> / by <a href="/blog/author/Jacinto Escano"><strong><span class="orange">{[{ news.name }]}</span></strong></a></span> / {[{ news.date }]}
                             <br><br>
                           </div>
                           <div class="col-sm-12">
                            <div class="font1 size14 summary">
                              {[{ news.summary }]}
                              <br><br>
                            </div>
                          </div>
                        </div>
                        <div style="clear:both"></div>
                        <br>
                      </div>
                    </div>
                  </div>
                </div>

                     <div ng-if="x.col=='CONTACT'">
                     <div class="col-sm-12">
                      <div class="col-sm-12">
                        <h1>Contact Us</h1>
                      </div>
                      <div class="{[{y.column == 1 || y.column == 2 ? 'col-sm-6' : ''}]}">
                          <!-- Contact form -->
                          <form name="formContactUs" class="form-validation booking ng-pristine ng-invalid ng-invalid-required" ng-submit="submit(feedback)">
                            <!-- Name -->
                              <div class="control-group margin-bot8">
                                <div class="">
                                  <label class="control-label size14 col-sm-12" for="name">Subject
                                    <span class="red">*</span>
                                  </label>
                                  <div class="col-sm-12">
                                    <input type="text" class="form-control textbox margin-bot8" id="" required="required" ng-model="feedback.subject">
                                  </div>
                                </div>
                              </div>
                              <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="name">Name
                                    <span class="red">*</span>
                                  </label>
                                <div class="{[{y.column == 1 || y.column == 2 ? 'col-sm-6' : 'col-sm-12'}]}">
                                    <input type="text" class="form-control textbox margin-bot8" id="fname" placeholder="First Name" required="required" ng-model="feedback.fname">
                                  </div>
                                <div class="{[{y.column == 1 || y.column == 2 ? 'col-sm-6' : 'col-sm-12'}]}">
                                    <input type="text" class="form-control textbox" id="lname" placeholder="Last Name" required="required" ng-model="feedback.lname">
                                  </div>
                                </div>
                              </div>
                              <!-- Email -->
                              <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="email">Email
                                    <span class="red">*</span>
                                  </label>
                                  <div class="col-sm-12">
                                    <span class="red hidden booking-email">Invalid Email Address</span>
                                    <input type="email" class="textbox form-control margin-bot8" id="email" required="required" ng-model="feedback.email" ng-change="onemail(feedback.email)">
                                  </div>
                                </div>
                              </div>
                              <!-- Comment -->
                              <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                    <span class="red">*</span>
                                  </label>
                                  <div class="col-sm-12">
                                    <textarea class="form-control input-large textarea" id="feedback" rows="5" required="required" ng-model="feedback.message"></textarea>
                                  </div>
                                </div>
                              </div>
                              <!-- Buttons -->
                              <div class="form-actions margin-bot8">
                                <div class="row">
                                  <div class="col-sm-12">
                                    <button type="submit" class="btn btn-sedona" ng-disabled="formContactUs.$invalid||res==false">Submit</button>
                                  </div>
                                </div>
                              </div>
                              <!-- Buttons -->
                            </form>
                          <div class="clearfix"></div>
                        </div>

                        <div class="{[{y.column == 1 || y.column == 2 ? 'col-sm-6' : 'col-sm-12'}]} storeinfo">
                          <div class="storeinfo-details">
                            <h3 class="orange"> <span ng-bind="x.coltitle"> </span> </h3>
                            <div class="title3">
                              <span class="titleb">Phone:</span>
                              <span ng-bind="x.colphone" class="thin-font3"> </span> <br/>  
                              <span class="titleb">Email:</span> <span ng-bind="x.colemail" class="thin-font3"> </span> <br/>
                              <span class="titleb">Hours:</span> <span ng-bind="x.colhours" class="thin-font3"> </span>  <br/> <br/>
                            </div>
                            <div class="title3">
                              <span class="titleb text-top">Address:</span>
                              <br/>
                              <div ng-repeat="x in x.str">
                                <span class="thin-font3" ng-bind="x"> </span> <br/>
                              </div>
                            </div>

                            <div class="wsite-map">
                              <iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 200px; margin-top: 10px; margin-bottom: 10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3273.673285579057!2d-111.7637428!3d34.8644358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872da1564d73fd15%3A0x3e31d123930491bf!2s201+Hwy+179%2C+Sedona%2C+AZ+86336%2C+USA!5e0!3m2!1sen!2sph!4v1434348998372"></iframe>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                        </div>

                     </div>
                     </div>

                      </div>  <!-- specialpage_col -->
                      </div> <!-- if index -->

                      <div ng-if="$index == 1">
                      <div class="specialpage_col {[{y.column == 2 ? 'col-sm-6' : ''}]} {[{(y.column == 3) && (leftsidebar==true && rightsidebar == true) ? 'special-col3' : ''}]} 
                      {[{y.column == 3 ? 'col-sm-4' : ''}]}
                      {[{ y.column == 4 ? 'col-sm-3' : ''}]}
                      {[{(y.column == 4) && (leftsidebar==true && rightsidebar == true) ? 'special-col4' : ''}]} {[{(y.column == 4) && (leftsidebar==true && rightsidebar == false) ? 'special-col4-no-one' : ''}]}
                      {[{(y.column == 4) && (leftsidebar==false && rightsidebar == true) ? 'special-col4-no-one' : ''}]}">
                       <div ng-if="x.col=='TEXT'">
                          <p ng-bind-html="trustAsHtml(x.colcontent)"></p>
                       </div>
                      <div ng-if="x.col=='IMAGE'">
                        <div ng-if="x.colimglink==null" class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{x.colimg}]}'); background-size:cover;background-repeat:no-repeat;background-position:center center;min-height:400px;margin-bottom:4px"></div>
                        <a ng-if="x.colimglink!=null" href="{[{x.colimglink}]}" target="_blank"><div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{x.colimg}]}');  background-size:cover; background-repeat:no-repeat;background-position:center center;min-height:400px;margin-bottom:4px"></div></a>
                      </div>
                      <div ng-if="x.col=='DIVIDER'">
                      <hr style="height:{[{x.colheight}]}px;background-color:{[{x.colcolor}]}">
                      </div>


                      <div ng-if="x.col=='BUTTON'">
                    <div ng-if="x.colbtnname!='' && x.colbtnlink!=''" style="text-align:{[{x.colposition}]}">
                         <a href="{[{x.colbtnlink.indexOf('http://') > -1 ? '' : 'http://'}]}{[{x.colbtnlink}]}" target="_blank" class="module-link" style="background-color:{[{x.colbgcolor}]};color:{[{x.colfcolor}]};padding:{[{x.colpadtop}]}px {[{x.colpadside}]}%;font-size:{[{x.colfont}]}px">{[{x.colbtnname}]}</a>
                         </div>
                      </div>

                      <div ng-if="x.col=='TESTIMONIAL'">
                           <hr class="styled-hr">
                               <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                                 <div ng-repeat="t in testi | filter:x.coltestimonialcat | limitTo:x.coltestimonial">
                                    <div class="size16 border-left italic margin-top special-testi-wrapper">
                                        <div class="row">
                                            <div class="col-sm-3 text-center page-testi-image">
                                                <img src="<?php echo $imageLink; ?>/uploads/testimonialpic/{[{t.picture}]}" class="testi-img">
                                            </div>
                                            <div class="col-sm-9">
                                                <div></div>
                                                <div>
                                                    <span class="fa fa-quote-left"></span>
                                                   {[{t.message}]}
                                                 </div>
                                                    <div class="text-right">-  {[{t.name}]} </div>
                                                </div>
                                            </div>
                                            <hr class="styled-hr">
                                    </div>
                                    </div>
                                   
                     </div>
                     <div ng-if="x.col=='NEWS'">
                      <h4 class="blog-title-list">Latest News</h4>
                      <div class="list-news-wrapper special-news-wrapper">
                       <div class="row list-title-blog specialpage-title-blog" ng-repeat="news in news | limitTo:x.colnewslimit">
                        <div class="col-sm-3 news-thumb-container {[{y.column == 3 || y.column == 4 ? 'specialpage-news-thumb' : ''}]}" style="background-image: url('{[{ news.videothumb | returnYoutubeThumb }]} {[{ news.imagethumb | returnImageThumb }]}')" ng-click="redirectNews(news.newsslugs);" title="{[{ news.title }]}">
                          <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}">
                          <a href="/blog/{[{news.newsslugs}]}">
                          </a>
                        </div>
                        <div class="col-xs-8 col-md-7 news-list-desc {[{y.column == 3 || y.column == 4 ? 'specialpage-news-desc' : ''}]} ">
                          <div class="row">

                            <div class="">
                              <span class="size25 font1 news-title" ng-click="redirectNews(news.newsslugs)">{[{news.title}]}</span>
                            </div>
                            <div class="">
                              <strong><span class="thin-font1 orange">
                               {[{ news.categorylist }]}
                             </span></strong><span ng-show="news.name !=''" class="thin-font1"> / by <a href="/blog/author/Jacinto Escano"><strong><span class="orange">{[{ news.name }]}</span></strong></a></span> / {[{ news.date }]}
                             <br><br>
                           </div>
                           <div class="col-sm-12">
                            <div class="font1 size14 summary">
                              {[{ news.summary }]}
                              <br><br>
                            </div>
                          </div>
                        </div>
                        <div style="clear:both"></div>
                        <br>
                      </div>
                    </div>
                  </div>
                </div>
                     <div ng-if="x.col=='CONTACT'">
                     <div class="col-sm-12">
                      <div class="col-sm-12">
                        <h1>Contact Us</h1>
                      </div>
                      <div class="{[{y.column == 1 || y.column == 2 ? 'col-sm-6' : ''}]}">
                          <!-- Contact form -->
                          <form name="formContactUs" class="form-validation booking ng-pristine ng-invalid ng-invalid-required" ng-submit="submit(feedback)">
                            <!-- Name -->
                              <div class="control-group margin-bot8">
                                <div class="">
                                  <label class="control-label size14 col-sm-12" for="name">Subject
                                    <span class="red">*</span>
                                  </label>
                                  <div class="col-sm-12">
                                    <input type="text" class="form-control textbox margin-bot8" id="" required="required" ng-model="feedback.subject">
                                  </div>
                                </div>
                              </div>
                              <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="name">Name
                                    <span class="red">*</span>
                                  </label>
                                 <div class="{[{y.column == 1 || y.column == 2 ? 'col-sm-6' : 'col-sm-12'}]}">
                                    <input type="text" class="form-control textbox margin-bot8" id="fname" placeholder="First Name" required="required" ng-model="feedback.fname">
                                  </div>
                                <div class="{[{y.column == 1 || y.column == 2 ? 'col-sm-6' : 'col-sm-12'}]}">
                                    <input type="text" class="form-control textbox" id="lname" placeholder="Last Name" required="required" ng-model="feedback.lname">
                                  </div>
                                </div>
                              </div>
                              <!-- Email -->
                              <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="email">Email
                                    <span class="red">*</span>
                                  </label>
                                  <div class="col-sm-12">
                                    <span class="red hidden booking-email">Invalid Email Address</span>
                                    <input type="email" class="textbox form-control margin-bot8" id="email" required="required" ng-model="feedback.email" ng-change="onemail(feedback.email)">
                                  </div>
                                </div>
                              </div>
                              <!-- Comment -->
                              <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                    <span class="red">*</span>
                                  </label>
                                  <div class="col-sm-12">
                                    <textarea class="form-control input-large textarea" id="feedback" rows="5" required="required" ng-model="feedback.message"></textarea>
                                  </div>
                                </div>
                              </div>
                              <!-- Buttons -->
                              <div class="form-actions margin-bot8">
                                <div class="row">
                                  <div class="col-sm-12">
                                    <button type="submit" class="btn btn-sedona" ng-disabled="formContactUs.$invalid||res==false">Submit</button>
                                  </div>
                                </div>
                              </div>
                              <!-- Buttons -->
                            </form>
                          <div class="clearfix"></div>
                        </div>

                        <div class="{[{y.column == 1 || y.column == 2 ? 'col-sm-6' : 'col-sm-12'}]} storeinfo">
                          <div class="storeinfo-details">
                            <h3 class="orange"> <span ng-bind="x.coltitle"> </span> </h3>
                            <div class="title3">
                              <span class="titleb">Phone:</span>
                              <span ng-bind="x.colphone" class="thin-font3"> </span> <br/>  
                              <span class="titleb">Email:</span> <span ng-bind="x.colemail" class="thin-font3"> </span> <br/>
                              <span class="titleb">Hours:</span> <span ng-bind="x.colhours" class="thin-font3"> </span>  <br/> <br/>
                            </div>
                            <div class="title3">
                              <span class="titleb text-top">Address:</span>
                              <br/>
                              <div ng-repeat="x in x.str">
                                <span class="thin-font3" ng-bind="x"> </span> <br/>
                              </div>
                            </div>

                            <div class="wsite-map">
                              <iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 200px; margin-top: 10px; margin-bottom: 10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3273.673285579057!2d-111.7637428!3d34.8644358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872da1564d73fd15%3A0x3e31d123930491bf!2s201+Hwy+179%2C+Sedona%2C+AZ+86336%2C+USA!5e0!3m2!1sen!2sph!4v1434348998372"></iframe>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                        </div>

                     </div>
                     </div>
                      </div>  <!-- specialpage_col -->
                      </div> <!-- if index -->
                      <div ng-if="$index == 2">
                     <div class="specialpage_col {[{(y.column == 3) && (leftsidebar==true && rightsidebar == true) ? 'special-col33' : ''}]} 
                      {[{y.column == 3 ? 'col-sm-4' : ''}]}
                      {[{ y.column == 4 ? 'col-sm-3' : ''}]}
                      {[{(y.column == 4) && (leftsidebar==true && rightsidebar == true) ? 'special-col4-3' : ''}]} {[{(y.column == 4) && (leftsidebar==true && rightsidebar == false) ? 'special-col4-no-3' : ''}]}
                      {[{(y.column == 4) && (leftsidebar==false && rightsidebar == true) ? 'special-col4-no-3' : ''}]}">
                       <div ng-if="x.col=='TEXT'">
                          <p ng-bind-html="trustAsHtml(x.colcontent)"></p>
                       </div>
                      <div ng-if="x.col=='IMAGE'">
                        <div ng-if="x.colimglink==null" class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{x.colimg}]}'); background-size:cover;background-repeat:no-repeat;background-position:center center;min-height:400px;margin-bottom:4px"></div>
                        <a ng-if="x.colimglink!=null" href="{[{x.colimglink}]}" target="_blank"><div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{x.colimg}]}');  background-size:cover; background-repeat:no-repeat;background-position:center center;min-height:400px;margin-bottom:4px"></div></a>
                      </div>
                      <div ng-if="x.col=='DIVIDER'">
                      <hr style="height:{[{x.colheight}]}px;background-color:{[{x.colcolor}]}">
                      </div>


                      <div ng-if="x.col=='BUTTON'">
                    <div ng-if="x.colbtnname!='' && x.colbtnlink!=''" style="text-align:{[{x.colposition}]}">
                         <a href="{[{x.colbtnlink.indexOf('http://') > -1 ? '' : 'http://'}]}{[{x.colbtnlink}]}" target="_blank" class="module-link" style="background-color:{[{x.colbgcolor}]};color:{[{x.colfcolor}]};padding:{[{x.colpadtop}]}px {[{x.colpadside}]}%;font-size:{[{x.colfont}]}px">{[{x.colbtnname}]}</a>
                         </div>
                      </div>

                      <div ng-if="x.col=='TESTIMONIAL'">
                           <hr class="styled-hr">
                               <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                                 <div ng-repeat="t in testi | filter:x.coltestimonialcat | limitTo:x.coltestimonial">
                                    <div class="size16 border-left italic margin-top special-testi-wrapper">
                                        <div class="row">
                                            <div class="col-sm-3 text-center page-testi-image">
                                                <img src="<?php echo $imageLink; ?>/uploads/testimonialpic/{[{t.picture}]}" class="testi-img">
                                            </div>
                                            <div class="col-sm-9">
                                                <div></div>
                                                <div>
                                                    <span class="fa fa-quote-left"></span>
                                                   {[{t.message}]}
                                                 </div>
                                                    <div class="text-right">-  {[{t.name}]} </div>
                                                </div>
                                            </div>
                                            <hr class="styled-hr">
                                    </div>
                                    </div>
                                   
                     </div>
                     <div ng-if="x.col=='NEWS'">
                      <h4 class="blog-title-list">Latest News</h4>
                      <div class="list-news-wrapper special-news-wrapper">
                       <div class="row list-title-blog specialpage-title-blog" ng-repeat="news in news | limitTo:x.colnewslimit">
                        <div class="col-sm-3 news-thumb-container specialpage-news-thumb" style="background-image: url('{[{ news.videothumb | returnYoutubeThumb }]} {[{ news.imagethumb | returnImageThumb }]}')" ng-click="redirectNews(news.newsslugs);" title="{[{ news.title }]}">
                          <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}">
                          <a href="/blog/{[{news.newsslugs}]}">
                          </a>
                        </div>
                        <div class="col-xs-8 col-md-7 news-list-desc specialpage-news-desc">
                          <div class="row">

                            <div class="">
                              <span class="size25 font1 news-title" ng-click="redirectNews(news.newsslugs)">{[{news.title}]}</span>
                            </div>
                            <div class="">
                              <strong><span class="thin-font1 orange">
                               {[{ news.categorylist }]}
                             </span></strong><span ng-show="news.name !=''" class="thin-font1"> / by <a href="/blog/author/Jacinto Escano"><strong><span class="orange">{[{ news.name }]}</span></strong></a></span> / {[{ news.date }]}
                             <br><br>
                           </div>
                           <div class="col-sm-12">
                            <div class="font1 size14 summary">
                              {[{ news.summary }]}
                              <br><br>
                            </div>
                          </div>
                        </div>
                        <div style="clear:both"></div>
                        <br>
                      </div>
                    </div>
                  </div>
                </div>
                     <div ng-if="x.col=='CONTACT'">
                     <div class="col-sm-12">
                      <div class="col-sm-12">
                        <h1>Contact Us</h1>
                      </div>
                      <div class="{[{y.column == 1 || y.column == 2 ? 'col-sm-6' : ''}]}">
                          <!-- Contact form -->
                          <form name="formContactUs" class="form-validation booking ng-pristine ng-invalid ng-invalid-required" ng-submit="submit(feedback)">
                            <!-- Name -->
                              <div class="control-group margin-bot8">
                                <div class="">
                                  <label class="control-label size14 col-sm-12" for="name">Subject
                                    <span class="red">*</span>
                                  </label>
                                  <div class="col-sm-12">
                                    <input type="text" class="form-control textbox margin-bot8" id="" required="required" ng-model="feedback.subject">
                                  </div>
                                </div>
                              </div>
                              <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="name">Name
                                    <span class="red">*</span>
                                  </label>
                                <div class="{[{y.column == 1 || y.column == 2 ? 'col-sm-6' : 'col-sm-12'}]}">
                                    <input type="text" class="form-control textbox margin-bot8" id="fname" placeholder="First Name" required="required" ng-model="feedback.fname">
                                  </div>
                                <div class="{[{y.column == 1 || y.column == 2 ? 'col-sm-6' : 'col-sm-12'}]}">
                                    <input type="text" class="form-control textbox" id="lname" placeholder="Last Name" required="required" ng-model="feedback.lname">
                                  </div>
                                </div>
                              </div>
                              <!-- Email -->
                              <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="email">Email
                                    <span class="red">*</span>
                                  </label>
                                  <div class="col-sm-12">
                                    <span class="red hidden booking-email">Invalid Email Address</span>
                                    <input type="email" class="textbox form-control margin-bot8" id="email" required="required" ng-model="feedback.email" ng-change="onemail(feedback.email)">
                                  </div>
                                </div>
                              </div>
                              <!-- Comment -->
                              <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                    <span class="red">*</span>
                                  </label>
                                  <div class="col-sm-12">
                                    <textarea class="form-control input-large textarea" id="feedback" rows="5" required="required" ng-model="feedback.message"></textarea>
                                  </div>
                                </div>
                              </div>
                              <!-- Buttons -->
                              <div class="form-actions margin-bot8">
                                <div class="row">
                                  <div class="col-sm-12">
                                    <button type="submit" class="btn btn-sedona" ng-disabled="formContactUs.$invalid||res==false">Submit</button>
                                  </div>
                                </div>
                              </div>
                              <!-- Buttons -->
                            </form>
                          <div class="clearfix"></div>
                        </div>

                        <div class="{[{y.column == 1 || y.column == 2 ? 'col-sm-6' : 'col-sm-12'}]} storeinfo">
                          <div class="storeinfo-details">
                            <h3 class="orange"> <span ng-bind="x.coltitle"> </span> </h3>
                            <div class="title3">
                              <span class="titleb">Phone:</span>
                              <span ng-bind="x.colphone" class="thin-font3"> </span> <br/>  
                              <span class="titleb">Email:</span> <span ng-bind="x.colemail" class="thin-font3"> </span> <br/>
                              <span class="titleb">Hours:</span> <span ng-bind="x.colhours" class="thin-font3"> </span>  <br/> <br/>
                            </div>
                            <div class="title3">
                              <span class="titleb text-top">Address:</span>
                              <br/>
                              <div ng-repeat="x in x.str">
                                <span class="thin-font3" ng-bind="x"> </span> <br/>
                              </div>
                            </div>

                            <div class="wsite-map">
                              <iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 200px; margin-top: 10px; margin-bottom: 10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3273.673285579057!2d-111.7637428!3d34.8644358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872da1564d73fd15%3A0x3e31d123930491bf!2s201+Hwy+179%2C+Sedona%2C+AZ+86336%2C+USA!5e0!3m2!1sen!2sph!4v1434348998372"></iframe>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                        </div>

                     </div>
                     </div>
                      </div>  <!-- specialpage_col -->
                      </div> <!-- if index -->

                      <div ng-if="$index == 3">
                      <div class="specialpage_col col-sm-3
                      {[{leftsidebar==true && rightsidebar == true ? 'special-col4-4' : ''}]} {[{leftsidebar==true && rightsidebar == false ? 'special-col4-no-4' : ''}]}
                      {[{leftsidebar==false && rightsidebar == true ? 'special-col4-no-4' : ''}]}">
                       <div ng-if="x.col=='TEXT'">
                          <p ng-bind-html="trustAsHtml(x.colcontent)"></p>
                       </div>
                      <div ng-if="x.col=='IMAGE'">
                        <div ng-if="x.colimglink==null" class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{x.colimg}]}'); background-size:cover;background-repeat:no-repeat;background-position:center center;min-height:400px;margin-bottom:4px"></div>
                        <a ng-if="x.colimglink!=null" href="{[{x.colimglink}]}" target="_blank"><div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{x.colimg}]}');  background-size:cover; background-repeat:no-repeat;background-position:center center;min-height:400px;margin-bottom:4px"></div></a>
                      </div>
                      <div ng-if="x.col=='DIVIDER'">
                      <hr style="height:{[{x.colheight}]}px;background-color:{[{x.colcolor}]}">
                      </div>


                      <div ng-if="x.col=='BUTTON'">
                    <div ng-if="x.colbtnname!='' && x.colbtnlink!=''" style="text-align:{[{x.colposition}]}">
                         <a href="{[{x.colbtnlink.indexOf('http://') > -1 ? '' : 'http://'}]}{[{x.colbtnlink}]}" target="_blank" class="module-link" style="background-color:{[{x.colbgcolor}]};color:{[{x.colfcolor}]};padding:{[{x.colpadtop}]}px {[{x.colpadside}]}%;font-size:{[{x.colfont}]}px">{[{x.colbtnname}]}</a>
                         </div>
                      </div>

                      <div ng-if="x.col=='TESTIMONIAL'">
                           <hr class="styled-hr">
                               <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                                 <div ng-repeat="t in testi | filter:x.coltestimonialcat | limitTo:x.coltestimonial">
                                    <div class="size16 border-left italic margin-top special-testi-wrapper">
                                        <div class="row">
                                            <div class="col-sm-3 text-center page-testi-image">
                                                <img src="<?php echo $imageLink; ?>/uploads/testimonialpic/{[{t.picture}]}" class="testi-img">
                                            </div>
                                            <div class="col-sm-9">
                                                <div></div>
                                                <div>
                                                    <span class="fa fa-quote-left"></span>
                                                   {[{t.message}]}
                                                 </div>
                                                    <div class="text-right">-  {[{t.name}]} </div>
                                                </div>
                                            </div>
                                            <hr class="styled-hr">
                                    </div>
                                    </div>
                                   
                     </div>
                     <div ng-if="x.col=='NEWS'">
                      <h4 class="blog-title-list">Latest News</h4>
                      <div class="list-news-wrapper special-news-wrapper">
                       <div class="row list-title-blog specialpage-title-blog" ng-repeat="news in news | limitTo:x.colnewslimit">
                        <div class="col-sm-3 news-thumb-container specialpage-news-thumb" style="background-image: url('{[{ news.videothumb | returnYoutubeThumb }]} {[{ news.imagethumb | returnImageThumb }]}')" ng-click="redirectNews(news.newsslugs);" title="{[{ news.title }]}">
                          <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}">
                          <a href="/blog/{[{news.newsslugs}]}">
                          </a>
                        </div>
                        <div class="col-xs-8 col-md-7 news-list-desc specialpage-news-desc">
                          <div class="row">

                            <div class="">
                              <span class="size25 font1 news-title" ng-click="redirectNews(news.newsslugs)">{[{news.title}]}</span>
                            </div>
                            <div class="">
                              <strong><span class="thin-font1 orange">
                               {[{ news.categorylist }]}
                             </span></strong><span ng-show="news.name !=''" class="thin-font1"> / by <a href="/blog/author/Jacinto Escano"><strong><span class="orange">{[{ news.name }]}</span></strong></a></span> / {[{ news.date }]}
                             <br><br>
                           </div>
                           <div class="col-sm-12">
                            <div class="font1 size14 summary">
                              {[{ news.summary }]}
                              <br><br>
                            </div>
                          </div>
                        </div>
                        <div style="clear:both"></div>
                        <br>
                      </div>
                    </div>
                  </div>
                </div>
                    <div ng-if="x.col=='CONTACT'">
                     <div class="col-sm-12">
                      <div class="col-sm-12">
                        <h1>Contact Us</h1>
                      </div>
                      <div class="{[{y.column == 1 || y.column == 2 ? 'col-sm-6' : ''}]}">
                          <!-- Contact form -->
                          <form name="formContactUs" class="form-validation booking ng-pristine ng-invalid ng-invalid-required" ng-submit="submit(feedback)">
                            <!-- Name -->
                              <div class="control-group margin-bot8">
                                <div class="">
                                  <label class="control-label size14 col-sm-12" for="name">Subject
                                    <span class="red">*</span>
                                  </label>
                                  <div class="col-sm-12">
                                    <input type="text" class="form-control textbox margin-bot8" id="" required="required" ng-model="feedback.subject">
                                  </div>
                                </div>
                              </div>
                              <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="name">Name
                                    <span class="red">*</span>
                                  </label>
                                <div class="{[{y.column == 1 || y.column == 2 ? 'col-sm-6' : 'col-sm-12'}]}">
                                    <input type="text" class="form-control textbox margin-bot8" id="fname" placeholder="First Name" required="required" ng-model="feedback.fname">
                                  </div>
                                <div class="{[{y.column == 1 || y.column == 2 ? 'col-sm-6' : 'col-sm-12'}]}">
                                    <input type="text" class="form-control textbox" id="lname" placeholder="Last Name" required="required" ng-model="feedback.lname">
                                  </div>
                                </div>
                              </div>
                              <!-- Email -->
                              <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="email">Email
                                    <span class="red">*</span>
                                  </label>
                                  <div class="col-sm-12">
                                    <span class="red hidden booking-email">Invalid Email Address</span>
                                    <input type="email" class="textbox form-control margin-bot8" id="email" required="required" ng-model="feedback.email" ng-change="onemail(feedback.email)">
                                  </div>
                                </div>
                              </div>
                              <!-- Comment -->
                              <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                    <span class="red">*</span>
                                  </label>
                                  <div class="col-sm-12">
                                    <textarea class="form-control input-large textarea" id="feedback" rows="5" required="required" ng-model="feedback.message"></textarea>
                                  </div>
                                </div>
                              </div>
                              <!-- Buttons -->
                              <div class="form-actions margin-bot8">
                                <div class="row">
                                  <div class="col-sm-12">
                                    <button type="submit" class="btn btn-sedona" ng-disabled="formContactUs.$invalid||res==false">Submit</button>
                                  </div>
                                </div>
                              </div>
                              <!-- Buttons -->
                            </form>
                          <div class="clearfix"></div>
                        </div>

                        <div class="{[{y.column == 1 || y.column == 2 ? 'col-sm-6' : 'col-sm-12'}]} storeinfo">
                          <div class="storeinfo-details">
                            <h3 class="orange"> <span ng-bind="x.coltitle"> </span> </h3>
                            <div class="title3">
                              <span class="titleb">Phone:</span>
                              <span ng-bind="x.colphone" class="thin-font3"> </span> <br/>  
                              <span class="titleb">Email:</span> <span ng-bind="x.colemail" class="thin-font3"> </span> <br/>
                              <span class="titleb">Hours:</span> <span ng-bind="x.colhours" class="thin-font3"> </span>  <br/> <br/>
                            </div>
                            <div class="title3">
                              <span class="titleb text-top">Address:</span>
                              <br/>
                              <div ng-repeat="x in x.str">
                                <span class="thin-font3" ng-bind="x"> </span> <br/>
                              </div>
                            </div>

                            <div class="wsite-map">
                              <iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 200px; margin-top: 10px; margin-bottom: 10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3273.673285579057!2d-111.7637428!3d34.8644358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872da1564d73fd15%3A0x3e31d123930491bf!2s201+Hwy+179%2C+Sedona%2C+AZ+86336%2C+USA!5e0!3m2!1sen!2sph!4v1434348998372"></iframe>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                        </div>

                     </div>
                     </div>
                      </div>  <!-- specialpage_col -->
                      </div> <!-- if index -->


                    </div> <!-- ng-repeat inner -->
               </div> <!-- row -->
               </div> <!-- ng-repeat -->
           
            </div>

        </div>


  <div ng-if="rightsidebar == true">
      <div class="{[{leftsidebar == true && rightsidebar == true ? 'col-sm-2 specialpage-sidebar' : 'col-sm-2'}]} ">

        <div class="right-sidebar">
           <!-- RIGHT SIDE  -->
 <div ng-repeat="x in sidebarright">

    <div ng-if="x.sidebar=='Image'">
      <hr class="styled-hr">
       <img ng-if="x.imglink==NULL" src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{x.img}]}" width="100%">

      <a ng-if="x.imglink!=NULL" target="_blank" href="{[{x.imglink}]}"><img src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{x.img}]}" width="100%"></a>
    </div>

     <div ng-if="x.sidebar=='Calendar'">
      <hr class="styled-hr">
      <div class="pos-rlt">

        <div class="calendar" ng-model="eventSources" calendar="calendar1" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div>

      </div>
     </div>

      <div ng-if="x.sidebar=='Menu'">
         <div class="special-menu">
            <div><a class="special-top-menu">Sedona Healing Arts</a></div>
            <nav>
              <ul style="list-style:disc">
                <li ng-repeat="mobmenu in shortcode" ng-if="mobmenu.menuID == x.menuID ">
                  <a class="special-menu-text" href="javascript:void(0);"><span ng-bind="mobmenu.subname"></span></a>

                  <ul class="no-list" ng-repeat="mob in children">
                    <li ng-repeat="mobsub1 in mob.child1" ng-if="mobsub1.parent==mobmenu.submenuID">
                    <span class="glyphicon glyphicon-menu-right" style="color:#58666e;">
                      <a class="special-menu-text" href="javascript:void(0);"><span ng-bind="mobsub1.subname"></span></a>

                      <ul class="no-list">
                        <li ng-repeat="mobsub2 in mob.child2" ng-if="mobsub2.parent==mobsub1.submenuID">
                         <span class="glyphicon glyphicon-menu-right" style="color:#58666e;">
                          <a class="special-menu-text" href="javascript:void(0);"><span ng-bind="mobsub2.subname"></span></a>

                          <ul class="no-list">
                            <li ng-repeat="mobsub3 in mob.child3" ng-if="mobsub3.parent==mobsub2.submenuID">
                            <span class="glyphicon glyphicon-menu-right" style="color:#58666e;">
                              <a class="special-menu-text" href="javascript:void(0);"><span ng-bind="mobsub3.subname"></span></a>
                            
                              <ul class="no-list">
                              <li ng-repeat="mobsub4 in mob.child4" ng-if="mobsub4.parent==mobsub3.submenuID">
                                  <span class="glyphicon glyphicon-menu-right" style="color:#58666e;">
                                  <a class="special-menu-text" href="javascript:void(0);"><span ng-bind="mobsub4.subname"></span></a>
                                </li>
                              </ul>

                            </li>
                          </ul>

                        </li>
                      </ul>

                    </li>
                  </ul>


                </li>

              </ul>
            </nav>
          </div>
      </div>

      <div ng-if="x.sidebar=='Testimonial'">
        <div class="size18"><h4 class="font1 italic">What People Are Saying</h4></div>
                             <div ng-repeat="x in testi | limitTo:x.limittest">
                              <div class="specialpage_col">  
                                <div class="size16 border-left italic margin-top special-testi-wrapper">
                                  <div class="row">
                                    <div class="col-sm-12 text-center page-testi-image">
                                      <img src="<?php echo $imageLink; ?>/uploads/testimonialpic/{[{x.picture}]}" class="testi-img">
                                    </div>
                                    <div class="col-sm-12">
                                      <div></div>
                                      <div>
                                        <span class="fa fa-quote-left"></span>
                                        {[{x.message}]}
                                      </div>
                                      <div class="text-right">-  {[{x.name}]} </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
      </div>

      <div ng-if="x.sidebar=='Link'">
      <div ng-if="x.url != '' && x.label != ''" class='lnk'>
        <hr class="styled-hr">
        <a target="_blank" href="{[{x.url}]}" class="page-link"> {[{x.label}]} </a>
      </div>
      </div>

      <div ng-if="x.sidebar=='News'">
         <h4 class="blog-title-list">Latest News </h4>
                        <div class="list-news-wrapper">
                          <div class="row list-title-blog specialpage-title-blog" ng-repeat="news in news | limitTo:x.limitnews">
                            <!-- <div class="col-sm-12 news-thumb-container" title="{[{ news.title }]}"> -->
                            <div class="col-sm-3 news-thumb-container specialpage-news-thumb" style="background-image: url('{[{ news.videothumb | returnYoutubeThumb }]} {[{ news.imagethumb | returnImageThumb }]}')" ng-click="redirectNews(news.newsslugs);" title="{[{ news.title }]}">
                              <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}">
                              <?php if ($news->videothumb) {
                                ?>
                                <div ng-if="news.videothumb" class="youtube-play"><a href="/blog/{[{news.newsslugs}]}"><img src="img/youtubeplay.png"/></a></div>
                                <?php
                              } ?>

                              <a href="/blog/{[{news.newsslugs}]}">
                              </a>
                            </div>
                      
                            <div class="news-list-desc specialpage-news-desc">
                              <div class="row">
                                <div class="">
                                  <span class="size25 font1 news-title">{[{ news.title }]}</span>
                                </div>
                                <div class="">
                                  <span class="thin-font1 orange">{[{ news.categorylist }]}</span><span ng-show="news.name !=''" class="thin-font1"> / by <span class="orange">{[{ news.name }]}</span></span> / {[{ news.date }]}
                                  <br/><br/>
                                </div>
                                <div class="col-sm-12" ng-if="news.summary">
                                  <div class="font1 size14 summary">
                                    {[{ news.summary }]}
                                    <br/><br/>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
      </div>
      <div ng-if="x.sidebar=='Archive'">
        <h3 class="font1"><span class="fa fa-calendar fa-1x"></span> Archives</h3>
                      <div class="ul-archives">
                       <ul ng-repeat="archive in archive" class="ul-archive">
                         <li>
                        <a style="color:#ff8421" href="javascript:void(0);"><span class="fa fa-chevron-right"></span> {[{archive.month}]} {[{archive.year}]}</a>
                         </li>
                       </ul>
                    </div>
      </div>
      <div ng-if="x.sidebar=='RSS'">
        <hr class="styled-hr"> <br/>
        <p class="size16"> <a href="<?php echo $base_url;?>/feed" target="_blank"><img class="rss" src="img/frontend/rss_feed.gif"> RSS Feed</a></p> <br/> <br/>
      </div>

    </div>  <!-- ng-repeat -->
        </div>


      </div>
  </div> <!--if rightside true -->

    </div>

</div>
</div>
<script type="text/javascript">
   var MyDiv1 = document.getElementById('main-div');
   var MyDiv2 = document.getElementById('second-div');
   MyDiv2.innerHTML = MyDiv1.innerHTML;
</script>
<!-- 

    <!-- Below Banner ends -->
</div>