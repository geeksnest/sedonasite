<?php echo $this->getContent()?>
   
<style type="text/css">
    h4.blog-title-list {
     border-top: 0px solid #D0D0D0; 
    }
     img.testi-img{
    width: 68px;
    height: 68px;
    border-radius: 100%;
    border: solid 1px #A9A8A8;
    -webkit-filter: grayscale(100%);
    filter: grayscale(100%);
    -win-filter: grayscale(100%);
  }
</style>

<!-- i wasted so much time on this :) do not destroy -->

<!-- Banner starts -->
<?php 
if ($datapages) {
?>
<div class="banner-container banner" style="background-image:url('<?php echo $imageLink;?>/uploads/pageimage/<?php echo $banner;?>');" title="<?php echo $title; ?>">
  <img src="<?php echo $imageLink;?>/uploads/pageimage/<?php echo $banner;?>" class="pinterest-img" alt="<?php echo $title; ?>" />
  <div class="black-box">
    <span class="banner-title"><?php echo $title;?></span>
    <br/>
    <?php if($subtitle1 !=''){echo "<span class='banner-sub-title1'>".$subtitle1."</span><br/>";}?>
    <?php if($subtitle2 !=''){echo "<span class='banner-sub-title1'>".$subtitle2."</span><br/>";}?>
     <br/>
    <div class="button"><a href="<?php echo $base_url;?>/booking" class="learn-sedona"><?php echo $buttontitle;?></a></div>
  </div>

</div>

<!-- Banner ends -->

<div class="container">
  <div class="row">
    <div class="col-sm-8 no-margin content-text">
      <div class="phar no-padding">
        <!-- <span class="size20"><h2><?php echo $title;?></h2></span>   -->
        <span class="size18">
          <?php echo $body;?>
        </span>
      </div>

      <div class="phar no-padding">
        <div class="size18"><h2 class="font1 italic"><!-- What People Are Saying --></h2></div>

        <?php
        $getginfo = $testimonials;
        foreach ($getginfo as $key => $value) {
          ?>
          <div class="size16 border-left italic margin-left margin-top">
            <div class="row">
              <div class="col-sm-2 text-center">
                <?php
                  if(!empty($getginfo[$key]->picture)){

                ?>
                <img src="<?php echo $this->config->application->amazonlink?>/uploads/testimonialpic/<?php echo $getginfo[$key]->picture;?>" class="testi-img">
                <?php
                }else{
                ?>
                <img src="img/testidefaultimage.png" class="testi-img">
                <?php
                }
                ?>
              </div>
              <div class="col-sm-9">

              <div></div>
                <div>
                <span class="fa fa-quote-left"></span>
                <?php echo $getginfo[$key]->message;?>
                </div>
                <div class="text-right">- <?php echo $getginfo[$key]->name;?></div>
              </div>
            </div>
            <hr class="styled-hr">
          </div>
          <?php } ?> <br/>

        </div>

      </div>

      <div class="col-sm-4">
        <!-- ===== SIDE BAR 1 ===== -->
        <div class="phar center no-padding sidebar1">
          <hr class="styled-hr">
          <h2><?php echo $title;?></h2>
          <?php if($subtitle1 !=''){echo "<h3>".$subtitle1."</h3>";}?>
          <h3 class="grayfont">Price: <?php echo $serviceprice;?></h3>
          <div class="button">
            <a href="<?php echo $base_url;?>/booking" class="learn-sedona">BOOK ONLINE NOW</a>
          </div><br/>
          <div class="center">
            <span class="size20">
              - OR -<br/>
            </span>
            <span class="size20 bold">
              Call 928-282-3875
            </span>
            <span class="size20"><br/>
              to make an appointment<br/> <br/>
            </span>
          </div>
          <hr class="styled-hr">
        </div>
        <!-- ===== END SIDE BAR 1 ===== -->


        <!-- ===== SIDE BAR 2 ===== -->
        <?php echo $sidebar;?>
        <!-- ===== END SIDE BAR 2 ===== -->
      </div>
    </div>
  </div>

  <div class="container margin-bot100 margin-top80">
    <hr class="styled-hr">
    <div class="row padding-topbot20">
      <div class="col-sm-12 center">
        <span class="size25 font2">Call 928-282-3875 or <a href="<?php echo $base_url;?>/booking">CLICK HERE</a> to make an appointment.</span>
      </div>
    </div>
    <hr class="styled-hr">
    <br><br><br>
  </div>

<?php } ?>



<?php 
if ($slides) {
?>
<!-- Below SlidEr starts -->
    <div class="container-fluid1">

        <div id="myCarousel" class="carousel slide mycarousel" data-ride='carousel'>

<!-- Indicators -->
            <ol class="carousel-indicators" style="margin-bottom:-10px">
            <?php
            $nslides = json_decode($slides);
            $dst = 0;
            foreach ($nslides as $key => $value) {
            ?>
                <li data-target="#myCarousel" data-slide-to="<?php echo $dst; ?>"
                 class="<?php if($nslides[$key]->sort == 1){ echo 'active'; } ?>"></li>
            <?php $dst++; } ?>
            </ol>

<!-- Wrapper for Slides -->
            <div class="carousel-inner carousel-wrap">

            <?php
            foreach ($nslides as $key => $value) {
                ?>
                <div class="item <?php if($nslides[$key]->sort == 1){ echo 'active'; } ?>">
                    <div class="sidebarimage" style="background-image: url(<?php echo $this->config->application->amazonlink; ?>/uploads/slider/<?php echo $nslides[$key]->foldername.'/'.$nslides[$key]->img; ?>);">
                    </div>

                        <div class="banner-container banner-content-wrapper-green">
                        <div class='containerlang'>
                          <?php if($nslides[$key]->display=='yes') { ?>
                            <?php if($nslides[$key]->position=='right') { ?>
                            <div class="col-sm-7 col-sm-offset-5 slider-right slider slider<?php echo $key; ?>" style='padding:6vw'> <!-- RIGHT ALIGN-->
                            <?php if($nslides[$key]->box=='on') { ?>
                              <div class="Slider-content" > <!-- BOX ON-->

                                  <!--ARROW!-->
                               <div class='row'>
                            <div class='pull-left click1'>
                                <span class="slider-title" style="color:<?php echo $nslides[$key]->titlecolor; ?>;font-size:<?php $size=$nslides[$key]->titlesize/16; echo $size; ?>vw;line-height:<?php if ($nslides[$key]->titlesize>=90) {echo '6';}?>vw "><?php echo $nslides[$key]->title; ?></span>
                                </div>

                                <div class='pull-right' style='text-align:center;width:50px'> <a class='btn-lg clickme clickme<?php echo $key; ?>' id='clickme' onclick='slide(<?php echo $key; ?>)'>  <i class='fa fa-arrow-circle-down'> </i> </a>
                                 <a class='clickshow clickshow<?php echo $key; ?>'id='clickshow' onclick='hide(<?php echo $key; ?>)'>
                           <i class='fa fa-arrow-circle-up'> </i> </a>
                            </div>
                            <!-- Overflow Auto-->
                            </div>

                                <br>
                                <span class="slider-desc slider-desc<?php echo $key; ?>" style="color:<?php echo $nslides[$key]->desccolor; ?>;font-size:<?php $size=$nslides[$key]->descsize/16; echo $size; ?>vw;">
                                  <?php echo strip_tags($nslides[$key]->description); ?>
                                     <?php if($nslides[$key]->btnlink != ''){ ?>
                              <a href="<?php echo $nslides[$key]->btnlink; ?>" class="Services-learnmore"><?php echo $nslides[$key]->btnname; ?></a>
                              <?php }?>
                              </span>


                             </div>
                            <?php } else {?>
                            <div class="Services-outcontent"> <!-- BOX OFF-->
                            <!-- TITLE -->
                                 <!--ARROW!-->
                               <div class='row'>
                            <div class='pull-left click1'>
                                <span class="slider-title" style="color:<?php echo $nslides[$key]->titlecolor; ?>;font-size:<?php $size=$nslides[$key]->titlesize/16; echo $size; ?>vw;line-height:<?php if ($nslides[$key]->titlesize>=90) {echo '6';}?>vw "><?php echo $nslides[$key]->title; ?></span>
                                </div>
                                <div class='pull-right' style='color:black;text-align:center;width:50px'> <a class='btn-lg clickme clickme<?php echo $key; ?>' id='clickme' onclick='slide(<?php echo $key; ?>)'>  <i class='fa fa-arrow-circle-down'> </i> </a>
                                 <a class='clickshow clickshow<?php echo $key; ?>'id='clickshow' onclick='hide(<?php echo $key; ?>)'>
                           <i class='fa fa-arrow-circle-up'> </i> </a>
                            </div>
                            <!-- Overflow Auto-->
                            </div>
                                <br>
                                <span class="slider-desc slider-desc<?php echo $key; ?>" style="color:<?php echo $nslides[$key]->desccolor; ?>;font-size:<?php $size=$nslides[$key]->descsize/16; echo $size; ?>vw">
                                  <?php echo strip_tags($nslides[$key]->description); ?>

                               <?php if($nslides[$key]->btnlink != ''){ ?>
                              <a href="<?php echo $nslides[$key]->btnlink; ?>" class="Services-learnmore"><?php echo $nslides[$key]->btnname; ?></a>
                              <?php }?>
                              </span>


                             </div>
                             <?php } ?>


                      </div>

                      <?php } if($nslides[$key]->position=='center') { ?>
                            <div class="slider-center slider slider<?php echo $key; ?>" style='padding:6vw'> <!-- Center ALIGN-->

                            <?php if($nslides[$key]->box=='on') { ?>
                              <div class="Slider-content" > <!-- BOX ON-->

                                <!--ARROW!-->
                               <div class='row'>
                            <div class='pull-left  click1'>
                                <span class="slider-title" style="color:<?php echo $nslides[$key]->titlecolor; ?>;font-size:<?php $size=$nslides[$key]->titlesize/16; echo $size; ?>vw;line-height:<?php if ($nslides[$key]->titlesize>=90) {echo '6';}?>vw "><?php echo $nslides[$key]->title; ?></span>
                                </div>
                                <div class='pull-right' style='color:black;text-align:center;width:50px'> <a class='btn-lg clickme clickme<?php echo $key; ?>' id='clickme' onclick='slide(<?php echo $key; ?>)'>  <i class='fa fa-arrow-circle-down'> </i> </a>
                                 <a class='clickshow clickshowcenter clickshow<?php echo $key; ?>'id='clickshow' onclick='hide(<?php echo $key; ?>)'>
                           <i class='fa fa-arrow-circle-up'> </i> </a>
                            </div>
                            <!-- Overflow Auto-->
                            </div>

                                <br>
                                <span class="slider-desc slider-desc<?php echo $key; ?>" style="color:<?php echo $nslides[$key]->desccolor; ?>;font-size:<?php $size=$nslides[$key]->descsize/16; echo $size; ?>vw">
                                  <?php echo strip_tags($nslides[$key]->description); ?>

                               <?php if($nslides[$key]->btnlink != ''){ ?>
                              <a href="<?php echo $nslides[$key]->btnlink; ?>" class="Services-learnmore"><?php echo $nslides[$key]->btnname; ?></a>
                              <?php }?>
                              </span>

                             </div>
                            <?php } else {?>
                            <div class="Services-outcontent"> <!-- BOX OFF-->
                            <!-- TITLE -->
                                   <!--ARROW!-->
                                 <div class='row'>
                            <div class='pull-left  click1'>
                                <span class="slider-title" style="color:<?php echo $nslides[$key]->titlecolor; ?>;font-size:<?php $size=$nslides[$key]->titlesize/16; echo $size; ?>vw;line-height:<?php if ($nslides[$key]->titlesize>=90) {echo '6';}?>vw "><?php echo $nslides[$key]->title; ?></span>
                                </div>
                                <div class='pull-right' style='color:black;text-align:center;width:50px'> <a class='btn-lg clickme clickme<?php echo $key; ?>' id='clickme' onclick='slide(<?php echo $key; ?>)'>  <i class='fa fa-arrow-circle-down'> </i> </a>
                                 <a class='clickshow clickshowcenter clickshow<?php echo $key; ?>'id='clickshow' onclick='hide(<?php echo $key; ?>)'>
                           <i class='fa fa-arrow-circle-up'> </i> </a>
                            </div>
                            <!-- Overflow Auto-->
                            </div>

                                <br>
                                <span class="slider-desc slider-desc<?php echo $key; ?>" style="color:<?php echo $nslides[$key]->desccolor; ?>;font-size:<?php $size=$nslides[$key]->descsize/16; echo $size; ?>vw">
                                  <?php echo strip_tags($nslides[$key]->description); ?>

                                   <?php if($nslides[$key]->btnlink != ''){ ?>
                              <a href="<?php echo $nslides[$key]->btnlink; ?>" class="Services-learnmore"><?php echo $nslides[$key]->btnname; ?></a>
                              <?php }?>
                              </span>

                             </div>
                             <?php } ?>


                      </div>
                      <?php } else if ($nslides[$key]->position=='left') { ?>
                      <div class="col-sm-7 slider-left slider slider<?php echo $key; ?>" style='padding:6vw' id='slider-left'> <!-- LEFT ALIGN-->
                         <?php if($nslides[$key]->box=='on') { ?>
                              <div class="Slider-content"> <!-- BOX ON-->

                            <!--ARROW!-->
                               <div class='row'>
                            <div class='pull-left  click1'>
                                <span class="slider-title" style="color:<?php echo $nslides[$key]->titlecolor; ?>;font-size:<?php $size=$nslides[$key]->titlesize/16; echo $size; ?>vw;line-height:<?php if ($nslides[$key]->titlesize>=90) {echo '6';}?>vw "><?php echo $nslides[$key]->title; ?></span>
                                </div>
                                <div class='pull-right' style='color:black;text-align:center;width:50px'> <a class='btn-lg clickme clickme<?php echo $key; ?>' id='clickme' onclick='slide(<?php echo $key; ?>)'>  <i class='fa fa-arrow-circle-down'> </i> </a>
                                 <a class='clickshow clickshow<?php echo $key; ?>'id='clickshow' onclick='hide(<?php echo $key; ?>)'>
                           <i class='fa fa-arrow-circle-up'> </i> </a>
                            </div>
                            <!-- Overflow Auto-->
                            </div>

                                <br>
                                <span class="slider-desc slider-desc<?php echo $key; ?>" style="color:<?php echo $nslides[$key]->desccolor; ?>;font-size:<?php $size=$nslides[$key]->descsize/16; echo $size; ?>vw">
                                  <?php echo strip_tags($nslides[$key]->description); ?>


                               <?php if($nslides[$key]->btnlink != ''){ ?>
                              <a href="<?php echo $nslides[$key]->btnlink; ?>" class="Services-learnmore"><?php echo $nslides[$key]->btnname; ?></a>
                              <?php }?>
                              </span>


                             </div>
                            <?php } else {?>
                            <div class="Services-outcontent"> <!-- BOX OFF-->

                            <div class='row'>
                            <div class='pull-left click1' stye='border:1px solid black'>
                                <span class="slider-title" style="color:<?php echo $nslides[$key]->titlecolor; ?>;font-size:<?php $size=$nslides[$key]->titlesize/16; echo $size; ?>vw;line-height:<?php if ($nslides[$key]->titlesize>=90) {echo '6';}?>vw "><?php echo $nslides[$key]->title; ?></span>
                                </div>
                                <div class='pull-right click' style='text-align:center;width:50px'> <a class='clickme clickme<?php echo $key; ?>'
                                 id='clickme' onclick='slide(<?php echo $key; ?>)'>  <i class='fa fa-arrow-circle-down'> </i> </a>
                                <a class='clickshow clickshow<?php echo $key; ?>' id='clickshow' onclick='hide(<?php echo $key; ?>)'>
                           <i class='fa fa-arrow-circle-up'> </i> </a>

                            </div>
                            </div>

                                <br>
                                <span class="slider-desc slider-desc<?php echo $key; ?>" style="color:<?php echo $nslides[$key]->desccolor; ?>;font-size:<?php $size=$nslides[$key]->descsize/16; echo $size; ?>vw">
                                  <?php echo strip_tags($nslides[$key]->description); ?>

                                  <?php if($nslides[$key]->btnlink != ''){ ?>
                              <a href="<?php echo $nslides[$key]->btnlink; ?>" class="Services-learnmore"><?php echo $nslides[$key]->btnname; ?></a>
                              <?php }?>
                              </span>

                             </div>
                             <?php } ?>
                  </div>
                  <?php } }?>

                  </div>

                </div>

                </div>

            <?php } ?>
            </div>
<!-- Controls -->
            <a type="button" class="left-arrow left carousel-control" href="javascript:void(0);" id="myCarousel" onclick="$('#myCarousel').carousel('prev')">
                <span class="icon-prev"></span>
            </a>
            <a type="button" class="right-arrow right carousel-control" href="javascript:void(0);" id="myCarousel" onclick="$('#myCarousel').carousel('prev')">
                <span class="icon-next"></span>
            </a>
        </div>
<!-- slider ends -->
    </div>

<?php 
}
?>
  <?php
          $getginfo = $panel;
          foreach ($getginfo as $key => $value) {
    ?>
    <?php if ($getginfo[$key]->thumboption=='custom') {
      ?>
      <div class="container-fluid Homepage-custom" style="min-height:<?php $custom = $getginfo[$key]->thumbsize/16; echo $custom;  ?>vw">
        <?php
      } else{
        ?>
    <div class="container-fluid Services">
    <?php } ?>
        <div class="Services-bg" style="background:linear-gradient( rgba(0, 0, 0, 0), rgba(250, 250, 250, 0) ), url('<?php echo $imageLink.'/uploads/pageimage/'. $getginfo[$key]->imagethumb  ?>');
                                        background-position:
                                          <?php
                                            if($getginfo[$key]->textalign=='left') { echo   'center right!important'; }
                                            elseif($getginfo[$key]->textalign=='right') { echo  'center left!important'; }
                                            else { echo   'center center!important'; }
                                          ?>">
          {#this is the banner#}
        </div>

        <div class="Services-body <?php echo $getginfo[$key]->classname; ?>">
            <?php if($getginfo[$key]->textalign == 'right') { ?>  {#RIGHT ALIGN#}
                      <div class="col-sm-6 col-sm-offset-6 Services-content" style="color:<?php echo $getginfo[$key]->color; ?>">

                        <?php  if($getginfo[$key]->box == 'true') { ?>
                          <div class="Services-box">
                        <?php } else { ?>
                          <div>
                        <?php }

                  } elseif ($getginfo[$key]->textalign == 'left') { ?> {#LEFT ALIGN#}
                      <div class="col-sm-6 Services-content" style="color:<?php echo $getginfo[$key]->color; ?>">

                        <?php  if($getginfo[$key]->box == 'true') { ?>
                          <div class="Services-box">
                        <?php } else { ?>
                          <div>
                        <?php }

                  } else { ?> {#CENTER ALIGN#}
                      <div class="col-sm-6 col-sm-offset-3 Services-content" style="color:<?php echo $getginfo[$key]->color; ?>">

                        <?php  if($getginfo[$key]->box == 'true') { ?>
                          <div class="Services-box">
                        <?php } else { ?>
                          <div>
                        <?php }
                  }  ?>

                  {#continuation of the Services-content div#}
                  <span class="Services-title"><?php echo $getginfo[$key]->imagethumbsubtitle; ?></span>
                  <span class="Services-description">
                    <?php echo strip_tags($getginfo[$key]->thumbdesc); ?>
                    <?php echo $color[$key]; ?>
                    <a href="<?php echo $base_url;?>/healing/<?php echo $getginfo[$key]->pageslugs;?>" class="Services-learnmore">Learn More</a>
                  </span>
                </div> {#end of Services-box#}
              </div> {#end of Services-content#}
        </div> {# end of body #}
    </div> {# END OF CONTAINER-FLUID #}
            <style>
              @media screen and (max-width:767px) {
                .<?php echo $getginfo[$key]->classname; ?> {
                  background:<?php echo $getginfo[$key]->bgcolor; ?>;
                }
              }
            </style>
    <?php } ?> {#end of foreach#}

<!-- Below Banner ends -->


<div ng-controller="SpecialCtrl">
<!-- do not touch -->
<input type="hidden" id="slugs" value="<?php echo $pageslugs; ?> ">
<!-- do not touch -->

<div ng-if="ban.banner==true || ban.banner=='true'" class="specialbanner">

<div class="ban-container" style="background:{[{ban.bgcolor}]} url(<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{ban.imagethumb}]});
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center center;">
  </div>
 
  <div ng-if="ban.imagethumbsubtitle || ban.thumbdesc" class="banner-body">
  <div class="content-ban {[{ban.align == 'center' ? 'col-sm-6 col-sm-offset-3' : ''}]}
  {[{ban.align == 'left' ? 'col-sm-6' : ''}]} {[{ban.align == 'right' ? 'col-sm-6 col-sm-offset-6' : ''}]}">
   <div class="{[{ban.box == true ? 'white-box' : 'no-box'}]}">
    <span class="banner-title" style="color:{[{ban.color}]}"> <span ng-bind="ban.imagethumbsubtitle"></span> </span>
    <span class="banner-sub-title1" style="color:{[{ban.color}]}"> <span ng-bind="ban.thumbdesc"></span></span> <br/>
       <div ng-if="ban.btnname" class="button"><a href="{[{ban.btnlink}]}"> <span ng-bind="ban.btnname"></span></a></div>
     </div>
    </div>
  </div> 

</div>


<div class="container-fluid">

    <div class="specialpage-content">

<!-- RESPONSIVE SPECIAL -->
<div class="col-sm-12 mobile-special-maincontent" id="second-div">
</div>
<!--  -->


<!-- RESPONSIVE NORMAL-->

<div ng-if="pagetype=='Normal'" class="col-sm-12 mobile-normal-maincontent">
           <div class="normalpage-body size14">
              <div ng-bind-html="body1"></div>
            </div>
</div>

<!-- -->


<!--     LEFT SIDE BAR -->
<div ng-if="leftsidebar == true">
<div class="{[{leftsidebar == true && rightsidebar == true ? 'col-sm-2 specialpage-sidebar' : 'col-sm-2'}]} ">

    <div class="left-sidebar">
    <div ng-repeat="x in sidebarleft">

    <div ng-if="x.sidebar=='Image'">
      <hr class="styled-hr">
      <img ng-if="x.imglink==NULL" src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{x.img}]}" width="100%">

      <a ng-if="x.imglink!=NULL" target="_blank" href="{[{x.imglink}]}"><img src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{x.img}]}" width="100%"></a>
    </div>

     <div ng-if="x.sidebar=='Calendar'">
      <hr class="styled-hr">
      <div class="pos-rlt">

        <div class="calendar" ng-model="eventSources" calendar="calendar1" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div>

      </div>
     </div>

      <div ng-if="x.sidebar=='Menu'">
         <div class="special-menu">
            <div><a class="special-top-menu">Sedona Healing Arts</a></div>
            <nav>
              <ul style="list-style:disc">
                <li ng-repeat="mobmenu in shortcode" ng-if="mobmenu.menuID == x.menuID ">
                  <a class="special-menu-text" href="{[{mobmenu.sublink}]}"><span ng-bind="mobmenu.subname"></span></a>

                  <ul class="no-list" ng-repeat="mob in children">
                    <li ng-repeat="mobsub1 in mob.child1" ng-if="mobsub1.parent==mobmenu.submenuID">
                    <span class="glyphicon glyphicon-menu-right" style="color:#58666e;">
                      <a class="special-menu-text" href="{[{mobsub1.sublink}]}"><span ng-bind="mobsub1.subname"></span></a>

                      <ul class="no-list">
                        <li ng-repeat="mobsub2 in mob.child2" ng-if="mobsub2.parent==mobsub1.submenuID">
                         <span class="glyphicon glyphicon-menu-right" style="color:#58666e;">
                          <a class="special-menu-text" href="{[{mobsub2.sublink}]}"><span ng-bind="mobsub2.subname"></span></a>

                          <ul class="no-list">
                            <li ng-repeat="mobsub3 in mob.child3" ng-if="mobsub3.parent==mobsub2.submenuID">
                            <span class="glyphicon glyphicon-menu-right" style="color:#58666e;">
                              <a class="special-menu-text" href="{[{mobsub3.sublink}]}"><span ng-bind="mobsub3.subname"></span></a>
                            
                              <ul class="no-list">
                              <li ng-repeat="mobsub4 in mob.child4" ng-if="mobsub4.parent==mobsub3.submenuID">
                                  <span class="glyphicon glyphicon-menu-right" style="color:#58666e;">
                                  <a class="special-menu-text" href="{[{mobsub4.sublink}]}"><span ng-bind="mobsub4.subname"></span></a>
                                </li>
                              </ul>

                            </li>
                          </ul>

                        </li>
                      </ul>

                    </li>
                  </ul>


                </li>

              </ul>
            </nav>
          </div>
      </div>

      <div ng-if="x.sidebar=='Testimonial'">
        <div class="size18"><h4 class="font1 italic">What People Are Saying</h4></div>
                             <div ng-repeat="x in testi | limitTo:x.limittest">
                              <div class="specialpage_col">  
                                <div class="size16 border-left italic margin-top special-testi-wrapper">
                                  <div class="row">
                                    <div class="col-sm-12 text-center page-testi-image">
                                      <img src="<?php echo $imageLink; ?>/uploads/testimonialpic/{[{x.picture}]}" class="testi-img">
                                    </div>
                                    <div class="col-sm-12">
                                      <div></div>
                                      <div>
                                        <span class="fa fa-quote-left"></span>
                                        {[{x.message}]}
                                      </div>
                                      <div class="text-right">-  {[{x.name}]} </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
      </div>

      <div ng-if="x.sidebar=='Link'">
      <div ng-if="x.url != '' && x.label != ''">
        <hr class="styled-hr">
        <a target="_blank" href="{[{x.url}]}" class="page-link"> {[{x.label}]} </a>
      </div>
      </div>

       <div ng-if="x.sidebar=='News'">
         <h4 class="blog-title-list">Latest News </h4>
                        <div class="list-news-wrapper">
                          <div class="row list-title-blog specialpage-title-blog" ng-repeat="news in news | limitTo:x.limitnews">
                            <!-- <div class="col-sm-12 news-thumb-container" title="{[{ news.title }]}"> -->
                            <div class="col-sm-3 news-thumb-container specialpage-news-thumb" style="background-image: url('{[{ news.videothumb | returnYoutubeThumb }]} {[{ news.imagethumb | returnImageThumb }]}')" ng-click="redirectNews(news.newsslugs);" title="{[{ news.title }]}">
                              <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}">
                              <?php if ($news->videothumb) {
                                ?>
                                <div ng-if="news.videothumb" class="youtube-play"><a href="/blog/{[{news.newsslugs}]}"><img src="img/youtubeplay.png"/></a></div>
                                <?php
                              } ?>

                              <a href="/blog/{[{news.newsslugs}]}">
                              </a>
                            </div>
                      
                            <div class="news-list-desc specialpage-news-desc">
                              <div class="row">
                                <div class="">
                                  <span class="size25 font1 news-title">{[{ news.title }]}</span>
                                </div>
                                <div class="">
                                  <span class="thin-font1 orange">{[{ news.categorylist }]}</span><span ng-show="news.name !=''" class="thin-font1"> / by <span class="orange">{[{ news.name }]}</span></span> / {[{ news.date }]}
                                  <br/><br/>
                                </div>
                                <div class="col-sm-12" ng-if="news.summary">
                                  <div class="font1 size14 summary">
                                    {[{ news.summary }]}
                                    <br/><br/>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
      </div>
      <div ng-if="x.sidebar=='Archive'">
        <h3 class="font1"><span class="fa fa-calendar fa-1x"></span> Archives</h3>
                      <div class="ul-archives">
                       <ul ng-repeat="archive in archive" class="ul-archive">
                         <li>
                        <a style="color:#ff8421" href="<?php echo $base_url; ?>/blog/archive/{[{archive.month}]}/{[{archive.year}]}"><span class="fa fa-chevron-right"></span> {[{archive.month}]} {[{archive.year}]}</a>
                         </li>
                       </ul>
                    </div>
      </div>
      <div ng-if="x.sidebar=='RSS'">
        <hr class="styled-hr"> <br/>
        <p class="size16"> <a href="<?php echo $base_url;?>/feed" target="_blank"><img class="rss" src="img/frontend/rss_feed.gif"> RSS Feed</a></p> <br/> <br/>
      </div>

    </div>  <!-- ng-repeat -->
    
    </div>
        </div>
</div> <!-- if leftsidebar -->
        <div class=" {[{leftsidebar == false && rightsidebar == false ? 'col-sm-12' : ''}]} {[{leftsidebar == true && rightsidebar == false ? 'col-sm-10' : ''}]} {[{leftsidebar == false && rightsidebar == true ? 'col-sm-10' : ''}]} {[{leftsidebar == true && rightsidebar == true ? 'col-sm-8' : ''}]} special-maincontent">

          <!-- Main@@! -->
           <div ng-if="pagetype=='Normal' || page=='Normal'" class="normalpage-body size14">
            <div ng-bind-html="body1"></div>

            <div ng-if="!body1" style='color: #bdbdbd;text-align:center '> Place content here </div>
           
            </div>
      
        <!-- MAIN CONTENT-->

<!-- Modules@@! -->

           <div ng-if="pagetype=='Special' || page=='Special'" class="normalpage-body size14"  id="main-div">
           <div ng-repeat="(indexX,y) in column track by indexX">
         
                <div class="row">
                    <div ng-repeat="x in y">
                    <div ng-if="$index == 0">
                   
                      <div class="specialpage_col {[{y.type == 1 ? 'col-sm-12' : ''}]} {[{y.type == 2 ? 'col-sm-6' : ''}]} {[{(y.type == 3) && (leftsidebar==true && rightsidebar == true) ? 'special-col3' : ''}]} 
                      {[{y.type == 3 ? 'col-sm-4' : ''}]} {[{ y.type == 4 ? 'col-sm-3' : ''}]}
                      {[{(y.type == 4) && (leftsidebar==true && rightsidebar == true) ? 'special-col4' : ''}]} {[{(y.type == 4) && (leftsidebar==true && rightsidebar == false) ? 'special-col4-no-one' : ''}]}
                      {[{(y.type == 4) && (leftsidebar==false && rightsidebar == true) ? 'special-col4-no-one' : ''}]}">

                       <div ng-if="x.col=='TEXT'">
                          <p ng-bind-html="trustAsHtml(x.content)"></p>
                       </div>
                      <div ng-if="x.col=='IMAGE'">
                        <div ng-if="x.link==null" class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{x.image}]}'); background-size:cover;background-repeat:no-repeat;background-position:center center;min-height:400px;margin-bottom:4px"></div>
                        <a ng-if="x.link!=null" href="{[{x.link}]}" target="_blank"><div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{x.image}]}');  background-size:cover; background-repeat:no-repeat;background-position:center center;min-height:400px;margin-bottom:4px"></div></a>
                      </div>
                      <div ng-if="x.col=='DIVIDER'">
                      <hr style="height:{[{x.height}]}px;background-color:{[{x.color}]}">
                      </div>


                      <div ng-if="x.col=='BUTTON'">
                    <div ng-if="x.btnname!='' && x.btnlink!=''" style="text-align:{[{x.position}]}">
                         <a href="{[{x.btnlink.indexOf('http://') > -1 ? '' : 'http://'}]}{[{x.btnlink}]}" target="_blank" class="module-link" style="background-color:{[{x.bgcolor}]};color:{[{x.fcolor}]};padding:{[{x.padtop}]}px {[{x.padside}]}%;font-size:{[{x.font}]}px">{[{x.btnname}]}</a>
                         </div>
                      </div>

                      <div ng-if="x.col=='TESTIMONIAL'">
                           <hr class="styled-hr">

                               <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                                 <div ng-repeat="t in testi | filter:x.category | limitTo:x.limitcount">

                                    <div class="size16 border-left italic margin-top special-testi-wrapper">
                                        <div class="row">
                                            <div class="col-sm-3 text-center page-testi-image">
                                                <img src="<?php echo $imageLink; ?>/uploads/testimonialpic/{[{t.picture}]}" class="testi-img">
                                            </div>
                                            <div class="col-sm-9">
                                                <div></div>
                                                <div>
                                                    <span class="fa fa-quote-left"></span>
                                                   {[{t.message}]}
                                                 </div>
                                                    <div class="text-right">-  {[{t.name}]} </div>
                                                </div>
                                            </div>
                                            <hr class="styled-hr">
                                    </div>
                                    </div>
                                   
                     </div>
                     <div ng-if="x.col=='NEWS'">
                      <h4 class="blog-title-list">Latest News</h4>
                      <div class="list-news-wrapper special-news-wrapper">
                       <div class="row list-title-blog specialpage-title-blog" ng-repeat="news in news | limitTo:x.limitcount">
                        <div class="col-sm-3 news-thumb-container {[{y.type == 3 || y.type == 4 ? 'specialpage-news-thumb' : ''}]}" style="background-image: url('{[{ news.videothumb | returnYoutubeThumb }]} {[{ news.imagethumb | returnImageThumb }]}')" ng-click="redirectNews(news.newsslugs);" title="{[{ news.title }]}">
                          <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}">
                          <a href="/blog/{[{news.newsslugs}]}">
                          </a>
                        </div>
                        <div class="col-xs-8 col-md-7 news-list-desc {[{y.type == 3 || y.type == 4 ? 'specialpage-news-desc' : ''}]}">
                          <div class="row">

                            <div class="">
                              <span class="size25 font1 news-title" ng-click="redirectNews(news.newsslugs)">{[{news.title}]}</span>
                            </div>
                            <div class="">
                              <strong><span class="thin-font1 orange">
                               {[{ news.categorylist }]}
                             </span></strong><span ng-show="news.name !=''" class="thin-font1"> / by <a href="/blog/author/Jacinto Escano"><strong><span class="orange">{[{ news.name }]}</span></strong></a></span> / {[{ news.date }]}
                             <br><br>
                           </div>
                           <div class="col-sm-12">
                            <div class="font1 size14 summary">
                              {[{ news.summary }]}
                              <br><br>
                            </div>
                          </div>
                        </div>
                        <div style="clear:both"></div>
                        <br>
                      </div>
                    </div>
                  </div>
                </div>

                     <div ng-if="x.col=='CONTACT'">
                     <div class="col-sm-12">
                      <div class="col-sm-12">
                        <h1>Contact Us</h1>
                      </div>
                      <div class="{[{y.type == 1 || y.type == 2 ? 'col-sm-6' : ''}]}">
                          <!-- Contact form -->
                          <form name="formContactUs" class="form-validation booking ng-pristine ng-invalid ng-invalid-required" ng-submit="submit(feedback)">
                            <!-- Name -->
                              <div class="control-group margin-bot8">
                                <div class="">
                                  <label class="control-label size14 col-sm-12" for="name">Subject
                                    <span class="red">*</span>
                                  </label>
                                  <div class="col-sm-12">
                                    <input type="text" class="form-control textbox margin-bot8" id="" required="required" ng-model="feedback.subject">
                                  </div>
                                </div>
                              </div>
                              <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="name">Name
                                    <span class="red">*</span>
                                  </label>
                                <div class="{[{y.type == 1 || y.type == 2 ? 'col-sm-6' : 'col-sm-12'}]}">
                                    <input type="text" class="form-control textbox margin-bot8" id="fname" placeholder="First Name" required="required" ng-model="feedback.fname">
                                  </div>
                                <div class="{[{y.type == 1 || y.type == 2 ? 'col-sm-6' : 'col-sm-12'}]}">
                                    <input type="text" class="form-control textbox" id="lname" placeholder="Last Name" required="required" ng-model="feedback.lname">
                                  </div>
                                </div>
                              </div>
                              <!-- Email -->
                              <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="email">Email
                                    <span class="red">*</span>
                                  </label>
                                  <div class="col-sm-12">
                                    <span class="red hidden booking-email">Invalid Email Address</span>
                                    <input type="email" class="textbox form-control margin-bot8" id="email" required="required" ng-model="feedback.email" ng-change="onemail(feedback.email)">
                                  </div>
                                </div>
                              </div>
                              <!-- Comment -->
                              <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                    <span class="red">*</span>
                                  </label>
                                  <div class="col-sm-12">
                                    <textarea class="form-control input-large textarea" id="feedback" rows="5" required="required" ng-model="feedback.message"></textarea>
                                  </div>
                                </div>
                              </div>
                              <!-- Buttons -->
                              <div class="form-actions margin-bot8">
                                <div class="row">
                                  <div class="col-sm-12">
                                    <button type="submit" class="btn btn-sedona" ng-disabled="formContactUs.$invalid||res==false">Submit</button>
                                  </div>
                                </div>
                              </div>
                              <!-- Buttons -->
                            </form>
                          <div class="clearfix"></div>
                        </div>

                        <div class="{[{y.type == 1 || y.type == 2 ? 'col-sm-6' : 'col-sm-12'}]} storeinfo">
                          <div class="storeinfo-details">
                            <h3 class="orange"> <span ng-bind="x.title"> </span> </h3>
                            <div class="title3">
                              <span class="titleb">Phone:</span>
                              <span ng-bind="x.phone" class="thin-font3"> </span> <br/>  
                              <span class="titleb">Email:</span> <span ng-bind="x.email" class="thin-font3"> </span> <br/>
                              <span class="titleb">Hours:</span> <span ng-bind="x.hours" class="thin-font3"> </span>  <br/> <br/>
                            </div>
                            <div class="title3">
                              <span class="titleb text-top">Address:</span>
                              <br/>
                              <div ng-repeat="s in x.str">
                                <span class="thin-font3" ng-bind="s"> </span> <br/>
                              </div>
                            </div>

                            <div class="wsite-map">
                              <iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 200px; margin-top: 10px; margin-bottom: 10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3273.673285579057!2d-111.7637428!3d34.8644358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872da1564d73fd15%3A0x3e31d123930491bf!2s201+Hwy+179%2C+Sedona%2C+AZ+86336%2C+USA!5e0!3m2!1sen!2sph!4v1434348998372"></iframe>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                        </div>

                     </div>
                     </div>

                      </div>  <!-- specialpage_col -->
                      </div> <!-- if index -->

                      <div ng-if="$index == 1">
                      <div class="specialpage_col {[{y.type == 2 ? 'col-sm-6' : ''}]} {[{(y.type == 3) && (leftsidebar==true && rightsidebar == true) ? 'special-col3' : ''}]} 
                      {[{y.type == 3 ? 'col-sm-4' : ''}]}
                      {[{ y.type == 4 ? 'col-sm-3' : ''}]}
                      {[{(y.type == 4) && (leftsidebar==true && rightsidebar == true) ? 'special-col4' : ''}]} {[{(y.type == 4) && (leftsidebar==true && rightsidebar == false) ? 'special-col4-no-one' : ''}]}
                      {[{(y.type == 4) && (leftsidebar==false && rightsidebar == true) ? 'special-col4-no-one' : ''}]}">
                       <div ng-if="x.col=='TEXT'">
                          <p ng-bind-html="trustAsHtml(x.content)"></p>
                       </div>
                      <div ng-if="x.col=='IMAGE'">
                        <div ng-if="x.link==null" class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{x.image}]}'); background-size:cover;background-repeat:no-repeat;background-position:center center;min-height:400px;margin-bottom:4px"></div>
                        <a ng-if="x.link!=null" href="{[{x.link}]}" target="_blank"><div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{x.image}]}');  background-size:cover; background-repeat:no-repeat;background-position:center center;min-height:400px;margin-bottom:4px"></div></a>
                      </div>
                      <div ng-if="x.col=='DIVIDER'">
                      <hr style="height:{[{x.height}]}px;background-color:{[{x.color}]}">
                      </div>


                      <div ng-if="x.col=='BUTTON'">
                    <div ng-if="x.btnname!='' && x.btnlink!=''" style="text-align:{[{x.position}]}">
                         <a href="{[{x.btnlink.indexOf('http://') > -1 ? '' : 'http://'}]}{[{x.btnlink}]}" target="_blank" class="module-link" style="background-color:{[{x.bgcolor}]};color:{[{x.fcolor}]};padding:{[{x.padtop}]}px {[{x.padside}]}%;font-size:{[{x.font}]}px">{[{x.btnname}]}</a>
                         </div>
                      </div>

                      <div ng-if="x.col=='TESTIMONIAL'">
                           <hr class="styled-hr">
                               <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                                 <div ng-repeat="t in testi | filter:x.category | limitTo:x.limitcount">
                                    <div class="size16 border-left italic margin-top special-testi-wrapper">
                                        <div class="row">
                                            <div class="col-sm-3 text-center page-testi-image">
                                                <img src="<?php echo $imageLink; ?>/uploads/testimonialpic/{[{t.picture}]}" class="testi-img">
                                            </div>
                                            <div class="col-sm-9">
                                                <div></div>
                                                <div>
                                                    <span class="fa fa-quote-left"></span>
                                                   {[{t.message}]}
                                                 </div>
                                                    <div class="text-right">-  {[{t.name}]} </div>
                                                </div>
                                            </div>
                                            <hr class="styled-hr">
                                    </div>
                                    </div>
                                   
                     </div>
                     <div ng-if="x.col=='NEWS'">
                      <h4 class="blog-title-list">Latest News</h4>
                      <div class="list-news-wrapper special-news-wrapper">
                       <div class="row list-title-blog specialpage-title-blog" ng-repeat="news in news | limitTo:x.limitcount">
                        <div class="col-sm-3 news-thumb-container {[{y.type == 3 || y.type == 4 ? 'specialpage-news-thumb' : ''}]}" style="background-image: url('{[{ news.videothumb | returnYoutubeThumb }]} {[{ news.imagethumb | returnImageThumb }]}')" ng-click="redirectNews(news.newsslugs);" title="{[{ news.title }]}">
                          <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}">
                          <a href="/blog/{[{news.newsslugs}]}">
                          </a>
                        </div>
                        <div class="col-xs-8 col-md-7 news-list-desc {[{y.type == 3 || y.type == 4 ? 'specialpage-news-desc' : ''}]} ">
                          <div class="row">

                            <div class="">
                              <span class="size25 font1 news-title" ng-click="redirectNews(news.newsslugs)">{[{news.title}]}</span>
                            </div>
                            <div class="">
                              <strong><span class="thin-font1 orange">
                               {[{ news.categorylist }]}
                             </span></strong><span ng-show="news.name !=''" class="thin-font1"> / by <a href="/blog/author/Jacinto Escano"><strong><span class="orange">{[{ news.name }]}</span></strong></a></span> / {[{ news.date }]}
                             <br><br>
                           </div>
                           <div class="col-sm-12">
                            <div class="font1 size14 summary">
                              {[{ news.summary }]}
                              <br><br>
                            </div>
                          </div>
                        </div>
                        <div style="clear:both"></div>
                        <br>
                      </div>
                    </div>
                  </div>
                </div>
                     <div ng-if="x.col=='CONTACT'">
                     <div class="col-sm-12">
                      <div class="col-sm-12">
                        <h1>Contact Us</h1>
                      </div>
                      <div class="{[{y.type == 1 || y.type == 2 ? 'col-sm-6' : ''}]}">
                          <!-- Contact form -->
                          <form name="formContactUs" class="form-validation booking ng-pristine ng-invalid ng-invalid-required" ng-submit="submit(feedback)">
                            <!-- Name -->
                              <div class="control-group margin-bot8">
                                <div class="">
                                  <label class="control-label size14 col-sm-12" for="name">Subject
                                    <span class="red">*</span>
                                  </label>
                                  <div class="col-sm-12">
                                    <input type="text" class="form-control textbox margin-bot8" id="" required="required" ng-model="feedback.subject">
                                  </div>
                                </div>
                              </div>
                              <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="name">Name
                                    <span class="red">*</span>
                                  </label>
                                 <div class="{[{y.type == 1 || y.type == 2 ? 'col-sm-6' : 'col-sm-12'}]}">
                                    <input type="text" class="form-control textbox margin-bot8" id="fname" placeholder="First Name" required="required" ng-model="feedback.fname">
                                  </div>
                                <div class="{[{y.type == 1 || y.type == 2 ? 'col-sm-6' : 'col-sm-12'}]}">
                                    <input type="text" class="form-control textbox" id="lname" placeholder="Last Name" required="required" ng-model="feedback.lname">
                                  </div>
                                </div>
                              </div>
                              <!-- Email -->
                              <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="email">Email
                                    <span class="red">*</span>
                                  </label>
                                  <div class="col-sm-12">
                                    <span class="red hidden booking-email">Invalid Email Address</span>
                                    <input type="email" class="textbox form-control margin-bot8" id="email" required="required" ng-model="feedback.email" ng-change="onemail(feedback.email)">
                                  </div>
                                </div>
                              </div>
                              <!-- Comment -->
                              <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                    <span class="red">*</span>
                                  </label>
                                  <div class="col-sm-12">
                                    <textarea class="form-control input-large textarea" id="feedback" rows="5" required="required" ng-model="feedback.message"></textarea>
                                  </div>
                                </div>
                              </div>
                              <!-- Buttons -->
                              <div class="form-actions margin-bot8">
                                <div class="row">
                                  <div class="col-sm-12">
                                    <button type="submit" class="btn btn-sedona" ng-disabled="formContactUs.$invalid||res==false">Submit</button>
                                  </div>
                                </div>
                              </div>
                              <!-- Buttons -->
                            </form>
                          <div class="clearfix"></div>
                        </div>

                        <div class="{[{y.type == 1 || y.type == 2 ? 'col-sm-6' : 'col-sm-12'}]} storeinfo">
                          <div class="storeinfo-details">
                            <h3 class="orange"> <span ng-bind="x.title"> </span> </h3>
                            <div class="title3">
                              <span class="titleb">Phone:</span>
                              <span ng-bind="x.phone" class="thin-font3"> </span> <br/>  
                              <span class="titleb">Email:</span> <span ng-bind="x.email" class="thin-font3"> </span> <br/>
                              <span class="titleb">Hours:</span> <span ng-bind="x.hours" class="thin-font3"> </span>  <br/> <br/>
                            </div>
                            <div class="title3">
                              <span class="titleb text-top">Address:</span>
                              <br/>
                              <div ng-repeat="s in x.str">
                                <span class="thin-font3" ng-bind="s"> </span> <br/>
                              </div>
                            </div>

                            <div class="wsite-map">
                              <iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 200px; margin-top: 10px; margin-bottom: 10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3273.673285579057!2d-111.7637428!3d34.8644358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872da1564d73fd15%3A0x3e31d123930491bf!2s201+Hwy+179%2C+Sedona%2C+AZ+86336%2C+USA!5e0!3m2!1sen!2sph!4v1434348998372"></iframe>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                        </div>

                     </div>
                     </div>
                      </div>  <!-- specialpage_col -->
                      </div> <!-- if index -->
                      <div ng-if="$index == 2">
                     <div class="specialpage_col {[{(y.type == 3) && (leftsidebar==true && rightsidebar == true) ? 'special-col33' : ''}]} 
                      {[{y.type == 3 ? 'col-sm-4' : ''}]}
                      {[{ y.type == 4 ? 'col-sm-3' : ''}]}
                      {[{(y.type == 4) && (leftsidebar==true && rightsidebar == true) ? 'special-col4-3' : ''}]} {[{(y.type == 4) && (leftsidebar==true && rightsidebar == false) ? 'special-col4-no-3' : ''}]}
                      {[{(y.type == 4) && (leftsidebar==false && rightsidebar == true) ? 'special-col4-no-3' : ''}]}">
                       <div ng-if="x.col=='TEXT'">
                          <p ng-bind-html="trustAsHtml(x.content)"></p>
                       </div>
                      <div ng-if="x.col=='IMAGE'">
                        <div ng-if="x.link==null" class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{x.image}]}'); background-size:cover;background-repeat:no-repeat;background-position:center center;min-height:400px;margin-bottom:4px"></div>
                        <a ng-if="x.link!=null" href="{[{x.link}]}" target="_blank"><div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{x.image}]}');  background-size:cover; background-repeat:no-repeat;background-position:center center;min-height:400px;margin-bottom:4px"></div></a>
                      </div>
                      <div ng-if="x.col=='DIVIDER'">
                      <hr style="height:{[{x.height}]}px;background-color:{[{x.color}]}">
                      </div>


                      <div ng-if="x.col=='BUTTON'">
                    <div ng-if="x.btnname!='' && x.btnlink!=''" style="text-align:{[{x.position}]}">
                         <a href="{[{x.btnlink.indexOf('http://') > -1 ? '' : 'http://'}]}{[{x.btnlink}]}" target="_blank" class="module-link" style="background-color:{[{x.bgcolor}]};color:{[{x.fcolor}]};padding:{[{x.padtop}]}px {[{x.padside}]}%;font-size:{[{x.font}]}px">{[{x.btnname}]}</a>
                         </div>
                      </div>

                      <div ng-if="x.col=='TESTIMONIAL'">
                           <hr class="styled-hr">
                               <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                                 <div ng-repeat="t in testi | filter:x.category | limitTo:x.limitcount">
                                    <div class="size16 border-left italic margin-top special-testi-wrapper">
                                        <div class="row">
                                            <div class="col-sm-3 text-center page-testi-image">
                                                <img src="<?php echo $imageLink; ?>/uploads/testimonialpic/{[{t.picture}]}" class="testi-img">
                                            </div>
                                            <div class="col-sm-9">
                                                <div></div>
                                                <div>
                                                    <span class="fa fa-quote-left"></span>
                                                   {[{t.message}]}
                                                 </div>
                                                    <div class="text-right">-  {[{t.name}]} </div>
                                                </div>
                                            </div>
                                            <hr class="styled-hr">
                                    </div>
                                    </div>
                                   
                     </div>
                     <div ng-if="x.col=='NEWS'">
                      <h4 class="blog-title-list">Latest News</h4>
                      <div class="list-news-wrapper special-news-wrapper">
                       <div class="row list-title-blog specialpage-title-blog" ng-repeat="news in news | limitTo:x.limitcount">
                        <div class="col-sm-3 news-thumb-container specialpage-news-thumb" style="background-image: url('{[{ news.videothumb | returnYoutubeThumb }]} {[{ news.imagethumb | returnImageThumb }]}')" ng-click="redirectNews(news.newsslugs);" title="{[{ news.title }]}">
                          <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}">
                          <a href="/blog/{[{news.newsslugs}]}">
                          </a>
                        </div>
                        <div class="col-xs-8 col-md-7 news-list-desc specialpage-news-desc">
                          <div class="row">

                            <div class="">
                              <span class="size25 font1 news-title" ng-click="redirectNews(news.newsslugs)">{[{news.title}]}</span>
                            </div>
                            <div class="">
                              <strong><span class="thin-font1 orange">
                               {[{ news.categorylist }]}
                             </span></strong><span ng-show="news.name !=''" class="thin-font1"> / by <a href="/blog/author/Jacinto Escano"><strong><span class="orange">{[{ news.name }]}</span></strong></a></span> / {[{ news.date }]}
                             <br><br>
                           </div>
                           <div class="col-sm-12">
                            <div class="font1 size14 summary">
                              {[{ news.summary }]}
                              <br><br>
                            </div>
                          </div>
                        </div>
                        <div style="clear:both"></div>
                        <br>
                      </div>
                    </div>
                  </div>
                </div>
                     <div ng-if="x.col=='CONTACT'">
                     <div class="col-sm-12">
                      <div class="col-sm-12">
                        <h1>Contact Us</h1>
                      </div>
                      <div class="{[{y.type == 1 || y.type == 2 ? 'col-sm-6' : ''}]}">
                          <!-- Contact form -->
                          <form name="formContactUs" class="form-validation booking ng-pristine ng-invalid ng-invalid-required" ng-submit="submit(feedback)">
                            <!-- Name -->
                              <div class="control-group margin-bot8">
                                <div class="">
                                  <label class="control-label size14 col-sm-12" for="name">Subject
                                    <span class="red">*</span>
                                  </label>
                                  <div class="col-sm-12">
                                    <input type="text" class="form-control textbox margin-bot8" id="" required="required" ng-model="feedback.subject">
                                  </div>
                                </div>
                              </div>
                              <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="name">Name
                                    <span class="red">*</span>
                                  </label>
                                <div class="{[{y.type == 1 || y.type == 2 ? 'col-sm-6' : 'col-sm-12'}]}">
                                    <input type="text" class="form-control textbox margin-bot8" id="fname" placeholder="First Name" required="required" ng-model="feedback.fname">
                                  </div>
                                <div class="{[{y.type == 1 || y.type == 2 ? 'col-sm-6' : 'col-sm-12'}]}">
                                    <input type="text" class="form-control textbox" id="lname" placeholder="Last Name" required="required" ng-model="feedback.lname">
                                  </div>
                                </div>
                              </div>
                              <!-- Email -->
                              <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="email">Email
                                    <span class="red">*</span>
                                  </label>
                                  <div class="col-sm-12">
                                    <span class="red hidden booking-email">Invalid Email Address</span>
                                    <input type="email" class="textbox form-control margin-bot8" id="email" required="required" ng-model="feedback.email" ng-change="onemail(feedback.email)">
                                  </div>
                                </div>
                              </div>
                              <!-- Comment -->
                              <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                    <span class="red">*</span>
                                  </label>
                                  <div class="col-sm-12">
                                    <textarea class="form-control input-large textarea" id="feedback" rows="5" required="required" ng-model="feedback.message"></textarea>
                                  </div>
                                </div>
                              </div>
                              <!-- Buttons -->
                              <div class="form-actions margin-bot8">
                                <div class="row">
                                  <div class="col-sm-12">
                                    <button type="submit" class="btn btn-sedona" ng-disabled="formContactUs.$invalid||res==false">Submit</button>
                                  </div>
                                </div>
                              </div>
                              <!-- Buttons -->
                            </form>
                          <div class="clearfix"></div>
                        </div>

                        <div class="{[{y.type == 1 || y.type == 2 ? 'col-sm-6' : 'col-sm-12'}]} storeinfo">
                          <div class="storeinfo-details">
                            <h3 class="orange"> <span ng-bind="x.title"> </span> </h3>
                            <div class="title3">
                              <span class="titleb">Phone:</span>
                              <span ng-bind="x.phone" class="thin-font3"> </span> <br/>  
                              <span class="titleb">Email:</span> <span ng-bind="x.email" class="thin-font3"> </span> <br/>
                              <span class="titleb">Hours:</span> <span ng-bind="x.hours" class="thin-font3"> </span>  <br/> <br/>
                            </div>
                            <div class="title3">
                              <span class="titleb text-top">Address:</span>
                              <br/>
                              <div ng-repeat="s in x.str">
                                <span class="thin-font3" ng-bind="s"> </span> <br/>
                              </div>
                            </div>

                            <div class="wsite-map">
                              <iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 200px; margin-top: 10px; margin-bottom: 10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3273.673285579057!2d-111.7637428!3d34.8644358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872da1564d73fd15%3A0x3e31d123930491bf!2s201+Hwy+179%2C+Sedona%2C+AZ+86336%2C+USA!5e0!3m2!1sen!2sph!4v1434348998372"></iframe>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                        </div>

                     </div>
                     </div>
                      </div>  <!-- specialpage_col -->
                      </div> <!-- if index -->

                      <div ng-if="$index == 3">
                      <div class="specialpage_col col-sm-3
                      {[{leftsidebar==true && rightsidebar == true ? 'special-col4-4' : ''}]} {[{leftsidebar==true && rightsidebar == false ? 'special-col4-no-4' : ''}]}
                      {[{leftsidebar==false && rightsidebar == true ? 'special-col4-no-4' : ''}]}">
                       <div ng-if="x.col=='TEXT'">
                          <p ng-bind-html="trustAsHtml(x.content)"></p>
                       </div>
                      <div ng-if="x.col=='IMAGE'">
                        <div ng-if="x.link==null" class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{x.image}]}'); background-size:cover;background-repeat:no-repeat;background-position:center center;min-height:400px;margin-bottom:4px"></div>
                        <a ng-if="x.link!=null" href="{[{x.link}]}" target="_blank"><div class="img-responsive" style="background:#000 url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{x.image}]}');  background-size:cover; background-repeat:no-repeat;background-position:center center;min-height:400px;margin-bottom:4px"></div></a>
                      </div>
                      <div ng-if="x.col=='DIVIDER'">
                      <hr style="height:{[{x.height}]}px;background-color:{[{x.color}]}">
                      </div>


                      <div ng-if="x.col=='BUTTON'">
                    <div ng-if="x.btnname!='' && x.btnlink!=''" style="text-align:{[{x.position}]}">
                         <a href="{[{x.btnlink.indexOf('http://') > -1 ? '' : 'http://'}]}{[{x.btnlink}]}" target="_blank" class="module-link" style="background-color:{[{x.bgcolor}]};color:{[{x.fcolor}]};padding:{[{x.padtop}]}px {[{x.padside}]}%;font-size:{[{x.font}]}px">{[{x.btnname}]}</a>
                         </div>
                      </div>

                      <div ng-if="x.col=='TESTIMONIAL'">
                           <hr class="styled-hr">
                               <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                                 <div ng-repeat="t in testi | filter:x.category | limitTo:x.limitcount">
                                    <div class="size16 border-left italic margin-top special-testi-wrapper">
                                        <div class="row">
                                            <div class="col-sm-3 text-center page-testi-image">
                                                <img src="<?php echo $imageLink; ?>/uploads/testimonialpic/{[{t.picture}]}" class="testi-img">
                                            </div>
                                            <div class="col-sm-9">
                                                <div></div>
                                                <div>
                                                    <span class="fa fa-quote-left"></span>
                                                   {[{t.message}]}
                                                 </div>
                                                    <div class="text-right">-  {[{t.name}]} </div>
                                                </div>
                                            </div>
                                            <hr class="styled-hr">
                                    </div>
                                    </div>
                                   
                     </div>
                     <div ng-if="x.col=='NEWS'">
                      <h4 class="blog-title-list">Latest News</h4>
                      <div class="list-news-wrapper special-news-wrapper">
                       <div class="row list-title-blog specialpage-title-blog" ng-repeat="news in news | limitTo:x.limitcount">
                        <div class="col-sm-3 news-thumb-container specialpage-news-thumb" style="background-image: url('{[{ news.videothumb | returnYoutubeThumb }]} {[{ news.imagethumb | returnImageThumb }]}')" ng-click="redirectNews(news.newsslugs);" title="{[{ news.title }]}">
                          <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}">
                          <a href="/blog/{[{news.newsslugs}]}">
                          </a>
                        </div>
                        <div class="col-xs-8 col-md-7 news-list-desc specialpage-news-desc">
                          <div class="row">

                            <div class="">
                              <span class="size25 font1 news-title" ng-click="redirectNews(news.newsslugs)">{[{news.title}]}</span>
                            </div>
                            <div class="">
                              <strong><span class="thin-font1 orange">
                               {[{ news.categorylist }]}
                             </span></strong><span ng-show="news.name !=''" class="thin-font1"> / by <a href="/blog/author/Jacinto Escano"><strong><span class="orange">{[{ news.name }]}</span></strong></a></span> / {[{ news.date }]}
                             <br><br>
                           </div>
                           <div class="col-sm-12">
                            <div class="font1 size14 summary">
                              {[{ news.summary }]}
                              <br><br>
                            </div>
                          </div>
                        </div>
                        <div style="clear:both"></div>
                        <br>
                      </div>
                    </div>
                  </div>
                </div>
                    <div ng-if="x.col=='CONTACT'">
                     <div class="col-sm-12">
                      <div class="col-sm-12">
                        <h1>Contact Us</h1>
                      </div>
                      <div class="{[{y.type == 1 || y.type == 2 ? 'col-sm-6' : ''}]}">
                          <!-- Contact form -->
                          <form name="formContactUs" class="form-validation booking ng-pristine ng-invalid ng-invalid-required" ng-submit="submit(feedback)">
                            <!-- Name -->
                              <div class="control-group margin-bot8">
                                <div class="">
                                  <label class="control-label size14 col-sm-12" for="name">Subject
                                    <span class="red">*</span>
                                  </label>
                                  <div class="col-sm-12">
                                    <input type="text" class="form-control textbox margin-bot8" id="" required="required" ng-model="feedback.subject">
                                  </div>
                                </div>
                              </div>
                              <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="name">Name
                                    <span class="red">*</span>
                                  </label>
                                <div class="{[{y.type == 1 || y.type == 2 ? 'col-sm-6' : 'col-sm-12'}]}">
                                    <input type="text" class="form-control textbox margin-bot8" id="fname" placeholder="First Name" required="required" ng-model="feedback.fname">
                                  </div>
                                <div class="{[{y.type == 1 || y.type == 2 ? 'col-sm-6' : 'col-sm-12'}]}">
                                    <input type="text" class="form-control textbox" id="lname" placeholder="Last Name" required="required" ng-model="feedback.lname">
                                  </div>
                                </div>
                              </div>
                              <!-- Email -->
                              <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="email">Email
                                    <span class="red">*</span>
                                  </label>
                                  <div class="col-sm-12">
                                    <span class="red hidden booking-email">Invalid Email Address</span>
                                    <input type="email" class="textbox form-control margin-bot8" id="email" required="required" ng-model="feedback.email" ng-change="onemail(feedback.email)">
                                  </div>
                                </div>
                              </div>
                              <!-- Comment -->
                              <div class="control-group margin-bot8">
                                <div class="row">
                                  <label class="control-label size14 col-sm-12" for="comment">Feedback:
                                    <span class="red">*</span>
                                  </label>
                                  <div class="col-sm-12">
                                    <textarea class="form-control input-large textarea" id="feedback" rows="5" required="required" ng-model="feedback.message"></textarea>
                                  </div>
                                </div>
                              </div>
                              <!-- Buttons -->
                              <div class="form-actions margin-bot8">
                                <div class="row">
                                  <div class="col-sm-12">
                                    <button type="submit" class="btn btn-sedona" ng-disabled="formContactUs.$invalid||res==false">Submit</button>
                                  </div>
                                </div>
                              </div>
                              <!-- Buttons -->
                            </form>
                          <div class="clearfix"></div>
                        </div>

                        <div class="{[{y.type == 1 || y.type == 2 ? 'col-sm-6' : 'col-sm-12'}]} storeinfo">
                          <div class="storeinfo-details">
                            <h3 class="orange"> <span ng-bind="x.title"> </span> </h3>
                            <div class="title3">
                              <span class="titleb">Phone:</span>
                              <span ng-bind="x.phone" class="thin-font3"> </span> <br/>  
                              <span class="titleb">Email:</span> <span ng-bind="x.email" class="thin-font3"> </span> <br/>
                              <span class="titleb">Hours:</span> <span ng-bind="x.hours" class="thin-font3"> </span>  <br/> <br/>
                            </div>
                            <div class="title3">
                              <span class="titleb text-top">Address:</span>
                              <br/>
                              <div ng-repeat="s in x.str">
                                <span class="thin-font3" ng-bind="s"> </span> <br/>
                              </div>
                            </div>

                            <div class="wsite-map">
                              <iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 200px; margin-top: 10px; margin-bottom: 10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3273.673285579057!2d-111.7637428!3d34.8644358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872da1564d73fd15%3A0x3e31d123930491bf!2s201+Hwy+179%2C+Sedona%2C+AZ+86336%2C+USA!5e0!3m2!1sen!2sph!4v1434348998372"></iframe>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                        </div>

                     </div>
                     </div>
                      </div>  <!-- specialpage_col -->
                      </div> <!-- if index -->


                    </div> <!-- ng-repeat inner -->
               </div> <!-- row -->
               </div> <!-- ng-repeat -->
           
            </div>

        </div>


  <div ng-if="rightsidebar == true">
      <div class="{[{leftsidebar == true && rightsidebar == true ? 'col-sm-2 specialpage-sidebar' : 'col-sm-2'}]} ">

        <div class="right-sidebar">
           <!-- RIGHT SIDE  -->
 <div ng-repeat="x in sidebarright">

    <div ng-if="x.sidebar=='Image'">
      <hr class="styled-hr">
       <img ng-if="x.imglink==NULL" src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{x.img}]}" width="100%">

      <a ng-if="x.imglink!=NULL" target="_blank" href="{[{x.imglink}]}"><img src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{x.img}]}" width="100%"></a>
    </div>

     <div ng-if="x.sidebar=='Calendar'">
      <hr class="styled-hr">
      <div class="pos-rlt">

        <div class="calendar" ng-model="eventSources" calendar="calendar1" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div>

      </div>
     </div>

      <div ng-if="x.sidebar=='Menu'">
         <div class="special-menu">
            <div><a class="special-top-menu">Sedona Healing Arts</a></div>
            <nav>
              <ul style="list-style:disc">
                <li ng-repeat="mobmenu in shortcode" ng-if="mobmenu.menuID == x.menuID ">
                  <a class="special-menu-text" href="{[{mobmenu.sublink}]}"><span ng-bind="mobmenu.subname"></span></a>

                  <ul class="no-list" ng-repeat="mob in children">
                    <li ng-repeat="mobsub1 in mob.child1" ng-if="mobsub1.parent==mobmenu.submenuID">
                    <span class="glyphicon glyphicon-menu-right" style="color:#58666e;">
                      <a class="special-menu-text" href="{[{mobsub1.sublink}]}"><span ng-bind="mobsub1.subname"></span></a>

                      <ul class="no-list">
                        <li ng-repeat="mobsub2 in mob.child2" ng-if="mobsub2.parent==mobsub1.submenuID">
                         <span class="glyphicon glyphicon-menu-right" style="color:#58666e;">
                          <a class="special-menu-text" href="{[{mobsub2.sublink}]}"><span ng-bind="mobsub2.subname"></span></a>

                          <ul class="no-list">
                            <li ng-repeat="mobsub3 in mob.child3" ng-if="mobsub3.parent==mobsub2.submenuID">
                            <span class="glyphicon glyphicon-menu-right" style="color:#58666e;">
                              <a class="special-menu-text" href="{[{mobsub3.sublink}]}"><span ng-bind="mobsub3.subname"></span></a>
                            
                              <ul class="no-list">
                              <li ng-repeat="mobsub4 in mob.child4" ng-if="mobsub4.parent==mobsub3.submenuID">
                                  <span class="glyphicon glyphicon-menu-right" style="color:#58666e;">
                                  <a class="special-menu-text" href="{[{mobsub4.sublink}]}"><span ng-bind="mobsub4.subname"></span></a>
                                </li>
                              </ul>

                            </li>
                          </ul>

                        </li>
                      </ul>

                    </li>
                  </ul>


                </li>

              </ul>
            </nav>
          </div>
      </div>

      <div ng-if="x.sidebar=='Testimonial'">
        <div class="size18"><h4 class="font1 italic">What People Are Saying</h4></div>
                             <div ng-repeat="x in testi | limitTo:x.limittest">
                              <div class="specialpage_col">  
                                <div class="size16 border-left italic margin-top special-testi-wrapper">
                                  <div class="row">
                                    <div class="col-sm-12 text-center page-testi-image">
                                      <img src="<?php echo $imageLink; ?>/uploads/testimonialpic/{[{x.picture}]}" class="testi-img">
                                    </div>
                                    <div class="col-sm-12">
                                      <div></div>
                                      <div>
                                        <span class="fa fa-quote-left"></span>
                                        {[{x.message}]}
                                      </div>
                                      <div class="text-right">-  {[{x.name}]} </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
      </div>

      <div ng-if="x.sidebar=='Link'">
      <div ng-if="x.url != '' && x.label != ''" class='lnk'>
        <hr class="styled-hr">
        <a target="_blank" href="{[{x.url}]}" class="page-link"> {[{x.label}]} </a>
      </div>
      </div>

      <div ng-if="x.sidebar=='News'">
         <h4 class="blog-title-list">Latest News </h4>
                        <div class="list-news-wrapper">
                          <div class="row list-title-blog specialpage-title-blog" ng-repeat="news in news | limitTo:x.limitnews">
                            <!-- <div class="col-sm-12 news-thumb-container" title="{[{ news.title }]}"> -->
                            <div class="col-sm-3 news-thumb-container specialpage-news-thumb" style="background-image: url('{[{ news.videothumb | returnYoutubeThumb }]} {[{ news.imagethumb | returnImageThumb }]}')" ng-click="redirectNews(news.newsslugs);" title="{[{ news.title }]}">
                              <img src="{[{ news.imagethumb | returnImageThumb }]}" class="pinterest-img" alt="{[{ news.title }]}">
                              <?php if ($news->videothumb) {
                                ?>
                                <div ng-if="news.videothumb" class="youtube-play"><a href="/blog/{[{news.newsslugs}]}"><img src="img/youtubeplay.png"/></a></div>
                                <?php
                              } ?>

                              <a href="/blog/{[{news.newsslugs}]}">
                              </a>
                            </div>
                      
                            <div class="news-list-desc specialpage-news-desc">
                              <div class="row">
                                <div class="">
                                  <span class="size25 font1 news-title">{[{ news.title }]}</span>
                                </div>
                                <div class="">
                                  <span class="thin-font1 orange">{[{ news.categorylist }]}</span><span ng-show="news.name !=''" class="thin-font1"> / by <span class="orange">{[{ news.name }]}</span></span> / {[{ news.date }]}
                                  <br/><br/>
                                </div>
                                <div class="col-sm-12" ng-if="news.summary">
                                  <div class="font1 size14 summary">
                                    {[{ news.summary }]}
                                    <br/><br/>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
      </div>
      <div ng-if="x.sidebar=='Archive'">
        <h3 class="font1"><span class="fa fa-calendar fa-1x"></span> Archives</h3>
                      <div class="ul-archives">
                       <ul ng-repeat="archive in archive" class="ul-archive">
                         <li>
                        <a style="color:#ff8421" href="<?php echo $base_url; ?>/blog/archive/{[{archive.month}]}/{[{archive.year}]}"><span class="fa fa-chevron-right"></span> {[{archive.month}]} {[{archive.year}]}</a>
                         </li>
                       </ul>
                    </div>
      </div>
      <div ng-if="x.sidebar=='RSS'">
        <hr class="styled-hr"> <br/>
        <p class="size16"> <a href="<?php echo $base_url;?>/feed" target="_blank"><img class="rss" src="img/frontend/rss_feed.gif"> RSS Feed</a></p> <br/> <br/>
      </div>

    </div>  <!-- ng-repeat -->
        </div>


      </div>
  </div> <!--if rightside true -->

    </div>

</div>
</div>
<script type="text/javascript">
   var MyDiv1 = document.getElementById('main-div');
   var MyDiv2 = document.getElementById('second-div');
   MyDiv2.innerHTML = MyDiv1.innerHTML;
</script>
<!-- 

    <!-- Below Banner ends -->
</div>
