<?php

error_reporting(E_ALL);
ini_set("display_errors", 0);
try {

	/**
	 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
	 */
	$di = new \Phalcon\DI\FactoryDefault();

	/**
	 * Registering a router
	 */
	$di['router'] = function() {

		$router = new \Phalcon\Mvc\Router(false);

		$router->add('/:controller/:action/:params', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'page404',
			'params' => 3
		));

		$router->add('/:controller', array(
			'module' => 'frontend',
			'controller' => 1
		));

		$router->add('/blog/:action/:params/:params', array(
			'module' => 'frontend',
			'controller' => 'blog',
			'action' => 1,
			'params' => 2,
			'params' => 3
		));

		//special page
		$router->add('/{pageslugs}', array(
			'module' => 'frontend',
			'controller' => 'specialpage',
			'action' => 'specialpage'
		));
		$router->add('/{catslugs}/{pageslugs}', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view'
			));

		$router->add('/booking', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'booking'
		));

		$router->add('/contactus', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'contactus'
		));

		// $router->add('/healing', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'page',
		// 	'action' => 'healing'
		// ));

		$router->add('/aboutsedonahealingarts', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'aboutsedonahealingarts'
		));


		// $router->add('/readings', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'page',
		// 	'action' => 'readings'
		// ));

		// $router->add('/acupuncture', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'page',
		// 	'action' => 'acupuncture'
		// ));

		// $router->add('/retreats', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'page',
		// 	'action' => 'retreats'
		// ));

		// $router->add('/workshops', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'page',
		// 	'action' => 'workshops'
		// ));

		$router->add('/reservation', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'reservation'
		));

		$router->add('/testlist', array(
			'module' => 'frontend',
			'controller' => 'test',
			'action' => 'testlist'
		));

		$router->add('/maintenance', array(
			'module' => 'frontend',
			'controller' => "maintenance"
		));

		$router->add('/blog/{newsslugs}', array(
			'module' => 'frontend',
			'controller' => 'blog',
			'action' => 'view'
		));

		$router->add('/blog/all', array(
			'module' => 'frontend',
			'controller' => 'blog',
			'action' => 'all'
		));

		$router->add('/blog/preview', array(
			'module' => 'frontend',
			'controller' => 'blog',
			'action' => 'preview'
		));
		$router->add('/specialpage/preview', array(
			'module' => 'frontend',
			'controller' => 'specialpage',
			'action' => 'preview'
		));

		$router->add('/blog/category/{category}', array(
			'module' => 'frontend',
			'controller' => 'blog',
			'action' => 'category'
		));

		$router->add('/blog/tags/{tags}', array(
			'module' => 'frontend',
			'controller' => 'blog',
			'action' => 'tags'
		));

		$router->add('/blog/author/{id}', array(
			'module' => 'frontend',
			'controller' => 'blog',
			'action' => 'author'
		));

		$router->add('/blog/archive/{month}/{date}', array(
			'module' => 'frontend',
			'controller' => 'blog',
			'action' => 'archive'
		));

		// $router->add('/healing/{pageslugs}', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'page',
		// 	'action' => 'view'
		// ));
		// $router->add('/readings/{pageslugs}', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'page',
		// 	'action' => 'view'
		// ));
		// $router->add('/acupuncture/{pageslugs}', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'page',
		// 	'action' => 'view'
		// ));
		// $router->add('/retreats/{pageslugs}', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'page',
		// 	'action' => 'view'
		// ));
		// $router->add('/workshops/{pageslugs}', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'page',
		// 	'action' => 'view'
		// ));

		$router->add('/aura/{pageslugs}', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view'
		));

		$router->add('/reservation/confirmation/{value}', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'reservation'
		));
		$router->add('/paypalipn', array(
			'module' => 'frontend',
			'controller' => 'index',
			'action' => 'paypalipn'
		));
		$router->add('/profilecompletion/{token}', array(
			'module' => 'frontend',
			'controller' => 'index',
			'action' => 'profilecompletion'
		));
		$router->add('/resetpassword/{token}', array(
			'module' => 'frontend',
			'controller' => 'index',
			'action' => 'resetpassword'
		));
		$router->add('/invoice/{invoiceno}', array(
			'module' => 'frontend',
			'controller' => 'index',
			'action' => 'invoice'
		));


		$router->add('/taketest/{testid}', array(
			'module' => 'frontend',
			'controller' => 'test',
			'action' => 'taketest'
		));

		$router->add('/testresult', array(
			'module' => 'frontend',
			'controller' => 'test',
			'action' => 'testresult'
		));

		//ecommerce
		$router->add('/shop', array(
			'module' => 'frontend',
			'controller' => 'ecommerce',
			'action' => 'index'
		));
		$router->add('/shop/{productcode}', array(
			'module' => 'frontend',
			'controller' => 'ecommerce',
			'action' => 'product'
		));
		$router->add('/shop/mycart', array(
			'module' => 'frontend',
			'controller' => 'ecommerce',
			'action' => 'mycart'
		));
		$router->add('/shop/checkout', array(
			'module' => 'frontend',
			'controller' => 'ecommerce',
			'action' => 'checkout'
		));
		$router->add('/shop/confirmation/{value}', array(
			'module' => 'frontend',
			'controller' => 'ecommerce',
			'action' => 'userconfirmation'
		));


		$router->add('/page404', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'page404',
		));

		$router->add('/', array(
			'module' => 'frontend',
			'controller' => 'index',
			'action' => 'index'
		));

		//user registration
		$router->add('/sd/register', array(
			'module' => 'frontend',
			'controller' => 'userlogin',
			'action' => 'userregistration'
		));

		$router->add('/sd/confirmation/{value}', array(
			'module' => 'frontend',
			'controller' => 'userlogin',
			'action' => 'userconfirmation'
		));

		//user login
		$router->add('/sd/login', array(
			'module' => 'frontend',
			'controller' => 'userlogin',
			'action' => 'userlogin'
		));

		$router->add('/sedonaadmin', array(
			'module' => 'backend',
			'controller' => 'index',
			'action' => 'index'
		));

		$router->add('/sedonaadmin/:controller', array(
			'module'=> 'backend',
			'controller' => 1,
			'action' => 'index'
		));

		$router->add('/sedonaadmin/:controller/:action', array(
			'module'=> 'backend',
			'controller' => 1,
			'action' => 3
		));

		$router->add('/sedonaadmin/:controller/:action/:params', array(
			'module'=> 'backend',
			'controller' => 1,
			'action' => 2,
			'params' => 3,
		));

		$router->removeExtraSlashes(true);

		return $router;
	};

	/**
	 * The URL component is used to generate all kind of urls in the application
	 */
	$di->set('url', function() {
		$url = new \Phalcon\Mvc\Url();
		$url->setBaseUri('/');
		return $url;
	});

	/**
	 * Start the session the first time some component request the session service
	 */
	$di->set('session', function() {
		$session = new \Phalcon\Session\Adapter\Files();
		$session->start();
		return $session;
	});

	/**
	 * If the configuration specify the use of metadata adapter use it or use memory otherwise
	 */
	$di->set('modelsMetadata', function () {
	    return new MetaDataAdapter();
	});

    /*
    ModelsManager
    */
	$di->set('modelsManager', function() {
	      return new Phalcon\Mvc\Model\Manager();
	});
    $config = include "../app/config/config.php";
    // Store it in the Di container
    $di->set('config', function () use ($config) {
        return $config;
    });
	/**
	 * Handle the request
	 */
	$application = new \Phalcon\Mvc\Application();

	$application->setDI($di);

	/**
	 * Register application modules
	 */
	$application->registerModules(array(
		'frontend' => array(
			'className' => 'Modules\Frontend\Module',
			'path' => '../app/frontend/Module.php'
		),
		'backend' => array(
			'className' => 'Modules\Backend\Module',
			'path' => '../app/backend/Module.php'
		)
	));

	echo $application->handle()->getContent();

} catch (Phalcon\Exception $e) {
	echo $e->getMessage();
} catch (PDOException $e){
	echo $e->getMessage();
}
