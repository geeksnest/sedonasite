'use strict';


// Declare app level module which depends on filters, and services
var app = angular.module('app', [
    'ngAnimate',
    'ui.router',
    'ui.bootstrap',
    'ui.load',
    'ngCookies',
    'ngStorage',
    'ui.jq',
    'ui.validate',
    'pascalprecht.translate',
    'app.factory',
    'app.directives',
    'app.controllers',
    'ngFileUpload',
    'xeditable',
    'angular.chosen',
    'angularMoment',
    'ui.select',
    'ngSanitize',
    'ui.calendar',
    'ui-notification',
    'highcharts-ng',
    'colorpicker.module',
    'ui.sortable',
    'ui.autocomplete',
    'uuid',
    'angular-storage',
    'ngCountryStateSelect',
    'checklist-model'
  ])

.run(['$rootScope', '$state', '$stateParams', 'editableOptions', 'store', '$http', 'Config', function ($rootScope,   $state,   $stateParams,editableOptions, store, $http, Config) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    editableOptions.theme = 'bs3';

    if (store.get('AccessToken') == null) {
        console.log('== Getting Access Log ==');
        window.location = '/sedonaadmin';
    }else{
        $http({
            url: Config.ApiURL + '/authenticate/get',
            method: "GET",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': "Bearer " + store.get('AccessToken')
            }
        }).success(function(data, status, headers, config) {
            // console.log(store.get('AccessToken'));
        })
    }

}])

.config(
  [          '$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$interpolateProvider','$parseProvider' , '$httpProvider',
  function ($stateProvider,   $urlRouterProvider,   $controllerProvider,   $compileProvider,   $filterProvider,   $provide, $interpolateProvider, $parseProvider, $httpProvider) {

        $httpProvider.interceptors.push(['$q', '$location', 'store', function($q, $location, store) {
            return {
                'request': function (config) {
                    config.headers = config.headers || {};
                    $httpProvider.defaults.headers.common['Authorization'] = "Bearer " + store.get('AccessToken');
                    return config;
                },
                'responseError': function(response) {
                    if(response.status === 401 || response.status === 403) {
                         store.remove('AccessToken')
                         window.location = '/sedonaadmin/admin/logout';
                    }
                    return $q.reject(response);
                }
            };
        }]);

        // lazy controller, directive and service
        app.controller = $controllerProvider.register;
        app.directive  = $compileProvider.directive;
        app.factory    = $provide.factory;
        app.constant   = $provide.constant;
        app.value      = $provide.value;

        $interpolateProvider.startSymbol('{[{');
        $interpolateProvider.endSymbol('}]}');


        $urlRouterProvider
        .otherwise('/dashboard');

        $stateProvider

        .state('dashboard', {
            url: '/dashboard',
            controller: 'DashboardCtrl',
            templateUrl: '/sedonaadmin/admin/dashboard',
            resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/dashboard.js',
                        '/be/js/scripts/factory/dashboard.js'
                        ]);
                }]
            }
        })

        /*USER STATE*/
        .state('userscreate', {
            url: '/userscreate',
            templateUrl: '/sedonaadmin/users/create',
            resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/users/createuser.js',
                        '/be/js/scripts/directives/validate.js',
                        '/be/js/scripts/factory/user.js'
                        ]);
                }]
            }
        })
        .state('userlist', {
            url: '/userlist/:userid',
            controller: 'UserListCtrl',
            templateUrl: '/sedonaadmin/users/userlist',
            resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/users/manageusers.js',
                        '/be/js/scripts/directives/validate.js',
                        '/be/js/scripts/factory/user.js'
                        ]);
                }]
            }
        })
        .state('updateuser', {
            url: '/updateuser/:userid',
            templateUrl: '/sedonaadmin/users/edituser',
            resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/users/edituser.js',
                        '/be/js/scripts/directives/validate.js',
                        '/be/js/scripts/factory/user.js'
                        ]);
                }]
            }
        })
        .state('logout', {
            url: '/updateuser/:userid',
             templateUrl: '/sedonaadmin/admin/logout',

        })
        /*END USER STATE*/


            //rainier state


            //start createpage
            .state('createpage', {
                url: '/createpage',
                controller: 'Createpage',
                templateUrl: '/sedonaadmin/pages/createpage',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/factory/page/page.js'
                            ]);
                        }]
                }

            })
            //end createpage

            //start managepage
            .state('managepage', {
                url: '/managepage',
                controller: 'Managepage',
                templateUrl: '/sedonaadmin/pages/managepage',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/factory/managepage.js'
                            ]);
                        }]
                }
            })
            //end managepage

            //start managepagecategory
            .state('managepagecategory', {
                url: '/managepagecategory',
                controller: 'Managepagecategory',
                templateUrl: '/sedonaadmin/pages/managepagecategory',

            })
            //end managepagecategory

            //start editpage
            .state('editpage', {
                url: '/editpage/:pageid',
                controller: 'Editpage',
                templateUrl: '/sedonaadmin/pages/editpage',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/factory/page/page.js'
                            ]);
                        }]
                }
            })
            //end editpage

            //start AURA
            .state('auraservice', {
                url: '/service/aura',
                controller: 'Createpage',
                templateUrl: '/sedonaadmin/pages/aura',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/factory/page/page.js'
                            ]);
                        }]
                }

            })
            //end AURA

            //start managepage
            .state('manage_auraservice', {
                url: '/manage/service/aura',
                controller: 'Managepage_aura',
                templateUrl: '/sedonaadmin/pages/managepage_aura',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/page/managepage_aura.js',
                                '/be/js/scripts/factory/page/page.js'
                            ]);
                        }]
                }
            })
            //end managepage

            //start editpage_aura
            .state('editpage_aura', {
                url: '/editpage/aura/:pageid',
                controller: 'Editpage',
                templateUrl: '/sedonaadmin/pages/editpage_aura',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/factory/page/page.js'
                            ]);
                        }]
                }
            })
            //end editpage_aura




            //start createnews
            .state('createnews', {
                url: '/createnews',
                controller: 'Createnews',
                templateUrl: '/sedonaadmin/news/createnews',
                resolve: {
                  deps: ['uiLoad',
                  function( uiLoad ){
                    return uiLoad.load( [
                      '/be/js/scripts/controllers/news/createnews.js',
                      '/be/js/scripts/factory/news/createnews.js',
                        '/be/js/scripts/factory/news/tags.js',
                        '/be/js/scripts/factory/news/category.js',
                        '/be/js/scripts/filter.js'
                      ]);
                  }]
                }

            })
            //end createnews

            //start managenews
            .state('managenews', {
                url: '/managenews',
                controller: 'Managenews',
                templateUrl: '/sedonaadmin/news/managenews',
                resolve: {
                  deps: ['uiLoad',
                  function( uiLoad ){
                    return uiLoad.load( [
                      '/be/js/scripts/controllers/news/managenews.js',
                      '/be/js/scripts/factory/news/managenews.js'
                      ]);
                  }]
                }

            })
            //end managenews

            //start editnews
            .state('editnews', {
                url: '/editnews/:newsid',
                controller: 'Editnews',
                templateUrl: '/sedonaadmin/news/editnews',
                resolve: {
                  deps: ['uiLoad',
                  function( uiLoad ){
                    return uiLoad.load( [
                      '/be/js/scripts/controllers/news/editnews.js',
                      '/be/js/scripts/factory/news/createnews.js',
                        '/be/js/scripts/factory/news/tags.js',
                        '/be/js/scripts/factory/news/category.js'
                      ]);
                  }]
                }

            })
            //end editnews

            //start tags
            .state('tags', {
                url: '/tags',
                controller: 'Tags',
                templateUrl: '/sedonaadmin/news/tags',
                resolve: {
                  deps: ['uiLoad',
                  function( uiLoad ){
                    return uiLoad.load( [
                      '/be/js/scripts/controllers/news/tags.js',
                        '/be/js/scripts/factory/news/tags.js'
                      ]);
                  }]
                }
            })
            //end tags

            //start tags
            .state('category', {
                url: '/category',
                controller: 'Category',
                templateUrl: '/sedonaadmin/news/category',
                resolve: {
                  deps: ['uiLoad',
                  function( uiLoad ){
                    return uiLoad.load( [
                      '/be/js/scripts/controllers/news/category.js',
                        '/be/js/scripts/factory/news/category.js'
                      ]);
                  }]
                }
            })
            //end tags

            //start Create Author
            .state('createauthor', {
                url: '/createauthor',
                controller: 'Createauthor',
                templateUrl: '/sedonaadmin/news/createauthor',
                resolve: {
                  deps: ['uiLoad',
                  function( uiLoad ){
                    return uiLoad.load( [
                      '/be/js/scripts/controllers/news/createauthor.js',
                      '/be/js/scripts/factory/news/createauthor.js'
                      ]);
                  }]
                }
            })
            //end Create Author

            //start Manage Author
            .state('manageauthor', {
                url: '/manageauthor',
                controller: 'Manageauthor',
                templateUrl: '/sedonaadmin/news/manageauthor',
                resolve: {
                  deps: ['uiLoad',
                  function( uiLoad ){
                    return uiLoad.load( [
                      '/be/js/scripts/controllers/news/manageauthor.js',
                      '/be/js/scripts/factory/news/manageauthor.js'
                      ]);
                  }]
                }
            })

            //start Edit Author
            .state('editauthor', {
                url: '/editauthor:authorid',
                controller: 'Editauthor',
                templateUrl: '/sedonaadmin/news/editauthor',
                resolve: {
                  deps: ['uiLoad',
                  function( uiLoad ){
                    return uiLoad.load( [
                      '/be/js/scripts/controllers/news/editauthor.js',
                      '/be/js/scripts/factory/news/createauthor.js'
                      ]);
                  }]
                }
            })
            //end Edit Author

            //start testi
            .state('testimonials', {
                url: '/testimonials',
                controller: 'TestimonialsCTRL',
                templateUrl: '/sedonaadmin/testimonials',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/testimonials.js',
                                '/be/js/scripts/factory/testimonials.js',
                                '/be/js/scripts/factory/page/page.js'
                            ]);
                        }]
                }
            })
            //end testi

            //start booking
            .state('booking', {
                url: '/booking',
                controller: 'BookingCTRL',
                templateUrl: '/sedonaadmin/booking',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/booking.js',
                                '/be/js/scripts/factory/booking.js'
                            ]);
                        }]
                }
            })

            .state('scheduling', {
                url: '/booking/scheduling',
                controller: 'SchedulingCTRL',
                templateUrl: '/sedonaadmin/booking/scheduling',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/scheduling.js',
                                '/be/js/scripts/factory/scheduling.js',
                                '/be/js/scripts/factory/page/page.js'
                            ]);
                        }]
                }
            })

            .state('schedulereports', {
                url: '/booking/reports',
                controller: 'SchedulingCTRL',
                templateUrl: '/sedonaadmin/booking/reports',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/scheduling.js',
                                '/be/js/scripts/factory/scheduling.js',
                                '/be/js/scripts/factory/page/page.js'
                            ]);
                        }]
                }
            })

            .state('schedulereports.servicesales', {
                url: '/booking/reports/sales',
                controller: 'SchedulingReportsCTRL',
                templateUrl: '/sedonaadmin/booking/salesreport',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/schedulingreports.js',
                                '/be/js/scripts/factory/scheduling.js',
                                '/be/js/scripts/factory/page/page.js'
                            ]);
                        }]
                }
            })


            .state('schedulereports.saleschart', {
                url: '/booking/reports/saleschart',
                controller: 'SalesChartCTRL',
                templateUrl: '/sedonaadmin/booking/saleschart',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/saleschart.js',
                                '/be/js/scripts/factory/saleschart.js'
                            ]);
                        }]
                }
            })
            //end booking

            //member reports

            .state('schedulereports.member_report', {
                url: '/booking/reports/member_report',
                controller: 'MemberReportsCTRL',
                templateUrl: '/sedonaadmin/booking/member_report',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/memberreports.js',
                                '/be/js/scripts/factory/member.js'
                            ]);
                        }]
                }
            })

            .state('schedulereports.memberchart', {
                url: '/booking/reports/memberchart',
                controller: 'MemberChartCTRL',
                templateUrl: '/sedonaadmin/booking/memberchart',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/memberchart.js',
                                '/be/js/scripts/factory/memberchart.js'
                            ]);
                        }]
                }
            })

            //end

            //create test
             .state('createtest', {
                url: '/test/createtest',
                controller: 'CreateTestCTRL',
                templateUrl: '/sedonaadmin/test/createtest',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/test/createtest.js',
                                '/be/js/scripts/factory/createtest.js'
                            ]);
                        }]
                }
            })

             .state('createresult', {
                url: '/test/createresult/:testid',
                controller: 'CreateResultCTRL',
                templateUrl: '/sedonaadmin/test/createresult',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/test/createresult.js',
                                '/be/js/scripts/factory/createtest.js'
                            ]);
                        }]
                }
            })

               .state('viewtests', {
                url: '/test/viewtests',
                controller: 'ViewTestCTRL',
                templateUrl: '/sedonaadmin/test/viewtests',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/test/viewtest.js',
                                '/be/js/scripts/factory/viewtest.js'
                            ]);
                        }]
                }
            })
                .state('testtakers', {
                url: '/test/testtakers',
                controller: 'TestMemberCTRL',
                templateUrl: '/sedonaadmin/test/testtakers',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/test/testtakers.js',
                                '/be/js/scripts/factory/viewtest.js'
                            ]);
                        }]
                }
            })

             //edit test
             .state('edittest', {
                url: '/edittest/:testid',
                controller: 'EditTestCTRL',
                templateUrl: '/sedonaadmin/test/edittest',
                resolve: {
                  deps: ['uiLoad',
                  function( uiLoad ){
                    return uiLoad.load( [
                      '/be/js/scripts/controllers/test/edittest.js',
                        '/be/js/scripts/factory/viewtest.js'
                      ]);
                  }]
                }

            })

              //SLIDER HERE
            .state('create_album', {
                url: '/create_album',
                controller: 'createAlbumCtrl',
                templateUrl: '/sedonaadmin/slider/create_album',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/slider/create_album.js'
                            // '/be/js/config.js'
                            ]);
                    }]
                }
            })
            .state('edit_album', {
                url: '/edit_album/:id',
                controller: 'editAlbumCtrl',
                templateUrl: '/sedonaadmin/slider/edit_album',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/slider/edit_album.js'
                            // '/be/js/config.js'
                            ]);
                    }]
                }
            })


            .state('menu_creator', {
                url: '/menu_creator',
                controller: 'menuCreatorCtrl',
                templateUrl: '/sedonaadmin/menu/menu_creator',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/menu/menu_creator.js',
                            '/be/js/scripts/factory/menu/menu_creator.js',
                            '/be/js/jquery/nestable/jquery.nestable.js'
                            ]);
                    }]
                }
            })




            //start settings
            .state('settings', {
                url: '/settings',
                controller: 'SettingsCTRL',
                templateUrl: '/sedonaadmin/settings',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/settings.js',
                                '/be/js/scripts/factory/settings.js'
                            ]);
                        }]
                }
            })
            //end settings

            //start settings
            .state('contactus', {
                url: '/contactus',
                controller: 'ContactusCtrl',
                templateUrl: '/sedonaadmin/contactus',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/contactus.js',
                                '/be/js/scripts/factory/contactus.js'
                            ]);
                        }]
                }
            })
            //end settings

            //Profile
            .state('profile', {
                url: '/profile/:userid',
                controller: 'ProfileCtrl',
                templateUrl: '/sedonaadmin/profile',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                          '/be/js/scripts/controllers/profile.js',
                          '/be/js/scripts/factory/user.js'
                        ]);
                    }]
                }
            })

            .state('homepagecreate', {
                url: '/homepagecreate',
                controller: 'HomepagecreateCtrl',
                templateUrl: '/sedonaadmin/homepage/create',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                          '/be/js/scripts/controllers/homepage/create.js',
                          '/be/js/scripts/factory/homepage/homepage.js'
                        ]);
                    }]
                }
            })

            .state('homepagemanage', {
                url: '/homepagemanage',
                controller: 'HomepagemanageCtrl',
                templateUrl: '/sedonaadmin/homepage/manage',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                          '/be/js/scripts/controllers/homepage/manage.js',
                          '/be/js/scripts/factory/homepage/homepage.js'
                        ]);
                    }]
                }
            })

            .state('updatehomepage', {
                url: '/homepageedit/:contentid',
                controller: 'HomepageupdateCtrl',
                templateUrl: '/sedonaadmin/homepage/edit',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/homepage/edit.js',
                            '/be/js/scripts/factory/homepage/homepage.js'
                            ]);
                    }]
                }
            })
             //start Pagebuilder
            .state('pagecreate', {
                url: '/pagecreate',
                controller: 'Pagecreate',
                templateUrl: '/sedonaadmin/page/pagecreate',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/pagebuilder/pagecreate.js',
                                '/be/js/scripts/factory/pagebuilder/specialpage.js',
                                '/be/js/jquery/nestable/jquery.nestable2.js'
                            ]);
                        }]
                }

            })


            /*product*/
            .state('addproduct', {
                url: '/addproduct',
                controller: 'AddProduct',
                templateUrl: '/sedonaadmin/product/addproduct',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/product/addproduct.js',
                                '/be/js/scripts/factory/product.js'
                            ]);
                        }
                    ]
                }
            })

            .state('manageproduct', {
                url: '/manageproduct',
                controller: 'ManageProduct',
                templateUrl: '/sedonaadmin/product/manageproduct',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/product/manage.js',
                                '/be/js/scripts/factory/product.js'
                            ]);
                        }
                    ]
                }
            })

            .state('editproduct', {
                url: '/editproduct/:productid',
                controller: 'EditProduct',
                templateUrl: '/sedonaadmin/product/editproduct',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/product/edit.js',
                                '/be/js/scripts/factory/product.js'
                            ]);
                        }
                    ]
                }
            })

            .state('viewproduct', {
                url: '/viewproduct/:productid',
                controller: 'ViewProduct',
                templateUrl: '/sedonaadmin/product/viewproduct',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/product/view.js',
                                '/be/js/scripts/factory/product.js'
                            ]);
                        }
                    ]
                }
            })

            .state('manageproductcategory', {
                url: '/manageproductcategory',
                controller: 'ManageCategory',
                templateUrl: '/sedonaadmin/product/managecategory',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/product/managecategory.js',
                                '/be/js/scripts/factory/product.js'
                            ])
                        }
                    ]
                }
            })

            .state('manageproductsubcategory', {
                url: '/managesubcategory',
                controller: 'ManageSubCategory',
                templateUrl: '/sedonaadmin/product/managesubcategory',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/product/managesubcategory.js',
                                '/be/js/scripts/factory/product.js'
                            ]);
                        }
                    ]
                }
            })

            .state('manageproducttype', {
                url: '/manageproducttype',
                controller: 'ManageType',
                templateUrl: '/sedonaadmin/product/managetype',
                resolve: {
                    deps: ['uiLoad', 
                        function(uiLoad){
                            return uiLoad.load([
                                '/be/js/scripts/controllers/product/managetype.js',
                                '/be/js/scripts/factory/product.js'
                            ]);
                        }
                    ]
                }
            })

            .state('manageproducttags', {
                url : '/manageproducttags',
                controller: 'ManageTags',
                templateUrl: '/sedonaadmin/product/managetags',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/product/managetags.js',
                                '/be/js/scripts/factory/product.js'
                            ])
                        }
                    ]
                }
            })

            .state('pagemanage', {
                url: '/pagemanage',
                controller: 'pagemanageCtrl',
                templateUrl: '/sedonaadmin/page/pagemanage',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/pagebuilder/pagemanage.js',
                                '/be/js/scripts/factory/pagebuilder/specialpage.js'
                            ]);
                        }]
                }

            })

            .state('pageedit', {
                url: '/pageedit/:pageid',
                controller: 'pageeditCtrl',
                templateUrl: '/sedonaadmin/page/pageedit',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                           '/be/js/scripts/controllers/pagebuilder/pageedit.js',
                            '/be/js/scripts/factory/pagebuilder/specialpage.js',
                            '/be/js/jquery/nestable/jquery.nestable2.js'
                            ]);
                    }]
                }
            })


            .state('pagefooter', {
                url: '/pagefooter',
                controller: 'pagefooterCtrl',
                templateUrl: '/sedonaadmin/page/pagefooter',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                           '/be/js/scripts/controllers/pagebuilder/pagefooter.js',
                            '/be/js/scripts/factory/pagebuilder/specialpage.js'
                            ]);
                    }]
                }
            })

            /*order*/
            .state('addorder', {
                url: '/offlineorder',
                controller: 'AddOrder',
                templateUrl: '/sedonaadmin/order/addorder',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/order/addorder.js',
                                '/be/js/scripts/factory/order.js'
                            ]);
                        }
                    ]
                }
            })

            .state('manageorder', {
                url: '/manageorders',
                controller: 'ManageOrder',
                templateUrl: '/sedonaadmin/order/manage',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/order/manage.js',
                                '/be/js/scripts/factory/order.js'
                            ]);
                        }
                    ]
                }
            })

            .state('vieworder', {
                url: '/vieworder/:id',
                controller: 'ViewOrder',
                templateUrl: '/sedonaadmin/order/view',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/order/view.js',
                                '/be/js/scripts/factory/order.js'
                            ]);
                        }
                    ]
                }
            })

            .state('orderreports', {
                url: '/order/reports',
                controller: 'ManageOrder',
                templateUrl: '/sedonaadmin/order/reports',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load([
                                '/be/js/scripts/controllers/order/manage.js',
                                '/be/js/scripts/factory/order.js'
                            ]);
                        }]
                }
            })

            .state('orderreports.ordersales', {
                url: '/ordersales',
                controller: 'Orderreports',
                templateUrl: '/sedonaadmin/order/salesreport',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/order/orderreports.js',
                                '/be/js/scripts/factory/order.js'
                            ]);
                        }]
                }
            })
            .state('orderreports.saleschart', {
                url: '/orderchart',
                controller: 'OrderCharts',
                templateUrl: '/sedonaadmin/order/saleschart',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/order/ordercharts.js',
                                '/be/js/scripts/factory/order.js'
                            ]);
                        }]
                }
            })


    }])


.config(['$translateProvider', function($translateProvider){

  // Register a loader for the static files
  // So, the module will search missing translation tables under the specified urls.
  // Those urls are [prefix][langKey][suffix].
  $translateProvider.useStaticFilesLoader({
    prefix: '/be/js/jsons/',
    suffix: '.json'
});

  // Tell the module what language to use by default
  $translateProvider.preferredLanguage('en');

  // Tell the module to store the language in the local storage
  $translateProvider.useLocalStorage();

}])

/**
 * jQuery plugin config use ui-jq directive , config the js and css files that required
 * key: function name of the jQuery plugin
 * value: array of the css js file located
 */
 .constant('JQ_CONFIG', {
    easyPieChart:   ['/be/js/jquery/charts/easypiechart/jquery.easy-pie-chart.js'],
    sparkline:      ['/be/js/jquery/charts/sparkline/jquery.sparkline.min.js'],
    plot:           ['/be/js/jquery/charts/flot/jquery.flot.min.js',
    '/be/js/jquery/charts/flot/jquery.flot.resize.js',
    '/be/js/jquery/charts/flot/jquery.flot.tooltip.min.js',
    '/be/js/jquery/charts/flot/jquery.flot.spline.js',
    '/be/js/jquery/charts/flot/jquery.flot.orderBars.js',
    '/be/js/jquery/charts/flot/jquery.flot.pie.min.js'],
    slimScroll:     ['/be/js/jquery/slimscroll/jquery.slimscroll.min.js'],
    sortable:       ['/be/js/jquery/sortable/jquery.sortable.js'],
    nestable:       ['/be/js/jquery/nestable/nestable.css'],
    filestyle:      ['/be/js/jquery/file/bootstrap-filestyle.min.js'],
    slider:         ['/be/js/jquery/slider/bootstrap-slider.js',
    '/be/js/jquery/slider/slider.css'],
    chosen:         ['/be/js/jquery/chosen/chosen.jquery.min.js',
    '/be/js/jquery/chosen/chosen.css'],
    TouchSpin:      ['/be/js/jquery/spinner/jquery.bootstrap-touchspin.min.js',
    '/be/js/jquery/spinner/jquery.bootstrap-touchspin.css'],
    wysiwyg:        ['/be/js/jquery/wysiwyg/bootstrap-wysiwyg.js',
    '/be/js/jquery/wysiwyg/jquery.hotkeys.js'],
    dataTable:      ['/be/js/jquery/datatables/jquery.dataTables.min.js',
    '/be/js/jquery/datatables/dataTables.bootstrap.js',
    '/be/js/jquery/datatables/dataTables.bootstrap.css'],
    vectorMap:      ['/be/js/jquery/jvectormap/jquery-jvectormap.min.js',
    '/be/js/jquery/jvectormap/jquery-jvectormap-world-mill-en.js',
    '/be/js/jquery/jvectormap/jquery-jvectormap-us-aea-en.js',
    '/be/js/jquery/jvectormap/jquery-jvectormap.css'],
    footable:       ['/be/js/jquery/footable/footable.all.min.js',
    '/be/js/jquery/footable/footable.core.css']
}
)


.constant('MODULE_CONFIG', {
    select2:        ['/be/js/jquery/select2/select2.css',
    '/be/js/jquery/select2/select2-bootstrap.css',
    '/be/js/jquery/select2/select2.min.js',
    '/be/js/modules/ui-select2.js']
}
)
;
