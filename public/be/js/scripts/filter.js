/**
 * Created by Apple on 7/18/15.
 */
app.filter('returnYoutubeThumb', function(){
    return function (item) {
        if(item){
            var newdata = item;
            var x;
            x = newdata.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
            return 'http://img.youtube.com/vi/' + x[1] + '/hqdefault.jpg';
        }else{
            return x;
        }
    };
});

app.filter('returnImageThumb', function(Config){
    return function (item) {
        if(item){
            return Config.amazonlink + "/uploads/newsimage/" + item;
        }else{
            return item;
        }
    };
})

app.filter('timehnow', function(Config){
    return function (sched) {
        if(sched){
            return moment(sched).format("h:mm a");
        }else{
            return item;
        }
    };
})
app.filter('unique', function() {
   return function(collection, keyname) {
      var output = [], 
          keys = [];

      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key);
              output.push(item);
          }
      });

      return output;
   };
});
