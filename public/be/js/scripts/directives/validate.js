

'use strict';
app.directive('uiModule', ['', function() {
  return{
    restrict: 'EA',
    controller: function($scope){

    }

  };
}])

/* Directives */
// All the directives rely on jQuery.
.directive('ngSpace', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if(event.which === 32) {
                        scope.$apply(function(){
                                scope.$eval(attrs.ngSpace);
                        });

                        event.preventDefault();
                }
            });
        };
});
