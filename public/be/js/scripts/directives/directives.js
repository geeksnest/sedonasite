'use strict';

/* Directives */
// All the directives rely on jQuery.

angular.module('app.directives', ['ui.load'])
  .directive('uiModule', ['MODULE_CONFIG','uiLoad', '$compile', function(MODULE_CONFIG, uiLoad, $compile) {
    return {
      restrict: 'A',
      compile: function (el, attrs) {
        var contents = el.contents().clone();
        return function(scope, el, attrs){
          el.contents().remove();
          uiLoad.load(MODULE_CONFIG[attrs.uiModule])
          .then(function(){
            $compile(contents)(scope, function(clonedElement, scope) {
              el.append(clonedElement);
            });
          });
        }
      }
    };
  }])
  .directive('uiShift', ['$timeout', function($timeout) {
    return {
      restrict: 'A',
      link: function(scope, el, attr) {
        // get the $prev or $parent of this el
        var _el = $(el),
            _window = $(window),
            prev = _el.prev(),
            parent,
            width = _window.width()
            ;

        !prev.length && (parent = _el.parent());
        
        function sm(){
          $timeout(function () {
            var method = attr.uiShift;
            var target = attr.target;
            _el.hasClass('in') || _el[method](target).addClass('in');
          });
        }
        
        function md(){
          parent && parent['prepend'](el);
          !parent && _el['insertAfter'](prev);
          _el.removeClass('in');
        }

        (width < 768 && sm()) || md();

        _window.resize(function() {
          if(width !== _window.width()){
            $timeout(function(){
              (_window.width() < 768 && sm()) || md();
              width = _window.width();
            });
          }
        });
      }
    };
  }])
  .directive('uiToggleClass', ['$timeout', '$document', function($timeout, $document) {
    return {
      restrict: 'AC',
      link: function(scope, el, attr) {
        el.on('click', function(e) {
          e.preventDefault();
          var classes = attr.uiToggleClass.split(','),
              targets = (attr.target && attr.target.split(',')) || Array(el),
              key = 0;
          angular.forEach(classes, function( _class ) {
            var target = targets[(targets.length && key)];            
            ( _class.indexOf( '*' ) !== -1 ) && magic(_class, target);
            $( target ).toggleClass(_class);
            key ++;
          });
          $(el).toggleClass('active');

          function magic(_class, target){
            var patt = new RegExp( '\\s' + 
                _class.
                  replace( /\*/g, '[A-Za-z0-9-_]+' ).
                  split( ' ' ).
                  join( '\\s|\\s' ) + 
                '\\s', 'g' );
            var cn = ' ' + $(target)[0].className + ' ';
            while ( patt.test( cn ) ) {
              cn = cn.replace( patt, ' ' );
            }
            $(target)[0].className = $.trim( cn );
          }
        });
      }
    };
  }])
  .directive('uiNav', ['$timeout', function($timeout) {
    return {
      restrict: 'AC',
      link: function(scope, el, attr) {
        var _window = $(window), 
        _mb = 768, 
        wrap = $('.app-aside'), 
        next, 
        backdrop = '.dropdown-backdrop';
        // unfolded
        el.on('click', 'a', function(e) {
          next && next.trigger('mouseleave.nav');
          var _this = $(this);
          _this.parent().siblings( ".active" ).toggleClass('active');
          _this.next().is('ul') &&  _this.parent().toggleClass('active') &&  e.preventDefault();
          // mobile
          _this.next().is('ul') || ( ( _window.width() < _mb ) && $('.app-aside').removeClass('show off-screen') );
        });

        // folded & fixed        
        el.on('mouseenter', 'a', function(e){
          next && next.trigger('mouseleave.nav');

          if ( !$('.app-aside-fixed.app-aside-folded').length || ( _window.width() < _mb )) return;
          var _this = $(e.target)
          , top
          , w_h = $(window).height()
          , offset = 50
          , min = 150;

          !_this.is('a') && (_this = _this.closest('a'));
          if( _this.next().is('ul') ){
             next = _this.next();
          }else{
            return;
          }
         
          _this.parent().addClass('active');
          top = _this.parent().position().top + offset;
          next.css('top', top);
          if( top + next.height() > w_h ){
            next.css('bottom', 0);
          }
          if(top + min > w_h){
            next.css('bottom', w_h - top - offset).css('top', 'auto');
          }
          next.appendTo(wrap);

          next.on('mouseleave.nav', function(e){
            $(backdrop).remove();
            next.appendTo(_this.parent());
            next.off('mouseleave.nav').css('top', 'auto').css('bottom', 'auto');
            _this.parent().removeClass('active');
          });

          $('.smart').length && $('<div class="dropdown-backdrop"/>').insertAfter('.app-aside').on('click', function(next){
            next && next.trigger('mouseleave.nav');
          });

        });

        wrap.on('mouseleave', function(e){
          next && next.trigger('mouseleave.nav');
        });
      }
    };
  }])
  .directive('uiScroll', ['$location', '$anchorScroll', function($location, $anchorScroll) {
    return {
      restrict: 'AC',
      link: function(scope, el, attr) {
        el.on('click', function(e) {
          $location.hash(attr.uiScroll);
          $anchorScroll();
        });
      }
    };
  }])
  .directive('uiFullscreen', ['uiLoad', function(uiLoad) {
    return {
      restrict: 'AC',
      template:'<i class="fa fa-expand fa-fw text"></i><i class="fa fa-compress fa-fw text-active"></i>',
      link: function(scope, el, attr) {
        el.addClass('hide');
        uiLoad.load('/be/js/libs/screenfull.min.js').then(function(){
          if (screenfull.enabled) {
            el.removeClass('hide');
          }
          el.on('click', function(){
            var target;
            attr.target && ( target = $(attr.target)[0] );            
            el.toggleClass('active');
            screenfull.toggle(target);
          });
        });
      }
    };
  }])
  .directive('uiButterbar', ['$rootScope', '$location', '$anchorScroll', function($rootScope, $location, $anchorScroll) {
     return {
      restrict: 'AC',
      template:'<span class="bar"></span>',
      link: function(scope, el, attrs) {        
        el.addClass('butterbar hide');        
        scope.$on('$stateChangeStart', function(event) {
          $location.hash('app');
          $anchorScroll();
          el.removeClass('hide').addClass('active');
        });
        scope.$on('$stateChangeSuccess', function( event, toState, toParams, fromState ) {
          event.targetScope.$watch('$viewContentLoaded', function(){
            el.addClass('hide').removeClass('active');
          })          
        });
      }
     };
  }])
  .directive('setNgAnimate', ['$animate', function ($animate) {
    return {
        link: function ($scope, $element, $attrs) {
            $scope.$watch( function() {
                return $scope.$eval($attrs.setNgAnimate, $scope);
            }, function(valnew, valold){
                $animate.enabled(!!valnew, $element);
            });
        }
    };
  }])
  .directive('ckEditor', [function () {
    return {
      require: '?ngModel',
      restrict: 'C',
      link: function (scope, elm, attr, model,ngModel) {
        CKEDITOR.config.allowedContent = true;
        var isReady = false;
        var data = [];
        var ck = CKEDITOR.replace(elm[0],{
          toolbarGroups: [
          { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
          { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
          { name: 'links' },
          { name: 'insert' },
          { name: 'tools', groups: [ 'mode' ] },
          { name: 'mode'},
          { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
          { name: 'styles' },
          { name: 'colors' }
          ]

        // NOTE: Remember to leave 'toolbar' property with the default value (null).
      });


        function setData() {
          if (!data.length) {
            return;
          }

          var d = data.splice(0, 1);
          ck.setData(d[0] || '<span></span>', function () {
            setData();
            isReady = true;
          });
        }

        ck.on('instanceReady', function (e) {
          if (model) {
            setData();
          }
        });

        elm.bind('$destroy', function () {
          ck.destroy(false);
        });

        function updateModel() {
          scope.$apply(function() {
            if ( ck.getData().length ) {
              model.$setViewValue('');
            }
          });
        }





        if (model) {
          ck.on('change', function () {
                        // scope.$apply(function () {
                          var data = ck.getData();
                          if (data == '<span></span>') {
                            data = null;
                          }
                          model.$setViewValue(data);
                        // });
        });

          model.$render = function (value) {
            if (model.$viewValue === undefined) {
              model.$setViewValue(null);
              model.$viewValue = null;
            }

            data.push(model.$viewValue);

            if (isReady) {
              isReady = false;
              setData();
            }
          };
        }

      }
    };
  }])
// .directive('onlyDigits', function () {
//   return {
//     restrict: 'A',
//     require: '?ngModel',
//     link: function (scope, element, attrs, ngModel) {
//       if (!ngModel) return;
//       ngModel.$parsers.unshift(function (inputValue) {
//         var digits = inputValue.split('').filter(function (s) { return (!isNaN(s) && s != ' '); }).join('');
//         ngModel.$viewValue = digits;
//         ngModel.$render();
//         return parseInt(digits);
//       });
//     }
//   };
// })
.directive('valueDateTime', function () {
      return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel) {
          if (!ngModel) return;
          ngModel.$parsers.unshift(function (inputValue) {
            var exist=false;
            var cont = true;
            var digits = inputValue.split('').filter(function (s) {
              console.log(cont);
              if((s==='h' || s==='m') && !exist){
                  exist=true;
                  return true;
              }else{
                if(exist){
                  return false;
                }else{
                  return (!isNaN(s) && s != ' ');
                }
              }
            }).join('');
            if(digits === 'h' || digits === 'm'){
              digits = '';
            }
            ngModel.$viewValue = digits;
            ngModel.$render();
            return digits;
          });
        }
      };
    }).directive('entries', function(){
      // Developer: Efren A. Bautista Jr.
      return {
        restrict: 'E',
        template: '<div class="some"> Showing {{ from }} to {{ to }} of {{ total }} entries</div>',
        //require: 'ngModel',
        link: function($scope, elem, attr, ctrl) {
          $scope.max = 0;
          $scope.offset = 0;
          $scope.total = 0;
          $scope.$watch(attr.max, function(value){
            if(value){
                $scope.max = value;
            }
          });
          $scope.$watch(attr.offset, function(value){
            if(value){
              $scope.offset = value;
            }
          });
          $scope.$watch(attr.total, function(value){
            if(value){
              $scope.total = value;
            }
          });
          $scope.$watchGroup(['max', 'offset', 'total'], function(newValues, oldValues, scope) {
            if(newValues[0] != 0 && newValues[1] != 0 && newValues[2] != 0 ){
              var showfrom = '';
              var showto = '';
              if(newValues[1]==1){
                showfrom = newValues[1];
                showto = newValues[0]
              }else{
                showto = (newValues[1] * newValues[0]);
                showfrom = (showto - newValues[0]) + 1 ;

              }

              if(showto > newValues[2]){
                showto = newValues[2];
              }
              $scope.from = showfrom;
              $scope.to = showto;
              $scope.total = newValues[2];
            }

          });

          //var textField = $('input', elem).attr('ng-model', 'myDirectiveVar');
          // $compile(textField)($scope.$parent);
        }
      };
    })
    .directive('uiEvent', ['$parse',
      function ($parse) {
        return function ($scope, elm, attrs) {
          var events = $scope.$eval(attrs.uiEvent);
          angular.forEach(events, function (uiEvent, eventName) {
            var fn = $parse(uiEvent);
            elm.bind(eventName, function (evt) {
              var params = Array.prototype.slice.call(arguments);
              //Take out first paramater (event object);
              params = params.splice(1);
              fn($scope, {$event: evt, $params: params});
              if (!$scope.$$phase) {
                $scope.$apply();
              }
            });
          });
        };
      }])

     .directive('onlyDigits', function () {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, element, attr, ctrl) {
        function inputValue(val) {
          if (val) {
            var digits = val.replace(/[^0-9.]/g, '');

            if (digits !== val) {
              ctrl.$setViewValue(digits);
              ctrl.$render();
            }
            return parseFloat(digits);
          }
          return undefined;
        }            
        ctrl.$parsers.push(inputValue);
      }
    };
 }).directive('allowPattern', function () {
    return {
        restrict: "A",
        compile: function(tElement, tAttrs) {
            return function(scope, element, attrs) {
        // I handle key events
                element.bind("keypress", function(event) {
                    var keyCode = event.which || event.keyCode; // I safely get the keyCode pressed from the event.
                    var keyCodeChar = String.fromCharCode(keyCode); // I determine the char from the keyCode.
          
          // If the keyCode char does not match the allowed Regex Pattern, then don't allow the input into the field.
                    if (!keyCodeChar.match(new RegExp(attrs.allowPattern, "i"))) {
            event.preventDefault();
                        return false;
                    }
          
                });
            };
        }
    };
});