'use strict';

/* Controllers */

app.controller('Managepage', function($scope, $state ,$q, $http, Config, $log, Managepages, $interval, $modal){
    
    $scope.alerts = [];
// console.log("dasdas");
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.data = {};
    var num = 10;
    var off = 1;
    var keyword = null;
    var sort = null;
    $scope.currentstatusshow = '';

    var loadlist = function(num, off, keyword, sort){
    Managepages.sample(num,off, keyword,sort, function(data){
        $scope.datas = data.tot;
        $scope.ocount = data.ocount;
        $scope.others = data.others;
        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
        
    });
}
 loadlist(num, off, keyword, sort);



    $scope.search = function (keyword) {
        // var off = 0;
        $scope.keyword = keyword;
        loadlist(num, off, keyword, $scope.sort);
    }
$scope.clear = function(){
        $scope.keyword=null;
        loadlist(num, off, keyword, $scope.sort);
    }

    $scope.sortpage = function(sort){
        loadlist(num, off, keyword, sort);
    }

    
    $scope.numpages = function (off, keyword) {
        loadlist(num, off, keyword, $scope.sort);
  
    }

    $scope.setPage = function (pageNo) {
        loadlist(num, pageNo, keyword, $scope.sort);
    };


    $scope.setstatus = function (status,pageid,keyword) {
        if(status == 1)
        {
            $http({
                    url: Config.ApiURL + "/pages/updatepagestatus/0/" + pageid + '/' + keyword,
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function (data, status, headers, config) {
                    // Managepages.sample(num,$scope.bigCurrentPage, keyword, function(data){
                    //     $scope.data = data;

                    //     $scope.maxSize = 5;
                    //     $scope.bigTotalItems = data.total_items;
                    //     $scope.bigCurrentPage = data.index;
                    // });

                    $scope.currentstatusshow = pageid;
                    var i = 2;
                    setInterval(function(){
                        i--;

                        if(i == 0)
                        {
                            Managepages.sample(num,$scope.bigCurrentPage, keyword, function(data){
                                $scope.data = data;

                                $scope.maxSize = 5;
                                $scope.bigTotalItems = data.total_items;
                                $scope.bigCurrentPage = data.index;
                            });

                            $scope.currentstatusshow = 0;
                        }


                    },1000)
                    
                }).error(function (data, status, headers, config) {
                    Managepages.sample(num,$scope.bigCurrentPage, keyword, function(data){
                        $scope.data = data;

                        $scope.maxSize = 5;
                        $scope.bigTotalItems = data.total_items;
                        $scope.bigCurrentPage = data.index;
                    });
                    
                });
        }
        else
        {
             $http({
                    url: Config.ApiURL + "/pages/updatepagestatus/1/" + pageid + '/' + keyword,
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function (data, status, headers, config) {
                    $scope.currentstatusshow = pageid;
                    var i = 2;
                    setInterval(function(){
                        i--;
                        if(i == 0)
                        {
                            Managepages.sample(num,$scope.bigCurrentPage, keyword, function(data){
                                $scope.data = data;

                                $scope.maxSize = 5;
                                $scope.bigTotalItems = data.total_items;
                                $scope.bigCurrentPage = data.index;
                            });
                            $scope.currentstatusshow = 0;
                        }


                    },1000)
                }).error(function (data, status, headers, config) {
                   Managepages.sample(num,$scope.bigCurrentPage, keyword, function(data){
                        $scope.data = data;

                        $scope.maxSize = 5;
                        $scope.bigTotalItems = data.total_items;
                        $scope.bigCurrentPage = data.index;
                    });
                });
        }
    }

    

  
    $scope.deletepage = function(pageid) {

        var modalInstance = $modal.open({
            templateUrl: 'pageDelete.html',
            controller: pageDeleteCTRL,
            resolve: {
                pageid: function() {
                    return pageid
                }
            }
        });
    }

    var loadpage = function(){

        Managepages.sample(num,$scope.bigCurrentPage, $scope.searchtext, function(data){
                        $scope.data = data;
                        $scope.maxSize = 5;
                        $scope.bigTotalItems = data.total_items;
                        $scope.bigCurrentPage = data.index;
                });

    }


    var successloadalert = function(){
            $scope.alerts.splice(0, 1);
            $scope.alerts.push({ type: 'success', msg: 'Page successfully Deleted!' });
            
    }

    var errorloadalert = function(){
            $scope.alerts.push({ type: 'danger', msg: 'Something went wrong Page not Deleted!' });
    }


       
    var pageDeleteCTRL = function($scope, $modalInstance, pageid) {
        $scope.ok = function() {
            $http({
                url: Config.ApiURL + "/page/pagedelete/" + pageid,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            }).success(function(data, status, headers, config) {
                
                loadpage();
                $modalInstance.close();
                successloadalert();

            }).error(function(data, status, headers, config) {
                loadpage();
                $modalInstance.close();
                errorloadalert();
            });

        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };


        };


    var pageEditCTRL = function($scope, $modalInstance, pageid, $state) {
        $scope.pageid = pageid;
        $scope.ok = function(pageid) {
            $scope.pageid = pageid;
            $state.go('editpage', {pageid: pageid });
                $modalInstance.dismiss('cancel');
            };
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }

        $scope.editpage = function(pageid) {
            $scope.newsid
            var modalInstance = $modal.open({
                templateUrl: 'pageEdit.html',
                controller: pageEditCTRL,
                resolve: {
                    pageid: function() {
                        return pageid
                    }
                }
            });
        }

    

})