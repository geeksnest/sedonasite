'use strict';

/* Controllers */

app.controller('SchedulingCTRL', function($scope, $state ,$q, $http, Config, $timeout, $modal, uiCalendarConfig, Page, Scheduling, Notification, $compile){

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $scope.sched = {reservation: ''};

    $scope.counts = {
        unattended: 0,
        done: 0,
        void: 0,
        pending: 0,
        reserved: 0
    }

    $scope.user = {
        'email': '',
        'lastname': '',
        'firstname': '',
        'address1': '',
        'address2': '',
        'zipcode': '',
        'country': '',
        'state': '',
        'phoneno': ''
    };

    $scope.table=false;

    $scope.invoice = '';
    $scope.schedlist = '';
    $scope.showOfflineReg = false;

    $scope.searchdate=null;
    $scope.keyword=null;

    $scope.months =  Scheduling.month();
    $scope.days =  Scheduling.day();
    $scope.years =  Scheduling.year();

    $scope.searchStatus = 'ALL';



    var paginate = function(num, off,keyword){
        $scope.loading = true;
        var currdate = moment().format('YYYY-MM-DD HH:mm:00');

        // console.log($scope.filterdate['day'] + " "  + $scope.filterdate['month'] + " "  + $scope.filterdate['year']);

        if(!$scope.filterdate['day']){
            $scope.filterdate['day'] = 0
        }

        if(!$scope.filterdate['month']){
            $scope.filterdate['month'] = 0
        }

        if(!$scope.filterdate['year']){
            $scope.filterdate['year'] = 0
        }

        if(!$scope.filterdate['day'] && !$scope.filterdate['month'] && $scope.filterdate['year']){
            $scope.txtdate = $scope.filterdate['year']
        }

        if(!$scope.filterdate['day'] && $scope.filterdate['month'] && $scope.filterdate['year']){
            $scope.txtdate = moment($scope.filterdate['year'] + "-" +$scope.filterdate['month'] + "-" + "1").format('MMMM YYYY');
        }

        if($scope.filterdate['day'] && $scope.filterdate['month'] && $scope.filterdate['year']){
            $scope.txtdate = moment($scope.filterdate['year'] + "-" + $scope.filterdate['month'] + "-" + $scope.filterdate['day']).format('dddd, MMMM Do YYYY');
        }



        Scheduling.list(num, off, keyword , $scope.filterdate['day'], $scope.filterdate['month'], $scope.filterdate['year'] , $scope.searchStatus , currdate,  function(data){

            $scope.loading = false;
            $scope.maxSize = 10;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
            // console.log("ORIGIN LIST");
            // console.log(data.data);
            for(var key in data.data){
                var n = {
                    title: data.data[key]['title'],
                    start: moment(data.data[key]['startdatetime']),
                    end: moment(data.data[key]['enddatetime']),
                    txtdate: moment(data.data[key]['scheduledate']).format('dddd, MMMM Do YYYY'),
                    txtstime: moment(data.data[key]['startdatetime']).format('h:mm:ss a'),
                    txtetime: moment(data.data[key]['enddatetime']).format('h:mm:ss a'),
                    txtname: data.data[key]['firstname'] + " " + data.data[key]['lastname'],
                    hourday: data.data[key]['hourday'],
                    status: data.data[key]['status'],
                    invoiceno: data.data[key]['invoiceno'],
                    schedid: data.data[key]['schedid'],
                    info: data.data[key]['description'],
                    paymenttype: data.data[key]['paymenttype'],
                    updated_at: moment(data.data[key]['updated_at']).format('YYYY-MM-DD'),
                    created_at: moment(data.data[key]['created_at']).format('YYYY-MM-DD')
                }
                var ndata = getStatus(data.data[key]['status'], data.data[key]['startdatetime'], data.data[key]['enddatetime']);
                n['unattended'] = ndata.unattended;
                n['textColor'] = data.data[key]['colorlegend'];

                data.data[key] = n;
            }
            $scope.schedlist = data.data;
            console.log(data.data);
        });
    }

    $scope.setPage = function (pageNo) {
        paginate(10, pageNo, $scope.keyword);
    };

    Page.categorylist(function(data){
        $scope.pagescatlegen = data;
    })
    $scope.getTotal = function(){
        var total = 0;
        for(var i = 0; i < $scope.user.reservationlist.length; i++){
            var service = $scope.user.reservationlist[i];
            total += parseFloat(service.servicechoice.price);
        }
        return total;
    }
    $scope.search = function (keyword) {
        $scope.keyword = keyword;
        paginate(10, 1, keyword);
        $scope.searchkeyword = null;
    }
    $scope.clearsearch = function(){
        $scope.keyword=null;
        paginate(10, 1, $scope.keyword);
    }
    $scope.clearfilter= function(){
        $scope.filterdate = {
            day : 0,
            month: 0,
            year: 0
        }
        $scope.txtdate = '';
        $scope.search($scope.keyword);
    }

    $scope.clearfilter();

    var getStatus = function(status, startdatetime, enddatetime){
        var data = {
          classname : '',
          unattended : false
        };
        if(status=='VOID'){
            data['classname'] = ['bg-danger bg'];
        }else if (status=='DONE'){
            data['classname'] = ['bg-success bg'];
        }else if (moment(startdatetime).unix() <= moment().unix()  && moment(enddatetime).unix() <= moment().unix()){
            //moment(data[key]['scheduledate']).isBefore(moment())
            data['classname'] = ['bg-warning bg'];
            data['unattended'] = true;
        }else{
            data['classname'] = ['b-l b-2x b-info'];
        }
        return data;
    }

    /* this is for the booking list */
    $scope.tablelist = function(){
        var num = 10;
        var off = 1;
        var keyword = null;
        var searchdate = null;
        // console.log("TABLE LISTING");
        $scope.table = !$scope.table;
        paginate(num, off, keyword, searchdate);
    }
    /* end of booking list functions*/

    var getCurrentCount = function(){
        var currdate = moment().format('YYYY-MM-DD HH:mm:00');
        Scheduling.getcountstatus(currdate, function(data){
            $scope.counts = {
                unattended: data[0]['unattended'],
                done: data[0].done,
                void: data[0].void,
                pending: data[0].pending,
                reserved: data[0].reserved
            }
        });
    }

    getCurrentCount();

    var updateFormReset = function(){
        $scope.updateerror = '';
    }

    updateFormReset();

    var resetForm = function(){
        $scope.conflict = false;
        $scope.pagelist = '';
        $scope.editingsched = false;
        $scope.editingval = '';
        $scope.servicepricelist = '';
        $scope.user = {reservationlist: [], paymenttype: '', country: 'USA'};
        $scope.computedtime = '';
        $scope.currentprice='';
        $scope.currenttimevalue='';
        $scope.sum = 0.00;
        $scope.billinginfo = '';
        $scope.radioModel = 'cash';
        $scope.processing = false;
        $scope.steps={percent:0, step1:true, step2:false, step3:false}

        $scope.emailexist = 0 ;
        $scope.existinguser = '';

        /* Start preloading data */
        Page.getpagescat(function(data){
            $scope.pagelist = data;
            $scope.loading=false;
            // console.log('PAGE LIST!=================');
            // console.log($scope.pagelist);
        });

    }

    resetForm();

    $scope.toggleMin = function() {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();
    $scope.sched.reservation = moment().format('YYYY-MM-DD');

    $scope.selectServiceCat = function(pg){
        $scope.servicepricelist = pg.prices;
        $scope.changeView('agendaDay', 'calendar1');
        // console.log($scope.servicepricelist);
    }

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };



    $scope.$watch('sched.reservation', function(val, oldval){
        if(uiCalendarConfig.calendars['calendar1']){
            $scope.changeView('agendaDay', 'calendar1');
            console.log(val);
            uiCalendarConfig.calendars['calendar1'].fullCalendar('gotoDate',val);
        }
    });



    $scope.gotodate = function(val, editing,ss){
        console.log('HITTING EDIT');
        console.log(ss);
        $scope.table = false;
        var d = new Date(val);
        $timeout(function() {
            $scope.editingsched = editing;
            if(editing){
                $scope.editsched(ss);
            }
            $scope.changeView('agendaDay', 'calendar1');
            uiCalendarConfig.calendars['calendar1'].fullCalendar('gotoDate', d);
        }, 500);
    }

    $scope.checkEmail = function(val){
        $scope.emailexist = 0;
        $scope.existinguser = '';
        var re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if(re.test(val)){
            Scheduling.checkmemberemail(val, function(data){
                console.log(data);
                if(data.length == 0){
                    $scope.emailexist = 2;
                    console.log("Clear not used");
                    $scope.user = {email: val, reservationlist: [], paymenttype: '', country: 'USA'};
                }else{
                    $scope.emailexist = 1;
                    console.log("Email is used");
                    $scope.existinguser = data;
                }
            });
        }
    }

    $scope.useuser = function(){
        $scope.emailexist = 3;
        console.log($scope.existinguser[0]);
        $scope.user = $scope.existinguser[0];

        /* HACK - hybrid jquery and angular, if you touch this i will find you and will kill you -ef */
        $timeout(function() {
            var cindex = $("#country")[0].selectedIndex;
            print_state('state', cindex);
            console.log(cindex);
            $("#state").val($scope.existinguser[0].state);
        }, 500);
        $scope.user['reservationlist'] = [];
    }

    $scope.changedtime = function(mytime){
        console.log("CHANGING TIME===================");
        computetime();
    }

    $scope.priceselected = function(price){
        var newjson = JSON.parse(price);
        $scope.currentprice = newjson.hourday.substring(0,newjson.hourday.length-1);
        $scope.currenttimevalue = newjson.hourday.substring(newjson.hourday.length-1, newjson.hourday.length);
        console.log($scope.currenttimevalue);
        computetime();
    }

    var computetime = function(){
        var computedtime = 0;
        if($scope.currenttimevalue=='h'){
            computedtime = moment($scope.sched.mytime).add($scope.currentprice,'hour');
        }else if($scope.currenttimevalue=='m'){
            computedtime = moment($scope.sched.mytime).add($scope.currentprice,'minutes');
        }

        var starttime = moment($scope.sched.mytime).format('h:mm A');
        var endtime = moment(computedtime).format('h:mm A');
        $scope.sched.endtime = computedtime;
        $scope.computedtime = starttime + ' to ' + endtime;
    }

    $scope.saveSched = function(sched){
        $scope.conflict = false;

        var servicechoice = JSON.parse(sched.servicechoice);
        $scope.sum = parseFloat($scope.sum) + parseFloat(servicechoice.price);

        var arraylist = $scope.user.reservationlist;

        var reservationdate = moment(sched.reservation).format('YYYY-MM-DD');
        console.log(reservationdate);
        var cstime = moment(sched.mytime).format( reservationdate + ' HH:mm:00');
        var cetime = moment(sched.endtime).format(reservationdate + ' HH:mm:00');

        for(var x=0 ; x < arraylist.length; x++){

            var stime = moment(arraylist[x].startdatetime);
            var etime = moment(arraylist[x].enddatetime);

            if(moment(cstime).unix() < moment(etime).unix()  && moment(cetime).unix() > moment(stime).unix()){
                $scope.conflict = true;
                console.log("IS BETWEEN BREAK!!!!!!");
                break;
            }
            console.log(arraylist[x].servicechoice.price);
        }

        Scheduling.validate(cstime,cetime, function(data){
            console.log(data);
            if(data.length > 0 || $scope.conflict){
                $scope.conflict = true;
            }else{
                var newsched = {
                    reservationdate: reservationdate,
                    startdatetime: cstime,
                    enddatetime: cetime,
                    servicechoice: servicechoice,
                    service: sched.service,
                    text: reservationdate + ' ' + $scope.computedtime
                };
                $scope.user.reservationlist.push(newsched);
                $scope.sched.service='';
                $scope.sched.servicechoice='';
                $scope.servicepricelist='';
                $scope.conflict = false;
            }
        });

    }

    $scope.deleteService = function(index){
        $scope.sum = parseFloat($scope.sum) - parseFloat($scope.user.reservationlist[index].servicechoice.price);
        $scope.user.reservationlist.splice(index, 1);
    }

    // console.log($scope.months);

    /* Payment Modal*/
    $scope.paymentModal = function(type, user){
        $scope.user.paymenttype = type;
        var modalInstance = $modal.open({
            templateUrl: 'paymentModal.html',
            controller: function($scope, $modalInstance, $state, type,user,billinginfo, Scheduling) {
                // console.log(user);
                $scope.card = false;
                $scope.check = false;

                $scope.months =  Scheduling.month();
                $scope.years =  Scheduling.year();
                $scope.countries = Scheduling.contries().list();
                // console.log($scope.countries);
                if(type=='card'){
                    $scope.paymenttype = "CreditCard";
                    $scope.card = true;
                    /* displaying entered data as billing */
                    if(billinginfo){
                            $scope.userccreg = billinginfo;
                    }else{
                        $scope.userccreg = {
                            billingfname: user.firstname,
                            billinglname: user.lastname,
                            al1: user.address1,
                            al2: user.address2,
                            state: user.state,
                            country: user.country,
                            zip: user.zipcode
                        }
                    }
                }else{
                    $scope.paymenttype = "E-check";
                    $scope.check = true;
                    /* displaying entered data as billing */
                    if(billinginfo){
                        $scope.userecheck = billinginfo;
                    }else{
                        $scope.userecheck = {
                            billingfname: user.firstname,
                            billinglname: user.lastname,
                            al1: user.address1,
                            al2: user.address2,
                            state: user.state,
                            country: user.country,
                            zip: user.zipcode
                        }
                    }
                }
                $scope.cancel = function(){
                    $modalInstance.dismiss('cancel');
                }
                $scope.submit = function(data){
                    data.paymenttype = type;
                    $modalInstance.close(data);
                };
                $scope.submitLater = function(data){
                    data.paymenttype = type;
                    $modalInstance.close(data);
                };
            },
            resolve: {
                type: function(){
                    return type;
                },
                user: function(){
                    return user
                },
                billinginfo: function(){
                    return $scope.user.billinginfo
                }
            }
        }).result.then(function(data){
            $scope.user.billinginfo = data;
        });
    }

    $scope.submit = function(user, firststep1, radioModel){
        $scope.processing = true;
        console.log(user);
        user['amount'] = $scope.sum;
        user['paymenttype']=radioModel;
        Scheduling.add(user, function(data) {
            console.log(data);
            if(data.hasOwnProperty('200')){
                $scope.invoice = data.invoiceno;
                Notification.success({message: '<icon class="fa fa-check"></icon> The schedule has been reserved succesfully.', positionY: 'bottom', positionX: 'right'});
                uiCalendarConfig.calendars['calendar1'].fullCalendar( 'refetchEvents' );
                getCurrentCount();
                resetForm();
                firststep1.$setPristine();
                $scope.openinvoice(data.invoiceno);
            }else{
                console.log(data);
                $scope.processing = false;
                Notification.error({message: '<icon class="fa fa-check"></icon> ' + data.error, positionY: 'bottom', positionX: 'right'});
            }
        });
    }

    $scope.refresh = function(){
        console.log('================== HITTING REFRESH');
        uiCalendarConfig.calendars['calendar1'].fullCalendar( 'refetchEvents' )
    }

    $scope.openinvoice = function(invoiceno){
        window.open(Config.BaseURL + '/sedonaadmin/index/invoice/' + invoiceno ,'liveMatches','directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=720,height=800');

    }
    $scope.printinvoice = function(invoiceno){
        var WinPrint = window.open(Config.BaseURL + '/sedonaadmin/index/invoice/' + invoiceno ,'liveMatches','directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=720,height=800');
        $timeout(function(){
            WinPrint.print();
        },1000);
    }

    $scope.editsched = function(event){
        $scope.table = false;
        $scope.editingsched = true;
        console.log(event);
        $scope.editingval = event;
        $scope.currentprice = event.hourday.substring(0,event.hourday.length-1);
        $scope.currenttimevalue = event.hourday.substring(event.hourday.length-1, event.hourday.length);
        computetime();
    }

    $scope.updateschedule = function(sched){
        $scope.processing = true;

        $scope.currentprice = $scope.editingval.hourday.substring(0,$scope.editingval.hourday.length-1);
        $scope.currenttimevalue = $scope.editingval.hourday.substring($scope.editingval.hourday.length-1, $scope.editingval.hourday.length);
        computetime();


        var reservation = moment(sched.reservation).format('YYYY-MM-DD');
        //console.log(reservationdate);
        var cstime = moment(sched.mytime).format( reservation + ' HH:mm:00');
        var cetime = moment(sched.endtime).format(reservation + ' HH:mm:00');
        var newval = {schedid: $scope.editingval.schedid ,cstime: cstime, cetime: cetime, reservation: reservation}

        Scheduling.updatesched(newval, function(data){
            if(data.hasOwnProperty('error')){
                Notification.error({message: '<icon class="fa fa-times-circle"></icon> ' + data['error'], positionY: 'bottom', positionX: 'right'});
                $scope.processing = false;
            }else{
                $scope.updateerror = '';
                $scope.processing = false;
                Notification.success({message: '<icon class="fa fa-check"></icon> The schedule has been updated succesfully.', positionY: 'bottom', positionX: 'right'});
                uiCalendarConfig.calendars['calendar1'].fullCalendar( 'refetchEvents' )
                getCurrentCount();

            }
        });

    }

    $scope.changeStat = function(schedid, status, message){
        $scope.processing = true;
        Scheduling.void(schedid, status, message, function(data){
            $scope.processing = false;
            $scope.editingval.updated_at = data.updated_at;
            Notification.success({message: '<icon class="fa fa-check"></icon> Status has been changed to ' +status +'.', positionY: 'bottom', positionX: 'right'});
            uiCalendarConfig.calendars['calendar1'].fullCalendar( 'refetchEvents' )
            getCurrentCount();
        });
    }



    $scope.closeedit = function(){
        $scope.editingsched = false;
    }

    /* Preloading Time*/
    $scope.sched.mytime = new Date();
    $scope.sched.mytime.setHours('8');
    $scope.sched.mytime.setMinutes('00');

    // console.log($scope.sched.mytime);

    $scope.hstep = 1;
    $scope.mstep = 1;

    $scope.ismeridian = true;

    /* event source that pulls from google.com */
    $scope.eventSource = {
        url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic",
        className: 'gcal-event',           // an option!
        currentTimezone: 'America/Chicago' // an option!
    };

    /* event source that contains custom events on the scope */

    var getEventsPerMonth = function(callback){
        var m = uiCalendarConfig.calendars['calendar1'].fullCalendar('getDate');
        var month = m.format('M');
        var year = m.format('YYYY');
        var events = [];
        Scheduling.getmonthevents(month,year, function(data){
            // console.log("Get Month Events");
            // console.log(data);
            for(var key in data){
                var n = {
                    title: data[key]['title'],
                    start: moment(data[key]['startdatetime']),
                    end: moment(data[key]['enddatetime']),
                    txtdate: moment(data[key]['scheduledate']).format('dddd, MMMM Do YYYY'),
                    txtstime: moment(data[key]['startdatetime']).format('h:mm:ss a'),
                    txtetime: moment(data[key]['enddatetime']).format('h:mm:ss a'),
                    txtname: data[key]['firstname'] + " " + data[key]['lastname'],
                    hourday: data[key]['hourday'],
                    status: data[key]['status'],
                    invoiceno: data[key]['invoiceno'],
                    schedid: data[key]['schedid'],
                    info: data[key]['description'],
                    phoneno: data[key]['phoneno'],
                    email: data[key]['email'],
                    updated_at: moment(data[key]['updated_at']).format('YYYY-MM-DD'),
                    created_at: moment(data[key]['created_at']).format('YYYY-MM-DD'),
                    unattended: false
                }

                var ndata = getStatus(data[key]['status'], data[key]['startdatetime'], data[key]['enddatetime']);
                // console.log('getting classes for background');
                // console.log(ndata);
                n['className'] = ndata.classname;
                n['unattended'] = ndata.unattended;

                if(!n['unattended'] && data[key]['status'] == 'RESERVED'){
                    n['backgroundColor'] = data[key]['colorlegend'];
                    n['textColor'] = '#fff';
                }

                events.push(n)
            }
            callback(events);
        });
    }

    // console.log("-=========================================================================");
    // console.log($scope.events);

    /* alert on eventClick */
    $scope.alertOnEventClick = function( event, jsEvent, view ){
        // console.log(event);
        $scope.gotodate(event.start, true, event)
    };
    /* alert on Drop */
    $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){
        $scope.alertMessage = ('Event Droped to make dayDelta ' + delta);
    };
    /* alert on Resize */
    $scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view){
        $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
    };

    $scope.overlay = $('.fc-overlay');

    $scope.alertOnMouseOver = function( event, jsEvent, view ){
        $scope.event = event;
        $scope.overlay.removeClass('left right').find('.arrow').removeClass('left right top pull-up');
        var wrap = $(jsEvent.target).closest('.fc-event');
        var cal = wrap.closest('.calendar');
        var left = wrap.offset().left - cal.offset().left;
        var right = cal.width() - (wrap.offset().left - cal.offset().left + wrap.width());
        if( right > $scope.overlay.width() ) {
            $scope.overlay.addClass('left').find('.arrow').addClass('left pull-up')
        }else if ( left > $scope.overlay.width() ) {
            $scope.overlay.addClass('right').find('.arrow').addClass('right pull-up');
        }else{
            $scope.overlay.find('.arrow').addClass('top');
        }
        $scope.overlay.css('z-index', 10000);
         wrap.append( $scope.overlay );
    }

    /* config object */
    $scope.uiConfig = {
        calendar:{
            height: 450,
            editable: false,
            header:{
                left: 'prev',
                center: 'title',
                right: 'next'
            },
            eventClick: $scope.alertOnEventClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            eventMouseover: $scope.alertOnMouseOver,
            events: function(start, end, timezone, callback) {
                console.log("HITTING EVENTS!!!");
                getEventsPerMonth(function(data){
                    callback(data);
                });
            }
        }
    };

    /* add custom event*/
    $scope.addEvent = function() {
        //$scope.events.push({
        //    title: 'New Event',
        //    start: new Date(y, m, d),
        //    className: ['b-l b-2x b-info']
        //});
        $scope.loading=false;

    };

    /* remove event */
    $scope.remove = function(index) {
        $scope.events.splice(index,1);
    };

    /* Change View */
    $scope.changeView = function(view, calendar) {
        uiCalendarConfig.calendars['calendar1'].fullCalendar('changeView',view);
    };

    $scope.today = function(calendar) {
        uiCalendarConfig.calendars['calendar1'].fullCalendar('today');
    };

    $scope.renderCalender = function(calendar) {
        if(calendar){
            calendar.fullCalendar('render');
        }
    };
    /* event sources array*/
    $scope.eventSources = [];

    $scope.changed = function () {
        //console.log('Time changed to: ' + $scope.mytime);
    };

    $scope.clear = function() {
        $scope.mytime = null;
    };

    /* Void Form*/
    $scope.voidForm = function(id, status){
        var modalInstance = $modal.open({
            templateUrl: 'voidForm.html',
            controller: function($scope, $modalInstance, $state, id,status) {
                $scope.cancel = function(){
                    $modalInstance.dismiss('cancel');
                }
                $scope.ok = function(data){
                    $modalInstance.close(data);
                };
            },
            resolve: {
                id: function(){
                    return id;
                },
                status: function(){
                    return status
                }
            }
        }).result.then(function(data){
                $scope.changeStat(id, status, data.message);
            });
    }
});
