/**
 * Created by Apple on 9/15/15.
 */
app.controller('SalesChartCTRL', function($scope, $state ,$q, $http, Config,$filter,SalesChart,highchartsNG) {

 SalesChart.reservestats(function (data){
	 for(var key in data){
	 				var total= data[key]['count2']
	 				var res= data[key]['count']
	 				var percent = Math.floor((res / total) * 100)

	                var n = {
	              		name: data[key]['pt'],
	                  	y:percent
	                }
	                data[key] = n;
	              
	            }

	     
	        var data1 = data;
	       

	       


 $scope.chartConfig = {
        options: {
            chart: {
            	plotBackgroundColor: null,
            	plotBorderWidth: null,
            	plotShadow: false,
            	type: 'pie'

            },
            tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        	},
            plotOptions: {
                pie: {
                	allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    },
                    point: {
                events: {
                    legendItemClick: function () {
                        return false; // <== returning false will cancel the default action
                    }
                }
            },
                    showInLegend: true
                }
            }
        },
        title: {
                text: 'Reservation Statistics of Sub-Services'
            },

        series: [{
                name: "Services",
                colorByPoint: true,

                data:data1

             }]

 
    }


});



 SalesChart.reserve_statservice(function (data){
	 for(var key in data){
	 				var total= data[key]['count2']
	 				var res= data[key]['count']
	 				var percent = Math.floor((res / total) * 100)
	                var n = {
	              		name: data[key]['pc_title'],
	                  	y:percent
	                }
	                data[key] = n;
	              
	            }

	     
	        var data1 = data;
	     

	       


 $scope.chartService = {
        options: {
            chart: {
            	plotBackgroundColor: null,
            	plotBorderWidth: null,
            	plotShadow: false,
            	type: 'pie'

            },
            tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        	},
            plotOptions: {
                pie: {
                	allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    },
                    point: {
                events: {
                    legendItemClick: function () {
                        return false; // <== returning false will cancel the default action
                    }
                }
            },
                    showInLegend: true
                }
            }
        },
        title: {
                text: 'Reservation Statistics of Services'
            },

        series: [{
                name: "Services",
                colorByPoint: true,
                data:data1
             }]

 
    }


});


SalesChart.Sales_Reservation(function (data){

for(var key in data.total){
  var n ={
    tot: data.total[key]['total']
  }
   data.total[key] = n;
}
for(var key in data.overall){
  var n ={
    tot: data.overall[key]['total']
  }
   data.overall[key] = n;
}
      $scope.totalsales = data.total[0].tot;
      $scope.expectedsales = data.overall[0].tot;


var months = data.months;
months.reverse();


 for(var key in data.data){
	 				var total= data.data[key]['tot']
	                var n = [
	                  	total
	                ]
	                data.data[key] = n;
	              
	            }
	            var data2 = data.data;
	               data2.reverse();

$scope.SalesReservation = {
     chart: {
            type: 'area',
            spacingBottom: 30
        },
     plotOptions: {
        series: {
            connectNulls: false
        }
    },
      title: {
            text: 'Monthly Sales',
            x: -20 //center
        },
        subtitle: {
            text: 'Sedona Healing Arts',
            x: -20
        },
        xAxis: {
            categories: data.months
        },
        yAxis: {
            title: {
                text: 'Total Sales in Dollars'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: '$'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
         series: [{
         	tooltip: {
                valuePrefix: '$'
            },
                name: "Sales",
            	data: data2
             	}]
           
        

}
});



SalesChart.MonthlyReservation(function (data){

$scope.totalReservation = data.totalreservation;
var months = data.months;
months.reverse();

 for(var key in data.data){
	 				var total= data.data[key]['count']

	                var n = [
	                  	total
	                ]
	                data.data[key] = n;

	            }
	            var data2 = data.data;
	            data2.reverse();
          

$scope.MonthlyReservation = {
     chart: {
            type: 'area',
            spacingBottom: 30
        },
     plotOptions: {
        series: {
            connectNulls: false
        }
    },
      title: {
            text: 'Monthly Reservation',
            x: -20 //center
        },
        subtitle: {
            text: 'Sedona Healing Arts',
            x: -20
        },
         xAxis: {
            categories: months
        },
        yAxis: {
            title: {
                text: 'Total Reservation in Numbers'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: '$'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
         series: [
                {
               	name: "TOTAL NUMBER OF RESERVATION",
                data: data2
                }]
           
        

}

});


SalesChart.TopService(function (data){

for(var key in data.data){
                    var title= data.data[key]['pc_title']
                    var n = [
                        title
                    ]
                    
                    data.data[key] = n;
                }
                var data1 = data.data;


 for(var lock in data.extradata){
                
                    var count= data.extradata[lock]['count']
                    var m = [
                        count
                    ]
                    
                    data.extradata[lock] = m;
                }
                var data2 = data.extradata; 


$scope.TopService = {
         options: {
            chart: {
            
                type: 'column'

            }
        },
        title: {
            text: 'Top Services'
        },
        subtitle: {
            text: 'Sedona Healing Arts'
        },
        xAxis: {
            categories: data1,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'TOTAL NUMBER',
            data:data2,
            colorByPoint: true,
        }]

      


}

});



SalesChart.TopSubservice(function (data){

for(var key in data.data){
                    var title= data.data[key]['pt']
                    var n = [
                        title
                    ]
                    
                    data.data[key] = n;
                }
                var data1 = data.data;


 for(var lock in data.extradata){
                
                    var count= data.extradata[lock]['count']
                    var m = [
                        count
                    ]
                    
                    data.extradata[lock] = m;
                }
                var data2 = data.extradata;



$scope.TopSubservice = {
         options: {
            chart: {
            
                type: 'column'

            }
        },
        title: {
            text: 'Top Sub-Services'
        },
        subtitle: {
            text: 'Sedona Healing Arts'
        },
        xAxis: {
            categories: data1,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'TOTAL NUMBER',
            data:data2,
            colorByPoint: true,

        }] 


}

});


SalesChart.MonthlySchedule(function (data){
   
    var months = data.months;
    months.reverse();

    var reserve = data.reserve;
    reserve.reverse();

    var pending = data.pending;
    pending.reverse();

    var voiddata = data.voiddata;
    voiddata.reverse();

    var done = data.done;
    done.reverse();

    var unattended = data.unattended;
    unattended.reverse();


$scope.MonthlySchedule = {

   options: {
            chart: {
            
                type: 'column'

            }
        },
        title: {
            text: 'Monthly Schedule'
        },
        subtitle: {
            text: 'Sedona Healing Arts'
        },
        xAxis: {
            categories: months,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total Number'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'RESERVED',
            color: '#7266ba',
            data: reserve

        }, {
            name: 'PENDING',
            color: '#23b7e5',
            data: pending

        }, {
            name: 'VOID',
            color: '#f05050',
            data: voiddata

        }, {
            name: 'UNATTENDED',
            color: '#fad733',
            data: unattended

        },{
            name: 'DONE',
            color: '#27c24c',
            data: done

        }]


}


});



});