'use strict';

app.controller('pagefooterCtrl', function ($timeout,$http, $scope, $stateParams, $modal,$sce, Config, Special, $filter, $anchorScroll) {
    $scope.process = 0;
    $scope.amazonlink = Config.amazonlink;
    $scope.alerts = [];
    $scope.s3link = Config.amazonlink;
    $scope.directory = 'pageimage';
      $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
      };
    $scope.page = {};
    $scope.page = [];
    $scope.row = [];
    $scope.trustAsHtml = $sce.trustAsHtml;
    var info = function(){
    Special.infofooter(function(data) {
      console.log(data.datacol)
      for(var key in data.datacol){
            var length=data.datacol[key].length;
               $scope.row.push({row:[],colcount:length,column:length,rowcolor:data.datarow[key].rowcolor,rowfcolor:data.datarow[key].rowfontcolor});

                for(var k in data.datacol[key]){
                  // if (data.datacol[key][k].address!=undefined) {
                  //   var str = data.datacol[key][k].address;
                  //   str = str.split('\n');
                  //   }
            switch(data.datacol[key][k].module){
            case 'TEXT':
                  $scope.row[key].row.push({col:data.datacol[key][k].module,colcontent:data.datacol[key][k].content});
                break;
            case 'IMAGE':
                $scope.row[key].row.push({col:data.datacol[key][k].module,colimg:data.datacol[key][k].image,colimglink:
                data.datacol[key][k].link});
                break;
            case 'TESTIMONIAL':
                $scope.row[key].row.push({col:data.datacol[key][k].module,coltestimonial:data.datacol[key][k].limitcount,
                coltestimonialcat:data.datacol[key][k].category});
                break;
            case 'CONTACT':
                $scope.row[key].row.push({col:data.datacol[key][k].module,coltitle:data.datacol[key][k].title,colphone:data.datacol[key][k].phone,
                    colemail:data.datacol[key][k].email,colhours:data.datacol[key][k].hours,coladdress:data.datacol[key][k].address,str:str});
                break;
                break;
            case 'NEWS':
                $scope.row[key].row.push({col:data.datacol[key][k].module,colnewslimit:data.datacol[key][k].limitcount}); 
                break;
            case 'DIVIDER':
              $scope.row[key].row.push({col:data.datacol[key][k].module,colheight:data.datacol[key][k].height,colcolor:data.datacol[key][k].color}); 
            break;
            case 'BUTTON':
              $scope.row[key].row.push({col:data.datacol[key][k].module,colbtnname:data.datacol[key][k].btnname,colbtnlink:data.datacol[key][k].btnlink
                ,colbgcolor:data.datacol[key][k].bgcolor,colfcolor:data.datacol[key][k].fcolor,colpadtop:data.datacol[key][k].padtop,colpadside:data.datacol[key][k].padside
                ,colfont:data.datacol[key][k].font,colposition:data.datacol[key][k].position}); 
            break;
            default:
                  $scope.row[key].row.push({col:data.datacol[key][k].module}); 
            break;
         }
                   
                }

    }
    // console.log($scope.row)

    });


    }


    info();

//load news
Special.loadnews(function(data) {
$scope.news = data;
 for(var key in data){
                var categorylist = [];
                data[key].category.map(function(val){
                    categorylist.push(val.categoryname);
                });
                data[key]['categorylist'] = categorylist.join(", ");
            }


});
//
//load news
Special.loadtesti(function(data) {
$scope.testi = data.testi;
$scope.archive = data.archive;
$scope.shortcode = data.shortcode;
$scope.children = data.children;
});
//

    $scope.imageloader = false;
    $scope.imagecontent = true;
    $scope.showUpdate=false;

    $scope.savePage = function() {
        $scope.alerts = [];
        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };
      var page = {
          footer:$scope.row
        };
        Special.addfooter(page,function(data){
            $scope.alerts = [{type: 'success', msg: 'Footer successfully updated!'}];
        });

        $anchorScroll();
    }

    $scope.alertss = [];

    $scope.closeAlerts = function (index) {
            $scope.alertss.splice(index, 1);
    };

    /////START SPECIAL PAGE
  $scope.rowdelitem = function(index){
         var modalInstance = $modal.open({
            templateUrl: 'deleterow.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                $scope.ok = function(){
                    rowdel(index);
                     $modalInstance.dismiss('cancel');
                }
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
              };
          }
      })
  }

  var rowdel = function(index)
  {
       $scope.row.splice(index,1);
  }

    $scope.rownewitem = function($event){
        $event.preventDefault();
        $scope.row.push({row:[],colcount:length,column:length,rowcolor:"",rowfcolor:""});
     
    }

    var selectedcol = '';
    $scope.column = [];
    $scope.page.colcount = [];
    $scope.page.row= [];
    var column = function(index,column){
        if (column != undefined) {
             $scope.row[index].column=column;
        }else{
             $scope.row[index].column=selectedcol;
        }
        $scope.row[index].colcount=selectedcol;

    }


 $scope.delcolumn = function(countcol,index,col){
    $scope.row[index].colcount = countcol - 1;
    $scope.row[index].column = countcol - 1;
    $scope.row[index].row.splice(col,1);
}
    //MODULE 
    var selectedmod = '';
    
    // $scope.rowindex = [];
    $scope.insertmodule = function(col,index){
        var modalInstance = $modal.open({
            templateUrl: 'module.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                if (col != undefined) {
                    col = parseInt(col+1);
                }
                else{
                       col = 0+1;
                }
          
                $scope.setmod = function(mod){
                    if(mod=="TEXT"){
                        textmodule(col,index);
                        $modalInstance.dismiss('cancel');
                    }
                    else if(mod=="IMAGE"){
                        media('featured','banner',index,col);
                        $modalInstance.dismiss('cancel');
                    }
                    else if(mod=="TESTIMONIAL"){
                        testimonialmodule(col,index);
                        $modalInstance.dismiss('cancel');
                    }
                    else if(mod=="CONTACT"){
                        contactmodule(col,index);
                        $modalInstance.dismiss('cancel');
                    }
                    else if(mod=="NEWS"){
                        newsmodule(col,index);
                        $modalInstance.dismiss('cancel');
                    }
                    else if(mod=="DIVIDER"){
                        dividermodule(col,index);
                        $modalInstance.dismiss('cancel');
                    }
                    else if(mod=="BUTTON"){
                        buttonmodule(col,index);
                        $modalInstance.dismiss('cancel');
                    }
                }
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
              };
          }
      })
}


    var rowcolor = '',rowfcolor='';
    var selectedcolor = function(index){
      $scope.row[index].rowcolor = rowcolor;
      $scope.row[index].rowfcolor = rowfcolor;
    }

    $scope.rowcolor = function(index,color,fcolor){
        var modalInstance = $modal.open({
            templateUrl: 'rowcolor.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                if(color==undefined || color==""){
                    $scope.bcolor = '#ffffff';
                }
                else{
                    $scope.bcolor = color;
                }
                if(fcolor==undefined || fcolor==""){
                    $scope.fcolor = '#ffffff';
                }
                else{
                    $scope.fcolor = fcolor;
                }
                $scope.setoption = function(color,fcolor){
                    rowcolor = color;
                    rowfcolor = fcolor;
                    selectedcolor(index);
                    $modalInstance.dismiss('cancel');
                }
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
                };
            }
        })
    }


 ////NEWS MODULE 
    var newslimit4 = '',
        allnewslimit = '';


    var newsmodule = function(col,index,view,popular,limit,childindex){
          console.log(col);
        var modalInstance = $modal.open({
            templateUrl: 'modulenews.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                if(view==undefined){
                    $scope.viewed = 'false';
                }
                else if(view!=undefined){
                    $scope.viewed = view;
                }
                else if(popular==undefined){
                    $scope.popular = 'false';
                }
                else if(popular!=undefined){
                    $scope.popular = popular;
                }
                $scope.limit = limit;
                $scope.setoption = function(view,popular,limit){
                   
                    selectedmod = 'NEWS';
                    allnewslimit = limit;
                    if (childindex != undefined) {
                       editrow(index,childindex,'NEWS');
                   }
                   else{
                       allrow(col,index,'NEWS');
                   }
                   selectedcol = col;
                   column(index);
                   $modalInstance.dismiss('cancel');
               }
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
              };
          }
      })
}

$scope.editnewsmodule = function(col,index,view,popular,limit,childindex){
   newsmodule(col,index,view,popular,limit,childindex);
}


    ////Divider MODULE 
    var allcolor = '',
        allheight = '';

    var dividermodule = function(col,index,height,color,childindex){
        var modalInstance = $modal.open({
            templateUrl: 'moduledivider.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                $scope.height = height;
                if(color==undefined){
                    $scope.color = '#ffffff';
                }
                else if(color!=undefined){
                    $scope.color = color;
                }
                $scope.setoption = function(height,color){
                    
                    selectedmod = 'DIVIDER';
                    allcolor = color;
                    allheight = height;
                    // dividercontent(col,index,column);
                    // module(col,index);
                    selectedcol = col;
                   
                      if (childindex != undefined) {
                           editrow(index,childindex,'DIVIDER');
                       }
                       else{
                          column(index);
                           allrow(col,index,'DIVIDER');
                       }
                    $modalInstance.dismiss('cancel');
                }
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
              };
          }
      })
}

$scope.editdividermodule = function(col,index,height,color,childindex){
   dividermodule(col,index,height,color,childindex);
}

///Button MODULE 
    var allbtnname = '',
        allbgcolor = '',
        allfcolor = '',
        allpadtop = '',
        allpadside = '',
        allfont = '',
        allposition = '',
        allbtnlink = '';

    var buttonmodule = function(col,index,counter,childindex){
        var modalInstance = $modal.open({
            templateUrl: 'modulebutton.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                $scope.button = [];
                if(counter==undefined){
                    $scope.button.bgcolor = '#5cce5f';
                    $scope.button.fcolor = '#ffffff';
                    $scope.button.padtop = 5;
                    $scope.button.padside = 25;
                    $scope.button.position = 'Center';
                    $scope.button.font = 16;
                }else{
                     $scope.button.btnname = counter.colbtnname;
                    $scope.button.btnlink = counter.colbtnlink;
                    $scope.button.bgcolor = counter.colbgcolor;
                    $scope.button.fcolor = counter.colfcolor;
                    $scope.button.padtop = counter.colpadtop;
                    $scope.button.padside = counter.colpadside;
                    $scope.button.font = counter.colfont;
                    $scope.button.position = counter.colposition;
                }
                $scope.setoption = function(button){
                    selectedmod = 'BUTTON';
                    allbtnname = button.btnname;
                    allbtnlink = button.btnlink;
                    allbgcolor = button.bgcolor;
                    allfcolor = button.fcolor;
                    allpadside = button.padside;
                    allpadtop = button.padtop;
                    allfont = button.font;
                    allposition = button.position;
                    selectedcol = col;
                      if (childindex != undefined) {
                           editrow(index,childindex,'BUTTON');
                       }
                       else{
                          column(index);
                          allrow(col,index,'BUTTON');
                       }
                    $modalInstance.dismiss('cancel');
                }
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
              };
          }
      })
}

$scope.editbuttonmodule = function(col,index,counter,childindex){
   buttonmodule(col,index,counter,childindex);
}

    ////TEXT MODULE 
    var alltext = '';


   var allrow = function (col,index,columnname) {
            if (columnname=='TEXT') {
                 $scope.row[index].row.push({col:selectedmod,colcontent:alltext});
            }
            else if (columnname=='IMAGE'){
                $scope.row[index].row.push({col:selectedmod,colimg:allimg,colimglink:allimglink});
            }
             else if (columnname=='TESTIMONIAL'){
                $scope.row[index].row.push({col:selectedmod,coltestimonial:testi,
                    coltestimonialcat:testicat});
            }
            else if (columnname=='CONTACT'){
                 $scope.row[index].row.push({col:selectedmod,coltitle:contacttitle,colphone:contactphone,
                    colemail:contactemail,colhours:contacthours,coladdress:contactaddress,str:str});
            }
             else if (columnname=='NEWS'){
                $scope.row[index].row.push({col:selectedmod,colnewslimit:allnewslimit});
            }
             else if (columnname=='DIVIDER'){
                $scope.row[index].row.push({col:selectedmod,colheight:allheight,colcolor:allcolor});
            }
            else if (columnname=='BUTTON'){
                $scope.row[index].row.push({col:selectedmod,colbtnname:allbtnname,colbtnlink:allbtnlink,
                    colbgcolor:allbgcolor,colfcolor:allfcolor,colpadtop:allpadtop,colpadside:allpadside
                    ,colfont:allfont,colposition:allposition});
            }
            // console.log($scope.row)
     
    }

     var editrow = function (index,childindex,columnname) {
            if (columnname=='TEXT') {
                 $scope.row[index].row[childindex].col = selectedmod;
                 $scope.row[index].row[childindex].colcontent = alltext;
            }
            else if (columnname=='IMAGE'){
                 $scope.row[index].row[childindex].col = selectedmod;
                $scope.row[index].row[childindex].colimg = allimg;
                $scope.row[index].row[childindex].colimglink = allimglink;
            }
             else if (columnname=='TESTIMONIAL'){
                 $scope.row[index].row[childindex].col = selectedmod;
                $scope.row[index].row[childindex].coltestimonial = testi;
                $scope.row[index].row[childindex].coltestimonialcat = testicat;
            }
            else if (columnname=='CONTACT'){
               $scope.row[index].row[childindex].col = selectedmod;
               $scope.row[index].row[childindex].coltitle = contacttitle;
               $scope.row[index].row[childindex].colphone = contactphone;
               $scope.row[index].row[childindex].colemail = contactemail;
               $scope.row[index].row[childindex].colhours = contacthours;
               $scope.row[index].row[childindex].coladdress = contactaddress;
               $scope.row[index].row[childindex].str = str;
            }
             else if (columnname=='NEWS'){
                 $scope.row[index].row[childindex].col = selectedmod;
                $scope.row[index].row[childindex].colnewslimit = allnewslimit;
            }
            else if (columnname=='DIVIDER'){
             $scope.row[index].row[childindex].col = selectedmod;
             $scope.row[index].row[childindex].colheight = allheight;
             $scope.row[index].row[childindex].colcolor = allcolor;
           }
           else if (columnname=='BUTTON'){
               $scope.row[index].row[childindex].col = selectedmod;
               $scope.row[index].row[childindex].colbtnname = allbtnname;
               $scope.row[index].row[childindex].colbtnlink = allbtnlink;
               $scope.row[index].row[childindex].colbgcolor = allbgcolor;
               $scope.row[index].row[childindex].colfcolor = allfcolor;
               $scope.row[index].row[childindex].colpadtop = allpadtop;
               $scope.row[index].row[childindex].colpadside = allpadside;
               $scope.row[index].row[childindex].colfont = allfont;
               $scope.row[index].row[childindex].colposition = allposition;
            }
        
        // console.log($scope.row1);
    }

    var textmodule = function(col,index,childindex){
 
        var modalInstance = $modal.open({
            templateUrl: 'moduletext.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                $scope.gallery = function(){
                    media('content');
                }
                $scope.settext = function(text){
                    selectedmod = 'TEXT';
                    alltext = text;
                    selectedcol = col;
                    column(index);
                    if (childindex != undefined) {
                       editrow(index,childindex,'TEXT');
                   }
                   else{
                       allrow(col,index,'TEXT');
                   }
                    // textcontent(col,index);
                    // module(col,index);
                    column(index);
                    $modalInstance.dismiss('cancel');
                }
                $scope.cancel = function() {

                  $modalInstance.dismiss('cancel');
              };
          }
      })
    }

    $scope.edittextmodule = function(col,index,content,childindex){
        var modalInstance = $modal.open({
            templateUrl: 'moduletext.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                $scope.textarea = content;
                $scope.gallery = function(){
                    media('content');
                }
                $scope.settext = function(text){
                    selectedmod = 'TEXT';
                    alltext = text;
                    editrow(index,childindex,'TEXT');
                    $modalInstance.dismiss('cancel');
                }
                $scope.cancel = function(){
                  $modalInstance.dismiss('cancel');
              };
          }
      })
    }


    //Image Module

  var allimg = '', 
        allimglink = '';


    var imagemodule = function(col,index,imageset,childindex,linkset){
        var modalInstance = $modal.open({
            templateUrl: 'moduleimage.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                $scope.s3link = Config.amazonlink;
                $scope.gallery = function(){
                    media('featured','banner',index,col,childindex);
                    $modalInstance.dismiss('cancel');
                }
                $scope.setimg = function(image,link){
                    
                    selectedmod = 'IMAGE';
                    allimg = image;
                    allimglink = link;
                    if (childindex != undefined) {
                     editrow(index,childindex,'IMAGE');
                 }
                 else{
                     allrow(col,index,'IMAGE');
                 }
                 selectedcol = col;
                 column(index);
                 $modalInstance.dismiss('cancel');
                }
                $scope.colimg = imageset;
                $scope.link = linkset;
                $scope.cancel = function() {

                  $modalInstance.dismiss('cancel');
              };
          }
      })
}



$scope.editimagemodule = function(col,index,images,childindex,link){
    imagemodule(col,index,images,childindex,link);
}

 ////TESTIMONIAL MODULE 
    var testi = '',
        testicat = '';

    var testimonialmodule = function(col,index,childindex){
        var modalInstance = $modal.open({
            templateUrl: 'moduletestimonial.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                $scope.category ='';
                $scope.settestimonial = function(text,cat){
                    testi = text;
                    testicat = cat;
                    selectedmod = 'TESTIMONIAL';
                    // testimonialcontent(col,index);
                    // module(col,index);
                     selectedcol = col;
                    column(index);
                    if (childindex != undefined) {
                           editrow(index,childindex,'TESTIMONIAL');
                       }
                       else{
                           allrow(col,index,'TESTIMONIAL');
                       }
                      
                    $modalInstance.dismiss('cancel');
                }
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
              };

              Special.get(function(pages){
                $scope.pages = pages;
            });
          }
      })
}


$scope.edittestimonialmodule = function(col,index,content,cat,childindex){
        var modalInstance = $modal.open({
            templateUrl: 'moduletestimonial.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
               $scope.text = content;
                Special.get(function(pages){
                   $scope.pages = pages;
                });
                $scope.category = cat;
                $scope.settestimonial = function(text,cat){
                    selectedmod = 'TESTIMONIAL';
                    testi = text;
                    testicat = cat;
                    editrow(index,childindex,'TESTIMONIAL');
                    $modalInstance.dismiss('cancel');
                }
                $scope.cancel = function() {
                 
                  $modalInstance.dismiss('cancel');
              };
  }
})
}


    ////TESTIMONIAL MODULE 

////Contact MODULE 
    var contacttitle = '',
        contactemail = '',
        contacthours = '',
        contactaddress = '',
        str = '',
        contactphone = '';


    var contactmodule = function(col,index,counter,childindex){
        var modalInstance = $modal.open({
            templateUrl: 'modulecontact.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                if (counter==undefined) {
                    Special.getinfo(function(data) {
                      $scope.contact = data[0];
                    });
                }else{
                    $scope.contact = [];
                    $scope.contact.title = counter.coltitle;
                    $scope.contact.phone = counter.colphone;
                    $scope.contact.email = counter.colemail;
                    $scope.contact.hours = counter.colhours;
                    $scope.contact.address = counter.coladdress;
                }
                 
                $scope.ok = function(contact){
                    selectedmod = 'CONTACT';
                    contacttitle = contact.title;
                    contactemail = contact.email;
                    contacthours = contact.hours;
                    contactaddress = contact.address;
                    contactphone = contact.phone;
                    str = contact.address;
                    str = str.split('\n');
                    if (childindex != undefined) {
                       editrow(index,childindex,'CONTACT');
                   }
                   else{
                       allrow(col,index,'CONTACT');
                   }
                   selectedcol = col;
                   column(index);
                   $modalInstance.dismiss('cancel');
               }
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
              };
          }
      })
}

$scope.editcontactmodule = function(col,index,counter,childindex){
   contactmodule(col,index,counter,childindex);
}




/////END SPECIAL PGE



var media = function(type, thumb,index,col,childindex){
    var modalInstance = $modal.open({
        templateUrl: 'mediagallery.html',
        controller: function($scope, $modalInstance,Config, type) {
            $scope.imageloader=false;
            $scope.directory = 'pageimage';
            $scope.videoenabled=false;
            $scope.mediaGallery = 'images';
            $scope.imagecontent=true;
            $scope.invalidvideo = false;
            $scope.currentSelected = '';
            $scope.currentDeleting = '';
            $scope.type = type;
            $scope.s3link = Config.amazonlink;
            // console.log($scope.s3link);

            $scope.set = function(id){
                $scope.currentSelected = id;
                $scope.contentvideo = id;
                if(type=='content'){
                    $scope.copy();
                }
            }

            $scope.returnImageThumb = function(){
                return function (item) {
                    if(item){
                        return Config.amazonlink + "/uploads/" + $scope.directory + "/" + item;
                    }else{
                        return item;
                    }
                };
            }

            var loadimages = function() {
                Special.loadimages(function(data){
                    // console.log(data);
                    $scope.imagelist = data;
                    $scope.imagelength = data.length;
                });
            }

            loadimages();

            $scope.upload = function (files)
            {
                var filename
                var filecount = 0;
                if (files && files.length)
                {
                    $scope.imageloader=true;
                    $scope.imagecontent=false;

                    for (var i = 0; i < files.length; i++)
                    {
                        var file = files[i];

                        if (file.size >= 2000000)
                        {
                            $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
                            filecount = filecount + 1;

                            if(filecount == files.length)
                            {
                                $scope.imageloader=false;
                                $scope.imagecontent=true;
                            }


                        }
                        else

                        {
                            var promises;

                            promises = Upload.upload({

                                    url: Config.amazonlink, //S3 upload url including bucket name
                                    method: 'POST',
                                    transformRequest: function (data, headersGetter) {
                                        //Headers change here
                                        var headers = headersGetter();
                                        delete headers['Authorization'];
                                        return data;
                                    },
                                    fields : {
                                        key: 'uploads/'+$scope.directory+'/' + file.name, // the key to store the file on S3, could be file name or customized
                                        AWSAccessKeyId: Config.AWSAccessKeyId,
                                        acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                                        policy: Config.policy, // base64-encoded json policy (see article below)
                                        signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                        "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                    },
                                    file: file
                                })
promises.then(function(data){

    filecount = filecount + 1;
    filename = data.config.file.name;
    var fileout = {
        'imgfilename' : filename
    };
    Special.saveimage(fileout, function(data){
        loadimages();
        if(filecount == files.length)
        {
            $scope.imageloader=false;
            $scope.imagecontent=true;
        }
    });

});


}


}
}

};

$scope.deletenewsimg = function (dataimg, $event)
{
    var fileout = {
        'imgfilename' : dataimg
    };
    Special.deleteimage(fileout, function(data) {
        loadimages();
    });
    $event.stopPropagation();
}

$scope.copy = function() {
    // console.log($scope.contentvideo);
    var text = '';
    if($scope.contentvideo.filename){
        text = Config.amazonlink + '/uploads/'+$scope.directory+'/' + $scope.contentvideo.filename;
    }
    var pp = window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
    if(pp != "" && pp !== null) {
        $modalInstance.dismiss('cancel');
    }
}

$scope.ok = function(category) {
    $modalInstance.close($scope.contentvideo);
}

$scope.cancel = function() {
    $modalInstance.dismiss('cancel');
}
},
resolve: {
    type: function(){
        return type;
    }
},
windowClass: 'xxx-modal-window'
}).result.then(function(res) {

    if(res.filename) {
        if(thumb == 'banner'){
            $scope.colimg = res.filename;
            imagemodule(col,index,res.filename,childindex);
        }
    }
});
}


}) //end



