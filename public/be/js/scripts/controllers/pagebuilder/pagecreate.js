'use strict';
/* Controllers */
app.controller('Pagecreate', function($scope, $state, Upload ,$q, $http, Config, $log,$filter, $interval, $modal, Special, $anchorScroll,$sce){
    $scope.imageloader = false;
    $scope.imagecontent = true;
    $scope.showUpdate=false;
    $scope.editslug = false;
    $scope.editedslug = false;
    var slugstorage = "";
    $scope.trustAsHtml = $sce.trustAsHtml;
    var oripage = angular.copy($scope.page);
    $scope.s3link = Config.amazonlink;
    $scope.directory = 'pageimage';
    $scope.eventSources = [];
    $scope.closeAlert = function(index) {
     $scope.alerts.splice(index, 1);
    };
    //Convert to Slug
    $scope.validslugs = false;
    $scope.onpagetitle = function convertToSlug(Text) {
        if(Text != null && $scope.editedslug == false && $scope.editslug == false){
         var text1 = Text.replace(/[^\w /]+/g,'');
         $scope.page.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
     }
 }
    $scope.onslugs = function(Text){ if(Text != null){
        var text1 = Text.replace(/[^\w /]+/g,'');
       $scope.page.slugs = angular.lowercase(text1.replace(/ +/g,'-')); }
   }
    $scope.uniqueslugs = function(slugs){
        Special.uniquepage(slugs,function(data){
             if(data==0){
                     $scope.validslugs = false;
                 }
                 else{
                    $scope.validslugs = true;
                }
        });
    }
    $scope.editpageslug = function(){
        $scope.editslug = true;
        slugstorage = $scope.page.slugs;
    }

    $scope.cancelpageslug = function(title) {
        if(title != null)
        {
            $scope.editedslug = false;
            $scope.page.slugs = slugstorage;
        }else {
            $scope.page.slugs = '';
            $scope.editedslug = false;
        }
        $scope.editslug = false;
    }

    $scope.setslug = function(slug){
        $scope.editedslug = true;
        $scope.editslug = false;
        if(slug != null)
        {
            $scope.page.slugs = slug.replace(/\s+/g, '-').toLowerCase();

        }
    }

    $scope.clearslug = function(title){
        if(title != null)
        {
            $scope.editedslug = false;
            $scope.page.slugs = title.replace(/\s+/g, '-').toLowerCase();
            var slugs = title.replace(/\s+/g, '-').toLowerCase();
            Special.uniquepage(slugs,function(data){
             if(data==0){
                     $scope.validslugs = false;
                 }
                 else{
                    $scope.validslugs = true;
                }
            });
        }else {
            $scope.page.slugs = '';
            $scope.editedslug = false;
        }
    }

    $scope.stylepreview = function(page) {
        console.log(page)
      var modalInstance = $modal.open({
          templateUrl: 'stylepreview',
          size:'lg',
          controller: function($scope, $modalInstance, $state) {
            $scope.page = page;
            $scope.banner = Config.amazonlink + "/uploads/pageimage/" + page.imagethumb;
            $scope.cancel = function () {
                $modalInstance.dismiss();
            };
          }
      });
    }

//load news
Special.loadnews(function(data) {
$scope.news = data;
 for(var key in data){
                var categorylist = [];
                data[key].category.map(function(val){
                    categorylist.push(val.categoryname);
                });
                data[key]['categorylist'] = categorylist.join(", ");
            }


});
//
//load news
Special.loadtesti(function(data) {
$scope.testi = data.testi;
$scope.archive = data.archive;
$scope.shortcode = data.shortcode;
$scope.children = data.children;
});
//


//select menu
Special.getmenu(function(data) {
for(var key in data){
                       var n={
                      
                            menuID:data[key]['menuID'],
                            name:data[key]['name']
                        }
                        data[key]=n;
                    }
                   $scope.menu = data;
});
//

$scope.saveme = function(page) {
    // console.log(page)
            $scope.alerts = [];
        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };
        $scope.isSaving = true;
        page['column'] = $scope.row;

        Special.add(page, function(data){
            $scope.isSaving = false;
            $scope.alerts = [{type: 'success', msg: 'Page successfully saved!'}];
            $scope.page = angular.copy(oripage);
            $scope.formpage.$setPristine();
            $scope.page = {};
            $scope.page.sidebarleft = false;
            $scope.page.sidebarright = false;
            $scope.sidebarleft = false;
            $scope.sidebarright = false;

            $scope.page.sidedataR = [];
            $scope.sidebar = {};
            $scope.page.sidedataL = [];
            $scope.sidebarR = {};
            $scope.page.banner = false;
            $scope.page.row= [];
            $scope.column = [];
            $scope.page.colcount = [];
            $scope.page.type = "Normal";
            $scope.page.status = true;
            $scope.checkedz = true;
            counter2=0;
            $scope.countrow = 0;
            $scope.rowcount = [ { count: counter2 } ];
        });
        $anchorScroll();
}
var checkside = function(page) {
    if (page['sidebarleft'] == true && page['sidebarright'] == true) {
        if ($scope.page.sidedataL=='') {
            $anchorScroll();
            $scope.alerts = [{type: 'danger', msg: 'Please add left sidebar'}];
        }else if ($scope.page.sidedataR=='') {
            $anchorScroll();
            $scope.alerts = [{type: 'danger', msg: 'Please add right sidebar'}];
        }else{
            $scope.saveme(page);
        }
    }
    else if (page['sidebarleft'] == true && page['sidebarright'] == false) {
        if ($scope.page.sidedataL=='') {
            $anchorScroll();
            $scope.alerts = [{type: 'danger', msg: 'Please add left sidebar'}];
        }else{
            $scope.saveme(page);
        }
    }
    else if (page['sidebarleft'] == false && page['sidebarright'] == true) {
        if ($scope.page.sidedataR=='') {
            $anchorScroll();
            $scope.alerts = [{type: 'danger', msg: 'Please add right sidebar'}];
        }else{
            $scope.saveme(page);
        }
    }
    else{
        $scope.saveme(page);
    }
}
    $scope.savePage = function(page) {
        $scope.chicken=false;
        // console.log($scope.chicken)
        if ($scope.validslugs==true) {
            $anchorScroll();
            $scope.alerts = [{type: 'danger', msg: 'Slugs Already Exist!'}];
        }else{
        switch(page['type']){
            case 'Normal':
             if (!page['body']) {
                $anchorScroll();
                $scope.alerts = [{type: 'danger', msg: 'Page Content is required'}];
            }else{
                checkside(page);
            }
            break;
            default:
            if ($scope.row=="") {
                $anchorScroll();
                $scope.alerts = [{type: 'danger', msg: 'Please fill out columns'}];
            }else{
                for (var i = $scope.row.length - 1; i >= 0; i--) {
                    if ($scope.row[i].row.length == 0) {
                        $scope.chicken = true;
                        console.log("chicken")
                    }
                };

                if ($scope.chicken==true) {
                    $anchorScroll();
                    $scope.alerts = [{type: 'danger', msg: 'Please fill out columns'}];
                }
                else{
                    console.log("check")
                    checkside(page);
                }
                
            }

            break;
        }
    }
}

    $scope.preview = function(page){
        page['column'] = $scope.row;
        console.log(page['column']);
         localStorage["page"] = JSON.stringify(page);
        window.open(Config.BaseURL + "/specialpage/preview");
    }

    ////////////////////SIDEBAR////////////////////////////////////////////////////////
    $scope.page = {};
    //status
     
    //left
    $scope.addbtn = true;
    $scope.sidebar = {};
    $scope.page.sidedataL = [];

    $scope.addleftsidebar = function(sidebar){
        $scope.page.sidedataL.push({sidebar:sidebar.left, menuID:sidebar.leftmenu, imglink:sidebar.leftimglink,
        img:sidebar.leftimg,limittest:sidebar.lefttestimoniallimit,label:sidebar.leftlinklabel,
        url:sidebar.leftlinkurl,limitnews:sidebar.leftnewslimit});
        $scope.sidebar = {};
    
    }

    $scope.deleteleft = function(index){
        $scope.page.sidedataL.splice(index, 1);
    }

    $scope.editleft = function(index){
        document.getElementById("left").focus();
        document.getElementById("left").blur();
        $scope.currentEdit = index;
        $scope.addbtn = false;
        if($scope.page.sidedataL[index].sidebar == 'Menu'){
            $scope.sidebar.leftmenu = $scope.page.sidedataL[index].menuID;
        }
        else if($scope.page.sidedataL[index].sidebar == 'Image'){
            $scope.sidebar.leftimglink = $scope.page.sidedataL[index].imglink;
            $scope.sidebar.leftimg =  $scope.page.sidedataL[index].img;
        }
        else if($scope.page.sidedataL[index].sidebar == 'Testimonial'){
            $scope.sidebar.lefttestimoniallimit =  $scope.page.sidedataL[index].limittest;
        }
        else if($scope.page.sidedataL[index].sidebar == 'Link'){
            $scope.sidebar.leftlinklabel = $scope.page.sidedataL[index].label;
            $scope.sidebar.leftlinkurl = $scope.page.sidedataL[index].url;
        }
        else if($scope.page.sidedataL[index].sidebar == 'News'){
            $scope.sidebar.leftnewslimit = $scope.page.sidedataL[index].limitnews;
        }
        $scope.sidebar.left = $scope.page.sidedataL[index].sidebar;
    }

    $scope.updateleftsidebar = function(index,sidebar){
        if(sidebar.left == 'Menu'){
            $scope.page.sidedataL[index].menuID = sidebar.leftmenu;
        }
        else if(sidebar.left == 'Image'){
            $scope.page.sidedataL[index].imglink = sidebar.leftimglink;
            $scope.page.sidedataL[index].img = sidebar.leftimg;
        }
        else if(sidebar.left == 'Testimonial'){
            $scope.page.sidedataL[index].limittest = sidebar.lefttestimoniallimit;
        }
        else if(sidebar.left == 'Link'){
            $scope.page.sidedataL[index].label = sidebar.leftlinklabel;
            $scope.page.sidedataL[index].url = sidebar.leftlinkurl;
        }
        else if(sidebar.left == 'News'){
            $scope.page.sidedataL[index].limitnews = sidebar.leftnewslimit;
        }
        $scope.page.sidedataL[index].sidebar = sidebar.left;
        $scope.currentEdit = 99999;
        $scope.addbtn = true;
        $scope.sidebar = {};
    }

    $scope.leftCancel = function(){
        $scope.currentEdit = 99999;
        $scope.addbtn = true;
        $scope.sidebar = {};
    }



     //right
    $scope.addbtnright = true;
    $scope.sidebarR = {};
    $scope.page.sidedataR = [];

    $scope.addrightsidebar = function(sidebarR){
        $scope.page.sidedataR.push({sidebar:sidebarR.right, menuID:sidebarR.rightmenu, imglink:sidebarR.rightimglink,
        img:sidebarR.rightimg,limittest:sidebarR.righttestimoniallimit,label:sidebarR.rightlinklabel,
        url:sidebarR.rightlinkurl,limitnews:sidebarR.rightnewslimit});
        $scope.sidebarR = {};
    }

    $scope.deleteright = function(index){
        $scope.page.sidedataR.splice(index, 1);
    }

    $scope.editright = function(index){
        document.getElementById("right").focus();
        document.getElementById("right").blur();
        $scope.currentEditright = index;
        $scope.addbtnright = false;
        if($scope.page.sidedataR[index].sidebar == 'Menu'){
            $scope.sidebarR.rightmenu = $scope.page.sidedataR[index].menuID;
        }
        else if($scope.page.sidedataR[index].sidebar == 'Image'){
            $scope.sidebarR.rightimglink = $scope.page.sidedataR[index].imglink;
            $scope.sidebarR.rightimg =  $scope.page.sidedataR[index].img;
        }
        else if($scope.page.sidedataR[index].sidebar == 'Testimonial'){
             $scope.sidebarR.righttestimoniallimit =  $scope.page.sidedataR[index].limittest;
        }
        else if($scope.page.sidedataR[index].sidebar == 'Link'){
             $scope.sidebarR.rightlinklabel = $scope.page.sidedataR[index].label;
             $scope.sidebarR.rightlinkurl = $scope.page.sidedataR[index].url;
        }
        else if($scope.page.sidedataR[index].sidebar == 'News'){
             $scope.sidebarR.rightnewslimit = $scope.page.sidedataR[index].limitnews;
        }
        $scope.sidebarR.right = $scope.page.sidedataR[index].sidebar;
    }

    $scope.updaterightsidebar = function(index,sidebarR){
        if(sidebarR.right == 'Menu'){
            $scope.page.sidedataR[index].menuID = sidebarR.rightmenu;
        }
        else if(sidebarR.right == 'Image'){
            $scope.page.sidedataR[index].imglink = sidebarR.rightimglink;
            $scope.page.sidedataR[index].img = sidebarR.rightimg;
        }
        else if(sidebarR.right == 'Testimonial'){
            $scope.page.sidedataR[index].limittest = sidebarR.righttestimoniallimit;
        }
        else if(sidebarR.right == 'Link'){
            $scope.page.sidedataR[index].label = sidebarR.rightlinklabel;
            $scope.page.sidedataR[index].url = sidebarR.rightlinkurl;
        }
        else if(sidebarR.right == 'News'){
            $scope.page.sidedataR[index].limitnews = sidebarR.rightnewslimit;
        }
        $scope.page.sidedataR[index].sidebar = sidebarR.right;
        $scope.currentEditright = 99999;
        $scope.addbtnright = true;
        $scope.sidebarR = {};
    }

    $scope.rightCancel = function(){
        $scope.currentEditright = 99999;
        $scope.addbtnright = true;
        $scope.sidebarR = {};
    }


    ////SIDEBAR/////////////////////////////////////////////////////////////////////////////////////////////
    $scope.alertss = [];

    $scope.closeAlerts = function (index) {
        $scope.alertss.splice(index, 1);
    };

    /////START SPECIAL PAGE

    var counter2=0;
    $scope.countrow = 0;
    $scope.rowcount = [];
    $scope.row = [];
    $scope.rowcount.push({counter2});
    $scope.row.push({row:[],count:counter2,column:"",colcount:""});

 $scope.rowdelitem = function(index){
         var modalInstance = $modal.open({
            templateUrl: 'deleterow.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                $scope.ok = function(){
                    rowdel(index);
                     $modalInstance.dismiss('cancel');
                }
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
              };
          }
      })
  }

  var rowdel = function(index)
  {
       $scope.row.splice(index,1);
  }
    $scope.rownewitem = function($event){
        counter2++;
        $scope.countrow++;
        $scope.rowcount.push( {counter2} );
        $event.preventDefault();
        $scope.row.push({row:[],count:counter2,column:"",colcount:""});
    }

    var selectedcol = '';
    $scope.column = [];
    $scope.page.colcount = [];
    $scope.page.row= [];
    var column = function(index,column){
        console.log(selectedcol)
     
        if (column != undefined) {
            $scope.row[index].column=column;
        }else{
            $scope.row[index].column=selectedcol;
        }

        $scope.row[index].colcount=selectedcol;
     
        // $scope.rowindex.push({column});
    }


 $scope.delcolumn = function(countcol,index,col){
    $scope.row[index].colcount = countcol - 1;
    $scope.row[index].column = countcol - 1;
    $scope.row[index].row.splice(col,1);
    $scope.rowcount.splice(index,1);

}
    //MODULE 
    var selectedmod = '';


    
    // $scope.rowindex = [];
    $scope.insertmodule = function(col,index){
        var modalInstance = $modal.open({
            templateUrl: 'module.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
            col = col+1;
                $scope.setmod = function(mod){
                    if(mod=="TEXT"){
                        console.log(col)
                        textmodule(col,index);
                        $modalInstance.dismiss('cancel');
                    }
                    else if(mod=="IMAGE"){
                        media('featured','banner',index,col);
                        $modalInstance.dismiss('cancel');
                    }
                    else if(mod=="TESTIMONIAL"){
                        testimonialmodule(col,index);
                        $modalInstance.dismiss('cancel');
                    }
                    else if(mod=="CONTACT"){
                        contactmodule(col,index)
                        $modalInstance.dismiss('cancel');
                    }
                    else if(mod=="NEWS"){
                        newsmodule(col,index);
                        $modalInstance.dismiss('cancel');
                    }
                    else if(mod=="DIVIDER"){
                        dividermodule(col,index);
                        $modalInstance.dismiss('cancel');
                    }
                    else if(mod=="BUTTON"){
                        buttonmodule(col,index);
                        $modalInstance.dismiss('cancel');
                    }
                }
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
              };
          }
      })
}

    ////TEXT MODULE 
    var alltext = '';


    var allrow = function (col,index,columnname) {
            if (columnname=='TEXT') {
                 $scope.row[index].row.push({col:selectedmod,colcontent:alltext});
            }
            else if (columnname=='IMAGE'){
                $scope.row[index].row.push({col:selectedmod,colimg:allimg,colimglink:allimglink});
            }
             else if (columnname=='TESTIMONIAL'){
                $scope.row[index].row.push({col:selectedmod,coltestimonial:testi,
                    coltestimonialcat:testicat});
            }
             else if (columnname=='NEWS'){
                $scope.row[index].row.push({col:selectedmod,colnewslimit:allnewslimit});
            }
             else if (columnname=='DIVIDER'){
                $scope.row[index].row.push({col:selectedmod,colheight:allheight,colcolor:allcolor});
            }
            else if (columnname=='BUTTON'){
                $scope.row[index].row.push({col:selectedmod,colbtnname:allbtnname,colbtnlink:allbtnlink,
                    colbgcolor:allbgcolor,colfcolor:allfcolor,colpadtop:allpadtop,colpadside:allpadside
                    ,colfont:allfont,colposition:allposition});
            }
            else if (columnname=='CONTACT'){
                $scope.row[index].row.push({col:selectedmod,coltitle:contacttitle,colphone:contactphone,
                    colemail:contactemail,colhours:contacthours,coladdress:contactaddress,str:str});
            }
          
            // console.log($scope.row)
    
    }
     var editrow = function (index,childindex,columnname) {
        $scope.s3link = Config.amazonlink;
                if (columnname=='TEXT') {
                   $scope.row[index].row[childindex].col = selectedmod;
                   $scope.row[index].row[childindex].colcontent = alltext;
               }
               else if (columnname=='IMAGE'){
                   $scope.row[index].row[childindex].col = selectedmod;
                   $scope.row[index].row[childindex].colimg = allimg;
                   $scope.row[index].row[childindex].colimglink = allimglink;
               }
               else if (columnname=='TESTIMONIAL'){
                   $scope.row[index].row[childindex].col = selectedmod;
                   $scope.row[index].row[childindex].coltestimonial = testi;
                   $scope.row[index].row[childindex].coltestimonialcat = testicat;
               }
               else if (columnname=='NEWS'){
                   $scope.row[index].row[childindex].col = selectedmod;
                   $scope.row[index].row[childindex].colnewslimit = allnewslimit;
               }
               else if (columnname=='DIVIDER'){
                 $scope.row[index].row[childindex].col = selectedmod;
                 $scope.row[index].row[childindex].colheight = allheight;
                 $scope.row[index].row[childindex].colcolor = allcolor;
             }
             else if (columnname=='BUTTON'){
               $scope.row[index].row[childindex].col = selectedmod;
               $scope.row[index].row[childindex].colbtnname = allbtnname;
               $scope.row[index].row[childindex].colbtnlink = allbtnlink;
               $scope.row[index].row[childindex].colbgcolor = allbgcolor;
               $scope.row[index].row[childindex].colfcolor = allfcolor;
               $scope.row[index].row[childindex].colpadtop = allpadtop;
               $scope.row[index].row[childindex].colpadside = allpadside;
               $scope.row[index].row[childindex].colfont = allfont;
               $scope.row[index].row[childindex].colposition = allposition;
            }
            else if (columnname=='CONTACT'){
               $scope.row[index].row[childindex].col = selectedmod;
               $scope.row[index].row[childindex].coltitle = contacttitle;
               $scope.row[index].row[childindex].colphone = contactphone;
               $scope.row[index].row[childindex].colemail = contactemail;
               $scope.row[index].row[childindex].colhours = contacthours;
               $scope.row[index].row[childindex].coladdress = contactaddress;
            }

    }

    var textmodule = function(col,index,childindex){
 
        var modalInstance = $modal.open({
            templateUrl: 'moduletext.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                $scope.gallery = function(){
                    media('content');
                }
                $scope.settext = function(text){
                    selectedmod = 'TEXT';
                    alltext = text;
                    selectedcol = col;
                    column(index);
                    if (childindex != undefined) {
                       editrow(index,childindex,'TEXT');
                   }
                   else{
                       allrow(col,index,'TEXT');
                   }

                    column(index);
                    $modalInstance.dismiss('cancel');
                }
                $scope.cancel = function() {

                  $modalInstance.dismiss('cancel');
              };
          }
      })
    }

    $scope.edittextmodule = function(col,index,content,childindex){
        var modalInstance = $modal.open({
            templateUrl: 'moduletext.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                $scope.textarea = content;
                $scope.gallery = function(){
                    media('content');
                }
                $scope.settext = function(text){
                    selectedmod = 'TEXT';
                    alltext = text;
                    editrow(index,childindex,'TEXT');
                    $modalInstance.dismiss('cancel');
                }
                $scope.cancel = function(){
                  $modalInstance.dismiss('cancel');
              };
          }
      })
    }


    //Image Module

    var allimg = '', 
        allimglink = '';


    var imagemodule = function(col,index,imageset,childindex,linkset){
        var modalInstance = $modal.open({
            templateUrl: 'moduleimage.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                $scope.s3link = Config.amazonlink;
                $scope.gallery = function(){
                    media('featured','banner',index,col,childindex);
                    $modalInstance.dismiss('cancel');
                }
                $scope.setimg = function(image,link){
                    
                    selectedmod = 'IMAGE';
                    allimg = image;
                    allimglink = link;
                    if (childindex != undefined) {
                     editrow(index,childindex,'IMAGE');
                 }
                 else{
                     allrow(col,index,'IMAGE');
                 }
                 selectedcol = col;
                 column(index);
                    // imgcontent(col,index);
                   // module(col,index);
                    $modalInstance.dismiss('cancel');
                }
                $scope.colimg = imageset;
                $scope.link = linkset;
                $scope.cancel = function() {

                  $modalInstance.dismiss('cancel');
              };
          }
      })
}



$scope.editimagemodule = function(col,index,images,childindex,link){
    imagemodule(col,index,images,childindex,link);
}


    ////TESTIMONIAL MODULE 
    var testi = '',
        testicat = '';

    var testimonialmodule = function(col,index,childindex){
        var modalInstance = $modal.open({
            templateUrl: 'moduletestimonial.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                $scope.category ='';
                $scope.settestimonial = function(text,cat){
                    testi = text;
                    testicat = cat;
                    selectedmod = 'TESTIMONIAL';
                    // testimonialcontent(col,index);
                    // module(col,index);
                     selectedcol = col;
                    column(index);
                    if (childindex != undefined) {
                           editrow(index,childindex,'TESTIMONIAL');
                       }
                       else{
                           allrow(col,index,'TESTIMONIAL');
                       }
                      
                    $modalInstance.dismiss('cancel');
                }
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
              };

              Special.get(function(pages){
                $scope.pages = pages;
            });
          }
      })
}


$scope.edittestimonialmodule = function(col,index,content,cat,childindex){
        var modalInstance = $modal.open({
            templateUrl: 'moduletestimonial.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
               $scope.text = content;
                Special.get(function(pages){
                   $scope.pages = pages;
                });
                $scope.category = cat;
                $scope.settestimonial = function(text,cat){
                    selectedmod = 'TESTIMONIAL';
                    testi = text;
                    testicat = cat;
                    editrow(index,childindex,'TESTIMONIAL');
                    $modalInstance.dismiss('cancel');
                }
                $scope.cancel = function() {
                 
                  $modalInstance.dismiss('cancel');
              };
  }
})
}


    ////TESTIMONIAL MODULE 



    ////NEWS MODULE 
    var newslimit4 = '',
        allnewslimit = '';


    var newsmodule = function(col,index,view,popular,limit,childindex){
        console.log(childindex)
        var modalInstance = $modal.open({
            templateUrl: 'modulenews.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                if(view==undefined){
                    $scope.viewed = 'false';
                }
                else if(view!=undefined){
                    $scope.viewed = view;
                }
                else if(popular==undefined){
                    $scope.popular = 'false';
                }
                else if(popular!=undefined){
                    $scope.popular = popular;
                }
                $scope.limit = limit;
                $scope.setoption = function(view,popular,limit){
                   
                    selectedmod = 'NEWS';
                    allnewslimit = limit;
                    if (childindex != undefined) {
                       editrow(index,childindex,'NEWS');
                   }
                   else{
                       allrow(col,index,'NEWS');
                   }
                   selectedcol = col;
                   column(index);
                   $modalInstance.dismiss('cancel');
               }
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
              };
          }
      })
}

$scope.editnewsmodule = function(col,index,view,popular,limit,childindex){
   newsmodule(col,index,view,popular,limit,childindex);
}


    ////Divider MODULE 
    var allcolor = '',
        allheight = '';

    var dividermodule = function(col,index,height,color,childindex){
        var modalInstance = $modal.open({
            templateUrl: 'moduledivider.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                $scope.height = height;
                if(color==undefined){
                    $scope.color = '#ffffff';
                }
                else if(color!=undefined){
                    $scope.color = color;
                }
                $scope.setoption = function(height,color){
                    
                    selectedmod = 'DIVIDER';
                    allcolor = color;
                    allheight = height;
                    // dividercontent(col,index,column);
                    // module(col,index);
                    selectedcol = col;
                   
                      if (childindex != undefined) {
                           editrow(index,childindex,'DIVIDER');
                       }
                       else{
                          column(index);
                           allrow(col,index,'DIVIDER');
                       }
                    $modalInstance.dismiss('cancel');
                }
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
              };
          }
      })
}

$scope.editdividermodule = function(col,index,height,color,childindex){
   dividermodule(col,index,height,color,childindex);
}


////Button MODULE 
    var allbtnname = '',
        allbgcolor = '',
        allfcolor = '',
        allpadtop = '',
        allpadside = '',
        allfont = '',
        allposition = '',
        allbtnlink = '';

    var buttonmodule = function(col,index,counter,childindex){
        var modalInstance = $modal.open({
            templateUrl: 'modulebutton.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                $scope.button = [];
                if(counter==undefined){
                    $scope.button.bgcolor = '#5cce5f';
                    $scope.button.fcolor = '#ffffff';
                    $scope.button.padtop = 5;
                    $scope.button.padside = 25;
                    $scope.button.position = 'Center';
                    $scope.button.font = 16;
                }else{
                     $scope.button.btnname = counter.colbtnname;
                    $scope.button.btnlink = counter.colbtnlink;
                    $scope.button.bgcolor = counter.colbgcolor;
                    $scope.button.fcolor = counter.colfcolor;
                    $scope.button.padtop = counter.colpadtop;
                    $scope.button.padside = counter.colpadside;
                    $scope.button.font = counter.colfont;
                    $scope.button.position = counter.colposition;
                }
                $scope.setoption = function(button){
                    selectedmod = 'BUTTON';
                    allbtnname = button.btnname;
                    allbtnlink = button.btnlink;
                    allbgcolor = button.bgcolor;
                    allfcolor = button.fcolor;
                    allpadside = button.padside;
                    allpadtop = button.padtop;
                    allfont = button.font;
                    allposition = button.position;
                    selectedcol = col;
                      if (childindex != undefined) {
                           editrow(index,childindex,'BUTTON');
                       }
                       else{
                          column(index);
                          allrow(col,index,'BUTTON');
                       }
                    $modalInstance.dismiss('cancel');
                }
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
              };
          }
      })
}

$scope.editbuttonmodule = function(col,index,counter,childindex){
   buttonmodule(col,index,counter,childindex);
}

////Contact MODULE 
    var contacttitle = '',
        contactemail = '',
        contacthours = '',
        contactaddress = '',
        str = '',
        contactphone = '';


    var contactmodule = function(col,index,counter,childindex){
        // console.log(index)
        var modalInstance = $modal.open({
            templateUrl: 'modulecontact.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                if (counter==undefined) {
                    Special.getinfo(function(data) {
                      $scope.contact = data[0];
                    });
                }else{
                    $scope.contact = [];
                    $scope.contact.title = counter.coltitle;
                    $scope.contact.phone = counter.colphone;
                    $scope.contact.email = counter.colemail;
                    $scope.contact.hours = counter.colhours;
                    $scope.contact.address = counter.coladdress;
                }
                 
                $scope.ok = function(contact){
                    selectedmod = 'CONTACT';
                    contacttitle = contact.title;
                    contactemail = contact.email;
                    contacthours = contact.hours;
                    contactaddress = contact.address;
                    contactphone = contact.phone;
                    str = contact.address;
                    str = str.split('\n');
                    if (childindex != undefined) {
                       editrow(index,childindex,'CONTACT');
                   }
                   else{
                       allrow(col,index,'CONTACT');
                   }
                   selectedcol = col;
                   column(index);
                   $modalInstance.dismiss('cancel');
               }
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
              };
          }
      })
}

$scope.editcontactmodule = function(col,index,counter,childindex){
   contactmodule(col,index,counter,childindex);
}




    /////END SPECIAL PGE

    $scope.media = function(type, thumb,sidebar){
        var modalInstance = $modal.open({
            templateUrl: 'mediagallery.html',
            controller: function($scope, $modalInstance,Config, type) {
                $scope.imageloader=false;
                $scope.directory = 'pageimage';
                $scope.videoenabled=false;
                $scope.mediaGallery = 'images';
                $scope.imagecontent=true;
                $scope.invalidvideo = false;
                $scope.currentSelected = '';
                $scope.currentDeleting = '';
                $scope.type = type;
                $scope.s3link = Config.amazonlink;
                console.log($scope.s3link);

                $scope.set = function(id){
                    $scope.currentSelected = id;
                    $scope.contentvideo = id;
                    if(type=='content'){
                        // $scope.copy();
                    }
                }

                $scope.returnImageThumb = function(){
                    return function (item) {
                        if(item){
                            return Config.amazonlink + "/uploads/" + $scope.directory + "/" + item;
                        }else{
                            return item;
                        }
                    };
                }

                var loadimages = function() {
                    Special.loadimages(function(data){
                        // console.log(data);
                        $scope.imagelist = data;
                        $scope.imagelength = data.length;
                    });
                }

                loadimages();

                $scope.upload = function (files)
                {
                $scope.alertss = [];
                    var filename
                    var filecount = 0;
                    if (files && files.length)
                    {
                        $scope.imageloader=true;
                        $scope.imagecontent=false;

                        for (var i = 0; i < files.length; i++)
                        {
                            var file = files[i];

                            if (file.size >= 2000000)
                            {
                                $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big. Maximum size is 2MB'});
                                filecount = filecount + 1;

                                if(filecount == files.length)
                                {
                                    $scope.imageloader=false;
                                    $scope.imagecontent=true;
                                }


                            }
                            else

                            {
                                var promises;
                                var f = file.name;
                                var fname=f.replace(/\s+/g, "-");
                                promises = Upload.upload({

                                    url: Config.amazonlink, //S3 upload url including bucket name
                                    method: 'POST',
                                    transformRequest: function (data, headersGetter) {
                                        //Headers change here
                                        var headers = headersGetter();
                                        delete headers['Authorization'];
                                        return data;
                                    },
                                    fields : {
                                        key: 'uploads/'+$scope.directory+'/' + fname, // the key to store the file on S3, could be file name or customized
                                        AWSAccessKeyId: Config.AWSAccessKeyId,
                                        acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                                        policy: Config.policy, // base64-encoded json policy (see article below)
                                        signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                        "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                    },
                                    file: file
                                })
promises.then(function(data){

    filecount = filecount + 1;
    filename = data.config.file.name;
    var fn = filename;
    var finalname=fn.replace(/\s+/g, "-");
    var fileout = {
        'imgfilename' : finalname
    };
    Special.saveimage(fileout, function(data){
        loadimages();
        if(filecount == files.length)
        {
            $scope.imageloader=false;
            $scope.imagecontent=true;
        }
    });

});


}


}
}

};
$scope.closeAlerts = function (index) {
        $scope.alertss.splice(index, 1);
    };
$scope.deletenewsimg = function (dataimg, $event)
{
    var fileout = {
        'imgfilename' : dataimg
    };
    Special.deleteimage(fileout, function(data) {
        loadimages();
    });
    $event.stopPropagation();
}

$scope.copy = function() {
    console.log($scope.contentvideo.filename);
    var text = '';
    if($scope.contentvideo.filename){
        text = Config.amazonlink + '/uploads/'+$scope.directory+'/' + $scope.contentvideo.filename;
    }
    // var pp = window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
    // if(pp != "" && pp !== null) {
    //     $modalInstance.dismiss('cancel');
    // }
    var oEditor = CKEDITOR.instances.normaleditor;
    var image = $scope.s3link+'/uploads/pageimage/'+$scope.contentvideo.filename;
    console.log(image);
    var html = "<img src="+ image +" style='width:300px;height:200px'>";

    var newElement = CKEDITOR.dom.element.createFromHtml( html, oEditor.document );
    oEditor.insertElement( newElement );
    $modalInstance.dismiss('cancel');
}

$scope.ok = function(category) {
    console.log(category)
    $modalInstance.close($scope.contentvideo);
}

$scope.cancel = function() {
    $modalInstance.dismiss('cancel');
}
},
resolve: {
    type: function(){
        return type;
    }
},
windowClass: 'xxx-modal-window'
}).result.then(function(res) {
    console.log("============================");
    if(res.filename) {
        if(thumb == 'banner'){
            if(sidebar == 'left'){
                $scope.sidebar.leftimg = res.filename;
            }else{
                $scope.sidebarR.rightimg = res.filename;
            }
        }else if(thumb == 'thumb'){
            $scope.page.imagethumb = res.filename;
        }
    }
});
}



var media = function(type, thumb,index,col,childindex){
    var modalInstance = $modal.open({
        templateUrl: 'mediagallery.html',
        controller: function($scope, $modalInstance,Config, type) {
            $scope.imageloader=false;
            $scope.directory = 'pageimage';
            $scope.videoenabled=false;
            $scope.mediaGallery = 'images';
            $scope.imagecontent=true;
            $scope.invalidvideo = false;
            $scope.currentSelected = '';
            $scope.currentDeleting = '';
            $scope.type = type;
            $scope.s3link = Config.amazonlink;
            console.log($scope.s3link);

            $scope.set = function(id){
                $scope.currentSelected = id;
                $scope.contentvideo = id;
                if(type=='content'){
                    $scope.copy();
                }
            }

            $scope.returnImageThumb = function(){
                return function (item) {
                    if(item){
                        return Config.amazonlink + "/uploads/" + $scope.directory + "/" + item;
                    }else{
                        return item;
                    }
                };
            }

            var loadimages = function() {
                Special.loadimages(function(data){
                    // console.log(data);
                    $scope.imagelist = data;
                    $scope.imagelength = data.length;
                });
            }

            loadimages();

            $scope.upload = function (files)
            {
                $scope.alertss = [];

                var filename
                var filecount = 0;
                console.log(files)
                if (files && files.length)
                {
                    // $scope.imageloader=true;
                    // $scope.imagecontent=false;

                    for (var i = 0; i < files.length; i++)
                    {
                        var file = files[i];

                        if (file.size >= 2000000)
                        {
                            $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big. Maximum size is 2MB'});
                            filecount = filecount + 1;

                            if(filecount == files.length)
                            {
                                $scope.imageloader=false;
                                $scope.imagecontent=true;
                            }


                        }
                        else

                        {
                            var promises;
                            var f = file.name;
                            var fname=f.replace(/\s+/g, "-");
                            // console.log(file)
                            promises = Upload.upload({

                                    url: Config.amazonlink, //S3 upload url including bucket name
                                    method: 'POST',
                                    transformRequest: function (data, headersGetter) {
                                        //Headers change here
                                        var headers = headersGetter();
                                        delete headers['Authorization'];
                                        return data;
                                    },
                                    fields : {
                                        key: 'uploads/'+$scope.directory+'/' + fname, // the key to store the file on S3, could be file name or customized
                                        AWSAccessKeyId: Config.AWSAccessKeyId,
                                        acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                                        policy: Config.policy, // base64-encoded json policy (see article below)
                                        signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                        "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                    },
                                    file: file
                                })
promises.then(function(data){

    filecount = filecount + 1;
    filename = data.config.file.name;
        var fn = filename;
        var finalname=fn.replace(/\s+/g, "-");
    var fileout = {
        'imgfilename' : finalname
    };
    Special.saveimage(fileout, function(data){
        loadimages();
        if(filecount == files.length)
        {
            $scope.imageloader=false;
            $scope.imagecontent=true;
        }
    });

});


}


}
}

};

$scope.closeAlerts = function (index) {
        $scope.alertss.splice(index, 1);
    };

$scope.deletenewsimg = function (dataimg, $event)
{
    var fileout = {
        'imgfilename' : dataimg
    };
    Special.deleteimage(fileout, function(data) {
        loadimages();
    });
    $event.stopPropagation();
}

$scope.copy = function() {
    console.log($scope.contentvideo);
    var text = '';
    if($scope.contentvideo.filename){
        text = Config.amazonlink + '/uploads/'+$scope.directory+'/' + $scope.contentvideo.filename;
    }
    var pp = window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
    if(pp != "" && pp !== null) {
        $modalInstance.dismiss('cancel');
    }
}

$scope.ok = function(category) {
    $modalInstance.close($scope.contentvideo);
}

$scope.cancel = function() {
    $modalInstance.dismiss('cancel');
}
},
resolve: {
    type: function(){
        return type;
    }
},
windowClass: 'xxx-modal-window'
}).result.then(function(res) {

    if(res.filename) {
        if(thumb == 'banner'){
            $scope.colimg = res.filename;
            imagemodule(col,index,res.filename,childindex);
        }
    }
});
}


}) //end
