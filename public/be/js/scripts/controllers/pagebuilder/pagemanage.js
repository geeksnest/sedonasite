app.controller('pagemanageCtrl', function($http, $scope, $stateParams, $modal, $sce, Config, Special, $anchorScroll) {
  //LIST
  var num = 10;
  var off = 1;
  var keyword = null;
  $scope.loading = false;


  var paginate = function(off, num, keyword) {
    $scope.loading = true;
    Special.list(num, off, keyword, function(data) {
      $scope.data = data.data;
      console.log(data.data)
      $scope.total_items = data.total_items;
      if (keyword !== undefined && off === 0) {
        $scope.keywordshow = true;
        $scope.keyword = keyword;
      }
      $scope.maxSize = 10;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
      $scope.loading = false;


    });
  };

paginate(num, off, keyword);

 $scope.setstatus = function (status,pageid,keyword) {
  console.log(status,pageid,keyword)
        var newstat;
        if(status == 1)
        {
            newstat = 0;
        }
        else
        {
            newstat = 1;
        }
        Special.updatestatus(pageid,$scope.keyword, newstat, function(data){
            $scope.currentstatusshow = pageid;
            var i = 2;
            setInterval(function(){
                i--;
                if(i == 0)
                {
                    paginate(10, 1, $scope.keyword, $scope.sort);
                    $scope.currentstatusshow = 0;
                }
            },1000)
        });
    }


 $scope.search = function (keyword) {
        var off = 0;
        $scope.keyword = keyword;
        paginate(num, off, keyword);
        $scope.searchtext='';
    }

 
    $scope.clear = function(){
        $scope.keyword=null;
        paginate(num, off, keyword);
    }
  $scope.setPage = function (pageNo) {
        paginate(num, pageNo, keyword);
    };

  //END LISTING


  //DELETE
  $scope.delete = function(id) {
    var modalInstance = $modal.open({
      templateUrl: 'homepageDelete.html',
      controller: dltCTRL,
      resolve: {
        dlt: function() {
          return id;
        }
      }
    });
  };

  $scope.alerts = [];
  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };
  var alertme = function(data) {
$scope.alerts = [{type: 'success', msg: 'Page successfully deleted!'}];
  };
  var dltCTRL = function($scope, $modalInstance, dlt) {
    $scope.id = dlt;

    $scope.ok = function(dlt) {
      Special.delete(dlt, function(data) {
        paginate(num,off, keyword);
        $modalInstance.dismiss('cancel');
        $scope.keywordshow = false;
      });
    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  };
  //UPDATE USER
  $scope.update = function(id) {
    var modalInstance = $modal.open({
      templateUrl: 'homepageUpdate.html',
      controller: updateCTRL,
      resolve: {
        update: function() {
          return id;
        }
      }
    });
  };
  var updateCTRL = function($scope, $modalInstance, update, $state) {
    $scope.update = update;
    $scope.ok = function(update) {
      $state.go('pageedit', {
        pageid: update
      });
      $modalInstance.dismiss('cancel');

    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  };

});
