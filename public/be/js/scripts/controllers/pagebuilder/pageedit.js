'use strict';

app.controller('pageeditCtrl', function($timeout,$http, Upload,$scope, $stateParams, $modal,$sce, Config, Special, $filter, $anchorScroll) {
    $scope.process = 0;
    $scope.amazonlink = Config.amazonlink;
    $scope.alerts = [];
    $scope.closeAlert = function(index) {
     $scope.alerts.splice(index, 1);
    };
    $scope.s3link = Config.amazonlink;
    $scope.directory = 'pageimage';
    $scope.eventSources = [];
//load news
Special.loadnews(function(data) {
$scope.news = data;
 for(var key in data){
                var categorylist = [];
                data[key].category.map(function(val){
                    categorylist.push(val.categoryname);
                });
                data[key]['categorylist'] = categorylist.join(", ");
            }

});
//
//load news
Special.loadtesti(function(data) {
$scope.testi = data.testi;
$scope.archive = data.archive;
$scope.shortcode = data.shortcode;
$scope.children = data.children;
});
//

    $scope.page = {};
    $scope.page = [];
    $scope.row1 = [];
    var info = function(){
        Special.info($stateParams.pageid, function(data) {
          console.log(data)
           for(var key in data.datacol){
            var length=data.datacol[key].length;
               $scope.row1.push({row:[],colcount:length,columns:length,column:length});

                for(var k in data.datacol[key]){
                    if (data.datacol[key][k].address!=undefined) {
                    var str = data.datacol[key][k].address;
                    str = str.split('\n');
                    }
                    
            switch(data.datacol[key][k].module){
            case 'TEXT':
                  $scope.row1[key].row.push({col:data.datacol[key][k].module,colcontent:data.datacol[key][k].content});
                break;
            case 'IMAGE':
                $scope.row1[key].row.push({col:data.datacol[key][k].module,colimg:data.datacol[key][k].image,colimglink:
                data.datacol[key][k].link});
                break;
            case 'TESTIMONIAL':
                $scope.row1[key].row.push({col:data.datacol[key][k].module,coltestimonial:data.datacol[key][k].limitcount,
                coltestimonialcat:data.datacol[key][k].category});
                break;
            case 'CONTACT':
                $scope.row1[key].row.push({col:data.datacol[key][k].module,coltitle:data.datacol[key][k].title,colphone:data.datacol[key][k].phone,
                    colemail:data.datacol[key][k].email,colhours:data.datacol[key][k].hours,coladdress:data.datacol[key][k].address,str:str});
                break;
            case 'NEWS':
                $scope.row1[key].row.push({col:data.datacol[key][k].module,colnewslimit:data.datacol[key][k].limitcount}); 
                break;
            case 'DIVIDER':
              $scope.row1[key].row.push({col:data.datacol[key][k].module,colheight:data.datacol[key][k].height,colcolor:data.datacol[key][k].color}); 
            break;
            case 'BUTTON':
              $scope.row1[key].row.push({col:data.datacol[key][k].module,colbtnname:data.datacol[key][k].btnname,colbtnlink:data.datacol[key][k].btnlink
                ,colbgcolor:data.datacol[key][k].bgcolor,colfcolor:data.datacol[key][k].fcolor,colpadtop:data.datacol[key][k].padtop,colpadside:data.datacol[key][k].padside
                ,colfont:data.datacol[key][k].font,colposition:data.datacol[key][k].position}); 
            break;
            default:
                  $scope.row1[key].row.push({col:data.datacol[key][k].module}); 
            break;
         }
                   
                }

    }
          $scope.page = data.normal[0];
          if (data.normal[0].leftsidebar=='false' || data.normal[0].leftsidebar==null) {
            $scope.page.sidebarleft = false;
          }else{
            $scope.page.sidebarleft = true;
          }
           if (data.normal[0].rightsidebar=='false' || data.normal[0].rightsidebar==null) {
            $scope.page.sidebarright = false;
           }else{
            $scope.page.sidebarright = true;
          }
          $scope.defaultslugs = data.normal[0].slugs;
          $scope.page.slugs = data.normal[0].slugs;
          $scope.leftsidebarelement = data.leftsidebar;
          $scope.rightsidebarelement = data.rightsidebar;
          
          if(data.normal[0]['banner'] == 'false'){
            $scope.page.banner = false;
            $scope.page.align = 'center'; 
            $scope.page.bgcolor = '#ffffff';
            $scope.page.color = '#ffffff';
            $scope.page.box=false;
          }else{
            $scope.page.banner = true;
          }
          if(data.normal[0]['box'] == 'true'){
           $scope.page.box=true;  
          }else{
              $scope.page.box=false;     
          }

          $scope.leftsidebardata = data.leftsidebardata;
          $scope.rightsidebardata = data.rightsidebardata;
         if (data.leftsidebardata==undefined) {
             $scope.countleft = 0;   
         }
         else
         {
            $scope.countleft = data.leftsidebardata.length;
         }

        $scope.sleft = data.normal[0]['leftsidebar'];
        $scope.sright = data.normal[0]['rightsidebar'];

        $scope.page.sidedataL = [];
        $scope.page.sidedataR = [];
        
      if(data.normal[0]['leftsidebar']=='true')
      {
        for(var key in data.leftsidebardata){
         switch(data.leftsidebardata[key]['sidebar']){
            case 'Menu':
                $scope.page.sidedataL.push({sidebar:data.leftsidebardata[key]['sidebar'], menuID:data.leftsidebardata[key]['menuID'], imglink:"",
                img:"",limittest:"",label:"",
                url:"",limitnews:""});
                break;
            case 'Testimonial':
                $scope.page.sidedataL.push({sidebar:data.leftsidebardata[key]['sidebar'], menuID:"", imglink:"",
                img:"",limittest:data.leftsidebardata[key]['limittest'],label:"",
                url:"",limitnews:""});
                break;
            case 'Image':
                $scope.page.sidedataL.push({sidebar:data.leftsidebardata[key]['sidebar'], menuID:"", imglink:data.leftsidebardata[key]['imglink'],
                img:data.leftsidebardata[key]['img'],limittest:"",label:"",
                url:"",limitnews:""});  
                break;
            case 'Link':
                $scope.page.sidedataL.push({sidebar:data.leftsidebardata[key]['sidebar'], menuID:"", imglink:"",
                img:"",limittest:"",label:data.leftsidebardata[key]['label'],
                url:data.leftsidebardata[key]['url'],limitnews:""}); 
                break;
            case 'News':
                $scope.page.sidedataL.push({sidebar:data.leftsidebardata[key]['sidebar'], menuID:"", imglink:"",
                img:"",limittest:"",label:"",
                url:"",limitnews:data.leftsidebardata[key]['limitnews']}); 
                break;
            default:
                 $scope.page.sidedataL.push({sidebar:data.leftsidebardata[key]['sidebar'], menuID:"", imglink:"",
                img:"",limittest:"",label:"",
                url:"",limitnews:""}); 
            break;
         }
        }
        $scope.page.sidebarleft = true;
        $scope.sidebarleft = true;
      }

      if(data.normal[0]['rightsidebar']=='true')
      {
        for(var key in data.rightsidebardata){
         switch(data.rightsidebardata[key]['sidebar']){
           case 'Menu':
                $scope.page.sidedataR.push({sidebar:data.rightsidebardata[key]['sidebar'], menuID:data.rightsidebardata[key]['menuID'], imglink:"",
                img:"",limittest:"",label:"",
                url:"",limitnews:""});
                break;
            case 'Testimonial':
                $scope.page.sidedataR.push({sidebar:data.rightsidebardata[key]['sidebar'], menuID:"", imglink:"",
                img:"",limittest:data.rightsidebardata[key]['limittest'],label:"",
                url:"",limitnews:""});
                break;
            case 'Image':
                $scope.page.sidedataR.push({sidebar:data.rightsidebardata[key]['sidebar'], menuID:"", imglink:data.rightsidebardata[key]['imglink'],
                img:data.rightsidebardata[key]['img'],limittest:"",label:"",
                url:"",limitnews:""});  
                break;
            case 'Link':
                $scope.page.sidedataR.push({sidebar:data.rightsidebardata[key]['sidebar'], menuID:"", imglink:"",
                img:"",limittest:"",label:data.rightsidebardata[key]['label'],
                url:data.rightsidebardata[key]['url'],limitnews:""}); 
                break;
            case 'News':
                $scope.page.sidedataR.push({sidebar:data.rightsidebardata[key]['sidebar'], menuID:"", imglink:"",
                img:"",limittest:"",label:"",
                url:"",limitnews:data.rightsidebardata[key]['limitnews']}); 
                break;
            default:
                 $scope.page.sidedataR.push({sidebar:data.rightsidebardata[key]['sidebar'], menuID:"", imglink:"",
                img:"",limittest:"",label:"",
                url:"",limitnews:""}); 
            break;
         }
        }
        $scope.page.sidebarright = true;
        $scope.sidebarright = true;
        $scope.page.sidebarright = true;
      }

      
     
      
    });

}
Special.getmenu(function(data) {
for(var key in data){
            
                       var n={
                      
                            menuID:data[key]['menuID'],
                            name:data[key]['name']
                        }
                        data[key]=n;
                    }
                   $scope.menu = data;

});

    info();

    $scope.imageloader = false;
    $scope.imagecontent = true;
    $scope.showUpdate=false;
    $scope.editslug = false;
    $scope.editedslug = false;
    var slugstorage = "";

    var oripage = angular.copy($scope.page);

    $scope.onpagetitle = function convertToSlug(Text) {
        if(Text != null && $scope.editedslug == false && $scope.editslug == false)
        {
            $scope.page.slugs = Text.replace(/\s+/g, '-').toLowerCase();
        }
    }

    $scope.uniqueslugs = function(slugs){
        if($scope.defaultslugs != slugs){
            Special.uniquepage(slugs,function(data){
                 if(data==0){
                     $scope.validslugs = false;
                 }
                 else{
                    $scope.validslugs = true;
                }
            }); 
        }
    }

    $scope.onslugs = function(Text){
        if(Text != null)
        {
            $scope.page.slugs = Text.replace(/\s+/g, '-').toLowerCase();
        }
    }

    $scope.editpageslug = function(){
        $scope.editslug = true;
        slugstorage = $scope.page.slugs;
    }

    $scope.cancelpageslug = function(title) {
        if(title != null)
        {
            $scope.editedslug = false;
            $scope.page.slugs = slugstorage;
        }else {
            $scope.page.slugs = '';
            $scope.editedslug = false;
        }
        $scope.editslug = false;
    }

    $scope.setslug = function(slug){
        $scope.editedslug = true;
        $scope.editslug = false;
        if(slug != null)
        {
            $scope.page.slugs = slug.replace(/\s+/g, '-').toLowerCase();

        }
    }

    $scope.clearslug = function(title){
        if(title != null)
        {
            $scope.editedslug = false;
            var slugs = title.replace(/\s+/g, '-').toLowerCase();
            if($scope.defaultslugs != slugs){
                Special.uniquepage(slugs,function(data){
                     if(data==0){
                     $scope.validslugs = false;
                 }
                 else{
                    $scope.validslugs = true;
                }
                
                }); 
            }
            $scope.page.slugs = title.replace(/\s+/g, '-').toLowerCase();
        }else {
            $scope.page.slugs = '';
            $scope.editedslug = false;
        }
    }
    
    $scope.saveme = function(page) {
        // console.log(page)
         $scope.alerts = [];
        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };
        if(page.banner == false){
            $scope.page.imagethumb = "";
            $scope.page.imagethumbsubtitle = "";
            $scope.page.thumbdesc = "";
            $scope.page.align = 'center'; 
            $scope.page.bgcolor = '#ffffff';
            $scope.page.color = '#ffffff';
            $scope.page.box=false;
            $scope.page.btnname = "";
            $scope.page.btnlink = "";
        }
        // $scope.isSaving = true;
        page['column'] = $scope.row1;
        Special.update(page, function(data){
            $scope.alerts = [{type: 'success', msg: 'Page successfully updated!'}];
        });
        
        $anchorScroll();
    }
    var checkside = function(page) {
    if (page['sidebarleft'] == true && page['sidebarright'] == true) {
        if ($scope.page.sidedataL=='') {
            $anchorScroll();
            $scope.alerts = [{type: 'danger', msg: 'Please add left sidebar'}];
        }else if ($scope.page.sidedataR=='') {
            $anchorScroll();
            $scope.alerts = [{type: 'danger', msg: 'Please add right sidebar'}];
        }else{
            $scope.saveme(page);
        }
    }
    else if (page['sidebarleft'] == true && page['sidebarright'] == false) {
        if ($scope.page.sidedataL=='') {
            $anchorScroll();
            $scope.alerts = [{type: 'danger', msg: 'Please add left sidebar'}];
        }else{
            $scope.saveme(page);
        }
    }
    else if (page['sidebarleft'] == false && page['sidebarright'] == true) {
        if ($scope.page.sidedataR=='') {
            $anchorScroll();
            $scope.alerts = [{type: 'danger', msg: 'Please add right sidebar'}];
        }else{
            $scope.saveme(page);
        }
    }
    else{
        $scope.saveme(page);
    }
}

    $scope.savePage = function(page) {
        $scope.chicken=false;
        // console.log($scope.validslugs)
        if ($scope.validslugs==true) {
            $anchorScroll();
            $scope.alerts = [{type: 'danger', msg: 'Slugs Already Exist!'}];
        }else{
            switch(page['pagetype']){
            case 'Normal':
             if (!page['body']) {
                $anchorScroll();
                $scope.alerts = [{type: 'danger', msg: 'Page Content is required'}];
            }else{
                checkside(page);
            }
            break;
            default:
            if ($scope.row1=="") {
                $anchorScroll();
                $scope.alerts = [{type: 'danger', msg: 'Please fill out columns'}];
            }else{
                for (var i = $scope.row1.length - 1; i >= 0; i--) {
                    if ($scope.row1[i].row.length == 0) {
                        $scope.chicken = true;
                    }
                };

                if ($scope.chicken==true) {
                    $anchorScroll();
                    $scope.alerts = [{type: 'danger', msg: 'Please fill out columns'}];
                }
                else{
                    // console.log("check")
                    checkside(page);
                }

            }
            break;
        }
        }
       
 }

    $scope.preview = function(page){
        page['column'] = $scope.row1;
        console.log(page);
        localStorage["page"] = JSON.stringify(page);
        window.open(Config.BaseURL + "/specialpage/preview");
    }

     $scope.stylepreview = function(page) {
        // console.log(page)
      var modalInstance = $modal.open({
          templateUrl: 'stylepreview',
          size:'lg',
          controller: function($scope, $modalInstance, $state) {
            $scope.page = page;
            $scope.banner = Config.amazonlink + "/uploads/pageimage/" + page.imagethumb;
            $scope.cancel = function () {
                $modalInstance.dismiss();
            };
          }
      });
    }



    ////////////////////SIDEBAR////////////////////////////////////////////////////////
    //left
    $scope.addbtn = true;
    $scope.sidebar = {};
    $scope.addleftsidebar = function(sidebar){
        $scope.page.sidedataL.push({sidebar:sidebar.sidebar, menuID:sidebar.menuID, imglink:sidebar.imglink,
        img:sidebar.img,limittest:sidebar.limittest,label:sidebar.label,
        url:sidebar.url,limitnews:sidebar.limitnews});
        $scope.sidebar = {};
        console.log($scope.page.sidedataL);
    }

    $scope.deleteleft = function(index){
        $scope.page.sidedataL.splice(index, 1);
    }

    $scope.editleft = function(index){
        document.getElementById("left").focus();
        document.getElementById("left").blur();
        $scope.currentEdit = index;
        $scope.addbtn = false;
        if($scope.page.sidedataL[index].sidebar == 'Menu'){
            $scope.sidebar.menuID = $scope.page.sidedataL[index].menuID;
        }
        else if($scope.page.sidedataL[index].sidebar == 'Image'){
            $scope.sidebar.imglink = $scope.page.sidedataL[index].imglink;
            $scope.sidebar.img =  $scope.page.sidedataL[index].img;
        }
        else if($scope.page.sidedataL[index].sidebar == 'Testimonial'){
             $scope.sidebar.limittest =  $scope.page.sidedataL[index].limittest;
        }
        else if($scope.page.sidedataL[index].sidebar == 'Link'){
             $scope.sidebar.label = $scope.page.sidedataL[index].label;
             $scope.sidebar.url = $scope.page.sidedataL[index].url;
        }
        else if($scope.page.sidedataL[index].sidebar == 'News'){
             $scope.sidebar.limitnews = $scope.page.sidedataL[index].limitnews;
        }
        $scope.sidebar.sidebar = $scope.page.sidedataL[index].sidebar;
    }

    $scope.updateleftsidebar = function(index,sidebar){
        if(sidebar.sidebar == 'Menu'){
            $scope.page.sidedataL[index].menuID = sidebar.menuID;
        }
        else if(sidebar.sidebar == 'Image'){
            $scope.page.sidedataL[index].imglink = sidebar.imglink;
            $scope.page.sidedataL[index].img = sidebar.img;
        }
        else if(sidebar.sidebar == 'Testimonial'){
            $scope.page.sidedataL[index].limittest = sidebar.limittest;
        }
        else if(sidebar.sidebar == 'Link'){
            $scope.page.sidedataL[index].label = sidebar.label;
            $scope.page.sidedataL[index].url = sidebar.url;
        }
        else if(sidebar.sidebar == 'News'){
            $scope.page.sidedataL[index].limitnews = sidebar.limitnews;
        }
        $scope.page.sidedataL[index].sidebar = sidebar.sidebar;
        $scope.currentEdit = 99999;
        $scope.addbtn = true;
        $scope.sidebar = {};
    }

    $scope.leftCancel = function(){
        $scope.currentEdit = 99999;
        $scope.addbtn = true;
        $scope.sidebar = {};
    }


    //RIght
    $scope.addbtnright = true;
    $scope.sidebarR = {};
    $scope.removeright = function(sidebarright){
        // if (sidebarright==true) {

        // };
        // $scope.sidebarright = 
    }
    $scope.addrightsidebar = function(sidebarR){
         $scope.page.sidedataR.push({sidebar:sidebarR.sidebar, menuID:sidebarR.menuID, imglink:sidebarR.imglink,
        img:sidebarR.img,limittest:sidebarR.limittest,label:sidebarR.label,
        url:sidebarR.url,limitnews:sidebarR.limitnews});
        $scope.sidebarR = {};
    }

    $scope.deleteright = function(index){
        $scope.page.sidedataR.splice(index, 1);
    }

    $scope.editright = function(index){
        document.getElementById("right").focus();
        document.getElementById("right").blur();
        $scope.currentEditright = index;
        $scope.addbtnright = false;
        if($scope.page.sidedataR[index].sidebar == 'Menu'){
            $scope.sidebarR.menuID = $scope.page.sidedataR[index].menuID;
        }
        else if($scope.page.sidedataR[index].sidebar == 'Image'){
            $scope.sidebarR.imglink = $scope.page.sidedataR[index].imglink;
            $scope.sidebarR.img =  $scope.page.sidedataR[index].img;
        }
        else if($scope.page.sidedataR[index].sidebar == 'Testimonial'){
             $scope.sidebarR.limittest =  $scope.page.sidedataR[index].limittest;
        }
        else if($scope.page.sidedataR[index].sidebar == 'Link'){
             $scope.sidebarR.label = $scope.page.sidedataR[index].label;
             $scope.sidebarR.url = $scope.page.sidedataR[index].url;
        }
        else if($scope.page.sidedataR[index].sidebar == 'News'){
             $scope.sidebarR.limitnews = $scope.page.sidedataR[index].limitnews;
        }
        $scope.sidebarR.sidebar = $scope.page.sidedataR[index].sidebar;
    }

    $scope.updaterightsidebar = function(index,sidebarR){
        if(sidebarR.sidebar == 'Menu'){
            $scope.page.sidedataR[index].menuID = sidebarR.menuID;
        }
        else if(sidebarR.sidebar == 'Image'){
            $scope.page.sidedataR[index].imglink = sidebarR.imglink;
            $scope.page.sidedataR[index].img = sidebarR.img;
        }
        else if(sidebarR.sidebar == 'Testimonial'){
            $scope.page.sidedataR[index].limittest = sidebarR.limittest;
        }
        else if(sidebarR.sidebar == 'Link'){
            $scope.page.sidedataR[index].label = sidebarR.label;
            $scope.page.sidedataR[index].url = sidebarR.url;
        }
        else if(sidebarR.sidebar == 'News'){
            $scope.page.sidedataR[index].limitnews = sidebarR.limitnews;
        }
        $scope.page.sidedataR[index].sidebar = sidebarR.sidebar;
        $scope.currentEditright = 99999;
        $scope.addbtnright = true;
        $scope.sidebarR = {};
    }

    $scope.rightCancel = function(){
        $scope.currentEditright = 99999;
        $scope.addbtnright = true;
        $scope.sidebarR = {};
    }


    ////SIDEBAR/////////////////////////////////////////////////////////////////////////////////////////////
    $scope.alertss = [];

    $scope.closeAlerts = function (index) {
            $scope.alertss.splice(index, 1);
    };

      /////START SPECIAL PAGE
  $scope.rowdelitem = function(index){
         var modalInstance = $modal.open({
            templateUrl: 'deleterow.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                $scope.ok = function(){
                    rowdel(index);
                     $modalInstance.dismiss('cancel');
                }
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
              };
          }
      })
  }

  var rowdel = function(index)
  {
       $scope.row1.splice(index,1);
  }


    $scope.rownewitem = function($event){
        $event.preventDefault();
        $scope.row1.push({row:[],columns:"",column:"",colcount:""});
        // console.log($scope.row1)
    }

    var selectedcol = '';
    $scope.column = [];
    $scope.page.colcount = [];
    $scope.page.row= [];
    var column = function(index,column){
        if (column != undefined) {
             $scope.row1[index].columns=column;
             $scope.row1[index].column=column;
        }else{
             $scope.row1[index].columns=selectedcol;
             $scope.row1[index].column=selectedcol;
        }

        $scope.row1[index].colcount=selectedcol;
        // console.log($scope.row1[index].columns)
    }


 $scope.delcolumn = function(countcol,index,col){
    $scope.row1[index].colcount = countcol - 1;
    $scope.row1[index].columns = countcol - 1;
    $scope.row1[index].column = countcol - 1;
    $scope.row1[index].row.splice(col,1);
}
    //MODULE 
    var selectedmod = '';


    
    // $scope.rowindex = [];
    $scope.insertmodule = function(col,index){
        var modalInstance = $modal.open({
            templateUrl: 'module.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                if (col != undefined) {
                    col = parseInt(col+1);
                }
                else{
                       col = 0+1;
                }
          
                $scope.setmod = function(mod){
                    if(mod=="TEXT"){
                        textmodule(col,index);
                        $modalInstance.dismiss('cancel');
                    }
                    else if(mod=="IMAGE"){
                        media('featured','banner',index,col);
                        $modalInstance.dismiss('cancel');
                    }
                    else if(mod=="TESTIMONIAL"){
                        testimonialmodule(col,index);
                        $modalInstance.dismiss('cancel');
                    }
                    else if(mod=="CONTACT"){
                         contactmodule(col,index)
                        $modalInstance.dismiss('cancel');
                    }
                    else if(mod=="NEWS"){
                        newsmodule(col,index);
                        $modalInstance.dismiss('cancel');
                    }
                    else if(mod=="DIVIDER"){
                        dividermodule(col,index);
                        $modalInstance.dismiss('cancel');
                    }
                    else if(mod=="BUTTON"){
                        buttonmodule(col,index);
                        $modalInstance.dismiss('cancel');
                    }
                }
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
              };
          }
      })
}


    ////NEWS MODULE 
    var newslimit4 = '',
        allnewslimit = '';


    var newsmodule = function(col,index,view,popular,limit,childindex){

        var modalInstance = $modal.open({
            templateUrl: 'modulenews.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                if(view==undefined){
                    $scope.viewed = 'false';
                }
                else if(view!=undefined){
                    $scope.viewed = view;
                }
                else if(popular==undefined){
                    $scope.popular = 'false';
                }
                else if(popular!=undefined){
                    $scope.popular = popular;
                }
                $scope.limit = limit;
                $scope.setoption = function(view,popular,limit){
                   
                    selectedmod = 'NEWS';
                    allnewslimit = limit;
                    if (childindex != undefined) {
                       editrow(index,childindex,'NEWS');
                   }
                   else{
                       allrow(col,index,'NEWS');
                   }
                   selectedcol = col;
                   column(index);
                   $modalInstance.dismiss('cancel');
               }
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
              };
          }
      })
}

$scope.editnewsmodule = function(col,index,view,popular,limit,childindex){
   newsmodule(col,index,view,popular,limit,childindex);
}


    ////Divider MODULE 
    var allcolor = '',
        allheight = '';

    var dividermodule = function(col,index,height,color,childindex){
        var modalInstance = $modal.open({
            templateUrl: 'moduledivider.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                $scope.height = height;
                if(color==undefined){
                    $scope.color = '#ffffff';
                }
                else if(color!=undefined){
                    $scope.color = color;
                }
                $scope.setoption = function(height,color){
                    
                    selectedmod = 'DIVIDER';
                    allcolor = color;
                    allheight = height;
                    // dividercontent(col,index,column);
                    // module(col,index);
                    selectedcol = col;
                   
                      if (childindex != undefined) {
                           editrow(index,childindex,'DIVIDER');
                       }
                       else{
                          column(index);
                           allrow(col,index,'DIVIDER');
                       }
                    $modalInstance.dismiss('cancel');
                }
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
              };
          }
      })
}

$scope.editdividermodule = function(col,index,height,color,childindex){
   dividermodule(col,index,height,color,childindex);
}

////Button MODULE 
    var allbtnname = '',
        allbgcolor = '',
        allfcolor = '',
        allpadtop = '',
        allpadside = '',
        allfont = '',
        allposition = '',
        allbtnlink = '';

    var buttonmodule = function(col,index,counter,childindex){
        var modalInstance = $modal.open({
            templateUrl: 'modulebutton.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                $scope.button = [];
                if(counter==undefined){
                    $scope.button.bgcolor = '#5cce5f';
                    $scope.button.fcolor = '#ffffff';
                    $scope.button.padtop = 5;
                    $scope.button.padside = 25;
                    $scope.button.position = 'Center';
                    $scope.button.font = 16;
                }else{
                     $scope.button.btnname = counter.colbtnname;
                    $scope.button.btnlink = counter.colbtnlink;
                    $scope.button.bgcolor = counter.colbgcolor;
                    $scope.button.fcolor = counter.colfcolor;
                    $scope.button.padtop = counter.colpadtop;
                    $scope.button.padside = counter.colpadside;
                    $scope.button.font = counter.colfont;
                    $scope.button.position = counter.colposition;
                }
                $scope.setoption = function(button){
                    selectedmod = 'BUTTON';
                    allbtnname = button.btnname;
                    allbtnlink = button.btnlink;
                    allbgcolor = button.bgcolor;
                    allfcolor = button.fcolor;
                    allpadside = button.padside;
                    allpadtop = button.padtop;
                    allfont = button.font;
                    allposition = button.position;
                    selectedcol = col;
                      if (childindex != undefined) {
                           editrow(index,childindex,'BUTTON');
                       }
                       else{
                          column(index);
                          allrow(col,index,'BUTTON');
                       }
                    $modalInstance.dismiss('cancel');
                }
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
              };
          }
      })
}

$scope.editbuttonmodule = function(col,index,counter,childindex){
   buttonmodule(col,index,counter,childindex);
}

    ////TEXT MODULE 
    var alltext = '';


    var allrow = function (col,index,columnname) {
        $scope.s3link = Config.amazonlink;
        $scope.directory = 'pageimage';
            if (columnname=='TEXT') {
                 $scope.row1[index].row.push({col:selectedmod,colcontent:alltext});
            }
            else if (columnname=='IMAGE'){
                $scope.row1[index].row.push({col:selectedmod,colimg:allimg,colimglink:allimglink});
            }
             else if (columnname=='TESTIMONIAL'){
                $scope.row1[index].row.push({col:selectedmod,coltestimonial:testi,
                    coltestimonialcat:testicat});
            }
            else if (columnname=='CONTACT'){
                $scope.row1[index].row.push({col:selectedmod,coltitle:contacttitle,colphone:contactphone,
                    colemail:contactemail,colhours:contacthours,coladdress:contactaddress,str:str});
            }
             else if (columnname=='NEWS'){
                $scope.row1[index].row.push({col:selectedmod,colnewslimit:allnewslimit});
            }
             else if (columnname=='DIVIDER'){
                $scope.row1[index].row.push({col:selectedmod,colheight:allheight,colcolor:allcolor});
            }
            else if (columnname=='BUTTON'){
                $scope.row1[index].row.push({col:selectedmod,colbtnname:allbtnname,colbtnlink:allbtnlink,
                    colbgcolor:allbgcolor,colfcolor:allfcolor,colpadtop:allpadtop,colpadside:allpadside
                    ,colfont:allfont,colposition:allposition});
            }
        // console.log($scope.row1)
    }

     var editrow = function (index,childindex,columnname) {
            if (columnname=='TEXT') {
                 $scope.row1[index].row[childindex].col = selectedmod;
                 $scope.row1[index].row[childindex].colcontent = alltext;
            }
            else if (columnname=='IMAGE'){
                 $scope.row1[index].row[childindex].col = selectedmod;
                $scope.row1[index].row[childindex].colimg = allimg;
                $scope.row1[index].row[childindex].colimglink = allimglink;
            }
             else if (columnname=='TESTIMONIAL'){
                 $scope.row1[index].row[childindex].col = selectedmod;
                $scope.row1[index].row[childindex].coltestimonial = testi;
                $scope.row1[index].row[childindex].coltestimonialcat = testicat;
            }
            else if (columnname=='CONTACT'){
                $scope.row1[index].row[childindex].col = selectedmod;
               $scope.row1[index].row[childindex].coltitle = contacttitle;
               $scope.row1[index].row[childindex].colphone = contactphone;
               $scope.row1[index].row[childindex].colemail = contactemail;
               $scope.row1[index].row[childindex].colhours = contacthours;
               $scope.row1[index].row[childindex].coladdress = contactaddress;
               $scope.row1[index].row[childindex].str = str;
            }
             else if (columnname=='NEWS'){
                 $scope.row1[index].row[childindex].col = selectedmod;
                $scope.row1[index].row[childindex].colnewslimit = allnewslimit;
            }
             else if (columnname=='DIVIDER'){
                 $scope.row1[index].row[childindex].col = selectedmod;
                  $scope.row1[index].row[childindex].colheight = allheight;
                   $scope.row1[index].row[childindex].colcolor = allcolor;
            }
            else if (columnname=='BUTTON'){
               $scope.row1[index].row[childindex].col = selectedmod;
               $scope.row1[index].row[childindex].colbtnname = allbtnname;
               $scope.row1[index].row[childindex].colbtnlink = allbtnlink;
               $scope.row1[index].row[childindex].colbgcolor = allbgcolor;
               $scope.row1[index].row[childindex].colfcolor = allfcolor;
               $scope.row1[index].row[childindex].colpadtop = allpadtop;
               $scope.row1[index].row[childindex].colpadside = allpadside;
               $scope.row1[index].row[childindex].colfont = allfont;
               $scope.row1[index].row[childindex].colposition = allposition;
            }
        
        // console.log($scope.row1);
    }

    var textmodule = function(col,index,childindex){
 
        var modalInstance = $modal.open({
            templateUrl: 'moduletext.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                $scope.gallery = function(){
                    media('content');
                }
                $scope.settext = function(text){
                    selectedmod = 'TEXT';
                    alltext = text;
                    selectedcol = col;
                    column(index);
                    if (childindex != undefined) {
                       editrow(index,childindex,'TEXT');
                   }
                   else{
                       allrow(col,index,'TEXT');
                   }
                    // textcontent(col,index);
                    // module(col,index);
                    column(index);
                    $modalInstance.dismiss('cancel');
                }
                $scope.cancel = function() {

                  $modalInstance.dismiss('cancel');
              };
          }
      })
    }

    $scope.edittextmodule = function(col,index,content,childindex){
        var modalInstance = $modal.open({
            templateUrl: 'moduletext.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                $scope.textarea = content;
                $scope.gallery = function(){
                    media('content');
                }
                $scope.settext = function(text){
                    selectedmod = 'TEXT';
                    alltext = text;
                    editrow(index,childindex,'TEXT');
                    $modalInstance.dismiss('cancel');
                }
                $scope.cancel = function(){
                  $modalInstance.dismiss('cancel');
              };
          }
      })
    }

     //Image Module

    var allimg = '', 
        allimglink = '';


    var imagemodule = function(col,index,imageset,childindex,linkset){
        var modalInstance = $modal.open({
            templateUrl: 'moduleimage.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                $scope.s3link = Config.amazonlink;
                $scope.gallery = function(){
                    media('featured','banner',index,col,childindex);
                    $modalInstance.dismiss('cancel');
                }
                $scope.setimg = function(image,link){
                    
                    selectedmod = 'IMAGE';
                    allimg = image;
                    allimglink = link;
                    if (childindex != undefined) {
                     editrow(index,childindex,'IMAGE');
                 }
                 else{
                     allrow(col,index,'IMAGE');
                 }
                 selectedcol = col;
                 column(index);
                 $modalInstance.dismiss('cancel');
                }
                $scope.colimg = imageset;
                $scope.link = linkset;
                $scope.cancel = function() {

                  $modalInstance.dismiss('cancel');
              };
          }
      })
}



$scope.editimagemodule = function(col,index,images,childindex,link){
    imagemodule(col,index,images,childindex,link);
}

 ////TESTIMONIAL MODULE 
    var testi = '',
        testicat = '';

    var testimonialmodule = function(col,index,childindex){
        var modalInstance = $modal.open({
            templateUrl: 'moduletestimonial.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                $scope.category ='';
                $scope.settestimonial = function(text,cat){
                    testi = text;
                    testicat = cat;
                    selectedmod = 'TESTIMONIAL';
                    // testimonialcontent(col,index);
                    // module(col,index);
                     selectedcol = col;
                    column(index);
                    if (childindex != undefined) {
                           editrow(index,childindex,'TESTIMONIAL');
                       }
                       else{
                           allrow(col,index,'TESTIMONIAL');
                       }
                      
                    $modalInstance.dismiss('cancel');
                }
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
              };

              Special.get(function(pages){
                $scope.pages = pages;
            });
          }
      })
}


$scope.edittestimonialmodule = function(col,index,content,cat,childindex){
        var modalInstance = $modal.open({
            templateUrl: 'moduletestimonial.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
               $scope.text = content;
                Special.get(function(pages){
                   $scope.pages = pages;
                });
                $scope.category = cat;
                $scope.settestimonial = function(text,cat){
                    selectedmod = 'TESTIMONIAL';
                    testi = text;
                    testicat = cat;
                    editrow(index,childindex,'TESTIMONIAL');
                    $modalInstance.dismiss('cancel');
                }
                $scope.cancel = function() {
                 
                  $modalInstance.dismiss('cancel');
              };
  }
})
}


    ////Contact MODULE 
    var contacttitle = '',
        contactemail = '',
        contacthours = '',
        contactaddress = '',
        str = '',
        contactphone = '';


    var contactmodule = function(col,index,counter,childindex){
        var modalInstance = $modal.open({
            templateUrl: 'modulecontact.html',
            size: 'lg',
            controller: function($scope,$modalInstance,Config){
                if (counter==undefined) {
                    Special.getinfo(function(data) {
                      $scope.contact = data[0];
                    });
                }else{
                    $scope.contact = [];
                    $scope.contact.title = counter.coltitle;
                    $scope.contact.phone = counter.colphone;
                    $scope.contact.email = counter.colemail;
                    $scope.contact.hours = counter.colhours;
                    $scope.contact.address = counter.coladdress;
                }
                 
                $scope.ok = function(contact){
                    selectedmod = 'CONTACT';
                    contacttitle = contact.title;
                    contactemail = contact.email;
                    contacthours = contact.hours;
                    contactaddress = contact.address;
                    contactphone = contact.phone;
                    str = contact.address;
                    str = str.split('\n');
                    if (childindex != undefined) {
                       editrow(index,childindex,'CONTACT');
                   }
                   else{
                       allrow(col,index,'CONTACT');
                   }
                   selectedcol = col;
                   column(index);
                   $modalInstance.dismiss('cancel');
               }
                $scope.cancel = function() {
                  $modalInstance.dismiss('cancel');
              };
          }
      })
}

$scope.editcontactmodule = function(col,index,counter,childindex){
   contactmodule(col,index,counter,childindex);
}


    /////END SPECIAL PGE
    $scope.page.leftimg=[];
    $scope.media = function(type, thumb,sidebar){
        // $scope.page.leftimg=[];
        var modalInstance = $modal.open({
            templateUrl: 'mediagallery.html',
            controller: function($scope, $modalInstance,Config, type) {
                $scope.imageloader=false;
                $scope.directory = 'pageimage';
                $scope.videoenabled=false;
                $scope.mediaGallery = 'images';
                $scope.imagecontent=true;
                $scope.invalidvideo = false;
                $scope.currentSelected = '';
                $scope.currentDeleting = '';
                $scope.type = type;
                $scope.s3link = Config.amazonlink;
                // console.log($scope.s3link);

                $scope.set = function(id){
                    $scope.currentSelected = id;
                    $scope.contentvideo = id;
                    if(type=='content'){
                        // $scope.copy();
                    }
                }

                $scope.returnImageThumb = function(){
                    return function (item) {
                        if(item){
                            return Config.amazonlink + "/uploads/" + $scope.directory + "/" + item;
                        }else{
                            return item;
                        }
                    };
                }

                var loadimages = function() {
                    Special.loadimages(function(data){
                        // console.log(data);
                        $scope.imagelist = data;
                        $scope.imagelength = data.length;
                    });
                }

                loadimages();

                $scope.upload = function (files)
                {
                    $scope.alertss = [];
                    var filename
                    var filecount = 0;
                    if (files && files.length)
                    {
                        $scope.imageloader=true;
                        $scope.imagecontent=false;

                        for (var i = 0; i < files.length; i++)
                        {
                            var file = files[i];

                            if (file.size >= 2000000)
                            {
                                $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big. Maximum size is 2MB'});
                                filecount = filecount + 1;

                                if(filecount == files.length)
                                {
                                    $scope.imageloader=false;
                                    $scope.imagecontent=true;
                                }


                            }
                            else

                            {
                                var promises;
                                var f = file.name;
                                var fname=f.replace(/\s+/g, "-");
                                promises = Upload.upload({

                                    url: Config.amazonlink, //S3 upload url including bucket name
                                    method: 'POST',
                                    transformRequest: function (data, headersGetter) {
                                        //Headers change here
                                        var headers = headersGetter();
                                        delete headers['Authorization'];
                                        return data;
                                    },
                                    fields : {
                                        key: 'uploads/'+$scope.directory+'/' + fname, // the key to store the file on S3, could be file name or customized
                                        AWSAccessKeyId: Config.AWSAccessKeyId,
                                        acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                                        policy: Config.policy, // base64-encoded json policy (see article below)
                                        signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                        "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                    },
                                    file: file
                                })
promises.then(function(data){

    filecount = filecount + 1;
    filename = data.config.file.name;
    var fn = filename;
    var finalname=fn.replace(/\s+/g, "-");
    var fileout = {
        'imgfilename' : finalname
    };
    Special.saveimage(fileout, function(data){
        loadimages();
        if(filecount == files.length)
        {
            $scope.imageloader=false;
            $scope.imagecontent=true;
        }
    });

});


}


}
}

};

$scope.closeAlerts = function (index) {
        $scope.alertss.splice(index, 1);
    };

$scope.deletenewsimg = function (dataimg, $event)
{
    var fileout = {
        'imgfilename' : dataimg
    };
    Special.deleteimage(fileout, function(data) {
        loadimages();
    });
    $event.stopPropagation();
}

$scope.copy = function() {
    // console.log($scope.contentvideo);
    var text = '';
    if($scope.contentvideo.filename){
        text = Config.amazonlink + '/uploads/'+$scope.directory+'/' + $scope.contentvideo.filename;
    }
    // var pp = window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
    // if(pp != "" && pp !== null) {
    //     $modalInstance.dismiss('cancel');
    // }
    var oEditor = CKEDITOR.instances.normaleditor;
    var image = $scope.s3link+'/uploads/pageimage/'+$scope.contentvideo.filename;
    var html = "<img src="+ image +" style='width:300px;height:200px'>";

    var newElement = CKEDITOR.dom.element.createFromHtml( html, oEditor.document );
    oEditor.insertElement( newElement );
    $modalInstance.dismiss('cancel');
}

$scope.ok = function(category) {
    $modalInstance.close($scope.contentvideo);
}

$scope.cancel = function() {
    $modalInstance.dismiss('cancel');
}
},
resolve: {
    type: function(){
        return type;
    }
},
windowClass: 'xxx-modal-window'
}).result.then(function(res) {
    console.log("============================");
    if(res.filename) {
        if(thumb == 'banner'){
            if(sidebar == 'left'){
                $scope.sidebar.img = res.filename;
            }else{
                $scope.sidebarR.img = res.filename;
            }
        }else if(thumb == 'thumb'){
            $scope.page.imagethumb = res.filename;
        }
    }
});
}



var media = function(type, thumb,index,col,childindex){
    var modalInstance = $modal.open({
        templateUrl: 'mediagallery.html',
        controller: function($scope, $modalInstance,Config, type) {
            $scope.imageloader=false;
            $scope.directory = 'pageimage';
            $scope.videoenabled=false;
            $scope.mediaGallery = 'images';
            $scope.imagecontent=true;
            $scope.invalidvideo = false;
            $scope.currentSelected = '';
            $scope.currentDeleting = '';
            $scope.type = type;
            $scope.s3link = Config.amazonlink;
            // console.log($scope.s3link);

            $scope.set = function(id){
                $scope.currentSelected = id;
                $scope.contentvideo = id;
                if(type=='content'){
                    $scope.copy();
                }
            }

            $scope.returnImageThumb = function(){
                return function (item) {
                    if(item){
                        return Config.amazonlink + "/uploads/" + $scope.directory + "/" + item;
                    }else{
                        return item;
                    }
                };
            }

            var loadimages = function() {
                Special.loadimages(function(data){
                    // console.log(data);
                    $scope.imagelist = data;
                    $scope.imagelength = data.length;
                });
            }

            loadimages();

            $scope.upload = function (files)
            {
                $scope.alertss = [];
                var filename
                var filecount = 0;
                if (files && files.length)
                {
                    $scope.imageloader=true;
                    $scope.imagecontent=false;

                    for (var i = 0; i < files.length; i++)
                    {
                        var file = files[i];

                        if (file.size >= 2000000)
                        {
                            $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big. Maximum size is 2MB'});
                            filecount = filecount + 1;

                            if(filecount == files.length)
                            {
                                $scope.imageloader=false;
                                $scope.imagecontent=true;
                            }


                        }
                        else

                        {
                            var promises;
                            var f = file.name;
                            var fname=f.replace(/\s+/g, "-");
                            promises = Upload.upload({

                                    url: Config.amazonlink, //S3 upload url including bucket name
                                    method: 'POST',
                                    transformRequest: function (data, headersGetter) {
                                        //Headers change here
                                        var headers = headersGetter();
                                        delete headers['Authorization'];
                                        return data;
                                    },
                                    fields : {
                                        key: 'uploads/'+$scope.directory+'/' + fname, // the key to store the file on S3, could be file name or customized
                                        AWSAccessKeyId: Config.AWSAccessKeyId,
                                        acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                                        policy: Config.policy, // base64-encoded json policy (see article below)
                                        signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                        "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                    },
                                    file: file
                                })
promises.then(function(data){

    filecount = filecount + 1;
    filename = data.config.file.name;
    var fn = filename;
    var finalname=fn.replace(/\s+/g, "-");
    var fileout = {
        'imgfilename' : finalname
    };
    Special.saveimage(fileout, function(data){
        loadimages();
        if(filecount == files.length)
        {
            $scope.imageloader=false;
            $scope.imagecontent=true;
        }
    });

});


}


}
}

};
$scope.closeAlerts = function (index) {
        $scope.alertss.splice(index, 1);
    };
$scope.deletenewsimg = function (dataimg, $event)
{
    var fileout = {
        'imgfilename' : dataimg
    };
    Special.deleteimage(fileout, function(data) {
        loadimages();
    });
    $event.stopPropagation();
}

$scope.copy = function() {
    // console.log($scope.contentvideo);
    var text = '';
    if($scope.contentvideo.filename){
        text = Config.amazonlink + '/uploads/'+$scope.directory+'/' + $scope.contentvideo.filename;
    }
    var pp = window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
    if(pp != "" && pp !== null) {
        $modalInstance.dismiss('cancel');
    }
}

$scope.ok = function(category) {
    $modalInstance.close($scope.contentvideo);
}

$scope.cancel = function() {
    $modalInstance.dismiss('cancel');
}
},
resolve: {
    type: function(){
        return type;
    }
},
windowClass: 'xxx-modal-window'
}).result.then(function(res) {

    if(res.filename) {
        if(thumb == 'banner'){
            $scope.colimg = res.filename;
            imagemodule(col,index,res.filename,childindex);  
        }
    }
});
}


}) //end


