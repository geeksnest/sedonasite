'use strict';

/* Controllers */

app.controller('menuCreatorCtrl', function ($scope, $http, $modal, $state, $sce, $q, $timeout, Createmenu, Config, Upload, $filter) {

$scope.keyword=null;
$scope.loading = false;
var keyword = null;
var num = 10;
var off = 1;

  $scope.menus = [];

  $scope.draggableObjects = [
  {name: 'one'},
  {name: 'two'},
  {name: 'three'},
  {name: 'four'},
  {name: 'five'}
  ];
  $scope.onDropComplete = function (index, obj, evt) {
    var otherObj = $scope.draggableObjects[index];
    var otherIndex = $scope.draggableObjects.indexOf(obj);
    $scope.draggableObjects[index] = obj;
    $scope.draggableObjects[otherIndex] = otherObj;
  }

 
  $scope.onDropMenu = function (index, obj, evt) {
    var otherObj = $scope.menus[index];
    var otherIndex = $scope.menus.indexOf(obj);
    $scope.menus[index] = obj;
    $scope.menus[otherIndex] = otherObj;
  }
  //////////////////////////////////////////////////////
  $scope.ontitle = function convertToSlug(Text)
  {

    var text1 = Text.replace(/[^\w ]+/g,'');
    $scope.shortCode = angular.lowercase(text1.replace(/ +/g,'-'));

  }

  var loadMenuList = function (num, off, keyword) {    
    Createmenu.menulist(num, off, keyword,function(data){
      $scope.menus = data.data; 
        $scope.maxSize = 10;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;

    });
  }
  loadMenuList(num, off, keyword);
  
   $scope.search = function (keyword) {
        var off = 0;
        $scope.keyword = keyword;
        loadMenuList(num, off, keyword);
        $scope.searchtext='';
    }

    $scope.clear = function(){
        $scope.keyword=null;
        loadMenuList(num, off, keyword);
    }

  $scope.setPage = function (pageNo) {
        loadMenuList(num, pageNo, keyword);
     
    };

  $scope.addmenu = function(menudata)
  {  
    $scope.alerts = []; 
    menudata['shortCode'] = $scope.shortCode;
    
    Createmenu.add(menudata,function(data){
      if(data.hasOwnProperty('error')){
        if(data.error.hasOwnProperty('existTitle')){
          $scope.alerts =[{type: 'danger', msg: 'Menu already exists.'}];
          $scope.isSaving = false;
          $anchorScroll();
        }
      }else {
        $scope.isSaving = false;
        $scope.alerts =[{type: 'success', msg: 'Menu successfully saved!'}];
        $scope.data.newmenu = null; 
        loadMenuList(num, off, keyword);       
      }

    });

  }
  
  $scope.deletemenu = function(menuID) {
    var modalInstance = $modal.open({
      templateUrl: 'deletemenu.html',
      controller: function($scope, $modalInstance, menuID) {
        $scope.ok = function() {
          Createmenu.delete(menuID, function(data){
            loadMenuList(num, off, keyword); 
            $modalInstance.close();
          });
        };
        $scope.cancel = function() {
          $modalInstance.dismiss('cancel');
        };

      },
      resolve: {
        menuID: function() {
          return menuID
        }
      }
    });
  }
  
  //EDIT MENU
  $scope.editmenu = function(menuID) {
    var modalInstance = $modal.open({
      templateUrl: 'editMenu.html',
      windowClass: 'editmenu-modal-window',
      controller: function($scope, $modalInstance, menuID) {
        
        $scope.sub = {};     


    var slugstorage = "";
    $scope.onpagetitle = function convertToSlug(Text) {if(Text != null && $scope.editedslug == false && $scope.editslug == false){$scope.page.slugs = Text.replace(/\s+/g, '-').toLowerCase();}}
    $scope.onslugs = function(Text){ if(Text != null){ $scope.menu.name = Text.replace(/\s+/g, '-').toLowerCase(); }}
    $scope.editpageslug = function(){
        $scope.editslug = true;
        slugstorage = $scope.menu.name;
    }

    $scope.cancelpageslug = function(title) {
        if(title != null)
        {
            $scope.editedslug = false;
            $scope.menu.name = slugstorage;
        }else {
            $scope.menu.name = '';
            $scope.editedslug = false;
        }
        $scope.editslug = false;
    }

    $scope.setslug = function(slug){
        $scope.editedslug = true;
        $scope.editslug = false;
        if(slug != null)
        {
            $scope.menu.name = slug.replace(/\s+/g, '-').toLowerCase();

        }

    }

    $scope.clearslug = function(title){
        if(title != null)
        {
            $scope.editedslug = false;
            $scope.menu.name = title.replace(/\s+/g, '-').toLowerCase();
        }else {
            $scope.menu.name = '';
            $scope.editedslug = false;
        }
    }
    $scope.close = function () {
      $scope.path="close";
      $scope.path = false;
      $scope.sub.subname = "";
      $scope.sub.linktext = "";
    }

    $scope.changename = false;

        var loadSubMenuList = function () {
          Createmenu.submenulist(menuID,function(data){
            $scope.submenu = data.data; 
            $scope.child1 = data.child1;
            $scope.child2 = data.child2;
            $scope.child3 = data.child3;
            $scope.child4 = data.child4;
            $scope.menu = data.menuname[0]; 
            console.log(data.menuname[0]);
            $scope.menulength = data.data.length;   
          });
        }

        loadSubMenuList();

       
        $scope.cancel = function() {
          $modalInstance.dismiss('cancel');
        };

        $scope.deletesubmenu = function(submenuID) {
          var modalInstance = $modal.open({
            templateUrl: 'deletemenu.html',
            controller: function($scope, $modalInstance, submenuID) {
              $scope.ok = function() {
                Createmenu.deletesubmenu(submenuID, function(data){
                  loadSubMenuList(); 
                  $modalInstance.close();
                });
              };
              $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
              };

            },
            resolve: {
              submenuID: function() {
                return submenuID
              }
            }
          });
        }


        $scope.expand = function(){$('.dd').nestable('expandAll');}
        $scope.collapse = function(){$('.dd').nestable('collapseAll');}


        $scope.save = function(){
          var menuStruc = angular.toJson($('.dd').nestable('serialize'));
          var menu = {};
          menu = menuStruc;
          var name = $scope.menu.name;      
          Createmenu.savemenu(menu,menuID,name, function(data){
            loadSubMenuList();
             loadMenuList(num, off, keyword);
            $modalInstance.dismiss('cancel');
          });
 
        }

//MENU ITEM
        $scope.addmenuitem = function () {
          var modalInstance = $modal.open({
            templateUrl: 'addMenuItem.html',
            windowClass: 'editmenu-modal-window',
            controller: function($scope, $modalInstance) {
               $scope.sub = {};   
              $scope.link = [];
                  $scope.changename = false;
                   $scope.option = function(link, path, slugs, newtitle) {

                link['slugs'] = slugs;
                link['subname'] = newtitle;
                $scope.savedisable = false;
                if(path == 'null' || path == null){
                  var concat = Config.BaseURL + "/" + slugs;
                }else{
                  var concat = Config.BaseURL + "/" + angular.lowercase(path) + "/" + slugs;
                }
                
                link.subname = newtitle;
                link.linktext = concat;
                $scope.sub.subname = newtitle;
                $scope.sub.linktext = concat;
                $scope.link.subname = newtitle;
                $scope.link.linktext = concat;
               
              };
               $scope.onslugs = function(Text){ 
                if(Text != null){ $scope.sub.linktext = Text.replace(/\s+/g, '-').toLowerCase();
                $scope.link.linktext = Text.replace(/\s+/g, '-').toLowerCase();}
              }
             
              $scope.text = function(link) {
                link.linkoption = 'false';
                link.linkoption = false;
                if(link.linktext == ''){
                  $scope.savedisable = true;
                }else{
                  $scope.savedisable = false;
                }
              };

          
                      var loadpageList = function () {    
                Createmenu.pagelist(function(data){
                  $scope.pages = data.data; 
                   
                });
              }
              loadpageList();

              var loadcategoryList = function () {    
                Createmenu.categorylist(function(data){
                  $scope.category = data.data; 
                  $scope.pagecategory = data.datapage;
                });
              }
              loadcategoryList();

              var loadtagList = function () {    
                Createmenu.taglist(function(data){
                  $scope.tags = data.data; 
                
                });
              }
              loadtagList();

              var loadpostList = function () {    
                Createmenu.postlist(function(data){
                  $scope.news = data.data; 
                  
                });
              }
              loadpostList();

              var loadauraList = function () {    
                Createmenu.auralist(function(data){
                  $scope.aura = data; 
                     
                });
              }
              loadauraList();

              $scope.pathtype = function(type) {
                $scope.path = type;                
              };

              $scope.addsub = function(subdata) {
                $scope.alerts = [];
                $scope.closeAlert = function(index) {
                  $scope.alerts.splice(index, 1);
                };
                subdata['menuID'] = menuID;
                subdata['path'] = $scope.path;
                subdata['newtab'] = $scope.link.newtab;
                $scope.path="close";
                $scope.path = false;

                var menuStruc = angular.toJson($('.dd').nestable('serialize'));
                var menu = {};
                menu = menuStruc; 
                Createmenu.submenulist(menuID,function(data){
                  var namemenu = data.menuname[0].name;
                  Createmenu.savemenu(menu,menuID,namemenu, function(data){
                    Createmenu.addsub(subdata, function(data){
                      loadSubMenuList();
                      loadMenuList(num, off, keyword);
                      $scope.alerts = [{type: 'success', msg: 'Menu Item successfully added!'}];
                    });
                  });   
                });   
                
                $scope.link.newtab = false;
                 $scope.sub = {};
              };
                $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
              };
                  $scope.save = function() {
                $modalInstance.dismiss('cancel');
              };
              $scope.newtabbot = function(link) {             
                if(link.newtab == 'true'){
                  link.newtab = false;
                }
                link['newtab'] = link.newtab;
           
                $scope.savedisable = false;
              };

      }
    })

}

        $scope.addsubmenu = function (submenuID) {
                  var modalInstance = $modal.open({
            templateUrl: 'addSubMenu.html',
            windowClass: 'editmenu-modal-window',
            controller: function($scope, $modalInstance, submenuID) {
                $scope.savedisable = true;
              $scope.submenuID = submenuID;
              $scope.changename = false; 
              $scope.link = {};
           
              var loadSubMenuName = function () {  
                Createmenu.submenuname(submenuID,function(data){
                  $scope.origsubname = data.data[0].subname;
                  $scope.origmenuID = data.data[0].menuID;
                });
              }
              loadSubMenuName();

              var loadpageList = function () {    
                Createmenu.pagelist(function(data){
                  $scope.pages = data.data; 
                      
                });
              }
              loadpageList();

              var loadcategoryList = function () {    
                Createmenu.categorylist(function(data){
                  $scope.category = data.data; 
                  $scope.pagecategory = data.datapage;
                });
              }
              loadcategoryList();

              var loadtagList = function () {    
                Createmenu.taglist(function(data){
                  $scope.tags = data.data;
                });
              }
              loadtagList();

              var loadpostList = function () {    
                Createmenu.postlist(function(data){
                  $scope.news = data.data;      
                });
              }
              loadpostList();

              var loadauraList = function () {    
                Createmenu.auralist(function(data){
                  $scope.aura = data;    
                });
              }
              loadauraList();

              $scope.pathtype = function(type) {
                $scope.path = type;
              };

               $scope.option = function(link, path, slugs, newtitle) {
                link['slugs'] = slugs;
                link['subname'] = newtitle;
                $scope.savedisable = false;
                if(path == 'null' || path==null){
                  var concat = Config.BaseURL + "/" + slugs;
                }else{
                  var concat = Config.BaseURL + "/" + angular.lowercase(path) + "/" + slugs;
                }
                
                link.subname = newtitle;
                link.linktext = concat;
                $scope.link.subname = newtitle;
                $scope.link.linktext = concat;
               
              };
               $scope.onslugs = function(Text){ 
                if(Text != null){ $scope.link.linktext = Text.replace(/\s+/g, '-').toLowerCase();}
              }
              $scope.text = function(link) {
                link.linkoption = 'false';
                link.linkoption = false;
                if(link.linktext == ''){
                  $scope.savedisable = true;
                }else{
                  $scope.savedisable = false;
                }
              };
              $scope.newtabbot = function(link) {                
                if(link.newtab == 'true'){
                  link.newtab = false;
                }
                link['newtab'] = link.newtab;
           
                $scope.savedisable = false;
              };

              $scope.changet = function() {                
                $scope.savedisable = false;
              };
              $scope.change = function() {
                $scope.changename = true;
              };
              $scope.changeok = function() {
                $scope.changename = false;
              };
              $scope.changecancel = function(link) {
                $scope.changename = false;
                link.subname = $scope.origsubname;
              };

              $scope.addsub = function(link) {
                $scope.alerts = [];
                $scope.closeAlert = function(index) {
                  $scope.alerts.splice(index, 1);
                };
                link['subtype'] = $scope.path;
                link['parentmenuID'] = submenuID;                
                link['sublink'] = link.linktext;
                link['menuID'] = $scope.origmenuID;

                

                $timeout(function() {
                 Createmenu.addsubmenu(link, function(data){
                   $scope.link = [];
                   link.subname = "";
                   link.linktext = "";
                   $scope.path="close";
                   $scope.path = false;
                   $scope.link.newtab = false;
                  loadSubMenuList();
                  loadMenuList(num, off, keyword);
                  $scope.alerts = [{type: 'success', msg: 'Submenu successfully added!'}];
                  });
                }, 500);
                var menuStruc = angular.toJson($('.dd').nestable('serialize'));
                 var menu = {};
                 menu = menuStruc;
                  Createmenu.submenulist(link['menuID'],function(data){
                  var namemenu = data.menuname[0].name;
                  Createmenu.savemenu(menu,link['menuID'],namemenu, function(data){
                      
                  });   
                });
              };
              $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
              };



            },resolve: {
              submenuID: function() {
                return submenuID
              }
            }
          })
        }

        $scope.editsubmenu = function(submenuID) {
          var modalInstance = $modal.open({
            templateUrl: 'editSubMenu.html',
            windowClass: 'editmenu-modal-window',
            controller: function($scope, $modalInstance, submenuID) {

              $scope.savedisable = false;
              $scope.submenuID = submenuID;
              $scope.path = 'Link';
              $scope.changename = false; 

              var loadSubMenuName = function () {  
                Createmenu.submenuname(submenuID,function(data){
                  $scope.link = data.data[0];
                  $scope.path = data.data[0].subtype; 
                  $scope.origsubname = data.data[0].subname; 
                    $scope.link.linktext = data.data[0].sublink;
                    $scope.link.linkoption = data.data[0].slugs;
                
                });
              }
              loadSubMenuName();

              var loadpageList = function () {    
                Createmenu.pagelist(function(data){
                  $scope.pages = data.data;
                });
              }
              loadpageList();

              var loadcategoryList = function () {    
                Createmenu.categorylist(function(data){
                  $scope.category = data.data; 
                  $scope.pagecategory = data.datapage;
                });
              }
              loadcategoryList();

              var loadtagList = function () {    
                Createmenu.taglist(function(data){
                  $scope.tags = data.data; 
                   
                });
              }
              loadtagList();

              var loadpostList = function () {    
                Createmenu.postlist(function(data){
                  $scope.news = data.data; 
                  
                });
              }
              loadpostList();

              var loadauraList = function () {    
                Createmenu.auralist(function(data){
                  $scope.aura = data;      
                });
              }
              loadauraList();

              $scope.pathtype = function(type) {
                $scope.path = type;                
              
              };

              $scope.option = function(link, path, slugs, newtitle) {
                link['slugs'] = slugs;
                link['subname'] = newtitle;
                $scope.savedisable = false;
                if(path == 'null' || path == null){
                  var concat = Config.BaseURL + "/" + slugs;
                }
                else{
                  var concat = Config.BaseURL + "/" + angular.lowercase(path) + "/" + slugs;
                }
                
                link.subname = newtitle;
                link.linktext = concat;
               
              };
              $scope.text = function(link) {
              
                link.linkoption = 'false';
                link.linkoption = false;
                if(link.linktext == ''){
                  $scope.savedisable = true;
                }else{
                  $scope.savedisable = false;
                }
              };

              $scope.onslugs = function(Text){ 
                if(Text != null){ $scope.link.linktext = Text.replace(/\s+/g, '-').toLowerCase();}
              }

              $scope.newtabbot = function(link) {                
                if(link.newtab == 'true'){
                  link.newtab = false;
                }
                link['newtab'] = link.newtab;
                
          
                $scope.savedisable = false;
              };

              $scope.changet = function() {                
                $scope.savedisable = false;
              };
              $scope.change = function() {
                $scope.changename = true;
              };
              $scope.changeok = function() {
                $scope.changename = false;
              };
              $scope.changecancel = function(link) {
                $scope.changename = false;
                link.subname = $scope.origsubname;
              };

              $scope.save = function(link) {
                link['subtype'] = $scope.path;
                link['submenuID'] = $scope.submenuID;                
                link['sublink'] = link.linktext;

                Createmenu.updatesubmenu(link, function(data){
                  loadSubMenuList();
                  $modalInstance.dismiss('cancel');
                });
              };
              $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
              };

            },
            resolve: {
              submenuID: function() {
                return submenuID
              }
            }
          });
        }

      },
      resolve: {
        menuID: function() {
          return menuID
        }
      }
    });
  }




  $scope.addnewmenu = function() {
    var modalInstance = $modal.open({
      templateUrl: 'addMenu.html',
      windowClass: 'editmenu-modal-window',
      controller: function($scope, $modalInstance) {
$scope.keyword=null;
var keyword = null;
var num = 10;
var off = 1;
var loadSubMenuList = function (menuID) {
        Createmenu.submenulist(menuID,function(data){
          $scope.submenu = data.data; 
          $scope.child1 = data.child1;
          $scope.child2 = data.child2;
          $scope.child3 = data.child3;
          $scope.child4 = data.child4;
          $scope.menu = data.menuname[0]; 
          $scope.menulength = data.data.length;   
        });
      }

        $scope.data={};
        $scope.menu={};
        $scope.ontitle = function convertToSlug(Text)
        {
          var text1 = Text.replace(/[^\w ]+/g,'');
          $scope.shortCode = angular.lowercase(text1.replace(/ +/g,'-'));
        }
       $scope.addmenu = function(menudata)
       {
      $scope.menu=[];
       $scope.addnewmenu = true; 
        $scope.alerts = []; 
        menudata['shortCode'] = $scope.shortCode;
   
        Createmenu.add(menudata,function(data){
          if(data.hasOwnProperty('error')){
            if(data.error.hasOwnProperty('existTitle')){
              $scope.alerts =[{type: 'danger', msg: 'Menu already exists.'}];
              $scope.isSaving = false;
              $anchorScroll();
            }
          }else {
            $scope.isSaving = false;
            $scope.alerts =[{type: 'success', msg: 'Menu successfully saved!'}];
            $scope.data.newmenu = null;
            $scope.menuID = data[0]['menuID'];
            $scope.menu.name = data[0]['name'];
            loadSubMenuList($scope.menuID);
          }

        });

          loadMenuList(num, off, keyword);
      }




    $scope.sub = {};     
    var slugstorage = "";
    $scope.onpagetitle = function convertToSlug(Text) {if(Text != null && $scope.editedslug == false && $scope.editslug == false){$scope.page.slugs = Text.replace(/\s+/g, '-').toLowerCase();}}
    $scope.onslugs = function(Text){ if(Text != null){ $scope.menu.name = Text.replace(/\s+/g, '-').toLowerCase(); }}
    $scope.editpageslug = function(){
        $scope.editslug = true;
        slugstorage = $scope.menu.name;
    }

    $scope.cancelpageslug = function(title) {
        if(title != null)
        {
            $scope.editedslug = false;
            $scope.menu.name = slugstorage;
        }else {
            $scope.menu.name = '';
            $scope.editedslug = false;
        }
        $scope.editslug = false;
    }

    $scope.setslug = function(slug){
        $scope.editedslug = true;
        $scope.editslug = false;
        if(slug != null)
        {
            $scope.menu.name = slug.replace(/\s+/g, '-').toLowerCase();
        }

    }

    $scope.clearslug = function(title){
        if(title != null)
        {
            $scope.editedslug = false;
            $scope.menu.name = title.replace(/\s+/g, '-').toLowerCase();
        }else {
            $scope.menu.name = '';
            $scope.editedslug = false;
        }
    }
    $scope.close = function () {
      $scope.path="close";
      $scope.path = false;
      $scope.sub.subname = "";
      $scope.sub.linktext = "";
    }

    $scope.changename = false;

        $scope.cancel = function() {
          $modalInstance.dismiss('cancel');
        };

        $scope.deletesubmenu = function(submenuID,menuID) {
          var modalInstance = $modal.open({
            templateUrl: 'deletemenu.html',
            controller: function($scope, $modalInstance, submenuID) {
              $scope.ok = function() {
                Createmenu.deletesubmenu(submenuID, function(data){
                  loadSubMenuList(menuID); 
                  $modalInstance.close();
                });
              };
              $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
              };

            },
            resolve: {
              submenuID: function() {
                return submenuID
              }
            }
          });
        }


        $scope.expand = function(){$('.dd').nestable('expandAll');}
        $scope.collapse = function(){$('.dd').nestable('collapseAll');}



        $scope.save = function(){
          var menuStruc = angular.toJson($('.dd').nestable('serialize'));
          var menu = {};
          menu = menuStruc;
          var name = $scope.menu.name;
          var menuID = $scope.menuID;        
          Createmenu.savemenu(menu,menuID,name, function(data){
            loadSubMenuList(menuID);
             loadMenuList(num, off, keyword);
            $modalInstance.dismiss('cancel');
          });

        }



//MENU ITEM
        $scope.addmenuitem = function (menuID) {
          var modalInstance = $modal.open({
            templateUrl: 'addMenuItem.html',
            windowClass: 'editmenu-modal-window',
            controller: function($scope, $modalInstance) {
               $scope.sub = {};   
              $scope.link = [];
                  $scope.changename = false;
                  $scope.onslugs = function(Text){ 
                if(Text != null){ $scope.sub.linktext = Text.replace(/\s+/g, '-').toLowerCase();
                $scope.link.linktext = Text.replace(/\s+/g, '-').toLowerCase();}
              }
                   $scope.option = function(link, path, slugs, newtitle) {
                link['slugs'] = slugs;
                link['subname'] = newtitle;
                $scope.savedisable = false;
                if(path == 'null' || path==null){
                  var concat = Config.BaseURL + "/" + slugs;
                }else{
                  var concat = Config.BaseURL + "/" + angular.lowercase(path) + "/" + slugs;
                }
                
                link.subname = newtitle;
                link.linktext = concat;
                  $scope.sub.subname = newtitle;
                $scope.sub.linktext = concat;
               
              };
               
              $scope.text = function(link) {
                link.linkoption = 'false';
                link.linkoption = false;
                if(link.linktext == ''){
                  $scope.savedisable = true;
                }else{
                  $scope.savedisable = false;
                }
              };

          
              var loadpageList = function () {    
                Createmenu.pagelist(function(data){
                  $scope.pages = data.data; 
                
                });
              }
              loadpageList();

              var loadcategoryList = function () {    
                Createmenu.categorylist(function(data){
                  $scope.category = data.data; 
                  $scope.pagecategory = data.datapage;
                });
              }
              loadcategoryList();

              var loadtagList = function () {    
                Createmenu.taglist(function(data){
                  $scope.tags = data.data;    
                });
              }
              loadtagList();

              var loadpostList = function () {    
                Createmenu.postlist(function(data){
                  $scope.news = data.data; 
                });
              }
              loadpostList();

              var loadauraList = function () {    
                Createmenu.auralist(function(data){
                  $scope.aura = data;    
                });
              }
              loadauraList();

              $scope.pathtype = function(type) {
                $scope.path = type;                
              };

              $scope.addsub = function(subdata) {
                $scope.alerts = [];
                $scope.closeAlert = function(index) {
                  $scope.alerts.splice(index, 1);
                };
                // var menuID = $scope.menuID;
                subdata['menuID'] = menuID;
                subdata['path'] = $scope.path;
                subdata['newtab'] = $scope.link.newtab;
                $scope.path="close";
                $scope.path = false;
                Createmenu.addsub(subdata, function(data){
                    $scope.alerts = [{type: 'success', msg: 'Menu Item successfully added!'}];
                     loadSubMenuList(menuID);
                });
                $scope.link.newtab = false;
                 $scope.sub = {};
                
              };


                $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
              };
              $scope.newtabbot = function(link) {             
                if(link.newtab == 'true'){
                  link.newtab = false;
                }
                link['newtab'] = link.newtab;
           
                $scope.savedisable = false;
              };

      }
    })

}


         $scope.addsubmenu = function (submenuID) {
                  var modalInstance = $modal.open({
            templateUrl: 'addSubMenu.html',
            windowClass: 'editmenu-modal-window',
            controller: function($scope, $modalInstance, submenuID) {
                $scope.savedisable = true;
              $scope.submenuID = submenuID;
              $scope.changename = false; 
              $scope.link = {};

              var loadSubMenuName = function () {  
                Createmenu.submenuname(submenuID,function(data){
                  $scope.origsubname = data.data[0].subname;
                  $scope.origmenuID = data.data[0].menuID;
                });
              }
              loadSubMenuName();

              var loadpageList = function () {    
                Createmenu.pagelist(function(data){
                  $scope.pages = data.data; 
                      
                });
              }
              loadpageList();

               var loadcategoryList = function () {    
                Createmenu.categorylist(function(data){
                  $scope.category = data.data; 
                  $scope.pagecategory = data.datapage;
                });
              }
              loadcategoryList();

              var loadtagList = function () {    
                Createmenu.taglist(function(data){
                  $scope.tags = data.data;
                });
              }
              loadtagList();

              var loadpostList = function () {    
                Createmenu.postlist(function(data){
                  $scope.news = data.data;      
                });
              }
              loadpostList();

              var loadauraList = function () {    
                Createmenu.auralist(function(data){
                  $scope.aura = data;    
                });
              }
              loadauraList();

              $scope.pathtype = function(type) {
                $scope.path = type;
              };

              $scope.option = function(link, path, slugs, newtitle) {
                link['slugs'] = slugs;
                link['subname'] = newtitle;
                $scope.savedisable = false;
                if(path == 'null' || path==null){
                  var concat = Config.BaseURL + "/" + slugs;
                }else{
                  var concat = Config.BaseURL + "/" + angular.lowercase(path) + "/" + slugs;
                }
                
                link.subname = newtitle;
                link.linktext = concat;
                $scope.link.subname = newtitle;
                $scope.link.linktext = concat;
               
              };
               $scope.onslugs = function(Text){ 
                if(Text != null){
                $scope.link.linktext = Text.replace(/\s+/g, '-').toLowerCase();}
              }
              $scope.text = function(link) {
                link.linkoption = 'false';
                link.linkoption = false;
                if(link.linktext == ''){
                  $scope.savedisable = true;
                }else{
                  $scope.savedisable = false;
                }
              };
              $scope.newtabbot = function(link) {                
                if(link.newtab == 'true'){
                  link.newtab = false;
                }
                link['newtab'] = link.newtab;
           
                $scope.savedisable = false;
              };

              $scope.changet = function() {                
                $scope.savedisable = false;
              };
              $scope.change = function() {
                $scope.changename = true;
              };
              $scope.changeok = function() {
                $scope.changename = false;
              };
              $scope.changecancel = function(link) {
                $scope.changename = false;
                link.subname = $scope.origsubname;
              };

              $scope.addsub = function(link) {
                $scope.alerts = [];
                $scope.closeAlert = function(index) {
                  $scope.alerts.splice(index, 1);
                };
                link['subtype'] = $scope.path;
                link['parentmenuID'] = submenuID;                
                link['sublink'] = link.linktext;
                link['menuID'] = $scope.origmenuID;
             
                Createmenu.addsubmenu(link, function(data){
                  loadSubMenuList($scope.origmenuID);
                   $scope.alerts = [{type: 'success', msg: 'Submenu successfully added!'}];
                });
                $scope.link = [];
                link.subname = "";
                link.linktext = "";
                $scope.path="close";
                $scope.path = false;
                $scope.link.newtab = false;
              };
              $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
              };



            },resolve: {
              submenuID: function() {
                return submenuID
              }
            }
          })
        }


        $scope.editsubmenu = function(submenuID,menuID) {
          var modalInstance = $modal.open({
            templateUrl: 'editSubMenu.html',
            windowClass: 'editmenu-modal-window',
            controller: function($scope, $modalInstance, submenuID) {

              $scope.savedisable = true;
              $scope.submenuID = submenuID;
              $scope.path = 'Link';
              $scope.changename = false; 

              var loadSubMenuName = function () {  
                Createmenu.submenuname(submenuID,function(data){
                  $scope.link = data.data[0];
                  $scope.path = data.data[0].subtype; 
                  $scope.origsubname = data.data[0].subname; 
                  
                    $scope.link.linktext = data.data[0].sublink;
                  
                    $scope.link.linkoption = data.data[0].slugs;
                });
              }
              loadSubMenuName();

              var loadpageList = function () {    
                Createmenu.pagelist(function(data){
                  $scope.pages = data.data; 
                    
                });
              }
              loadpageList();

              var loadcategoryList = function () {    
                Createmenu.categorylist(function(data){
                  $scope.category = data.data; 
                  $scope.pagecategory = data.datapage;
                });
              }
              loadcategoryList();

              var loadtagList = function () {    
                Createmenu.taglist(function(data){
                  $scope.tags = data.data; 
                  
                });
              }
              loadtagList();

              var loadpostList = function () {    
                Createmenu.postlist(function(data){
                  $scope.news = data.data;    
                });
              }
              loadpostList();

              var loadauraList = function () {    
                Createmenu.auralist(function(data){
                  $scope.aura = data;    
                });
              }
              loadauraList();

              $scope.pathtype = function(type) {
                $scope.path = type;
              };

              $scope.option = function(link, path, slugs, newtitle) {
        
                link['slugs'] = slugs;
                link['subname'] = newtitle;
                $scope.savedisable = false;
                if(path == 'null' || path==null){
                  var concat = Config.BaseURL + "/" + slugs;
                }else{
                  var concat = Config.BaseURL + "/" + angular.lowercase(path) + "/" + slugs;
                }
                
                link.subname = newtitle;
                link.linktext = concat;
          
              };
               $scope.onslugs = function(Text){ 
                if(Text != null){
                $scope.link.linktext = Text.replace(/\s+/g, '-').toLowerCase();}
              }
              $scope.text = function(link) {
       
                link.linkoption = 'false';
                link.linkoption = false;
                if(link.linktext == ''){
                  $scope.savedisable = true;
                }else{
                  $scope.savedisable = false;
                }
              };
              $scope.newtabbot = function(link) {                
                if(link.newtab == 'true'){
                  link.newtab = false;
                }
                link['newtab'] = link.newtab;
                
                $scope.savedisable = false;
              };

              $scope.changet = function() {                
                $scope.savedisable = false;
              };
              $scope.change = function() {
                $scope.changename = true;
              };
              $scope.changeok = function() {
                $scope.changename = false;
              };
              $scope.changecancel = function(link) {
                $scope.changename = false;
                link.subname = $scope.origsubname;
              };

              $scope.save = function(link) {
                link['subtype'] = $scope.path;
                link['submenuID'] = $scope.submenuID;                
                link['sublink'] = link.linktext;

                Createmenu.updatesubmenu(link, function(data){
                  loadSubMenuList(menuID);
                  $modalInstance.dismiss('cancel');
                });
              };


              $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
              };

            },
            resolve: {
              submenuID: function() {
                return submenuID
              }
            }
          });
        }

      }
      // resolve: {
      //   menuID: function() {
      //     return menuID
      //   }
      // }
    });
  }


})
