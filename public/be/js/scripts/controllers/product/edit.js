app.controller('EditProduct', function($scope, $state, $stateParams, Upload ,$q, $http, Config, $modal, $sce, $anchorScroll, $filter, Product, $timeout, $anchorScroll){
    $scope.amazonlink = Config.amazonlink;
    $scope.product = { tags: [], images: [], type: [], category: [], subcategory: [], size:[]};
    $scope.alerts = [];
    $scope.today = moment().format('YYYY-MM-DD');
    $scope.required = { type : false, description: false, category: false, subcategory: false, tags: false, images: false }

    var alert = function(data){ 
        $scope.closeAlert();
        if(data.type == 'success'){
            $scope.alerts.push({ type: 'success', msg: data.msg });
        }else {
            $scope.alerts.push({ type: 'danger', msg: 'An error occurred please try again later.' });
        }
    }

    $scope.closeAlert = function() {
        $scope.alerts.splice(0,1);
    }

    $scope.dataPickerStates = {
      open1:false,
      open2:false
    }

    $scope.open = function($event, opened) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.dataPickerStates[opened] = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    var loadproduct = function(productid) {
        Product.loadedit(productid, function(data) {
            $scope.product = data.product;
            $scope.product.status = data.product.status==1 ? true : false;
            $scope.tags = data.tags;
            $scope.types = data.type;
            $scope.sizes = data.size;
            $scope.categories = data.category;
            $scope.discount(data.product.discount, 'percentage');

            if($scope.product.discount_from <= $scope.today && $scope.today <= $scope.product.discount_to){
                $scope.stat = true;
            }else {
                $scope.stat = false;
            }

            $timeout(function() {
                $scope.validation($scope.product.category, 'category');
                $timeout(function() {
                    $scope.validation($scope.product.subcategory, 'subcategory');
                },500);
            }, 500);
        });
    } 

    loadproduct($stateParams.productid);

    $scope.$watch(function(){
        return $scope.product.price;
    }, function() {
        $scope.discount($scope.product.discount, 'percentage');
    });

    $scope.discount = function(data, type) {
        if($scope.product.price!=undefined && $scope.product.price!=0){
            if(type=="percentage" && data != undefined){
                var res = ($scope.product.price * (data / 100)).toFixed(2);
                $scope.product.discprice = res;
                $scope.product.discounted = $scope.product.price - res;
            }else if(type=="price" && data != undefined){
                var res = (data * 100) / $scope.product.price;
                $scope.product.discount = (res % 2) != 0 ? res.toFixed(2) : res;
                $scope.product.discounted = $scope.product.price - data;
            }
        }
    }

    $scope.media = function(type){
        var modalInstance = $modal.open({
            templateUrl: 'mediagallery.html',
            controller: function($scope, $modalInstance,Config, type, productid) {
                $scope.imageloader=false;
                $scope.imagecontent=true;
                $scope.directory = 'productimage/' + productid;
                $scope.videoenabled=false;
                $scope.mediaGallery = 'images';
                $scope.invalidvideo = false;
                $scope.currentSelected = '';
                $scope.currentDeleting = '';
                $scope.type = type;
                $scope.s3link = Config.amazonlink;
                $scope.imagelength = 0;
                $scope.alertss = [];
                var selected = {};

                $scope.closeAlerts = function() {
                    $scope.alertss = [];
                }

                $scope.set = function(data){
                    selected = data;
                    $scope.currentSelected = data;
                }

                var returnYoutubeThumb = function(item){
                    var x= '';
                    var thumb = {};
                        if(item){
                            var newdata = item;
                            var x;
                            x = newdata.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
                            thumb['url'] ='http://img.youtube.com/vi/' + x[1] + '/hqdefault.jpg';
                            thumb['yid'] = x[1];
                            return thumb;
                        }else{
                            return x;
                        }
                }



                $scope.returnImageThumb = function(){
                    return function (item) {
                        if(item){
                            return Config.amazonlink + "/uploads/productimage/" + productid + "/" + item;
                        }else{
                            return item;
                        }
                    };
                }

                var loadimages = function() {
                    Product.loadimages(productid, function(data){
                        $scope.imagelist = data;
                        $scope.imagelength = data.length;
                        Product.images = data;
                    });
                }

                loadimages();

                $scope.$watch('files', function () {
                    $scope.upload($scope.files);
                });

                $scope.upload = function (files)
                {

                    var filename
                    var filecount = 0;
                    if (files && files.length)
                    {
                        $scope.imageloader=true;
                        $scope.imagecontent=false;
                        var i = 0;
                        for (i; i < files.length; i++)
                        {
                            var file = files[i];

                            if (file.size >= 2000000)
                            {
                                $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
                                filecount = filecount + 1;

                                if(filecount == files.length)
                                {
                                    $scope.imageloader=false;
                                    $scope.imagecontent=true;
                                }


                            }
                            else

                            {   
                                var count = $scope.imagelength + i + 1;
                                if(count <= 6){
                                    var promises;

                                    promises = Upload.upload({

                                        url: Config.amazonlink, //S3 upload url including bucket name
                                        method: 'POST',
                                        transformRequest: function (data, headersGetter) {
                                            //Headers change here
                                            var headers = headersGetter();
                                            delete headers['Authorization'];
                                            return data;
                                        },
                                        fields : {
                                            key: 'uploads/productimage/' + productid+ '/' + file.name, // the key to store the file on S3, could be file name or customized
                                            AWSAccessKeyId: Config.AWSAccessKeyId,
                                            acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                                            policy: Config.policy, // base64-encoded json policy (see article below)
                                            signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                            "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                        },
                                        file: file
                                    })
                                    promises.then(function(data){

                                        filecount = filecount + 1;
                                        filename = data.config.file.name;
                                        var fileout = {
                                            'filename' : filename
                                        };
                                        Product.saveImage(productid, fileout, function(data){
                                            loadimages();
                                            if(filecount == files.length || count >= 6)
                                            {
                                                $scope.imageloader=false;
                                                $scope.imagecontent=true;
                                            }
                                        });
                                    });
                                }
                            }

                        }
                    }
                };

                $scope.deletenewsimg = function (dataimg, $event)
                {
                    var fileout = {
                        'filename' : dataimg
                    };
                    Product.deleteImage(productid, fileout, function(data) {
                        loadimages();
                    });
                    $event.stopPropagation();
                }

                $scope.ok = function(category) {
                    if(selected.hasOwnProperty('filename')){
                        Product.selectImage(selected, function(data) {
                            Product.images = data;
                            $timeout(function() {
                                $modalInstance.dismiss();
                            },500);
                        });
                    }else {
                        $modalInstance.dismiss();
                    }
                }

                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                }

            },
            resolve: {
                type: function(){
                    return type;
                },
                productid: function() {
                    return $scope.product.productid;
                }
            },
            windowClass: 'xxx-modal-window'
        }).result.finally(function() {
            loadproduct($stateParams.productid);
        });
    }

    // $scope.validatepcode = function(code, name) {
    //     Product.validatecode(code, name, function(data) {
    //         $scope.exist = data.exist;
    //     });
    // }

    var validate = function(callback) {  
        if($scope.product.tags.length == 0){
            $scope.required.tags = true;
        }
        if($scope.product.category.length == 0) {
            $scope.required.category = true;
        }
        if($scope.product.type.length == 0){
            $scope.required.type = true;
        }
        if($scope.product.description == undefined) {
            $scope.required.description = true;
        }
        if($scope.product.images.length == 0){
            $scope.required.images = true;
        }
        $('input').trigger("blur");
        $('textarea').trigger("blur");
        if($scope.product.tags.length > 0 && $scope.product.category.length > 0 && $scope.product.type.length > 0 && $scope.product.description != undefined && $scope.product.images.length > 0 && $scope.product.name != undefined && $scope.product.shortdesc != undefined && $scope.product.quantity!=undefined && $scope.product.price != undefined && $scope.product.belowquantity!=undefined && $scope.product.maxquantity != undefined && $scope.product.minquantity!=undefined){
            callback();
        }else {
            console.log('error');
            console.log($scope.product);
        }
    }

    $scope.editproduct = function(data) {
        data.discount_from = moment(data.discount_from).format('YYYY-MM-DD');
        data.discount_to = moment(data.discount_to).format('YYYY-MM-DD');
        

        validate(function(){
            Product.edit(data, function(data) {
                alert(data);
                loadproduct($stateParams.productid);
                $anchorScroll();
            });
        });
    }

    $scope.validateName = function(name, id) {
        Product.validatename(id, name, function(data) {
            $scope.exist2 = data.exist;
            $scope.onslugs(name);
        });
    }

    // slugs
    $scope.editprodslug = function(){
        $scope.editslug = true;
        slugstorage = $scope.product.slugs;
    }

    $scope.clearslug = function(name){
        if(name != null)
        {
            $scope.editedslug = false;
            $scope.product.slugs = name.replace(/\s+/g, '-').toLowerCase();
        }else {
            $scope.product.slugs = '';
            $scope.editedslug = false;
        }
    }

    $scope.setslug = function(slug){
        $scope.editedslug = true;
        $scope.editslug = false;
        if(slug != null)
        {
            $scope.product.slugs = slug.replace(/\s+/g, '-').toLowerCase();

        }
    }

    $scope.cancelpageslug = function(name) {
        if(name != null)
        {
            $scope.editedslug = false;
            $scope.product.slugs = slugstorage;
        }else {
            $scope.product.slugs = '';
            $scope.editedslug = false;
        }
        $scope.editslug = false;
    }

    $scope.onslugs = function(Text){
        if(Text != null)
        {
            Text = Text.replace(/[^\w ]+/g, '');
            $scope.product.slugs = Text.replace(/ +/g,'-').toLowerCase();
            Product.validateSlugs($scope.product.productid, $scope.product.slugs, function(data){
                $scope.exist3 = data.exist;
            });
        }
    }

    $scope.validation = function (data, type) {
        if(data == undefined || data.length==0){
            if(type=='type'){
                $scope.required.type = true;
            }else if(type=='description') {
                $scope.required.description = true;
            }else if(type=='category') {
                $scope.required.category = true;
                $scope.required.subcategory = true;
                $scope.required.type = true;
                $scope.subcategories = [];
                $scope.product.subcategory = [];
                $scope.product.type = [];
            }else if(type=='subcategory') {
                $scope.required.subcategory = true;
                $scope.required.type = true;
                $scope.types = [];
                $scope.product.type = [];
            }else if(type=='tags') {
                $scope.required.tags = true;
            }
        }else {
            var diff = [];
            if(type=='type'){
                $scope.required.type = false;
            }else if(type=='description') {
                $scope.required.description = false;
            }else if(type=='category') {
                $scope.required.category = false;
                Product.getsubcat({ category : data }, function(data) {
                    $scope.subcategories = data;
                    diff = $($scope.product.subcategory).not(data).get();
                });
            }else if(type=='subcategory') {
                $scope.required.subcategory = false;
                Product.gettypes({ subcategory : data }, function(data) {
                    $scope.types = data;
                    diff = $($scope.product.type).not(data).get();
                });
            }else if(type=='tags') {
                $scope.required.tags = false;
            }

            $timeout(function(){
                if(diff.length > 0){
                    diff.map(function(dif) {
                        if(type=='subcategory'){
                            $scope.product.type.splice( $.inArray(dif, $scope.product.type), 1 );
                        }else if(type=='category'){
                            $scope.product.subcategory.splice( $.inArray(dif, $scope.product.subcategory), 1 );

                            Product.gettypes({ subcategory : $scope.product.subcategory }, function(data) {
                                $scope.types = data;
                                diff2 = $($scope.product.type).not(data).get();
                                 $timeout(function(){
                                    if(diff2.length > 0){
                                        diff2.map(function(dif2) {
                                            $scope.product.type.splice( $.inArray(dif2, $scope.product.type), 1 );
                                        })
                                    }
                                }, 500);
                            });
                        }
                    });
                }
            }, 500);
        }
    }
})