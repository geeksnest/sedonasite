app.controller('HomepagemanageCtrl', function($http, $scope, $stateParams, $modal, $sce, Config, Homepage, $anchorScroll) {
  //LIST
  var num = 10;
  var off = 1;
  var keyword = null;
  $scope.loading = false;

  var paginate = function(off, num, keyword) {
    $scope.loading = true;
    Homepage.list(num, off, keyword, function(data) {
      $scope.data = data.data;
      $scope.total_items = data.total_items;
      if (keyword !== undefined && off === 0) {
        $scope.keywordshow = true;
        $scope.keyword = keyword;
      }
      $scope.maxSize = 10;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
      $scope.loading = false;

      console.log($scope.data);
    });
  };

  $scope.shit = function(wew){
 
    var content = {
      id : wew
    };
       // console.log(content);
    Homepage.sortthis(content,function(){
      $scope.alerts.splice(0, 1);
      $scope.alerts.push({
        type: "success",
        msg: "Updated"
      });
    });
  };

  $scope.search = function(keyword) {
    if (keyword === "") {
      keyword = undefined;
    }
    var off = 0;
    $scope.searchtext = undefined;
    paginate(off, num, keyword);
  };

  $scope.clear = function() {
    paginate(off, num, undefined);
    $scope.keywordshow = false;
  };
  $scope.setPage = function(off, keyword) {
    paginate(off, keyword);
  };

  paginate(off, keyword);
  //END LISTING


  //DELETE
  $scope.delete = function(id) {
    var modalInstance = $modal.open({
      templateUrl: 'homepageDelete.html',
      controller: dltCTRL,
      resolve: {
        dlt: function() {
          return id;
        }
      }
    });
  };

  $scope.alerts = [];
  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };
  var alertme = function(data) {
    console.log(data);
    $scope.alerts.splice(0, 1);
    $scope.alerts.push({
      type: data.type,
      msg: data.msg
    });
  };
  var dltCTRL = function($scope, $modalInstance, dlt) {
    $scope.id = dlt;

    $scope.ok = function(dlt) {
      Homepage.delete(dlt, function(data) {
        paginate(off, keyword);
        $modalInstance.dismiss('cancel');
        alertme(data);
        $scope.keywordshow = false;
      });
    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  };
  //UPDATE USER
  $scope.update = function(id) {
    var modalInstance = $modal.open({
      templateUrl: 'homepageUpdate.html',
      controller: updateCTRL,
      resolve: {
        update: function() {
          return id;
        }
      }
    });
  };
  var updateCTRL = function($scope, $modalInstance, update, $state) {
    $scope.update = update;
    $scope.ok = function(update) {
      $state.go('updatehomepage', {
        contentid: update
      });
      $modalInstance.dismiss('cancel');

    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  };

});
