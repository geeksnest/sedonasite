'use strict';

/* Controllers */

app.controller('HomepagecreateCtrl', function($scope, $state, $q, $http, Config,$anchorScroll, $timeout, $modal, Homepage) {
  console.log("-------------HOMEPAGE--------------");
  $scope.base_url = Config.BaseURL;

  $scope.master = {
    bgcolor: '#ffffff',
    titlesize: 44,
    descriptionsize: 20,
    position: 'Center',
    box: 'off'
  }

  $scope.test = function(val) {
    console.log(val);
  }

  $scope.alerts = [];
  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };

  Homepage.categories(function(data){
    $scope.categories = data.categories;
  })

  $scope.process = 0;
     var list = [];
    for (var x = 10; x<=60; x++)
    {
        list.push(x);
    }

    $scope.tag = list;
  //SAVE DATA
    $scope.submitData = function(content, file) {
                        content.backgroundimg = file;
                        Homepage.create(content, function(data) {
                          // console.log(content)
                            console.log(data);
                            $scope.file = undefined;
                            $scope.alerts.push({type: data.type,  msg: data.msg});
                            $scope.contentform.$setPristine();
                            $scope.process = 0;
                            $('input').val(undefined);
                            $scope.amazonpath = undefined;
                            $scope.content = angular.copy($scope.master,$scope.content);
                            $scope.content = undefined;
                        });

                        $anchorScroll();
                    }

    $scope.prepare = function(file) {
        $scope.alerts = [];
        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };
        if (file && file.length) {
            if (file.size >= 2000000) {
                console.log('File is too big!');
                $scope.alerts = [{
                    type: 'danger',
                    msg: 'File ' + file.name + ' is too big'
                }];
                $scope.file = '';
            } else {
                console.log("below maximum");
                $scope.file = file;
                $scope.closeAlert();
            }
        }
    }


    // UPLOAD PHOTO ///////////////////////////////////////////////////////////////////////////////////////////
    $scope.showimageList = function(size,path){
        var amazon = $scope.amazon;
        var modalInstance = $modal.open({
            templateUrl: 'imagelist.html',
            controller: imagelistCTRL,
            size: size,
            resolve: {
                path: function() {
                    return amazon
                }
            }

        });
    }
    $scope.stat="1";
    var pathimage = "";

    var pathimages = function(){
        $scope.amazonpath=pathimage;
    }

    var imagelistCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path){
        $scope.amazonpath= path;
        $scope.imageloader=false;
        $scope.imagecontent=true;
        $scope.noimage = false;
        $scope.imggallery = false;

        var loadimages = function() {
            $http({
                url: Config.ApiURL + "/panel/listimages",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                if(data.error == "NOIMAGE" ){
                  $scope.imggallery=false;
                  $scope.noimage = true;
              }else{
                  $scope.noimage = false;
                  $scope.imggallery=true;
                  $scope.imagelist = data;
              }
          }).error(function(data) {
          });
      }
      loadimages();

      $scope.path=function(path){
          var texttocut = Config.amazonlink + '/uploads/homepagebackground/';
          var newpath = path.substring(texttocut.length);
          pathimage = newpath;
          pathimages();
          $modalInstance.dismiss('cancel');
      }



      $scope.upload = function(files) {
         $scope.upload(files);
     };

     $scope.delete = function(id){
      var modalInstance = $modal.open({
        templateUrl: 'notification.html',
        controller: deleteCTRL,
        resolve: {
          imgid: function() {
            return id
        }
    }
});
  }

  var deleteCTRL = function($scope, $modalInstance, imgid) {

      $scope.alerts = [];

      $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.message="Are you sure do you want to delete this Photo?";
    $scope.ok = function() {
     $http({
         url: Config.ApiURL+"/panel/deleteimage/"+ imgid,
         method: "get",
         headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
      },
  }).success(function(data, status, headers, config) {
    $scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });
    loadimages();
    $modalInstance.close();
}).error(function(data, status, headers, config) {
    loadimages();
    $modalInstance.close();
    $scope.alerts.push({ type: 'success', msg: 'Something went wrong Image not Deleted!' });
});
};

$scope.cancel = function() {
  $modalInstance.dismiss('cancel');
};
}

$scope.upload = function (files)
{
  var filename;
  var filecount = 0;
  if (files && files.length)
  {
    $scope.imageloader=true;
    $scope.imagecontent=false;

    for (var i = 0; i < files.length; i++)
    {
      var file = files[i];

      if (file.size >= 2000000)
      {
        $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
        filecount = filecount + 1;

        if(filecount == files.length)
        {
          $scope.imageloader=false;
          $scope.imagecontent=true;
      }
  }
  else
  {
    var promises;

    var fileExtension = '.' + file.name.split('.').pop();

                  // rename the file with a sufficiently random value and add the file extension back
                  var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + i + fileExtension;

                  promises = Upload.upload({

                               url:Config.amazonlink, //S3 upload url including bucket name
                               method: 'POST',
                               transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                            },
                            fields : {
                                  key: 'uploads/homepagebackground/' + renamedFile, // the key to store the file on S3, could be file name or customized
                                  AWSAccessKeyId: Config.AWSAccessKeyId,
                                  acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                                  policy: Config.policy, // base64-encoded json policy (see article below)
                                  signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                              },
                              file: file
                          })
promises.then(function(data){

  filecount = filecount + 1;

  filename = data.config.file.name;
  var fileout = {
    'imgfilename' : renamedFile
};
$http({
    url: Config.ApiURL + "/panel/saveimage",
    method: "POST",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    data: $.param(fileout)
}).success(function (data, status, headers, config) {
    loadimages();
    if(filecount == files.length)
    {
      $scope.imageloader=false;
      $scope.imagecontent=true;
  }

}).error(function (data, status, headers, config) {
    $scope.imageloader=false;
    $scope.imagecontent=true;
});

});
}



}
}
};
$scope.cancel = function() {
    $modalInstance.dismiss('cancel');

};
};
// UPLOAD PHOTO ///////////////////////////////////////////////////////////////////////////////////////////

$scope.stylepreview = function(content, amazonpath) {
  console.log(content)
  var modalInstance = $modal.open({
    templateUrl: 'stylepreview_homepage',
    size:'lg',
    controller: function($scope, $modalInstance, $state) {
      $scope.content = content;
      $scope.banner = Config.amazonlink + "/uploads/homepagebackground/" + amazonpath;
      $scope.cancel = function () {
        $modalInstance.dismiss();
      };
    }
  });
}


})
