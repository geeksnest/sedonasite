/**
 * Created by Apple on 9/15/15.
 */
app.controller('MemberReportsCTRL', function($scope, $state ,$q, $http, Config, MemberReport,$filter) {

var dt = new Date();
var today = dt.getFullYear() + "-" + (dt.getMonth() + 1) + "-" + dt.getDate();
$scope.enddate= today;


 $scope.searchbtn = function(startdate,enddate,status,payment,members) {


         var start = $filter('date')(startdate,'yyyy-MM-dd');
         var end = $filter('date')(enddate,'yyyy-MM-dd');

         $scope.start2 = start;
         $scope.stat = status;
         $scope.pay = payment;
         $scope.members2 = members;


         if (members=="")
         {
                paginate(10,0, start,end,status,payment,undefined);

         }

         else
         {

            paginate(10,0, start,end,status,payment,members);
         }



//print all search for pdf//
if (members=="")
{
MemberReport.searchreportdummy(start,end,status,payment,undefined,function (data){
for(var key in data){
  var n = {
                    title: data[key]['title'],
                    start: moment(data[key]['startdatetime']),
                    end: moment(data[key]['enddatetime']),
                    txtdate: moment(data[key]['scheduledate']).format('dddd, MMMM Do YYYY'),
                    txtstime: moment(data[key]['startdatetime']).format('h:mm:ss a'),
                    txtetime: moment(data[key]['enddatetime']).format('h:mm:ss a'),
                    txtname: data[key]['firstname'] + " " + data[key]['lastname'],
                    hourday: data[key]['hourday'],
                    status: data[key]['status'],
                    schedid: data[key]['schedid'],
                    info: data[key]['description'],
                    paymenttype: data[key]['paymenttype'],
                    updated_at: moment(data[key]['updated_at']).format('YYYY-MM-DD'),
                    created_at: moment(data[key]['created_at']).format('YYYY-MM-DD')
                }
                n['textColor'] = data[key]['colorlegend'];

                data[key] = n;
            }
            $scope.allsearch = data;
})

}

else
{
          MemberReport.searchreportdummy(start,end,status,payment,members,function (data){
            for(var key in data){
              var n = {
                title: data[key]['title'],
                start: moment(data[key]['startdatetime']),
                end: moment(data[key]['enddatetime']),
                txtdate: moment(data[key]['scheduledate']).format('dddd, MMMM Do YYYY'),
                txtstime: moment(data[key]['startdatetime']).format('h:mm:ss a'),
                txtetime: moment(data[key]['enddatetime']).format('h:mm:ss a'),
                txtname: data[key]['firstname'] + " " + data[key]['lastname'],
                hourday: data[key]['hourday'],
                status: data[key]['status'],
                invoiceno: data[key]['invoiceno'],
                schedid: data[key]['schedid'],
                info: data[key]['description'],
                paymenttype: data[key]['paymenttype'],
                updated_at: moment(data[key]['updated_at']).format('YYYY-MM-DD'),
                created_at: moment(data[key]['created_at']).format('YYYY-MM-DD')
              }
                n['textColor'] = data[key]['colorlegend'];

                data[key] = n;
              }
              $scope.allsearch = data;
              console.log(data);
            })
  }

//print all search for pdf//

}


    $scope.filterdate = {
        day : 0,
        month: 0,
        year: 0
    }

    var paginate = function(num,off,start,end,status,payment,members){
        $scope.loading = true;
       
        MemberReport.memberreportlist(num,off,start,end,status,payment,members,function (data){
     
            $scope.loading = false;
            $scope.maxSize = 10;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
          
            for(var key in data.data){
                var n = {
                    title: data.data[key]['title'],
                    start: moment(data.data[key]['startdatetime']),
                    end: moment(data.data[key]['enddatetime']),
                    txtdate: moment(data.data[key]['scheduledate']).format('dddd, MMMM Do YYYY'),
                    txtstime: moment(data.data[key]['startdatetime']).format('h:mm:ss a'),
                    txtetime: moment(data.data[key]['enddatetime']).format('h:mm:ss a'),
                    txtname: data.data[key]['firstname'] + " " + data.data[key]['lastname'],
                    hourday: data.data[key]['hourday'],
                    status: data.data[key]['status'],
                    invoiceno: data.data[key]['invoiceno'],
                    schedid: data.data[key]['schedid'],
                    info: data.data[key]['description'],
                    paymenttype: data.data[key]['paymenttype'],
                    updated_at: moment(data.data[key]['updated_at']).format('YYYY-MM-DD'),
                    created_at: moment(data.data[key]['created_at']).format('YYYY-MM-DD')
                }
                //var ndata = getStatus(data.data[key]['status'], data.data[key]['startdatetime'], data.data[key]['enddatetime']);
                //n['unattended'] = ndata.unattended;
                n['textColor'] = data.data[key]['colorlegend'];

                data.data[key] = n;
            }
            $scope.schedlist = data.data;
        });

    }
   
    paginate(10,0,undefined,undefined,undefined,undefined,undefined);
    $scope.setPage = function(bigCurrentPage)
    {
             paginate(10,bigCurrentPage,$scope.start2,$scope.enddate,$scope.stat,$scope.pay,$scope.member2);
 
    }


$scope.show = function (data)
{
    paginate(10,0,undefined,undefined,undefined,undefined,undefined);
    $scope.member2 = undefined;
    $scope.news = undefined;
    $scope.startdate = undefined;
    $scope.start2 = undefined;
    $scope.stat = undefined;
    $scope.pay = undefined;
}


    $scope.status = [
        'PENDING',
        'RESERVED',
        'VOID',
        'UNATTENDED',
        'DONE'
    ]

  $scope.payment = [
        'CASH',
        'CARD',
        'PAYPAL',
        'CHECK'
    ]



       MemberReport.memberslist(function (data){
        $scope.news = [];
         $scope.news.members= "";
         for(var key in data){
                var n = {
                    name: data[key]['firstname'] + " " + data[key]['lastname'],
                    id: data[key]['memberid']
                }
                data[key] = n;
            }
        $scope.members=data;
       
    });


//print all for pdf //
MemberReport.salesreportdummy(function (data){
for(var key in data){
                var n = {
                    title: data[key]['title'],
                    start: moment(data[key]['startdatetime']),
                    end: moment(data[key]['enddatetime']),
                    txtdate: moment(data[key]['scheduledate']).format('dddd, MMMM Do YYYY'),
                    txtstime: moment(data[key]['startdatetime']).format('h:mm:ss a'),
                    txtetime: moment(data[key]['enddatetime']).format('h:mm:ss a'),
                    txtname: data[key]['firstname'] + " " + data[key]['lastname'],
                    hourday: data[key]['hourday'],
                    status: data[key]['status'],
                    schedid: data[key]['schedid'],
                    info: data[key]['description'],
                    paymenttype: data[key]['paymenttype'],
                    updated_at: moment(data[key]['updated_at']).format('YYYY-MM-DD'),
                    created_at: moment(data[key]['created_at']).format('YYYY-MM-DD')
                }
                //var ndata = getStatus(data.data[key]['status'], data.data[key]['startdatetime'], data.data[key]['enddatetime']);
                //n['unattended'] = ndata.unattended;
                n['textColor'] = data[key]['colorlegend'];

                data[key] = n;
            }

            $scope.allentry = data;

})


    $scope.today = function() {
        $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
        $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
        return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    $scope.toggleMin = function() {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    
    $scope.toggleMin();

    $scope.open = function($event, opened) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope[opened] = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];




});