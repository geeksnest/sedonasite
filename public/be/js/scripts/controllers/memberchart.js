/**
 * Created by Apple on 9/15/15.
 */
app.controller('MemberChartCTRL', function($scope, $state ,$q, $http, Config,$filter,MemberChart,highchartsNG) {

//  $scope.limit=10;

// var limitStep = 999999999;
//   $scope.button = true;
//   $scope.button2 = false;
//  $scope.showmore = function() {
//      $scope.button = false;
//      $scope.button2 = true;
//      loadthat();
//      $scope.limit += limitStep;
//     };

//      $scope.showless = function() {
//      $scope.button = true;
//      $scope.button2 = false;
//      loadthat();
//      $scope.limit = 10;
//     };


// var loadthat = function(){

var paginate = function(num,off){    
    $scope.loading = true;

MemberChart.TopUser(num,off, function (data){
    console.log(data.total_items.length)
 $scope.loading = false;
            $scope.maxSize = 10;
            $scope.bigTotalItems = data.total_items.length;
            $scope.bigCurrentPage = data.index;

for(var key in data.data){
                    var count= data.data[key]['count']
                 var total= data.data[key]['totalcount']
                    var fnn= data.data[key]['firstname']
                    var ln= data.data[key]['lastname']
                    var n = {
                        count: count,
                        fn: fnn,
                        ln:ln
                    }
                    
                    
                    data.data[key] = n;
                }
                $scope.mem = data.data;
                
                
});

}

// }   

// loadthat();
 paginate(10,0);
    $scope.setPage = function(bigCurrentPage)
    {
             paginate(10,bigCurrentPage);
 
    }




MemberChart.UserStat(function (data){

$scope.totalsummary = data.totalsummary;
$scope.totaloffline = data.totalsummaryoff;
$scope.totalonline = data.totalsummaryon;


var months = data.months;
months.reverse();

  for(var key in data.off){
                 var offline= data.off[key]['off']
                 var n = [
                    offline
                 ]
                 data.off[key] = n;
             }
             var data1 = data.off;
             data1.reverse();

     for(var key in data.on){
         var online= data.on[key]['on']
         var n = [
            online
         ]
         data.on[key] = n;
     }
     var data2 = data.on;
    data2.reverse();

  for(var key in data.totaloff){
         var offline= data.totaloff[key]['off']
         var tot = data.totaloff[key]['tot']
        var percent = Math.floor((offline / tot) * 100);
         var n = [
            percent
         ]
         data.totaloff[key] = n;
     }
     var data3 = data.totaloff;
    data3.reverse();

for(var key in data.toot){
         var online= data.toot[key]['on']
         var tot = data.toot[key]['tot']
        var percent = Math.floor((online / tot) * 100);
         var n = [
            percent
         ]
         data.toot[key] = n;
     }
     var data4 = data.toot;
     data4.reverse();


$scope.UserStat = {
        title: {
            text: 'Statistics of Users'
        },
        subtitle: {
            text: 'Sedona Healing Arts',
           
        },
        options: {
            chart: {
            
                type: 'column'

            }
        },
       xAxis: [{
                categories: months
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value}'
                },
                title: {
                    text: 'PERCENTAGE'
                }
            }, { // Secondary yAxis
                title: {
                    text: 'TOTAL NUMBER'
                },
                labels: {
                    format: '{value}'
                },
                opposite: true
            }],
            tooltip: {
                formatter : function() {
                        // console.log(this);
                     return this.y + ' ' +  this.series.tooltipOptions.valueSuffix;
                }
            },
            series: [{
                name: 'OFFLINE(NUMBER OF USERS)',
                type: 'column',
                data: data1,
            },
            {
                name: 'ONLINE(NUMBER OF USERS)',
                type: 'column',
                data: data2
            },
             {
            type: 'spline',
            name: 'OFFLINE(PERCENTAGE)',
            data: data3,
            marker: {
                lineWidth: 2,
                lineColor: Highcharts.getOptions().colors[3],
                fillColor: 'white'
            },
             tooltip: {
                    valueSuffix: '%'
                }
        },

         {
            type: 'spline',
            name: 'ONLINE(PERCENTAGE)',
            data: data4,
            marker: {
                lineWidth: 2,
                lineColor: Highcharts.getOptions().colors[4],
                fillColor: 'white'
            },
             tooltip: {
                    valueSuffix: '%'
                }
        }


            ]

      


}

});




MemberChart.OverallUserStat(function (data){

  for(var key in data.off){
                 var offline= data.off[key]['off']
                 var n = [
                    offline
                 ]
                 data.off[key] = n;
             }
             var data1 = data.off;
             data1.reverse();

     for(var key in data.on){
         var online= data.on[key]['on']
         var n = [
            online
         ]
         data.on[key] = n;
     }
     var data2 = data.on;
    data2.reverse();


$scope.OverallUserStat = {
        title: {
            text: 'Overall Statistics of Users'
        },
        subtitle: {
            text: 'Sedona Healing Arts',
           
        },
        options: {
            chart: {
            
                type: 'column'

            }
        },
       xAxis: [{
                categories: ['Overall Statistics']
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value}'
                },
                title: {
                    text: 'TOTAL NUMBER'
                }
            }],
            tooltip: {
                formatter : function() {
                        // console.log(this);
                     return this.y + ' ' +  this.series.tooltipOptions.valueSuffix;
                }
            },
            series: [{
                name: 'OFFLINE',
                type: 'column',
                data: data1,
            },
            {
                name: 'ONLINE',
                type: 'column',
                data: data2
            }]

      


}

});


});