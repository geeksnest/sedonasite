/**
 * Created by Apple on 9/15/15.
 */
app.controller('EditTestCTRL', function($scope, $state ,$q, $http, Config,$filter,$modal,ViewTest,$stateParams) {

var loadthat = function () {
 ViewTest.view($stateParams.testid, function (data){
    $scope.indexko = $stateParams.testid;
                    for(var key in data.q){
                        var lock=key;
                        var n={
                            q_id:data.q[key]['q_id'],
                            question:data.q[key]['question'],
                            A:data.q[key]['choiceA'],
                            B:data.q[key]['choiceB'],
                            C:data.q[key]['choiceC'],
                            lock:lock
                        }
                        data.q[key]=n;
                    }
                    $scope.testlist = data.q;
                    
                    //RESULT
                for(var key in data.result){
                        var n={
                            id:data.result[key]['id'],
                            choice:data.result[key]['choice'],
                            description:data.result[key]['description'],
                            result:data.result[key]['result']
                        }
                        data.result[key]=n;
                    }
                    $scope.resultlist = data.result;


                    //SERVICES
                    for(var key in data.recommend){
                        var n={
                            id:data.recommend[key]['id'],
                            choice:data.recommend[key]['choice'],
                            services:data.recommend[key]['services'],
                            link:data.recommend[key]['link'],
                            products:data.recommend[key]['products']
                        }
                        data.recommend[key]=n;
                    }

                    $scope.recommendedlist = data.recommend;
                    console.log(data.recommend)
                    for(var key in data.testname){
                         var n={
                            testname:data.testname[key]['testname'],
                            testid:data.testname[key]['testid']
                        }
                        data.testname[key]=n;
                    }
                    $scope.test = data.testname;

                
        })
}

loadthat();

$scope.editq=function(q_id)
{
    var modalInstance = $modal.open({
            templateUrl: 'EditQuestion.html',
            controller: function($scope, $modalInstance,$state) {
                $scope.id = q_id;
                 ViewTest.viewQuestion(q_id, function (data){
                    $scope.option1 = data.question;
                    $scope.option2 = data.optionA;
                    $scope.option3 = data.optionB;
                    $scope.option4 = data.optionC;
                 })
                $scope.ok = function(Question,optionA,optionB,optionC,q_id) {
                    var asset = {
                        question:Question,
                        A:optionA,
                        B:optionB,
                        C:optionC,
                        q_id:q_id
                    }

                    ViewTest.EditQuestion(asset, function (data){
                            loadthat();
                    })

                    $modalInstance.dismiss('cancel');
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
           
            }
        }).result.then(function(){
              ViewTest.view($stateParams.testid, function (data){
                                console.log(data)
                        })
        });
}

$scope.deleteq = function(q_id)
{
    var modalInstance = $modal.open({
            templateUrl: 'DeleteQuestion.html',
            controller: function($scope, $modalInstance,$state) {
                $scope.id = q_id;
                
                $scope.ok = function(q_id) {
                    ViewTest.DeleteQuestion(q_id, function (data){
                        console.log(data)
                        loadthat();
                    })
                    $modalInstance.dismiss('cancel');
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
           
            }
        })
}
$scope.go = function ()
{
    $state.go('viewtests');
}


$scope.editr=function(id,testid,choice)
{
    var modalInstance = $modal.open({
            templateUrl: 'EditResult.html',
            controller: function($scope, $modalInstance,$state) {
                $scope.id = id;
                 $scope.testid = testid;
                 $scope.opt = choice;
                 ViewTest.viewResult(id, function (data){
                    $scope.option1 = data.choice;
                    $scope.option2 = data.result;
                    $scope.option3 = data.description;
                 })
                 var loadhere = function()
                 {
                       ViewTest.view(testid, function (data){
                           for(var key in data.recommend){
                            var n={
                                id:data.recommend[key]['id'],
                                choice:data.recommend[key]['choice'],
                                services:data.recommend[key]['services'],
                                link:data.recommend[key]['link'],
                                products:data.recommend[key]['products']
                            }
                            data.recommend[key]=n;
                        }

                        $scope.recommendedlist = data.recommend;

                    })
                 }
                 loadhere();

                $scope.ok = function(probability,result,description,id) {
                    var asset = {
                        probability:probability,
                        result:result,
                        description:description,
                        id:id
                    }

                    ViewTest.EditResult(asset, function (data){
                        console.log(data);
                        loadthat();
                    })
                    $modalInstance.dismiss('cancel');
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
           
            }
        })
}


$scope.edittestname=function(testid)
{
    var modalInstance = $modal.open({
            templateUrl: 'Testname.html',
            controller: function($scope, $modalInstance,$state) {
                $scope.id = testid;
                 ViewTest.viewTestname(testid, function (data){
                    console.log(data)
                    $scope.testname = data.testname;
                 })
                $scope.ok = function(testname,testid) {
                    console.log(testid);
                    var asset = {
                        testname:testname,
                        testid:testid
                    }

                    ViewTest.EditTestname(asset, function (data){
                        console.log(data);
                            loadthat();
                    })
                         $modalInstance.dismiss('cancel');
                    
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
           
            }
        }).result.then(function(){
              ViewTest.view($stateParams.testid, function (data){
                                console.log(data)
                        })
        });
}


var options = [];

$scope.addq = function(indexko)
{
        var modalInstance = $modal.open({
            templateUrl: 'AddQuestion.html',
            controller: function($scope, $modalInstance,$state) {
                $scope.submitQuestion = function(Question,A,B,C)
                {
                    if((A==undefined) || (B==undefined) || (C==undefined) || (Question==undefined))
                    {
                        $scope.req = 'Question and Choices are required';
                    }
                    else
                    {
                        
                        options.push({Question,A,B,C});
                        asset = {
                            option:options,
                            testid:indexko
                            }
                          
                            ViewTest.AddEditQ(asset, function (data){
                                console.log(data);
                                 loadthat();
                            })
                       
                         $('input').val(undefined);
                        $('#qq').val(undefined);
                        // loadresponse($scope.response);

                    }


                }
                
                $scope.cancel = function () {
                    $modalInstance.dismiss('close');
                };



            }
        }).result.then(function(data){
         loadthat();
        });

}

var prob = [];
$scope.check = function(indexko)
{
        var modalInstance = $modal.open({
            templateUrl: 'AddEditResult.html',
            controller: function($scope, $modalInstance,$state) {
                $scope.rs = [];
                $scope.ok = function(Probability,Result,Description)
                {
                    if((Probability==undefined) || (Result==undefined) || (Description==undefined))
                    {
                        $scope.req = 'Question and Choices are required';
                    }
                    else
                    {
                        
                        prob.push({Probability,Result,Description});
                        asset = {
                            prob:prob,
                            testid:indexko
                            }
                        
                            ViewTest.AddEditResult(asset, function (data){
                                console.log(data);
                                 loadthat();
                            })
                       
                        $scope.rs = {Description:""};
                         $('#qq').val(undefined);
                          $('#pr').val(undefined);
                        // loadresponse($scope.response);

                    }


                }
                
                $scope.cancel = function () {
                    $modalInstance.dismiss('close');
                };



            }
        }).result.then(function(data){
         loadthat();
        });

}


$scope.deleterr = function(id,choice,indexko)
{
    var modalInstance = $modal.open({
            templateUrl: 'DeleteResult.html',
            controller: function($scope, $modalInstance,$state) {
                $scope.id = id;
                $scope.choice = choice;
                $scope.testid = indexko;
                
                $scope.ok = function(id,choice,testid) {
                    ViewTest.DeleteResult(id,choice,testid, function (data){
                        console.log(data)
                        loadthat();
                    })
                    $modalInstance.dismiss('cancel');
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
           
            }
        })
}


var pro = [];
$scope.rec = function(index,probability,ind)
{

     // pro.push({choice:probability, services: [], products: []})
     var modalInstance = $modal.open({
            templateUrl: 'AddEditRec.html',
            controller: function($scope, $modalInstance, $state) {
                $scope.opt = probability;
                  var loadhere = function()
                 {
                       ViewTest.view(index, function (data){
                           for(var key in data.recommend){
                            var n={
                                id:data.recommend[key]['id'],
                                choice:data.recommend[key]['choice'],
                                services:data.recommend[key]['services'],
                                link:data.recommend[key]['link'],
                                products:data.recommend[key]['products']
                            }
                            data.recommend[key]=n;
                        }

                        $scope.recommendedlist = data.recommend;
                       console.log($scope.recommendedlist)
                    })
                 }
                 loadhere();
                 $scope.rs = [];
                $scope.submitServices = function(Services,Link)
                {
                    asset = {
                            probability:probability,
                            services:Services,
                            link:Link,
                            testid:index
                            }
                        
                            ViewTest.AddEditRec(asset, function (data){
                                console.log(data);
                                 loadthat();
                                 loadhere();
                                 $scope.rs = {Services:"",Link:""}
                                 $scope.form1.$setPristine(true);
                            })
                  
                }

                $scope.submitProducts = function(Products)
               { 
                 asset = {
                            probability:probability,
                            products:Products,
                            testid:index
                            }
                            console.log(asset)
                        
                            ViewTest.AddEditProd(asset, function (data){
                                console.log(data);
                                 loadthat();
                                 loadhere();
                                 $('#pr').val(undefined);
                            })

                
                }

               $scope.edits=function(id)
               {
                var modalInstance = $modal.open({
                    templateUrl: 'ManageServices.html',
                    controller: function($scope, $modalInstance,$state) {
                        $scope.id = id;
                        ViewTest.viewServices(id, function (data){
                            $scope.option1 = data.choice;
                            $scope.option2 = data.services;
                            $scope.option3 = data.link;
                            $scope.option4 = data.products;
                        })
                        $scope.ok = function(services,link,r_id) {
                            var asset = {
                                services:services,
                                link:link,
                                id:r_id
                            }

                            ViewTest.EditServices(asset, function (data){
                                console.log(data);
                                loadthat();
                                loadhere();
                            })

                            $modalInstance.dismiss('cancel');
                        };
                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                    }
                })
}

  $scope.editp=function(id)
               {
                var modalInstance = $modal.open({
                    templateUrl: 'ManageProducts.html',
                    controller: function($scope, $modalInstance,$state) {
                        $scope.id = id;
                        ViewTest.viewServices(id, function (data){
                            console.log(data);
                            $scope.option1 = data.choice;
                            // $scope.option2 = data.services;
                            // $scope.option3 = data.link;
                            $scope.option4 = data.products;
                        })
                        $scope.ok = function(products,id) {
                            var asset = {
                                products:products,
                                id:id
                            }

                            ViewTest.EditProducts(asset, function (data){
                                console.log(data);
                                loadthat();
                                loadhere();
                            })
                            $modalInstance.dismiss('cancel');
                        };
                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                    }
                })

    }

     $scope.deletess = function(id)
                {
                    var modalInstance = $modal.open({
                        templateUrl: 'DeleteResult.html',
                        controller: function($scope, $modalInstance,$state) {
                            $scope.id = id;
                            console.log(id)
                            $scope.ok = function(id) {

                                ViewTest.DeleteRec(id, function (data){
                                    console.log(data)
                                    loadthat();
                                    loadhere();
                                })

                                $modalInstance.dismiss('cancel');
                            };
                            $scope.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };

                        }
                    })
                }

                 $scope.deletepp = function(id)
                {
                    var modalInstance = $modal.open({
                        templateUrl: 'DeleteResult.html',
                        controller: function($scope, $modalInstance,$state) {
                            $scope.id = id;
                            console.log(id)
                            $scope.ok = function(id) {

                                ViewTest.DeleteProd(id, function (data){
                                    console.log(data)
                                    loadthat();
                                    loadhere();
                                })

                                $modalInstance.dismiss('cancel');
                            };
                            $scope.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };

                        }
                    })
                }

                $scope.ok = function() {
                       
                       $modalInstance.dismiss('cancel');
                };
                        $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                        };

            }

        }).result.then(function(data){
           $scope.res = [];
           $scope.res = data;
        });
}


$scope.sort = function(testid)
{
        var modalInstance = $modal.open({
            templateUrl: 'ArrangeQuestions.html',
            controller: function($scope, $modalInstance,$state) {
                         // $scope.list = option;
                         ViewTest.view(testid, function (data){
                            for(var key in data.q){
                                var lock=key;
                                var n={
                                    q_id:data.q[key]['q_id'],
                                    Question:data.q[key]['question']
                                }
                                data.q[key]=n;
                            }
                            $scope.list = data.q;
                            var opt = data.q;
                            $scope.sortableOptions = {
                                update: function(e, ui) {
                                  console.log("update");
                                  var logEntry = opt.map(function(i){
                                    return i.value;
                                }).join(', ');
                              },
                              stop: function(e, ui) {

                                  var logEntry = opt.map(function(i){
                                   return i.value;
                               })

                              }

                          };

             })

            $scope.ok = function () {
                var asset = {
                    list:$scope.list
                }
                     ViewTest.arrange(asset, function (data){
                        console.log(data)
                        loadthat();
                        $modalInstance.dismiss('close');
                     })

                };
                
                $scope.cancel = function () {
                    $modalInstance.dismiss('close');
                };

            }
        }).result.then(function(data){
         $scope.response = data;
        });

}

});