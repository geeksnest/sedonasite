/**
 * Created by Apple on 9/15/15.
 */
app.controller('CreateTestCTRL', function($scope, $state ,$q, $http, Config,$filter,$modal,CreateTest,Notification) {

var loadthis = function()
{
    CreateTest.max(function (data){
       for(var key in data){
        var maxid= data[key]['maxid']
    }
    id =  parseInt(maxid) + 1
    if (maxid==null)
    {
        $scope.testid = 1;
    }
    else
    {
        $scope.testid = id;
    }
    // console.log($scope.testid)
})
}

loadthis();

var option=[];
var probability=[];

$scope.wew = true;


$scope.Create = function(testid,testname,response,prob,firststep1)
{
var asset = {
    testid:testid,
    testname:testname,
    response:response,
    prob:prob
}

if((testname == undefined) || (response==undefined) || (prob == undefined) || (prob[0]['services'].length == 0) || (prob[0]['products'].length==0) )
{
        $scope.req = true;
}
else
    {
     $scope.req = false;
            $scope.processing = true;
            CreateTest.savetest(asset,function (data){
                        console.log(data);
                        if(data.result=='success'){
                            Notification.success({message: '<icon class="fa fa-check"></icon> The test has been saved succesfully.', positionY: 'bottom', positionX: 'right'});
                            resetForm();
                            firststep1.$setPristine();
                            $state.go('viewtests')
                           
                        }else{
                            console.log(data);
                            $scope.processing = false;
                            Notification.error({message: '<icon class="fa fa-check"></icon> ' + data.error, positionY: 'bottom', positionX: 'right'});
                        }
                        loadthis();
                    })
    
    }

}

var resetForm = function(){
    $scope.processing = false;
    $("#tab2").attr('disabled', true);
    $scope.tab2 = true;
    $scope.testname = undefined;
    $scope.steps={step1:true, step2:false, step3:false}
    option = [];
    probability = [];
    $scope.prob = [];
    $scope.res = [];
    $scope.response = [];

    }


$scope.add = function()
{
        var modalInstance = $modal.open({
            templateUrl: 'AddQuestion.html',
            controller: function($scope, $modalInstance,$state) {
                $scope.submitQuestion = function(Question,A,B,C)
                {
                    if((A==undefined) || (B==undefined) || (C==undefined) || (Question==undefined))
                    {
                        $scope.req = 'Question and Choices are required';
                    }
                    else
                    {
                        var value = option.length;
                        $('#a').val(undefined);
                        $('#b').val(undefined);
                        $('#c').val(undefined);
                        $('#qq').val(undefined);
                        option.push({Question,A,B,C,value});
                        $scope.response = option;
                        $scope.wew = false;
                        loadresponse($scope.response);

                    }


                }
                
                $scope.cancel = function () {
                    $modalInstance.dismiss('close');
                };

            }
        }).result.then(function(data){
         $scope.response = data;
        });

}

$scope.sort = function()
{
        var modalInstance = $modal.open({
            templateUrl: 'ArrangeQuestions.html',
            controller: function($scope, $modalInstance,$state) {
                         $scope.list = option;

                         $scope.sortableOptions = {
                            update: function(e, ui) {
                              console.log("update");
                              var logEntry = option.map(function(i){
                                return i.value;
                            }).join(', ');
                          },
                          stop: function(e, ui) {
                  // console.log("stop");   
                  var logEntry = option.map(function(i){
                   return i.value;
               })
                  console.log(option)

              }

            };
            $scope.ok = function () {
                    $modalInstance.dismiss('close');
                };
                
                $scope.cancel = function () {
                    $modalInstance.dismiss('close');
                };

            }
        }).result.then(function(data){
         $scope.response = data;
        });

}

var loadresponse =function (data) {
    $scope.response = data;
}


$scope.edit = function(index)
{
         var modalInstance = $modal.open({
            templateUrl: 'EditChoices.html',
            controller: function($scope, $modalInstance, index, $state) {
               $scope.indexko = index;
                $scope.option1 = option[index].Question;
                $scope.option2 = option[index].A;
                $scope.option3 = option[index].B;
                $scope.option4 = option[index].C;
                // console.log(option[index].Question);

                $scope.closeAlert = function(index) {
                    $scope.alerts.splice(index, 1);
                };;
                $scope.ok = function(Question,A,B,C,index) {
                    console.log(index);
                   option.splice(index, 1);
                   option.push({Question,A,B,C});
                   $scope.response = option;
                  
                   $scope.alerts = [{type: 'success', msg: 'Successfully Updated!'}];
                    $modalInstance.dismiss('cancel');
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            },
            resolve: {
                index: function() {
                    return index
                }
            }
        })


};

$scope.delete = function(index)
{ 
    option.splice(index, 1);
}


$scope.sub = true;
$scope.addprob = function(choice)
{
    probability.push({choice:choice, result:null, description:null, services: [], products: []})
    $scope.prob = probability;
    $('#pr').val(undefined);
}



var loadme = function()
{
    $scope.res = [];
    $scope.res = probability;
}

$scope.deleteresult = function(index)
{
    var modalInstance = $modal.open({
        templateUrl: 'ConfirmDeleteR.html',
        controller: function($scope, $modalInstance, $state) {
            $scope.rs = [];
            $scope.ok = function() {
                  probability.splice(index,1);
                  loadme();
                   $modalInstance.dismiss('cancel');
              };


           $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    }

})

}

$scope.check = function()
{
  var modalInstance = $modal.open({
    templateUrl: 'TestResult.html',
    controller: function($scope, $modalInstance, $state) {
        $scope.rs = [];
        $scope.ok = function(choice,Result,Description,form1) {
         probability.push({choice:choice, result:Result, description:Description, services: [], products: []})
         $('#pp').val(undefined);
         $('textarea').val(undefined);
         form1.$setPristine();
         $scope.rs = {Description:""};
         loadme();

     };


     $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

}

}).result.then(function(data){
   $scope.res = [];
   $scope.res = data;
   console.log(data)

});

}


$scope.rec = function(index,choice)
{

     var modalInstance = $modal.open({
            templateUrl: 'AddRec.html',
            controller: function($scope, $modalInstance, $state) {
                $scope.opt = choice;
                $scope.rservices = probability[index]['services'];
                $scope.rproducts = probability[index]['products'];
                $scope.submitServices = function(Services,Link)
                {
               
                    probability[index]['services'].push({Services,Link});
                    var svc = probability[index]['services'];
                   $scope.rservices = svc;
                   $('#srv').val(undefined);
                   $('#srv1').val(undefined);
                     $scope.req1 = false;

                  
               }
                $scope.deleteservices = function(indexko)
                {
               
                     probability[index]['services'].splice(indexko,1);
                }
               $scope.editservices = function(indexko)
               {
                    var modalInstance = $modal.open({
                    templateUrl: 'EditServices.html',
                     controller: function($scope, $modalInstance,$state) {
                        $scope.indexko = indexko;
                        $scope.option1 = probability[index]['services'][indexko].Services;
                        $scope.option2 = probability[index]['services'][indexko].Link;

                        $scope.ok = function(Services,Link,indexko) {

                            if((Services=="") || (Link==""))
                            {
                                $scope.req = "All fields are required"
                            }
                            else
                            {
                               probability[index]['services'].splice(indexko,1);
                               probability[index]['services'].push({Services,Link});
                               $scope.rservices = probability[index]['services'];
                               $modalInstance.dismiss('cancel');
                            }

                        };
                        $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                        };

                     }
                    });
               }

                $scope.submitProducts = function(Products)
               { 
                   probability[index]['products'].push({Products});
                   var svc = probability[index]['products'];
                   $scope.rproducts = svc;
                   $('#pr').val(undefined);
                             $scope.req2 = false;
                
               }

                $scope.deleteproducts = function(indexko)
                {
               
                     probability[index]['products'].splice(indexko,1);
                }

                $scope.editproducts = function(indexko)
               {
                    var modalInstance = $modal.open({
                    templateUrl: 'EditProducts.html',
                     controller: function($scope, $modalInstance,$state) {
                        $scope.indexko = indexko;
                        $scope.option1 = probability[index]['products'][indexko].Products;
                        $scope.ok = function(Products,indexko) {
                            if(Products=="")
                            {
                                $scope.req = "Product Field is required";
                            }
                            else
                            {
                               probability[index]['products'].splice(indexko,1);
                               probability[index]['products'].push({Products});
                               $scope.rproducts = probability[index]['products'];
                               $modalInstance.dismiss('cancel');


                            }

                        };
                        $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                        };

                     }
                    });
               }


                $scope.ok = function(services,products) {
                
                       console.log(probability[0]['products'].length);

                       if((probability[0]['services'].length == 0))
                        {
                             $scope.req1 = true;
                             $scope.req2 = false;
                          
                        }
                        else if((probability[0]['products'].length == 0))
                        {
                             $scope.req2 = true;
                             $scope.req1 = false;
                        }
                        else if((probability[0]['services'].length==0) && (probability[0]['products'].length==0))
                        {
                            $scope.req1 = true;
                            $scope.req2 = true;
                        }

                        else
                        {
                        $scope.res1 = probability;
                        $modalInstance.close($scope.res1);
                        }
                    
                       // console.log(probability);
                    

                };
                        $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                        };

            }

        }).result.then(function(data){
           $scope.res = [];
           $scope.res = data;
        });
}
 
   
$scope.editresult = function(index)
{
     var modalInstance = $modal.open({
            templateUrl: 'EditAllRec.html',
            controller: function($scope, $modalInstance, $state) {
                $scope.opt =  probability[index]['choice'];
                $scope.probability =  probability[index]['choice'];
                $scope.Result =  probability[index]['result'];
                $scope.Description =  probability[index]['description'];
                $scope.rservices = probability[index]['services'];
                $scope.rproducts = probability[index]['products'];

                $scope.ok = function(Result,Description) {
                
                       probability[index]['result'] = Result;
                       probability[index]['description'] = Description;
                       $scope.res1 = probability;
                       $modalInstance.close($scope.res1);
                       // console.log(probability);
                    

                };
                        $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                        };

            }

        }).result.then(function(data){
           $scope.res = [];
           $scope.res = data;
        });
}



});