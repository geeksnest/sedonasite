/**
 * Created by Apple on 9/15/15.
 */
app.controller('TestMemberCTRL', function($scope, $state ,$q, $http, Config,$filter,$modal,ViewTest) {
$scope.keyword=null;
$scope.loading = false;
var keyword = null;
var num = 10;
var off = 1;
var loadthat = function (num, off, keyword) {

ViewTest.testmemberlist(num, off, keyword,function (data){
    $scope.loading = true;
        for(var key in data.data){
            
                       var n={
                      
                            name:data.data[key]['fname'] + " "+ data.data[key]['lname'],
                            lname:data.data[key]['lname'],
                            email:data.data[key]['email'],
                            date:data.data[key]['date'],
                            id:data.data[key]['mem_id']
                        }
                        data.data[key]=n;
                    }
                    $scope.testlist = data.data;
                    $scope.maxSize = 10;
                    $scope.bigTotalItems = data.total_items;
                    $scope.bigCurrentPage = data.index;
                    $scope.loading = false;

     })

}

loadthat(num, off, keyword);

 $scope.search = function (keyword) {
        var off = 0;
        $scope.keyword = keyword;
        loadthat(num, off, keyword);
        $scope.searchtext='';
    }

    $scope.clear = function(){
        $scope.keyword=null;
        loadthat(num, off, keyword);
    }

$scope.setPage = function (pageNo) {
        loadthat(num, pageNo, keyword);
     
    };
$scope.view = function(testid)
{
         var modalInstance = $modal.open({
            templateUrl: 'ViewTest.html',
            controller: function($scope, $modalInstance,$state) {
                $scope.id = testid;
                $scope.ok = function(testid) {
                     $state.go('edittest', {testid:testid});
                     $modalInstance.dismiss('cancel');
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
           
            }
        })

};




$scope.manageresult = function(testid)
{
         var modalInstance = $modal.open({
            templateUrl: 'ManageResult.html',
            controller: function($scope, $modalInstance,$state) {
                $scope.id = testid;
                $scope.ok = function(testid) {
                     $state.go('editresult', {testid:testid});
                     $modalInstance.dismiss('cancel');
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
           
            }
        })

};

$scope.delete = function(id)
{
         var modalInstance = $modal.open({
            templateUrl: 'DeleteTest.html',
            controller: function($scope, $modalInstance,$state) {
                // console.log(testid)
                $scope.ok = function() {
                    console.log(id)
                    ViewTest.DeleteMem(id, function (data){
                        console.log(data);
                      loadthat(num, off, keyword);
                    })

                     $modalInstance.dismiss('cancel');
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
           
            }
        })

};


});

