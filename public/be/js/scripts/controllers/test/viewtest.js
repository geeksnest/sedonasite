/**
 * Created by Apple on 9/15/15.
 */
app.controller('ViewTestCTRL', function($scope, $state ,$q, $http, Config,$filter,$modal,ViewTest) {
$scope.keyword=null;
$scope.loading = false;
var keyword = null;
var num = 10;
var off = 1;
var loadthat = function (num, off, keyword) {

ViewTest.testname(num, off, keyword,function (data){
    $scope.loading = true;
        for(var key in data.data){
            
                       var n={
                      
                            testid:data.data[key]['testid'],
                            testname:data.data[key]['testname'],
                            status:data.data[key]['status']
                        }
                        data[key]=n;
                    }
                    $scope.testlist = data.data;
                    $scope.maxSize = 10;
                    $scope.bigTotalItems = data.total_items;
                    $scope.bigCurrentPage = data.index;
                    $scope.loading = false;

     })
}

loadthat(num, off, keyword);

 $scope.search = function (keyword) {
        var off = 0;
        $scope.keyword = keyword;
        loadthat(num, off, keyword);
        $scope.searchtext='';
    }

    $scope.clear = function(){
        $scope.keyword=null;
        loadthat(num, off, keyword);
    }

$scope.setPage = function (pageNo) {
        loadthat(num, pageNo, keyword);
        console.log(pageNo);
    };
$scope.view = function(testid)
{
         var modalInstance = $modal.open({
            templateUrl: 'ViewTest.html',
            controller: function($scope, $modalInstance,$state) {
                $scope.id = testid;
                $scope.ok = function(testid) {
                     $state.go('edittest', {testid:testid});
                     $modalInstance.dismiss('cancel');
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
           
            }
        })

};




$scope.manageresult = function(testid)
{
         var modalInstance = $modal.open({
            templateUrl: 'ManageResult.html',
            controller: function($scope, $modalInstance,$state) {
                $scope.id = testid;
                $scope.ok = function(testid) {
                     $state.go('editresult', {testid:testid});
                     $modalInstance.dismiss('cancel');
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
           
            }
        })

};

$scope.delete = function(testid)
{
         var modalInstance = $modal.open({
            templateUrl: 'DeleteTest.html',
            controller: function($scope, $modalInstance,$state) {
                $scope.id = testid;
                $scope.ok = function(testid) {

                    ViewTest.DeleteTest(testid, function (data){
                        console.log(data);
                      loadthat(num, off, keyword);
                    })

                     $modalInstance.dismiss('cancel');
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
           
            }
        })

};

 $scope.setstatus = function (testid,status) {
        var newstat;
        if(status==1){
            newstat = 0;
        }else{
            newstat = 1;
        }
        ViewTest.updatestatus(testid,newstat, function(data){
            var i = 1;
            setInterval(function(){
                i--;
                if(i == 0)
                {
                    loadthat(num, off, keyword);
                    $scope.currentstatusshow = 0;
                }
            },1000)

        });

    }


});

