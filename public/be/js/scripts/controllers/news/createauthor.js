'use strict';

/* Controllers */

app.controller('Createauthor', function($scope, $state, Upload ,$q, $http, Config, Createauthor, $modal, $anchorScroll, $filter){

    $scope.imageloader=false;
    $scope.imagecontent=true;

    $scope.author = {
      name: ""
    };

    var oriauthor = angular.copy($scope.author);


    $scope.cutlink = function convertToSlug(Text)
    {
        var texttocut = Config.amazonlink + '/uploads/saveauthorimage/';
        $scope.author.photo = Text.substring(texttocut.length); 
    }


    $scope.saveAuthor = function(author)
    {
        
        $scope.alerts = [];

        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };

        $scope.isSaving = true;
        author.since = $filter('date')(author.since,'yyyy-MM-dd');
        $http({
            url: Config.ApiURL + "/news/createauthor",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(author)
        }).success(function (data, status, headers, config) {
            console.log("aw");
            $scope.isSaving = false;
            $scope.alerts.push({type: 'success', msg: 'Author successfully saved!'});
            $scope.author = angular.copy(oriauthor);
            $scope.formpage.$setPristine();
            $anchorScroll();
        }).error(function (data, status, headers, config) {
            scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
        });
     
    }

    $scope.alertss = [];

    $scope.closeAlerts = function (index) {
        $scope.alertss.splice(index, 1);
    };

    
    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.author.since = moment().format('YYYY-MM-DD');

    //media
    $scope.media = function(type){
        var modalInstance = $modal.open({
            templateUrl: 'mediagallery.html',
            controller: function($scope, $modalInstance,Config, type) {
                $scope.imageloader=false;
                $scope.imagecontent=true;
                $scope.directory = 'saveauthorimage';
                $scope.videoenabled=false;
                $scope.mediaGallery = 'images';
                $scope.invalidvideo = false;
                $scope.currentSelected = '';
                $scope.currentDeleting = '';
                $scope.type = type;
                $scope.s3link = Config.amazonlink;
                console.log($scope.s3link);

                $scope.set = function(id){
                    $scope.currentSelected = id;
                    $scope.contentvideo = id;
                    if(type=='content'){
                        $scope.copy();
                    }
                }


                $scope.returnImageThumb = function(){
                    return function (item) {
                        if(item){
                            return Config.amazonlink + "/uploads/saveauthorimage/" + item;
                        }else{
                            return item;
                        }
                    };
                }

                var loadimages = function() {
                    Createauthor.loadimages(function(data){
                        console.log(data);
                        $scope.imagelist = data;
                        $scope.imagelength = data.length;
                        });
                }

                loadimages();

                $scope.$watch('files', function () {
                    $scope.upload($scope.files);

                });

                $scope.upload = function (files)
                {

                    var filename
                    var filecount = 0;
                    if (files && files.length)
                    {
                        $scope.imageloader=true;
                        $scope.imagecontent=false;

                        for (var i = 0; i < files.length; i++)
                        {
                            var file = files[i];

                            if (file.size >= 2000000)
                            {
                                $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
                                filecount = filecount + 1;

                                if(filecount == files.length)
                                {
                                    $scope.imageloader=false;
                                    $scope.imagecontent=true;
                                }


                            }
                            else

                            {

                                var promises;

                                promises = Upload.upload({

                                    url: Config.amazonlink, //S3 upload url including bucket name
                                    method: 'POST',
                                    transformRequest: function (data, headersGetter) {
                                        //Headers change here
                                        var headers = headersGetter();
                                        delete headers['Authorization'];
                                        return data;
                                    },
                                    fields : {
                                        key: 'uploads/saveauthorimage/' + file.name, // the key to store the file on S3, could be file name or customized
                                        AWSAccessKeyId: Config.AWSAccessKeyId,
                                        acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                                        policy: Config.policy, // base64-encoded json policy (see article below)
                                        signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                        "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                    },
                                    file: file
                                })
                                promises.then(function(data){

                                    filecount = filecount + 1;
                                    filename = data.config.file.name;
                                    var fileout = {
                                        'imgfilename' : filename
                                    };
                                    Createauthor.saveImage(fileout, function(data){
                                        loadimages();
                                        if(filecount == files.length)
                                        {
                                            $scope.imageloader=false;
                                            $scope.imagecontent=true;
                                        }
                                    });
                                });
                            }

                        }
                    }
                };

                $scope.deletenewsimg = function (dataimg, $event)
                {
                    var fileout = {
                        'imgfilename' : dataimg
                    };
                    Createnews.deleteImage(fileout, function(data) {
                        loadimages();

                    });
                    $event.stopPropagation();
                }

                $scope.copy = function() {
                    console.log($scope.contentvideo);
                    var text = '';
                    if($scope.contentvideo.filename){
                        text = Config.amazonlink + '/uploads/saveauthorimage/' + $scope.contentvideo.filename;
                    }else if($scope.contentvideo.videoid){
                        text = $scope.contentvideo.video;
                    }
                    var pp = window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
                    if(pp != "" && pp !== null) {
                        $modalInstance.dismiss('cancel');
                    }
                }

                $scope.ok = function(category) {
                    $modalInstance.close($scope.contentvideo);
                }

                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                }

            },
            resolve: {
                type: function(){
                    return type;
                }
            },
            windowClass: 'xxx-modal-window'
        }).result.then(function(res) {
                console.log("============================");
                console.log(res);
                if(res.filename){
                    $scope.author.photo = res.filename;
                    $scope.vidpath = '';
                    $scope.author.featuredthumbtype='image';
                    $scope.author.featuredthumb = res.filename;
                }else if(res.videoid){
                    $scope.author.imagethumb = '';
                    $scope.author.featuredthumbtype='video';
                    $scope.vidpath=$sce.trustAsHtml(res.video);
                    $scope.author.featuredthumb = res.video;
                }
            });
    }
});
