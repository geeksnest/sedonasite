'use strict';

/* Controllers */

app.controller('Tags', function($scope, $state, $q, $http, Config, $stateParams, $modal, Tags, $timeout) {

  $scope.keyword = null;
  $scope.loading = false;

  $scope.alerts = [];

  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };

  var removeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };

  $scope.editcategoryshow = false;


  $scope.data = {};
  var num = 10;
  var off = 1;
  var keyword = null;
  var paginate = function(num, off, keyword) {
    $scope.loading = true;
    Tags.list(num, off, keyword, function(data) {
      $scope.loading = false;
      $scope.data = data;
      $scope.maxSize = 10;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    });
  }

  paginate(num, off, keyword);

  $scope.search = function(searchkeyword) {
    var off = 0;
    $scope.keyword = searchkeyword;
    paginate(num, off, $scope.keyword);
    $scope.searchtext = '';
  }
  $scope.setPage = function(pageNo) {
    off = pageNo;
    paginate(10, pageNo, $scope.keyword);
  };
  $scope.clear = function() {
      $scope.keyword = null;
      paginate(10, 1, null);
    }
    /* Add Tags*/
  $scope.addtags = function() {
    var modalInstance = $modal.open({
      templateUrl: 'tagsAdd.html',
      controller: function($scope, $modalInstance) {
        var tagsslugs = '';
        console.log('addtags');

        $scope.ontagstitle = function convertToSlug(Text) {
          if (Text == null) {
            $scope.alerts = [{
              type: 'danger',
              msg: 'Tag Title is required.'
            }];
          } else {
            var text1 = Text.replace(/[^\w ]+/g, '');
            tagsslugs = angular.lowercase(text1.replace(/ +/g, '%20'));
          }
        }

        $scope.ok = function(tags) {
          Tags.addtags(tags, function(data) {
            if (data.hasOwnProperty('error')) {
              if (data.error.hasOwnProperty('existTags')) {
                $scope.alerts = [{
                  type: 'danger',
                  msg: 'Tag Title already exists.'
                }];
              }
            } else {
              $scope.alerts = [{
                type: 'success',
                msg: 'Tag Title had been added.'
              }];
              $timeout(function() {
                paginate(10, 1, $scope.keyword);
                $modalInstance.dismiss('cancel');
              }, 1000);
            }
          });
          tags['slugs'] = tagsslugs;
        }
        $scope.cancel = function() {
          $modalInstance.dismiss('cancel');
        }

      },
      resolve: {}
    });
  }

  var loadalert = function() {
    $scope.alerts.push({
      type: 'success',
      msg: 'Tag successfully Deleted!'
    });
  }

  $scope.tagsDelete = function(id) {
    var modalInstance = $modal.open({
      templateUrl: 'tagsDelete.html',
      controller: function($scope, $modalInstance, id, $state) {

        var datalength = 0;

        var getconflict = function() {
          Tags.conflicts(id, function(data) {
            $scope.catconflict = data.dataconflict;
            $scope.tags = data.newstags;
            datalength = data.dataconflict.length
            if (datalength > 0) {
              $scope.viewconflicts = true;
              $scope.disabledelete = true;
            } else {
              $scope.viewconflicts = false;
              $scope.disabledelete = false;
            }
          });
        }

        getconflict();

        $scope.editTag = function(news) {
            if(news.id != undefined){
                Tags.updateconflict(news, function(){
                    getconflict();
                });
            }else {
                console.log("undefined");
            }
        }

        $scope.ok = function() {
          removeAlert();
          var tags = {
            'tags': id
          };
          Tags.delete(id, function(data) {
            paginate(num, off, $scope.keyword);
            $modalInstance.close();
            loadalert();
          });
        }

        $scope.cancel = function() {
          $modalInstance.dismiss('cancel');
        }
      },
      resolve: {
        id: function() {
          return id
        }
      }
    });
  }


  $scope.updatetags = function(data, memid) {
    Tags.update(data, memid, function(data) {
      removeAlert();
      if (data.hasOwnProperty('error')) {
        $scope.alerts = [{
          type: 'danger',
          msg: 'Tag name already in use.'
        }];
      } else {
        $scope.alerts = [{
          type: 'success',
          msg: 'Tag successfully updated!'
        }];
      }
      paginate(num, off, $scope.keyword);
    })
  }

})
