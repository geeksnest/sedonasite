'use strict';

/* Controllers */

app.controller('Manageauthor', function($scope, $state ,$q, $http, Config, $log, Manageauthor, $interval, $modal){


  $scope.keyword=null;
  $scope.loading = false;

  $scope.alerts = [];
  console.log("===MANAGE AUTHOR===");

  $scope.closeAlert = function (index) {
    $scope.alerts.splice(index, 1);
  };

  $scope.data = {};
  var num = 10;
  var off = 1;
  var keyword = null;
  $scope.currentstatusshow = '';

  var paginate = function(num, off, keyword){
    $scope.loading = true;
    Manageauthor.list(num,off, keyword, function(data){
      $scope.data = data;
      $scope.maxSize = num;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
      $scope.loading = false;
    });
  }

  paginate(num, off, keyword);

  $scope.search = function (keyword) {
      $scope.keyword = keyword;
      paginate(10, 1, $scope.keyword);
      $scope.searchtext='';
  }

  $scope.clear = function(){
    $scope.keyword=null;
    paginate(10,1, null);
  }

  $scope.setPage = function (pageNo) {
    paginate(10, pageNo, $scope.keyword);
  };

  var successloadalert = function(){
    $scope.alerts = [{ type: 'success', msg: 'Author successfully Deleted!' }];

  }

  var errorloadalert = function(){
    $scope.alerts = [{ type: 'danger', msg: 'Something went wrong Author not Deleted!' }];
  }


  $scope.deleteauthor = function(authorid) {
    var modalInstance = $modal.open({
      templateUrl: 'authorDelete.html',
      controller: function($scope, $modalInstance, authorid) {
        var datalength = 0;

        var getconflict = function(){
            Manageauthor.loadauthornews(authorid, function(data){
                $scope.authconflict = data.dataconflict;
                $scope.authors = data.newsauthors;
                datalength = data.dataconflict.length
                if(datalength > 0){
                    $scope.viewconflicts = true;
                    $scope.disabledelete = true;
                }else {
                    $scope.viewconflicts = false;
                    $scope.disabledelete = false;
                }
            });
        }

        getconflict();

        $scope.alerts = [];
        
        $scope.editauthor = function(news) {
          if(news.authorid != undefined){
              Manageauthor.updatenewsauthors(news, function(){
                  getconflict();
              });
          }else {
              console.log("undefined");
          }
        }

        $scope.ok = function() {
          Manageauthor.delete(authorid, function(data){
            console.log("deleted");
            $scope.alerts = [];
            paginate(10, 1, $scope.keyword);
            $modalInstance.close();
            successloadalert();
          });
        };

        $scope.cancel = function() {
          $modalInstance.dismiss('cancel');
        };
      },
      resolve: {
        authorid: function() {
          return authorid
        }
      }
    });
  }


  $scope.editauthor = function(authorid) {
    $scope.authorid = authorid;
    console.log(authorid);
    var modalInstance = $modal.open({
      templateUrl: 'authorEdit.html',
      controller:  function($scope, $modalInstance, authorid, $state) {
        $scope.authorid = authorid;
        $scope.ok = function(authorid) {
          $scope.authorid = authorid;
          console.log(authorid);
          $state.go('editauthor', {authorid: authorid });
          $modalInstance.dismiss('cancel');
        };
        $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };
      },
      resolve: {
        authorid: function() {
          return authorid
        }
      }
    });
  }





})