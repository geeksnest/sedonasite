'use strict';

/* Controllers */

app.controller('Editnewscenter', function($scope, $state, Upload ,$q, $http, Config, Createnews, $stateParams){

    $scope.imageloader=false;
    $scope.imagecontent=true;

    $http({
        url: Config.ApiURL + "/news/newscenteredit/" + $stateParams.newsid ,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
        Createnews.loadcategory(function(data1){
         $scope.category = data1;
         $scope.news = data;
        });
        
    }).error(function (data, status, headers, config) {
        $scope.status = status;
    });

    $scope.news = {
      title: ""
    };

    var orinews = angular.copy($scope.news);

    $scope.onnewstitle = function convertToSlug(Text)
    {

            if(Text == null)
            {
           
            }
            else
            {
                var text1 = Text.replace(/[^\w ]+/g,'');
                $scope.news.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
            }
     
    }


    $scope.cutlink = function convertToSlug(Text)
    {
        var texttocut = Config.amazonlink + '/uploads/newsimage/';
        $scope.news.banner = Text.substring(texttocut.length); 
    }


    



    $scope.saveNews = function(news)
    {
        if (news.date == undefined) 
        {
            news.date = new Date(news.date2);
        }
        else
        {
            news.date = new Date(news.date);
        }

        if(news.newslocation == 'Main Site')
        {

            $scope.alerts = [];

            $scope.closeAlert = function (index) {
                $scope.alerts.splice(index, 1);
            };

            $scope.isSaving = true;

            $http({
                url: Config.ApiURL + "/news/edit",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(news)
            }).success(function (data, status, headers, config) {
                $scope.isSaving = false;
                $scope.alerts.push({type: 'success', msg: 'News successfully Updated!'});
            }).error(function (data, status, headers, config) {
                scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
            });
        }
        else
        {
            $scope.alerts = [];

            $scope.closeAlert = function (index) {
                $scope.alerts.splice(index, 1);
            };

            $scope.isSaving = true;

            $http({
                url: Config.ApiURL + "/newscenter/edit",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(news)
            }).success(function (data, status, headers, config) {
                $scope.isSaving = false;
                $scope.alerts.push({type: 'success', msg: 'News successfully Updated!'});
            }).error(function (data, status, headers, config) {
                scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
            });
        }
        

    }

    var loadimages = function() {

        $http({
            url: Config.ApiURL + "/news/listimages",
            method: "GET",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function(data) {
            $scope.imagelist = data;
        }).error(function(data) {
            $scope.status = status;
        });
    }

    loadimages();


    $scope.$watch('files', function () {
        $scope.upload($scope.files);
        
    });



    $scope.alertss = [];

        $scope.closeAlerts = function (index) {
            $scope.alertss.splice(index, 1);
        };

    $scope.upload = function (files) 
    {

        
        
        var filename
        var filecount = 0;
        if (files && files.length) 
        {
            $scope.imageloader=true;
            $scope.imagecontent=false;

            for (var i = 0; i < files.length; i++) 
            {
                var file = files[i];

                    if (file.size >= 2000000)
                    {
                        $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
                        filecount = filecount + 1;
                        
                        if(filecount == files.length)
                            {
                                $scope.imageloader=false;
                                $scope.imagecontent=true;
                            }
                        
    
                    }
                    else

                    {
                        
                   

                        var promises;
                            
                            promises = Upload.upload({
                                
                                url: Config.amazonlink, //S3 upload url including bucket name
                                method: 'POST',
                                transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                                },
                                fields : {
                                  key: 'uploads/newsimage/' + file.name, // the key to store the file on S3, could be file name or customized
                                  AWSAccessKeyId: Config.AWSAccessKeyId,
                                  acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
                                  policy: Config.policy, // base64-encoded json policy (see article below)
                                  signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                },
                                file: file
                            })
                                promises.then(function(data){

                                    filecount = filecount + 1;
                                    filename = data.config.file.name;

                                    $http({
                                        url: Config.ApiURL + "/news/saveimage/" + filename,
                                        method: "POST",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        data: $.param(files)
                                    }).success(function (data, status, headers, config) {
                                        loadimages();
                                        if(filecount == files.length)
                                        {
                                            $scope.imageloader=false;
                                            $scope.imagecontent=true;
                                        }
                                        
                                    }).error(function (data, status, headers, config) {
                                            $scope.imageloader=false;
                                            $scope.imagecontent=true;
                                    });
                                    
                                });


                 }


                            
            }
        }
        
          
    };


    $scope.today = function () {
        $scope.dt = new Date();
    };

    $scope.today();
    $scope.clear = function () {
    $scope.dt = null;
       
    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];


    


    

})