'use strict';

/* Controllers */

app.controller('Category', function($scope, $state ,$q, $http, Config, $stateParams, $modal, Category, $timeout){

    $scope.keyword=null;
    $scope.loading = false;

    $scope.alerts = [];

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.editcategoryshow = false;

    $scope.data = {};
    var num = 10;
    var off = 1;
    var keyword = null;
    var paginate = function(num,off, keyword) {
        $scope.loading = true;
        Category.list(num, off, keyword, function(data){
            $scope.data = data;
            $scope.maxSize = 10;
            $scope.loading = false;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
        });
    }

    paginate(num,off, keyword);

    $scope.search = function (searchkeyword) {
        var off = 0;
        $scope.keyword = searchkeyword;
        paginate(num,off, $scope.keyword);
        $scope.searchtext='';
        $scope.loading = false;
    }

    $scope.clear = function(){
        $scope.keyword=null;
        paginate(10,1, null);
    }

    $scope.setPage = function (pageNo) {
        paginate(num,pageNo, $scope.keyword);
        off = pageNo;
    };

    // Add Category
    $scope.addcategory = function() {
        var modalInstance = $modal.open({
            templateUrl: 'categoryAdd.html',
            controller: function($scope, $modalInstance) {
                var categoryslugs = '';
                $scope.oncategorytitle = function convertToSlug(Text)
                {
                    if(Text == null)
                    {
                        $scope.alerts =[{type: 'danger', msg: 'Category Title is required.'}];
                    }
                    else
                    {
                        var text1 = Text.replace(/[^\w ]+/g,'');
                        categoryslugs = angular.lowercase(text1.replace(/ +/g,'%20'));
                    }
                }

                $scope.ok = function(category) {
                    category['slugs'] = categoryslugs;
                    Category.addcategory(category, function(data){
                        if(data.hasOwnProperty('error')){
                            if(data.error.hasOwnProperty('existCategory')){
                                $scope.alerts =[{type: 'danger', msg: 'Category Title already exists.'}];
                            }
                        }else {
                            $scope.alerts =[{type: 'success', msg: 'Category Title added.'}];
                            $timeout(function(){
                                paginate(10,1, $scope.keyword);
                                $modalInstance.dismiss('cancel');
                            }, 1000);
                        }
                    })
                }

                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                }
            }
        })
    }

    var loadalert = function(){
        $scope.alerts = [{ type: 'success', msg: 'Category successfully Deleted!' }];
    }

    $scope.categoryDelete = function(id) {
        var modalInstance = $modal.open({
            templateUrl: 'categoryDelete.html',
            controller: function($scope, $modalInstance, id, $state) {
                var datalength = 0;

                var getconflict = function(){
                    Category.conflicts(id, function(data){
                        $scope.catconflict = data.dataconflict;
                        $scope.categories = data.newscategory;
                        datalength = data.dataconflict.length
                        if(datalength > 0){
                            $scope.viewconflicts = true;
                            $scope.disabledelete = true;
                        }else {
                            $scope.viewconflicts = false;
                            $scope.disabledelete = false;
                        }
                    });
                }

                getconflict();

                $scope.editcategory = function(news) {
                    if(news.catid != undefined){
                        Category.updateconflict(news, function(){
                            getconflict();
                        });
                    }else {
                        console.log("undefined");
                    }
                }

                $scope.ok = function() {
                    var news = {
                        'news': id
                    };
                    if($scope.viewconflicts == true){
                        $update
                    }
                    Category.delete(id, function(data){
                        paginate(10,1, $scope.keyword);
                        $modalInstance.close();
                        loadalert();
                    });
                }

                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                }
            },
            resolve: {
                id: function() {
                    return id;
                }
            }
        });
    }

    $scope.updatecategory = function(data,memid) {
        Category.update(data,memid, function(data){
            if(data.hasOwnProperty('error')){
                $scope.alerts = [{ type: 'danger', msg: 'Category name already in use!' }];
            }else{
                $scope.alerts = [{ type: 'success', msg: 'Category successfully updated!' }];
            }

            paginate(10,1,$scope.keyword);
        });
    }

})
