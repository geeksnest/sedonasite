/**
 * Created by Apple on 9/15/15.
 */
app.controller('OrderCharts', function($scope, $state ,$q, $http, Config,$filter,Order,highchartsNG) {
var d = new Date();
var year = d.getFullYear();
var month = new Array();
month[0] = "January";
month[1] = "February";
month[2] = "March";
month[3] = "April";
month[4] = "May";
month[5] = "June";
month[6] = "July";
month[7] = "August";
month[8] = "September";
month[9] = "October";
month[10] = "November";
month[11] = "December";
var months = month[d.getMonth()];

Order.TopMonthly(function (data){
for(var key in data.data){
                    var title= data.data[key]['prodname']
                    var n = [
                        title
                    ]
                    
                    data.data[key] = n;
                }
                var data1 = data.data;


 for(var lock in data.xdata){
                
                    var count= data.xdata[lock]['qty']
                    var m = [
                        count
                    ]
                    
                    data.xdata[lock] = m;
                }
                var data2 = data.xdata;


$scope.Top10Monthly = {
         options: {
            chart: {
            
                type: 'column'

            },
            series: {
                colorByPoint: true
            }
        },
        title: {
            text: 'Top 10 Products of'+" "+months+" "+year
        },
        subtitle: {
            text: 'Sedona Healing Arts'
        },
        xAxis: {
            categories: data1,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'NUMBER OF QUANTITY PURCHASED',
            data:data2,
            colorByPoint: true,
            pointWidth: 28,
            colors: [
            '#4572A7', 
            '#AA4643', 
            '#89A54E', 
            '#80699B', 
            '#3D96AE', 
            '#DB843D', 
            '#92A8CD', 
            '#A47D7C', 
            '#B5CA92',
            '#f7a35c'
            ],
        }] 


}

});



Order.MonthlySales(function (data){
$scope.totalReservation = data.totalreservation;
var months = data.months;
months.reverse();

 for(var key in data.data){
	 				var total= data.data[key]['tot']

	                var n = [
	                  	total
	                ]
	                data.data[key] = n;

	            }
	            var data2 = data.data;
	            data2.reverse();
          

$scope.MonthlySales = {
     chart: {
            type: 'area',
            spacingBottom: 30
        },
     plotOptions: {
        series: {
            connectNulls: false
        }
    },
      title: {
            text: 'Monthly Sales',
            x: -20 //center
        },
        subtitle: {
            text: 'Sedona Healing Arts',
            x: -20
        },
         xAxis: {
            categories: months
        },
        yAxis: {
            title: {
                text: 'Total Sales'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: '$'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
         series: [
                {
                    tooltip: {
                valuePrefix: '$'
            },
               	name: "TOTAL NUMBER OF Sales",
                data: data2
                }]
           
        

}

});



Order.DailyOrders(function (data){
    var date = data.date;
for(var key in data.data){
                    var title= data.data[key]['prodname']
                    var n = [
                        title
                    ]
                    
                    data.data[key] = n;
                }
                var data1 = data.data;


 for(var lock in data.xdata){
                
                    var count= data.xdata[lock]['data']
                    var m = [
                        count
                    ]
                    
                    data.xdata[lock] = m;
                }
                var data2 = data.xdata;

    if((data.data==null) && (data.xdata==null))
    {
        $scope.norecords = true;
        $scope.date = date;
        $scope.schedule = '0';
    }

$scope.DailyOrders = {
         options: {
            chart: {
            
                type: 'column'

            }
        },
        title: {
            text: 'Daily Orders'
        },
        subtitle: {
            text: 'Sedona Healing Arts'
        },
        xAxis: {
            categories: data1,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'NUMBER OF QUANTITY PURCHASED',
            data:data2,
            colorByPoint: true,
            pointWidth: 28
        }] 


}

});


Order.MonthlyOrders(function (data){
for(var key in data.data){
                    var title= data.data[key]['prodname']
                    var n = [
                        title
                    ]
                    
                    data.data[key] = n;
                }
                var data1 = data.data;


 for(var lock in data.xdata){
                
                    var count= data.xdata[lock]['data']
                    var m = [
                        count
                    ]
                    
                    data.xdata[lock] = m;
                }
                var data2 = data.xdata;


$scope.MonthlyOrders = {
         options: {
            chart: {
            
                type: 'column'

            }
        },
        title: {
            text: 'Monthly Orders'
        },
        subtitle: {
            text: 'Sedona Healing Arts'
        },
        xAxis: {
            categories: data1,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'NUMBER OF QUANTITY PURCHASED',
            data:data2,
            colorByPoint: true,
            pointWidth: 28
        }] 


}

});


Order.TopProducts(function (data){
for(var key in data.data){
                    var title= data.data[key]['prodname']
                    var n = [
                        title
                    ]
                    
                    data.data[key] = n;
                }
                var data1 = data.data;


 for(var lock in data.xdata){
                
                    var count= data.xdata[lock]['data']
                    var m = [
                        count
                    ]
                    
                    data.xdata[lock] = m;
                }
                var data2 = data.xdata;


$scope.TopProducts = {
         options: {
            chart: {
            
                type: 'column'

            }
        },
        title: {
            text: 'Top Products'
        },
        subtitle: {
            text: 'Sedona Healing Arts'
        },
        xAxis: {
            categories: data1,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'NUMBER OF QUANTITY PURCHASED',
            data:data2,
            colorByPoint: true,
            pointWidth: 28
        }] 


}

});


});