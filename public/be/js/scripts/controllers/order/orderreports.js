app.controller('Orderreports', function($scope, $state, Config, $modal, $anchorScroll, Order, $timeout, $filter){
    var filter = null;
    var num = 10;
    var off = 1;
    $scope.keyword = null
    $scope.maxSize = 10;
var dt = new Date();
var today = dt.getFullYear() + "-" + (dt.getMonth() + 1) + "-" + dt.getDate();
$scope.enddate=today;
    var loadlist = function(num, off,startdate,enddate,status,payment,members) {
        Order.loadreportlist(num, off,startdate,enddate,status,payment,members, function(data) {
            $scope.orders = data.orders;
            $scope.orders2 = data.allorders;
            $scope.orders3 = data.searchorders;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
            $scope.today = data.today;
            $scope.processing = data.processing;
            $scope.pending = data.pending;
            $scope.completed = data.completed
            console.log(data)
        });
    }

    loadlist(num, off, undefined, undefined,undefined,undefined,undefined);

 $scope.searchbtn = function(startdate,enddate,status,payment,members) {
    if (members=="") {members=undefined};
    if (payment=="CREDIT CARD") {payment="card";}
        else if (payment=="E-CHECK"){
            payment="check";
        }
    var start = $filter('date')(startdate,'yyyy-MM-dd');
    var end = $filter('date')(enddate,'yyyy-MM-dd');
    loadlist(num, off, start,end,status,payment,members);
    $scope.start2 = start;
    $scope.stat = status;
    $scope.pay = payment;
    $scope.member2 = members;
    console.log(start,end,status,payment,members);
 }
 $scope.show = function ()
{
    loadlist(num,off,undefined,undefined,undefined,undefined,undefined);
    $scope.news = {};
    $scope.news.status = undefined;
    $scope.news.payment = undefined;
    $scope.news.members = undefined;
    $scope.startdate = undefined;
    $scope.start2 = undefined;
    $scope.stat = undefined;
    $scope.pay = undefined;
    $scope.member2 = undefined;
}

    $scope.notif = function(filter) {
        loadlist(num, off, null, filter);
        $scope.keyword = filter;
    }
        $scope.setPage = function(bigCurrentPage)
    {
    var start = $filter('date')($scope.startdate,'yyyy-MM-dd');
    var end = $filter('date')($scope.enddate,'yyyy-MM-dd');
     loadlist(10,bigCurrentPage,start,end,$scope.stat,$scope.pay,$scope.member2);
    
    }

    $scope.view = function(id) {
        $state.go('vieworder', {id: id });
    }

    $scope.print = function() {
            $timeout(function() {
                var pdf = new jsPDF('p', 'pt', 'letter');
                
                source = $('#print')[0];
                specialElementHandlers = {
                  '#bypassme': function(element, renderer){
                    return true
                  }
                }
                margins = {
                    top: 50,
                    left: 60,
                    right: 0,
                    width: 645
                  };

                pdf.fromHTML(
                    source // HTML string or DOM elem ref.
                    , margins.left // x coord
                    , margins.top // y coord
                    , {
                      'width': margins.width // max width of content on PDF
                      , 'elementHandlers': specialElementHandlers
                    },
                    function (dispose) {
                    
                      // dispose: object with X, Y of the last line add to the PDF
                      //          this allow the insertion of new lines after html
                        pdf.save('SedonaHealingArts.pdf');
                    }
                )
            }, 500);
     
    }


   Order.memberslist(function (data){
        $scope.news = [];
         $scope.news.members= "";
         for(var key in data){
                var n = {
                    name: data[key]['firstname'] + " " + data[key]['lastname'],
                    id: data[key]['memberid']
                }
                data[key] = n;
            }
        $scope.members=data;
       
    });
    $scope.payment = [
        'CASH',
        'CREDIT CARD',
        'PAYPAL',
        'E-CHECK'
    ]

     $scope.status = [
        'PENDING',
        'PROCESSING',
        'VOID',
        'COMPLETED'
    ]


        $scope.today = function() {
        $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
        $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
        return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    $scope.toggleMin = function() {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    
    $scope.toggleMin();

    $scope.open = function($event, opened) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope[opened] = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

})
