'use strict';

/* Controllers */

app.controller('BookingCTRL', function($scope, $state ,$q, $http, Config, $timeout, $modal, $filter, Bookings){
    $scope.data = {};
    $scope.searchdate=null;
    $scope.keyword=null;
    $scope.loading = false;
    var num = 10;
    var off = 1;
    var keyword = null;
    var searchdate = null;

    var paginate = function(num, off,keyword,searchdate){
        $scope.loading = true;
        Bookings.paging(num, off, keyword , searchdate ,  function(data){
            $scope.data = data;
            $scope.loading = false;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
        });
    }

    paginate(num, off,keyword,searchdate);

    $scope.today = function () {
        $scope.dt = new Date();
    };

    $scope.today();
    $scope.clear = function () {
        $scope.dt = null;
    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.clear = function(){
        $scope.keyword=null;
        $scope.searchdate=null;
        paginate(10,1, $scope.keyword, $scope.searchdate);
    }

    $scope.setPage = function (pageNo) {
        paginate(10,pageNo, $scope.keyword, $scope.searchdate);
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');


    $scope.search = function (keyword, searchdate) {
        var off = 1;
        console.log('Searching...');
        if(searchdate){
            searchdate = $filter('date')(searchdate,'yyyy-MM-dd');
            console.log(searchdate);
        }
        $scope.keyword = keyword;
        $scope.searchdate = searchdate;

        paginate(num, off,$scope.keyword,$scope.searchdate);

        $scope.searchtext = '';
        $scope.searchdateelem = '';
        $scope.formbooking.$setPristine();

    }

    var bookingCTRL = function($scope, $modalInstance, bookId, $state) {
        $scope.booking = '';
        $scope.bookingrep = '';
        $scope.message = '';
        $scope.saving = false;
        Bookings.get(bookId, function(data){
            $scope.booking = data[0];
            console.log($scope.booking);
            if($scope.booking.status == 'new'){
                Bookings.read(bookId);
            }
            Bookings.getreplies(bookId, function(data){

                $scope.bookingrep = data;
            });
        });
        $scope.objectKeys = function(data){
            return Object.keys(data);
        }
        $scope.sendreply = function(data){
            $scope.saving = true;
            Bookings.reply(data, bookId, function(data){
                console.log("Clearing MEssage");
                $scope.message = '';
                Bookings.getreplies(bookId, function(data){

                    $scope.saving = false;
                    $scope.bookingrep = data;

                });
            });
        }

        $scope.ok = function() {
            $scope.saving = true;

        };
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }

    $scope.replyBook = function(bookId){
        console.log('Modal Reply');
            var modalInstance = $modal.open({
                templateUrl: 'replyBooking.html',
                controller: bookingCTRL,
                resolve: {
                    bookId: function() {
                        return bookId
                    }
                }
            }).result.finally(function(){
                    $state.go($state.current, {}, {reload: true});
                });
    }

    var deleteBookingCTRL = function($scope, $modalInstance, bookId, $state) {
        $scope.msg='Are you sure you want to delete this booking?';
        $scope.showbuttons = true;
        $scope.ok = function(){
            Bookings.delete(bookId, function(result){
                console.log(result);
                if(result.hasOwnProperty('success')){
                    $scope.msg='Booking has been deleted.';
                    $timeout(function(){
                        $scope.cancel();
                    }, 2000);
                }else{
                    $scope.msg='Error occured.';
                    $timeout(function(){
                        $scope.cancel();
                    }, 2000);
                }
                $scope.showbuttons = false;
            });
        }
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }

    $scope.deleteBook = function(bookId){
        console.log('Modal Reply');

        var modalInstance = $modal.open({
            templateUrl: 'bookDelete.html',
            controller: deleteBookingCTRL,
            resolve: {
                bookId: function() {
                    return bookId
                }
            }
        }).result.finally(function(){
                $state.go($state.current, {}, {reload: true});
            });
    }

})
