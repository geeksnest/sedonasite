/**
 * Created by Apple on 5/14/15.
 */

// Flot Chart controller
app.controller('DashboardCtrl',function($scope, $state ,$q, $http, Config,$filter,DashboardCtrl,highchartsNG) {
    // $scope.d = [ [1,6.5],[2,6.5],[3,7],[4,8],[5,7.5],[6,7],[7,6.8],[8,7],[9,7.2],[10,7],[11,6.8],[12,7] ];

    // $scope.d0_1 = [ [0,7],[1,6.5],[2,12.5],[3,7],[4,9],[5,6],[6,11],[7,6.5],[8,8],[9,7] ];

    // $scope.d0_2 = [ [0,4],[1,4.5],[2,7],[3,4.5],[4,3],[5,3.5],[6,6],[7,3],[8,4],[9,3] ];

    // $scope.d1_1 = [ [10, 120], [20, 70], [30, 70], [40, 60] ];

    // $scope.d1_2 = [ [10, 50],  [20, 60], [30, 90],  [40, 35] ];

    // $scope.d1_3 = [ [10, 80],  [20, 40], [30, 30],  [40, 20] ];

    // $scope.d2 = [];

    // for (var i = 0; i < 20; ++i) {
    //     $scope.d2.push([i, Math.sin(i)]);
    // }

    // $scope.d3 = [
    //     { label: "iPhone5S", data: 40 },
    //     { label: "iPad Mini", data: 10 },
    //     { label: "iPad Mini Retina", data: 20 },
    //     { label: "iPhone4S", data: 12 },
    //     { label: "iPad Air", data: 18 }
    // ];

    // $scope.getRandomData = function() {
    //     var data = [],
    //         totalPoints = 150;
    //     if (data.length > 0)
    //         data = data.slice(1);
    //     while (data.length < totalPoints) {
    //         var prev = data.length > 0 ? data[data.length - 1] : 50,
    //             y = prev + Math.random() * 10 - 5;
    //         if (y < 0) {
    //             y = 0;
    //         } else if (y > 100) {
    //             y = 100;
    //         }
    //         data.push(y);
    //     }
    //     // Zip the generated y values with the x values
    //     var res = [];
    //     for (var i = 0; i < data.length; ++i) {
    //         res.push([i, data[i]])
    //     }
    //     return res;
    // }

    // $scope.d4 = $scope.getRandomData();

DashboardCtrl.MonthlyReservation(function (data){

$scope.totalReservation = data.totalreservation;
var months = data.months;
months.reverse();

 for(var key in data.data){
                    var total= data.data[key]['count']

                    var n = [
                        total
                    ]
                    data.data[key] = n;

                }
                var data2 = data.data;
                data2.reverse();
          

$scope.MonthlyReservation = {
     chart: {
            type: 'area',
            spacingBottom: 30
        },
     plotOptions: {
        series: {
            connectNulls: false
        }
    },
      title: {
            text: 'Monthly Reservation',
            x: -20 //center
        },
        subtitle: {
            text: 'Sedona Healing Arts',
            x: -20
        },
         xAxis: {
            categories: months
        },
        yAxis: {
            title: {
                text: 'Total Reservation in Numbers'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: '$'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
         series: [
                {
                name: "TOTAL NUMBER OF RESERVATION",
                data: data2
                }]
           
        

}

});

    DashboardCtrl.TotalNews(function (data){
        $scope.totalnews = data;
    })


    DashboardCtrl.DailySchedule(function (data){
    var months = data.months;
    var reserve = data.reserve;
    var pending = data.pending;
    var voiddata = data.voiddata;
    var done = data.done;
    var unattended = data.unattended;

    if((reserve==null) && (pending==null) && (voiddata==null) && (done==null) && (unattended==null  ))
    {
        $scope.norecords = true;
        $scope.date = months;
        $scope.schedule = '0';
    }
    else
    {
       var all = parseInt(reserve) + parseInt(pending) + parseInt(voiddata) + parseInt(done) + parseInt(unattended);
       $scope.schedule = all;
       $scope.norecords = false;
       $scope.date = '';
    }
    $scope.DailySchedule = {

   options: {
            chart: {
            
                type: 'column'

            }
        },
        title: {
            text: 'Daily Schedule'
        },
        subtitle: {
            text: 'Sedona Healing Arts'
        },
        xAxis: {
            categories: months,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total Number'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'RESERVED',
            color: '#7266ba',
            data: reserve

        }, {
            name: 'PENDING',
            color: '#23b7e5',
            data: pending

        }, {
            name: 'VOID',
            color: '#f05050',
            data: voiddata

        }, {
            name: 'UNATTENDED',
            color: '#fad733',
            data: unattended

        },{
            name: 'DONE',
            color: '#27c24c',
            data: done

        }]


}

    })


    DashboardCtrl.DailyReservation(function (data){
    var months = data.months;
    var reserve = data.reserve;
    var pending = data.pending;
    var voiddata = data.voiddata;
    var done = data.done;
    var unattended = data.unattended;


    if((reserve==null) && (pending==null) && (voiddata==null) && (done==null) && (unattended==null  ))
    {
        $scope.norecords2 = true;
        $scope.date2 = months;
        $scope.reservation='0';
    }
    else
    {
      var all = parseInt(reserve) + parseInt(pending) + parseInt(voiddata) + parseInt(done) + parseInt(unattended);
      $scope.reservation=all;
      $scope.norecords2 = false;
      $scope.date2 = '';
    }
    $scope.DailyReservation = {

   options: {
            chart: {
            
                type: 'column'

            }
        },
        title: {
            text: 'Daily Reservation'
        },
        subtitle: {
            text: 'Sedona Healing Arts'
        },
        xAxis: {
            categories: months,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total Number'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'RESERVED',
            color: '#7266ba',
            data: reserve

        }, {
            name: 'PENDING',
            color: '#23b7e5',
            data: pending

        }, {
            name: 'VOID',
            color: '#f05050',
            data: voiddata

        }, {
            name: 'UNATTENDED',
            color: '#fad733',
            data: unattended

        },{
            name: 'DONE',
            color: '#27c24c',
            data: done

        }]


}

    })



});