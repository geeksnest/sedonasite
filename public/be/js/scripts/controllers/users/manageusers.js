app.controller('UserListCtrl', function($http, $scope, $stateParams, $modal, $sce, Config, User, $anchorScroll) {
  //LIST ALL USERS
  var num = 10;
  var off = 1;
  var keyword = null;
  $scope.loading = false;
  var adminid = $stateParams.userid;
  $scope.useridArray = [];
  $scope.checkedAll = false;

  var paginate = function(off, num, keyword) {
    $scope.loading = true;
    User.list(num, off, keyword, adminid, function(data) {
      $scope.data = data;
      $scope.total_items = data.total_items;
      if (keyword !== undefined && off === 0) {
        $scope.keywordshow = true;
        $scope.keyword = keyword;
      }
      $scope.maxSize = 10;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
      $scope.loading = false;

      angular.forEach($scope.data.data, function(data) {
        data["checked"] = false;
      });

      console.log($scope.data);
    });
  };

  $scope.search = function(keyword) {
    if (keyword === "") {
      keyword = undefined;
    }
    var off = 0;
    $scope.searchtext = undefined;
    paginate(off, num, keyword);
  };

  $scope.clear = function() {
    paginate(off, num, undefined);
    $scope.keywordshow = false;
  };
  $scope.setPage = function(off, keyword) {
    paginate(off, keyword);
  };

  paginate(off, keyword);
  //END USER LISTING


  //DELETE USER
  $scope.delete = function(user) {
    var modalInstance = $modal.open({
      templateUrl: 'userDelete.html',
      controller: dltCTRL,
      resolve: {
        dltuser: function() {
          return user;
        }
      }
    });
  };

  $scope.alerts = [];
  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };
  var alertme = function(data) {
    console.log(data);
    $scope.alerts.splice(0, 1);
    $scope.alerts.push({
      type: data.type,
      msg: data.msg
    });
  };
  var dltCTRL = function($scope, $modalInstance, dltuser) {
    $scope.dltuser = dltuser;

    $scope.ok = function(dltuser) {
      User.delete(dltuser, function(data) {
        paginate(off, keyword);
        $modalInstance.dismiss('cancel');
        alertme(data);
        $scope.keywordshow = false;
      });
    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  };
  //UPDATE USER
  $scope.update = function(user) {
    var modalInstance = $modal.open({
      templateUrl: 'userUpdate.html',
      controller: updateCTRL,
      resolve: {
        update: function() {
          return user;
        }
      }
    });
  };
  var updateCTRL = function($scope, $modalInstance, update, $state) {
    $scope.update = update;
    $scope.ok = function(update) {
      $state.go('updateuser', {
        userid: update
      });
      $modalInstance.dismiss('cancel');

    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  };

  $scope.setstatus = function(status, id, keyword) {
    var newstat;
    if (status == 1) {
      newstat = 0;
    } else {
      newstat = 1;
    }

    User.updatestatus(id, newstat, function(data) {
      $scope.currentstatusshow = id;
      var i = 2;
      setInterval(function() {
        i--;
        if (i == 0) {
          paginate(10, 1, keyword);
          $scope.currentstatusshow = 0;
          $scope.keywordshow = false;
        }
      }, 1000);
    });
  };

  //Delete USERS
  $scope.toggleCheckAll = function() {
    if ($scope.checkedAll) {
      angular.forEach($scope.data.data, function(data) {
        data.checked = true
        $scope.usersArray(data);
      });
    } else {
      angular.forEach($scope.data.data, function(data) {
        data.checked = false;
        $scope.usersArray(data);
      });
    }
  }

  $scope.usersArray = function(data) {
    if (data.checked && $scope.useridArray.indexOf(data.id) == -1) {
      $scope.useridArray.push(data.id);
    } else if (!data.checked) {
      $scope.useridArray.splice($scope.useridArray.indexOf(data.id), 1);
    }
  }

  var deleteUsersCtrl = function($scope, $modalInstance, id, $state){
    console.log(id);
    $scope.ok = function() {
      User.deletemultiple(id, function(data) {
        $modalInstance.dismiss('cancel');
        $scope.useridArray = [];
        $scope.checkedAll = false;
        paginate(off, keyword);
        alertme({ type : "success", msg : "User/s successfully deleted." });
        $anchorScroll();
      });
    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  }

  $scope.deleteUsers = function() {
    var modalInstance = $modal.open({
      templateUrl: 'deleteUsers.html',
      controller: deleteUsersCtrl,
      resolve: {
        id: function() {
          return $scope.useridArray;
        }
      }
    })
  }

});
