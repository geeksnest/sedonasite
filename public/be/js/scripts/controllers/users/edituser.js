'use strict';

app.controller('UserUpdateCtrl', function ($http, $scope, $stateParams, $modal,$sce, Config, User, $filter, $anchorScroll) {
	// console.log($stateParams.userid);
	$scope.process = 0;
	$scope.amazonlink = Config.amazonlink;
	$scope.loader = false;
	var info = function(){
		User.info($stateParams.userid, function(data) {
			$scope.user = data;
			var str = data.task;
			str = str.split(',');
			$scope.role = ['Administrator','Page Manager','News Editor','Testimonial Editor','Booking Manager']
			$scope.user.userrole = str;
			var bd = data.bday;
			var split = bd.split('-');
			$scope.user.byear = split[0];
			$scope.user.bmonth = parseInt(split[1]);
			$scope.user.bday = split[2];
			if(data.status == 1){
				$scope.user.status = true;
			}else {
				$scope.user.status = false;
			}
			console.log($scope.user);
		});
	}

	info();

	//VALIDATE USERNAME
    $scope.chkusername = function(username) {
        User.usernameExists(username, $stateParams.userid, function(data) {
            $scope.validusrname = data.exists;
            // console.log(data.exists);
        });
    };


    //VALIDATE EMAIL
    $scope.chkemail = function(email) {
        User.emailExists(email, $stateParams.userid, function(data) {
            $scope.validemail = data.exists;
        });
    };
    
	$scope.prepare = function(file){
		console.log(file)
		$scope.alerts = [];
		$scope.closeAlert = function(index) {
			$scope.alerts.splice(index, 1);
		};
		if (file && file.length) {
			if(file.size >= 2000000){
	            console.log('File is too big!');
	            $scope.alerts = [{type: 'danger', msg: 'File ' + file.name + ' is too big'}];
	            $scope.file='';
	        }else {
				console.log("below maximum");
				// $scope.file = file;
				$scope.user.profile_pic_name = file;
				$scope.closeAlert();
	        }
		}
	}

	$scope.changepass =function(id,email){
    var modalInstance = $modal.open({
        templateUrl: 'changePass.html',
        controller: function($scope, $modalInstance, id, email){
        $scope.pwdconfirm = false;
        $scope.validpass = false;
        $scope.showvalidpass =false;
        $scope.minpass = false;
        $scope.alerts = [];
        $scope.loader = false;
          $scope.chkoldpass = function(password){
            if(password != ''){
              User.chkpass(password, $stateParams.userid, function(data) {
                $scope.validpass = data.valid;
                if(data.valid == false){
                  $scope.showvalidpass = true;
                }else {
                  $scope.showvalidpass = false;
                }
              });
            }else {
              $scope.showvalidpass = false;
              $scope.user = "";
              // delete $scope.user.password;
              // delete $scope.user.conpass;
              // delete $scope.user.oldpass;
              $scope.validpass = false;
            }
          };

          //VALIDATE PASSWORD length
          $scope.chkpass = function(password, conpass) {
            if(angular.isDefined(password)){
              var length = password.length;
              if (length >= 6 && length <= 20) {
                  $scope.pwd = false;
                  $scope.minpass = false;
                  if(conpass != password){
                      $scope.pwdconfirm = true;
                  }else {
                      $scope.pwdconfirm = false;
                  }
              } else {
                  if (conpass !== undefined && conpass == password) {
                      $scope.pwdconfirm = true;
                  } else {
                      $scope.pwdconfirm = false;
                  }
                  $scope.pwd = true;
                  $scope.minpass = true;
              }
            }
          };
          //VALIDATE PASSWORD IF MATCH
          $scope.confirmpass = function(password, confirmpass) {
              if (password != confirmpass) {
                  $scope.pwdconfirm = true;
              } else {
                  $scope.pwdconfirm = false;
              }
          };

          $scope.cancel = function(){
            $modalInstance.dismiss('cancel');
          }

          $scope.ok = function(user){
            user.id = id;
            user.email = email;
            $scope.loader = true;
            User.changepassword(user, function(data){
			$scope.alerts = [{type:data.type, msg: data.msg}];
            $scope.loader = false;
            $scope.user = "";
            });
          }
        },
        resolve: {
            id: function() {
                return id
            },
            email: function() {
                return email
            }
        }
    });
  }

	$scope.updateData = function(user){
		$scope.alerts = [];
		$scope.closeAlert = function(index) {
			$scope.alerts.splice(index, 1);
		};
		if ($scope.validusrname==true) {
             $("#usn").focus();
        }
        else if ($scope.validemail==true) {
            $("#email").focus();
        }
        else if ($scope.user.userrole==undefined || $scope.user.userrole=="" ) {
            $scope.nrole = true;
        }
        else if (!$scope.user.byear || !$scope.user.bmonth || !$scope.user.bday) {
            $scope.nbday = true;
        }
        else{
		var x = 1;
		if (user.profile_pic_name != null || user.profile_pic_name != undefined) {
			if(user.profile_pic_name.length == 1){
				var file = user.profile_pic_name;
				user.profile_pic_name = user.profile_pic_name[0].name; 

				if (file && file.length) {

					if (file.size >= 2000000){
						console.log('File is too big!');
						$scope.alerts = [{type: 'danger', msg: 'File ' + file.name + ' is too big'}];
						$scope.file='';
						$scope.userform.$setPristine();
						$anchorScroll();
					}else{
						$scope.loader = true;
						User.uploadpic(file, function(log) {
							$scope.process = log;
							if(log == 100 && x == 1){
								x = 2;
								user.bday = $filter('date')(user.bday,'yyyy-MM-dd');

								User.update(user, function(data){
									$scope.alerts = [{type:data.type, msg: data.msg}];
									$scope.process = 0;
									$scope.loader = false;
									$anchorScroll();
									info();
									// angular.element("#profpic").attr("ngf-default-src", "{[{amazonlink}]}/uploads/userimages/{[{user.profile_pic_name}]}");
								});
							}
						});
					}
				}
			}else {
				$scope.loader = true;
				User.update(user, function(data){
					$scope.alerts = [{type:data.type, msg: data.msg}];
					$scope.loader = false;
					$anchorScroll();
					info();
					$scope.userform.$setPristine();
				});
			}
	} // not null
	else{
		$scope.loader = true;
		User.update(user, function(data){
			$scope.alerts = [{type:data.type, msg: data.msg}];
			$scope.loader = false;
			$anchorScroll();
			info();
			$scope.userform.$setPristine();
		});
	}



	}


}

	//DATE PICKER
	$scope.today = function () {
    $scope.dt = new Date();
    };

    $scope.today();
    $scope.clear = function () {
    $scope.dt = null;
       
    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
});