app.controller('AddUserCtrl', function($scope, $http, User, Config, Upload, $anchorScroll, $modal, $filter, $timeout) {
   
    //VALIDATE USERNAME
    $scope.validusrname = false;
    $scope.validemail = false;
    $scope.pwdconfirm = false;
    $scope.base_url = Config.BaseURL;
    $scope.process = 0;
    $scope.loader = false;
    var userid = null;
    var minpass = false;

    $scope.chkusername = function(username) {
        User.usernameExists(username, userid, function(data) {
            $scope.validusrname = data.exists;
        });
    };

   
    //VALIDATE EMAIL
    $scope.chkemail = function(email) {
        User.emailExists(email, userid, function(data) {
            $scope.validemail = data.exists;
             // console.log($scope.validemail)
        });
    };
    //VALIDATE PASSWORD length
    $scope.chkpass = function(password, conpass) {
        if (password.length >= 6 && password.length <= 20) {
            if(password != conpass){
                $scope.pwdconfirm = true;
            }
            $scope.pwd = false;
            $scope.minpass = false;
        } else {
            if (conpass !== undefined && conpass == password) {
                $scope.pwdconfirm = true;
            } else {
                $scope.pwdconfirm = false;
            }
            $scope.pwd = true;
            $scope.minpass = true;
        }
        // console.log($scope.pwdconfirm);
    };
    //VALIDATE PASSWORD IF MATCH
    $scope.confirmpass = function() {
        var password = $('#password').val();
        var confirmpass = $scope.user.conpass;
        if (password != confirmpass) {
            $scope.pwdconfirm = true;
        } else {
            $scope.pwdconfirm = false;
        }
    };

    $scope.prepare = function(file) {
        $scope.alerts = [];
        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };
        if (file && file.length) {
            if (file.size >= 2000000) {
                console.log('File is too big!');
                $scope.alerts = [{
                    type: 'danger',
                    msg: 'File ' + file.name + ' is too big'
                }];
                $scope.file = '';
            } else {
                console.log("below maximum");
                $scope.file = file;
                $scope.closeAlert();
            }
        }
    }

   
    //SAVE DATA
    $scope.submitData = function(user, file) {
        // console.log($scope.user.userrole);
        if ($scope.validusrname==true) {
             $("#usn").focus();
        }
        else if ($scope.validemail==true) {
            $("#email").focus();
        }
        else if ($scope.pwdconfirm==true) {
            $("#conpass").focus();
        }
        else if ($scope.pwd==true) {
            $("#password").focus();
        }
        else if ($scope.user.userrole==undefined || $scope.user.userrole=="" ) {
            $scope.nrole = true;
        }
        else if (!$scope.user.byear || !$scope.user.bmonth || !$scope.user.bday) {
            $scope.nbday = true;
        }
        else{
             $scope.alerts = [];
             var x = 1;
             $scope.closeAlert = function(index) {
                $scope.alerts.splice(index, 1);
            };

            if (file && file.length) {

                if (file.size >= 2000000) {
                    console.log('File is too big!');
                    $scope.alerts = [{type:'danger', msg: 'File ' + file.name + ' is too big'}];
                    $scope.file = '';
                } else {
                    $scope.loader = true;
                    User.uploadpic(file, function(log) {
                        console.log(log);
                        $scope.process = log;
                        if (log == 100 && x == 1) {
                            x = 2;
                            user.profpic = file[0].name;
                                User.register(user, function(data) {
                                    console.log(data);
                                    $scope.loader = false;
                                    $scope.file = undefined;
                                    $scope.alerts = [{type:data.type, msg: data.msg}];
                                    $scope.userform.$setPristine();
                                    $scope.process = 0;
                                    $('input').val(undefined);
                                    $scope.user = "";
                                    $scope.nbday = false;
                                    $scope.nrole = false;
                                    $anchorScroll();
                                });

                                
                            }
                        });
                }
            }else{
                $scope.loader = true;
                User.register(user, function(data) {
                    console.log(data);
                        $scope.loader = false;
                        $scope.file = undefined;
                        $scope.alerts = [{type:data.type, msg: data.msg}];
                        $scope.userform.$setPristine();
                        $scope.process = 0;
                        $scope.user = "";
                        $('input').val(undefined);
                        $scope.nbday = false;
                        $scope.nrole = false;
                        $anchorScroll();
                    });
                   

            }
        }
        

}



    //DATE PICKER
    $scope.today = function() {
        $scope.dt = new Date();
    };

    $scope.today();
    $scope.clear = function() {
        $scope.dt = null;

    };

    $scope.toggleMin = function() {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

})