'use strict';

/* Controllers */

app.controller('editAlbumCtrl', function ($scope, $http, $modal, $stateParams, $sce, $q, $timeout, Config, Upload, $filter) {

  // console.log($stateParams.id);
  $scope.data = {};

  $scope.imageloader=false;
  $scope.imagecontent=true;
  $scope.addphoto = false;
  $scope.bigImages = false;
  $scope.file = [];
  $scope.bigfile = [];
  $scope.imageselected = false;

  var loadAlbumsImages = function (id) {
    $http({
      url: Config.ApiURL+"/slider/albumsimages/" + $stateParams.id,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data;
      $scope.img = data.item;
      console.log(data.item);
      $scope.images = [];
      for (var i = 1; i <= data.itemcount; i++)
      {
        $scope.images.push(i);
        console.log(i);        
      }
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadAlbumsImages();

  $scope.remove = function(index) {
    $scope.file.splice(index, 1);
    console.log('dasdasd');
    console.log($scope.file);
    if($scope.file.length == 0){
      $scope.addphoto = false;
    }
    
  }
  $scope.uploadAlbum2 = function(files,albumname) {
    console.log('ito upload');
    console.log(files);
    console.log('ito album name');
    console.log(albumname);
  }

  $scope.sortSlider = function(imgID,sort,album_id) {
    console.log(imgID);
    console.log(sort);
    $http({
        url: Config.ApiURL + "/slider/sortSlider/" + imgID + "/" + sort + "/" + album_id,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {        
        loadAlbumsImages();
      }).error(function (data, status, headers, config) {
        
      });
  }

//get this!
  // $scope.prepare = function(files,file) {
  //   $scope.alerts = [];
  //   $scope.closeAlert = function(index) {
  //     $scope.alerts.splice(index, 1);
  //   };
  //   if (files && files.length) {
  //     for (var i = 0; i < files.length; i++)
  //     {
  //       console.log(files[i].size);

  //       if (files[i].size >= 2000000) {
  //         console.log(files[i]);
  //         $scope.bigfile.push(files[i]);
  //         $scope.bigImages = true;
  //       }else{
  //         $scope.file.push(files[i]);
  //       }
  //     }
  //     $scope.addphoto = true;
  //     $scope.closeAlert();
  //     $scope.imageselected = true;
  //   }
  // }


  $scope.editimage = function (imgID,foldername){
    var modalInstance = $modal.open({
      templateUrl: 'editAlbumImage.html',
      controller: editImageCTRL,
      windowClass: 'app-modal-window',

      resolve: {
        imgID: function () {
          return imgID;
        },
        foldername: function () {
          return foldername;
        }
      }
    });
  }
            
  var editImageCTRL = function ($scope, $modalInstance, imgID,foldername, $state) {
    $scope.isLoading = false;
    $scope.data={};
    $scope.data.imgID = imgID;
    $scope.data.foldername = foldername;
    $scope.imgtype = 'slider';
    $scope.amazonlink = Config.amazonlink;
$scope.file = [];
    var loadImage = function () {
      $http({
        url: Config.ApiURL+"/slider/editImage/" + imgID,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $scope.data = data[0];
        console.log(data[0]);
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    }
    loadImage();
    var list = [];
    for (var x = 10; x<=60; x++)
    {
        list.push(x);
    }

    $scope.tag = list;

       $scope.editphoto = function (files,file){
         $scope.alerts = [];
         $scope.closeAlert = function(index) {
          $scope.alerts.splice(index, 1);
        };
        if (files && files.length) {
          for (var i = 0; i < files.length; i++)
          {
            console.log(files[i].size);

            if (files[i].size >= 2000000) {
              console.log(files[i]);
              $scope.bigfile.push(files[i]);
              $scope.bigImages = true;
            }else{
              $scope.file.push(files[i]);
            }
          }
          $scope.addphoto = true;
          $scope.closeAlert();
          $scope.imageselected = true;
        }
        $scope.delete = function(index) {
          $scope.file.splice(index, 1);
          console.log('dasdasd');
          console.log($scope.file);
          if($scope.file.length == 0){
            $scope.addphoto = false;
          }

        }

 
  }

    $scope.save = function (files,album) {
      $scope.isLoading = true;
    var filename
    var filecount = 0;
    $scope.imageloader=true;
    // console.log($scope.file.length);
    if ($scope.file.length==1)
    {
            $scope.imageloader=true;
        console.log("=====upload======");
    if (files && files.length)
    {
      console.log("=====upload======");
      $scope.imageloader=true;
      $scope.imagecontent=false;

      for (var i = 0; i < files.length; i++)
      {
        var file = files[i];

        if (file.size >= 2000000)
        {
          $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
          filecount = filecount + 1;

          if(filecount == files.length)
          {
            $scope.imageloader=false;
            $scope.imagecontent=true;
          }


        }
        else

        {
          var promises;

          promises = Upload.upload({

            url: Config.amazonlink, //S3 upload url including bucket name
            method: 'POST',
            transformRequest: function (data, headersGetter) {
            //Headers change here
            var headers = headersGetter();
            delete headers['Authorization'];
            return data;
            },
            fields : {
                key: 'uploads/slider/'+ album.foldername +'/'+ file.name, // the key to store the file on S3, could be file name or customized
                AWSAccessKeyId: Config.AWSAccessKeyId,
                acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                policy: Config.policy, // base64-encoded json policy (see article below)
                signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
              },
            file: file
          })
          promises.then(function(data){

            filecount = filecount + 1;
            filename = data.config.file.name;
            var fileout = {
              'img' : filename,
              'imgID' :album.imgID,
              'title':album.title,
              'titlecolor':album.titlecolor,
              'titlesize':album.titlesize,
              'description':album.description,
              'desccolor':album.desccolor,
              'descsize':album.descsize,
              'position':album.position,
              'box':album.box,
              'display':album.display,
              'btnname':album.btnname,
              'btnlink':album.btnlink
            };

            console.log(fileout);

            $http({
              url: Config.ApiURL + "/slider/updateAlbumItem",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(fileout)
            }).success(function (data, status, headers, config) {
              if(filecount == files.length)
              {
                $scope.imageloader=false;
                $scope.imagecontent=true;
                loadAlbumsImages();
                $scope.imageselected = false;
                $scope.addphoto = false;
                $scope.file = [];
                $scope.bigfile = [];
                $modalInstance.dismiss('cancel');
                loadImage();
              }

            }).error(function (data, status, headers, config) {
              $scope.imageloader=false;
              $scope.imagecontent=true;
            });

          });
        }
      }
    }
}
else
{
   console.log("=====do not upload======");
      $http({
        url: Config.ApiURL + "/slider/updateImage",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(album)
      }).success(function (data, status, headers, config) {
        $modalInstance.dismiss('cancel');      
        $scope.isLoading = false;
        loadAlbumsImages();
      }).error(function (data, status, headers, config) {
        $scope.imageloader=false;
        $scope.imagecontent=true;
      });
    };
}

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

};


   $scope.deleteimage = function (imgID){
    var modalInstance = $modal.open({
      templateUrl: 'DeleteAlbumImage.html',
      controller: deleteImageCTRL,
      resolve: {
        imgID: function () {
          return imgID;
        }
      }
    });
  }

  var deleteImageCTRL = function ($scope, $modalInstance, imgID, $state) {
    $scope.imgID = imgID;
    $scope.delete = function (imgID) {
      console.log(imgID);

      $http({
        url: Config.ApiURL + "/slider/deleteImage/" + imgID,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $modalInstance.dismiss('cancel');
        loadAlbumsImages();
      }).error(function (data, status, headers, config) {

      });

    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  };


 $scope.setstatus = function (imgid,status) {
        var newstat;
        if(status==1){
            newstat = 0;
        }else{
            newstat = 1;
        }
        console.log(newstat)
        $http({
          url: Config.ApiURL + "/slider/setstatus/" + imgid + '/' +newstat,
          method: "GET",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
          console.log(data);
          loadAlbumsImages();
        }).error(function (data, status, headers, config) {

        });

    }


//Add new image!
    $scope.addimg = function (album_id,album_name,foldername){
    var modalInstance = $modal.open({
      templateUrl: 'AddnewImage.html',
      controller: addImageCTRL,
      windowClass: 'app-modal-window',
       resolve: {
        album_id: function () {
          return album_id;
        },
        album_name: function () {
          return album_name;
        },
        foldername: function () {
          return foldername;
        }
      }
    });
  }
            
  var addImageCTRL = function ($scope, $modalInstance,album_id,album_name,foldername,$state) {
    $scope.isLoading = false;
    $scope.data = {}; 
    console.log(album_id,album_name,foldername)
    $scope.data.album_id =album_id;
    $scope.data.album_name =album_name;
    $scope.data.foldername =foldername;
  $scope.prepare = function(files,file) {
    $scope.alerts = [];
    $scope.file = [];
    $scope.closeAlert = function(index) {
      $scope.alerts.splice(index, 1);
    };
    if (files && files.length) {
      for (var i = 0; i < files.length; i++)
      {
        console.log(files[i].size);

        if (files[i].size >= 2000000) {
          console.log(files[i]);
          $scope.bigfile.push(files[i]);
          $scope.bigImages = true;
        }else{
          $scope.file.push(files[i]);
        }
      }
      $scope.addphoto = true;
      $scope.closeAlert();
      $scope.imageselected = true;
    }
  }

   $scope.remove = function(index) {
    $scope.file.splice(index, 1);
    console.log('dasdasd');
    console.log($scope.file);
    if($scope.file.length == 0){
      $scope.addphoto = false;
    }
    
  }

    var list = [];
    for (var x = 10; x<=60; x++)
    {
        list.push(x);
    }

    $scope.tag = list;

     $scope.uploadAlbum = function (files,album)
  {
    console.log("=====upload======");

    var filename
    var filecount = 0;
    if (files && files.length)
    {
      $scope.imageloader=true;
      $scope.imagecontent=false;

      for (var i = 0; i < files.length; i++)
      {
        var file = files[i];

        if (file.size >= 2000000)
        {
          $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
          filecount = filecount + 1;

          if(filecount == files.length)
          {
            $scope.imageloader=false;
            $scope.imagecontent=true;
          }


        }
        else

        {
          var promises;

          promises = Upload.upload({

            url: Config.amazonlink, //S3 upload url including bucket name
            method: 'POST',
            transformRequest: function (data, headersGetter) {
            //Headers change here
            var headers = headersGetter();
            delete headers['Authorization'];
            return data;
            },
            fields : {
                key: 'uploads/slider/'+ album.foldername +'/'+ file.name, // the key to store the file on S3, could be file name or customized
                AWSAccessKeyId: Config.AWSAccessKeyId,
                acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                policy: Config.policy, // base64-encoded json policy (see article below)
                signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
              },
            file: file
          })
          promises.then(function(data){

            filecount = filecount + 1;
            filename = data.config.file.name;
            var fileout = {
              'img' : filename,
              'album_name' :album.album_name,
              'foldername' : album.foldername,
              'title': album.title,
              'titlecolor':album.titlecolor,
              'titlesize':album.titlesize,
              'titlecolor':album.titlecolor,
              'description':album.description,
              'descsize':album.descsize,
              'desccolor':album.desccolor,
              'position':album.position,
              'box':album.box,
              'display':album.display,
              'btnname':album.btnname,
              'btnlink':album.btnlink
            };

            console.log(fileout);

            $http({
              url: Config.ApiURL + "/slider/addAlbumItem",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(fileout)
            }).success(function (data, status, headers, config) {
              if(filecount == files.length)
              {
                $scope.imageloader=false;
                $scope.imagecontent=true;
                loadAlbumsImages();
                $scope.imageselected = false;
                $scope.addphoto = false;
                $scope.file = [];
                $scope.bigfile = [];
                $modalInstance.dismiss('cancel');
              }

            }).error(function (data, status, headers, config) {
              $scope.imageloader=false;
              $scope.imagecontent=true;
            });

          });
        }
      }
    }
  };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  };

})
/////////////////////////////===============end===============/////////////////////////////////


app.directive('onlyDigits', function () {
  return {
    restrict: 'A',
    require: '?ngModel',
    link: function (scope, element, attrs, ngModel) {
      if (!ngModel) return;
      ngModel.$parsers.unshift(function (inputValue) {
        var digits = inputValue.split('').filter(function (s) { return (!isNaN(s) && s != ' '); }).join('');
        ngModel.$viewValue = digits;
        ngModel.$render();
        return digits;
      });
    }
  };
});