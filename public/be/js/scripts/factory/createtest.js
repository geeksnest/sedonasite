app.factory('CreateTest', function($http, $q, Config, $filter){
    return {

         max: function (callback) {
            $http({
                url: Config.ApiURL + "/test/max",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
               
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

         savetest: function (asset,callback) {
            $http({
                url: Config.ApiURL + "/test/savetest",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'},
                     data:$.param(asset)
            }).success(function (data, status, headers, config) {
              
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        saveresult: function (asset,callback) {
            $http({
                url: Config.ApiURL + "/test/saveresult",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'},
                     data:$.param(asset)
            }).success(function (data, status, headers, config) {
              
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        }



    }
})