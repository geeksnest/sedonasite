app.factory('MemberChart', function($http, $q, Config, $filter){
    return {

         TopUser: function (num,off,callback) {
            $http({
                url: Config.ApiURL + "/booking/TopUser/"+num + '/'+off,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
               
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

         UserStat: function (callback) {
            $http({
                url: Config.ApiURL + "/booking/UserStat",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
               
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

          OverallUserStat: function (callback) {
            $http({
                url: Config.ApiURL + "/booking/OverallUserStat",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
               
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        }


    }
})