app.factory('MemberReport', function($http, $q, Config, $filter){
    return {


        memberreportlist: function (num, off,start,end,status,payment,members,callback) {
            $http({
                url: Config.ApiURL + "/booking/memberreport/" + num + '/' + off + '/' + start + '/' + end + '/'+ status + '/' + payment + '/' + members,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
               
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        searchreportdummy: function (start,end,status,payment,members,callback) {
            $http({
                url: Config.ApiURL + "/booking/memberreportdummy/" + start + '/' + end + '/'+ status + '/' + payment + '/' + members,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
               
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

      salesreportdummy: function (callback) {
            $http({
                url: Config.ApiURL + "/booking/salesreportdummy/",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
               
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        
        totsum: function (callback) {
            $http({
                url: Config.ApiURL + "/booking/totsum/",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
               
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        
        expected: function (callback) {
            $http({
                url: Config.ApiURL + "/booking/expected/",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
               
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

          servicelist: function (callback) {
            $http({
                url: Config.ApiURL + "/booking/servicelist",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
               
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

          subservice: function (serv, callback) {
            $http({
                url: Config.ApiURL + "/booking/subservice/"+serv,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                // console.log(data);
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        pricelist: function (subserv, callback) {
            $http({
                url: Config.ApiURL + "/booking/pricelist/"+subserv,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                // console.log(data);
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        memberslist: function (callback) {
            $http({
                url: Config.ApiURL + "/booking/memberslist",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                // console.log(data);
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },


        month: function () {
            return [
                {name: 'January', val: 1},
                {name: 'February', val: 2},
                {name: 'March', val: 3},
                {name: 'April', val: 4},
                {name: 'May', val: 5},
                {name: 'June', val: 6},
                {name: 'July', val: 7},
                {name: 'August', val: 8},
                {name: 'September', val: 9},
                {name: 'October', val: 10},
                {name: 'November', val: 11},
                {name: 'December', val: 12}
            ]
        },
        day: function () {
            var day = [];
            for (var x = 1; x <= 31; x++) {
                day.push({'val': x})
            }
            return day;
        },
        year: function () {
            var year = [];
            for (var y = 2015; y <= 2050; y++) {
                year.push({'val': y})
            }
            return year;
        }
      



    }
})