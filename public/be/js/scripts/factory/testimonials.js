app.factory('Testimonials', function($http, $q, Config, Upload){
  	return {
    	 paging: function(num, off, keyword , callback){
			 $http({
				 url: Config.ApiURL + "/testimonials/list/" + num + '/' + off + '/' + keyword,
				 method: "GET",
				 headers: {
					 'Content-Type': 'application/x-www-form-urlencoded'
				 }
			 }).success(function(data, status, headers, config) {
				 callback(data);

			 }).error(function(data, status, headers, config) {
			 });
    	 },
		create: function(data, callback){
			$http({
				url: Config.ApiURL +"/testimonials/create",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(data)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {

			});
		},
		update: function(data, callback){
			$http({
				url: Config.ApiURL +"/testimonials/update",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(data)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {

			});
		},
		delete: function(testId, callback){
			$http({
				url: Config.ApiURL + "/testimonials/delete/" + testId,
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
			}).success(function(data, status, headers, config) {
				callback(data);
			}).error(function(data, status, headers, config) {
			});
		},
		uploadprofile: function(file, callback){
			if (file && file.length) {
				var file = file[0];
				var promises;
				promises = Upload.upload({
					url: Config.amazonlink, //S3 upload url including bucket name
					method: 'POST',
					transformRequest: function (data, headersGetter) {
						//Headers change here
						var headers = headersGetter();
						delete headers['Authorization'];
						return data;
					},
					fields: {
						key: 'uploads/testimonialpic/' + file.name, // the key to store the file on S3, could be file name or customized
						AWSAccessKeyId: Config.AWSAccessKeyId,
						acl: 'private', // sets the access to the uploaded file in the bucket: private or public
						policy: Config.policy, // base64-encoded json policy (see article below)
						signature: Config.signature, // base64-encoded signature based on policy string (see article below)
						"Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
					},
					file: file
				})
				promises.then(function (data) {
					callback(file.name);
				});
			}else{
				callback('');
			}
		},
		getTesti: function(id, callback){
			$http({
				url: Config.ApiURL + "/testimonials/get/" + id ,
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data, status, headers, config) {
				callback(data);
			}).error(function(data, status, headers, config) {
			});
		}
    }
   
})