app.factory('DashboardCtrl', function($http, $q, Config, $filter){
    return {
       

          DailySchedule: function (callback) {
            $http({
                url: Config.ApiURL + "/booking/DailySchedule",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                // console.log(data);
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
          TotalNews: function (callback) {
            $http({
                url: Config.ApiURL + "/booking/TotalNews",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                // console.log(data);
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
           MonthlyReservation: function (callback) {
            $http({
                url: Config.ApiURL + "/booking/MonthlyReservation",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                // console.log(data);
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        DailyReservation: function (callback) {
            $http({
                url: Config.ApiURL + "/booking/DailyReservation",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                // console.log(data);
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },


    }
})