app.factory('Managepages', function($http, $q, Config){
  	return {

    	 
    	 data: {},
    	 sample: function(num, off, keyword , sort, callback){
    	 	$http({
			            url: Config.ApiURL +"/pages/managepage/" + num + '/' + off + '/' + keyword + '/' + sort,
			            method: "GET",
			            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			        }).success(function (data, status, headers, config) {
			           data = data;
			           callback(data);
			           pagetotalitem = data.total_items;
			           currentPage = data.index;
			           
			        }).error(function (data, status, headers, config) {
			            
			        });
    	 }

    }
   
});