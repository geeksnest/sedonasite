app.factory('Createauthor', function($http, $q, Config){
  	return {

  		data: {},
    	loadcategory: function(callback){
    	 	$http({
    	 		url: Config.ApiURL + "/news/listcategory",
    	 		method: "GET",
    	 		headers: {
    	 			'Content-Type': 'application/x-www-form-urlencoded'
    	 		}
    	 	}).success(function (data, status, headers, config) {
    	 		data = data;
    	 		callback(data);
    	 	}).error(function (data, status, headers, config) {
    	 		data = data;
    	 		callback(data);
    	 	});
    	},
        loadimages: function(callback){
            $http({
                url: Config.ApiURL + "/news/authorlistimages",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);
            }).error(function(data) {
                callback(data);
            });
        },
        deleteImage: function(fileout, callback){
            $http({
                url: Config.ApiURL + "/news/deleteauthorimg",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(fileout)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        saveImage: function(fileout, callback){
           $http({
                url: Config.ApiURL + "/news/saveauthorimage",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(fileout)
            }).success(function (data, status, headers, config) {
                callback(data);
                
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        }

    }
   
})