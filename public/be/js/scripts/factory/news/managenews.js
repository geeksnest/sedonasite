app.factory('Managenews', function($http, $q, Config){
  	return {

    	 
    	 data: {},
    	 list: function(num, off, keyword, sort, callback){
    	 	$http({
	            url: Config.ApiURL +"/news/managenews/" + num + '/' + off + '/' + keyword + '/' + sort,
	            method: "GET",
	            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	        }).success(function (data, status, headers, config) {
	           data = data;
	           pagetotalitem = data.total_items;
	           currentPage = data.index;
	           callback(data);
	        }).error(function (data, status, headers, config) {
	           callback(data);
	        });
    	 },
		 delete: function(newsid, callback){
			$http({
				url: Config.ApiURL + "/news/newsdelete/" + newsid,
				method: "POST",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data, status, headers, config) {
				callback(data);
			}).error(function(data, status, headers, config) {
				callback(data);
			});
		 },
		 updateNewsTags: function(status,newsid, keyword, callback){
			 $http({
				 url: Config.ApiURL + "/news/updatenewsstatus/"+status+"/" + newsid + '/' + keyword,
				 method: "POST",
				 headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			 }).success(function (data, status, headers, config) {
				 callback(data);
			 }).error(function (data, status, headers, config) {
				 callback(data);
			 });
		 }


    }
   
})