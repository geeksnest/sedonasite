app.factory('Category', function($http, $q, Config){
    return {
        loadcategory: function(callback){
            $http({
                url: Config.ApiURL + "/news/listcategory",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
        },
        addcategory: function(category, callback){
            $http({
                url: Config.ApiURL + "/news/savecategory",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(category)
            }).success(function(data, status, headers, config) {
                callback(data);
                //loadcategory();
                //$modalInstance.close();
            }).error(function(data, status, headers, config) {
                $modalInstance.close();
            });
        },
        list: function(num,off,keyword, callback){
            $http({
                url: Config.ApiURL + "/news/managecategory/" + num + '/' + off + '/' + keyword,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                $scope.status = status;
            });
        },
        delete: function(id, callback){
            $http({
                url: Config.ApiURL + "/news/categorydelete/" + id,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        },
        update: function(data, memid, callback){
            $http({
                url: Config.ApiURL + "/news/updatecategorynames/" + data + "/"+memid,

                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)

            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        },
        conflicts: function(id, callback) {
            $http({
                url: Config.ApiURL + "/news/loadconflict/" + id,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        },
        updateconflict: function(data, callback){
            $http({
                url: Config.ApiURL + "/news/updateconflict",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        }
    }

})