app.factory('Createnews', function($http, $q, Config){
  	return {

  		data: {},
        loadcategory: function(callback){
            $http({
                url: Config.ApiURL + "/news/listcategory",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
         },

        loadtags: function(callback){
            $http({
                url: Config.ApiURL + "/news/listtags",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {         
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
         },

    	loadauthor: function(callback){
    	 	$http({
    	 		url: Config.ApiURL + "/news/listauthor",
    	 		method: "GET",
    	 		headers: {
    	 			'Content-Type': 'application/x-www-form-urlencoded'
    	 		}
    	 	}).success(function (data, status, headers, config) {
    	 		data = data;
    	 		callback(data);
    	 	}).error(function (data, status, headers, config) {
    	 		data = data;
    	 		callback(data);
    	 	});
    	 },
        add: function(news,callback){
            $http({
                url: Config.ApiURL + "/news/create",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(news)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({error: { msg: 'Something went wrong please check your fields'}});
            });
        },
        updateNews: function(news, callback) {
            $http({
                url: Config.ApiURL + "/news/updatenews",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(news)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                console.log(data)
            });
        },
        loadimages: function(callback){
            $http({
                url: Config.ApiURL + "/news/listimages",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);
            }).error(function(data) {
                callback(data);
            });
        },
        loadVideo: function(callback){
            $http({
                url: Config.ApiURL + "/news/listvideo",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);
            }).error(function(data) {
                callback(data);
            });
        },
        saveImage: function(fileout, callback){
            $http({
                url: Config.ApiURL + "/news/saveimage",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(fileout)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        saveVid: function(newslink, callback){
            $http({
                url: Config.ApiURL + "/news/addvid",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(newslink)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        },
        deleteVideo: function(datavideo, callback){
            $http({
                url: Config.ApiURL + "/news/deletevideo",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(datavideo)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        deleteImage: function(fileout, callback){

            $http({
                url: Config.ApiURL + "/news/deletenewsimg",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(fileout)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        }

    }
   
})