app.factory('Manageauthor', function($http, $q, Config){
  	return {

    	 
    	 data: {},
    	 list: function(num, off, keyword , callback){
    	 	$http({
			            url: Config.ApiURL +"/news/manageauthor/" + num + '/' + off + '/' + keyword,
			            method: "GET",
			            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			        }).success(function (data, status, headers, config) {
			           data = data;
			           callback(data);
			           pagetotalitem = data.total_items;
			           currentPage = data.index;
			           
			        }).error(function (data, status, headers, config) {
			           callback(data);
			        });
    	 },
		delete: function(authorid, callback){
			$http({
				url: Config.ApiURL + "/news/authordelete/" + authorid,
				method: "POST",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data, status, headers, config) {
				callback(data);
			}).error(function(data, status, headers, config) {
				callback(data);
			});
		},
		loadauthornews: function(authorid, callback){
			$http({
				url: Config.ApiURL + "/news/loadauthornews/" + authorid,
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data, status, headers, config) {
				callback(data);
			}).error(function(data, status, headers, config) {
				callback(data);
			});
		},
		updatenewsauthors: function(data, callback){
			$http({
				url: Config.ApiURL + "/news/updatenewsauthors",
				method: "post",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
                data: $.param(data)
			}).success(function(data, status, headers, config) {
				callback(data);
			}).error(function(data, status, headers, config) {
				callback(data);
			});
		}

    }
   
})