app.factory('Managepagescategory', function($http, $q, Config){
  	return {

    	 
    	 data: {},
    	 sample: function(num, off, keyword , callback){
    	 	$http({
			            url: Config.ApiURL +"/pages/managepagecategory/" + num + '/' + off + '/' + keyword,
			            method: "GET",
			            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			        }).success(function (data, status, headers, config) {
			           data = data;
			           callback(data);
			           pagetotalitem = data.total_items;
			           currentPage = data.index;
			           
			        }).error(function (data, status, headers, config) {
			           callback(data);
			        });
    	 },
		delete: function(catid, callback){
			$http({
				url: Config.ApiURL + "/pages/categorydelete/" + catid,
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data, status, headers, config) {
				callback(data);
			}).error(function(data, status, headers, config) {
				callback(data);
			});
		},
		add: function(category, callback){
			$http({
				url: Config.ApiURL + "/pages/createpagecategory",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(category)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		edit: function(category,catid, callback){
			$http({
				url: Config.ApiURL + "/pages/updatecategorynames/"+catid,
				method: "POST",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				data: $.param(category)
			}).success(function(data, status, headers, config) {
				callback(data);
			}).error(function(data, status, headers, config) {
				callback(data);
			});
		},
		loadcatpages: function(catid, callback){
			$http({
				url: Config.ApiURL + "/pages/loadcatpages/" + catid,
				method: "get",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data, status, headers, config) {
				callback(data);
			}).error(function(data, status, headers, config) {
				callback(data);
			});
		},
		uniquepage: function(slugs,callback){
			$http({
				url: Config.ApiURL +"/specialpage/uniquepage/"+slugs,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
			});
		},
		updateconflict: function(data, callback) {
			$http({
				url: Config.ApiURL + "/pages/updatepagescategory",
				method: "POST",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				data: $.param(data)
			}).success(function(data, status, headers, config) {
				callback(data);
			}).error(function(data, status, headers, config) {
				callback(data);
			});
		}
    }
})