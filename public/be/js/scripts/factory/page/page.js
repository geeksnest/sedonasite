app.factory('Page', function($http, $q, Config){
  	return {
    	 data: {},
    	 sample: function(num, off, keyword, sort, callback) {
    	 	$http({
			            url: Config.ApiURL +"/pages/managepage/" + num + '/' + off + '/' + keyword + '/' + sort,
			            method: "GET",
			            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			        }).success(function (data, status, headers, config) {
			           data = data;
			           callback(data);
			           pagetotalitem = data.total_items;
			           currentPage = data.index;

			        }).error(function (data, status, headers, config) {
			           callback(data);
			        });
    	 },
     aura_list: function(num, off, keyword, sort, callback) {
      $http({
                url: Config.ApiURL +"/pages/managepage/aura/" + num + '/' + off + '/' + keyword + '/' + sort,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
               data = data;
               callback(data);
               pagetotalitem = data.total_items;
               currentPage = data.index;

            }).error(function (data, status, headers, config) {
               callback(data);
            });
     },
		get: function(callback){
			$http({
				url: Config.ApiURL +"/pages/get",
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function (data, status, headers, config) {
				console.log(data);
				callback(data);
			}).error(function (data, status, headers, config) {

			});
		},
		updatestatus: function(pageid, keyword, status, callback){
			$http({
				url: Config.ApiURL + "/pages/updatepagestatus/"+status+"/" + pageid + '/' + keyword,
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		delete: function(pageid, callback){
			$http({
				url: Config.ApiURL + "/page/pagedelete/" + pageid,
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data, status, headers, config) {
				callback(data);
			}).error(function(data, status, headers, config) {
				callback(data);
			});
		},
		add: function(page, callback) {

			$http({
				url: Config.ApiURL + "/pages/create",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(page)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback({'error':data});
			});
		},
		deleteimage: function(fileout, callback) {
			$http({
				url: Config.ApiURL + "/pages/deletepagesimg",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(fileout)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		savepagecat: function (category, callback){
			$http({
				url: Config.ApiURL + "/pages/createpagecategory",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(category)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		categorylist: function(callback){
			$http({
				url: Config.ApiURL + "/pages/listpagecategory",
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function (data) {
				// console.log("success");
				callback(data);
			}).error(function (data) {
				callback(data);
			});
		},
		loadimages: function(callback){
			$http({
				url: Config.ApiURL + "/pages/listimages",
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data) {
				callback(data);
			}).error(function(data) {
				callback(data)
			});
		},
		saveimage: function(fileout, callback){
			$http({
				url: Config.ApiURL + "/pages/saveimage",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(fileout)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		updatepage: function(page, callback){
			$http({
				url: Config.ApiURL + "/pages/saveeditedpage",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(page)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},

		 sortthis:function(content, callback){
            $http({
                url: Config.ApiURL + "/pages/sortcontent",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(content)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        },
		getpagescat: function(callback){
			$http({
				url: Config.ApiURL + "/pages/getpages",
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		}
    }

})
