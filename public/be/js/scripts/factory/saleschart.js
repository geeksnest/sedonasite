app.factory('SalesChart', function($http, $q, Config, $filter){
    return {
       
         reservestats: function (callback) {
            $http({
                url: Config.ApiURL + "/booking/reservestats",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                // console.log(data);
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        reserve_statservice: function (callback) {
            $http({
                url: Config.ApiURL + "/booking/reserve_statservice",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                // console.log(data);
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        
         Sales_Reservation: function (callback) {
            $http({
                url: Config.ApiURL + "/booking/Sales_Reservation",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                // console.log(data);
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        MonthlyReservation: function (callback) {
            $http({
                url: Config.ApiURL + "/booking/MonthlyReservation",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                // console.log(data);
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

          MonthlySchedule: function (callback) {
            $http({
                url: Config.ApiURL + "/booking/MonthlySchedule",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                // console.log(data);
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

         TopService: function (callback) {
            $http({
                url: Config.ApiURL + "/booking/TopService",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
               
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });

        },

         TopSubservice: function (callback) {
            $http({
                url: Config.ApiURL + "/booking/TopSubservice",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
               
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
            
        }

        


    }
})