app.factory('Bookings', function($http, $q, Config, $filter){
    return {
        paging: function (num, off, keyword,searchdate ,callback) {

            $http({
                url: Config.ApiURL + "/booking/list/" + num + '/' + off + '/' + keyword + '/' + searchdate,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);

            }).error(function (data, status, headers, config) {
            });
        },
        get: function (id, callback) {
            $http({
                url: Config.ApiURL + "/booking/get/" + id,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
            });
        },
        read: function (id) {
            $http({
                url: Config.ApiURL + "/booking/read/" + id,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                console.log(data);
            }).error(function (data, status, headers, config) {
            });
        },
        reply: function (data, id, callback) {
            var newdata = {
                'message': data,
                'bookid': id
            };

            $http({
                url: Config.ApiURL + "/booking/reply",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(newdata)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {

            });
        },
        getreplies: function (id, callback) {
            $http({
                url: Config.ApiURL + "/booking/getreplies/" + id,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
            });
        },
        delete: function (id, callback) {
            $http({
                url: Config.ApiURL + "/booking/delete/" + id,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
            });
        }
    }

})