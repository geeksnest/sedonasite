app.factory('ViewTest', function($http, $q, Config, $filter){
    return {

    testname: function (num,off,keyword,callback){
            $http({
                url: Config.ApiURL +"/test/view/"+num + '/' +off + '/' +keyword,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

            view: function (testid,callback){
            $http({
                url: Config.ApiURL +"/test/viewtest/"+testid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        viewQuestion: function (q_id,callback){
            $http({
                url: Config.ApiURL +"/test/viewQuestion/"+q_id,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

          viewTestname: function (testid,callback){
            $http({
                url: Config.ApiURL +"/test/viewTestname/"+testid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

         EditQuestion: function (asset,callback){
            $http({
                url: Config.ApiURL +"/test/EditQuestion",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data:$.param(asset)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        EditTestname: function (asset,callback){
            $http({
                url: Config.ApiURL +"/test/EditTestname",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data:$.param(asset)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        viewResult: function (id,callback){
            $http({
                url: Config.ApiURL +"/test/viewResult/"+id,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        viewServices: function (id,callback){
            $http({
                url: Config.ApiURL +"/test/viewServices/"+id,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        EditResult: function (asset,callback){
            $http({
                url: Config.ApiURL +"/test/EditResult",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                 data:$.param(asset)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        EditServices: function (asset,callback){
            $http({
                url: Config.ApiURL +"/test/EditServices",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                 data:$.param(asset)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        EditProducts: function (asset,callback){
            $http({
                url: Config.ApiURL +"/test/EditProducts",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                 data:$.param(asset)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        DeleteQuestion: function (q_id,callback){
            $http({
                url: Config.ApiURL +"/test/DeleteQuestion/"+q_id,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        DeleteTest: function (testid,callback){
            $http({
                url: Config.ApiURL +"/test/DeleteTest/"+testid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
         DeleteResult: function (id,choice,testid,callback){
            $http({
                url: Config.ApiURL +"/test/DeleteResult/"+id+'/'+choice+'/'+testid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
         DeleteRec: function (id,callback){
            $http({
                url: Config.ApiURL +"/test/DeleteRec/"+id,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        DeleteProd: function (id,callback){
            $http({
                url: Config.ApiURL +"/test/DeleteProd/"+id,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
         updatestatus: function (testid,newstat,callback){
            $http({
                url: Config.ApiURL +"/test/updatestatus/"+testid+'/'+newstat,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        AddEditQ: function (asset,callback){
            $http({
                url: Config.ApiURL +"/test/AddEditQ",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                 data:$.param(asset)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

         AddEditResult: function (asset,callback){
            $http({
                url: Config.ApiURL +"/test/AddEditResult",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                 data:$.param(asset)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        AddEditRec: function (asset,callback){
            $http({
                url: Config.ApiURL +"/test/AddEditRec",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                 data:$.param(asset)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
          AddEditProd: function (asset,callback){
            $http({
                url: Config.ApiURL +"/test/AddEditProd",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                 data:$.param(asset)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        testmemberlist: function (num,off,keyword,callback){
            $http({
                url: Config.ApiURL +"/test/testmemberlist/"+num + '/' +off + '/' +keyword,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

         arrange: function (list,callback){
            $http({
                url: Config.ApiURL +"/test/arrange",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                 data:$.param(list)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
         DeleteMem: function (id,callback){
            $http({
                url: Config.ApiURL +"/test/DeleteMem/"+id,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },




    }
})