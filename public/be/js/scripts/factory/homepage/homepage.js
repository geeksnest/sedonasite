app.factory('Homepage', function($http, $q, Config, Upload){
    return {

        create:function(content, callback){
            $http({
                url: Config.ApiURL + "/homepage/createcontent",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(content)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        },
        sortthis:function(content, callback){
            $http({
                url: Config.ApiURL + "/homepage/sortcontent",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(content)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        },
        list: function(num, off, keyword, callback){
            $http({
                url: Config.ApiURL +"/homepage/list/" + num + '/' + off + '/' + keyword,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        delete:function(id, callback){
            $http({
                url:Config.ApiURL +"/homepage/delete/"+id,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            })
        },
        info : function(id, callback) {
            $http({
                url: Config.ApiURL +"/homepage/info/"+id,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
               callback(data);
            }).error(function (data, status, headers, config) {
               callback(data);
            })
        },
        uploadpic : function(files, callback){
            var file = files[0];
            var promises;
            promises = Upload.upload({
                url: Config.amazonlink, //S3 upload url including bucket name
                method: 'POST',
                transformRequest: function (data, headersGetter) {
                    //Headers change here
                    var headers = headersGetter();
                    delete headers['Authorization'];
                    return data;
                },
                fields: {
                    key: 'uploads/homepagebackground/' + file.name, // the key to store the file on S3, could be file name or customized
                    AWSAccessKeyId: Config.AWSAccessKeyId,
                    acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                    policy: Config.policy, // base64-encoded json policy (see article below)
                    signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                    "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                },
                file: file
            }).progress(function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                var log = progressPercentage;
                callback(log);
            })
        },
        update:function(content, callback){
            $http({
                url: Config.ApiURL +"/homepage/update",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(content)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config){
                callback(data);
            });
        },
        categories: function(callback) {
          $http({
              url: Config.ApiURL + "/homepage/categories",
              method: "GET",
              headers: {"Content-Type": "application/x-www-form-urlencoded"}
          }).success(function(data, status, headers, config) {
            callback(data);
          }).error(function(data, status, headers, config){
            callback(status);
          })
        }
    }

})
