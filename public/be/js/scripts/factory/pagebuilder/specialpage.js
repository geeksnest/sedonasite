app.factory('Special', function($http, $q, Config){
  	return {
    	 data: {},
    	 sample: function(num, off, keyword, sort, callback){
    	 			$http({
			            url: Config.ApiURL +"/pages/managepage/" + num + '/' + off + '/' + keyword + '/' + sort,
			            method: "GET",
			            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			        }).success(function (data, status, headers, config) {
			           data = data;
			           callback(data);
			           pagetotalitem = data.total_items;
			           currentPage = data.index;

			        }).error(function (data, status, headers, config) {
			           callback(data);
			        });
    	 },
    	 uniquepage: function(slugs,callback){
			$http({
				url: Config.ApiURL +"/specialpage/uniquepage/"+slugs,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
			});
		},
		get: function(callback){
			$http({
				url: Config.ApiURL +"/pages/get",
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function (data, status, headers, config) {
			
				callback(data);
			}).error(function (data, status, headers, config) {

			});
		},
		updatestatus: function(pageid, keyword, status, callback){
			$http({
				url: Config.ApiURL + "/specialpage/updatestatus/"+status+"/" + pageid + '/' + keyword,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		// delete: function(pageid, callback){
		// 	$http({
		// 		url: Config.ApiURL + "/page/pagedelete/" + pageid,
		// 		method: "GET",
		// 		headers: {
		// 			'Content-Type': 'application/x-www-form-urlencoded'
		// 		}
		// 	}).success(function(data, status, headers, config) {
		// 		callback(data);
		// 	}).error(function(data, status, headers, config) {
		// 		callback(data);
		// 	});
		// },
		add: function(page, callback) {

			$http({
				url: Config.ApiURL + "/specialpage/save",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(page)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback({'error':data});
			});
		},
		addfooter: function(page, callback) {

			$http({
				url: Config.ApiURL + "/special/footer/save",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(page)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback({'error':data});
			});
		},
		deleteimage: function(fileout, callback) {
			$http({
				url: Config.ApiURL + "/pages/deletepagesimg",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(fileout)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		loadimages: function(callback){
			$http({
				url: Config.ApiURL + "/pages/listimages",
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data) {
				callback(data);
			}).error(function(data) {
				callback(data)
			});
		},
		saveimage: function(fileout, callback){
			$http({
				url: Config.ApiURL + "/pages/saveimage",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(fileout)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		updatepage: function(page, callback){
			$http({
				url: Config.ApiURL + "/pages/saveeditedpage",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(page)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		list: function(num,off,keyword,callback){
			$http({
				url: Config.ApiURL + '/specialpage/list/'+num+'/'+off+'/'+keyword,
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data) {
				callback(data);
			}).error(function(data) {
				callback(data)
			});
		},
		delete: function(id,callback){
			$http({
				url: Config.ApiURL + '/specialpage/delete/'+id,
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data) {
				callback(data);
			}).error(function(data) {
				callback(data)
			});
		},
		list: function(num,off,keyword,callback){
			$http({
				url: Config.ApiURL + '/specialpage/list/'+num+'/'+off+'/'+keyword,
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data) {
				callback(data);
			}).error(function(data) {
				callback(data)
			});
		},
		info: function(id,callback){
			$http({
				url: Config.ApiURL + '/specialpage/info/'+id,
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data) {
				callback(data);
			}).error(function(data) {
				callback(data)
			});
		},
		infofooter: function(callback){
			$http({
				url: Config.ApiURL + '/special/footer/info',
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data) {
				callback(data);
			}).error(function(data) {
				callback(data)
			});
		},
		update: function(page, callback) {
			$http({
				url: Config.ApiURL + "/specialpage/update",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(page)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback({'error':data});
			});
		},
		loadnews: function(callback){
			$http({
				url: Config.ApiURL + '/special/loadnews',
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data) {
				callback(data);
			}).error(function(data) {
				callback(data)
			});
		},
		loadtesti: function(callback){
			$http({
				url: Config.ApiURL + '/special/loadtesti',
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data) {
				callback(data);
			}).error(function(data) {
				callback(data)
			});
		},
		getmenu: function(callback){
			$http({
				url: Config.ApiURL + '/specialpage/getmenu/',
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data) {
				callback(data);
			}).error(function(data) {
				callback(data)
			});
		},
		getinfo: function(callback){
            $http({
                url: Config.ApiURL + "/contactdetails/getinfo",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },


    }

})
