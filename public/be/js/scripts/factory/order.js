app.factory('Order', function($http, $q, Config){
  	return {
    	getproducts: function(callback) {
			$http({
				url: Config.ApiURL + "/order/getproducts",
				method: "get",
				headers: {'Content-Type' : 'application/x-www-form-urlencoded'}
			}).success(function (data, status, headers, config){
				callback(data);
			})
		},
		getaccountinfo: function(email, callback) {
			$http({
				url: Config.ApiURL + "/order/getaccountinfo/" + email,
				method: "get",
				headers: {'Content-Type' : 'application/x-www-form-urlencoded'}
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		month: function () {
            return [
                {name: 'January', val: 1},
                {name: 'February', val: 2},
                {name: 'March', val: 3},
                {name: 'April', val: 4},
                {name: 'May', val: 5},
                {name: 'June', val: 6},
                {name: 'July', val: 7},
                {name: 'August', val: 8},
                {name: 'September', val: 9},
                {name: 'October', val: 10},
                {name: 'November', val: 11},
                {name: 'December', val: 12}
            ]
        },
        day: function () {
            var day = [];
            for (var x = 1; x <= 31; x++) {
                day.push({'val': x})
            }
            return day;
        },
        year: function () {
            var year = [];
            for (var y = 2015; y <= 2050; y++) {
                year.push({'val': y})
            }
            return year;
        },
        addoffline: function(data, callback) {
        	$http({
        		url: Config.ApiURL + "/order/offline/add",
        		method: "POST",
        		headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
        		data: $.param(data)
        	}).success(function(data, status, headers, config) {
        		callback(data);
        	});
        },
        loadlist: function(num, off, keyword, filter, callback) {
        	$http({
        		url: Config.ApiURL + "/order/list/" + num + "/" + off + "/" + keyword + "/" + filter,
        		method: "GET",
        		headers: {'Content-type' : 'application/x-www-form-urlencoded'}
        	}).success(function(data, status, headers, Config) {
        		callback(data);
        	});
        },
        loadreportlist: function(num, off,startdate,enddate,status,payment,members, callback) {
            $http({
                url: Config.ApiURL + "/order/loadreportlist/" + num + "/" + off + "/" + startdate + "/" + enddate + "/" + status+ "/" + payment+ "/" + members,
                method: "GET",
                headers: {'Content-type' : 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, Config) {
                callback(data);
            });
        },
        getorder: function(id, callback){
        	$http({
        		url: Config.ApiURL + "/order/get/" + id,
        		method: "GET",
        		headers: {'Content-type' : 'application/x-www-form-urlencoded'}
        	}).success(function(data, status, headers, Config) {
        		callback(data);
        	});
        },
        comment: function(data, callback){
        	$http({
        		url: Config.ApiURL + "/order/comment/add",
        		method: "POST",
        		headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
        		data: $.param(data)
        	}).success(function(data, status, headers, config) {
        		callback(data);
        	});
        },
        updateStatus: function(data, callback) {
        	$http({
        		url: Config.ApiURL + "/order/updatestatus",
        		method: "POST",
        		headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
        		data: $.param(data)
        	}).success(function(data, status, headers, config) {
        		callback(data);
        	});
        },
        getorderreport: function(callback) {
            $http({
                url: Config.ApiURL + "/order/getreport",
                method: "GET",
                headers: {'Content-type' : 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, Config) {
                callback(data);
            });
        },
        memberslist: function (callback) {
            $http({
                url: Config.ApiURL + "/booking/memberslist",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                // console.log(data);
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        TopMonthly: function (callback) {
            $http({
                url: Config.ApiURL + "/order/TopMonthly",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                // console.log(data);
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        MonthlySales: function (callback) {
            $http({
                url: Config.ApiURL + "/order/MonthlySales",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                // console.log(data);
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        DailyOrders: function (callback) {
            $http({
                url: Config.ApiURL + "/order/DailyOrders",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                // console.log(data);
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
         MonthlyOrders: function (callback) {
            $http({
                url: Config.ApiURL + "/order/MonthlyOrders",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                // console.log(data);
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        TopProducts: function (callback) {
            $http({
                url: Config.ApiURL + "/order/TopProducts",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                // console.log(data);
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
    }

})

// getproducts: function(callback) {
// 	var related = { 'newsid':newsid, 'title':title };
// 	$http({
// 		url: Config.ApiURL + "/mainnews/addrelated",
// 		method: "POST",
// 		headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
// 		data: $.param(related)
// 	}).success(function (data, status, headers, config){
// 		callback(data);
// 	})
// }
