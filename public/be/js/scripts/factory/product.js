app.factory('Product', function($http, $q, Config){
	return {
		images : [],
		add: function(data, callback) {
    	 	$http({
    	 		url: Config.ApiURL + "/add/product",
    	 		method: "post",
    	 		headers: {
    	 			'Content-Type': 'application/x-www-form-urlencoded'
    	 		},
                data: $.param(data)
    	 	}).success(function (data, status, headers, config) {
    	 		callback(data);
    	 	}).error(function (data, status, headers, config) {
    	 		callback(data);
    	 	});
		},
		loadimages: function(id, callback) {
    	 	$http({
    	 		url: Config.ApiURL + "/product/loadimages/" + id,
    	 		method: "get",
    	 		headers: {
    	 			'Content-Type': 'application/x-www-form-urlencoded'
    	 		}
    	 	}).success(function (data, status, headers, config) {
    	 		callback(data);
    	 	}).error(function (data, status, headers, config) {
    	 		callback(data);
    	 	});
		},
		saveImage: function(id, data, callback) {
    	 	$http({
    	 		url: Config.ApiURL + "/product/saveimage/" + id,
    	 		method: "post",
    	 		headers: {
    	 			'Content-Type': 'application/x-www-form-urlencoded'
    	 		},
                data: $.param(data)
    	 	}).success(function (data, status, headers, config) {
    	 		callback(data);
    	 	}).error(function (data, status, headers, config) {
    	 		callback(data);
    	 	});
		},
		deleteImage: function(id, data, callback) {
    	 	$http({
    	 		url: Config.ApiURL + "/product/deleteimage/" + id,
    	 		method: "post",
    	 		headers: {
    	 			'Content-Type': 'application/x-www-form-urlencoded'
    	 		},
                data: $.param(data)
    	 	}).success(function (data, status, headers, config) {
    	 		callback(data);
    	 	}).error(function (data, status, headers, config) {
    	 		callback(data);
    	 	});
		},
		load: function(callback) {
    	 	$http({
    	 		url: Config.ApiURL + "/product/loadaddproduct",
    	 		method: "get",
    	 		headers: {
    	 			'Content-Type': 'application/x-www-form-urlencoded'
    	 		}
    	 	}).success(function (data, status, headers, config) {
    	 		callback(data);
    	 	}).error(function (data, status, headers, config) {
    	 		callback(data);
    	 	});
		},
		complete: function(data, callback) {
    	 	$http({
    	 		url: Config.ApiURL + "/product/load/" + data ,
    	 		method: "get",
    	 		headers: {
    	 			'Content-Type': 'application/x-www-form-urlencoded'
    	 		}
    	 	}).success(function (data, status, headers, config) {
    	 		callback(data);
    	 	}).error(function (data, status, headers, config) {
    	 		callback(data);
    	 	});
		},
		getlist: function(num, off, keyword, find, callback) {
    	 	$http({
    	 		url: Config.ApiURL + "/product/loadlist/" + num + "/" + off + "/" + keyword + "/" + find,
    	 		method: "get",
    	 		headers: {
    	 			'Content-Type': 'application/x-www-form-urlencoded'
    	 		}
    	 	}).success(function (data, status, headers, config) {
    	 		callback(data);
    	 	}).error(function (data, status, headers, config) {
    	 		callback(data);
    	 	});
		},
        validatecode: function(id, name, callback) {
            $http({
                url: Config.ApiURL + "/product/validatecode/" + id + "/" + name,
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        delete: function(id, callback) {
            $http({
                url: Config.ApiURL + "/product/delete/" + id,
                method: "post",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        loadedit: function(id, callback) {
            $http({
                url: Config.ApiURL + "/product/loadedit/" + id,
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        edit: function(data, callback) {
            $http({
                url: Config.ApiURL + "/product/edit",
                method: "post",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        validatename: function(id, name, callback) {
            $http({
                url: Config.ApiURL + "/product/validatename/" + id + "/" + name,
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        updatestatus: function(status, id, callback) {
            $http({
                url: Config.ApiURL + "/product/updatestatus/" + id + "/" + status,
                method: "post",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        validateSlugs: function(id, slugs, callback) {
            $http({
                url: Config.ApiURL + "/product/validateslug/" + id + "/" + slugs,
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        addQuantity: function(id, data, callback) {
            $http({
                url: Config.ApiURL + "/product/addquantity/" + id,
                method: "post",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        addDiscount: function(id, data, callback){
            $http({
                url: Config.ApiURL + "/product/adddiscount/" + id,
                method: "post",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        viewproduct: function(id, callback) {
            $http({
                url: Config.ApiURL + "/product/viewproduct/" + id,
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        viewproductdetails: function(id, page, type, callback) {
            $http({
                url: Config.ApiURL + "/product/viewdetails/" + id + "/" + type + "/" + page,
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        selectImage: function(data, callback) {
            $http({
                url: Config.ApiURL + "/product/selectimage",
                method: "post",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        loadcategory: function(num, off, keyword, callback) {
            $http({
                url: Config.ApiURL + "/product/loadcategorylist/" + num + "/" + off + "/" + keyword,
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        validateCategory: function(id, category, callback) {
            $http({
                url: Config.ApiURL + "/product/validatecategory/" + id + "/" + category,
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        loadsubcateogry: function(callback) {
            $http({
                url: Config.ApiURL + "/product/loadsubcateogry",
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        saveCategory: function(data, callback) {
            $http({
                url: Config.ApiURL + "/product/saveCategory",
                method: "post",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        deleteCategory: function(id, callback) {
            $http({
                url: Config.ApiURL + "/product/deleteCategory/" + id,
                method: "post",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        loadeditcategory: function(id, callback) {
            $http({
                url: Config.ApiURL + "/product/loadeditcategory/" + id,
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        editCategory: function(data, callback) {
            $http({
                url: Config.ApiURL + "/product/editcategory",
                method: "post",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        loadtags: function(num, off, keyword, callback) {
            $http({
                url: Config.ApiURL + "/product/loadtags/" + num + "/" + off + "/" + keyword,
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        deleteTags: function(id, callback) {
            $http({
                url: Config.ApiURL + "/product/deletetags/" + id,
                method: "post",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        validateTags: function(id, tags, callback) {
            $http({
                url: Config.ApiURL + "/product/validatetags/" + id + "/" + tags,
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        addTags: function(data, callback) {
            $http({
                url: Config.ApiURL + "/product/saveTag",
                method: "post",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        updatetags: function(data, callback) {
            $http({
                url: Config.ApiURL + "/product/updatetags",
                method: "post",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        loadsubcategory: function(num, off, keyword, callback) {
            $http({
                url: Config.ApiURL + "/product/loadsubcategory/" + num + "/" + off + "/" + keyword,
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        validateSubCategory: function(id, subcategory, callback) {
            $http({
                url: Config.ApiURL + "/product/validatesubcategory/" + id + "/" + subcategory,
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        loadtype: function(callback) {
            $http({
                url: Config.ApiURL + "/product/loadtypes",
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        addSubCategory: function(data, callback) {
            $http({
                url: Config.ApiURL + "/product/addsubcategory",
                method: "post",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        deleteSubcategory: function(id, callback) {
            $http({
                url: Config.ApiURL + "/product/deletesubcategory/" + id,
                method: "post",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        loadsubcatedit: function(id, callback){
            $http({
                url: Config.ApiURL + "/product/loadeditsubcategory/" + id,
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        updateSubcategory: function(data, callback) {
            $http({
                url: Config.ApiURL + "/product/updatesubcategory",
                method: "post",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        loadtypes: function(num, off, keyword, callback) {
            $http({
                url: Config.ApiURL + "/product/loadtypes/" + num + "/" + off + "/" + keyword,
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        deleteType: function(id, callback){
            $http({
                url: Config.ApiURL + "/product/deletetype/" + id,
                method: "post",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        updatetype: function(data, callback){
            $http({
                url: Config.ApiURL + "/product/updatetype",
                method: "post",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        validateType: function(type, callback){
            $http({
                url: Config.ApiURL + "/product/validatetype/" + type,
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        addType: function(data, callback) {
            $http({
                url: Config.ApiURL + "/product/savetype",
                method: "post",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        getsubcat: function(data, callback) {
            $http({
                url: Config.ApiURL + "/product/getsubcat",
                method: "post",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        gettypes: function(data, callback) {
            $http({
                url: Config.ApiURL + "/product/gettypes",
                method: "post",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        getReport: function(id, callback) {
            $http({
                url: Config.ApiURL + "/product/getReport/" + id,
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        loadtagssize: function(callback) {
            $http({
                url: Config.ApiURL + "/product/loadtagssize",
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        loadsize: function(num, off, keyword, callback){
            $http({
                url: Config.ApiURL + "/product/loadsize/" + num + "/" + off + "/" + keyword,
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        deleteSize: function(id, callback) {
            $http({
                url: Config.ApiURL + "/product/deletesize/" + id,
                method: "post",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        updatesize: function(data, callback) {
            $http({
                url: Config.ApiURL + "/product/updatesize",
                method: "post",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        getproductreport: function(callback) {
            $http({
                url: Config.ApiURL + "/product/getproductreport",
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        }
	};
})