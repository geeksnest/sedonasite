<?php

/**
 * @author Jete O'Keeffe
 * @version 1.0
 * @link http://docs.phalconphp.com/en/latest/reference/micro.html#defining-routes
 * @eg.

  $routes[] = [
  'method' => 'post',
  'route' => '/api/update',
  'handler' => 'myFunction'
  ];

 */
$routes[] = [
    'method' => 'post',
    'route' => '/members/registration',
    'handler' => ['Controllers\MembersController', 'registrationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/registration/check',
    'handler' => ['Controllers\MembersController', 'registrationCheckAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/confirmationcms/resendcon/{email}',
    'handler' => ['Controllers\MembersController', 'resendconAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/completioncms/resendcom/{email}',
    'handler' => ['Controllers\MembersController', 'resendcomAction'],
    'authentication' => FALSE
];


$routes[] = [
    'method' => 'post',
    'route' => '/members/confirmation/resend',
    'handler' => ['Controllers\MembersController', 'resendconfirmationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/completion/resend',
    'handler' => ['Controllers\MembersController', 'resendcompletionAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/members/memberslist/{num}/{off}/{keyword}/{sort}/{sortto}',
    'handler' => ['Controllers\MembersController', 'memberslistAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/members/memberslistsearchbydate/{num}/{off}/{keyword}',
    'handler' => ['Controllers\MembersController', 'memberslistsearchbydateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/checkuser',
    'handler' => ['Controllers\MembersController', 'checkuserAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/updatedonations',
    'handler' => ['Controllers\MembersController', 'donationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/members/userdonation',
    'handler' => ['Controllers\MembersController', 'userdonationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/members/userdonationNepal',
    'handler' => ['Controllers\MembersController', 'userdonationNepalAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/members/userdonationNY',
    'handler' => ['Controllers\MembersController', 'userdonationNYAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/members/userdonationHouston',
    'handler' => ['Controllers\MembersController', 'userdonationHoustonAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/members/userdonationSeattle',
    'handler' => ['Controllers\MembersController', 'userdonationSeattleAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/sendmail',
    'handler' => ['Controllers\MembersController', 'sendmailAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/members/memberinfo/{memberid}',
    'handler' => ['Controllers\MembersController', 'membersinfoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/memberupdate/{memberid}',
    'handler' => ['Controllers\MembersController', 'memberupdateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/memberupdatepassword/{memberid}',
    'handler' => ['Controllers\MembersController', 'memberupdatepasswordAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/memberdelete/{memberid}',
    'handler' => ['Controllers\MembersController', 'memberdeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/test/{id}',
    'handler' => ['Controllers\ExampleController', 'testAction']
];

$routes[] = [
    'method' => 'post',
    'route' => '/peacemap/create',
    'handler' => ['Controllers\PeacemapController', 'createAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/peacemap/update',
    'handler' => ['Controllers\PeacemapController', 'updateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/peacemap/list',
    'handler' => ['Controllers\PeacemapController', 'listAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/peacemap/getmap/{id}',
    'handler' => ['Controllers\PeacemapController', 'getmapAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/peacemap/delete/{id}',
    'handler' => ['Controllers\PeacemapController', 'deleteAction'],
    'authentication' => FALSE
];




//Slider
//Slider Add Album
$routes[] = [
	'method' => 'post', 
	'route' => '/slider/addAlbum', 
	'handler' => ['Controllers\ViewalbumController', 'addAlbumAction'],
	'authentication' => FALSE
];

//Slider Album List
$routes[] = [
	'method' => 'get', 
	'route' => '/slider/albumslist/{all}', 
	'handler' => ['Controllers\ViewalbumController', 'albumslistAction'],
	'authentication' => FALSE
];

//Slider View Album
$routes[] = [
	'method' => 'get', 
	'route' => '/slider/albumsimages/{albumid}', 
	'handler' => ['Controllers\ViewalbumController', 'albumsimagesAction'],
	'authentication' => FALSE
];

//Slider Set Status
$routes[] = [
	'method' => 'get', 
	'route' => '/slider/status/{imgID}', 
	'handler' => ['Controllers\ViewalbumController', 'sliderSetStatusAction'],
	'authentication' => FALSE
];

//Slider Update Album
$routes[] = [
	'method' => 'post', 
	'route' => '/slider/addAlbumItem', 
	'handler' => ['Controllers\ViewalbumController', 'addAlbumItemAction'],
	'authentication' => FALSE
];

//Slider Edit Album Image
$routes[] = [
	'method' => 'get', 
	'route' => '/slider/editImage/{imgID}', 
	'handler' => ['Controllers\ViewalbumController', 'editImageAction'],
	'authentication' => FALSE
];

//Slider Edit Update Image
$routes[] = [
	'method' => 'post', 
	'route' => '/slider/updateImage', 
	'handler' => ['Controllers\ViewalbumController', 'updateImageAction'],
	'authentication' => FALSE
];

//Slider Delete Image
$routes[] = [
	'method' => 'get', 
	'route' => '/slider/deleteImage/{imgID}', 
	'handler' => ['Controllers\ViewalbumController', 'deleteImageAction'],
	'authentication' => FALSE
];

//Slider Delete Album
$routes[] = [
	'method' => 'get', 
	'route' => '/slider/deleteAlbum/{album_id}', 
	'handler' => ['Controllers\ViewalbumController', 'deleteAlbumAction'],
	'authentication' => FALSE
];

//Slider Set Album as Main Slider
$routes[] = [
	'method' => 'get', 
	'route' => '/slider/setMainSlider/{album_id}', 
	'handler' => ['Controllers\ViewalbumController', 'setMainSliderAction'],
	'authentication' => FALSE
];

//Slider Sort Slider Images
$routes[] = [
	'method' => 'get', 
	'route' => '/slider/sortSlider/{imgID}/{sort}/{album_id}', 
	'handler' => ['Controllers\ViewalbumController', 'sortSliderAction'],
	'authentication' => FALSE
];



//Banner
//banner Add Album
$routes[] = [
	'method' => 'post', 
	'route' => '/banner/banneraddAlbum', 
	'handler' => ['Controllers\ViewalbumController', 'banneraddrAlbumAction'],
	'authentication' => FALSE
];

//banner Album List
$routes[] = [
	'method' => 'get', 
	'route' => '/banner/banneralbumslist/{all}', 
	'handler' => ['Controllers\ViewalbumController', 'banneralbumslistAction'],
	'authentication' => FALSE
];

//banner View Album
$routes[] = [
	'method' => 'get', 
	'route' => '/banner/banneralbumsimages/{albumid}', 
	'handler' => ['Controllers\ViewalbumController', 'banneralbumsimagesAction'],
	'authentication' => FALSE
];

//banner Set Status
$routes[] = [
	'method' => 'get', 
	'route' => '/banner/status/{imgID}', 
	'handler' => ['Controllers\ViewalbumController', 'bannerSetStatusAction'],
	'authentication' => FALSE
];

//banner Update Album
$routes[] = [
	'method' => 'post', 
	'route' => '/banner/banneraddAlbumItem', 
	'handler' => ['Controllers\ViewalbumController', 'banneraddAlbumItemAction'],
	'authentication' => FALSE
];

//banner Edit Album Image
$routes[] = [
	'method' => 'get', 
	'route' => '/banner/bannereditImage/{imgID}', 
	'handler' => ['Controllers\ViewalbumController', 'bannereditImageAction'],
	'authentication' => FALSE
];

//banner Edit Update Image
$routes[] = [
	'method' => 'post', 
	'route' => '/banner/bannerupdateImage', 
	'handler' => ['Controllers\ViewalbumController', 'bannerupdateImageAction'],
	'authentication' => FALSE
];

//banner Delete Image
$routes[] = [
	'method' => 'get', 
	'route' => '/banner/bannerdeleteImage/{imgID}', 
	'handler' => ['Controllers\ViewalbumController', 'bannerdeleteImageAction'],
	'authentication' => FALSE
];

//banner Delete Album
$routes[] = [
	'method' => 'get', 
	'route' => '/banner/bannerdeleteAlbum/{album_id}', 
	'handler' => ['Controllers\ViewalbumController', 'bannerdeleteAlbumAction'],
	'authentication' => FALSE
];

//Set Album as Main Slider
$routes[] = [
	'method' => 'get', 
	'route' => '/banner/setMainBanner/{album_id}', 
	'handler' => ['Controllers\ViewalbumController', 'setMainBannerAction'],
	'authentication' => FALSE
];

//Sort Slider Images
$routes[] = [
	'method' => 'get', 
	'route' => '/banner/sortBanner/{imgID}/{sort}/{album_id}', 
	'handler' => ['Controllers\ViewalbumController', 'sortBannerAction'],
	'authentication' => FALSE
];



//News Banner
$routes[] = [
	'method' => 'get', 
	'route' => '/news/newsbanner', 
	'handler' => ['Controllers\ViewalbumController', 'newsBannerAction'],
	'authentication' => FALSE
];



$routes[] = [
	'method' => 'post', 
	'route' => '/sliderimage/slider', 
	'handler' => ['Controllers\SliderimageController', 'slideruploadAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/utility/ajaxfileuploader/{filename}/{folderName}/{folderid}', 
	'handler' => ['Controllers\FileuploaderController', 'ajaxfileuploaderAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/utility/listsliderimages/{getid}', 
	'handler' => ['Controllers\FileuploaderController', 'imagelistAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/utility/imginfoupdate', 
	'handler' => ['Controllers\FileuploaderController', 'updateinfoimgAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/utility/dltphoto', 
	'handler' => ['Controllers\FileuploaderController', 'dltphotoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/utility/generateid', 
	'handler' => ['Controllers\FileuploaderController', 'folderidAction'],
	'authentication' => FALSE
];

// hmm
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/mainset/{folderid}', 
	'handler' => ['Controllers\FileuploaderController', 'mainsetAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/utility/mainslides', 
	'handler' => ['Controllers\ViewalbumController', 'mainslideAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/utility/mainbanners', 
	'handler' => ['Controllers\ViewalbumController', 'mainbannersAction'],
	'authentication' => FALSE
];
// hmm

$routes[] = [
	'method' => 'get', 
	'route' => '/utility/testimonials/{num}/{off}/{keyword}', 
	'handler' => ['Controllers\TestimonialsController', 'testimonialAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/utility/testimonial/{offset}',
	'handler' => ['Controllers\TestimonialsController', 'showtestimonialAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/utility/dlt/{id}', 
	'handler' => ['Controllers\TestimonialsController', 'dltAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/view/{id}', 
	'handler' => ['Controllers\TestimonialsController', 'infoviewAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/utility/status/{id}/{status}', 
	'handler' => ['Controllers\TestimonialsController', 'updatestatusAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/utility/updatetestimony/{id}/{message}', 
	'handler' => ['Controllers\TestimonialsController', 'updateAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/utility/submit', 
	'handler' => ['Controllers\TestimonialsController', 'submitAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/utility/getpage', 
	'handler' => ['Controllers\MenuCreatorController', 'getPageAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/utility/savemenu', 
	'handler' => ['Controllers\MenuCreatorController', 'saveAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/utility/listmenu', 
	'handler' => ['Controllers\MenuCreatorController', 'listmenuAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/utility/submenu', 
	'handler' => ['Controllers\MenuCreatorController', 'submenuAction'],
	'authentication' => FALSE
];



//rainier pages


$routes[] = [
	'method' => 'post', 
	'route' => '/pages/ajaxfileuploader', 
	'handler' => ['Controllers\ImageuploaderController', 'ajaxfileuploaderAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/pages/listpageimages', 
	'handler' => ['Controllers\ImageuploaderController', 'imagelistAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/pages/imginfoupdate', 
	'handler' => ['Controllers\ImageuploaderController', 'updateinfoimgAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/pages/dltpagephoto', 
	'handler' => ['Controllers\ImageuploaderController', 'dltphotoAction'],
	'authentication' => FALSE
];


$routes[] = [
	'method' => 'post', 
	'route' => '/pages/create', 
	'handler' => ['Controllers\PagesController', 'createPageAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/pages/manage/{num}/{off}/{keyword}/{sort}/{sortto}', 
	'handler' => ['Controllers\PagesController', 'managepagesAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/pages/editpage/{pageid}', 
	'handler' => ['Controllers\PagesController', 'pageinfoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/pages/updatepage', 
	'handler' => ['Controllers\PagesController', 'pageUpdateAction'],
	'authentication' => FALSE
];


$routes[] = [
	'method' => 'post', 
	'route' => '/page/pagedelete/{pageid}', 
	'handler' => ['Controllers\PagesController', 'pagedeleteAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/pages/getpage/{pageid}',
	'handler' => ['Controllers\PagesController', 'getPageAction'],
	'authentication' => FALSE
];

//UPDATE PAGE STATUS
$routes[] = [
    'method' => 'get',
    'route' => '/page/updatepagestatus/{status}/{pageid}',
    'handler' => ['Controllers\PagesController', 'updatepagestatusAction'],
    'authentication' => FALSE
];

//CHECK FOR PAGE TITLE
$routes[] = [
	'method' => 'get', 
	'route' => '/pages/checkpagetitles/{pagetitle}', 
	'handler' => ['Controllers\PagesController', 'checkpagetitlesAction'],
	'authentication' => FALSE
];
//CHECK FOR NEWS TITLE
$routes[] = [
	'method' => 'get', 
	'route' => '/news/checknewstitles/{newstitle}', 
	'handler' => ['Controllers\NewsController', 'checknewstitlesAction'],
	'authentication' => FALSE
];
//CHECK FOR CATEGORY TITLE
$routes[] = [
	'method' => 'get', 
	'route' => '/news/categorytitle/{cattitle}', 
	'handler' => ['Controllers\NewsController', 'checkcattitlesAction'],
	'authentication' => FALSE
];
//CHECK FOR TAGS TITLE
$routes[] = [
	'method' => 'get', 
	'route' => '/news/tagstitle/{tagstitle}', 
	'handler' => ['Controllers\NewsController', 'checktagstitlesAction'],
	'authentication' => FALSE
];

// Event Manager

$routes[] = [
	'method' => 'get', 
	'route' => '/events/listeventsbanner', 
	'handler' => ['Controllers\EventsmanagerController', 'listeventsbannerAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/events/addeventsbanner', 
	'handler' => ['Controllers\EventsmanagerController', 'addeventsbannerAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/events/publishEvent', 
	'handler' => ['Controllers\EventsmanagerController', 'publishEventAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/events/updateEvent', 
	'handler' => ['Controllers\EventsmanagerController', 'updateEventAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/events/saveEmailInfo', 
	'handler' => ['Controllers\EventsmanagerController', 'saveEmailInfoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/events/saveBanner', 
	'handler' => ['Controllers\EventsmanagerController', 'saveBannerAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/events/saveAmounts', 
	'handler' => ['Controllers\EventsmanagerController', 'saveAmountsAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/events/savePaymentInfo', 
	'handler' => ['Controllers\EventsmanagerController', 'savePaymentInfoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/events/savebillingInfo', 
	'handler' => ['Controllers\EventsmanagerController', 'savebillingInfoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/events/savehdyla', 
	'handler' => ['Controllers\EventsmanagerController', 'savehdylaAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/events/saveCname', 
	'handler' => ['Controllers\EventsmanagerController', 'saveCnameAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/events/eventlist/{num}/{off}/{keyword}/{sort}/{sortto}', 
	'handler' => ['Controllers\EventsmanagerController', 'eventlistAction'],
	'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/events/updateEventStatus/{status}/{eventID}',
    'handler' => ['Controllers\EventsmanagerController', 'updateEventStatusAction'],
    'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/events/deleteEvent/{eventID}', 
	'handler' => ['Controllers\EventsmanagerController', 'deleteEventAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/events/deleteEventCollection/{transactionId}', 
	'handler' => ['Controllers\EventsmanagerController', 'deleteEventCollectionAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/events/eventcontents/{eventID}', 
	'handler' => ['Controllers\EventsmanagerController', 'eventcontentsAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/events/eventsCollection/{num}/{off}/{keyword}/{eventID}/{sort}/{sortto}', 
	'handler' => ['Controllers\EventsmanagerController', 'eventsCollectionAction'],
	'authentication' => FALSE
];

//event Frontend

$routes[] = [
	'method' => 'get', 
	'route' => '/events/getEventContents/{eventsURL}', 
	'handler' => ['Controllers\EventsmanagerController', 'getEventContentsAction'],
	'authentication' => FALSE
];



//rainier news

$routes[] = [
	'method' => 'post', 
	'route' => '/news/savevideo', 
	'handler' => ['Controllers\NewsImageuploaderController', 'savevideoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/news/listnewsimages', 
	'handler' => ['Controllers\NewsImageuploaderController', 'imagelistAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/news/listnewsvideo', 
	'handler' => ['Controllers\NewsImageuploaderController', 'videolistAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/news/imginfoupdate', 
	'handler' => ['Controllers\NewsImageuploaderController', 'updateinfoimgAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/news/dltnewsimg', 
	'handler' => ['Controllers\NewsImageuploaderController', 'dltnewsimgAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/news/dltnewsvid', 
	'handler' => ['Controllers\NewsImageuploaderController', 'dltnewsvidAction'],
	'authentication' => FALSE
];


$routes[] = [
	'method' => 'post', 
	'route' => '/news/loadcategory', 
	'handler' => ['Controllers\NewsController', 'loadcategoryAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/createalbum/{albumname}/{albumid}', 
	'handler' => ['Controllers\FileuploaderController', 'createalbumAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/utility/manage/{num}/{off}/{keyword}', 
	'handler' => ['Controllers\viewalbumController', 'managealbumAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/editalbum/{albumid}', 
	'handler' => ['Controllers\viewalbumController', 'albuminfoAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/updatealbum/{filename}', 
	'handler' => ['Controllers\viewalbumController', 'ajaxfileuploaderAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/slider/deletealbum/{album_id}', 
	'handler' => ['Controllers\ViewalbumController', 'deletealbumAction'],
	'authentication' => FALSE
];


$routes[] = [
	'method' => 'post', 
	'route' => '/news/create', 
	'handler' => ['Controllers\NewsController', 'createNewsAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/news/manage/{num}/{off}/{keyword}/{sortingdate}/{sort}/{sortto}', 
	'handler' => ['Controllers\NewsController', 'managenewsAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/news/managebyfeatcat/{num}/{off}/{keyword}/{sortingdate}/{sort}/{sortto}', 
	'handler' => ['Controllers\NewsController', 'managebyfeatcatAction'],
	'authentication' => FALSE
];



$routes[] = [
	'method' => 'get', 
	'route' => '/news/listcategory', 
	'handler' => ['Controllers\NewsController', 'listcategoryAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/news/listtags', 
	'handler' => ['Controllers\NewsController', 'listtagsAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/news/listauthor', 
	'handler' => ['Controllers\NewsController', 'listauthorAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/news/listauthorimages', 
	'handler' => ['Controllers\NewsController', 'listauthorimagesAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/news/newsdelete/{newsid}', 
	'handler' => ['Controllers\NewsController', 'newsdeleteAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/news/editnews/{newsid}', 
	'handler' => ['Controllers\NewsController', 'newsinfoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/news/updatenews', 
	'handler' => ['Controllers\NewsController', 'newsUpdateAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/news/ajaxfileuploader', 
	'handler' => ['Controllers\NewsImageuploaderController', 'ajaxfileuploaderAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/news/savecategory', 
	'handler' => ['Controllers\NewsController', 'createcategoryAction'],
	'authentication' => FALSE
];

$routes[] = [
 'method' => 'get', 
 'route' => '/news/news/{num}/{off}/{page}', 
 'handler' => ['Controllers\NewsController', 'showNewsAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get', 
 'route' => '/news/showlistnews/{num}/{off}/{page}', 
 'handler' => ['Controllers\NewsController', 'showlistNewsAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get', 
 'route' => '/news/showlistofauthornews/{num}/{off}/{page}/{authorid}', 
 'handler' => ['Controllers\NewsController', 'showlistofauthorNewsAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get', 
 'route' => '/news/showlistnewsbycategory/{num}/{off}/{page}/{categoryslugs}', 
 'handler' => ['Controllers\NewsController', 'showlistnewsbycategoryAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get', 
 'route' => '/news/showlistfeaturedcategory/{num}/{off}/{page}/{categoryslugs}', 
 'handler' => ['Controllers\NewsController', 'showlistfeaturedcategoryAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get', 
 'route' => '/news/showlistnewsbytags/{num}/{off}/{page}/{tagname}', 
 'handler' => ['Controllers\NewsController', 'showlistnewsbytagsAction'],
 'authentication' => FALSE
];

$routes[] = [
	'method' => 'get',
	'route' => '/news/frontend/listnewsbyarchive/{num}/{off}/{page}/{month}/{year}',
	'handler' => ['Controllers\NewsController', 'listnewsbyarchiveAction'],
	'authentication' => FALSE
];

$routes[] = [
 'method' => 'get', 
 'route' => '/news/view/{newsslugs}', 
 'handler' => ['Controllers\NewsController', 'fullnewsAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get', 
 'route' => '/news/author/{authorid}', 
 'handler' => ['Controllers\NewsController', 'metaauthorAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get', 
 'route' => '/news/frontend/listtags/{newsslugs}', 
 'handler' => ['Controllers\NewsController', 'viewfronenttagsAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get', 
 'route' => '/news/frontend/listcategories/{newsslugs}', 
 'handler' => ['Controllers\NewsController', 'viewfronentcategoryAction'],
 'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/news/fullnews/{newsslugs}', 
	'handler' => ['Controllers\NewsController', 'fullnewsAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/news/list', 
	'handler' => ['Controllers\NewsController', 'newslistAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/news/managecategory/{num}/{off}/{keyword}', 
	'handler' => ['Controllers\NewsController', 'managecategoryAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/news/categorydelete/{id}', 
	'handler' => ['Controllers\NewsController', 'categorydeleteAction'],
	'authentication' => FALSE
];


$routes[] = [
	'method' => 'post', 
	'route' => '/news/updatecategorynames',
	'handler' => ['Controllers\NewsController', 'categoryUpdateAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/news/getcategoryfrontend', 
	'handler' => ['Controllers\NewsController', 'getcategoryfrontendAction'],
	'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/gettagsfrontend',
    'handler' => ['Controllers\NewsController', 'TagsListAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/getarchivesfrontend',
    'handler' => ['Controllers\NewsController', 'ArchiveListAction'],
    'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/news/category/{catname}', 
	'handler' => ['Controllers\NewsController', 'getcategorynamefrontendAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/news/tags/{tagname}', 
	'handler' => ['Controllers\NewsController', 'gettagnamefrontendAction'],
	'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/updatenewsstatus/{status}/{newsid}/{keyword}',
    'handler' => ['Controllers\NewsController', 'newsUpdatestatusAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/updatenewsfeatstatus/{feat}/{featurednews}/{newsid}/{keyword}',
    'handler' => ['Controllers\NewsController', 'updatenewsfeatstatusAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post', 
    'route' => '/news/savetags', 
    'handler' => ['Controllers\NewsController', 'createtagsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get', 
    'route' => '/news/managetags/{num}/{off}/{keyword}', 
    'handler' => ['Controllers\NewsController', 'managetagsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post', 
    'route' => '/news/tagsdelete/{id}', 
    'handler' => ['Controllers\NewsController', 'tagsdeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post', 
    'route' => '/news/updatetags',
    'handler' => ['Controllers\NewsController', 'updatetagsAction'],
    'authentication' => FALSE
];


////////////////////////AUTHOR

$routes[] = [
    'method' => 'get',
    'route' => '/news/authorlistimages',
    'handler' => ['Controllers\NewsImageuploaderController', 'authorimagelistAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/createauthor',
    'handler' => ['Controllers\NewsController', 'createAuthorAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get', 
    'route' => '/news/manageauthor/{num}/{off}/{keyword}', 
    'handler' => ['Controllers\NewsController', 'manageAuthorAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/editauthor/{authorid}',
    'handler' => ['Controllers\NewsController', 'authoreditoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/edieauthor',
    'handler' => ['Controllers\NewsController', 'authorupdateAction'],
    'authentication' => FALSE
];

$routes[] = [
 'method' => 'post', 
 'route' => '/news/uploadauthorimage', 
 'handler' => ['Controllers\NewsImageuploaderController', 'uploadauthorimageAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'post', 
 'route' => '/news/deleteauthorimg', 
 'handler' => ['Controllers\NewsImageuploaderController', 'dltauthorimageAction'],
 'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/authordelete/{authorid}',
    'handler' => ['Controllers\NewsController', 'authordeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
 'method' => 'get', 
 'route' => '/news/getauthorbyid/{authorid}', 
 'handler' => ['Controllers\NewsController', 'getauthorbyidAction'],
 'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/authordelete/{authorid}',
    'handler' => ['Controllers\NewsController', 'authordeleteAction'],
    'authentication' => FALSE
];

//////////////////////

//////FEATURED NEWS MAIN PAGE

$routes[] = [
 'method' => 'get', 
 'route' => '/news/featuredblog/{num}', 
 'handler' => ['Controllers\NewsController', 'featuredblogAction'],
 'authentication' => FALSE
];

/////////////////////////

$routes[] = [
	'method' => 'post', 
	'route' => '/newsletter/create',
	'handler' => ['Controllers\NewsLetterController', 'createNewsLetterAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/newsletter/manage/{num}/{off}/{keyword}', 
	'handler' => ['Controllers\NewsLetterController', 'managenewsletterAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/newsletter/newsletterdelete/{newsletterid}', 
	'handler' => ['Controllers\NewsLetterController', 'newsletterdeleteAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/newsletter/editnewsletter/{newsletterid}', 
	'handler' => ['Controllers\NewsLetterController', 'newsletterinfoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/newsletter/updatenewsletter', 
	'handler' => ['Controllers\NewsLetterController', 'newsletterUpdateAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/newsletter/subscriberslist/{newsletterid}', 
	'handler' => ['Controllers\NewsLetterController', 'subscriberslistAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/newsletter/sendnewsletter/{NSemail}', 
	'handler' => ['Controllers\NewsLetterController', 'sendnewsletterAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/newsletter/memberlist/{newsletterid}', 
	'handler' => ['Controllers\NewsLetterController', 'memberlistAction'],
	'authentication' => FALSE
];






// uson


$routes[] = [
	'method' => 'get', 
	'route' => '/proposals/listfiles', 
	'handler' => ['Controllers\ProposalsController', 'filelistAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/proposals/create', 
	'handler' => ['Controllers\ProposalsController', 'submitproposalAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/proposals/manage/{num}/{off}/{keyword}', 
	'handler' => ['Controllers\ProposalsController', 'manageproposalsAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/proposals/view/{id}', 
	'handler' => ['Controllers\ProposalsController', 'viewproposalAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/proposals/reply', 
	'handler' => ['Controllers\ProposalsController', 'replyproposalAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/bnb/centernamelist', 
	'handler' => ['Controllers\MembersController', 'centernamesAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/newmemreg/{userid}', 
	'handler' => ['Controllers\MembersController', 'newmemregAction'],
	'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/members/getinfo/{memid}',
    'handler' => ['Controllers\MembersController', 'getinfoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/newmember',
    'handler' => ['Controllers\MembersController', 'newmemberAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/settings/managesettings',
    'handler' => ['Controllers\SettingsController', 'managesettingsAction'],
    'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/settings/on/{id}', 
	'handler' => ['Controllers\SettingsController', 'maintenanceAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/settings/maintenanceon',  
	'handler' => ['Controllers\SettingsController', 'maintenanceonAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/settings/off/{id}', 
	'handler' => ['Controllers\SettingsController', 'maintenanceAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/settings/maintenanceoff',  
	'handler' => ['Controllers\SettingsController', 'maintenanceoffAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/proposals/ajaxfileuploader/{filename}/{type}', 
	'handler' => ['Controllers\ProposalsController', 'ajaxfileuploaderAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/forgotpassword/resetpassword', 
	'handler' => ['Controllers\ForgotpasswordController', 'resetpasswordAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/checktoken/check/{email}', 
	'handler' => ['Controllers\ForgotpasswordController', 'checktokenAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/updatepassword/token', 
	'handler' => ['Controllers\ForgotpasswordController', 'updatepasswordtokenAction'],
	'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/settings/clienttimezone/{tztztz}',
    'handler' => ['Controllers\MembersController', 'getinfoAction'],
    'authentication' => FALSE
];



// uson end


// unahan ng rota ni jimmy

// Donation List
$routes[] = [
	'method' => 'post', 
	'route' => '/donation/donationlist/{num}/{off}/{keyword}/{sort}/{sortto}/{listIn}', 
	'handler' => ['Controllers\DonationController', 'donationlistAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get',
	'route' => '/donation/donationlistothers/{num}/{off}/{keyword}/{sort}/{sortto}/{listIn}',
	'handler' => ['Controllers\DonationController', 'donationlistothersAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get',
	'route' => '/donation/donationlisthouston/{num}/{off}/{keyword}/{sort}/{sortto}/{listIn}',
	'handler' => ['Controllers\DonationController', 'donationlisthoustonAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get',
	'route' => '/donation/donationlistseattle/{num}/{off}/{keyword}/{sort}/{sortto}/{listIn}',
	'handler' => ['Controllers\DonationController', 'donationlistseattleAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get',
	'route' => '/donation/donationlistny/{num}/{off}/{keyword}/{sort}/{sortto}/{listIn}',
	'handler' => ['Controllers\DonationController', 'donationlistnyAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/donation/donationlistothersbytimestamp1/{num}/{off}/{keyword}',
	'handler' => ['Controllers\DonationController', 'donationlistothersbytimestamp1Action'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get',
	'route' => '/donation/donationlisthoustonbytimestamp/{num}/{off}/{keyword}',
	'handler' => ['Controllers\DonationController', 'donationlisthoustonbytimestampAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/donation/donationlistseattlebytimestamp/{num}/{off}/{keyword}',
	'handler' => ['Controllers\DonationController', 'donationlistseattlebytimestampAction'],
	'authentication' => FALSE
];


$routes[] = [
	'method' => 'post', 
	'route' => '/donation/deletedonor/{pageid}', 
	'handler' => ['Controllers\DonationController', 'deletedonorAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/donation/deleteotherdonor/{pageid}', 
	'handler' => ['Controllers\DonationController', 'deleteotherdonorAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/donation/deleteotherdonor1/{pageid}',
	'handler' => ['Controllers\DonationController', 'deleteotherdonor1Action'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/donation/deletehoustondonor/{pageid}',
	'handler' => ['Controllers\DonationController', 'deletehoustondonorAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/donation/deleteseattledonor/{pageid}',
	'handler' => ['Controllers\DonationController', 'deleteseattledonorAction'],
	'authentication' => FALSE
];

// featured project

$routes[] = [
	'method' => 'post', 
	'route' => '/featuredprojects/insdata',
	'handler' => ['Controllers\FeaturedprojectsController', 'featdataAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/featuredprojects/ajaxfileuploader/{filename}', 
	'handler' => ['Controllers\FeaturedprojectsController', 'ajaxfileuploaderAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/featuredprojects/listsimg', 
	'handler' => ['Controllers\FeaturedprojectsController', 'imagelistAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/featuredprojects/slidelist', 
	'handler' => ['Controllers\FeaturedprojectsController', 'slidesAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/featuredprojects/dltpagephoto', 
	'handler' => ['Controllers\FeaturedprojectsController', 'dltphotoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/featuredprojects/dltfeatphoto', 
	'handler' => ['Controllers\FeaturedprojectsController', 'dltfeatphotoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/featuredprojects/managefeature/{num}/{off}/{keyword}', 
	'handler' => ['Controllers\FeaturedprojectsController', 'managefeaturedprojectAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/featuredprojects/editfeature/{pageid}', 
	'handler' => ['Controllers\FeaturedprojectsController', 'featureinfoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/featuredprojects/updatefeature', 
	'handler' => ['Controllers\FeaturedprojectsController', 'featureUpdateAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/featuredprojects/featuredelete/{pageid}', 
	'handler' => ['Controllers\FeaturedprojectsController', 'featuredeleteAction'],
	'authentication' => FALSE
];

// front end routes

$routes[] = [
	'method' => 'get', 
	'route' => '/featuredprojects/viewfeature', 
	'handler' => ['Controllers\FeaturedprojectsController', 'featureviewAction'],
	'authentication' => FALSE
];

// offset (show more)

$routes[] = [
	'method' => 'get', 
	'route' => '/featuredprojects/lispro/{offset}',
	'handler' => ['Controllers\FeaturedprojectsController', 'featuredsAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/featuredprojects/lispro2/{offset}',
	'handler' => ['Controllers\FeaturedprojectsController', 'featuredsAction2'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/featuredprojects/listact/{pageid}', 
	'handler' => ['Controllers\FeaturedprojectsController', 'listactAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/featuredprojects/projectslider/{featid}', 
	'handler' => ['Controllers\FeaturedprojectsController', 'projectsliderAction'],
	'authentication' => FALSE
];

// calendar

$routes[] = [
	'method' => 'post', 
	'route' => '/calendar/insdata',
	'handler' => ['Controllers\CalendarController', 'addactAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/calendar/ajaxfileuploader/{filename}', 
	'handler' => ['Controllers\CalendarController', 'ajaxfileuploaderAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/calendar/listsimg', 
	'handler' => ['Controllers\CalendarController', 'imagelistAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/calendar/dltpagephoto', 
	'handler' => ['Controllers\CalendarController', 'dltphotoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/calendar/dltfeatphoto', 
	'handler' => ['Controllers\CalendarController', 'dltactphotoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/calendar/manageactivity', 
	'handler' => ['Controllers\CalendarController', 'viewcalendarAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/calendar/listview', 
	'handler' => ['Controllers\CalendarController', 'listviewAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/calendar/editactivity/{pageid}',
	'handler' => ['Controllers\CalendarController', 'activityinfoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/calendar/updateactivity', 
	'handler' => ['Controllers\CalendarController', 'activityUpdateAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/calendar/activitydelete/{pageid}', 
	'handler' => ['Controllers\CalendarController', 'activitydeleteAction'],
	'authentication' => FALSE
];


// subscriber+

$routes[] = [
	'method' => 'post', 
	'route' => '/subscribers/addNMS', 
	'handler' => ['Controllers\SubscribersController', 'nmsaddAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/subscribers/insdata', 
	'handler' => ['Controllers\SubscribersController', 'addsubscriberAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/subscribers/subscriberslist/{num}/{off}/{keyword}', 
	'handler' => ['Controllers\SubscribersController', 'subscriberslistAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/subscribers/editfeature/{pageid}', 
	'handler' => ['Controllers\SubscribersController', 'subscriberinfoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/subscribers/updatefeature', 
	'handler' => ['Controllers\SubscribersController', 'updatesubscriberAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/subscribers/subscriberslist/{pageid}', 
	'handler' => ['Controllers\SubscribersController', 'subscribersdeleteAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/pages/getpageproject', 
	'handler' => ['Controllers\PagesController', 'getPageprojectAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/example/skip/{name}', 
	'handler' => ['Controllers\ExampleController', 'skipAction'],
	'authentication' => FALSE
];

// bnb reg
$routes[] = [
	'method' => 'get', 
	'route' => '/bnb/cnamelist',
	'handler' => ['Controllers\MembersController', 'centernamesAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/bnb/insToMember', 
	'handler' => ['Controllers\MembersController', 'addToMemberAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/subscribers/sentlist/{num}/{off}/{keyword}', 
	'handler' => ['Controllers\SubscribersController', 'sentlistAction'],
	'authentication' => FALSE
];


$routes[] = [
	'method' => 'get',
	'route' => '/user/text/{num}',
	'handler' => ['Controllers\UserController', 'indexAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/users/create', 
	'handler' => ['Controllers\UserController', 'createSaveAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/users/chkUsername', 
	'handler' => ['Controllers\UserController', 'chkUsernameAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/users/chkEmail', 
	'handler' => ['Controllers\UserController', 'chkEmailAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/users/changepassword', 
	'handler' => ['Controllers\UserController', 'changePasswordAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/users/userchangepassword', 
	'handler' => ['Controllers\UserController', 'userchangePasswordAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/users/editRestrictions', 
	'handler' => ['Controllers\UserController', 'editRestrictionsAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/users/updateuserInfo', 
	'handler' => ['Controllers\UserController', 'updateuserInfoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/users/updateProfilePhoto', 
	'handler' => ['Controllers\UserController', 'updateProfilePhotoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/users/manageusers/{num}/{off}/{keyword}/{sort}/{sortto}', 
	'handler' => ['Controllers\UserController', 'manageusersAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/users/deleteuser/{userid}', 
	'handler' => ['Controllers\UserController', 'deleteuserAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/users/edituser/{userid}', 
	'handler' => ['Controllers\UserController', 'edituserAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/users/updateuser', 
	'handler' => ['Controllers\UserController', 'updateuserAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/users/updateusername', 
	'handler' => ['Controllers\UserController', 'updateusernameAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/users/updateuseremail', 
	'handler' => ['Controllers\UserController', 'updateuseremailAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/users/status/{userid}/{userLevel}/{status}', 
	'handler' => ['Controllers\UserController', 'updatestatusAction'],
	'authentication' => FALSE
];


$routes[] = [
	'method' => 'post', 
	'route' => '/users/updateprofile', 
	'handler' => ['Controllers\UserController', 'updateprofileAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/users/UploadImage/{filename}', 
	'handler' => ['Controllers\UserController', 'ajaxfileuploaderAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/users/UploadNewImage/{filename}/{userid}', 
	'handler' => ['Controllers\UserController', 'ajaxfileuploader2Action'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/users/img', 
	'handler' => ['Controllers\UserController', 'imagelistAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/users/delimg/{id}', 
	'handler' => ['Controllers\UserController', 'dltphotoAction'],
	'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/vendor/mailcheck',
    'handler' => ['Controllers\VendorController', 'MailcheckvendorAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/vendor/seattlecreditcard',
    'handler' => ['Controllers\VendorController', 'CCseattlevendorAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/vendor/seattleecheck',
    'handler' => ['Controllers\VendorController', 'EcheckseattlevendorAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/vendor/seattlevendorlist/{num}/{off}/{keyword}/{sortpaytype}/{sort}/{sortto}/{listIn}',
    'handler' => ['Controllers\VendorController', 'SeattlevendorlistAction'],
    'authentication' => FALSE
];

$routes[] = [
	'method' => 'get',
	'route' => '/vendor/viewvendor/{vendorid}',
	'handler' => ['Controllers\VendorController', 'viewvendorAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get',
	'route' => '/vendor/markpaid/{vendorid}/{stat}',
	'handler' => ['Controllers\VendorController', 'markpaidvendorAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get',
	'route' => '/vendor/delete/{vendorid}',
	'handler' => ['Controllers\VendorController', 'deletevendorAction'],
	'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/donate/creditcard',
    'handler' => ['Controllers\DonationController', 'donatecreditcardAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/donate/echeck',
    'handler' => ['Controllers\DonationController', 'echeckdonateAction'],
    'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/users/viewuser/{userid}', 
	'handler' => ['Controllers\UserController', 'viewuserAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/settings/googleanalytics',  
	'handler' => ['Controllers\SettingsController', 'scriptAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/settings/loadscript',  
	'handler' => ['Controllers\SettingsController', 'loadscriptAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/settings/script',  
	'handler' => ['Controllers\SettingsController', 'displaytAction'],
	'authentication' => FALSE
];


$routes[] = [
	'method' => 'post',
	'route' => '/email/send',
	'handler' => ['Controllers\MembersController', 'sendMandrillEmailAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/donate/other/creditcard',
	'handler' => ['Controllers\DonationController', 'donatecreditcardotherAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/donate/other/echeck',
	'handler' => ['Controllers\DonationController', 'echeckdonateotherAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/donate/creditcardworeg',
	'handler' => ['Controllers\DonationController', 'donatecreditcardworegAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/donate/echeckcardworeg',
	'handler' => ['Controllers\DonationController', 'echeckdonateworegAction'],
	'authentication' => FALSE
];

//RSS
$routes[] = [
 'method' => 'get', 
 'route' => '/news/newsrss/{offset}', 
 'handler' => ['Controllers\NewsController', 'newrssAction'],
 'authentication' => FALSE
];

//Programs Payment

$routes[] = [
	'method' => 'post',
	'route' => '/programPayment/creditcard',
	'handler' => ['Controllers\ProgramsController', 'HLTcreditcardAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/programPayment/echeck',
	'handler' => ['Controllers\ProgramsController', 'HLTecheckAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get',
	'route' => '/programs/hlt/{num}/{off}/{keyword}/{sort}/{sortto}',
	'handler' => ['Controllers\ProgramsController', 'hltAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/program/deletehtl/{id}',
	'handler' => ['Controllers\ProgramsController', 'deletehtlAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get',
	'route' => '/program/hltview/{id}',
	'handler' => ['Controllers\ProgramsController', 'viewhltAction'],
	'authentication' => FALSE
];

return $routes;
