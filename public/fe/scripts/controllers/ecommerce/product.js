app.controller("productCtrl", function($scope, appConfig, $modal, store, ProductsFactory, Login, $window){
  $scope.cart = store.get('cart');
  Login.getUserFromToken(function(data) {
    $scope.user = data;
  });

  $scope.addtocart = function(productid, minquantity) {
    if(!$scope.itemQuantityOnCart()){
      Login.getUserFromToken(function(data) {
        if(!data.hasOwnProperty('error')){
          var post = {
            id : productid,
            quantity : minquantity,
            memberid : data.id
          }
        } else {
          var post = {
            id : productid,
            quantity : minquantity,
            memberid : undefined
          }
        }

        if($scope.cart == null){
          $scope.cart = [];
        }

        ProductsFactory.addtocart(post, function(data) {
          $scope.product = data.product;
          $scope.total = data.total;
          if(data.push){
            $scope.cart.push(data.product);
          }else if(data.push == false) {
            angular.forEach($scope.cart, function(value, key) {
              if(productid == value['productid']){
                $scope.cart[key] = data.product;
              }
            });
          } else if(data.push == undefined) {
            var push = true;
            angular.forEach($scope.cart, function(value, key) {
              if(value['productid'] == productid){
                $scope.cart[key]['cartquantity'] = parseInt($scope.cart[key]['cartquantity']) + parseInt(minquantity);
                push = false;
              }
            });

            if(push){
              $scope.cart.push(data.product);
            }
          }

          store.set('cart', $scope.cart);
          $window.location = "/shop/mycart";
        })
      });
    }
  }


  // EXTRA FUNCTIONS
  $scope.convertToInt= function (value) {
    return parseFloat(value);
  };
  $scope.itemQuantityOnCart = function(productid, maxquantity) {
    var data = false;
    $scope.enable = true;
    if(store.get('cart')){
      store.get('cart').map(function(value){
        if(value['productid'] == productid) { 
          if(maxquantity <= value['cartquantity']){
            data = true;
            $scope.limit = maxquantity;
          }  else {
            data = false;
          }
        }
      })
    }
    return data;
  }
})