app.controller('mycartCtrl', function($scope, appConfig, Login, $modal, MycartFactory, $window, store) {
  $scope.amazonimage = appConfig.ImageLinK + "/uploads/productimage";
  $scope.cart = store.get('cart');
  $scope.shippingfee = 0;
  $scope.total = 0;
  Login.getUserFromToken(function(data) {
    $scope.user = data;
  });

  var taxpercentage = 0.05;

  var setshippingfee = function() {
    if($scope.cart.length > 0){
      $scope.shippingfee = 2;
    } else {
      $scope.shippingfee = 0;
      $scope.total = 0;
    }
  }

  $scope.$watch(function() {
    return $scope.cart.length;
  }, function() {
    setshippingfee();
  });

  var generatetotal = function() {
    $scope.total = 0;
    angular.forEach($scope.cart, function(value, key) {
      var discount_from = new Date(value['discount_from']).getTime();
      var discount_to = new Date(value['discount_to']).getTime();
      var now = new Date(new Date().toJSON().slice(0,10)).getTime();
      if(discount_from <= now && now <= discount_to){
          $scope.cart[key].disc = true;
          var total =  value['price'] - ((value['discount'] / 100) * value['price']);
          $scope.cart[key].total = total * value['cartquantity'];
      }else {
          $scope.cart[key].disc = false;
          $scope.cart[key].total = value['price'] * value['cartquantity'];
      }
      $scope.total = $scope.total + $scope.cart[key].total;
    });
    setshippingfee();
    $scope.subtotal = $scope.total;
    $scope.tax = $scope.total * taxpercentage;
    $scope.total = $scope.total + $scope.tax + $scope.shippingfee;
    console.log($scope.shippingfee);
  };

  generatetotal(); 

  $scope.convertToInt= function (value) {
    return parseFloat(value);
  };

  $scope.minusquantity = function(productid, quantity) {
    quantity--;
    $scope.selectquantity(productid, quantity);
  }

  $scope.plusquantity = function(productid, quantity) {
    quantity++;
    $scope.selectquantity(productid, quantity);
  }

  var updatecart = function(productid, quantity) {
    angular.forEach($scope.cart, function(value, key) {
      if(value['productid'] == productid){
        $scope.cart[key]['cartquantity'] = quantity;
      }
    });

    store.set('cart', $scope.cart);
    generatetotal();
  }

  $scope.selectquantity = function(productid, quantity) {
    if($scope.user.firstname != undefined){
      MycartFactory.updatecart(productid, quantity, function(data) {
        updatecart(productid, quantity);
      });
    } else {
      angular.forEach($scope.cart, function(value, key) {
        updatecart(productid, quantity);
      }) 
    }
  }

  var spliceitem = function(productid) {
    angular.forEach($scope.cart, function(value, key) {
      if(value['productid'] == productid){
        $scope.cart.splice(key, 1);
      }
    });
    generatetotal();
    store.set('cart',$scope.cart);
  }

  $scope.removeitem = function(productid) {
    Login.getUserFromToken(function(data) {
      if(!data.hasOwnProperty('error')){
        MycartFactory.removeitem(productid, data.id, function(data) {
          if(data.hasOwnProperty('success')){
            spliceitem(productid);
          }
        });
      } else {
        spliceitem(productid);
      }
    });
  }

  $scope.emptycart = function() {
    $modal.open({
      templateUrl: 'confirm',
      size:'sm',
      controller: function($scope, $modalInstance){
        $scope.cancel = function() {
          $modalInstance.close();
        }
        $scope.ok = function() {
          Login.getUserFromToken(function(data) {
            if(!data.hasOwnProperty('error')){
              MycartFactory.removeallitem(data.id, function(data) {
                if(data.hasOwnProperty('success')){
                  store.remove('cart');
                  $modalInstance.close([]);
                }
              });
            }else {
              store.remove('cart');
              $modalInstance.close([]);
            }
            generatetotal();
          });
        }
      }
    }).result.then(function(data) {
      $scope.cart = data;
    });
  }

  // EXTRA FUNCTIONS
  $scope.optionQuantity = function(min, max) {
    var quantities = [];
    for(var n=min; n<=max; n++) {
      quantities.push(n);
    }
    return quantities;
  }

  $scope.gotoproduct = function(slugs) {
    $window.location.href = appConfig.shop + "/" + slugs;
  }

})