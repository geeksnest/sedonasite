app.controller('productsCtrl', function($scope, appConfig, $localStorage, $modal, ProductsFactory, $window, Login, store) {
  $scope.amazonimage = appConfig.ImageLinK + "/uploads/productimage";

  $scope.sort = "popular";
  $scope.page = 1;
  $scope.limit = 12;
  $scope.keyword = null;

  var loadlist = function(page, limit, sort, keyword) {
    ProductsFactory.listofproducts(page, limit, sort, keyword, function(data){
      console.log(data);
    $scope.products = data.products;
    $scope.totalItems = data.total;
    })
  }

  loadlist($scope.page, $scope.limit, $scope.sort, $scope.keyword);

  Login.getUserFromToken(function(data) {
    $scope.user = data;
  });

  //setting the localstorage for cart
  // if($localStorage.cart == undefined) {
  //   $localStorage.cart = []; //intialization (also might need this same line in clearing the cart)
  // }

  $scope.cart = store.get('cart'); //scope of cart

  $scope.addtocart = function(productid, minquantity) {
    // cart(productid, minquantity);
    MODAL({
      templateUrl : "addtocart",
      controller  : addtocartCTRL,
      size        : "lg",
      asset       : { productid : productid, quantity : minquantity }
    });
  }
  // var cart = function(productid, minquantity) {
  //   for(var n=1; n<=minquantity; n++) {
  //     $localStorage.cart.push(productid);
  //   }
  // };

  //BELOW IS ALL MODAL RELATED
  var MODAL = function(modal) {
    var modalInstance = $modal.open({
      templateUrl: modal.templateUrl,
      controller: modal.controller,
      size: modal.size,
      resolve: {
        asset: function() {
          return modal.asset;
        },
        cart: function() {
          return $scope.cart;
        }
      }
    }).result.then(function(data) { // if close not dismiss

    });
  }

  var addtocartCTRL = function($scope, $modalInstance, asset, cart) {
    $scope.amazonimage = appConfig.ImageLinK + "/uploads/productimage";

    Login.getUserFromToken(function(data) {
      if(!data.hasOwnProperty('error')){
        var post = {
          id : asset.productid,
          quantity : asset.quantity,
          memberid : data.id
        }
      } else {
        var post = {
          id : asset.productid,
          quantity : asset.quantity,
          memberid : undefined
        }
      }

      if(cart == null){
        cart = [];
      }

      ProductsFactory.addtocart(post, function(data) {
        $scope.product = data.product;
        $scope.total = data.total;
        if(data.push){
          cart.push(data.product);
        }else if(data.push == false) {
          angular.forEach(cart, function(value, key) {
            if(asset.productid == value['productid']){
              cart[key] = data.product;
            }
          });
        } else if(data.push == undefined) {
          var push = true;
          angular.forEach(cart, function(value, key) {
            if(value['productid'] == asset.productid){
              cart[key]['cartquantity'] = parseInt(cart[key]['cartquantity']) + parseInt(asset.quantity);
              push = false;
            }
          });

          if(push){
            cart.push(data.product);
          }
        }
        store.set('cart', cart);
      })
    });

    $scope.cancel = function() {
      $modalInstance.dismiss();
    }
    $scope.ok = function() {

    }
  };

  // EXTRA FUNCTIONS
  $scope.gotoproduct = function(slugs) {
    $window.location.href = "/shop/"+slugs;
  }
  $scope.convertToInt= function (value) {
    return parseFloat(value);
  };
  $scope.itemQuantityOnCart = function(productid) {
    var quantity = 0;
    if(store.get('cart')){
      store.get('cart').map(function(value){
        if(value['productid'] == productid) { quantity = value['cartquantity']; }
      })
    }
    return quantity;
  }
  $scope.productPrice = function(price, discount) {
    if(discount != "" || discount != undefined || discount != 0 || discount < 0) {
      return price - (price * (discount/100));
    } else {
      return price;
    }
  }

  $scope.sortby = function(sort) {
    loadlist(1, $scope.limit, sort, null);
  }

  $scope.limitby = function(limit) {
    loadlist(1, limit, $scope.sort, null);
  }

  $scope.search = function(keyword) {
    if(keyword == ""){
      keyword = null;
    }
    loadlist(1, $scope.limit, $scope.sort, keyword);
    $scope.keyword = null;
  }

  $scope.setPage = function(page) {
    loadlist(page, $scope.limit, $scope.sort, $scope.keyword);
  }
})