app.controller('checkoutCtrl', function($scope, appConfig, $modal, $window, store, DateFactory, MycartFactory, CheckoutFactory, jwtHelper, Login, rfc4122, toaster, Reservation, $timeout) {
  $scope.amazonimage = appConfig.ImageLinK + "/uploads/productimage";
  $scope.BaseURL = appConfig.BaseURL;
  var TOKEN = store.get("jwt");
  $scope.token = TOKEN;
  $scope.cart = store.get('cart');
  $scope.payment = {};
  $scope.shippinginfo = {};
  var taxpercentage = 0.05;
  var uniqid = rfc4122.v4();
  $scope.userlogin = {};
  $scope.invoiceno = parseInt(uniqid.substr(0, 8), 16) * 10;
  $scope.emailverify = false;
  $scope.paymenttype = 'card';
  Login.getUserFromToken(function(data) {
    $scope.member = data;
  });
  $scope.repasswordconf = false ;

  if(TOKEN !== undefined) {
    $timeout(function() {
      angular.element("#shippingMethod").addClass("in");
    }, 500);
  }
  var accordion = ['checkoutMethod','billingInformation','shippingMethod','paymentInformation','orderReview'];

  $scope.collapse = function(n) {
    COLLAPSE(n);
  };

  var COLLAPSE = function(n) {
    angular.element(".collapse").removeClass("in");
    angular.element(".collapse").attr("href", "");

    angular.element("#"+accordion[n]).addClass("in");

    for(var o=0; o<=accordion.length; o++) {
      if(o <= n && o != accordion.indexOf("shippingMethod")) {
        // angular.element(".class"+o).attr("href", "#"+accordion[o]);
      }
      else { angular.element(".class"+o).attr("href", ""); }
    }
    angular.element("#"+accordion[n]).addClass("in");
  };

  $scope.paymentmethod = function(payment) {
    COLLAPSE(4);
  };

  var setshippingfee = function() {
    if($scope.cart.length > 0){
      $scope.shippingfee = 2;
    } else {
      $scope.shippingfee = 0;
      $scope.total = 0;
    }
  };

  var generatetotal = function() {
    $scope.total = 0;
    angular.forEach($scope.cart, function(value, key) {
      var discount_from = new Date(value.discount_from).getTime();
      var discount_to = new Date(value.discount_to).getTime();
      var now = new Date(new Date().toJSON().slice(0,10)).getTime();
      if(discount_from <= now && now <= discount_to){
          $scope.cart[key].disc = true;
          var total =  value.price - ((value.discount / 100) * value.price);
          $scope.cart[key].total = total * value.cartquantity;
      }else {
          $scope.cart[key].disc = false;
          $scope.cart[key].total = value.price * value.cartquantity;
      }
      $scope.total = $scope.total + $scope.cart[key].total;
    });
    setshippingfee();
    $scope.subtotal = $scope.total;
    $scope.tax = $scope.total * taxpercentage;
    $scope.total = $scope.total + $scope.tax + $scope.shippingfee;
  };

  generatetotal();

  $scope.granddtotal = function(flatrate) {
    $scope.grandtotal = $scope.total + flatrate;
    return $scope.grandtotal;
  };

  $scope.PLACEORDER = function() {
      $scope.loader = true;
      var order = {
          paymenttype : $scope.paymenttype,
          order: $scope.cart,
          shippinginfo: $scope.shippinginfo,
          payment : $scope.payment,
          totalamount: $scope.subtotal,
          tax : $scope.tax,
          shippingfee : $scope.shippingfee,
          invoice : $scope.invoiceno
      };
      CheckoutFactory.placeorder(order, $scope.member.id, function(data) {
          if(data.hasOwnProperty('success')){
            toaster.pop('success', '', 'Your order has been successfully submitted.');
            store.remove('cart');
            $timeout(function() {
                $window.location = '/shop';
            }, 4000);
          }else {
            toaster.pop('error', '', '* make sure you have already entered your billing information on the account settings.');
          }
      });
  };

  $scope.expimonth = DateFactory.month();
  $scope.expiyear = DateFactory.expiyear_up();

  $scope.test = function(val) {
    console.log(val);
  };

  $scope.dologin_checkout = function(userlogin) {
    $scope.saveloading = true;
    CheckoutFactory.login({ user: userlogin, cart: store.get('cart') }, function(data){
        if(data.hasOwnProperty('access')){
            $scope.errorpanel = false;
            store.set('jwt', data.access);
            var token = jwtHelper.decodeToken(store.get('jwt'));
            store.set('loginname', token.firstname +' ' + token.lastname);
            store.set('logintype', 'normal');
            $scope.loginname =  token.firstname +' ' + token.lastname;
            $window.location = '/shop/checkout';
            $scope.saveloading = false;
        } else{
            $scope.errormsg = data.error;
            $scope.errorpanel = true;
            $scope.saveloading = false;
        }
    });
  };

  $scope.processpaypal = function(){
    console.log("ASD");
    var params = {
      'invoice': $scope.invoiceno,
      'shippinginfo': $scope.shippinginfo,
      'orders' : $scope.cart,
      'member' : $scope.member,
      'totalamount' : $scope.total
    };

    CheckoutFactory.savepaypaltemp(params, function(data) {
      if(data.hasOwnProperty('success')){
        store.set('bag', {});
        $('.paypalform').submit();
      } else if(data.hasOwnProperty('error')){
        $scope.paypalprocess = false;
        $scope.paypalprocesserror = 'Something went wrong please try again later!';
      }
    });
  };

  $scope.checkEmail = function(val){
      $scope.emailexist = 0;
      $scope.existinguser = '';
      var re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
      if(re.test(val)){
          Reservation.checkmemberemail(val, function(data){
              if(data.length === 0){
                  $scope.emailexist = 2;
                  console.log("Clear not used");
                  $scope.user = {email: val, reservationlist: [], paymenttype: '', country: 'USA'};
              }else{
                  if(data[0].status=='INCOMPLETE'){
                      $scope.emailexist = 3;
                      console.log('Incomplete Profile');
                  }else{
                      $scope.emailexist = 1;
                      console.log("Email is used");
                      $scope.existinguser = data;
                  }
              }
          });
      }
  };

  var set = function(){
      $scope.register = { country: 'USA' };
  };

  set();

  $scope.registerUser_checkout = function(member, form){
      $scope.saveloading = true;
      member.formfrom = 'checkout';
      $scope.saving = true;
      member.login = true;
      Reservation.registration(member, function(data){
          if(data.hasOwnProperty('success')){
              form.$setPristine();
              $scope.login=true;
              set();
              $scope.saving = false;
              $scope.regsuccess = true;
              $scope.regform = false;
              $scope.emailexist = false;
              $scope.user = {};
              $scope.email = member.email;
              $scope.saveloading = false;
              store.set("shop", true);
              $scope.emailverify = true;
          }else if(data.hasOwnProperty('error')){
              Notification.error({message: data.error, positionY: 'bottom', positionX: 'right'});
              $scope.saving = false;
              $scope.saveloading = false;
          }
      });
  };

  $scope.passwordmatch = function(password, repassword) {
    if(password == repassword) {
      $scope.repasswordconf = false;
    } else {
      $scope.repasswordconf = true;
    }
    console.log($scope.repasswordconf);
  };

  $scope.gotoproduct = function(slugs) {
    $window.location.href = appConfig.shop + "/" + slugs;
  };
});
