'use strict';

/* Controllers */

app.controller('UserloginCtrl', function($scope, $http, Login, jwtHelper, store, $window, Facebook, UserregistrationFactory, Reservation, Notification){
    $scope.dologin = function(userlogin) {
        $scope.saveloading = true;
        Login.login(userlogin, function(data){
            if(data.hasOwnProperty('access')){
                $scope.errorpanel = false;
                store.set('jwt', data.access);
                store.set('cart', data.cart)
                var token = jwtHelper.decodeToken(store.get('jwt'));
                store.set('loginname', token.firstname +' ' + token.lastname);
                store.set('logintype', 'normal');
                $scope.loginname =  token.firstname +' ' + token.lastname;
                $window.location = '/';
            }
            else{
                $scope.errormsg = data.error;
                $scope.errorpanel = true;
                $scope.saveloading = false;
            }
        });
    }

     $scope.facebooklogin = function() {
        $scope.saveloading = true;
        Facebook.login(function(loginresponse) {
            console.log(loginresponse);
            facebookdbcheck();
        },{ scope: 'email,public_profile,user_birthday,user_hometown' });
    };


    var facebookdbcheck = function(){
        Facebook.getLoginStatus(function(getLoginStatusresponse) {
            if(getLoginStatusresponse.status === 'connected') {

                Facebook.api('/me',{fields: 'last_name,email,first_name,birthday,picture,hometown'}, function(apiresponse) {
                    var fbid = {
                        'fbid':apiresponse.id,
                        'last_name':apiresponse.last_name,
                        'first_name':apiresponse.first_name,
                        'hometown':apiresponse.hometown
                    };
                    console.log(fbid);
                    UserregistrationFactory.checkfacebook(fbid, function(data){
                        console.log(data);
                        if(data.hasOwnProperty('access')){

                            store.set('jwt', data.access);
                            var token = jwtHelper.decodeToken(store.get('jwt'));
                            store.set('loginname', token.firstname +' ' + token.lastname);
                            store.set('logintype', 'facebook');
                            $scope.loginname =  token.firstname +' ' + token.lastname;
                            $window.location = '/';
                            $scope.saveloading = false;
                        }
                        else{

                        }

                    });

                });

            }
            else {

                $scope.saveloading = false;

            }
        });
    }



    // Registration
    $scope.checkEmail = function(val){
        $scope.emailexist = 0;
        $scope.existinguser = '';
        var re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if(re.test(val)){
            Reservation.checkmemberemail(val, function(data){
                console.log(data[0]);
                if(data.length == 0){
                    $scope.emailexist = 2;
                    console.log("Clear not used");
                    $scope.user = {email: val, reservationlist: [], paymenttype: '', country: 'USA'};
                }else{
                    if(data[0].status=='INCOMPLETE'){
                        $scope.emailexist = 3;
                        console.log('Incomplete Profile');
                    }else{
                        $scope.emailexist = 1;
                        console.log("Email is used");
                        $scope.existinguser = data;
                    }
                }
            });
        }
    }

    var set = function(){
        $scope.register = { country: 'USA' };
    }

    set();

    $scope.registerUser = function(member, form){
        $scope.saveloading = true;
        member['formfrom'] = 'registration';
        $scope.saving = true;
        Reservation.registration(member, function(data){
            if(data.hasOwnProperty('success')){
                form.$setPristine();
                $scope.login=true;
                set();
                // Notification.success({message: data.success, positionY: 'bottom', positionX: 'right'});
                $scope.saving = false;
                $scope.regsuccess = true;
                $scope.regform = false;
                $scope.emailexist = false;
                $scope.user = {};
                $scope.email = member.email;
                $scope.saveloading = false;
            }else if(data.hasOwnProperty('error')){
                Notification.error({message: data.error, positionY: 'bottom', positionX: 'right'});
                $scope.saving = false;
                $scope.saveloading = false;
            }
        });
    }


})
