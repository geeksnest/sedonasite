
/* Controllers */

app.controller('ReservationCTRL', function($scope, $state, $http, appConfig, store, Notification, Reservation, $timeout, uiCalendarConfig, $modal, jwtHelper, $rootScope, $cookieStore){
    $scope.paymenttype = '';
    $scope.wizardStep = 1;
    $scope.saving = false;
    $scope.sum = 0;
    $scope.invoiceno = '';
    $scope.member = {'reservationlist' : []};
    $scope.loginname = '';
    $scope.login=true;
    $scope.useyouraddress = '';
    $scope.useyouraddress1 = '';
    $scope.buttonclicked = '';

    $scope.sendingcompletionlink = false;
    $scope.sendingcompletionlinksuccess = false;

    $scope.profilecompletionsuccess = false;

    $scope.forgotpassword = false;
    $scope.resendconfirmation = false;

    $scope.paymentprocessing = false;
    $scope.paymentsuccess =false;
    $scope.requestingreset =false;
    $scope.requestingresend =false;
    $scope.successchange = false;
    $scope.userccreg ={
        billingfname: '',
        billinglname: '',
        al1: '',
        al2: '',
        city: '',
        state: '',
        zip: ''
    }

    $scope.userecheck ={
        billingfname: '',
        billinglname: '',
        al1: '',
        al2: '',
        city: '',
        state: '',
        zip: ''
    }

    var checkjwt = function(){
        if (store.get('jwt')) {
            var token = jwtHelper.decodeToken(store.get('jwt'));
            $scope.loginname =  store.get('loginname');
            //$scope.wizardStep = 2;
            //$timeout(function() {
            //    angular.element('.btn-next').trigger('click');
            //}, 100);
        }
    }

    var cache = function(){
        $cookieStore.put('reservations', $scope.member.reservationlist, {'expires': moment().add(1, 'hours')});
    }

    var removecache = function(){
        $cookieStore.remove('reservations');
    }


    checkjwt();

    $rootScope.$on('emitloginname', function(event, data) {
        store.set('loginname', data)
        $scope.loginname =  data;
    });

    $scope.requestpassword = function(email){
        console.log('Requesting for new password.');
        $scope.requestingreset = true;
        Reservation.requestpassword(email, function(data){
            if(data.hasOwnProperty('success')){
                Notification.success({message: data.success, positionY: 'bottom', positionX: 'right'});
                $scope.forgotpassword = false;
                $scope.requestingreset = false;
            }else{
                Notification.error({message: data.error, positionY: 'bottom', positionX: 'right'});
                $scope.requestingreset = false;
            }
            console.log(data);
        });
    }

    $scope.updatepassword = function(pass){
        $scope.requestingreset =true;
        Reservation.updatepassword(pass, function(data){
            if(data.hasOwnProperty('success')){
                $scope.requestingreset = false;
                $scope.successchange = true;
                $timeout(function() {
                    window.location = "/reservation";
                }, 5000);
            }else{
                Notification.error({message: data.error, positionY: 'bottom', positionX: 'right'});
                $scope.requestingreset = false;
            }
        });
    }

    $scope.resendemail = function(info){
        $scope.requestingresend =true;
        Reservation.resendconfirmation(info, function(data){
            if(data.hasOwnProperty('success')){
                Notification.success({message: data.success, positionY: 'bottom', positionX: 'right'});
                $scope.forgotpassword = false;
                $scope.requestingresend = false;
            }else{
                Notification.error({message: data.error, positionY: 'bottom', positionX: 'right'});
                $scope.requestingresend = false;
            }
        });
    }

    $scope.logoutmember = function(){
        store.remove('jwt');
        store.remove('loginname');
        removecache();
        window.location = "/reservation";
    }

    var set = function(){
        $scope.register = { country: 'USA' };
    }

    set();

    $scope.facebooklogin = function() {
        // From now on you can use the Facebook service just as Facebook api says
        //Facebook.login(function(response) {
        //    // Do something with response.
        //    Facebook.api('/me?fields=id,first_name,last_name,email,picture', function(res) {
        //        $scope.user = res;
        //        console.log(res);
        //    });
        //});
    };

    $scope.loginbutton = function(user) {
        Reservation.login(user, function(data){
            console.log(data);
            if(data.hasOwnProperty('token')){
                store.set('jwt', data.token);
                //$scope.wizardStep = 2;
                var token = jwtHelper.decodeToken(store.get('jwt'));
                store.set('loginname', token.firstname +' ' + token.lastname);
                $scope.loginname =  token.firstname +' ' + token.lastname;
                $timeout(function() {
                    angular.element('.btn-next').trigger('click');
                }, 100);
            }else if(data.hasOwnProperty('pending')){
                $scope.loginerror = data.pending;
                Notification.warning({message: data.pending, positionY: 'bottom', positionX: 'right'});
            }else{
                $scope.loginerror = data.error;
                Notification.error({message: data.error, positionY: 'bottom', positionX: 'right'});
            }
        });
    };

    $scope.checkEmail = function(val){
        $scope.emailexist = 0;
        $scope.existinguser = '';
        var re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if(re.test(val)){
            Reservation.checkmemberemail(val, function(data){
                console.log(data[0]);
                if(data.length == 0){
                    $scope.emailexist = 2;
                    console.log("Clear not used");
                    $scope.user = {email: val, reservationlist: [], paymenttype: '', country: 'USA'};
                }else{
                    if(data[0].status=='INCOMPLETE'){
                        $scope.emailexist = 3;
                        console.log('Incomplete Profile');
                    }else{
                        $scope.emailexist = 1;
                        console.log("Email is used");
                        $scope.existinguser = data;
                    }
                }
            });
        }
    }

    $scope.sendUserCompletionLink = function(email){
        $scope.sendingcompletionlink = true;
        Reservation.sendusercompletionlink(email, function(data){
            console.log(data);
            if(data.hasOwnProperty('success')){
                $scope.sendingcompletionlinksuccess = true;
                $scope.sendingcompletionlink = false;
            }else{
                $scope.sendingcompletionlink = false;
            }

        });
    }

    $scope.registerUser = function(member, form){
        $scope.saving = true;
        Reservation.registration(member, function(data){
            if(data.hasOwnProperty('success')){
                form.$setPristine();
                $scope.login=true;
                set();
                Notification.success({message: data.success, positionY: 'bottom', positionX: 'right'});
                $scope.saving = false;
            }else if(data.hasOwnProperty('error')){
                Notification.error({message: data.error, positionY: 'bottom', positionX: 'right'});
                $scope.saving = false;
            }
        });
    }

    $scope.getcurrentaddress = function(data, type){
        if(data){
            var info = jwtHelper.decodeToken(store.get('jwt'));

            if(type=='card'){
                $scope.userccreg.billingfname = info.firstname;
                $scope.userccreg.billinglname = info.lastname;
                $scope.userccreg.al1 = info.address1;
                $scope.userccreg.al2 = info.address2;
                $scope.userccreg.city = info.city;
                $scope.userccreg.state = info.state;
                $scope.userccreg.zip = info.zipcode;
                $scope.userccreg.country = info.country;
            }else if(type=='check'){
                $scope.userecheck.billingfname = info.firstname;
                $scope.userecheck.billinglname = info.lastname;
                $scope.userecheck.al1 = info.address1;
                $scope.userecheck.al2 = info.address2;
                $scope.userecheck.city = info.city;
                $scope.userecheck.state = info.state;
                $scope.userecheck.zip = info.zipcode;
                $scope.userecheck.country = info.country;
            }

        }else{
            if(type=='card'){
                $scope.userccreg.billingfname = '';
                $scope.userccreg.billinglname = '';
                $scope.userccreg.al1 = '';
                $scope.userccreg.al2 = '';
                $scope.userccreg.city = '';
                $scope.userccreg.state = '';
                $scope.userccreg.zip = '';
                $scope.userccreg.country = '';
            }else if(type=='check'){
                $scope.userecheck.billingfname = '';
                $scope.userecheck.billinglname = '';
                $scope.userecheck.al1 = '';
                $scope.userecheck.al2 = '';
                $scope.userecheck.city = '';
                $scope.userecheck.state = '';
                $scope.userecheck.zip = '';
                $scope.userecheck.country = '';
            }
        }
    }



    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();


    /* alert on eventClick */
    $scope.alertOnEventClick = function( event, jsEvent, view ){

    };
    /* alert on Drop */
    $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){
        $scope.alertMessage = ('Event Droped to make dayDelta ' + delta);
    };
    /* alert on Resize */
    $scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view){
        $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
    };

    $scope.alertOnMouseOver = function( event, jsEvent, view ){

    }

    /* event source that contains custom events on the scope */
    var getEventsPerMonth = function(callback){
        var m = uiCalendarConfig.calendars['calendar1'].fullCalendar('getDate');
        var month = m.format('M');
        var year = m.format('YYYY');
        var events = [];
        Reservation.getmonthevents(month,year, function(data){
            console.log("ORIGINAL");
            // console.log(data);
            for(var key in data){
                if(data[key]['status'] != 'VOID'){
                    var stime = moment(data[key]['startdatetime']).format('h:mm a');
                    var etime = moment(data[key]['enddatetime']).format('h:mm a');
                    var n = {
                        title: stime + " - " + etime,
                        start: moment(data[key]['startdatetime']),
                        end: moment(data[key]['enddatetime']),
                        txtdate: moment(data[key]['scheduledate']).format('dddd, MMMM Do YYYY'),
                        txtstime: stime,
                        txtetime: etime,
                        txtname: data[key]['firstname'] + " " + data[key]['lastname'],
                        hourday: data[key]['hourday'],
                        status: data[key]['status'],
                        invoiceno: data[key]['invoiceno'],
                        schedid: data[key]['schedid'],
                        info: data[key]['description'],
                        unattended: false
                    }
                    events.push(n)
                }
            }
            console.log("NEW");
            console.log(events);
            callback(events);
        });
    }

    //Loading Schedules
    $timeout(function() {

        /* config object */
        $scope.uiConfig = {
            calendar:{
                height: 450,
                editable: false,
                header:{
                    left: 'prev',
                    center: 'title',
                    right: 'next'
                },
                eventClick: $scope.alertOnEventClick,
                eventDrop: $scope.alertOnDrop,
                eventResize: $scope.alertOnResize,
                eventMouseover: $scope.alertOnMouseOver,
                timeFormat: ' ',
                dayClick: $scope.dayClick,
                events: function(start, end, timezone, callback) {
                    getEventsPerMonth(function(data){
                        callback(data);
                    });
                },
                viewRender: function (view, element) {
                    console.log('HITTING CHANGE OF MONTH AND OTHER BUTTONS');
                    $timeout(function() {
                        var m = uiCalendarConfig.calendars['calendar1'].fullCalendar('getDate');
                        var datecal = m.format('M-YYYY');

                        if( $cookieStore.get('reservations')){
                            $scope.member.reservationlist  = $cookieStore.get('reservations');
                        }

                        if($scope.member.reservationlist.length > 0){
                            console.log('Reservation List has contents');
                            for(var key in $scope.member.reservationlist){
                                var data = $scope.member.reservationlist;
                                console.log(moment(data[key].startdatetime).format('M-YYYY') == datecal);
                                if(moment(data[key].startdatetime).format('M-YYYY') == datecal){
                                    var stime = moment(data[key].startdatetime).format('h:mm a');
                                    var etime = moment(data[key].enddatetime).format('h:mm a')
                                    var n = {
                                        title: stime + " - " + etime,
                                        start: moment(data[key].startdatetime),
                                        end: moment(data[key].enddatetime),
                                        txtdate: moment(data[key].scheduledate).format('dddd, MMMM Do YYYY'),
                                        txtstime: stime,
                                        txtetime: etime,
                                        txtname: '',
                                        hourday: '',
                                        status: '',
                                        invoiceno: '',
                                        schedid: '',
                                        info: '',
                                        unattended: false,
                                        backgroundColor: '#fff',
                                        textColor: '#000'
                                    }

                                    $scope.events.push(n);

                                }
                            }
                        }

                    }, 100);
                }
            }
        };
    }, 100);

    $scope.nextClick = function(){
        $timeout(function() {
            angular.element('.btn-next').trigger('click');
        }, 100);
    }

    $scope.prevClick = function(){
        $timeout(function() {
            angular.element('.btn-previous').trigger('click');
        }, 100);
    }

    $scope.$watch('wizardStep', function(step){
        if(step==2){
            if (store.get('jwt')) {
                if($scope.buttonclicked == 'next'){
                    $scope.nextClick();
                }else if($scope.buttonclicked == 'prev'){
                    $scope.prevClick();
                }
            }
            cache();
        }
    })

    /*  */
    $scope.dayClick = function(date, jsEvent, view) {
        console.log("DAY CLICKED");

        var dateclicked = date.format('YYYY-MM-DD');

        var events = uiCalendarConfig.calendars['calendar1'].fullCalendar('clientEvents');

        //PASS THE DATE OF THE DAY CLICKED AND FILTER THE EVENTS MATCHING THE DAY CLICKED THEN VALIDATE IF IT ALREADY EXISTS
        if(date.unix() >= moment().unix()){

            var modalInstance = $modal.open({
                templateUrl: 'addscheduleModal.html',
                controller: function($scope, $modalInstance, $state, dateclicked) {
                    $scope.datefor = moment(dateclicked).format('dddd, MMMM Do YYYY');
                    $scope.sched = { 'mytime':new Date() }

                    /* Preloading Time*/
                    $scope.sched.mytime.setHours('8');
                    $scope.sched.mytime.setMinutes('00');

                    $scope.hstep = 1;
                    $scope.mstep = 30;
                    $scope.conflict = false;
                    $scope.ismeridian = true;
                    $scope.toggleMin = function() {
                        $scope.minDate = $scope.minDate ? null : new Date();
                    };
                    $scope.toggleMin();
                    $scope.dateOptions = {
                        formatYear: 'yy',
                        startingDay: 1,
                        class: 'datepicker'
                    };
                    var computetime = function(){
                        var computedtime = 0;
                        if($scope.currenttimevalue=='h'){
                            computedtime = moment($scope.sched.mytime).add($scope.currentprice,'hour');
                        }else if($scope.currenttimevalue=='m'){
                            computedtime = moment($scope.sched.mytime).add($scope.currentprice,'minutes');
                        }

                        var starttime = moment($scope.sched.mytime).format('h:mm A');
                        var endtime = moment(computedtime).format('h:mm A');
                        $scope.sched.endtime = computedtime;
                        $scope.computedtime = starttime + ' to ' + endtime;
                    }
                    $scope.changedtime = function(mytime){
                        console.log("CHANGING TIME===================");
                        computetime();
                    }
                    $scope.priceselected = function(price){
                        var newjson = JSON.parse(price);
                        $scope.currentprice = newjson.hourday.substring(0,newjson.hourday.length-1);
                        $scope.currenttimevalue = newjson.hourday.substring(newjson.hourday.length-1, newjson.hourday.length);
                        computetime();
                    }

                    /* Start preloading data */
                    Reservation.getpagescat(function(data){
                        $scope.pagelist = data;
                        $scope.loading=false;
                        console.log('PAGE LIST!=================');
                        console.log($scope.pagelist);
                    });
                    $scope.selectServiceCat = function(pg){
                        $scope.servicepricelist = pg.prices;
                        $scope.sched.servicechoice = '';
                    }
                    $scope.cancel = function(){
                        $modalInstance.dismiss('cancel');
                    }
                    $scope.submit = function(sched){
                        console.log(sched);
                        $scope.conflict = false;
                        var servicechoice = JSON.parse(sched.servicechoice);
                        var cstime = moment(sched.mytime).format( dateclicked + ' HH:mm:00');
                        var cetime = moment(sched.endtime).format(dateclicked + ' HH:mm:00');
                        for(var key in events){
                            if(events[key].start.format('YYYY-MM-DD') == dateclicked){
                                console.log('Day Current!');
                                if(moment(cstime).unix() < events[key].end.unix()  && moment(cetime).unix() > events[key].start.unix()){
                                    $scope.conflict = true;
                                    Notification.error({message: 'The schedule you are trying to add is already reserved to another client.', positionY: 'bottom', positionX: 'right'});
                                    console.log("IS BETWEEN BREAK!!!!!!");
                                    break;
                                }
                            }
                        }
                        if(!$scope.conflict) {
                            Reservation.validate(cstime, cetime, function (data) {
                                console.log(data);
                                if (data.length > 0 || $scope.conflict) {
                                    Notification.error({
                                        message: 'The schedule you are trying to add is already reserved to another client.',
                                        positionY: 'bottom',
                                        positionX: 'right'
                                    });
                                    $scope.conflict = true;
                                } else {
                                    if (!$scope.conflict) {
                                        Reservation.validate(cstime, cetime, function (data) {
                                            if (data.length > 0 || $scope.conflict) {
                                                Notification.error({
                                                    message: 'The schedule you are trying to add is already reserved to another client.',
                                                    positionY: 'bottom',
                                                    positionX: 'right'
                                                });
                                            } else {
                                                var newsched = {
                                                    reservationdate: dateclicked,
                                                    startdatetime: cstime,
                                                    enddatetime: cetime,
                                                    servicechoice: servicechoice,
                                                    service: sched.service,
                                                    text: dateclicked + ' ' + $scope.computedtime
                                                };
                                                $scope.sched.service = '';
                                                $scope.sched.servicechoice = '';
                                                $scope.servicepricelist = '';
                                                $scope.conflict = false;
                                                $modalInstance.close(newsched);
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    };
                },
                resolve: {
                    dateclicked: function(){
                        return dateclicked;
                    }
                }
            }).result.then(function(data){
                console.log("=====Hitting Result");
                $scope.member.reservationlist.push(data);
                    var stime = moment(data.startdatetime).format('h:mm a');
                    var etime = moment(data.enddatetime).format('h:mm a')
                    var n = {
                        title: stime + " - " + etime,
                        start: moment(data.startdatetime),
                        end: moment(data.enddatetime),
                        txtdate: moment(data.scheduledate).format('dddd, MMMM Do YYYY'),
                        txtstime: stime,
                        txtetime: etime,
                        txtname: '',
                        hourday: '',
                        status: '',
                        invoiceno: '',
                        schedid: '',
                        info: '',
                        unattended: false,
                        backgroundColor: '#fff',
                        textColor: '#000'
                    }
                    $scope.events.push(n);
                    Notification.success({message: '<icon class="fa fa-check"></icon> The schedule has been added to your reservation list.', positionY: 'bottom', positionX: 'right'});

                console.log(data);
            });

        }else{
            console.log('DAY DISABLED');
            Notification.error({message: 'These dates have already passed.', positionY: 'bottom', positionX: 'right'});
        }

    }

    $scope.deleteService = function(index, startdatetime){
        $scope.member.reservationlist.splice(index, 1);
        console.log($scope.member.reservationlist);
        console.log(index);
        for(var key in $scope.events){
            console.log();
            if(moment($scope.events[key].start).format('YYYY-MM-DD HH:mm:00') == startdatetime){
                $scope.events.splice(key, 1);
            }
        }
    }

    /* remove event */
    $scope.remove = function(index) {
        $scope.events.splice(index,1);
    };

    /* Change View */
    $scope.changeView = function(view, calendar) {
        uiCalendarConfig.calendars['calendar1'].fullCalendar('changeView', view);
    };

    $scope.getTotal = function(){
        var total = 0;
        for(var i = 0; i < $scope.member.reservationlist.length; i++){
            var service = $scope.member.reservationlist[i];
            total += parseFloat(service.servicechoice.price);
        }
        return total;
    }

    $scope.today = function(calendar) {
        uiCalendarConfig.calendars['calendar1'].fullCalendar('today');
    };

    $scope.renderCalender = function(calendar) {

        if(calendar){
            uiCalendarConfig.calendars['calendar1'].fullCalendar('render');
        }
    };
    $scope.events = [];
    /* event sources array*/
    $scope.eventSources = [$scope.events];

    /* PAYMENT GATEWAY */

    $scope.months =  Reservation.month();
    $scope.years =  Reservation.year();
    $scope.countries = Reservation.contries().list();

    $scope.process = function(billing, paymenttype){
        $scope.member.billinginfo = billing;
        $scope.paymentprocessing = true;
        if (store.get('jwt')) {
            var user = jwtHelper.decodeToken(store.get('jwt'));

            $scope.member.id = user.id;
            $scope.member.paymenttype = paymenttype;
            $scope.member.amount = $scope.getTotal();
            console.log($scope.member);
            Reservation.payment($scope.member, function(data){
                if(data.hasOwnProperty('200')){
                    $scope.invoiceno = data.invoiceno;
                    removecache();
                    if(paymenttype=='paypal'){
                        $('#invoiceno').val(data.invoiceno);
                        $('.paypalform').submit();
                        console.log($('#invoiceno').val());
                    }else if(paymenttype=='card'){
                        Notification.success({message: 'Credit Card Success', positionY: 'bottom', positionX: 'right'});
                        $scope.paymentsuccess = true;
                    }else if(paymenttype=='check'){
                        Notification.success({message: 'E-Check Success', positionY: 'bottom', positionX: 'right'});
                        $scope.paymentsuccess = true;
                    }
                }else{
                    Notification.error({message: 'Something went wrong in our servers, please try again later.', positionY: 'bottom', positionX: 'right'});
                    $scope.paymentprocessing = false;
                }
            });
        }else{
            Notification.error({message: 'You are not login.', positionY: 'bottom', positionX: 'right'});
        }
    }

    /* Profile Completion */
    $scope.loadprofilecompletion = function(){

        $timeout(function() {
            var cindex = $("#regcountry")[0].selectedIndex;
            print_state('regstate', cindex);
            console.log(cindex);
            $("#regstate").val($scope.profile.state);
        }, 500);
    }
    $scope.profileCompletion = function(member, form){
        $scope.saving = true;
        Reservation.completionmember(member, function(data){
            $scope.profile = {
                'email': '',
                'username': '',
                'lastname': '',
                'firstname': '',
                'address1': '',
                'address2': '',
                'zipcode': '',
                'country': '',
                'state': '',
                'phoneno': ''
            };
            if(data.hasOwnProperty('success')){
                $scope.saving = false;
                $scope.profilecompletionsuccess = true;
                $timeout(function() {
                    window.location = "/reservation";
                }, 5000);
            }else if(data.hasOwnProperty('error')){
                Notification.error({message: data.error, positionY: 'bottom', positionX: 'right'});
                $scope.saving = false;
            }
        });
    }

    /* User Profile */
    $scope.userprofile = function(){

        var modalInstance = $modal.open({
            templateUrl: 'userprofileModal.html',
            controller: function($scope, $modalInstance, $state) {
                $scope.profile = {
                    'email': '',
                    'username': '',
                    'lastname': '',
                    'firstname': '',
                    'address1': '',
                    'address2': '',
                    'zipcode': '',
                    'country': '',
                    'state': '',
                    'phoneno': ''
                };

                $scope.saving = false;
                $scope.members  = {reservationlist:[]};
                var token = jwtHelper.decodeToken(store.get('jwt'));

                $scope.membername = token.firstname + " " + token.lastname;

                console.log(token);
                Reservation.checkmemberemail(token.email, function(data){
                    $scope.profile = {
                        'email': data[0].email,
                        'username': data[0].username,
                        'lastname': data[0].lastname,
                        'firstname': data[0].firstname,
                        'address1': data[0].address1,
                        'address2': data[0].address2,
                        'zipcode': data[0].zipcode,
                        'country': data[0].country,
                        'state': data[0].state,
                        'phoneno': data[0].phoneno
                    };

                    $timeout(function() {
                        var cindex = $("#profilecountry")[0].selectedIndex;
                        print_state('profilestate', cindex);
                        console.log(cindex);
                        $("#profilestate").val(data[0].state);
                    }, 500);

                });

                var getStatus = function(status, startdatetime, enddatetime){
                    var data = {
                        classname : '',
                        unattended : false
                    };
                    if(status=='VOID'){
                        data['classname'] = ['bg-danger bg'];
                    }else if (status=='DONE'){
                        data['classname'] = ['bg-success bg'];
                    }else if (moment(startdatetime).unix() <= moment().unix()  && moment(enddatetime).unix() <= moment().unix()){
                        //moment(data[key]['scheduledate']).isBefore(moment())
                        data['classname'] = ['bg-warning bg'];
                        data['unattended'] = true;
                    }else{
                        data['classname'] = ['b-l b-2x b-info'];
                    }
                    return data;
                }

                Reservation.getuserreservations(token.id, function(res){
                    var data = res[0];
                    var events = [];
                    for(var key in data){
                        var n = {
                            title: data[key]['title'],
                            start: moment(data[key]['startdatetime']),
                            end: moment(data[key]['enddatetime']),
                            txtdate: moment(data[key]['scheduledate']).format('dddd, MMMM Do YYYY'),
                            txtstime: moment(data[key]['startdatetime']).format('h:mm:ss a'),
                            txtetime: moment(data[key]['enddatetime']).format('h:mm:ss a'),
                            txtname: data[key]['firstname'] + " " + data[key]['lastname'],
                            hourday: data[key]['hourday'],
                            price: data[key]['price'],
                            status: data[key]['status'],
                            invoiceno: data[key]['invoiceno'],
                            schedid: data[key]['schedid'],
                            info: data[key]['description'],
                            unattended: false
                        }

                        var ndata = getStatus(data[key]['status'], data[key]['startdatetime'], data[key]['enddatetime']);
                        n['className'] = ndata.classname;
                        n['unattended'] = ndata.unattended;

                        if(!n['unattended'] && data[key]['status'] == 'RESERVED'){
                            n['backgroundColor'] = data[key]['colorlegend'];
                            n['textColor'] = '#fff';
                        }

                        events.push(n)
                    }

                    $scope.members['reservationlist'] = events;
                    console.log("EVENTS USERS");
                    console.log(events);
                });

                $scope.openinvoice = function(invoiceno){
                    window.open('/invoice/' + invoiceno ,'liveMatches','directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=720,height=800');
                }

                $scope.updateprofile = function(profile){
                    $scope.profile['id']=token.id;
                    $scope.saving = true;
                    Reservation.profileupdate(profile, function(data){
                        console.log(data);
                        if(data.hasOwnProperty('success')){
                            $scope.$emit('emitloginname', profile.firstname +' ' + profile.lastname);
                            $scope.membername = profile.firstname + " " + profile.lastname;
                            Notification.success({message: data.success, positionY: 'bottom', positionX: 'right'});
                        }else{
                            Notification.success({message: data.error, positionY: 'bottom', positionX: 'right'});
                        }
                        $scope.saving = false;
                    });
                };

                $scope.cancel = function(){
                    $modalInstance.dismiss('cancel');
                }

                $scope.updatepassword = function(password){
                    password['id']=token.id;
                    $scope.saving = true;
                    Reservation.updatememberpassword(password, function(data){
                        console.log(data);
                        if(data.hasOwnProperty('success')){
                            Notification.success({message: data.success, positionY: 'bottom', positionX: 'right'});
                        }else{
                            $scope.saving = false;
                            Notification.error({message: data.error, positionY: 'bottom', positionX: 'right'});
                        }
                    });
                };

            }
            //},
            //resolve: {
            //    dateclicked: function(){
            //        return dateclicked;
            //    }
            //}
        }).result.then(function(data){
            console.log('then is called');
            });

    }

    $scope.bookanother = function(){
        window.location = "/reservation";
    }

})
