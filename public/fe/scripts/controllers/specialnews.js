'use strict';

/* Controllers */

app.controller('SNewsCtrl', function($scope, $http, appConfig, News, $window, $sce, Lightbox){
    // Disable weekend selection
    var offset = 0;
    if(slimitcount != undefined){
            var page = slimitcount;
    }else{
            var page = 10;
    }
    console.log(page);
    console.log(limitcount);
    var list = [];
    $scope.loading = false;
    $scope.newslist = [];
    $scope.hideloadmore = false;
    $scope.showmorenews = function(){
        console.log('showmore');
        $scope.loading = true;

        News.list(offset, page, function(data){

            for(var key in data){
                var categorylist = [];
                data[key].category.map(function(val){
                    categorylist.push(val.categoryname);
                });
                data[key]['categorylist'] = categorylist.join(", ");
            }
            console.log(data);
            list = list.concat(data);
            offset = offset + page;
            $scope.loading = false;
            //console.log(text.match((src="https://www.youtube.com/embed/(.*?)")e));
            console.log(list);
            $scope.newslist = list;
            if(data.length < page){
                $scope.hideloadmore = true;
            }
        });
    }

    $scope.openlightbox = function(image){
        console.log(image);
        var img = [{'url': "https://sedonahealing.s3.amazonaws.com/uploads/newsimage/me.png"}];
        Lightbox.openModal(img, 0);
    }

})