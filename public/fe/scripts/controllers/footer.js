'use strict';

/* Controllers */

app.controller('footerCtrl', function($scope, Login, store,$sce, Special, Pagination, $window){
var pageslugs = $('#slugs').val()
$scope.trustAsHtml = $sce.trustAsHtml;
$scope.eventSources = [];
// //meta info
$scope.redirectNews = function(link){
        $window.location.href = '/blog/' + link;
    }

Special.loadfooter(function(data) {
    console.log(data)
    $scope.foot = data.datacol;
});

Special.loadtesti(function(data) {
$scope.testi = data.testi;
$scope.shortcode = data.shortcode;
$scope.children = data.children;
$scope.archive = data.archive;
});

Special.loadnews(function(data) {
$scope.news = data;
 for(var key in data){
                var categorylist = [];
                data[key].category.map(function(val){
                    categorylist.push(val.categoryname);
                });
                data[key]['categorylist'] = categorylist.join(", ");
            }

});



})
