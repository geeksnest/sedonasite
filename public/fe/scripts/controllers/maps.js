'use strict';

/* Controllers */

app.controller('MapsCreateCtrl', function($http,$log,$scope, Maps, $cookieStore, rfc4122, $compile, $rootScope, $timeout, $sce, $location, appConfig, Login, $q){

    $log.info('==== Maps Create Controller ====');

    $cookieStore.remove('markersInfo');
    $cookieStore.put('markersInfo', []);
    $scope.notiCheck = false;
    $scope.markers = [];
    $scope.markerImages = [];
    $scope.PinImages = [];
    $scope.currentPinImages = [];
    $scope.flashError = [];
    $scope.loader = false;
    $scope.loaderText = '';
    $scope.rightPanelInfo = Maps.getToggleMarkerInfo();
    $scope.showDelete = Maps.showDelete;
    $scope.mk = {
        'description': '',
        'videolink': ''
    }

    $scope.$watch(function() { return Maps.getToggleMarkerInfo();}, function(){
        var el;
        if(Maps.getToggleMarkerInfo()){
            el = '<a ng-click="controlClick()"><i class="fa fa-ban"></i> Cancel</a> ';
            console.log('Show Cancel');
        }else{
            el = '<a ng-click="clickInfo(map)"><i class="fa fa-plus-circle"></i> Info</a> ';
            console.log('Show Info');
        }
        $scope.buttons = el;
    });

    $scope.$watch(function() { return Maps.showDelete;}, function(){
        console.log('----------------------');
        console.log(Maps.showDelete );
        $scope.showDelete = Maps.showDelete;

        // var array = Maps.markers;
        // for(var x in array){
        //     if(array[x].idKey == Maps.currentPin.idKey){
        //         array.splice(x, 1);
        //         if(Maps.getToggleMarkerInfo()){
        //             $scope.controlClick();
        //         }
        //         //Display the index 0 info's
        //         Maps.markers=array;
        //     }
        // }
        // var mi = $cookieStore.get('markersInfo');
        // for(var x in mi){
        //     if(mi[x].idKey == Maps.currentPin.idKey){
        //         mi.splice(x, 1);
        //         $cookieStore.put('markersInfo', mi);
        //     }
        // }        
        // var pm = Maps.pinImages;
        // for(var x in pm){
        //     if(pm[x].idKey == Maps.currentPin.idKey){
        //         pm.splice(x, 1);
        //         Maps.pinImages = pm;
        //     }
        // }     

    });

    $scope.deletePin = function(){
        var array = Maps.markers;
        var currentPin = Maps.currentPin.idKey;
        for(var x in array){
            if(array[x].idKey == currentPin){
                array.splice(x, 1);
                        Maps.setCurrentPin(Maps.markers[0]);
                        Maps.showTrueMarker(currentPin);

                        var mkr = viewCookieMap(currentPin);

                            $scope.mk.description = mkr.description;
                            $scope.mk.videolink = mkr.videolink;
                            $scope.currentPinImages = viewPinPic(currentPin)
                            $scope.viddisp($scope.mk.videolink);
                            Maps.markers[0].options.icon = Maps.kingpin;
                //Display the index 0 info's
                Maps.markers=array;
                $scope.showDelete = Maps.showDelete = false;
            }
        }

        var mi = $cookieStore.get('markersInfo');
        for(var x in mi){
            if(mi[x].idKey == currentPin){
                mi.splice(x, 1);
                $cookieStore.put('markersInfo', mi);
            }
        }
        var pm = Maps.pinImages;
        for(var x in pm){
            if(pm[x].idKey == currentPin){
                pm.splice(x, 1);
                Maps.pinImages = pm;
            }
        }    
    }

    /* Saving Map Informations to Cookies1 */
    var saveCookieMap = function(data){
        $log.log('Saving User Information');
        var array = $cookieStore.get('markersInfo');
        for(var i in array){
            if(array[i].idKey == data.idKey){
                array[i]['description'] = data.description;
                array[i]['videolink'] = data.videolink;
                array[i]['images'] = [];
                $cookieStore.put('markersInfo', array);
                return true;
            }
        }
        array.push(data);
        $cookieStore.put('markersInfo', array);
    }

    var viewCookieMap = function(id){
        $log.log('Viewing User Information');
        var array = $cookieStore.get('markersInfo');
        for(var x in array){
            if(array[x].idKey == id){
                return array[x];
            }
        }
    }

    var savePinPic = function(pics, idkey){
        var array = Maps.pinImages;
        for(var y in array){
            if(array[y].idKey == idkey){
                $log.log(array[y]['images']);
                $log.log(typeof array[y]['images'] == 'undefined');
                    var arrayPic = array[y]['images'];
                    pics.map(function(pic){
                        arrayPic.push(pic);
                    });
                    array[y]['images'] = arrayPic;
                    $log.log('TEST==================================');
                    $log.log(Maps.pinImages);
                    Maps.pinImages = array;
                    $scope.currentPinImages = arrayPic;
                return true;
            }
        }
    }

    var viewPinPic = function(id){
        var array = Maps.pinImages;
        for(var x in array){
            if(array[x].idKey == id){
                return array[x].images;
            }
        }
    }

    // var saveMarkerPicInfo = function(pics, idkey){
    //     $log.log('Saving Marker Pitures1');
    //     $log.log(pics);
    //     var array = $cookieStore.get('markersInfo');
    //     for(var y in array){
    //         if(array[y].idKey == idkey){
    //             $log.log(array[y]['images']);
    //             $log.log(typeof array[y]['images'] == 'undefined');

    //                 var arrayPic = array[y]['images'];
    //                 pics.map(function(pic){
    //                     arrayPic.push(pic);
    //                 });
    //                 array[y]['images'] = arrayPic;
    //                 $log.log('==================================');
    //                 $log.log(arrayPic)
    //                 $scope.currentPinImages = arrayPic;
    //                 $cookieStore.put('markersInfo', array);
    //             return true;
    //         }
    //     }
    // }
    /* End Map Informations to Cookies*/

    // var showTrueMarker = function(id){
    //     for(var x in $scope.markers){
    //         if($scope.markers[x].idKey == id){
    //             $scope.markers[ x ].show=true;
    //             return x;
    //         }
    //     }
    // }

    //Login.redirectToMainifLogin();
    $scope.map = {
        center: {
            latitude: 41.504189,
            longitude: -97.778320
        },
        zoom: 4,
        events: {
            click: function(event,a,b){
                createMarker(b[0].latLng.lat(),b[0].latLng.lng())
                //console.log(b[0].latLng.lat());
                //console.log(b[0].latLng.lng());
            }
        }

    };
    var createMarker = function(lat,long){

        var pin = '';
        if($scope.markers.length >0){
            pin =  Maps.pin;
        }else{
            pin = Maps.kingpin;
        }


        $scope.notiCheck = false;
        $scope.$apply(function(){
            var idmark = rfc4122.v4();
            var closeWindows = function(){
                for (var m in $scope.markers){
                    $scope.markers[m].show = false;
                }
            }
            $scope.markers.push({
                id: idmark,
                idKey: idmark,
                coords: {
                    latitude: lat,
                    longitude: long
                },
                options: { draggable: true, animation: google.maps.Animation.DROP, icon: pin },
                events: {
                    dragend: function (marker, eventName, args) {
                        //$log.log('marker dragend');
                        var lat = marker.getPosition().lat();
                        var lon = marker.getPosition().lng();
                        Maps.setCurrentPin(args);
                        $scope.notiCheck = false;
                        //$log.log(lat);
                        //$log.log(lon);
                    },
                    click: function(marker,eventName, args){
                        $scope.notiCheck = false;
                        closeWindows();
                        console.log('Click Marker');
                        Maps.setCurrentPin(args);
                        Maps.showTrueMarker(args.idKey);
                        //$scope.markers[ args.idKey ].show=true;
                        var array = viewCookieMap(args.idKey);
                        if(array){
                            $scope.mk.description = array.description;
                            $scope.mk.videolink = array.videolink;
                            $scope.currentPinImages = viewPinPic(args.idKey)
                            $scope.viddisp($scope.mk.videolink);
                        }else{
                            $scope.mk = {
                                'description': '',
                                'videolink': ''
                            }
                            $scope.videodisplay = '';
                        }
                        $scope.rightPanelInfo = Maps.getToggleMarkerInfo();
                    }
                },
                show: true,
                markerInfo: {},
                cancel: true
            });
            Maps.markers = $scope.markers;
            $scope.mk = {
                'description': '',
                'videolink': ''
            }
            closeWindows();
            var mapid = Maps.showTrueMarker(idmark);
            Maps.currentPin = $scope.markers[ mapid ];
            $scope.rightPanelInfo = Maps.getToggleMarkerInfo();

            var mi = $cookieStore.get('markersInfo');
            mi.push({'idKey':idmark, 'images': [] });
            $cookieStore.put('markersInfo', mi);
            $scope.videodisplay = '';
            Maps.pinImages.push({'idKey':idmark, 'images': [] });
            $scope.currentPinImages = [];
        });
    };
    $scope.saveMarkerInfo = function(data){
        console.log('Saving Marker Info');
        var pin = Maps.currentPin;
        data['idKey'] = pin.idKey;
        saveCookieMap(data);
        $scope.notiCheck = true;
    }

    $scope.prepare = function (files) {
        if (files && files.length) {
            //var markerpics = Maps.saveMarkerPics(files);
            var newData = [];

            //markerpics.then(function(values){
                files.map(function(file){
                    if (file.size >= 2000000){
                        console.log('File is too big!');
                        $scope.flashError.push({'name':file.name});
                    }else{
                        newData.push(file);
                    }
                });
                savePinPic(newData, Maps.currentPin.idKey)
                       $timeout(function(){
                          $scope.flashError = [];
                       }, 10000);
            //});
        }
    };

    $scope.removepinpic = function(index){
        console.log(index);
        var array = Maps.pinImages;
        for(var x in array){
            if(array[x].idKey == Maps.currentPin.idKey){
                array[x].images.splice(index, 1);
                $scope.currentPinImages = array[x].images;
                console.log(array);
                return  Maps.pinImages = array;
                
            }
        }
    }

    $scope.removeerror = function(index){
        $scope.flashError.splice(index, 1);
    }

    var parseQueryString = function( queryString ) {
        var params = {}, queries, temp, i, l;
     
        // Split into key/value pairs
        queries = queryString.split("&");
     
        // Convert the array of strings into an object
        for ( i = 0, l = queries.length; i < l; i++ ) {
            temp = queries[i].split('=');
            params[temp[0]] = temp[1];
        }
     
        return params;
    };

    $scope.viddisp = function(video){
        console.log(video)
        if(video !== undefined){
            var queryString = video.substring( video.indexOf('?') + 1 );
            var param = parseQueryString(queryString);
            if(param.hasOwnProperty('v')){
                $scope.videodisplay = $sce.trustAsHtml('<iframe src="https://www.youtube.com/embed/'+param.v+'" frameborder="0" allowfullscreen></iframe>');
            }else{
                $scope.videodisplay = '';
            }
        }else{
            $scope.videodisplay = '';
        }
    }

    $scope.hideDeleteButton = function(){
        $scope.showDelete = Maps.showDelete = false;
    }

    $scope.saveMap = function(gmap){
        //Display loader for saving
        $scope.loader = true;
        $scope.loaderText = 'Saving your map and markers information...';

        gmap['userid'] = Login.getCurrentUser()['id'];
        var mkinfo = $cookieStore.get('markersInfo');
        var mapmarker = Maps.markers;
        for(var m in mkinfo){
            mapmarker.map(function(mk){
                if (mkinfo[m]['idKey'] === mk['idKey']) {
                mkinfo[m]['latitude'] = mk['coords']['latitude'];
                mkinfo[m]['longitude'] = mk['coords']['longitude'];
              }
            });
        }
        gmap['markersInfo'] = mkinfo;
        console.log(gmap);
        $http({
            url: appConfig.ResourceUrl + "/map/create/info",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(gmap)
        }).success(function (data, status, headers, config) {
            $scope.loaderText = 'Uploading your marker pictures...';
            if(data.hasOwnProperty('200')) {
                if (Maps.pinImages && Maps.pinImages.length) {
                    var prom = [];

                    Maps.pinImages.map(function(image){
                        var deffered  = $q.defer();
                        if(image['images'] && image['images'].length) {
                            var markerpics = Maps.saveMarkerPics(image['images'], image['idKey']);
                            markerpics.then(function(ret){
                                deffered.resolve(data);
                            });
                            prom.push(deffered);
                        }
                    });

                    $q.all(prom).then(function(ret){
                        $scope.loaderText = 'Your map has been succesffuly generated.';
                    });
                }
            }else{

            }
            console.log(data);
        })
    }

})
app.controller('InfoWindowCtrl', function($scope, Maps, $anchorScroll, $location, User, store, $window, Login, $cookieStore){

    console.log($scope.pusanggala);

    $scope.$watch(function() { return Maps.getToggleMarkerInfo() }, function(){
        var el;
        if(Maps.getToggleMarkerInfo()){

            el = '<a ng-click="controlClick()"><i class="fa fa-ban"></i> Cancel</a> ';
            console.log('Show Cancel');
        }else{
            el = '<a ng-click="clickInfo(map)"><i class="fa fa-plus-circle"></i> Info</a> ';
            console.log('Show Info');
        }
        
        $scope.buttons = el;
    });
    $scope.clickInfo = function(map){
            $('.gmap').animate({
                width:'60%'
            }, {
                complete: function(){
                    $('.gmap-marker-info').show();
                    google.maps.event.trigger($scope.map, 'resize')
                    var pin = Maps.currentPin;
                    $scope.map.setCenter(new google.maps.LatLng(pin.coords.latitude, pin.coords.longitude));
                    $scope.rightPanelInfo = Maps.setToggleMarkerInfo(true);
                }
            })
        $scope.mk = {
            'description': '',
            'videolink': ''
        }
    }

    $scope.controlClick = function () {
        console.log('CONTROL HAS BEEN CLICKED');
        if(Maps.currentPin === null){
            alert('You do not have any pins in your map yet, please input one.');
        }else{
            if(Maps.getToggleMarkerInfo()){
                $scope.rightPanelInfo = Maps.setToggleMarkerInfo(false);
                $('.gmap').animate({
                    width:'100%'
                }, {
                    complete: function(){
                        $('.gmap-marker-info').hide();
                        google.maps.event.trigger($scope.map, 'resize')
                    }
                })
            }else{
                $('.gmap').animate({
                    width:'60%'
                }, {
                    complete: function(){
                        $('.gmap-marker-info').show();
                        google.maps.event.trigger($scope.map, 'resize')
                        var pin = Maps.currentPin;
                        if(pin!=null) {
                            $scope.map.setCenter(new google.maps.LatLng(pin.coords.latitude, pin.coords.longitude));
                        }

                    }
                })
                $scope.rightPanelInfo = Maps.setToggleMarkerInfo(true);
            }
        }
    };


    $scope.deleteMarker = function(){
        console.log(Maps.markers)
        Maps.showDelete = true;       
    }

})
