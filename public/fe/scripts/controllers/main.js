'use strict';

/* Controllers */

app.controller('MainCtrl', function($scope, Login, store, $window, $http,appConfig,Facebook){

    if(store.get('logintype') == 'normal'){
        Login.getUserFromToken(function(data){

            if(data.hasOwnProperty('id')){
                $scope.loginmenu = data.username;

                $scope.loginname = data.firstname + ' ' + data.lastname;
                $scope.loginpanel = true;
            }else{
                $scope.loginname = '';
                $scope.loginpanel = true;
            }
        })
        console.log('normal');
    }
    else if(store.get('logintype') == 'facebook'){
        Facebook.getLoginStatus(function(response) {
            if(response.status === 'connected') {
                console.log('connected');
                Login.getUserFromToken(function(data){

                    if(data.hasOwnProperty('id')){
                        $scope.loginmenu = data.username;

                        $scope.loginname = data.firstname + ' ' + data.lastname;
                        $scope.loginpanel = true;
                    }else{
                        $scope.loginname = '';
                        $scope.loginpanel = true;
                    }
                })
            } else {
                store.remove('jwt');
                store.remove('logintype');
                console.log('facebooklogouttttttttt');

                Login.getUserFromToken(function(data){

                    if(data.hasOwnProperty('id')){
                        $scope.loginmenu = data.username;

                        $scope.loginname = data.firstname + ' ' + data.lastname;
                        $scope.loginpanel = true;
                    }else{
                        $scope.loginname = '';
                        $scope.loginpanel = true;
                    }
                })
            }
        });
    }
    else{
        store.remove('jwt');
        store.remove('logintype');
        Login.getUserFromToken(function(data){

            if(data.hasOwnProperty('id')){
                $scope.loginmenu = data.username;

                $scope.loginname = data.firstname + ' ' + data.lastname;
                $scope.loginpanel = true;
            }else{
                $scope.loginname = '';
                $scope.loginpanel = true;
            }
        })
        console.log('nonlog');
    }

    

  
    

    $scope.logout = function(){
        store.remove('jwt');
        store.remove('cart');
        $window.location = '/';
        FB.logout(function(response) {
  // user is now logged out
});

    }

    var get = function() {
    $http({
        url: appConfig.ResourceUrl + "/contactdetails/getinfo",
        method: "GET",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).success(function (data, status, headers, config) {

        $scope.contact = data[0];
          console.log($scope.contact)
        var str = data[0].address;
        str = str.split('\n');
        $scope.address = str;
    }).error(function (data, status, headers, config) {
        callback(data);
    });
}
get();


})