"use strict";

/* Controllers */

app.controller('UserregistrationCtrl', function($scope, $http, appConfig, UserregistrationFactory, Reservation, Notification, $modal, $window, store, $timeout){
    $scope.regform = true;
    $scope.user = {};
    // $scope.registeruser = function(user){
    //     $scope.saveloading = true;

    //     if(user.email != user.emailagain){
    //         $scope.errormsg = 'There was a problem with your request';
    //         $scope.errormsg1 = 'Please check your emails match and try again.';
    //         $scope.errorpanel = true;
    //         $scope.saveloading = false;
    //     }
    //     else{
    //         if(user.password != user.passwordagain){
    //             $scope.errormsg = 'There was a problem with your request';
    //             $scope.errormsg1 = 'Please check your passwords match and try again.';
    //             $scope.errorpanel = true;
    //             $scope.saveloading = false;
    //         }
    //         else{
    //             $scope.errorpanel = false;

    //             UserregistrationFactory.registeruser(user, function(data){
    //                 console.log(data);
    //                 if(data.status == 'exist'){
    //                     $scope.regform = false;
    //                     $scope.emailexist = true;
    //                     $scope.email = data.email;
    //                     $scope.user = {};
    //                 }
    //                 else if(data.status == 'error'){
    //                     $scope.errorpanel = true;
    //                     $scope.errormsg = 'There was a problem with your request';
    //                     $scope.errormsg1 = 'Please try again later.';
    //                 }
    //                 else if(data.status == 'success'){
    //                     $scope.regsuccess = true;
    //                     $scope.regform = false;
    //                     $scope.emailexist = false;
    //                     $scope.user = {};
    //                 }


    //                 $scope.saveloading = false;
    //             })

    //         }
    //     }
    // }

    $scope.backtoreg = function(){
        $scope.regform = true;
        $scope.emailexist = false;
    }

    $scope.checkEmail = function(val){
        $scope.emailexist = 0;
        $scope.existinguser = '';
        var re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if(re.test(val)){
            Reservation.checkmemberemail(val, function(data){
                if(data.length == 0){
                    $scope.emailexist = 2;
                    console.log("Clear not used");
                    $scope.user = {email: val, reservationlist: [], paymenttype: '', country: 'USA'};
                }else{
                    if(data[0].status=='INCOMPLETE'){
                        $scope.emailexist = 3;
                        console.log('Incomplete Profile');
                    }else{
                        $scope.emailexist = 1;
                        console.log("Email is used");
                        $scope.existinguser = data;
                    }
                }
            });
        }
    }

    var set = function(){
        $scope.register = { country: 'USA' };
    }

    set();

    $scope.registerUser = function(member, form){
        $scope.saveloading = true;
        member['formfrom'] = 'registration';
        $scope.saving = true;
        Reservation.registration(member, function(data){
            if(!data.hasOwnProperty('error')){
                form.$setPristine();
                $scope.login=true;
                set();
                // Notification.success({message: data.success, positionY: 'bottom', positionX: 'right'});
                $scope.saving = false;
                $scope.regsuccess = true;
                $scope.regform = false;
                $scope.emailexist = false;
                $scope.user = {};
                $scope.email = member.email;
                $scope.saveloading = false;
            }else if(data.hasOwnProperty('error')){
                Notification.error({message: data.error, positionY: 'bottom', positionX: 'right'});
                $scope.saving = false;
                $scope.saveloading = false;
            }
        });
    }

    $scope.passwordmatch = function(password, repassword) {
      if(password == repassword) {
        $scope.registration.repassword.$setValidity("pwmatch",true);
      } else {
        $scope.registration.repassword.$setValidity("pwmatch",false);
      }
    }

})
