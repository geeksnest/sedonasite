'use strict';

/* Controllers */

app.controller('SpecialpreviewCtrl', function($scope, Login, store,$sce, Special, Pagination, $window){
var x = $('#slugs').val()
// console.log(x)

$scope.trustAsHtml = $sce.trustAsHtml;
var cars = JSON.parse(localStorage["page"]);
$scope.eventSources = [];
//meta info
$scope.pagetype = cars['type'];
$scope.page = cars['pagetype'];
$scope.body1 = $sce.trustAsHtml(cars['body']);

//banner
$scope.banner = cars['banner'];
$scope.bgcolor = cars['bgcolor'];
$scope.align = cars['align'];
$scope.box = cars['box'];
$scope.boxcolor = cars['boxcolor'];
$scope.boxedge = cars['boxedge'];
$scope.boxtrans = cars['boxtrans'];
$scope.btnname = cars['btnname'];
$scope.btnlink = cars['btnlink'];
$scope.color = cars['color'];
$scope.boxcolor = cars['boxcolor'];
$scope.boxedge = cars['boxedge'];
$scope.boxtrans = cars['boxtrans'];
$scope.imagethumb = cars['imagethumb'];
$scope.imagethumbsubtitle = cars['imagethumbsubtitle'];
$scope.thumbdesc = cars['thumbdesc'];


//column
$scope.column = cars['column'];
//sidebar
if (cars['sidebarleft'] == true || cars['sidebarleft'] == 'true') {
     $scope.leftsidebar = true;
}else{
    $scope.leftsidebar = false;
}
if (cars['sidebarright'] == true || cars['sidebarright'] == 'true') {
     $scope.rightsidebar = true;
}else{
    $scope.rightsidebar = false;
}


$scope.sidebarleft = cars['sidedataL'];
$scope.sidebarright = cars['sidedataR'];
////////

Special.loadtesti(function(data) {
$scope.testi = data.testi;
$scope.shortcode = data.shortcode;
$scope.children = data.children;
$scope.archive = data.archive;
});

Special.loadnews(function(data) {
$scope.news = data;
 for(var key in data){
                var categorylist = [];
                data[key].category.map(function(val){
                    categorylist.push(val.categoryname);
                });
                data[key]['categorylist'] = categorylist.join(", ");
            }

});



})
