'use strict';

/* Controllers */

app.controller('TestresultCtrl', function($scope, Login, store, Testtaker,Pagination, $localStorage,$window,$sce){

    console.log('==== TEST result ====');

// console.log($localStorage.asset);

Testtaker.displayresult($localStorage.asset, function(data)
{
	$scope.result = data.result[0].result;
	$scope.description = $sce.trustAsHtml(data.result[0].description)

	console.log(data.result[0].description)
 	$scope.services = data.services;
})


$scope.again = function()
{
  $window.location.href = 'testlist';
}

$scope.go = function(link)
{
  $window.location.href = link;
}


})
