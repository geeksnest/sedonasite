'use strict';

/* Controllers */

app.controller('MenuCtrl', function($scope, $http, appConfig, Menu, $window, $sce, $timeout){
    
 
    var shortCode = document.getElementById('shortCode').value;
    var loadSubMenuList = function () { 

        Menu.menulist(shortCode,function(data){
         
            $scope.webmenu = data.data; 
            $scope.mobmenu = data.data;
   
        });
    }
    loadSubMenuList();

    $timeout(function() {
        $(".flexnav").flexNav();
    }, 2000);

})
