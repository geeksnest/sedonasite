'use strict';

/* Controllers */

app.controller('SpecialCtrl', function($scope, Login, store,$sce, Special, Pagination, $window){
var pageslugs = $('#slugs').val()

$scope.eventSources = [];
// //meta info
$scope.redirectNews = function(link){
        $window.location.href = '/blog/' + link;
    }

Special.loadall(pageslugs,function(data) {

    $scope.pagetype = data.normal[0]['pagetype'];
    $scope.body1 = $sce.trustAsHtml(data.normal[0]['body']);

    //sidebar
    if (data.normal[0]['leftsidebar'] == true || data.normal[0]['leftsidebar'] == 'true') {
       $scope.leftsidebar = true;
    }else{
        $scope.leftsidebar = false;
    }
    if (data.normal[0]['rightsidebar'] == true || data.normal[0]['rightsidebar'] == 'true') {
       $scope.rightsidebar = true;
    }else{
        $scope.rightsidebar = false;
    }
    $scope.sidebarleft = data.leftsidebardata;
    $scope.sidebarright = data.rightsidebardata;
    //col
    $scope.column = data.datacol;

//banner
$scope.ban = data.normal[0];

});

Special.loadtesti(function(data) {
$scope.testi = data.testi;
$scope.shortcode = data.shortcode;
$scope.children = data.children;
$scope.archive = data.archive;
});

Special.loadnews(function(data) {
$scope.news = data;
 for(var key in data){
                var categorylist = [];
                data[key].category.map(function(val){
                    categorylist.push(val.categoryname);
                });
                data[key]['categorylist'] = categorylist.join(", ");
            }

});



})
