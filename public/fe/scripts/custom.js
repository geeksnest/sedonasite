/* JS */


$(document).ready(function(){
  $(".totop").hide();
    if($('#regcountry').length){
        print_country("regcountry");
        print_state('regstate', 239);
    }

  if($('#userccregcountry').length){
    print_country("userccregcountry");
    //print_state('regstate', 239);
  }

  if($('#userecheckcountry').length){
    print_country("userecheckcountry");
    //print_state('regstate', 239);
  }


  $(function(){
    $(window).scroll(function(){
      if ($(this).scrollTop()>400)
      {
        $('.totop').fadeIn();
      } 
      else
      {
        $('.totop').fadeOut();
      }
    });

    $('.totop a').click(function (e) {
      e.preventDefault();
      $('body,html').animate({scrollTop: 0}, 500);
    });

  });
});

