/**
 * Created by ebautistajr on 3/18/15.
 */

app.directive('userExistsValidator', function($q, User) {
    return {
        require : 'ngModel',
        link: function($scope, element, attrs, ngModel) {
            ngModel.$asyncValidators.exists = function(value) {
                var defer = $q.defer();
                var params = {};
                params[attrs.name] = value;

                User.usernameExists(params, function(data) {
                    if(data.exists == 'true') {
                        defer.reject();
                    } else {
                        defer.resolve();
                    }
                });
                return defer.promise;
            };
        }
    };
}).directive('emailExistsValidator', function($q, User) {
    return {
        require : 'ngModel',
        link: function($scope, element, attrs, ngModel) {
            ngModel.$asyncValidators.exists = function(value) {
                var defer = $q.defer();
                var params = {};
                params[attrs.name] = value;

                User.emailExists(params, function(data) {
                    if(data.exists == 'true') {
                        defer.reject();
                    } else {
                        defer.resolve();
                    }
                });
                return defer.promise;
            };
        }
    };
}).directive('passStrength', function() {
    return {
        // Restrict it to be an attribute in this case
        restrict: 'A',
        // responsible for registering DOM listeners as well as updating the DOM
        link: function(scope, element, attrs) {
            $(element).strength({
                strengthClass: 'strength',
                strengthMeterClass: 'strength_meter',
                strengthButtonClass: 'button_strength',
                strengthButtonText: 'Show Password',
                strengthButtonTextToggle: 'Hide Password'
            });
        }
    }
}).directive('onlyDigits', function () {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel) {
            if (!ngModel) return;
            ngModel.$parsers.unshift(function (inputValue) {
                var digits = inputValue.split('').filter(function (s) { return (!isNaN(s) && s != ' '); }).join('');
                ngModel.$viewValue = digits;
                ngModel.$render();
                return digits;
            });
        }
    };
})