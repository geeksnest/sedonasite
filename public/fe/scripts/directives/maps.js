/**
 * Created by ebautistajr on 4/7/15.
 */
app.directive('dynamic', function ($compile) {
  return {
    restrict: 'A',
    replace: true,
    link: function (scope, ele, attrs) {
      scope.$watch(attrs.dynamic, function(html) {
        ele.html(html);
        $compile(ele.contents())(scope);
      });
    }
  };
});

app.directive('fancybox', function(){
  return {
    restrict: 'A',

    link: function(scope, element, attrs){
      $(element).fancybox({
        type        :'iframe',
        scrolling   : 'no',
        maxWidth    : 800,
        maxHeight   : 400,
        fitToView   : true,
        width       : '70%',
        height      : '70%',
        autoSize    : false,
        closeClick  : true,
        openEffect  : 'none',
        closeEffect : 'none'
      });
    }
  }
});