/**
 * Created by ebautistajr on 3/19/15.
 */
app.factory('Menu', function (appConfig, $http) {
    return {
        menulist: function(shortCode,callback){
            $http({
                url: appConfig.ResourceUrl + "/menu/viewfrontendmenu/" + shortCode,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);
            }).error(function(data) {
                callback(data);
            });
        },
    }
});