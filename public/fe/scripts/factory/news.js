/**
 * Created by ebautistajr on 3/19/15.
 */
app.factory('News', function (appConfig, $http) {
    return {
        list: function (offset, page, callback) {
            $http({
                url: appConfig.ResourceUrl + "/news/frontend/listnews/" + offset + "/" + page,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
        },
        listbycat: function (category,offset, page, callback) {
            console.log(category);
            $http({
                url: appConfig.ResourceUrl + '/news/frontend/listnewsbycategory/' + category + "/" + offset + "/" + page,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
        },
        listbytag: function (tag,offset, page, callback) {
            $http({
                url: appConfig.ResourceUrl + '/news/frontend/listnewsbytag/' + tag + "/" + offset + "/" + page,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
        },
        listbyarchive: function (month, year, offset, page, callback){
            $http({
                url: appConfig.ResourceUrl + '/news/frontend/listnewsbyarchive/' + month + "/" +  year + "/"+ offset + "/" + page,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
        },
        author: function (auth,offset, page, callback) {
            $http({
                url: appConfig.ResourceUrl + '/news/frontend/author/' + auth + "/" + offset + "/" + page,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
        }
    }
});