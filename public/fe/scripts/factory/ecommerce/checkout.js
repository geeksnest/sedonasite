app.factory('CheckoutFactory', function($http, appConfig) {
  return {
    // mycart: function(cart, callback) {
    //     $http({
    //       url: appConfig.ResourceUrl + "/mycart/fe",
    //       method: "POST",
    //       headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    //       data: $.param({cart:cart})
    //     }).success(function (data, status, headers, config) {
    //         callback(data);
    //     }).error(function (data, status, headers, config) {
    //
    //     });
    // }
    placeorder: function(data, id, callback) {
        $http({
          url: appConfig.ResourceUrl + "/checkout/placeorder/" + id,
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(data)
        }).success(function (data, status, headers, config) {
            callback(data);
        }).error(function (data, status, headers, config) {

        });
    },
    login: function(data, callback){
        $http({
            url: appConfig.ResourceUrl +"/feuser/checkout/login",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(data)
        }).success(function (data, status, headers, config) {
            callback(data);
        }).error(function (data, status, headers, config) {
            callback(data);
        });
    },
    checkmail: function(email, callback) {
        $http({
            url: appConfig.ResourceUrl +"/feuser/checkemail/" + email,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (data, status, headers, config) {
            callback(data);
        }).error(function (data, status, headers, config) {
            callback(data);
        });
    },
    savepaypaltemp: function(data, callback) {
        $http({
            url: appConfig.ResourceUrl + "/shop/savepaypaltemp",
            method: "post",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(data)
        }).success(function (data, status, headers, config) {
            callback(data);
        }).error(function (data, status, headers, config) {
            callback({success: "Something went wrong please check your feilds!", type: 'danger'});
        });
    }
  };
});
