app.factory('MycartFactory', function($http, appConfig) {
  return {
    mycart: function(cart, callback) {
      $http({
        url: appConfig.ResourceUrl + "/mycart/fe",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param({cart:cart})
      }).success(function (data, status, headers, config) {
          callback(data);
      }).error(function (data, status, headers, config) {

      });
    },
    removeitem: function(productid, memberid, callback) {
      $http({
        url: appConfig.ResourceUrl + "/shop/removeitem/" + productid + "/" + memberid,
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function (data, status, headers, config) {
          callback(data);
      }).error(function (data, status, headers, config) {

      });
    },
    removeallitem: function(memberid, callback) {
      $http({
        url: appConfig.ResourceUrl + "/shop/removeallitem/" + memberid,
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function (data, status, headers, config) {
          callback(data);
      }).error(function (data, status, headers, config) {

      });
    },
    updatecart: function(id, quantity, callback) {
      $http({
        url: appConfig.ResourceUrl + "/shop/updatequantity/" + id + "/" + quantity,
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function (data, status, headers, config) {
          callback(data);
      }).error(function (data, status, headers, config) {

      });
    }
  }
})