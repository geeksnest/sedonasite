app.factory('ProductsFactory', function($http, appConfig) {
  return {
    listofproducts: function(page, limit, sort, keyword, callback) {
        $http({
          url: appConfig.ResourceUrl + "/products/fe/list/" + page + "/" + limit + "/" + sort + "/" + keyword,
          method: "get",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (data, status, headers, config) {
            callback(data);
        }).error(function (data, status, headers, config) {

        });
    },
    addtocart: function(data, callback) {
      $http({
        url: appConfig.ResourceUrl + "/shop/addtocart",
        method: "post",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(data)
      }).success(function (data, status, headers, config) {
        callback(data);
      }).error(function (data, status, headers, config) {
        callback({success: "Something went wrong please check your feilds!", type: 'danger'});
      });
    }
  }
})