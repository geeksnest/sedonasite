app.factory('UserregistrationFactory', function($http, appConfig){
    return {
        registeruser: function(user,callback){
            $http({
                url: appConfig.ResourceUrl + "/feuser/registeruser",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(user)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({error: { msg: 'Something went wrong please check your fields'}});
            });
        },
        checkfacebook: function(fbid,callback){
            $http({
                url: appConfig.ResourceUrl + "/feuser/checkfacebook",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(fbid)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({error: { msg: 'Something went wrong please check your fields'}});
            });
        },
        
    }
});