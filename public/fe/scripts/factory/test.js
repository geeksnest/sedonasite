app.factory('Testtaker', function($http, appConfig){
    return {
        asset: [],

       taketest: function (callback){
            $http({
                url: appConfig.ResourceUrl +"/test/testtaker",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        testname: function (callback){
            $http({
                url: appConfig.ResourceUrl +"/test/testname",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        testitems: function (testid,callback){
            $http({
                url: appConfig.ResourceUrl +"/test/testitems/"+testid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

         displayresult: function (asset,callback){
            $http({
                url: appConfig.ResourceUrl +"/test/displayresult",
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data:$.param(asset)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

               testmember: function (info,callback){
            $http({
                url: appConfig.ResourceUrl +"/test/testmember",
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data:$.param(info)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        
    }
});