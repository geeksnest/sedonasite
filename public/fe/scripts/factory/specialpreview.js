app.factory('Special', function($http, appConfig){
    return {
        asset: [],

       loadtesti: function(callback){
            $http({
                url: appConfig.ResourceUrl + '/special/loadtesti',
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);
            }).error(function(data) {
                callback(data)
            });
        },
        loadnews: function(callback){
            $http({
                url: appConfig.ResourceUrl + '/special/loadnews',
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);
            }).error(function(data) {
                callback(data)
            });
        },
        getinfo: function(callback){
            $http({
                url: appConfig.ResourceUrl + "/contactdetails/getinfo",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        loadall: function(pageslugs,callback){
            $http({
                url: appConfig.ResourceUrl + "/utility/special/"+pageslugs,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
         loadfooter: function(callback){
            $http({
                url: appConfig.ResourceUrl + "/utility/footer",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        
    }
});