/**
 * Created by ebautistajr on 3/30/15.
 */

app.factory('Login', function($http, store, appConfig, jwtHelper, $window,$q ,Facebook) {
    return {
        getUserFromToken: function (callback) {
            if (store.get('jwt')) {
                var token = store.get('jwt');
                callback(jwtHelper.decodeToken(token));
            } else {
                callback({'error': 'No login yet.'});
            }
        },
        redirectToMainifLogin: function(){
            if (store.get('jwt')) {
                if(!jwtHelper.isTokenExpired(store.get('jwt'))){
                    $window.location = '/';
                }else{
                    store.remove('jwt');
                }
            }
        },
        login: function(user, callback){
            $http({
                url: appConfig.ResourceUrl +"/feuser/login",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(user)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        getCurrentUser: function (){
            if (store.get('jwt')) {
                var token = store.get('jwt');
                return jwtHelper.decodeToken(token);
            } else {
                return null;
            }  
        },
        getMyLastName: function() {
            var deferred = $q.defer();
            FB.api('/me', {
                fields: 'last_name'
            }, function(response) {
                if (!response || response.error) {
                    deferred.reject('Error occured');
                } else {
                    deferred.resolve(response);
                }
            });
            return deferred.promise;
        }
    }
});