'use strict';

var init_module = [
    'ngAnimate',
    'ui.router',
    'ui.bootstrap',
    'ui.load',
    'ngCookies',
    'ngStorage',
    'ui.jq',
    'ui.validate',
    'app.factory',
    'app.directives',
    'bootstrapLightbox',
    'vcRecaptcha',
    'facebook',
    'ui-notification',
    'angular-storage',
    'angularMoment',
    'ui.calendar',
    'angular-jwt',
    'angularUtils.directives.dirPagination',
    'simplePagination',
    'angular.filter',
    'angular.vertilize',
    'ngCountryStateSelect',
    'uuid',
    'toaster'
];

var app = angular.module('app',init_module)
    .run([ '$rootScope','$stateParams', '$state',
        function ($rootScope,   $state,   $stateParams) {
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            $rootScope.varialbe = '';
        }
    ])
    .config(['NotificationProvider', 'FacebookProvider','$stateProvider', '$httpProvider',  '$locationProvider', '$controllerProvider', '$compileProvider',  '$provide', '$interpolateProvider','$urlRouterProvider',
        function (NotificationProvider, FacebookProvider, $stateProvider, $httpProvider,   $locationProvider,   $controllerProvider, $compileProvider,    $provide, $interpolateProvider, $urlRouterProvider) {
            FacebookProvider.init('1650027825276845');

            $httpProvider.interceptors.push(['$q', '$location', 'store', function($q, $location, store) {
                return {
                    'request': function (config) {
                        config.headers = config.headers || {};
                        if (store.get('jwt')) {
                            $httpProvider.defaults.headers.common['Authorization'] = 'Bearer ' + store.get('jwt');
                        }
                        return config;
                    },
                    'responseError': function(response) {
                        if(response.status === 401 || response.status === 403) {
                            store.remove('jwt')
                            window.location = '/';
                        }
                        return $q.reject(response);
                    }
                };
            }]);

            app.controller = $controllerProvider.register;
            app.directive  = $compileProvider.directive;
            app.factory    = $provide.factory;
            app.constant   = $provide.constant;
            app.value      = $provide.value;

            $interpolateProvider.startSymbol('{[{');
            $interpolateProvider.endSymbol('}]}');

            NotificationProvider.setOptions({
                delay: 12000,
                startTop: 20,
                startRight: 10,
                verticalSpacing: 20,
                horizontalSpacing: 20,
                positionX: 'left',
                positionY: 'bottom'
            });

            //$urlRouterProvider
            //    .otherwise('/');

            //
            //
            //$stateProvider
            //    .state('success', {
            //        url: "/success",
            //        templateUrl: "/registration/success"
            //    });

        }
    ]
)
