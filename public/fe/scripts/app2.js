'use strict';

var app = angular.module('app', [
    'ui-notification',
    'angular-storage',
    'angular-jwt',
    'facebook',
    'ui.bootstrap'
    ])
.config(function ($interpolateProvider,FacebookProvider){
    FacebookProvider.init('1650027825276845');
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');

})
.constant('appConfig', {
    // clientside specific constants
    ResourceUrl                        : "http://sedonaapi.dev",
    ClientToken                        : "82c4cb99-8440-41ef-a6a5-0e81d27b4c5f",
    ImageLinK                          : "https://sedonahealing.s3.amazonaws.com",
    sitekey                            : "6LfiYQsTAAAAAMZIe1BR0fLlSfJAdLxFMr4nzOlm",
    secretkey                          : "6LfiYQsTAAAAAHMNt62C45jNXDNfxfurQqwCERcz"
})
.directive('userExistsValidator', function($q, User) {
    return {
        require : 'ngModel',
        link: function($scope, element, attrs, ngModel) {
            ngModel.$asyncValidators.exists = function(value) {
                var defer = $q.defer();
                var params = {};
                params[attrs.name] = value;

                User.usernameExists(params, function(data) {
                    if(data.exists == 'true') {
                        defer.reject();
                    } else {
                        defer.resolve();
                    }
                });
                return defer.promise;
            };
        }
    };
}).directive('emailExistsValidator', function($q, User) {
    return {
        require : 'ngModel',
        link: function($scope, element, attrs, ngModel) {
            ngModel.$asyncValidators.exists = function(value) {
                var defer = $q.defer();
                var params = {};
                params[attrs.name] = value;

                User.emailExists(params, function(data) {
                    if(data.exists == 'true') {
                        defer.reject();
                    } else {
                        defer.resolve();
                    }
                });
                return defer.promise;
            };
        }
    };
}).directive('passStrength', function() {
    return {
// Restrict it to be an attribute in this case
restrict: 'A',
// responsible for registering DOM listeners as well as updating the DOM
link: function(scope, element, attrs) {
    $(element).strength({
        strengthClass: 'strength',
        strengthMeterClass: 'strength_meter',
        strengthButtonClass: 'button_strength',
        strengthButtonText: 'Show Password',
        strengthButtonTextToggle: 'Hide Password'
    });
}
}
}).directive('onlyDigits', function () {
return {
    restrict: 'A',
    require: '?ngModel',
    link: function (scope, element, attrs, ngModel) {
        if (!ngModel) return;
        ngModel.$parsers.unshift(function (inputValue) {
            var digits = inputValue.split('').filter(function (s) { return (!isNaN(s) && s != ' '); }).join('');
            ngModel.$viewValue = digits;
            ngModel.$render();
            return digits;
        });
    }
};
});